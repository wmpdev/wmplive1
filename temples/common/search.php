<div id="ticker" class="animate-me zoomIn">
	<div class=" temple-search-block">
		<?php
			if ($show_heading) {
		?>
		<h1 class="">Discover all the Temple's Located across</h1>
		<?php
			}
		?>
		<div class="temple-search">
			<form action="<?php echo Mage::getUrl('temples/results'); ?>" method="get" id="temple_search" name="temple_search">
				<input id="temple_name" class="input-text" name="temple_name" type="text" placeholder="Temple/Deity Name" value="<?php echo $_REQUEST['temple_name']; ?>"/><!-- long temple search -->
				<input id="temple_location" class="input-text" name="temple_location" type="text" placeholder="Location" value="<?php echo isset($temple_location)?$temple_location:$_REQUEST['temple_location']; ?>"/><!-- long location search -->
				<input id="is_ajax" name="is_ajax" type="hidden" value="0"/><!-- search id -->
				<input id="sid" name="sid" type="hidden" value="<?php echo $_REQUEST['sid']; ?>"/><!-- search id -->
				<input id="sts" name="sts" type="hidden" value="<?php echo $_REQUEST['sts']; ?>"/><!-- short temple search -->
				<input id="sls" name="sls" type="hidden" value="<?php echo $_REQUEST['sls']; ?>"/><!-- short location search -->
				<input id="sb" name="sb" type="hidden" value="<?php echo $_REQUEST['sb']; ?>"/><!-- sort by -->
				<input id="st" name="st" type="hidden" value="<?php //echo $_REQUEST['st']; ?>"/><!-- filter by temple id -->
				<input id="tsel" name="tsel" type="hidden" value="0"/><!-- is temple selected -->
				<input id="lsel" name="lsel" type="hidden" value="0"/><!-- is location selected -->
				<!--<input id="stp" name="stp" type="hidden" value="<?php echo $_REQUEST['temple_name']; ?>"/><!-- posted temple name -->
				<input id="sl" name="sl" type="hidden" value="<?php echo $_REQUEST['sl']; ?>"/><!-- filter by location -->
				<input id="sr" name="sr" type="hidden" value="<?php echo $_REQUEST['sr']; ?>"/><!-- filter by religion -->
				<input id="sd" name="sd" type="hidden" value="<?php echo $_REQUEST['sd']; ?>"/><!-- filter by deity -->
				<input id="sm" name="sm" type="hidden" value="<?php echo $_REQUEST['sm']; ?>"/><!-- filter by month -->
				<input id="su" name="su" type="hidden" value="<?php echo (empty($_REQUEST['su']))?'':$_REQUEST['su']; ?>"/><!-- filter by UNESCO -->
				<input id="sld" name="sld" type="hidden" value="<?php echo (empty($_REQUEST['sld']))?'':$_REQUEST['sld']; ?>"/><!-- filter by live darshan link -->
				<input id="sop" name="sop" type="hidden" value="<?php echo (empty($_REQUEST['sop']))?'':$_REQUEST['sop']; ?>"/><!-- filter by online prasad link -->
				<input id="spt" name="spt" type="hidden" value="<?php echo (empty($_REQUEST['spt']))?'':$_REQUEST['spt']; ?>"/><!-- filter by pooja in temple link -->
				<input id="sbh" name="sbh" type="hidden" value="<?php echo (empty($_REQUEST['sbh']))?'':$_REQUEST['sbh']; ?>"/><!-- filter by book hotels link -->
				<input id="start_at" name="start_at" type="hidden" value="<?php echo ($_REQUEST['start_at'] == 0)?0:$_REQUEST['start_at']; ?>"/><!-- records starting at -->
				<input id="loc_type" name="loc_type" type="hidden" value="<?php echo $_REQUEST['loc_type']; ?>"/><!-- location type ( city / state / country ) -->
				<input id="loc_id" name="loc_id" type="hidden" value="<?php echo $_REQUEST['loc_id']; ?>"/><!-- location id -->
				<input id="t_search_by" name="t_search_by" type="hidden" value="short"/><!-- do a search by temple ( short / long ) -->
				<input id="l_search_by" name="l_search_by" type="hidden" value="short"/><!-- do a search by location ( short / long ) -->
				<button type="submit" title="Search" class="button" id="searchtemp">
					<span><i class="fa fa-search"></i> Search </span>
				</button>
			</form>
		</div>
	</div>
</div>
<style>
  .ui-autocomplete-loading {
	background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
  }
  .ui-autocomplete-term { font-weight: bold; color: blue; }
</style>
<script language="javascript">
	jQuery(document).ready(function ($) {
		$.ui.autocomplete.prototype._renderItem = function (ul, item) {
			item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
			return $("<li></li>")
					.data("item.autocomplete", item)
					.append("<a>" + item.label + "</a>")
					.appendTo(ul);
		};

		$( "#temple_name" ).autocomplete({
			source: 'search?action=get_temples',
			minLength: 2,
			html: true,
			open: function() {
				//$("#t_search_by").val('short');
				$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			},
			close: function() {
				$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			},
			select: function(event, ui) {
				$("#sid").val('');
				$("#t_search_by").val('long');
				$("#sts").val($(this).val());
				$("#tsel").val('1');
				$("#st").val(ui.item.value);
				$("#sb").val('');
				$("#sl").val('');
				$("#sr").val('');
				$("#sd").val('');
				$("#temple_name").val(ui.item.text);
				$("#is_ajax").val('0');
				//$("#temple_search").submit();
				window.location.href = '/temples/' + ui.item.value;
				return false;
			},
			focus: function (event, ui) {
			   this.value = ui.item.text;
			   event.preventDefault(); // Prevent the default focus behavior.
			},
		});

		$( "#temple_location" ).autocomplete({
			source: 'search?action=get_locations',
			minLength: 2,
			html: true,
			open: function() {
				//$("#l_search_by").val('short');
				$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
			},
			close: function() {
				$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
			},
			select: function(event, ui) {
				$("#start_at").val('0');
				$("#sid").val('');
				$("#l_search_by").val('long');
				$("#lsel").val('1');
				$("#loc_type").val(ui.item.type);
				$("#loc_id").val(ui.item.location);
				$("#sls").val($(this).val());
				$("#sb").val('');
				$("#sl").val('');
				$("#sr").val('');
				$("#sd").val('');
                $("#temple_search").submit();
			},
		});

		$("#temple_search").submit(function(event) {
			if ($("#is_ajax").val() == 0) {
				$("#start_at").val('0');
				$("#sid").val('');
				$("#su").val('');
				$("#sb").val('');
				$("#sl").val('');
				$("#sr").val('');
				$("#sd").val('');
				$("#sm").val('');
				$("#sts").val('');
				$("#sls").val('');
				$("#sld").val('');
				$("#sop").val('');
				$("#spt").val('');
				$("#sbh").val('');
				$("#sls").val('');
				if ($("#lsel").val() == '0') {
					$("#loc_type").val('');
					$("#loc_id").val('');
				}
			}
			if (parseInt($("#start_at").val()) != 0) {
				event.preventDefault();
				$("#start_at").val('0');
				$(this).submit();
				return false;
			}			
		});
	});
</script>
