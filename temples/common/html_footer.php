<div class="f-block footer-top-wr">
	<section class="f-fix"></section>
	<div class="f-fix freeshipping-wrapper">
		<div class="container"></div>
	</div>
</div>

<div class="footer-container f-fix mainFooterPan01">
	<div class="f-block footer-top-wr">
		<div class="f-fix footer-blocks">
			<div class="container">
				<div class="f-fix">
					<div class="span3 news-letter">
						<h3>Newsletter</h3>
						<form action="http://www.wheresmypandit.com/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
							<div class="form-subscribe">
								<h4><i class="fa fa-envelope-o"></i> <span>Sign up for our newsletter</span></h4>
								<div class="input-box">
									<div class="msg-block f-fix">
										<input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email">
									</div>
									<button type="submit" title="Submit" class="button"><span>Subscribe</span></button>
								</div>
							</div>
						</form>
						<script type="text/javascript">
						//<![CDATA[
							var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
							new Varien.searchForm('newsletter-validate-detail', 'newsletter', 'Enter email address');
						//]]>
						</script>
					</div>
					<div class="span3 key-feature">
						<h3>Key Features</h3>
						<ul>
							<li><em class="fa fa-retweet"></em> <span>30 days return Policy</span></li>
							<li><em class="fa fa fa-shield"></em><span> Safe &amp; secure shipping</span></li>
							<li><em class="fa fa fa-lightbulb-o"></em> <span>promotions</span></li>
							<li><em class="fa fa-globe"></em><span>All Over Mumbai Delivery</span></li>
						</ul>
					</div>
					<div class="span3 lattest-tweets">
						<h3>Latest Tweets</h3>
						<div class="tweet">
						<ul class="twitter-twets">
							<li>
								#TheSacredSeven

								Vow #5 is for Progeny:

								The couple talks about their friendship and future here, their kids!... <a target="_blank" rel="nofollow" href="http://t.co/QPGfbZ42fi">http://t.co/QPGfbZ42fi</a>            <span> - 12 minutes ago</span>
							</li>
							<li>
								#TheSacredSeven
								The couple has now progressed to Vow #4, Family:

								Here, they pray for a joyous and peaceful life,... <a target="_blank" rel="nofollow" href="http://t.co/eDhURDKoNP">http://t.co/eDhURDKoNP</a>            <span> - 1 day ago</span>
							</li>
							<li>
								#TheSacredSeven
								As the couple move forward, they take #Vow3, for Prosperity, for
								preservation of wealth and... <a target="_blank" rel="nofollow" href="http://t.co/gO0syNMeSR">http://t.co/gO0syNMeSR</a>            <span> - 1 day ago</span>
							</li>
							<li>
								What <a target="_blank" rel="nofollow" href="http://twitter.com/search?q=%23WeddingVows">#WeddingVows</a> mean?
								<a target="_blank" rel="nofollow" href="http://twitter.com/search?q=%23Didyouknow">#Didyouknow</a> Vow 2 is for Strength:
								This Vow talks about the couples appeal to the... <a target="_blank" rel="nofollow" href="http://t.co/etAhhGZG3V">http://t.co/etAhhGZG3V</a>            <span> - 2 days ago</span>
							</li>
						</ul>
					</div>
				</div>
			
				<div class="span3 find-on-fb">
					<h3>Find us on Facebook</h3>
					<div class="block-content">                                                
						<div id="fb-root" class=" fb_reset">
							<div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
								<div>
									<iframe name="fb_xdm_frame_http" frameborder="0" allowtransparency="true" scrolling="no" id="fb_xdm_frame_http" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="http://static.ak.facebook.com/connect/xd_arbiter/QjK2hWv6uak.js?version=41#channel=f196a093a8&amp;origin=http%3A%2F%2Fwww.wheresmypandit.com" style="border: none;"></iframe>
									<iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" scrolling="no" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://s-static.ak.facebook.com/connect/xd_arbiter/QjK2hWv6uak.js?version=41#channel=f196a093a8&amp;origin=http%3A%2F%2Fwww.wheresmypandit.com" style="border: none;"></iframe>
								</div>
							</div>
							<div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
								<div></div>
							</div>
						</div>
						<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));
						</script>
						
						<div class="fb-like-box fb_iframe_widget" data-href="https://www.facebook.com/wheresmypandit" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;color_scheme=light&amp;header=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fwheresmypandit&amp;locale=en_US&amp;sdk=joey&amp;show_border=false&amp;show_faces=true&amp;stream=false">
							<span style="vertical-align: bottom; width: 300px; height: 241px;">
								<iframe name="f2f4ed2f98" width="1000px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:like_box Facebook Social Plugin" src="http://www.facebook.com/plugins/like_box.php?app_id=&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FQjK2hWv6uak.js%3Fversion%3D41%23cb%3Df257a84378%26domain%3Dwww.wheresmypandit.com%26origin%3Dhttp%253A%252F%252Fwww.wheresmypandit.com%252Ff196a093a8%26relation%3Dparent.parent&amp;color_scheme=light&amp;header=false&amp;href=https%3A%2F%2Fwww.facebook.com%2Fwheresmypandit&amp;locale=en_US&amp;sdk=joey&amp;show_border=false&amp;show_faces=true&amp;stream=false" style="border: none; visibility: visible; width: 300px; height: 241px;" class=""></iframe>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="f-fix footerBottom">
		<footer class="footer container">
			<div class="media-payment f-block">
				<div class="span6">
					<!--social links-->
					<ul>
					   <li class="facebook"><a title="facebook" href="https://www.facebook.com/wheresmypandit" target="_blank"><i class="fa fa-facebook"></i></a></li>
					
					   <li class="twitter"><a title="twitter" href="https://twitter.com/wheresmypandit" target="_blank"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
				<div class="span6">
					<div class="f-right">
						<img src="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/images/payment.png" alt="">
					</div>
				</div>
			</div>
			<div class="footer-links f-fix">
				<div class="link-block">
					<h3>Information</h3>
					<ul>
						<li><a title="About Us" href="http://www.wheresmypandit.com/about-us/"><span>About</span> Us</a></li>
						<li><a title="Privacy Policy" href="http://www.wheresmypandit.com/privacy-policy/"><span>Privacy</span> Policy</a></li>
						<li><a title="Cancellations &amp; Returns" href="http://www.wheresmypandit.com/cancellations-returns/"><span>Cancellations</span> &amp; Returns</a></li>
						<li><a title="Terms of Use" href="http://www.wheresmypandit.com/terms-use/"><span>Terms</span> of Use</a></li>
					</ul>
				</div>
				<div class="link-block">
					<h3>Customer Service</h3>
					<ul>
						<li><a title="Contact Us" href="http://www.wheresmypandit.com/contacts/"><span>Contact</span> Us</a></li>
						<li><a title="Returns" href="http://www.wheresmypandit.com/cancellations-returns/"><span>Returns</span></a></li>
						<!--<li><a title="Site Map" href="http://www.wheresmypandit.com/catalog/seo_sitemap/category/"><span>Site</span> Map</a></li>-->
					</ul>
				</div>
				<div class="link-block">
					<h3>Extras</h3>
					<ul>
						<li><a title="Affiliates" href="#"><span>Affiliates</span></a></li>
						<li><a title="Specials" href="#"><span>Specials</span></a></li>
					</ul>
				</div>
				<div class="link-block">
					<h3>My Account</h3>
					<ul>
						<li><a title="My Account" href="http://www.wheresmypandit.com/customer/account/"><span>My</span> Account</a></li>
						<li><a title="Order History" href="http://www.wheresmypandit.com/sales/order/history/"><span>Order</span> History</a></li>
						<li><a title="Wish List" href="http://www.wheresmypandit.com/wishlist/"><span>Wish</span> List</a></li>
					</ul>
				</div>
				<div class="link-block">
					<h3>Contact</h3>
					<p><em class="fa fa-map-marker">&nbsp;</em>Plot No. 156-158,</p>
					<p>Shree Shakambhari Corporate Park,</p>
					<p>Chakrvarti Ashok Complex,</p>
					<p>J B Nagar, Andheri (E), Mumbai-99</p>
					<p>Telephone: 022 67079797</p>
					<p><em class="fa fa-envelope">&nbsp;</em> <a href="mailto:<?php echo $config['mail']['support']; ?>"><?php echo $config['mail']['support']; ?></a></p>
				</div>
			</div>
			<div class="copyright f-block">
				<ul class="footer-bottom-links ul-left">
					<li>
						<a title="Privacy Policy" href="http://www.wheresmypandit.com/privacy-policy/">Privacy Policy</a> | <a title="Terms &amp; Conditions" href="http://www.wheresmypandit.com/terms-use/">Terms &amp; Conditions</a>
					</li>
				</ul>
				<p class="copyText f-right">&copy; 2014 Where's My Pandit. All Rights Reserved.</p>
			</div>
			<div class="clear"></div>
		</footer>
	</div>
</div>
<div id="back-top" class="goTop" style="display: none;">
	<a rel="tooltip" data-placement="left" href="javascript:void(0);" data-original-title="">
		<i class="fa fa-arrow-up"></i><span> </span>
	</a>
</div>
<script type="text/javascript">
    var ajax_cart_show_popup = 0;
            
    var loadingW = 260;
    var loadingH = 80;
    var confirmW = 420;
    var confirmH = 150;
</script>

<div class="mdl-overlay" id="mdl-overlay" style="display: none;">&nbsp;</div>

<div style="display: none; width: 260px; height: 80px; top: 550px;" class="mdlajax-progress" id="mdl_ajax_progress">
	&nbsp;
</div>
<div class="mdl-loading-data" id="mdl-loading-data" style="display: none;">
    <img alt="loading..." src="http://www.wheresmypandit.com/skin/frontend/nextlevel/nextlevel/images/loading.gif">
    <p>loading...</p>
</div>

<div style="display: none; width: 420px; height: 150px; position: fixed; top: 496px;" class="mdlajax-confirm" id="mdl_ajax_confirm">
	&nbsp;
</div>

<div id="mdl-temp-div" style="display:none;"></div>
<div class="clear"></div>
    </div>
</div>


</div>
</html>