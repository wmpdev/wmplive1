<?php
	$mageFilename = '../app/Mage.php';
	require_once $mageFilename;
	//Mage::setIsDeveloperMode(true);
	//ini_set('display_errors', 1);
	umask(0);
	Mage::app();
	Mage::getSingleton('core/session', array('name'=>'frontend'));
	
	$session = Mage::getSingleton('customer/session');
	if($session->isLoggedIn()) {
		$userId = $session->getCustomer()->getId();
	} else {
		$userId = 0;
	}

	$block              = Mage::getSingleton('core/layout');
	 
	# HEAD BLOCK
	$headBlock          = $block->createBlock('page/html_head');// this wont give you the css/js inclusion
    //set meta details
    if($record_temple->page_title)
    $headBlock->setTitle($record_temple->page_title);
//$headBlock->addLinkRel($record_temple->canonical_tag);
    $headBlock->setDescription($record_temple->meta_description);
    $headBlock->setKeywords($record_temple->meta_keywords);
	// add js
	$headBlock->addJs('prototype/prototype.js');
	$headBlock->addJs('lib/ccard.js');
	$headBlock->addJs('prototype/validation.js');
	$headBlock->addJs('scriptaculous/builder.js');
	$headBlock->addJs('scriptaculous/effects.js');
	$headBlock->addJs('scriptaculous/dragdrop.js');
	$headBlock->addJs('scriptaculous/controls.js');
	$headBlock->addJs('scriptaculous/slider.js');
	$headBlock->addJs('varien/js.js');
	$headBlock->addJs('varien/form.js');
	$headBlock->addJs('varien/menu.js');
	$headBlock->addJs('mage/translate.js');
	$headBlock->addJs('mage/cookies.js');
	# add css
	$headBlock->addCss('css/styles.css');
	$headBlock->getCssJsHtml();
	$headBlock->getIncludes();
	 
	# HEADER BLOCK
	$headerBlock        = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml')->toHtml();
	 
	# FOOTER BLOCK
	$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
?>
<!DOCTYPE html>
<html>
<head>
	<?php if($record_temple->canonical_tag): ?>
		<link rel="canonical" href="<?php echo $record_temple->canonical_tag ?>" />
	<?php endif; ?>
	<?php 	echo $headBlock->toHtml(); ?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Temple Search</title>

        <!--
	<link rel="icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
	<link rel="shortcut icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
        -->
        
        <!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

        
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/inner_css/home.css" type="text/css" rel="stylesheet">
	<!-- <link href="<?php echo Mage::getBaseUrl(); ?>temples/inner_css/styles.css" type="text/css" rel="stylesheet"> -->
	<link href="https://www.wheresmypandit.com/skin/frontend/wmp/default/css/wmplatest/css/main.css" rel="stylesheet" media="screen">
	<style>
		footer{
			padding-top: 40px;
		}
		button#searchtemp{
			padding-top: 8px;
			padding-bottom: 8px;
		}
	</style>
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/style.css" type="text/css" rel="stylesheet">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/inner_css/orange-color.css" type="text/css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
	<!-- STYLESHEETS -->
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/animate.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/inner_css/font-awesome.css" type="text/css" rel="stylesheet">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/style.css" rel="stylesheet" media="screen">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/options.css" rel="stylesheet" media="screen">
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/css/responsive.css" rel="stylesheet" media="screen">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

	<script src="<?php echo Mage::getBaseUrl(); ?>temples/js/thumbelina.js"></script>

	<script type="text/javascript" src="<?php echo Mage::getBaseUrl(); ?>temples/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo Mage::getBaseUrl(); ?>temples/js/jquery.accordion.js"></script>
	<link href="<?php echo Mage::getBaseUrl(); ?>temples/js/jquery-ui.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo Mage::getBaseUrl(); ?>temples/js/customslide.js"></script>

	<script type="text/javascript" src="<?php echo Mage::getBaseUrl(); ?>temples/js/jssor.js"></script>
	<script type="text/javascript" src="<?php echo Mage::getBaseUrl(); ?>temples/js/jssor.slider.js"></script>
</head>

