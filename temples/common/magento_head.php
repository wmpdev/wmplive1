<?php
	$mageFilename = '/var/www/html/wmp/app/Mage.php';
	require_once $mageFilename;
	//Mage::setIsDeveloperMode(true);
	//ini_set('display_errors', 1);
	umask(0);
	Mage::app();
	Mage::getSingleton('core/session', array('name'=>'frontend'));
	 
	$block              = Mage::getSingleton('core/layout');
	 
	# HEAD BLOCK
	$headBlock          = $block->createBlock('page/html_head');// this wont give you the css/js inclusion
	// add js
	$headBlock->addJs('prototype/prototype.js');
	$headBlock->addJs('lib/ccard.js');
	$headBlock->addJs('prototype/validation.js');
	$headBlock->addJs('scriptaculous/builder.js');
	$headBlock->addJs('scriptaculous/effects.js');
	$headBlock->addJs('scriptaculous/dragdrop.js');
	$headBlock->addJs('scriptaculous/controls.js');
	$headBlock->addJs('scriptaculous/slider.js');
	$headBlock->addJs('varien/js.js');
	$headBlock->addJs('varien/form.js');
	$headBlock->addJs('varien/menu.js');
	$headBlock->addJs('mage/translate.js');
	$headBlock->addJs('mage/cookies.js');
	# add css
	$headBlock->addCss('css/styles.css');
	$headBlock->getCssJsHtml();
	$headBlock->getIncludes();
	 
	# HEADER BLOCK
	$headerBlock        = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml')->toHtml();
	 
	# FOOTER BLOCK
	$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
?>
<?php echo $headBlock->toHtml(); ?>