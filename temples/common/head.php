<?php
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
//Mage::setIsDeveloperMode(true);
//ini_set('display_errors', 1);
umask(0);
Mage::app('default');
Mage::getSingleton('core/session', array('name' => 'frontend'));

$session = Mage::getSingleton('customer/session');
if ($session->isLoggedIn()) {
    $userId = $session->getCustomer()->getId();
} else {
    $userId = 0;
}

$block = Mage::getSingleton('core/layout');

# HEAD BLOCK
$headBlock = $block->createBlock('page/html_head'); // this wont give you the css/js inclusion
//set meta details
//$headBlock->setDescription("test");exit;
if($record_temple->page_title)
$headBlock->setTitle($record_temple->page_title);
if($record_temple->canonical_tag)
$headBlock->addLinkRel($record_temple->canonical_tag);
$headBlock->setDescription($record_temple->meta_description);
$headBlock->setKeywords($record_temple->meta_keywords);
// add js
$headBlock->addJs('prototype/prototype.js');
$headBlock->addJs('lib/ccard.js');
$headBlock->addJs('prototype/validation.js');
$headBlock->addJs('scriptaculous/builder.js');
$headBlock->addJs('scriptaculous/effects.js');
$headBlock->addJs('scriptaculous/dragdrop.js');
$headBlock->addJs('scriptaculous/controls.js');
$headBlock->addJs('scriptaculous/slider.js');
$headBlock->addJs('varien/js.js');
$headBlock->addJs('varien/form.js');
$headBlock->addJs('varien/menu.js');
$headBlock->addJs('mage/translate.js');
$headBlock->addJs('mage/cookies.js');
# add css
$headBlock->addCss('css/styles.css');
$headBlock->getCssJsHtml();
$headBlock->getIncludes();

# HEADER BLOCK
//$headerBlock        = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml')->toHtml();
# FOOTER BLOCK
$footerBlock = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
?>
<!DOCTYPE html>
<html>
    <head>
    	<?php if($record_temple->canonical_tag): ?>
			<link rel="canonical" href="<?php echo $record_temple->canonical_tag ?>" />
		<?php endif; ?>
    	<?php echo $headBlock->toHtml(); ?>
        <meta charset="utf-8">
        <!-- PAGE TITLE -->

        <!-- MAKE IT RESPONSIVE -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- SEO -->
        <!--  <title>Temple Search</title>  
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        -->

        <!-- BEGIN GOOGLE ANALYTICS CODEs -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53702404-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!-- END GOOGLE ANALYTICS CODE -->

        <!-- FAVICON -->
        <link rel="icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
        <link rel="shortcut icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
        <!--<link rel="shortcut icon" href="images/favicon.png" />-->
        <!-- STYLESHEETS -->
        <!--<link rel="stylesheet" id="main-styles-css" href="./Salient_files/style.css" type="text/css" media="all">-->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="https://www.wheresmypandit.com/skin/frontend/wmp/default/css/wmplatest/css/main.css" rel="stylesheet" media="screen">
        <style>
            .temple-search-block .temple-search button.button span {
                padding: 16px!important;
                line-height: 48px!important;
                
            }

            .temple-search-block .temple-search input{
            color: #aaa;
            }
        </style>
        <link href="css/style.css" rel="stylesheet" media="screen">
        <!-- <link href="inner_css/styles.css" rel="stylesheet" media="screen"> -->
        <link href="css/animate.min.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">

        <link href="css/options.css" rel="stylesheet" media="screen">
        <link href="css/responsive.css" rel="stylesheet" media="screen">
        <!-- FONTS -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:900,300,400,200,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="./js/jquery-ui.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
            <!--<script type="text/javascript" src="Salient_files/prettyPhoto.js"></script>-->
        <script src="js/jquery-1.11.1.min.js"></script>
    </head>
