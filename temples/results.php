<?php
	//ob_start();
	include_once('./common/config.php');

	$months = array('All', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $baseUrlTemple = "https://www.wheresmypandit.com/temples/";
	//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
	if (!isset($_REQUEST['start_at']) || $_REQUEST['start_at'] <= 0) {
		$_REQUEST['start_at'] = 0;
	}

	if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] == 0) {
		$_REQUEST['loc_id'] = '';
	}

	switch($_REQUEST['loc_type']) {
		case 'state':
			$locations = array('0'=>array("type"=>"country","location"=>"","value"=>"All"));
			if (!isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
				$records_states = $dbh->query("SELECT `state` FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $_REQUEST['loc_id'] . "' ORDER BY `state`, `city` ASC");
				if ($records_states->num_rows > 0) {
					while($record_state = $records_states->fetch_object()) {
						$records_locations = $dbh->query("SELECT `id`, `city` FROM `" . $mysql_table_prefix . "locations` CONVERT(`state` USING utf8) LIKE _utf8 '%" . $record_state->state . "%' COLLATE utf8_general_ci LIMIT 10");
						if ($records_locations->num_rows > 0) {
							while($record_location = $records_locations->fetch_object()) {
								$locations[$record_location->id] =  array('type'=>'state', 'location'=>$record_location->id, 'value'=>ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country));
							}
						}
					}
				}
			}
			break;
		case 'country':
			$locations = array('0'=>array("type"=>"country","location"=>"","value"=>"All"));
			if (!isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
				$records_states = $dbh->query("SELECT `country` FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $_REQUEST['loc_id'] . "' ORDER BY `country`, `state`, `city` ASC");
				if ($records_states->num_rows > 0) {
					while($record_state = $records_states->fetch_object()) {
						$records_locations = $dbh->query("SELECT `id`, `city` FROM `" . $mysql_table_prefix . "locations` CONVERT(`country` USING utf8) LIKE _utf8 '%" . $record_state->country . "%' COLLATE utf8_general_ci LIMIT 10");
						if ($records_locations->num_rows > 0) {
							while($record_location = $records_locations->fetch_object()) {
								$locations[$record_location->id] =  array('type'=>'country', 'location'=>$record_location->id, 'value'=>ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country));
							}
						}
					}
				}
			}
			break;

		case '':
			$locations = array('0'=>array("type"=>"country","location"=>"","value"=>"All"));
			break;

		case 'city':
		default:
			$locations = array('0'=>array("type"=>"country","location"=>"","value"=>"All"));
			if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
				$records_locations = $dbh->query("SELECT `id`, `city`, `state`, `country` FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $_REQUEST['loc_id'] . "' LIMIT 1");
				if($record_location = $records_locations->fetch_object()) {
					$records_locations = $dbh->query("SELECT `id`, `city`, `state`, `country` FROM `" . $mysql_table_prefix . "locations` WHERE CONVERT(`state` USING utf8) LIKE _utf8 '%" . $record_location->state . "%' COLLATE utf8_general_ci LIMIT 10");
					if($record_location = $records_locations->fetch_object()) {
						$locations[$record_location->id] =  array('type'=>'city', 'location'=>$record_location->id, 'value'=>ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country));
					}
				}
			}
			break;
	}

	if ((!isset($_REQUEST['sts']) || $_REQUEST['sts'] == '') && (!isset($_REQUEST['temple_name']) || $_REQUEST['temple_name'] == '')) {
		$religions = array();
		$records_religions = $dbh->query("SELECT `id`, `name` FROM `" . $mysql_table_prefix . "religions` WHERE `status` = 'active' ORDER BY `id`");
		if ($records_religions->num_rows > 0) {
			while($record_religion = $records_religions->fetch_object()) {
				$religions[$record_religion->id] = $record_religion->name;
			}
		}
	
		$deities = array(''=>'All');
		$records_deities = $dbh->query("SELECT `id`, `name` FROM `" . $mysql_table_prefix . "deities` WHERE `status` = 'active' ORDER BY `id`");
		if ($records_deities->num_rows > 0) {
			while($record_deity = $records_deities->fetch_object()) {
				$deities[$record_deity->id] = $record_deity->name;
			}
		}
	} else {
		$temple_name = (isset($_REQUEST['temple_name']) && !empty($_REQUEST['temple_name']))?$_REQUEST['temple_name']:((isset($_REQUEST['sts']) && !empty($_REQUEST['sts']))?$_REQUEST['sts']:'');

		//echo "SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE CONVERT(`name` USING utf8) LIKE _utf8 '%" . $temple_name . "%' COLLATE utf8_general_ci LIMIT 1";
		
		$religions = array();
		$deities = array(''=>'All');

		$record_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE CONVERT(`name` USING utf8) LIKE _utf8 '%" . $temple_name . "%' COLLATE utf8_general_ci");
		if ($record_temples->num_rows > 0) {
			while ($record_temple = $record_temples->fetch_object()) {
				$records_religions = $dbh->query("SELECT `id`, `name` FROM `" . $mysql_table_prefix . "religions` WHERE `status` = 'active' AND `id` = '" . $record_temple->religion_id . "' ORDER BY `name` ASC");
				if ($records_religions->num_rows > 0) {
					while($record_religion = $records_religions->fetch_object()) {
						$religions[$record_religion->id] = $record_religion->name;
					}
				}

				$records_deities = $dbh->query("SELECT `id`, `name` FROM `" . $mysql_table_prefix . "deities` WHERE `status` = 'active' AND `id` = '" . $record_temple->deity_id . "' ORDER BY `name` ASC");
				if ($records_deities->num_rows > 0) {
					while($record_deity = $records_deities->fetch_object()) {
						$deities[$record_deity->id] = $record_deity->name;
					}
				}

				$record_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $record_temple->location_id . "' AND `status` = 'active'");
				if ($record_locations->num_rows > 0) {
					while($record_location = $record_locations->fetch_object()) {
						$locations[$record_location->id] =  array('type'=>'city', 'location'=>$record_location->id, 'value'=>ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country));
					}
				}
			}
		}
	}

	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}

	//echo 'INSERT INTO `" . $mysql_table_prefix . "searches` (`id`, `user_id`, `short_temple_search`, `long_temple_search`, `short_location_search`, `long_location_search`, `religion_id`, `deity_id`, `ip`, `status`, `created_at`, `updated_at`) VALUES("", "' . $userId . '", "' . $_REQUEST['sts']. '", "' . $_REQUEST['temple_name']. '", "' . $_REQUST['sls'] . '", "' . $_REQUST['temple_location'] . '", "0", "0", "' . $ip . '", "active", now(), now())'; exit();

	if (isset($_REQUEST['sid']) && $_REQUEST['sid'] > 0) {
		$updates = array();

		if (isset($_REQUEST['sr']) && $_REQUEST['sr'] != '') {
			$updates['religion_id'] = $_REQUEST['sr'];
		}
		if (isset($_REQUEST['sd']) && $_REQUEST['sd'] != '') {
			$updates['deity_id'] = $_REQUEST['sd'];
		}
		if (isset($_REQUEST['sm']) && $_REQUEST['sm'] != '') {
			$updates['month'] = $months[$_REQUEST['sm']];
		}
		if (isset($_REQUEST['su']) && $_REQUEST['su'] != '') {
			$updates['unesco_searched'] = $_REQUEST['su'];
		} else {
			$updates['unesco_searched'] = 'no';
		}
		if (isset($_REQUEST['sld']) && $_REQUEST['sld'] != '') {
			$updates['live_darshan_searched'] = $_REQUEST['sld'];
		} else {
			$updates['live_darshan_searched'] = 'no';
		}
		if (isset($_REQUEST['sop']) && $_REQUEST['sop'] != '') {
			$updates['online_prasad_searched'] = $_REQUEST['sop'];
		} else {
			$updates['online_prasad_searched'] = 'no';
		}
		if (isset($_REQUEST['spt']) && $_REQUEST['spt'] != '') {
			$updates['pooja_in_temple_searched'] = $_REQUEST['spt'];
		} else {
			$updates['pooja_in_temple_searched'] = 'no';
		}
		if (isset($_REQUEST['sbh']) && $_REQUEST['sbh'] != '') {
			$updates['book_hotels_searched'] = $_REQUEST['sbh'];
		} else {
			$updates['book_hotels_searched'] = 'no';
		}
		if (isset($_REQUEST['sl']) && $_REQUEST['sl'] != '') {
			$records_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $_REQUEST['sl'] . "' LIMIT 1");
			$record_location = $records_locations->fetch_object();

			$updates['long_location_search'] = $record_location->city;
			$updates['short_location_search'] = $record_location->city;
		}
		if (isset($_REQUEST['sb']) && $_REQUEST['sb'] != '') {
			$updates['sort_by'] = $_REQUEST['sb'];
		} else {
			$updates['sort_by'] = 'recent';
			$_REQUEST['sb'] = 'recent';
		}

		if (count($updates) > 0) {
			$update_string = '';
			foreach ($updates as $key=>$update) {
				$update_string .= ('`' . $key . '` = "' . $update . '"');
			}

			$dbh->query('UPDATE `' . $mysql_table_prefix . 'searches` SET ' . $update_string . ' WHERE `id` = "' . $_REQUEST['sid'] . '"');
		}

		$search_id = $_REQUEST['sid'];
		$_REQUEST['sid'] = $search_id;
	} else {
		$_REQUEST['sb'] = 'recent';

		$dbh->query('INSERT INTO `' . $mysql_table_prefix . 'searches` (`id`, `user_id`, `short_temple_search`, `long_temple_search`, `short_location_search`, `long_location_search`, `religion_id`, `deity_id`, `sort_by`, `ip`, `status`, `created_at`, `updated_at`) VALUES("", "' . $userId . '", "' . $_REQUEST['sts'] . '", "' . $_REQUEST['temple_name'] . '", "' . $_REQUST['sls'] . '", "' . $_REQUST['temple_location'] . '", "0", "0", "recent", "' . $ip . '", "active", now(), now())');

		$search_id = $dbh->insert_id;
		$_REQUEST['sid'] = $search_id;
	}

	// Relevance : 1
	$where = array("`status` = 'active'");
	
	if (isset($_REQUEST['st']) && $_REQUEST['st'] > 0) {
		$where[] = "`id` = '" . $_REQUEST['st'] . "'";
	}
	if (isset($_REQUEST['sr']) && $_REQUEST['sr'] > 0) {
		$where[] = "`religion_id` = '" . $_REQUEST['sr'] . "'";
	}
	if (isset($_REQUEST['sd']) && $_REQUEST['sd'] > 0) {
		$where[] = "`deity_id` = '" . $_REQUEST['sd'] . "'";
	}
	if (isset($_REQUEST['sm']) && $_REQUEST['sm'] != '') {
		$where[] = "(`months` LIKE '" . $_REQUEST['sm'] . "'  OR `months` LIKE '" . $_REQUEST['sm'] . ",%' OR `months` LIKE '%," . $_REQUEST['sm'] . ",%' OR `months` LIKE '%," . $_REQUEST['sm'] . "')";
	}
	if (isset($_REQUEST['su']) && $_REQUEST['su'] != '') {
		$where[] = "`unesco_listed` = '" . $_REQUEST['su'] . "'";
	} else {
		//$where[] = "`unesco_listed` = 'yes'";
	}
	if (isset($_REQUEST['sld']) && $_REQUEST['sld'] != '') {
		$where[] = "`live_darshan_link` != ''";
	} else {
		//$where[] = "`live_darshan_link` != ''";
	}
	if (isset($_REQUEST['sop']) && $_REQUEST['sop'] != '') {
		$where[] = "`online_prasad_link` != ''";
	} else {
		//$where[] = "`online_prasad_link` = ''";
	}
	if (isset($_REQUEST['spt']) && $_REQUEST['spt'] != '') {
		$where[] = "`pooja_in_temple_link` != ''";
	} else {
		//$where[] = "`pooja_in_temple_link` = ''";
	}
	if (isset($_REQUEST['sbh']) && $_REQUEST['sbh'] != '') {
		$where[] = "`book_hotels_link` != ''";
	} else {
		//$where[] = "`book_hotels_link` = ''";
	}

	if (isset($_REQUEST['sl']) && $_REQUEST['sl'] > 0) {
		$where[] = "`location_id` = '" . $_REQUEST['sl'] . "'";
	} else if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
		$where[] = "`location_id` = '" . $_REQUEST['loc_id'] . "'";
	} else if (isset($_REQUEST['temple_location']) && $_REQUEST['temple_location']) {
		if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
			$temp_array = array();
			$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE `id` = "' . $_REQUEST['loc_id'] . '"');
			if ($record_locations->num_rows > 0) {
				while ($record_location = $records_locations->fetch_object()) {
					$temp_array[] = $record_location->id;
				}
			}
			if (count($temp_array) > 0) {
				$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
			}
		} else {
			$temp_array = array();
			switch ($_REQUEST['loc_type']) {
				case 'city':
					$exploded = explode(', ', $_REQUEST['temple_location']);
					$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`city` USING utf8) LIKE _utf8 "%' . $exploded[0] . '%" COLLATE utf8_general_ci) LIMIT 10');
					break;
				case 'state':
					$exploded = explode(', ', $_REQUEST['temple_location']);
					$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`state` USING utf8) LIKE _utf8 "%' . $exploded[1] . '%" COLLATE utf8_general_ci) LIMIT 10');
					break;
				case 'country':
					$exploded = explode(', ', $_REQUEST['temple_location']);
					$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`country` USING utf8) LIKE _utf8 "%' . $exploded[2] . '%" COLLATE utf8_general_ci) LIMIT 10');
					break;
				default:
					//echo 'SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`city` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci) OR (CONVERT(`state` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci) OR (CONVERT(`country` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci)<br/>';
					$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`city` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci) OR (CONVERT(`state` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci) OR (CONVERT(`country` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci)');
					break;
			}
			if ($records_locations->num_rows > 0) {
				while ($record_location = $records_locations->fetch_object()) {
					$temp_array[] = $record_location->id;
					$locations[$record_location->id] =  array('type'=>'city', 'location'=>$record_location->id, 'value'=>ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country));
					//$locations[$record_location->id] = ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
				}
			}
			//echo "<pre>"; print_r($temp_array); echo "</pre>";
			if (count($temp_array) > 0) {
				$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
			}
		}
	}
	if (isset($_REQUEST['temple_name']) && !empty($_REQUEST['temple_name']) && (!isset($_REQUEST['st']) || $_REQUEST['st'] == '')) {
		$where[] = 'CONVERT(`name` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_name'] . '%" COLLATE utf8_general_ci';
	}
	if (count($where) == 1 && preg_match('/`status` = /i', $where[0])) {
		$temple_location = 'Maharashtra';
		$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`state` USING utf8) LIKE _utf8 "Maharashtra" COLLATE utf8_general_ci)');
		if ($records_locations->num_rows > 0) {
			while ($record_location = $records_locations->fetch_object()) {
				$temp_array[] = $record_location->id;
				$locations[$record_location->id] =  array('type'=>'city', 'location'=>$record_location->id, 'value'=>ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country));
				//$locations[$record_location->id] = ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
			}
		}
		//echo "<pre>"; print_r($temp_array); echo "</pre>";
		if (count($temp_array) > 0) {
			$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
		}
	}
	//echo "<pre>"; print_r($_REQUEST); echo "</pre>";
	$temples = array();
	$query1 = "SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE " . implode(' AND ', $where) . " ORDER BY `id` DESC";
	//echo "<pre>q1 : " . $query1 . "</pre>";
	$records_temples = $dbh->query($query1);
	if ($records_temples->num_rows > 0) {
		while($record_temple = $records_temples->fetch_object()) {
			$services = array();
			/*$records_services = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "links` WHERE `temple_id` = '" . $record_temple->id . "'");
			if ($records_services->num_rows > 0) {
				while($record_service = $records_services->fetch_object()) {
					$services[] = array(
						'text'=>ucwords($record_service->text),
						'link'=>$record_service->link,
						'picture'=>$record_service->picture,
					);
				}
			}*/

			$uploads = array();
			$records_uploads = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "uploads` WHERE `temple_id` = '" . $record_temple->id . "' AND `is_primary` = 'yes' LIMIT 1");
			if($record_upload = $records_uploads->fetch_object()) {
				$temples[$record_temple->id]['primary_image'] = $record_upload->name;
			}

			$record_deities = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "deities` WHERE `id` = '" . $record_temple->deity_id . "' LIMIT 1");
			$record_deity = $record_deities->fetch_object();

			$record_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $record_temple->location_id . "' LIMIT 1");
			$record_location = $record_locations->fetch_object();

			$temples[$record_temple->id]['id'] = $record_temple->id;
			$temples[$record_temple->id]['name'] = $record_temple->name;
			$temples[$record_temple->id]['alt_msg'] = $record_temple->alt_msg;
			$temples[$record_temple->id]['urlkey'] = $baseUrlTemple.$record_temple->urlkey;
			$temples[$record_temple->id]['deity'] = ucwords($record_deity->name);
			$temples[$record_temple->id]['deity_id'] = $record_temple->deity_id;
			$temples[$record_temple->id]['location'] = ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
			$temples[$record_temple->id]['location_id'] = $record_temple->location_id;
			//$temples[$record_temple->id]['services'] = $services;
			$temples[$record_temple->id]['short_description'] = implode(' ', array_slice(explode(' ', $record_temple->description), 0, 30));
			$temples[$record_temple->id]['relevance'] = 1;
			$temples[$record_temple->id]['updated'] = $record_temple->updated;
			$temples[$record_temple->id]['popularity'] = $record_temple->visits;
			$temples[$record_temple->id]['live_darshan_link'] = $record_temple->live_darshan_link;
			$temples[$record_temple->id]['pooja_in_temple_link'] = $record_temple->pooja_in_temple_link;
			$temples[$record_temple->id]['online_prasad_link'] = $record_temple->online_prasad_link;
			$temples[$record_temple->id]['book_hotels_link'] = $record_temple->book_hotels_link;

			$dbh->query("UPDATE `" . $mysql_table_prefix . "temples` SET `searches` = '" . ($record_temple->searches+1) . "' WHERE `id` = '" . $record_temple->id . "'");
		}
	}

	// Relevance : 2
	if (!isset($_REQUEST['st']) || $_REQUEST['st'] == 0) {
		$where = array("`status` = 'active'");
		
		if (isset($_REQUEST['sr']) && $_REQUEST['sr'] > 0) {
			$where[] = "`religion_id` = '" . $_REQUEST['sr'] . "'";
		}
		if (isset($_REQUEST['sd']) && $_REQUEST['sd'] > 0) {
			$where[] = "`deity_id` = '" . $_REQUEST['sd'] . "'";
		}
		if (isset($_REQUEST['sm']) && $_REQUEST['sm'] != '') {
			$where[] = "(`months` LIKE '" . $_REQUEST['sm'] . "'  OR `months` LIKE '" . $_REQUEST['sm'] . ",%' OR `months` LIKE '%," . $_REQUEST['sm'] . ",%' OR `months` LIKE '%," . $_REQUEST['sm'] . "')";
		}
		if (isset($_REQUEST['su']) && $_REQUEST['su'] != '') {
			$where[] = "`unesco_listed` = '" . $_REQUEST['su'] . "'";
		} else {
			//$where[] = "`unesco_listed` = 'yes'";
		}
		if (isset($_REQUEST['sld']) && $_REQUEST['sld'] != '') {
			$where[] = "`live_darshan_link` != ''";
		} else {
			//$where[] = "`live_darshan_link` = ''";
		}
		if (isset($_REQUEST['sop']) && $_REQUEST['sop'] != '') {
			$where[] = "`online_prasad_link` != ''";
		} else {
			//$where[] = "`online_prasad_link` = ''";
		}
		if (isset($_REQUEST['spt']) && $_REQUEST['spt'] != '') {
			$where[] = "`pooja_in_temple_link` != ''";
		} else {
			//$where[] = "`pooja_in_temple_link` = ''";
		}
		if (isset($_REQUEST['sbh']) && $_REQUEST['sbh'] != '') {
			$where[] = "`book_hotels_link` != ''";
		} else {
			//$where[] = "`book_hotels_link` = ''";
		}
		if (isset($_REQUEST['sl']) && $_REQUEST['sl'] > 0) {
			$where[] = "`location_id` = '" . $_REQUEST['sl'] . "'";
		} else if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
			$where[] = "`location_id` = '" . $_REQUEST['loc_id'] . "'";
		} else if (isset($_REQUEST['temple_location']) && $_REQUEST['temple_location']) {
			if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
				$temp_array = array();
				$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE `id` = "' . $_REQUEST['loc_id'] . '"');
				if ($records_locations->num_rows > 0) {
					while ($record_location = $records_locations->fetch_object()) {
						$temp_array[] = $record_location->id;
					}
				}
				if (count($temp_array) > 0) {
					$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
				}
			} else {
				$temp_array = array();
				switch ($_REQUEST['loc_type']) {
					case 'city':
						$exploded = explode(', ', $_REQUEST['temple_location']);
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`city` USING utf8) LIKE _utf8 "%' . $exploded[0] . '%" COLLATE utf8_general_ci) LIMIT 10');
						break;
					case 'state':
						$exploded = explode(', ', $_REQUEST['temple_location']);
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`state` USING utf8) LIKE _utf8 "%' . $exploded[1] . '%" COLLATE utf8_general_ci) LIMIT 10');
						break;
					case 'country':
						$exploded = explode(', ', $_REQUEST['temple_location']);
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`country` USING utf8) LIKE _utf8 "%' . $exploded[2] . '%" COLLATE utf8_general_ci) LIMIT 10');
						break;
					default:
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`city` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci) OR (CONVERT(`state` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci) OR (CONVERT(`country` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_location'] . '%" COLLATE utf8_general_ci)');
						break;
				}
				if ($records_locations->num_rows > 0) {
					while ($record_location = $records_locations->fetch_object()) {
						$temp_array[] = $record_location->id;
					}
				}
				if (count($temp_array) > 0) {
					$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
				}
			}
		}
		if (isset($_REQUEST['temple_name']) && !empty($_REQUEST['temple_name'])) {
			$where[] = 'CONVERT(`tags` USING utf8) LIKE _utf8 "%' . $_REQUEST['temple_name'] . '%" COLLATE utf8_general_ci';
		}
		//echo "SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE " . implode(' AND ', $where) . " ORDER BY `updated` DESC";
		$query2 = "SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE " . implode(' AND ', $where) . " ORDER BY `id` DESC";
		//echo "<pre>q2 : " . $query2 . "</pre>";
		$records_temples = $dbh->query($query2);
		if ($records_temples->num_rows > 0) {
			while($record_temple = $records_temples->fetch_object()) {
				if (isset($temples[$record_temple->id]) && !empty($temples[$record_temple->id])) {
					continue;
				}
				/*$services = array();
				$records_services = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "links` WHERE `temple_id` = '" . $record_temple->id . "'");
				if ($records_services->num_rows > 0) {
					while($record_service = $records_services->fetch_object()) {
						$services[] = array(
							'text'=>ucwords($record_service->text),
							'link'=>$record_service->link,
							'picture'=>$record_service->picture,
						);
					}
				}*/

				$uploads = array();
				$records_uploads = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "uploads` WHERE `temple_id` = '" . $record_temple->id . "' AND `is_primary` = 'yes' LIMIT 1");
				if($record_upload = $records_uploads->fetch_object()) {
					$temples[$record_temple->id]['primary_image'] = $record_upload->name;
				}

				$record_deities = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "deities` WHERE `id` = '" . $record_temple->deity_id . "' LIMIT 1");
				$record_deity = $record_deities->fetch_object();

				$record_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $record_temple->location_id . "' LIMIT 1");
				$record_location = $record_locations->fetch_object();

				$temples[$record_temple->id]['name'] = $record_temple->name;
				$temples[$record_temple->id]['urlkey'] = $baseUrlTemple.$record_temple->urlkey;
				$temples[$record_temple->id]['deity'] = ucwords($record_deity->name);
				$temples[$record_temple->id]['deity_id'] = $record_temple->deity_id;
				$temples[$record_temple->id]['location'] =  ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
				$temples[$record_temple->id]['location_id'] = $record_temple->location_id;
				//$temples[$record_temple->id]['services'] = $services;
				$temples[$record_temple->id]['short_description'] = implode(' ', array_slice(explode(' ', $record_temple->description), 0, 40));
				$temples[$record_temple->id]['relevance'] = 2;
				$temples[$record_temple->id]['updated'] = $record_temple->updated;
				$temples[$record_temple->id]['popularity'] = $record_temple->visits;
				$temples[$record_temple->id]['live_darshan_link'] = $record_temple->live_darshan_link;
				$temples[$record_temple->id]['pooja_in_temple_link'] = $record_temple->pooja_in_temple_link;
				$temples[$record_temple->id]['online_prasad_link'] = $record_temple->online_prasad_link;
				$temples[$record_temple->id]['book_hotels_link'] = $record_temple->book_hotels_link;

				$dbh->query("UPDATE `" . $mysql_table_prefix . "searches` SET `searches` = '" . ($record_temple->searches+1) . "' WHERE `id` = '" . $record_temple->id . "'");
			}
		}
	}

	// Relevance : 3
	/*
	if (!isset($_REQUEST['st']) || $_REQUEST['st'] == 0) {
		$where = array("`status` = 'active'");
		
		if (isset($_REQUEST['sr']) && $_REQUEST['sr'] > 0) {
			$where[] = "`religion_id` = '" . $_REQUEST['sr'] . "'";
		}
		if (isset($_REQUEST['sd']) && $_REQUEST['sd'] > 0) {
			$where[] = "`deity_id` = '" . $_REQUEST['sd'] . "'";
		}
		if (isset($_REQUEST['sl']) && $_REQUEST['sl'] > 0) {
			$where[] = "`location_id` = '" . $_REQUEST['sl'] . "'";
		} else if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
			$where[] = "`location_id` = '" . $_REQUEST['loc_id'] . "'";
		} else if (isset($_REQUEST['temple_location']) && !empty($_REQUEST['temple_location'])) {
			if (isset($_REQUEST['loc_id']) && $_REQUEST['loc_id'] > 0) {
				$temp_array = array();
				$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE `id` = "' . $_REQUEST['loc_id'] . '"');
				if ($records_locations->num_rows > 0) {
					while ($record_location = $records_locations->fetch_object()) {
						$temp_array[] = $record_location->id;
					}
				}
				if (count($temp_array) > 0) {
					$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
				}
			} else {
				$temp_array = array();
				switch ($_REQUEST['loc_type']) {
					case 'city':
						$exploded = explode(', ', $_REQUEST['temple_location']);
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`city` USING utf8) LIKE _utf8 "%' . $exploded[0] . '%" COLLATE utf8_general_ci) LIMIT 10');
						break;
					case 'state':
						$exploded = explode(', ', $_REQUEST['temple_location']);
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`state` USING utf8) LIKE _utf8 "%' . $exploded[1] . '%" COLLATE utf8_general_ci) LIMIT 10');
						break;
					case 'country':
						$exploded = explode(', ', $_REQUEST['temple_location']);
						$records_locations = $dbh->query('SELECT * FROM `' . $mysql_table_prefix . 'locations` WHERE (CONVERT(`country` USING utf8) LIKE _utf8 "%' . $exploded[2] . '%" COLLATE utf8_general_ci) LIMIT 10');
						break;
				}
				if ($records_locations->num_rows > 0) {
					while ($record_location = $records_locations->fetch_object()) {
						$temp_array[] = $record_location->id;
					}
				}
				if (count($temp_array) > 0) {
					$where[] = "`location_id` IN (" . implode(',', $temp_array) . ")";
				}
			}
		}
		
		$records_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE " . implode(' AND ', $where) . " ORDER BY `id` DESC");
		if ($records_temples->num_rows > 0) {
			while($record_temple = $records_temples->fetch_object()) {
				#$services = array();
				#$records_services = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "links` WHERE `temple_id` = '" . $record_temple->id . "'");
				#if ($records_services->num_rows > 0) {
					#while($record_service = $records_services->fetch_object()) {
					#	$services[] = array(
					#		'text'=>ucwords($record_service->text),
					#		'link'=>$record_service->link,
					#		'picture'=>$record_service->picture,
					#	);
					#}
				#}

				$uploads = array();
				$records_uploads = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "uploads` WHERE `temple_id` = '" . $record_temple->id . "' AND `is_primary` = 'yes' LIMIT 1");
				if($record_upload = $records_uploads->fetch_object()) {
					$temples[$record_temple->id]['primary_image'] = $record_upload->name;
				}

				$record_deities = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "deities` WHERE `id` = '" . $record_temple->deity_id . "' LIMIT 1");
				$record_deity = $record_deities->fetch_object();

				$record_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $record_temple->location_id . "' LIMIT 1");
				$record_location = $record_locations->fetch_object();

				
				$temples[$record_temple->id]['name'] = $record_temple->name;
				$temples[$record_temple->id]['deity'] = ucwords($record_deity->name);
				$temples[$record_temple->id]['deity_id'] = $record_temple->deity_id;
				$temples[$record_temple->id]['location'] =  ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
				$temples[$record_temple->id]['location_id'] = $record_temple->location_id;
				//$temples[$record_temple->id]['services'] = $services;
				$temples[$record_temple->id]['short_description'] = implode(' ', array_slice(explode(' ', $record_temple->description), 0, 40));
				$temples[$record_temple->id]['relevance'] = 3;
				$temples[$record_temple->id]['updated'] = $record_temple->updated;
				$temples[$record_temple->id]['popularity'] = $record_temple->visits;
				$temples[$record_temple->id]['live_darshan_link'] = $record_temple->live_darshan_link;
				$temples[$record_temple->id]['pooja_in_temple_link'] = $record_temple->pooja_in_temple_link;
				$temples[$record_temple->id]['online_prasad_link'] = $record_temple->online_prasad_link;
				$temples[$record_temple->id]['book_hotels_link'] = $record_temple->book_hotels_link;

				$dbh->query("UPDATE `" . $mysql_table_prefix . "searches` SET `searches` = '" . ($record_temple->searches+1) . "' WHERE `id` = '" . $record_temple->id . "'");
			}
		}
	}*/

	include_once('./common/inner_head.php');
	//include_once('./common/magento_head.php');
?>
	<!-- START BODY -->
	<body>
		<div id="page">
			<!-- START HEADER -->
			<header id="header" class="small  with-separation-bottom">
				<!-- POINTER ANIMATED -->
				<canvas id="header-canvas"></canvas>
	  		
				<!-- TOP NAVIGATION 
				<div id="top-navigation">
		  			<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
			  			<div class="summary">
							<h2 class="classy f-left">
								<span>Shopping Cart -</span>
                        		<a href="javascript:;">0</a><span class="Itext"> item</span>
							</h2>
						</div>
					</ul>
				</div>
	  	
				<!-- MOBILE NAVIGATION -->
				

				<!-- MAIN MENU -->
				<nav id="navigation">
				
				
					<!-- CLOSE MOBILE MENU -->
					<a href="javascript:;" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
					<div  class="animate-me flipInX" data-wow-duration="3s">
						<a href="/" id="logo-navigation"></a>
					</div>
				</nav>
				<?php
					$show_heading = false;
					include_once('common/search.php');
				?>
	  		
				<!-- SHADOW -->
				<div id="shade2"></div>

				<!-- HEADER SLIDER -->
				<div class="flexslider" id="header-slider">
					<ul class="slides">
						<li><img src="images/bg1.jpg" alt="SLider Image"></li>
						<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
						<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
					</ul>	
				</div>
				<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
				<!--<div id="header-video"
					data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
				</div>-->	  		
			</header>
	  		<!-- END HEADER -->
	  	
			<!-- START MAIN CONTAINER -->
			<div class="main-container">
				<div class="container">
					<!--BLOG -->
					<h1 class="with-breaker animate-me fadeInUp">
						Temple Search <span>Filter Your Search</span>
					</h1>
				 
					<div class="filter-sort"> 
						<nav id="navigation" class="" style="display: block;">
							<ul id="left-navigation" class="animate-me fadeInLeftBig animated" style="visibility: visible; -webkit-animation: fadeInLeftBig;text-align:center;">
								<!--<li class="menu-item menu-item-has-children">
									<input id="sort_by" name="sort_by" type="text" placeholder="Sort By" value="<?php echo ucwords($_REQUEST['sb']); ?>">
									<ul class="sub-menu" id="ul_sort_by">
										<li class="menu-item"><a class="sort_by" id="popularity" name="popularity" href="javascript:;">Popularity</a></li>
										<li class="menu-item"><a class="sort_by" id="relevance" name="relevance" href="javascript:;">Relevance</a></li>
										<li class="menu-item"><a class="sort_by" id="recent" name="recent" href="javascript:;">Recent</a></li>
									</ul>
								</li>-->
								<?php
									if (count($locations) > 1) {
								?>
								<li class="menu-item menu-item-has-children">
									<input id="s_location" name="s_location" type="text" placeholder="Enter Location" value="<?php if ($_REQUEST['sl'] != '') { echo ucwords($locations[$_REQUEST['sl']]['value']); } ?>">
									<script language="javascript">
										jQuery(document).ready(function ($) {
											$( "#s_location" ).autocomplete({
												source: [<?php foreach($locations as $lid=>$lcn) { echo '{"type":"' . $lid['type'] . '","location":"' . $lcn['location'] . '","value":"' . $lcn['value'] . '"},'; } ?>],
												dataType: 'jsonp',
												minLength: 2,
												html: true,
												open: function() {
													$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
												},
												close: function() {
													$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
												},
												select: function(event, ui) {
													$("#start_at").val('0');
													$("#sl").val(ui.item.value);
													$("#loc_type").val(ui.item.type);
													$("#loc_id").val(ui.item.location);
													$.ajax({
														type: "POST",
														url: 'results',
														data: $("#temple_search").serialize(),
														success: function(html) {
															$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
															$(".blog-grid:eq(1)").html('');
														},
													});
												},
											});
										});
									</script>
								</li>
								<?php
									} else {
								?>
									<li class="menu-item menu-item-has-children">
										<input id="s_location" name="s_location" type="text" placeholder="Enter Location" value="">
									</li>
									<script language="javascript">
										jQuery(document).ready(function ($) {
											$( "#s_location" ).autocomplete({
												source: 'search?action=get_locations&all=1',
												minLength: 2,
												html: true,
												open: function() {
													//$("#l_search_by").val('short');
													$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
												},
												close: function() {
													$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
												},
												select: function(event, ui) {
													$("#start_at").val('0');
													$("#sl").val(ui.item.value);
													$("#loc_type").val(ui.item.type);
													$("#loc_id").val(ui.item.location);
													$.ajax({
														type: "POST",
														url: 'results',
														data: $("#temple_search").serialize(),
														success: function(html) {
															$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
															$(".blog-grid:eq(1)").html('');
														},
													});
												},
											});
										});
									</script>
								<?php
									}
								?>
								<?php
									if (count($religions) > 0) {
								?>
								<!--<li class="menu-item menu-item-has-children">
									<input id="s_religion" name="s_religion" type="text" placeholder="Religion" value="<?php echo ucwords($religions[$_REQUEST['sr']]); ?>" >
									<script language="javascript">
										jQuery(document).ready(function ($) {
											$( "#s_religion" ).autocomplete({
												source: <?php echo json_encode($religions); ?>,
												minLength: 3,
												html: true,
												open: function() {
													$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
												},
												close: function() {
													$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
												},
												select: function(event, ui) {
													$("#start_at").val('0');
													$("#sr").val(ui.item.value);
													$.ajax({
														type: "POST",
														url: 'results',
														data: $("#temple_search").serialize(),
														success: function(html) {
															$(".blog-grid").html($(html).find('.blog-grid').html());
														},
													});
													$("#start_at").val('0');
												},
											});
										});
									</script>
								</li>-->
								<?php
									} else {
								?>
									<!--<li class="menu-item menu-item-has-children">
										<input id="s_religion" name="s_religion" type="text" placeholder="Enter Religion" value="<?php echo $religion; ?>">
									</li>-->
								<?php
									}
								?>
								<li class="menu-item menu-item-has-children">
									<input id="s_deity" name="s_deity" type="text" placeholder="Deity" value="<?php echo ucwords($deities[$_REQUEST['sd']]); ?>" >
									<ul class="sub-menu" id="ul_deity">
										<?php
											foreach ($deities as $dkey=>$deity) {
										?>
											<li class="menu-item">
												<a class="deity" href="javascript:;" id="dei_<?php echo $dkey; ?>">
													<?php
														echo ucwords($deity);
													?>
												</a>
											</li>
										<?php
											}
										?>
									</ul>
								</li>
								<li class="menu-item menu-item-has-children">
									<input id="s_months" name="s_months" type="text" placeholder="Best Months" value="<?php echo $_REQUEST['sm']; ?>" >
									<ul class="sub-menu" id="ul_months">
										<?php
											foreach ($months as $month) {
										?>
											<li class="menu-item">
												<a class="months" href="javascript:;" id="mo_<?php echo $month; ?>">
													<?php
														echo ucwords($month);
													?>
												</a>
											</li>
										<?php
											}
										?>
									</ul>
								</li>
								<!--<li class="menu-item menu-item-has-children" style="border-bottom:none;text-align:center;text-shadow:none;width:100%;">-->
								<li class="menu-item menu-item-has-children">
									<input id="s_unesco" name="s_unesco" type="checkbox" <?php if ($_REQUEST['su'] == 'yes') {?>checked<?php } ?> style="width:20px"> <label for="s_unesco" style="color:#2f2f2f;font-weight:normal;">UNESCO Listed</label> 
									<!--<input id="s_live_darshan" name="s_live_darshan" type="checkbox" <?php if ($_REQUEST['sld'] == 'yes') {?>checked<?php } ?> style="margin-left:20px;width:20px"> <label for="s_live_darshan" style="color:#2f2f2f;font-weight:normal;">Live Darshan</label> 
									<input id="s_online_prasad" name="s_online_prasad" type="checkbox" <?php if ($_REQUEST['sop'] == 'yes') {?>checked<?php } ?> style="margin-left:20px;width:20px"> <label for="s_online_prasad" style="color:#2f2f2f;font-weight:normal;">Online Prasad</label> 
									<input id="s_pooja_in_temple" name="s_pooja_in_temple" type="checkbox" <?php if ($_REQUEST['spt'] == 'yes') {?>checked<?php } ?> style="margin-left:20px;width:20px">  <label for="s_pooja_in_temple" style="color:#2f2f2f;font-weight:normal;">Pooja In Temple</label> 
									<input id="s_book_hotels" name="s_book_hotels" type="checkbox" <?php if ($_REQUEST['sbh'] == 'yes') {?>checked<?php } ?> style="margin-left:20px;width:20px">  <label for="s_book_hotels" style="color:#2f2f2f;font-weight:normal;">Hotels</label>-->
								</li>
							</ul>
							<div class="clear"></div>
							<script language="javascript">
								jQuery(document).ready(function ($) {
									$(".sort_by").click(function(event) {
										event.preventDefault();
										$("#sort_by").val($(this).html().trim());
										$("#ul_sort_by").removeClass('open');
										//$("#sb").val($(this).attr('id'));
										$("#start_at").val('0');
										$("#is_ajax").val('1');
										$.ajax({
											type: "POST",
											url: 'results',
											data: $("#temple_search").serialize(),
											success: function(html) {
												$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
												$(".blog-grid:eq(1)").html('');
												$("#is_ajax").val('0');
											},
										});
									});
									/*$(".location").click(function(event) {
										event.preventDefault();
										
										var lid = $(this).attr('id').split('_')[1];
										$("#sl").val(lid);
										$("#start_at").val('0');
										$("#is_ajax").val('1');
										$("#temple_search").submit();
									});
									$(".religion").click(function(event) {
										event.preventDefault();
										
										var rid = $(this).attr('id').split('_')[1];
										$("#sr").val(rid);
										$("#start_at").val('0');
										$("#is_ajax").val('1');
										$("#temple_search").submit();
									});*/
									$(".deity").click(function(event) {
										event.preventDefault();
										
										var did = $(this).attr('id').split('_')[1];
										$("#s_deity").val($(this).html().trim());
										$("#ul_deity").removeClass('open');
										$("#sd").val(did);
										$("#start_at").val('0');
										$("#is_ajax").val('1');
										$.ajax({
											type: "POST",
											url: 'results',
											data: $("#temple_search").serialize(),
											success: function(html) {
												$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
												$(".blog-grid:eq(1)").html('');
												$("#is_ajax").val('0');
											},
										});
									});
									$(".months").click(function(event) {
										event.preventDefault();
										
										var mid = $(this).attr('id').split('_')[1];
										$("#s_months").val(mid.charAt(0).toUpperCase() + mid.substr(1));
										$("#ul_months").removeClass('open');
										$("#sm").val(mid);
										$("#start_at").val('0');
										$("#is_ajax").val('1');
										$.ajax({
											type: "POST",
											url: 'results',
											data: $("#temple_search").serialize(),
											success: function(html) {
												$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
												$(".blog-grid:eq(1)").html('');
												$("#is_ajax").val('0');
											},
										});
									});
									$("#s_unesco").change(function(event) {
										event.preventDefault();
										
										if ($(this).is(':checked')) {
											$("#su").val('yes');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										} else {
											$("#su").val('');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										}
									});
									$("#s_live_darshan").change(function(event) {
										event.preventDefault();
										
										if ($(this).is(':checked')) {
											$("#sld").val('yes');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										} else {
											$("#sld").val('');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										}
									});
									$("#s_online_prasad").change(function(event) {
										event.preventDefault();
										
										if ($(this).is(':checked')) {
											$("#sop").val('yes');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										} else {
											$("#sop").val('');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										}
									});
									$("#s_pooja_in_temple").change(function(event) {
										event.preventDefault();
										
										if ($(this).is(':checked')) {
											$("#spt").val('yes');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										} else {
											$("#spt").val('');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										}
									});
									$("#s_book_hotels").change(function(event) {
										event.preventDefault();
										
										if ($(this).is(':checked')) {
											$("#sbh").val('yes');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										} else {
											$("#sbh").val('');
											$("#start_at").val('0');
											$("#is_ajax").val('1');
											$.ajax({
												type: "POST",
												url: 'results',
												data: $("#temple_search").serialize(),
												success: function(html) {
													$(".blog-grid:eq(0)").html($(html).find('.blog-grid:eq(0)').html());
													$(".blog-grid:eq(1)").html('');
													$("#is_ajax").val('0');
												},
											});
										}
									});
								});
							</script>
						</nav>
					</div>
					<br/>	
					<ul class="blog-grid">
						<!--<li style="display:none;">
							<?php
								//echo $query1 . "<br/>";
								//echo $query2 . "<br/>";
							?>
						</li>-->
						<?php
							function sortByPopularity($a,$b){
								if($a['popularity'] == $b['popularity']){
									return 0;
								}
								return ($a['popularity'] > $b['popularity']) ? -1 : 1;
							}

							function sortByRelevance($a,$b){
								if($a['relevance'] == $b['relevance']){
									return 0;
								}
								return ($a['relevance'] > $b['relevance']) ? -1 : 1;
							}

							function sortByRecent($a,$b){
								if($a['id'] == $b['id']){
									return 0;
								}
								return ($a['id'] > $b['id']) ? -1 : 1;
							}

							if (isset($_REQUEST['sb']) && $_REQUEST['sb'] != '') {
								switch($_REQUEST['sb']) {
									case 'popularity':
										//echo "<pre>"; print_r($temples); echo "</pre>";
										usort($temples, 'sortByPopularity');
										//echo "<pre>"; print_r($temples); echo "</pre>";
										break;
									case 'relevance':
										usort($temples, 'sortByRelevance');
										break;
									case 'recent':
									default:
										usort($temples, 'sortByRecent');
										break;
								}	
							} else {
								usort($temples, 'sortByRecent');
							}

							if (!isset($_REQUEST['start_at']) || $_REQUEST['start_at'] <= 0) {
								$start_at = 0;
							} else {
								$start_at = $_REQUEST['start_at'];
							}

							$limit = $config['site']['rows_per_page'];

							if ($start_at >= count($temples)) {
						?>
								<li style="text-align:center;">
									<a href="javascript:;" id="load_more1" class="btn btn-default">No temples found.</a>
									<?php
										//if ($start_at > 0) {
									?>
										<script>
											jQuery("#load_more").hide();
										</script>
									<?php
										//}
									?>
								</li>
						<?php
							} else {
								$counter = 0;
								foreach ($temples as $tid=>$temple) {
									if ($counter < $start_at) {
										$counter++;
										continue;
									}
									if ($counter >= count($temples)) {
							?>
									<li style="text-align:center;">
										<a href="javascript:;" id="load_more1" class="btn btn-default">No more temples.</a>
									</li>
							<?php
										if ($counter > count($temples)) {
											break;
										}
									} else if ($counter > ($start_at + $limit - 1)) {
										break;
									}
									$counter++;
							?>
								<li class="blog-post">
									<script>
										jQuery("#load_more").show();
									</script>
									<!-- THUMBNAIL IMAGE -->
									<div class="blog-thumbnail">
										<a href="<?php echo $temple['urlkey']; ?>">
											<?php
												if (isset($temple['primary_image']) && $temple['primary_image'] != '') {
											?>
												<img src="/media/upload/image<?php echo $temple['primary_image']; ?>" alt="<?php echo ucwords($temple['alt_msg']); ?>">
											<?php
												} else {
											?>
												<img src="/media/upload/image/default_temple.png" alt="<?php echo ucwords($temple['alt_msg']); ?>">
											<?php
												}
											?>
										</a>
									</div>
							
									<!-- POST CONTENT -->
									<div class="blog-content">
										<a href="<?php echo $temple['urlkey']; ?>">
											<h2 class="temp-title">
												<?php echo strtoupper($temple['name']); ?>
											</h2>
										</a>
										<ul class="post-metadatas list-inline">
											<li>
												<i class="fa fa-sun-o"></i>
												<?php
													echo ucwords($temple['deity']);
												?>
											</li>
											<li>
												<i class="fa fa-map-marker"></i> 
												<?php
													echo ucwords($temple['location']);
												?>
											</li>
										</ul>
										<p>
											<?php
												echo $temple['short_description'];
											?>
										</p>
										<ul class="post-metadatas list-inline">
											<?php
												if (isset($temple['live_darshan_link']) && $temple['live_darshan_link'] != '') {
											?>
												<li>
													<i class="fa fa-video-camera"></i>
													<a target="_blank" href="<?php echo $temple['live_darshan_link']; ?>">
														Live Darshan
													</a>
												</li>
											<?php
												}
												
												if (isset($temple['pooja_in_temple_link']) && $temple['pooja_in_temple_link'] != '') {
											?>
												<li>
													<i class="fa fa-th-large"></i>
													<a target="_blank" href="<?php echo $temple['pooja_in_temple_link']; ?>">
														Pooja In Temple
													</a>
												</li>
											<?php
												}

												if (isset($temple['online_prasad_link']) && $temple['online_prasad_link'] != '') {
											?>
												<li>
													<i class="fa fa-fire"></i>
													<a target="_blank" href="<?php echo $temple['online_prasad_link']; ?>">
														Online Prasad
													</a>
												</li>
											<?php
												}

												if (isset($temple['book_hotels_link']) && $temple['book_hotels_link'] != '') {
											?>
												<li>
													<i class="fa fa-bookmark"></i>
													<a target="_blank" href="<?php echo $temple['book_hotels_link']; ?>">
														Book Hotels Now
													</a>
												</li>
											<?php
												}
											?>
										</ul>
										<div class="blog-button">
											<a href="<?php echo $temple['urlkey']; ?>" class="btn btn-default"> Read More</a>
										</div>
									</div>
								</li>
							<?php
								}
							}
						?>
					</ul>
					<ul class="blog-grid">
					</ul>
					<div class="blog-next-page center animate-me zoomIn">
						<a href="./results?start_at=<?php echo $_REQUEST['start_at']; ?>" id="load_more" class="btn btn-default">Load more</a>
						<img id="sloader" src="./images/loader.gif" style="display:none;width:16px;height:16px;">
						<script language="javascript">
							jQuery(document).ready(function ($) {
								$("#load_more").click(function(event) {
									event.preventDefault();
									$("#load_more").hide();
									$("#sloader").show();
									if ($("#start_at").val() == '0') {
										$("#start_at").val('<?php echo $limit; ?>');
									}
									$.ajax({
										type: "POST",
										url: 'results',
										data: $("#temple_search").serialize(),
										success: function(html) {
											$(".blog-grid:eq(1)").append($(html).find('.blog-grid:eq(0)').html());
											$("#sloader").hide();											
										},
									});
									$("#start_at").val(parseInt($("#start_at").val()) + parseInt(<?php echo $limit; ?>));
								});

								<?php
									if (count($temples) <= $limit) {
								?>
									$("#load_more").hide();
								<?php
									}
								?>
							});
						</script>
					</div>
				</div>
			</div>
	  	
			<!-- END MAIN CONTAINER -->
			<!-- START FOOTER -->
			<div class="with-separation-top"></div>
			<!-- SCRIPTS -->
			<script src="js/jquery-1.11.1.min.js"></script>
			<script src="js/plugins.js"></script>
			<script src="js/custom.js"></script>

			<!-- SCROLL TOP -->
			<a href="javascript:;" id="scroll-top" class="scrolltop fadeInRight animate-me">
				<i class="fa fa-angle-double-up"></i>
			</a><?php
				include_once('common/footer.php');
			?>
		</div>
		<script language="javascript">
			jQuery(window).scroll(function(){
				if (jQuery(this).scrollTop() > 100) {
					jQuery('.scrolltop').show();
				} else {
					jQuery('.scrolltop').hide();
				}
			});
			jQuery('.scrolltop').click(function(event){
				event.preventDefault();
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});
		</script>		
  	</body>
</html>
<?php
	//ob_end_clean();
?>
