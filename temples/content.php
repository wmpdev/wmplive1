<?php
	//ob_start();
	include_once('./common/config.php');

	/*if (!isset($_GET['tid']) || $_GET['tid'] < 1) {
		header('Location: /results');
		exit();
	}*/
	//print_r($_GET);exit;
    if (!isset($_GET['urlkey']) || $_GET['urlkey'] == "") {
        header('Location: /results');
        exit();
    } 
	function get_months($months) {
		$months_array = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$return = $return1 = array();
		
		$months_exploded = explode(',', $months);
		foreach ($months_array as $index=>$m) {
			if (!in_array($m, $months_exploded)) {
				$months_array[$index] = '';
			}
		}
		
		//echo "\n\n"; print_r($months_array); echo "\n\n";
		
		$current_counter = 0;
		for ($i=0; $i<12; $i++) {
			if ($months_array[$i] != '' && (!isset($return[$current_counter]) || count($return[$current_counter]) == 0)) {
				$return[$current_counter][] = $months_array[$i];
				if (isset($months_array[$i+1]) && $months_array[$i+1] == '') {
					$current_counter++;
				}
			} else if (isset($return[$current_counter]) && count($return[$current_counter]) > 0) {
				$return[$current_counter][] = $months_array[$i];
				if (isset($months_array[$i+1]) && $months_array[$i+1] == '') {
					$current_counter++;
				} else if ($i+1 >= 12) {
					if ($return[0][0] == 'January') {
						$return[0] = array_merge($return[$current_counter], $return[0]);
						unset($return[$current_counter]);
					}
				}
			}
			//echo "\n"; print_r($return); echo "\n";
		}

		//echo "\n\n"; print_r($return); echo "\n\n";
		
		foreach ($return as $r) {
			if (count($r) == 1) {
				$return1[] = substr($r[0], 0, 3);
			} else {
				$return1[] = substr($r[0], 0, 3) . '-' . substr(end($r), 0, 3);
			}
		}

		return implode(', ', $return1);
	}

	//$records_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE `id` = '" . $_GET['tid'] . "' AND `status` = 'active'");
    $records_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE `urlkey` = '" . $_GET['urlkey'] . "' AND `status` = 'active'");
	if ($records_temples->num_rows <= 0) {
		header('location: ./results');
		exit();
	}
	$record_temple = $records_temples->fetch_object();
	
	/*$services = array();
	$records_services = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "links` WHERE `temple_id` = '" . $record_temple->id . "'  AND `status` = 'active'");
	while($record_service = $records_services->fetch_object()) {
		$services[] = array(
			'text'=>ucwords($record_service->text),
			'link'=>$record_service->link,
			'picture'=>$record_service->picture,
		);
	}*/

	$uploads = array();
	$records_uploads = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "uploads` WHERE `temple_id` = '" . $record_temple->id . "' AND `type` = 'image' AND `status` = 'active'");
	while($record_upload = $records_uploads->fetch_object()) {
		$uploads[] = $record_upload->name;
	}

	$tabs = array();
	$records_tabs = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "tabs` WHERE `temple_id` = '" . $record_temple->id . "' AND `status` = 'active'");
	while($record_tab = $records_tabs->fetch_object()) {
		$tabs[] = $record_tab;
	}

	$records_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $record_temple->location_id . "' LIMIT 1");
	$record_location = $records_locations->fetch_object();

	$dbh->query("UPDATE `" . $mysql_table_prefix . "temples` SET `visits` = '" . ($record_temple->visits+1) . "' WHERE `id` = '" . $record_temple->id . "'");
	/*$records_near_by_city = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE `id` = '" . $record_temple->nearest_location . "' LIMIT 1");
	$record_near_by_city = $records_near_by_city->fetch_object();*/

	include_once('./common/inner_head.php');
?>
<!-- START BODY -->
	<body>
  		<div id="page">
	  		<!-- START HEADER -->
			<header id="header" class="small  with-separation-bottom">
				<!-- POINTER ANIMATED -->
				<canvas id="header-canvas"></canvas>
	  		
				<!-- TOP NAVIGATION 
				<div id="top-navigation">
		  			<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
			  			<div class="summary">
							<h2 class="classy f-left">
								<span>Shopping Cart -</span>
                        		<a href="#">0</a><span class="Itext"> item</span>
							</h2>
						</div>
					</ul>
				</div>

				<!-- MOBILE NAVIGATION -->
			

				<!-- MAIN MENU -->
				<nav id="navigation">
	  				<!-- DISPLAY MOBILE MENU -->
					
					<!-- CLOSE MOBILE MENU -->
					

					<div  class="animate-me flipInX" data-wow-duration="3s">
						<a href="/" id="logo-navigation"></a>
					</div>
				</nav>

				<!-- TEXT SLIDER -->
				<?php
					$show_heading = false;
					include_once('common/search.php');
				?>

				<!-- SHADOW -->
				<div id="shade2"></div>

				<!-- HEADER SLIDER -->
				<div class="flexslider" id="header-slider">
					<ul class="slides">
						<li><img src="images/bg1.jpg" alt="SLider Image"></li>
						<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
						<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
					</ul>	
				</div>
				
				<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
				<!--<div id="header-video"
					data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
				</div>-->
			</header>
	  		<!-- END HEADER -->
	  	
			<!-- START MAIN CONTAINER -->
			<div class="main-container">
				<div class="container">
					<!--BLOG -->
					<h1 class="with-breaker animate-me fadeInUp">
						<?php
							echo ucwords($record_temple->name);
						?>
						<span>
						<?php
							echo ucwords($record_location->city);
						?>
						</span>
					</h1>
					<div class="row">
						<div class="col-md-7">
							<div class="row">
								<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 380px; overflow: hidden; ">
								<!-- Loading Screen -->
								<div u="loading" style="position: absolute; top: 0px; left: 0px;">
									<div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
										background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
									</div>
									<div style="position: absolute; display: block; background: url(./images/loading.gif) no-repeat center center;
										top: 0px; left: 0px;width: 100%;height:100%;">
									</div>
								</div>
								<!-- Slides Container -->
								<div u="slides" style="cursor: move; position: absolute; left: 0px;margin:0 auto; top: 0px; width: 600px; height: 300px; overflow: hidden;">
									<?php
										if (count($uploads) > 0) {
											foreach ($uploads as $upload) {
									?>
											<div>
												<img alt="<?php echo $record_temple->alt_msg ?>" u="image" src="/media/upload/image<?php echo $upload; ?>"/>
												<img alt="<?php echo $record_temple->alt_msg ?> u="thumb" src="/media/upload/image<?php echo $upload; ?>" />
											</div>
									<?php
											}
										} else {
									?>
											<div>
												<img u="image" alt="<?php echo $record_temple->alt_msg ?>" src="/media/upload/image/default_temple.png"/>
											</div>
									<?php
										}
									?>
								</div>    
								<!-- Arrow Left -->
								<span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
								</span>
								<!-- Arrow Right -->
								<span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
								</span>
								<!-- Arrow Navigator Skin End -->

								<!-- ThumbnailNavigator Skin Begin -->
								<div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 0px;">
									<div style=" width: 100%; height:100%;"></div>

									<div u="slides" style="cursor: move;position: absolute;overflow: hidden;left: 115.5px;top: 5px;width: 369px;height: 50px;z-index: 0;">
										<div u="prototype" class="p" style="POSITION: absolute; WIDTH: 68px; HEIGHT: 40px; TOP: 0; LEFT: 0;">
											<div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
											<div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
											</div>
										</div>
									</div>
									<!-- Thumbnail Item Skin End -->
								</div>
							</div>
						</div>
						<br/>
						<br/>
						<div class="temple-description">
							<?php
								echo $record_temple->description;
							?>
						</div>
					</div>
					<div class="col-md-4">

				
					
						<iframe src="<?php echo $record_temple->google_map_link; ?>" width="400" height="300" frameborder="0" style="border:0"></iframe>
						<br/>
						<style type="text/css">
							table td {
								padding-left:4px;
								padding-top: 10px;
							}
						</style>
						<table class="location-special" style="margin-top:24px;">
							<tr>
								<td>
									<strong>Locality/village</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo ucwords($record_location->city); ?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>State</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo ucwords($record_location->state); ?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Country </strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo ucwords($record_location->country); ?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Nearest City/Town</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo ucwords($record_temple->nearest_location); ?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Best Season To Visit</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									<?php 
										if ($record_temple->months == 'All') {
											echo 'All';
										} else {
											echo get_months($record_temple->months);
										}
									?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Languages</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo ucwords($record_temple->languages); ?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Altitude</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo $record_temple->altitude; ?>
								</td>
							</tr>
							<tr>
								<td>
									<strong>Photography</strong>
								</td>
								<td>
									: 
								</td>
								<td>
									 <?php echo ucwords($record_temple->photography); ?>
								</td>
							</tr>
					
						</table>
						<br/>
						
<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q='<?php// echo ucwords($record_location->city);?>',Tamil+Nadu,India" title="'<?php //echo ucwords($record_location->city);?>', Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q='<?php// echo ucwords($record_location->city);?>',Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in '<?php //echo ucwords($record_location->city);?>', Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>/-->
						
						<br/>
						
					</div>
					
					
					
					
					
				</div>

				<div class="row">
					<ul class="temple-services">
						<?php
							$class = 'orange';
							if (isset($record_temple->live_darshan_link) && $record_temple->live_darshan_link != '') {
						?>
							<li class="<?php echo $class; ?>">
								<i class="fa fa-video-camera"></i>
								<a target="_blank" href="<?php echo $record_temple->live_darshan_link; ?>">
									Live Darshan
								</a>
							</li>
						<?php
								$class = 'yellow';
							}
							
							if (isset($record_temple->pooja_in_temple_link) && $record_temple->pooja_in_temple_link != '') {
						?>
							<li class="<?php echo $class; ?>">
								<i class="fa fa-th-large"></i>
								<a target="_blank" href="<?php echo $record_temple->pooja_in_temple_link; ?>">
									Pooja In Temple
								</a>
							</li>
						<?php
							}

							if (isset($record_temple->online_prasad_link) && $record_temple->online_prasad_link != '') {
						?>
							<li class="<?php echo $class; ?>">
								<i class="fa fa-fire"></i>
								<a target="_blank" href="<?php echo $record_temple->online_prasad_link; ?>">
									Online Prasad
								</a>
							</li>
						<?php
							}

							if (isset($record_temple->book_hotels_link) && $record_temple->book_hotels_link != '') {
						?>
							<li class="<?php echo $class; ?>">
								<i class="fa fa-bookmark"></i>
								<a target="_blank" href="<?php echo $record_temple->book_hotels_link; ?>">
									Book Hotels Now
								</a>
							</li>
						<?php
							}
						?>
					</ul>

				</div>
				
				<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<?php
							$counter = 1;
							foreach ($tabs as $tab) {
						?>
						<li class="<?php if ($counter == 1) {?>active<?php } ?>">
							<a href="#skill<?php echo $counter; ?>" role="tab" data-toggle="tab"><?php echo $tab->title; ?></a>
						</li>
						<?php
								$counter++;
							}
						?>
					</ul>

					<div class="tab-content">
						<?php
							$counter = 1;
							foreach ($tabs as $tab) {
						?>
								<div class="tab-pane <?php if ($counter == 1) {?>active<?php } ?> bounceInRight <?php echo $tab->extra_classes; ?>" id="skill<?php echo $counter; ?>">
									<h2><?php echo $tab->title; ?></h2>
									<?php
										echo $tab->description;
									?>
								</div>
						<?php
								$counter++;
							}
						?>
					</div>
					<script>
						$(function () {
							$('#SkillsTab a').click(function (e) {
								e.preventDefault();
								$(this).tab('show');
							});
						});
					</script>
				</div>
				<style type="text/css">
				</style>				
			</div>
		</div>
	  	<!-- END MAIN CONTAINER -->
	  	
		<!-- START FOOTER -->
	  	<div class="with-separation-top"></div>
		<!-- SCRIPTS -->
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins2.js"></script>
		<script src="js/custom2.js"></script>

		<?php
			include_once('./common/footer.php');
		?>
	</body>
</html>
<?php
	//ob_end_clean();
?>
