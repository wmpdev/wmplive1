<?php
	if (!isset($_SESSION) || count($_SESSION) == 0) {
		session_start();
	}
?>
<?php
	include_once('./common/config.php');
	
	switch($_GET['action']) {
		case 'get_temples':
			$temples = array();
			$temples_id_array = array();
			
			$records_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE CONVERT(`name` USING utf8) LIKE _utf8 '" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci ORDER BY `name` ASC LIMIT 10");
			while ($record_temple = $records_temples->fetch_object()) {
				$temples[] = array('value'=>$record_temple->urlkey, 'label'=>$record_temple->name, 'text'=>$record_temple->name);
				$temples_id_array[] = $record_temple->urlkey;
			}
			
			$records_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE CONVERT(`name` USING utf8) LIKE _utf8 '%" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci AND CONVERT(`name` USING utf8) NOT LIKE _utf8 '" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci ORDER BY `name` ASC LIMIT 10");
			while ($record_temple = $records_temples->fetch_object()) {
				if (!in_array($record_temple->id, $temples_id_array)) {
					$temples[] = array('value'=>$record_temple->urlkey, 'label'=>$record_temple->name, 'text'=>$record_temple->name, 'urlkey'=>$record_temple->urlkey);
					$temples_id_array[] = $record_temple->urlkey;
				}
			}
			
			$records_temples = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE CONVERT(`tags` USING utf8) LIKE _utf8 '%" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci ORDER BY `name` ASC LIMIT 10");
			while ($record_temple = $records_temples->fetch_object()) {
				if (!in_array($record_temple->id, $temples_id_array)) {
					$temples[] = array('value'=>$record_temple->urlkey, 'label'=>$record_temple->name, 'text'=>$record_temple->name, 'urlkey'=>$record_temple->urlkey);
					$temples_id_array[] = $record_temple->urlkey;
				}
			}
			echo json_encode($temples);
			break;

		case 'get_locations':
			$dbh->query();
			if (isset($_GET['all']) && $_GET['all'] == 1) {
				$locations = array('0'=>array("type"=>"country","location"=>"","value"=>"All"));
			} else {
				$locations = array();
			}
			
			$records_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE CONVERT(`city` USING utf8) LIKE _utf8 '%" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci LIMIT 10");
			if ($records_locations->num_rows > 0) {
				while ($record_location = $records_locations->fetch_object()) {
					$locations[$record_location->id]['type'] = 'city';
					$locations[$record_location->id]['location'] = $record_location->id;
					$locations[$record_location->id]['value'] = ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
				}
			} else {
				$records_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE CONVERT(`state` USING utf8) LIKE _utf8 '%" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci LIMIT 10");
				if ($records_locations->num_rows > 0) {
					while ($record_location = $records_locations->fetch_object()) {
						$locations[$record_location->id]['type'] = 'state';
						$locations[$record_location->id]['location'] = $record_location->id;
						$locations[$record_location->id]['value'] = ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
					}
				} else {
					$records_locations = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "locations` WHERE CONVERT(`country` USING utf8) LIKE _utf8 '%" . $_REQUEST['term'] . "%' COLLATE utf8_general_ci LIMIT 10");
					if ($records_locations->num_rows > 0) {
						while ($record_location = $records_locations->fetch_object()) {
							$locations[$record_location->id]['type'] = 'country';
							$locations[$record_location->id]['location'] = $record_location->id;
							$locations[$record_location->id]['value'] = ucwords($record_location->city . ', ' . $record_location->state . ', ' . $record_location->country);
						}
					}
				}
			}

			echo json_encode($locations);
			break;
	}
?>
