<?php
	//ob_start();
	include_once('./common/config.php');
	include_once('./common/head.php');
	//include_once('./common/magento_head.php');
?>
	<!-- START BODY -->
	<body>
		<!-- LOADER DIV - ONLY HOME 
		<div id="loader">
			<div id="loading-logo"></div>
		</div>

		<!-- START ONLY DEMO 
			<style type="text/css" scoped>
				@media only screen and (max-width: 992px) {
					#page-customizer{display: none;}
				}
				#customizer-toggle{
					background: #FFF;
					padding: 12px 15px;
					float: right;
					position: relative;
					right: -40px;
					border-radius: 0 5px 5px 0;
					margin-top: 30px;
					color: #444;
				}
				#customizer-toggle:hover{color: #B4B4B4;}
				#page-customizer{
					z-index: 100;
					position: fixed;
					left: -230px;
					top: 20%;
				}
				#page-customizer.show-me{left: 0;}
				#container-customizer{
					background: #FFF;
					padding: 30px;
					width: 230px;
					border-radius: 0 5px 5px 0;
				}
				#container-customizer a.btn{display: block;}
				#container-customizer h5{font-weight: bold;}
			</style>-->
			

		<div id="page">
			<!-- START HEADER -->
			<header id="header" class="big with-separation-bottom">
				<!-- POINTER ANIMATED -->
				<canvas id="header-canvas"></canvas>

				<!-- TOP NAVIGATION 
				<div id="top-navigation">
					<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
						<div class="summary">
							<h2 class="classy f-left"><span>Shopping Cart -</span>
								<a href="#">0</a><span class="Itext"> item</span>
							</h2>
						</div>
					</ul>
				</div>
	  	
				<!-- MOBILE NAVIGATION -->
				
	  	
				<!-- MAIN MENU -->
				<nav id="navigation">
					
	
					<div  class="animate-me flipInX" data-wow-duration="3s">
						<a href="/" id="logo-navigation"></a>
					</div>
				</nav>
 
				<!-- TEXT SLIDER -->
				<?php
					$show_heading = true;
					include_once('common/search.php');
				?>

				<!-- SCROLL BOTTOM -->
				<div id="scroll-bottom" class="animate-me fadeInUpBig">
					<a href="#"><i class="fa fa-angle-double-down"></i></a>
				</div>

				<!-- SHADOW -->
				<div id="shade"></div>

				<!-- SLIDER NAVIGATION (DELETE IF YOU DON'T USE THE SLIDER) 
				<div class="navigation-slider-container">
					<a href="#" id="sliderPrev"><i class="fa fa-arrow-left"></i></a>
					<a href="#" id="sliderNext"><i class="fa fa-arrow-right"></i></a>
				</div>-->

				<!-- HEADER SLIDER -->
				<div class="flexslider" id="header-slider">
					<ul class="slides">
						<li><img src="images/bg1.jpg" alt="SLider Image"></li>
						<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
						<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
					</ul>	
				</div>
				<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
				<!--<div id="header-video"
					data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
				</div>-->
			</header>

			<div class="container full-width">
			<div class="headingBox">
				<h2>
					<span>Most Famous Temples</span>
				</h2>
			</div>
		</div>
		<div class="row famous-temples">
			<?php
				$counter = 0;
				$records_famous = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "temples` WHERE `show_on_homepage` = 'yes'");
				while ($record_famous = $records_famous->fetch_object()) {
					$records_uploads = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "uploads` WHERE `temple_id` = '" . $record_famous->id . "' AND `is_primary` = 'yes' LIMIT 1");
					if($record_upload = $records_uploads->fetch_object()) {
			?>
						<div class="span3 spnhover <?php if (!($counter%4)) {?>firstdiv<?php } else {?>nonfirstdiv<?php } ?>">
							<a href="<?php echo $record_famous->urlkey; ?>"><img src="/media/upload/image<?php echo $record_upload->name; ?>" alt="<?php echo ucwords($record_famous->name); ?>"></a>
						</div>
			<?php
						$counter++;
						if ($counter >= 8) {
							break;
						}
					}
				}
			?>
		</div>

		<?php
			include_once('common/footer.php');
		?>
	  	
	  	<!-- SCROLL TOP -->
		
	  	<a href="#" id="scroll-top" class="fadeInRight animate-me">
			<i class="fa fa fa-arrow-up"></i>
		</a>
  	
  	</div>

    <!-- SCRIPTS -->
    <script src="js/jquery-ui.js"></script>
    <script src="js/plugins2.js"></script>
	
    <script src="js/custom2.js"></script>
	
  </body>
  <!-- END BODY -->
</html>
<?php
	//ob_end_clean();
?>