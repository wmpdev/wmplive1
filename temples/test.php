<?php
	include_once('./common/config.php');

	$months = array('2'=>'January', '3'=>'February', '4'=>'March', '5'=>'April', '6'=>'May', '7'=>'June', '8'=>'July', '9'=>'August', '10'=>'September', '11'=>'October', '12'=>'November', '13'=>'December');

	$record_temples = $dbh->query('SELECT * FROM `tm_temples` WHERE `months` != "All"');
	while ($record_temple = $record_temples->fetch_object()) {
		$return_array = array();

		$exploded = explode(',', $record_temple->months);

		foreach ($exploded as $index=>$ex) {
			if ($ex != 1) {
				$return_array[$index] = $months[$ex];
			}
		}

		$return_string = implode(',', $return_array);

		echo $record_temple->id . " :: " . $record_temple->months . " :: " . $return_string . "<br/>";
		//$dbh->query("UPDATE `tm_temples` SET `months` = '" . $return_string . "' WHERE `id` = '" . $record_temple->id . "'");
	}
