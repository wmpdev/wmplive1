<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . '/app/Mage.php'); // ABSOLUTH PATH TO MAGE
	umask(0);
	Mage::app ();
	 
	Mage::getSingleton('core/session', array('name'=>'frontend'));   // GET THE SESSION
	 
	$symbol= Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();  // GET THE CURRENCY symbol
	$store=Mage::app()->getStore()->getCode();  // GET THE STORE CODE
	 
	$cart = Mage::getSingleton('checkout/cart'); //->getItemsCount();   
	
	$_POST['qty'] = (!isset($_POST['qty']) || $_POST['qty'] == '0' || $_POST['qty'] == '' || !ctype_digit($_POST['qty']))?1:$_POST['qty'];

	$ajtem=$_POST['item'];    // THIS IS THE ITEM ID
	$items = $cart->getItems();
	foreach ($items as $item) {   // LOOP
	   if($item->getId()==$ajtem){  // IS THIS THE ITEM WE ARE CHANGING? IF IT IS:
			  $item->setQty($_POST['qty']); // UPDATE ONLY THE QTY, NOTHING ELSE!
			  $cart->save();  // SAVE
			  Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
		   echo $_POST['qty'] . " x ";
			  break;
	   }
	 
	}
?>
<script type="text/javascript">
	function updateTotalG(){
		jQuery("p.subtotal span.price").html("<?php echo $symbol . " " . number_format(Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal(),0); ?>");
		jQuery("div.summary h2.classy a").html("<?php echo Mage::helper('checkout/cart')->getSummaryCount(); ?>");
		if (<?php echo Mage::helper('checkout/cart')->getSummaryCount(); ?> == 1)
		{
			jQuery("div.shopping-bag p.empty").html("<?php echo 'Shopping Bag (' . Mage::helper('checkout/cart')->getSummaryCount() . ' item)'; ?>");
			jQuery("div.summary h2.classy span.Itext").html(" item");
		} else {
			jQuery("div.shopping-bag p.empty").html("<?php echo 'Shopping Bag (' . Mage::helper('checkout/cart')->getSummaryCount() . ' items)'; ?>");
			jQuery("div.summary h2.classy span.Itext").html(" items");
		}
		jQuery(".cart_input_<?php echo $_POST['item']; ?>").val("<?php echo $_POST['qty']; ?>");
	}
</script>