<?php
	
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
	
umask(0);
Mage::app();
Mage::getSingleton('core/session', array('name'=>'frontend'));
	
$session = Mage::getSingleton('customer/session');
if($session->isLoggedIn()) 
{
		$userId = $session->getCustomer()->getId();
} else {
		$userId = 0;
}

$block              = Mage::getSingleton('core/layout');
	
	
$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
 

?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Char Dham Yatra</title>
<meta name="description" content="We provides you comprehensive guidance on Char Dham yatra. It includes significance, history, itinerary, nearby temples, etc.">
<meta name="keywords" content="char dham yatra">
<link rel="canonical" href="https://www.wheresmypandit.com/yatras/char-dham.php" />

<link rel="icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link rel="shortcut icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="inner_css/home.css" type="text/css" rel="stylesheet">
<!--<link href="inner_css/styles.css" type="text/css" rel="stylesheet">-->
<link href="style.css" type="text/css" rel="stylesheet">
<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
 <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <!--<link href="css/options.css" rel="stylesheet" media="screen">-->
<link href="css/custom.css" type="text/css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
<link href="css/main.css" type="text/css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/thumbelina.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<link href="js/jquery-ui.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/customslide.js"></script>
<script type="text/javascript" src="js/jssor.js"></script>
<script type="text/javascript" src="js/jssor.slider.js"></script>
 <link href="css/hor-timeline.css" rel="stylesheet" media="screen">
<script src="js/jquery.timelinr-0.9.54.js"></script> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

</head>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h1 class="with-breaker animate-me fadeInUp">
		  			Char Dham Yatra
	  			</h1>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/cd1.png" alt="Char Dham Yatra" />
                <img u="thumb" src="images/slider/cd1-tn.png" alt="Char Dham Yatra" />
            </div>
            <div>
                <img u="image" src="images/slider/cd2.png" alt="4 Dham Yatra" />
                <img u="thumb" src="images/slider/cd2-tn.png" alt="4 Dham Yatra" />
            </div>
            <div>
                <img u="image" src="images/slider/cd3.png" alt="Char Dham Yatra - Dwarka" />
                <img u="thumb" src="images/slider/cd3-tn.png" alt="Char Dham Yatra - Dwarka" />
            </div>
            <div>
                <img u="image" src="images/slider/cd4.png" alt="Char Dham Yatra - Badrinath" />
                <img u="thumb" src="images/slider/cd4-tn.png" alt="Char Dham Yatra - Badrinath" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>The four heavenly abodes are known as Char Dham. These are the four pilgrimage sites in India - Badrinath, Dwarka, Puri and Rameswaram. 
				 These are greatly revered by Hindus. They are considered highly sacred by the Hindu devotees. One should surely go on a <strong>Char Dham Yatra</strong> once in his entire lifetime. 
				As defined by Aadi Shankaracharya, a Char Dham consists of three Vaishnavite and one Shaivite pilgrimages.</p>
<p></p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
				 <li><strong>No. of Places to be visited</strong> : 4</li>
                    <br/>
                    <!--<li><strong>Day 3 </strong>: CHINTAMANI , Girijatmaj  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
                    <li><strong>Total Distance</strong> : 6364 KM</li>
                    <br>
                    <li><strong>Best Season to visit</strong> : May to Oct</li>
				</ul><br/><br/>
					
				 <a href="images/chardham.jpg"><img class="img-map" src="images/chaar_dham_yatra.jpg" alt="Char Dham Yatra Map" width="400" height="450"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->

						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>
						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>To perform austerities and penance in order to attain salvation and ultimate happiness, people undertake this tour on a really large scale.
                            These holy and sacred pilgrimages are a must.
                            The term Char Dham was coined by the great Guru Aadi Shankaracharya which means the four abodes of God. It is known that the Lord resides here eternally.</p>
                            						
					  </div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul style="text-align:left">
								Badrinath became prominent when when Nar-Narayan performed penance here. This took place in the ‘Satyuga’ and hence it is the first Dham. In ‘Tretayuga’, Rameshwaram gained importance.
								It is believed that Lord Rama built a Shivlinga here and worshipped it faithfully. ‘Dwaparyuga’ made Dwarka popular. Lord Krishna resisded here for a long time leaving his birthplace, Mathura. 
                                The fourth dham, Jagannathpuri is also extremely famous for its Rath Yatra. 
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=44" title="Dwarkadhish Temple">Dwarkadhish</a></strong> Jamnagar</li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=31" title="Rameshwaram Temple">Rameshwaram</a></strong> Madhurai </li>
								</ul>
								
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=61" title="Badrinath Temple">Badrinath</a></strong> Dehradun </li><br>
								</ul>

								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=25" title="Jagannath Temple">Jagannath Puri</a></strong> Bhubneshwar  </li><br>
								</ul>
						</div>	

					<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Jamnagar</li>
<li><span class="city">Dwarkadesh Temple</span><span>131 kms </span><span>2h 16mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Dehradun</li>
<li><span class="city">Jamnagar</span><span>1436 kms </span><span>21h 21mins</span></li>
<li><span class="city">Badrinath Temple	</span><span>336 kms </span><span>5h 32mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Bhubneshwar </li>
<li><span class="city">Dehradun </span><span>1849 kms </span><span>27h 00mins</span></li>
<li><span class="city">Jagannath Puri temple</span><span>62.7 kms </span><span>1h 12mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Madurai</li>
<li><span class="city">Bhubneshwar </span><span>1683 kms </span><span>25h 0mins</span></li>
<li><span class="city">Rameshwaram Temple (Arulmigu Ramanathaswamy Temple)	</span><span>169  kms </span><span> 9mins</span></li>
</ul>
</div>

<hr/>


<!--Mobile content-->


<?php

}
else
{
?>

<!--Desktop content-->
						
	<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#jamnagar">Jamnagar</a></li>
			<li><a href="#dehradun">Dehradun</a></li>
			<li><a href="#bhubneshwar">Bhubneshwar</a></li>
			<li><a href="#madurai">Madurai</a></li>
			<!--<li><a href="#kedarnath">Kedarnath</a></li>-->
		</ul>
		<ul id="issues">
      
			<li id="Jamnagar">
					<ul class="route">
					<li> -</li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hour 0 min</li>
					<li  class="flight">0 Hours 0 min </li>
					<li class="km"> 0</li>
					<li>Jamnagar</li>
					</ul>
                    
                    <div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Dwarkadhish Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Jamnagar:</td>								
						<td>131  KM </td>								
						<td> 2 h 16 min</td>								
					</tr>
								
				</tbody>
				</table>
				

				</div>
                <div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong> Jamnagar-Dwarkadish Temple-Jamnagar
				<strong>Minimum Duration</strong>4 h 33 min
				<strong>Minimum Distance</strong>261 KM

				</div> 
				
				</div>
                    
				
			<li id="dehradun"> 
					<ul class="route">
					<li>Jamnagar</li>
					<li class="car"> 21 Hours 21 Min</li>
					<li class="train"> 32 Hours 13 Min</li>
					<li  class="flight"> 4 Hours 10 Min </li>
					<li class="km"> 1436 </li>
					<li> Dehradun</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Badrinath Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Dehradun:</td>								
						<td>336 KM </td>								
						<td> 5 Hour 32 min</td>								
					</tr>
				
			
				</tbody>
				</table>
			</div>
            <div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong> Dehradun-Badrinath Temple-Dehradun
				<strong>Minimum Duration</strong>11 h 17 min
				<strong>Minimum Distance</strong>673 KM

				</div> 
				</div>
								
			</li>
			
			<li id="bhubneshwar"> 
					<ul class="route">
					<li> Dehradun</li>
					<li class="car"> 27 Hours 00 Min</li>
					<li class="train"> 46 Hours 15 Min </li>
					<li  class="flight"> 05 Hours 35 Min</li>
					<li class="km"> 1849</li>
					<li> Bhubneshwar</li>
					</ul>
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Jagannath Puri temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Bhubneshwar:</td>								
						<td>62.7 KM </td>								
						<td> 1 h 12 min</td>								
					</tr>
				
									
				</tbody>
				</table>
				

				</div>
                <div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong> Bhubneshwar-Jagannath Puri Temple-Bhubneshwar 
				<strong>Minimum Duration</strong>2 h 19 min
				<strong>Minimum Distance</strong>125 KM

				</div> 
                
				</div>  
				
				
			</li>
			
			<li id="madurai">
					<ul class="route">
					<li> Bhubneshwar</li>
					<li class="car"> 25 Hours</li>
					<li class="train"> 31 Hours 54 Min</li>
					<li  class="flight"> 02 Hours 50 Min</li>
					<li class="km"> 1683</li>
					<li> Madurai</li>
					</ul>
			<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Rameshwaram Temple (Arulmigu Ramanathaswamy Temple) </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Madurai:</td>								
						<td>169 KM </td>								
						<td> 3 h 9 min</td>								
					</tr>
				
									
				</tbody>
				</table>
				

				</div>
                <div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong> Madurai-Rameshwaram Temple-Madhurai
				<strong>Minimum Duration</strong>6 h 18 min
				<strong>Minimum Distance</strong>338 KM

				</div> 
                
				</div>  				
				
			</li>
			
					
		</ul>	
		
	</div>
	<?php 

}
?>
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2oN6IZ8rywS4dflnIdWupkpOY2FLK0Bx';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
 <?php echo $footerBlock; ?>
		  </div>
		  </body>
		  </html>