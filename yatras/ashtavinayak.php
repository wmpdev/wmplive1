<?php
	
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
	
umask(0);
Mage::app();
Mage::getSingleton('core/session', array('name'=>'frontend'));
	
$session = Mage::getSingleton('customer/session');
if($session->isLoggedIn()) 
{
		$userId = $session->getCustomer()->getId();
} else {
		$userId = 0;
}

$block              = Mage::getSingleton('core/layout');
	
	
$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
 

?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ashtavinayak Yatra</title>
<meta name="description" content="We provides you comprehensive guidance on Ashtavinayak yatra. It includes significance, history, itinerary, nearby temples, etc.">
<meta name="keywords" content="ashtavinayak yatra">
<link rel="canonical" href="https://www.wheresmypandit.com/yatras/ashtavinayak.php" />

<link rel="icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link rel="shortcut icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="inner_css/home.css" type="text/css" rel="stylesheet">
<!--<link href="inner_css/styles.css" type="text/css" rel="stylesheet">-->
<link href="style.css" type="text/css" rel="stylesheet">
<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
 <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <!--<link href="css/options.css" rel="stylesheet" media="screen">-->
<link href="css/custom.css" type="text/css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
<link href="css/main.css" type="text/css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/thumbelina.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<link href="js/jquery-ui.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/customslide.js"></script>
<script type="text/javascript" src="js/jssor.js"></script>
<script type="text/javascript" src="js/jssor.slider.js"></script>
 <link href="css/hor-timeline.css" rel="stylesheet" media="screen">
<script src="js/jquery.timelinr-0.9.54.js"></script> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

</head>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h1 class="with-breaker animate-me fadeInUp">
		  			Ashtavinayak Yatra
	  			</h1>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/as1.png" alt="Ashtavinayak Yatra" />
                <img u="thumb" src="images/slider/as1-tn.png" alt="Ashtavinayak Darshan" />
            </div>
            <div>
                <img u="image" src="images/slider/as2.png" alt="Ashtavinayak Yatra - Lenyadri" />
                <img u="thumb" src="images/slider/as2-tn.png" alt="Ashtavinayak Yatra - Lenyadri" />
            </div>
            <div>
                <img u="image" src="images/slider/as3.png" alt="Eight Ganesha Pilgrimages" />
                <img u="thumb" src="images/slider/as3-tn.png" alt="Lenyadri - Ashtavinayak Yatra" />
            </div>
            <div>
                <img u="image" src="images/slider/as4.png" alt="Ashtavinayak pilgrimage" />
                <img u="thumb" src="images/slider/as4-tn.png" alt="Eight Ganesha temples" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>The Ashtavinayak pilgrimage is of deep significance. Ashtavinayak literally means ‘Eight Ganeshas’ in Sanskrit. Lord Ganesha is the deity of unity, prosperity and learning. The Lord also removes the obstacles. The <strong>Ashtavinayak yatra</strong> is basically a holy pilgrimage to the eight Hindu temples in the state of Maharashtra with eight distinct idols of Lord Ganesha in a pre-ascertained sequence. Every temple has its own legend and history associated with it. Also, the form of each Ganesha idol and its trunk is visibly different from the other. After visiting all the eight Ganpatis, the first Ganpati should be visited again in order to complete the yatra. </p>
				 <p></p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
					<li><strong>No. of Places to be visited</strong> : 8 </li><br/>
					<!--<li><strong>Day 3 </strong>: CHINTAMANI , Girijatmaj  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
					<li><strong>Total Distance</strong> : 946 KM</li><br>
					<li><strong>Best Season to visit</strong> : Nov to Mar</li>
				 </ul>
				 <br/><br/>
					
				 <a href="images/ashtavinayak.jpg"><img class="img-map" src="images/ashtavinayak.jpg" width="400" height="450"/ alt="Ashtavinayak Yatra Map"></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>

						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>There are numerous Ganesha temples around the world. But, these eight Ganesha pilgrimages are of utmost significance and auspiciousness than the others. They are worshipped with utmost reverence. The stories of the eight incarnations are ancient and so are the temples. While some of them are built and rebuilt during the time of Madhavrao Peshwa. It is believed that the idols are self-existent. Each of these idol is an incarnation of Lord Ganesha and has its own mythological belief associated to it and each of it conveys a story to us about how and why these idols got their names and existence. </p>
							
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul style="text-align:left">
								The Gaanpatya sect came into existence during the post Gupt period. The followers of the sect worshipped and paid homage to Lord Ganesha. According to the puranas, Lord Shiva and Goddess Parvati are his parents. 
								Throughout India, there are 21 very important Ganesha sites eight of which belong to Maharashtra. The rise of Marathas also brought development in the Ashtavinayak.
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i>  Near by Temple</h2>
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=60" title="Moreshwar Ganpati Temple">Moreshwar Temple</a> : </strong> Morgaon </li>
								<li><strong>Varadvinayak Temple :</strong> Mahad</li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=59" title="Vighneshwar Ganpati Temple">Vighneshwar Temple</a> :</strong> Ozar</li>
<!--								<li><strong>Temple Name</strong> Temple Location </li>-->
								
								</ul>
								
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=45" title="Shree Siddhivinayak Temple">Shree Siddhivinayak</a> : </strong> Siddhatek </li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=4" title="Chintamani Temple">Chintamani Temple</a> : </strong> Theur </li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=62" title="Mahaganpati Temple">Mahaganpati Temple</a> : </strong> Ranjangaon </li>
<!--								<li><strong>Temple Name</strong> Temple Location </li>-->
								
								</ul>
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=1" title="Ballaleshwar Ganpati Temple">Ballaleshwar Temple</a> : </strong> Pali </li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=5" title="Girijadmaj Ganpati Mandir">Girijadmaj Ganpati Mandir</a>:</strong>Lenyadri </li><br>

						<!--<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>-->
								
								</ul>
						</div>	
						<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Pune</li>
<li><span class="city">Moreshwar Temple (Morgaon)</span><span>66.6 kms </span><span>1h 47mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Shree Siddhivinayak Temple (Siddhatek)</li>
<li><span class="city">Morgaon</span><span>69.9 kms </span><span>1h 17mins</span></li>
<li><span class="city">Shree Siddhivinayak Temple (Siddhatek)	</span><span>105 kms </span><span>2h 5mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Ballaleshwar Ganpati Temple (Pali)</li>
<li><span class="city">Siddhatek</span><span>227 kms </span><span>4h 51mins</span></li>
<li><span class="city">Pune	</span><span>128 kms </span><span>2h 40mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Varad Vinayak Ganpati Mandir (Mahad)</li>
<li><span class="city">Pali</span><span>37.6 kms </span><span>0h 59mins</span></li>
<li><span class="city">Pune</span><span>85.8 kms </span><span>1h 32mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li> Shree Chintamani Temple (Theur)</li>
<li><span class="city">Mahad</span><span>105 kms </span><span>2h 54mins</span></li>
<li><span class="city">Pune	</span><span>27.2 kms </span><span>4h 8mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Girijatmaj Temple (Lenyadri)</li>
<li><span class="city">Theur</span><span>108 kms </span><span>2h 45mins</span></li>
<li><span class="city">Pune	</span><span>47 kms </span><span>2h 28mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Vighneshwar Temple (Ozar)</li>
<li><span class="city">Lenyadri</span><span>14.4 kms </span><span>0h 28mins</span></li>
<li><span class="city">Pune</span><span>87 kms </span><span>2h 16mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Mahaganpati Temple (Ranjangaon)</li>
<li><span class="city">Ozar</span><span>73.4 kms </span><span>1h 45mins</span></li>
<li><span class="city">Pune</span><span>51.6 kms </span><span>1h 21mins</span></li>
</ul>
</div>

<hr/>

<!--Mobile content-->


<?php

}
else
{
?>
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#pune">Pune</a></li>
			<li><a href="#morgaon">Morgaon</a></li>
			<li><a href="#siddhatek">Siddhatek</a></li>
			<li><a href="#pali">Pali</a></li>
			<li><a href="#mahad">Mahad</a></li>
   			<li><a href="#theur">Theur</a></li>
            <li><a href="#lenyadri">Lenyadri</a></li>
            <li><a href="#ozar">Ozar</a></li>
            <li><a href="#ranjangaon">Ranjangaon</a></li>
            <li><a href="#morgaon">Morgaon</a></li>
            <li><a href="#Pune1">Pune</a></li>
		</ul>
		<ul id="issues">
			<li id="pune">
					<ul class="route">
					<li> -</li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 0</li>
					<li> Pune</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Pune </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
                    
                    <tr>
						<td>From Moreshwar Temple:</td>								
						<td>76 KM </td>								
						<td> 1 h 47 min</td>								
					</tr>
                    
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>								
						<td>101 KM </td>								
						<td> 2 h 10 min</td>								
					</tr>
				
					<tr>
						<td>From Ballaleshwar Ganpati Temple:</td>								
						<td> 121 KM</td>								
						<td> 2 h 26 min</td>							
					</tr>
				
					<tr>
						<td>From Varad Vinayak Ganpati Mandir:</td>	
						<td>86.8 KM </td>								
						<td> 1 h 37 min</td>
					</tr>
					<tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>27.5 KM </td>								
						<td> 0 h 57 min</td>
					</tr>


					<tr>
						<td>From Girijatmaj Temple:</td>	
						<td>96.3 KM </td>								
						<td> 2 h 32 min</td>
					</tr>

					<tr>
						<td>From Vighneshwar Temple:</td>	
						<td>86.8 KM </td>								
						<td> 2 h 20 min</td>
					</tr>

					<tr>
						<td>From Mahaganpati Temple:</td>	
						<td>51 KM </td>								
						<td>1 h 23 min</td>
					</tr>


				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong> Pune-Morgaon-Siddhatek-Pali-Mahad-Theur-Lenyadri-Ozar-Ranjangaon-Moregaon-Pune
				<strong>Minimum Duration</strong>19 h 43 min
				<strong>Minimum Distance</strong>946 KM

				</div> 
				</div>
	
			</li>
			<li id="Morgaon"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 1 Hours 46 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 75.8</li>
					<li> Morgaon</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Moreshwar Temple (Morgaon)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>66.6 KM </td>								
						<td> 1 h 47 min</td>								
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>								
						<td> 66.3 KM</td>								
						<td> 1 h 18 min</td>							
					</tr>
				
					<tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>189 KM </td>								
						<td> 3 h 51 min</td>
					</tr>
                    
                    <tr>
						<td>From Varad Vinayak Ganpati Mandir:</td>	
						<td>155 KM </td>								
						<td> 3 h 2 min</td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>57.9 KM </td>								
						<td> 1 h 5 min</td>
					</tr>
                    <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>164 KM </td>								
						<td> 3 h 22 min</td>
					</tr>
                    <tr>
						<td>From Vighneshwar Temple:</td>	
						<td>140 KM </td>								
						<td> 3 h 3 min</td>
					</tr>
                     <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>70.2 KM </td>								
						<td> 1 h 34 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
					</div>
				
					
			</li>
			
			<li id="siddhatek"> 
					<ul class="route">
					<li> Morgaon</li>
					<li class="car"> 1 Hours 17 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 69.9</li>
					<li> Siddhatek</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Shree Siddhivinayak Temple (Siddhatek)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>105 KM </td>								
						<td> 2 h 5 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 69.9 KM</td>								
						<td> 1 h 17 min</td>							
					</tr>
				
					<tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>224 KM </td>								
						<td> 4 h 25 min</td>
					</tr>
                    
                    <tr>
						<td>From Varad Vinayak Ganpati Mandir:</td>	
						<td>190 KM </td>								
						<td> 3 h 36 min</td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>86.7 KM </td>								
						<td> 1 h 27 min</td>
					</tr>
                     <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>165 KM </td>								
						<td> 3 h 25 min</td>
					</tr>
                    <tr>
						<td>From Vighneshwar Temple:</td>	
						<td>151 KM </td>								
						<td> 3 h 9 min</td>
					</tr>
                          <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>81.2 KM </td>								
						<td> 1 h 43 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
								
			</li>
			
			<li id="pali"> 
					<ul class="route">
					<li> Siddhatek</li>
					<li class="car"> 4 Hours 51 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 227</li>
					<li> Pali</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Ballaleshwar Ganpati Temple (Pali)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>128 KM </td>								
						<td> 2 h 40 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 196 KM</td>								
						<td> 4 h 10 min</td>							
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>	
						<td>228 KM </td>								
						<td> 4 h 49 min</td>
					</tr>
                    
                    <tr>
						<td>From Varad Vinayak Ganpati Mandir:</td>	
						<td>44.5 KM </td>								
						<td> 1 h 18 min</td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>147 KM </td>								
						<td> 3 h 29 min</td>
					</tr>
                    <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>184 KM </td>								
						<td> 4 h 17 min</td>
					</tr>
                    <tr>
						<td>From Vighneshwar Temple:</td>	
						<td>174 KM </td>								
						<td> 4 h 4 min</td>
					</tr>
                    <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>163 KM </td>								
						<td> 3 h 38 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				
			</li>
			
			<li id="mahad">
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 0 Hours 59 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 37.6</li>
					<li> Mahad</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Varad Vinayak Ganpati Mandir(Mahad) </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>85.8 KM </td>								
						<td> 1 h 32 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 154 KM</td>								
						<td> 3 h 1 min</td>							
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>	
						<td>186 KM </td>								
						<td> 3 h 41 min</td>
					</tr>
                    
                    <tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>37.6 KM </td>								
						<td> 1 h </td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>105 KM </td>								
						<td> 2 h 21 min</td>
					</tr>
                    <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>142 KM </td>								
						<td> 3 h 8 min</td>
					</tr>
                    <tr>
						<td>From Vighneshwar Temple:</td>	
						<td>132 KM </td>								
						<td> 2 h 56 min</td>
					</tr>
                    <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>121 KM </td>								
						<td> 2 h 32 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			<li id="theur">
					<ul class="route">
					<li> Mahad</li>
					<li class="car"> 2 Hours 26 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 105</li>
					<li> Theur</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Shree Chintamani Temple (Theur)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>27.2 KM </td>								
						<td> 55 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 58 KM</td>								
						<td> 1 h 7 min</td>							
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>	
						<td>83.1 KM </td>								
						<td> 1 h 30 min</td>
					</tr>
                    
                    <tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>145 KM </td>								
						<td> 3 h 12 min </td>
					</tr>
                    <tr>
						<td>From  Varad Vinayak Ganpati Mandir:</td>	
						<td>107 KM </td>								
						<td> 2 h 22 min</td>
					</tr>
                    <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>108 KM </td>								
						<td> 2 h 42 min</td>
					</tr>
                    <tr>
						<td>From Vighneshwar Temple:</td>	
						<td>98 KM </td>								
						<td> 2 h 30 min</td>
					</tr>
                    <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>41.1 KM </td>								
						<td> 0 h 55 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>	
            
			<li id="lenyadri">
					<ul class="route">
					<li> Theur</li>
					<li class="car"> 2 Hours 45 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 108</li>
					<li> Lenyadri</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Girijatmaj Temple (Lenyadri)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>97 KM </td>								
						<td> 2 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 154 KM</td>								
						<td> 3 h 18 min</td>							
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>	
						<td>165 KM </td>								
						<td> 3 h 25 min</td>
					</tr>
                    
                    <tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>177 KM </td>								
						<td> 3 h 55 min </td>
					</tr>
                    <tr>
						<td>From  Varad Vinayak Ganpati Mandir:</td>	
						<td>143 KM </td>								
						<td> 3 h 9 min</td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>99.3 KM </td>								
						<td>2 h  35 min</td>
					</tr>
                    <tr>
						<td>From Vighneshwar Temple:</td>	
						<td>14.4 KM </td>								
						<td>0 h  28 min</td>
					</tr>
                    <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>85.3 KM </td>								
						<td> 2 h 2 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>	            
          <li id="ozar">
					<ul class="route">
					<li> Lenyadri</li>
					<li class="car"> 0 Hours 28 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 14.4</li>
					<li> Ozar</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>  Vighneshwar Temple (Ozar)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>87 KM </td>								
						<td> 2 h 16 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 143 KM</td>								
						<td> 3 h 2 min</td>							
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>	
						<td>151 KM </td>								
						<td> 3 h 9 min</td>
					</tr>
                    
                    <tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>167 KM </td>								
						<td> 3 h 42 min </td>
					</tr>
                    <tr>
						<td>From  Varad Vinayak Ganpati Mandir:</td>	
						<td>133 KM </td>								
						<td> 2 h 59 min</td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple:</td>	
						<td>89.3 KM </td>								
						<td> 2 h 23 min</td>
					</tr>
                    <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>14.4 KM </td>								
						<td> 0 h 28 min</td>
					</tr>
                    <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>73.8 KM </td>								
						<td> 1 h 46 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>  
			
<li id="ranjangaon">
					<ul class="route">
					<li> Lenyadri</li>
					<li class="car"> 1 Hours 45 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 73.4</li>
					<li> Ranjangaon</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Mahaganpati Temple (Ranjangaon)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>51.6 KM </td>								
						<td> 1 h 21 min</td>								
					</tr>
				
					<tr>
						<td>From Moreshwar Temple:</td>								
						<td> 69.83 KM</td>								
						<td> 1 h 32 min</td>							
					</tr>
				
					<tr>
						<td>From Shree Siddhivinayak Temple:</td>	
						<td>80.8 KM </td>								
						<td> 1 h 42 min</td>
					</tr>
                    
                    <tr>
						<td>From Ballaleshwar Ganpati Temple:</td>	
						<td>156 KM </td>								
						<td> 3 h 21 min </td>
					</tr>
                    <tr>
						<td>From  Varad Vinayak Ganpati Mandir:</td>	
						<td>122 KM </td>								
						<td> 2 h 36 min</td>
					</tr>
                    <tr>
						<td>From Shree Chintamani Temple::</td>	
						<td>41.6 KM </td>								
						<td> 56 min</td>
					</tr>
                    <tr>
						<td>From Girijatmaj Temple:</td>	
						<td>84.6 KM </td>								
						<td> 2 h 3 min</td>
					</tr>
                    <tr>
						<td>From Mahaganpati Temple:</td>	
						<td>73.4 KM </td>								
						<td> 1 h 47 min</td>
					</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>              


			<li id="morgaon">
					<ul class="route">
					<li> Lenyadri</li>
					<li class="car"> 1 Hours 33 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 69.9</li>
					<li> Morgaon</li>
					</ul>
				
				
				
			</li> 
			<li id="pune1">
					<ul class="route">
					<li> Lenyadri</li>
					<li class="car"> 1 Hours 48 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 75.8</li>
					<li> Pune</li>
					</ul>
				
				
				
			</li>                    

            
            		
		</ul>	
		
	</div>
	<!--<style>
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul#issues li{width:800px;}
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul li:before{display:none}
			div#timeline  ul#issues li{width:800px;text-align:center;overflow: hidden;}
			div#timeline ul li ul li {height: auto;}
	</style>-->
	
    		
<?php 

}
?>
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2oN6IZ8rywS4dflnIdWupkpOY2FLK0Bx';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
 <?php echo $footerBlock; ?>
		  </div>
		  </body>
		  </html>