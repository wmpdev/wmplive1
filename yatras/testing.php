<?php include'inner_head.php';?>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			V C</h2>
					<div class="row"></div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>
						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
							<p>The true essence of the greatness and auspiciousness of the Jyotirlingas is sung in the Puranas with utmost reverence. 
							All the sins or unacceptable actions are eliminated by reciting the name of Lord. For those who attain the Sadhaka level, calmness and purity become their characteristics.
							He gets illuminated and enlightened by indulging himself in the supreme knowledge that’s imparted. 
							There is no distinguishing feature as such, but it is rightly believed that a person can see these lingas as columns of fire piercing through the earth after he reaches a higher level of spiritual attainment.</p>
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul style="text-align:left">
								As per the mythological stories, the Shivapurana says that once there was an argument between Brahma and Vishnu regarding the supremacy of creation.  
								To resolve this, Shiva produced a test according to which all the three worlds got pierced and emerged as a huge endless pillar of light, the Jyotirlinga. </li>
								To search for the end of the lighted portion, both Brahma and Vishnu went upwards and downwards respectively. To shine out, Brahma lied to the lord that he had found the end and Vishnu put the reality forward. 
								Then lord Shiva emerged as a second pillar and cursed Brahma for lying and said that only Vishnu will be worshipped till the end of eternity! All the twelve Jyotirlingas have their own manifestations of Lord Shiva. 
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<!--<div class="tab-pane bounceInUp" id="skill4">
							<h2><i class="more-details"></i> Best Season for Yatra</h2>
							<p class="text-justify">Best months to travel are November to March and avoid April & May as it is too hot. One can travel in rainy 
season as this complete region is low rain belt. </p>
						</div>-->	
                        
                        <div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong>Trimbakeshwar </strong> Trimbakeshwar </li>
								<li><strong>Grishneshwar</strong> Aurangabad </li>
								<li><strong>Bhimashankar</strong> Bhimashankar </li>
								<li><strong>Somnath</strong> Somnath</li>
								
								</ul>
								
								<ul class="span3">
								<li><strong>Nageshvara Jyotirlinga</strong> Dwaraka </li>
								<li><strong>Mahakaleshwar</strong> Ujjain </li>
								<li><strong> Omkareshwar</strong> Omkareshwar </li>
								<li><strong>Uttar Pradesh</strong> Varanasi </li>
								
								</ul>
								<ul class="span3">
								<li><strong>Kedarnath</strong> Kedarnath </li>
								<li><strong>Vaidyanath Temple</strong> Deoghar </li>
								<li><strong>Mallikārjuna Swāmi</strong> Srisailam </li>
								<li><strong>Rameshwarm</strong> Rameshwaram</li>
								
								</ul>
						</div>	
                        
						<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Pune</li>
<li><span class="city">Trimbakeshwar Shiva Temple</span><span>239 kms </span><span>4h 58mins</span></li>
<li><span class="city">Grishneshwar Temple	</span><span>257 kms </span><span>4h 57mins</span></li>
<li><span class="city">Bhimashankar Temple</span><span>110kms </span><span>2h 36mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Rajkot</li>
<li><span class="city">Pune</span><span>968.2  kms </span><span>13h 04mins</span></li>
<li><span class="city">Somnath Temple	</span><span>227 kms </span><span>3h 37mins</span></li>
<li><span class="city">Nageshwara Jyotirlinga</span><span>221 kms </span><span>3h 54mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Indore</li>
<li><span class="city">Rajkot </span><span>592 kms </span><span>9h 29mins</span></li>
<li><span class="city">Mahakaleshwar Temple	</span><span>55.8 kms </span><span>3h 37mins</span></li>
<li><span class="city">Nageshwara Jyotirlinga</span><span>221 kms </span><span>3h 54mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Varanasi</li>
<li><span class="city">Indore</span><span>945.4 kms </span><span>15h 42mins</span></li>
<li><span class="city">Kashi Vishwanath Temple	</span><span>5.5 kms </span><span> 10mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Dehradun</li>
<li><span class="city">Varanasi</span><span>891 kms </span><span>12h 54mins</span></li>
<li><span class="city">Kedarnath Temple	</span><span>239  kms </span><span>4h 8mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Patna</li>
<li><span class="city">Dehradun</span><span>1140  kms </span><span>16h 33mins</span></li>
<li><span class="city">Vaidyanath Temple	</span><span>299 kms </span><span>5h 8mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Hyderabad</li>
<li><span class="city">Patna</span><span>1479  kms </span><span>25h 0mins</span></li>
<li><span class="city">Srisailam Devasthan (Mallikarjuna Temple)	</span><span>173 kms </span><span>3h 50mins</span></li>
</ul>
</div>
<hr/>

<!--Mobile content-->


<?php

}
else
{
?>

<!--Desktop content-->
				
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#pune">Pune</a></li>
			<li><a href="#rajkot">Rajkot</a></li>
			<li><a href="#indore">Indore</a></li>
			<li><a href="#varanasi">Varanasi</a></li>
			<li><a href="#dehradun">Dehradun</a></li>
			<li><a href="#patna">Patna</a></li>      
			<li><a href="#hyderabad">Hyderabad</a></li>   
    		<li><a href="#madurai">Madurai</a></li>                        
		</ul>
		<ul id="issues">
			<li id="pune">
					<ul class="route">
					<li> -</li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 0</li>
					<li> Pune</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar:</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar:</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grishneshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>15 h 20 min
				<strong>Minimum Distance</strong>819 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Grishneshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>257 KM </td>								
						<td> 4 h 57 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar:</td>								
						<td> 294 KM</td>								
						<td> 4 h 51 min</td>							
					</tr>
				
					<tr>
						<td>From Trimbakeshwar:</td>	
						<td>215 KM </td>								
						<td> 3 h 44 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Bhimashankar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Pune:</td>								
						<td>110 KM </td>								
						<td> 2 h 36 min</td>								
					</tr>
				
					<tr>
						<td>From Trimbakeshwar:</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar:</td>	
						<td>294 KM </td>								
						<td> 4 h 57 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				
			</li>
			<li id="rajkot"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 13 Hours</li>
					<li class="train"> 17 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 968.20</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Somnath Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Rajkot:</td>								
						<td>227 KM </td>								
						<td> 3 h 37 min</td>								
					</tr>
				
					<tr>
						<td>From Nageshvara Jyotirlinga:</td>								
						<td> 250 KM</td>								
						<td> 4 h 24 min</td>							
					</tr>
				
					<!--<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
					<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Rajkot-Nageshwar-Somnath-Rajkot
				<strong>Minimum Duration</strong>15 h 06 min
				<strong>Minimum Distance</strong>896 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Nageshvara Jyotirlinga Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Rajkot:</td>								
						<td>221 KM </td>								
						<td> 3 h 54 min</td>								
					</tr>
				
					<tr>
						<td>From Somnath Temple:</td>								
						<td> 250 KM</td>								
						<td> 4 h 24 min</td>							
					</tr>
				
					<!--<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>-->
					
			</li>
			
			<li id="indore"> 
					<ul class="route">
					<li> Rajkot</li>
					<li class="car"> 10 Hours</li>
					<li class="train"> 15 Hours</li>
					<li  class="flight"> 03 Hours</li>
					<li class="km"> 592</li>
					<li> Indore</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Mahakaleshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Indore:</td>								
						<td>56 KM </td>								
						<td> 0 h 55 min</td>								
					</tr>
				
					<tr>
						<td>From Omkareshwar:</td>								
						<td> 134 KM</td>								
						<td> 2 h 23 min</td>							
					</tr>
				
					<!--<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Indore-Omkareshwar-Mahakaleshwar-Indore
				<strong>Minimum Duration</strong>4 h 45 min
				<strong>Minimum Distance</strong>267 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Omkareshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Indore:</td>								
						<td>78 KM </td>								
						<td> 1 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Mahakaleshwar:</td>								
						<td> 134 KM</td>								
						<td> 2 h 23 min</td>							
					</tr>
				
					<!--<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>-->
				
			</li>
			
			<li id="varanasi"> 
					<ul class="route">
					<li> Indore</li>
					<li class="car"> 16 Hours</li>
					<li class="train"> 21 Hours</li>
					<li  class="flight"> 03 Hours</li>
					<li class="km"> 945.40</li>
					<li> Varanasi</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Kashi Vishwanath Temple</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Varanasi:</td>								
						<td>5.50 KM </td>								
						<td> 10 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Varanasi-Kashi Vishwanath Temple-Varanasi
				<strong>Minimum Duration</strong>0 h 21 min
				<strong>Minimum Distance</strong>11 KM

				</div> 
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>-->
				
			</li>
			
			<li id="dehradun">
					<ul class="route">
					<li> Varanasi</li>
					<li class="car"> 13 Hours</li>
					<li class="train"> 21 Hours</li>
					<li  class="flight"> 03 Hours</li>
					<li class="km"> 891</li>
					<li> Dehradun</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Kedarnath Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Dehradun:</td>								
						<td>239 KM </td>								
						<td> 4 h 08 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Dehradun-Kedarnath-Dehradun
				<strong>Minimum Duration</strong>9 h 27 min
				<strong>Minimum Distance</strong>507 KM

				</div> 
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>-->
				
			</li>
<!--		Patna	-->
            <li id="patna">
					<ul class="route">
					<li> Dehradun</li>
					<li class="car"> 17 Hours</li>
					<li class="train"> 20 Hours</li>
					<li  class="flight"> 03 Hours</li>
					<li class="km"> 1140</li>
					<li> Patna</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Vaidyanath Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Patna:</td>								
						<td>299 KM </td>								
						<td> 5 h 08 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Patna-Deogragh-Patna
				<strong>Minimum Duration</strong>9 h 00 min
				<strong>Minimum Distance</strong>492 KM

				</div> 
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>-->
				
			</li>
<!--			Patna end-->	
<!--		Hydrabad	-->
            <li id="hyderabad">
					<ul class="route">
					<li> Patna</li>
					<li class="car"> 25 Hours</li>
					<li class="train"> 34 Hours</li>
					<li  class="flight"> 04 Hours</li>
					<li class="km"> 1479</li>
					<li> Hyderabad</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Mallikārjuna Swāmi Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Hyderabad:</td>								
						<td>173 KM </td>								
						<td> 3 h 50 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Hyderabad-Srisalim-Hyderabad
				<strong>Minimum Duration</strong>8 h 00 min
				<strong>Minimum Distance</strong>425 KM

				</div> 
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>-->
				
			</li>
<!--			Hydrabad end-->	

<!--		Madurai	-->
            <li id="madurai">
					<ul class="route">
					<li> Hyderabad</li>
					<li class="car"> 15 Hours</li>
					<li class="train"> 15 Hours</li>
					<li  class="flight"> 03 Hours</li>
					<li class="km"> 1003</li>
					<li> Madurai</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Rameshwarm Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Madurai:</td>								
						<td>173 KM </td>								
						<td> 3 h 07 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Madurai-Rameshwaram-Madurai
				<strong>Minimum Duration</strong>6 h 15 min
				<strong>Minimum Distance</strong>337 KM

				</div> 
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grishneshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>-->
				
			</li>
<!--			Madurai end-->		
		</ul>	
		
	</div>
	
<!--Desktop content-->
	
<?php 

}
?>
						
		
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  
		  <?php echo $footerBlock; ?>
		  </div>
		  </body>
		  </html>