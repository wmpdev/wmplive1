<?php
	
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
	
umask(0);
Mage::app();
Mage::getSingleton('core/session', array('name'=>'frontend'));
	
$session = Mage::getSingleton('customer/session');
if($session->isLoggedIn()) 
{
		$userId = $session->getCustomer()->getId();
} else {
		$userId = 0;
}

$block              = Mage::getSingleton('core/layout');
	
	
$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
 

?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Yatras - Significance, History, Itinerary, Nearby Temple</title>
<meta name="description" content="Get complete guidance on yatras in India with it's significance, history, itinerary, nearby temples, total distance, best season to visit, etc." />
<meta name="keywords" content="yatras in india, yatras" />

<link rel="icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link rel="shortcut icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="inner_css/home.css" type="text/css" rel="stylesheet">
<!-- <link href="inner_css/styles.css" type="text/css" rel="stylesheet"> -->

<link href="style.css" type="text/css" rel="stylesheet">
<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
 <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <!-- <link href="style.css" rel="stylesheet" media="screen"> -->
    <link href="https://www.wheresmypandit.com/skin/frontend/wmp/default/css/wmplatest/css/main.css" rel="stylesheet" media="screen">
    <!-- <link href="css/options.css" rel="stylesheet" media="screen"> -->
    <link href="css/custom.css" rel="stylesheet" media="screen">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
    <link href="css/main.css" rel="stylesheet" media="screen">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/thumbelina.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<link href="js/jquery-ui.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/customslide.js"></script>
<script type="text/javascript" src="js/jssor.js"></script>
<script type="text/javascript" src="js/jssor.slider.js"></script>
 <link href="css/hor-timeline.css" rel="stylesheet" media="screen">
<script src="js/jquery.timelinr-0.9.54.js"></script> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

<link rel="canonical" href="https://wheresmypandit.com/yatras" />

</head>

<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<link href="https://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="js/yatras/jyotirlinga.js"></script>
	<script type="text/javascript" src="js/yatras/ashtavinayak.js"></script>
	<script type="text/javascript" src="js/yatras/chardham.js"></script>
    <script type="text/javascript" src="js/yatras/dodham.js"></script>
    <script type="text/javascript" src="js/yatras/chotachardham.js"></script>
    <script type="text/javascript" src="js/yatras/kashi.js"></script>
    <script type="text/javascript" src="js/yatras/navgrah.js"></script>    
    <script type="text/javascript" src="js/yatras/puri.js"></script>        
    <script type="text/javascript" src="js/yatras/saptapuri.js"></script>      
    <script type="text/javascript" src="js/yatras/teendham.js"></script> 
    <script type="text/javascript" src="js/yatras/maruti.js"></script>     
                   
<body onload="initialize()">
  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="big   with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  	
	  		
	  		<!-- TOP NAVIGATION
	  		<div id="top-navigation">
		  		<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
			  		<div class="summary">
        	
         	<h2 class="classy f-left"><span>Shopping Cart -</span>
                        	<a href="#">0</a><span class="Itext"> item</span>                                    </h2>
           
        </div>
		  		</ul>
	  		</div>
	  	
	  		<!-- MOBILE NAVIGATION -->
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<!--<a href="index.php" id="logo-navigation"></a>-->
				<a href="https://www.wheresmypandit.com" id="logo-navigation"></a>
                    
		  		</div>
	</nav>
	  		<!-- TEXT SLIDER -->
			
	  		<!-- HEADER SLIDER -->
		  	<div class="" id="header-slider">
		  	
	  		
	  			<!--PORTFOLIO -->
	  		<!--<h2 class="with-breaker animate-me fadeInUp animated" style="visibility: visible; -webkit-animation: fadeInUp;">
		  		Yatras <span>At least once in lifetime, One must do..</span>
	  			</h2>-->
	  			<!--<div id="portfolio-filters" class="animate-me fadeIn animated animated" style="visibility: visible; -webkit-animation: fadeIn;">
	  				
					<button class="btn btn-default" data-filter=".common-all"><i class="fa fa-arrows"></i> All</button>
					<button type="submit" class="btn btn-default" data-filter=".west"><i class="fa fa-arrows-h"></i> West</button>
					<button class="btn btn-default" data-filter=".north"><i class="fa fa-arrows-v"></i>North</button>
					<button class="btn btn-default" data-filter=".east"><i class="fa fa-arrows-h"></i>East</button>
					<button class="btn btn-default" data-filter=".south"><i class="fa fa-arrows-v"></i> South</button>
	  			</div>-->
		
				<div class="yatra-map">
				<div id="map_canvas"></div>
		
				</div></div>
						<div id="control_panel">
					<div>
					<table class="tbk-ytra" border="1">
					<tr>
                                        <h1><td class="tbl-list" colspan="2" align="centre">List of Yatras</td></h1>
					</tr>
					<tr class="1">
						<td class="tbl-color"><a onclick="calcRoute1();"><img src="images/yatra/1.png" alt="12 Jyotirlinga Yatra" />Show Route</a></td>
						<td class="tbl-place"><a href="12-jyotirlinga.php">12 Jyotirlinga Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/2.png" alt="Ashtavinayak Yatra" />Show Route</a></td>
						<td class="tbl-place"><a href="ashtavinayak.php">Ashtavinayak Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute3();"><img src="images/yatra/3.png" alt="Char Dham Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="char-dham.php">Char Dham Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute4();"><img src="images/yatra/4.png" alt="Do Dham Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="do-dham.php">Do Dham Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute5();"><img src="images/yatra/5.png" alt="Chota Char Dham Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="chota-char-dham.php">Chota Char Dham Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute6();"><img src="images/yatra/6.png" alt="Kashi Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="kashi.php">Kashi Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute7();"><img src="images/yatra/7.png" alt="Navgraha Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="navagraha-temples.php">Navgraha Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute8();"><img src="images/yatra/8.png" alt="Puri Rath Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="jagannath-puri-rath.php">Puri Rath Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute9();"><img src="images/yatra/9.png" alt="Sapta Puri Yatra" />Show Route</a></td>
						<td class="tbl-place"><a  href="sapta-puri.php">Sapta Puri Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute10();"><img src="images/yatra/10.png" alt="Teen Dham Yatra" />Show Route</a></td>
                        <td class="tbl-place"><a  href="teen-dham.php">Teen Dham Yatra</a></td>
					</tr>
                    
						<td class="tbl-color"><a onclick="calcRoute11();"><img src="images/yatra/11.png" alt="Maruti Temples Yatra" />Show Route</a></td>
                        <td class="tbl-place"><a  href="maruti-temples.php">11-Maruti Temples Yatra</a></td>
					</tr>
					<!--<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/11.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="amarnath.php">Amarnath Yatra</a></td>
					</tr>-->
					<!--<tr class="2">
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/12.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="vaisnodevi.php">Vaishnov Devi Yatra</a></td>
					</tr>-->
					</table>
					
					</div>
					
				</div>
		  	
			<div id="shade"></div>
		  	<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
		  	<!--<div id="header-video"
			    data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>-->
	  		
	  	</header>
	  	<!-- END HEADER -->
		
	
	
		
		<?php echo $footerBlock; ?>
	  	
	  	<!-- SCROLL TOP -->
	  	<a href="#" id="scroll-top" class="fadeInRight animate-me"><i class="fa fa-angle-double-up"></i></a>
  	</div>

    <!-- SCRIPTS -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/plugins.js"></script>
	<script type="text/javascript">
		/*TEXT TICKER (ONLY FOR HOME PAGE)*/
		$('#ticker-text').vTicker('init', {
			speed: 300, 
		    pause: 2000
	    });
	</script>
	<!-- ONLY DEMO -->
	
    <script src="js/custom.js"></script>
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/yatras/sample.js"></script>

<script>
function initialize()
{
	var mapOptions = {
       center: new google.maps.LatLng(20.1289956,78.7792201),
         zoom: 5,
        // mapTypeId: google.maps.MapTypeId.ROADMAP
     };
map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
directionsDisplay.setMap(map);
}
</script>	

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2oN6IZ8rywS4dflnIdWupkpOY2FLK0Bx';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
	</body>
  <!-- END BODY -->
</html>
