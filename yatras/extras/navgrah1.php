
<!DOCTYPE html>
<?php include'inner_head.php';?>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			Navagraha Temples
	  			</h2>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/ng1.png" />
                <img u="thumb" src="images/slider/ng1-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/ng2.png" />
                <img u="thumb" src="images/slider/ng2-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/ng3.png" />
                <img u="thumb" src="images/slider/ng3-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/ng4.png" />
                <img u="thumb" src="images/slider/ng4-tn.png" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>The Navagraha Temples are those that are devoted to the nine celestials  bodies and hence the name &lsquo;Navagraha&rsquo;.</p>
				 <p>In Tamil Nadu, there are nine such temples located at different places  that represent each of these nine celestial bodies according to the Hindu Astrology.</p>
				 <p> The uniqueness about the location of these temples is that all of them are  located within a radius of 65 kilometers around Kumbhakonam. </p>
				 <p></p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
					<li><strong>Travel Plan</strong> : Navagraha Temples,    </li><br/>
					<li><strong>Minimum Duration</strong> : 15 Days,  </li><br/>
					<!--<li><strong>Day 3 </strong>: CHINTAMANI , GIRIJMATMAJ  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
					<li><strong>Best Season To Visit</strong> : Any time 2-3 Days.  </li>
				</ul><br/><br/>
					
				 <a href="images/navgrah.jpg"><img class="img-map" src="images/navgrah.jpg" width="400" height="450"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>

						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>It is a popularly known belief that whoever makes a trip to all of these  temples and offers devotion and worship with utmost reverence attains relief  from all kinds of suffering and progresses really high in his life. </p> 
                            <p>To understand the real significance, the importance of the grahas should  also be known. According to Astrology, the nine grahas have a lot to do with a  person&rsquo;s fortune or misfortune.</p> 
                            <p>A pilgrimage to the Navagraha temples are largely undertaken to mitigate  the negative effects of a planet on the individual.</p>
                            <p>&nbsp;</p>
							
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul>
								<li>There are many historical records that signify the importance of the Navagrahas. According to the astrological treatise Prasna Marga, these garahas were a result of the anger of Lord Shiva.</li>
								<li>One more book called Puranic Encyclopedia, it is mentioned that certain grahas induce illnesses in children. Numberless other historical mentions prevail about the importance of grahas. </li>
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong>Suriyanaar Temple</strong> Thanjavur  </li>
								<li><strong>Naganatha Swami temple</strong> Thirunageswaram </li>
								<li><strong>Swetharanyeswarar Temple</strong> Thiruvengadu  </li>

								
								</ul>
								
								<ul class="span3">
								<li><strong>Chandra Navagrahastalam Temple</strong> Thingaloor  </li>
								<li><strong>Arulmighu Agneeswarar Temple</strong> Kanjanoor </li>
								<li><strong>Arulmigu Naganathaswamy Temple or Arulmigu Kethu Sthalam</strong> Keezhaperumpallam </li>

								
								</ul>
								<ul class="span3">
								<li><strong>Sri Aabhatsakay Eswarar</strong> Alangudi </li>
								<li><strong>Vaitheeswaran Temple or Pullirukkuvelur</strong> Vaitheeswarankovil  </li>
								<li><strong>Tirunallar Saniswaran Temple or Dharbaranyeswarar Temple</strong> Thirunallar  </li>

								
								</ul>
                               
						</div>	
						<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Trichy</li>
<li><span class="city">Sri Aabhatsakay Eswarar Temple (Alangudi)</span><span>67.2 kms </span><span>1h 20mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Suriyanaar Temple (Thanjavur)</li>
<li><span class="city">Alangudi</span><span>57.9  kms </span><span>11h 17mins</span></li>
<li><span class="city">Trichy</span><span>59.6 kms </span><span>1h 06mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Chandra Navagrahastalam Temple (Thingaloor)</li>
<li><span class="city">Thanjavur </span><span>15.9 kms </span><span>0h 26mins</span></li>
<li><span class="city">Trichy</span><span>60.1  kms </span><span>1h 16mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Naganatha Swami temple (Thirunageswaram)</li>
<li><span class="city"> Thingaloor</span><span>38.9 kms </span><span>0h 54mins</span></li>
<li><span class="city">Trichy	</span><span>110 kms </span><span>1h 58mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Arulmighu Agneeswarar Temple (Kanjanoor)</li>
<li><span class="city">Thirunageswaram</span><span>13.3  kms </span><span>0h 19mins</span></li>
<li><span class="city">Trichy	</span><span>119  kms </span><span>2h 11mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Vaitheeswaran Temple or Pullirukkuvelur  (Vaitheeswarankovil)</li>
<li><span class="city">Kanjanoor </span><span>34.6  kms </span><span>0h 43mins</span></li>
<li><span class="city">Trichy</span><span>153 kms </span><span>2h 51mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Swetharanyeswarar Temple (Thiruvengadu)</li>
<li><span class="city">Vaitheeswarankovil</span><span>44.5  kms </span><span>0h 59mins</span></li>
<li><span class="city">Trichy	</span><span>163 kms </span><span>3h 05mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Arulmigu Naganathaswamy Temple or Arulmigu Kethu Sthalam (Keezhaperumpallam)</li>
<li><span class="city">Thiruvengadu </span><span>8.5 kms </span><span>0h 16mins</span></li>
<li><span class="city">Trichy	</span><span>165 kms </span><span>3h 03mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Tirunallar Saniswaran Temple or Dharbaranyeswarar Temple (Thirunallar)</li>
<li><span class="city">Keezhaperumpallam </span><span>309 kms </span><span>5h 35mins</span></li>
<li><span class="city">Trichy	</span><span>165 kms </span><span>3h 34mins</span></li>
</ul>
</div>
<hr/>


<!--Mobile content end-->


<?php

}
else
{
?>

<!--Desktop content-->
						
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#trichy">Trichy</a></li>
			<li><a href="#alangudi">Alangudi</a></li>
			<li><a href="#thanjavur">Thanjavur</a></li>
			<li><a href="#thingaloor"> Thingaloor</a></li>
			<li><a href="#thirunageswaram"> Thirunageswaram</a></li>
            <li><a href="#kanjanoor"> Kanjanoor </a></li>
            <li><a href="#vaitheeswarankovil"> Vaitheeswarankovil </a></li>
            <li><a href="#thiruvengadu "> Thiruvengadu  </a></li>
            <li><a href="#keezhaperumpallam"> Keezhaperumpallam </a></li>
            <li><a href="#thirunallar"> Thirunallar </a></li>   
            <li><a href="#thirunallar1"> Trichy </a></li>   

		</ul>
		<ul id="issues">
			<li id="pune">
					<ul class="route">
					<li> - </li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 0</li>
					<li> Trichy </li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trichy  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Suriyanaar Temple:</td>								
						<td>59 KM </td>								
						<td>1 h 06 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>60 KM </td>								
						<td>1 h 16 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>67 KM </td>								
						<td>1 h 20 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>110 KM </td>								
						<td>1 h 58 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>119 KM </td>								
						<td>2 h 11 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>153 KM </td>								
						<td>2 h 51 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>163 KM </td>								
						<td>3 h 5 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>166 KM </td>								
						<td>3 h 3 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>195 KM </td>								
						<td>3 h 34 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>
				
                

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Tiruchirapalli International Airport-Alangudi-Thanjavur-Thingaloor-Thirunageswaram-Kanjanoor-Vaitheeswarankovil-Thiruvengadu-Keezhaperumpallam-Thirunallar-Tiruchirapalli International Airport
				<strong>Minimum Duration</strong>14 h 45 min
				<strong>Minimum Distance</strong>759 KM

				</div> 
				</div>
				
				
				
			</li>
			<li id="alangudi"> 
					<ul class="route">
					<li> Trichy</li>
					<li class="car"> 1.30 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 68</li>
					<li> Alangudi</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Sri Aabhatsakay Eswarar Temple (Alangudi)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Trichy:</td>								
						<td>67.2  KM </td>								
						<td>1 h 20 min</td>								
					</tr>
                
                    <tr>
						<td>From Suriyanaar Temple:</td>								
						<td>57.9 KM </td>								
						<td>1 h 17 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>78.7 KM </td>								
						<td>1 h 39 min</td>								
					</tr>
                   <!-- <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>67 KM </td>								
						<td>1 h 20 min</td>								
					</tr>-->
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>115 KM </td>								
						<td>2 h 2 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>128 KM </td>								
						<td>2 h 21 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>157 KM </td>								
						<td>2 h 56 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>167 KM </td>								
						<td>3 h 9 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>170 KM </td>								
						<td>3 h 6 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>169 KM </td>								
						<td>2 h 59 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>
				

				</div>	</div>
					
			</li>
			
			<li id="thanjavur"> 
					<ul class="route">
					<li> Alangudi</li>
					<li class="car"> 1 Hours 17 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 00 Hours</li>
					<li class="km"> 57.9</li>
					<li> Thanjavur</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Suriyanaar Temple (Thanjavur)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
                    <tr>
						<td>From Trichy::</td>								
						<td>59.6 KM </td>								
						<td>1 h 06 min</td>								
					</tr>
                      
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>15.9 KM </td>								
						<td>00h 26 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>58 KM </td>								
						<td>1 h 16 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>44.9 KM </td>								
						<td>1 h 2 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>55.5 KM </td>								
						<td>1 h 12 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>92 KM </td>								
						<td>1 h 53 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>99.5 KM </td>								
						<td>2 h 9 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>103 KM </td>								
						<td>2 h 6 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>213 KM </td>								
						<td>3 h 51 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			
			<li id="thingaloor"> 
					<ul class="route">
					<li> Thanjavur</li>
					<li class="car"> 00 Hours</li>
					<li class="train"> 00 Hours</li>
					<li  class="flight"> 00 Hours</li>
					<li class="km"> 15.9</li>
					<li> Thingaloor</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Chandra Navagrahastalam Temple (Thingaloor)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Suriyanaar Temple:</td>								
						<td>15.9 KM </td>								
						<td>0 h 25 min</td>								
					</tr>
                       
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>78.6 KM </td>								
						<td>1 h 35 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>38.9 KM </td>								
						<td>0 h 54 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>47.5 KM </td>								
						<td>1 h 2 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>84 KM </td>								
						<td>1 h 44 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>915 KM </td>								
						<td>2 h 0 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>94.9 KM </td>								
						<td>1 h 57 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>233 KM </td>								
						<td>4 h 10 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>
				

				</div>
					</div>

				
			</li>
			
			<li id="thirunageswaram">
					<ul class="route">
					<li> Thingaloor</li>
					<li class="car"> 0 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 38.9</li>
					<li> Thirunageswaram</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Naganatha Swami temple (Thirunageswaram)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Trichy Temple:</td>								
						<td>110 KM </td>								
						<td>1 h 58 min</td>								
					</tr>
                    <tr>
						<td>From Suriyanaar Temple:</td>								
						<td>45 KM </td>								
						<td>1 h 1 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>38.9 KM </td>								
						<td>0 h 54 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>19.6 KM </td>								
						<td>0 h 24 min</td>								
					</tr>
                    
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>13.3 KM </td>								
						<td>0 h 19 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>45.3 KM </td>								
						<td>0 h 56 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>55.3 KM </td>								
						<td>1 h 12 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>58.6 KM </td>								
						<td>1 h 9 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>248 KM </td>								
						<td>4 h 37 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>

				</div>
				
				</div>
			</li>

			<li id="kanjanoor">
					<ul class="route">
					<li> Thirunageswaram </li>
					<li class="car"> 19 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 13.03</li>
					<li> Kanjanoor</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Arulmighu Agneeswarar Temple (Kanjanoor)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
                    <tr>
						<td>From Trichy Temple:</td>								
						<td>119 KM </td>								
						<td>2 h 11 min</td>								
					</tr>
					<tr>
						<td>From Suriyanaar Temple:</td>								
						<td>56.2 KM </td>								
						<td>1 h 13 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>47.5 KM </td>								
						<td>1 h 4 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>32.9 KM </td>								
						<td>0 h 45 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>13.3 KM </td>								
						<td>0 h 21 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>34.5 KM </td>								
						<td>0 h 43 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>44.5 KM </td>								
						<td>0 h 59 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>47.8 KM </td>								
						<td>0 h 56 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>272 KM </td>								
						<td>4 h 56 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>

				</div>
				
				</div>
			</li>
<!---------->		
			<li id="vaitheeswarankovil">
					<ul class="route">
					<li> Kanjanoor  </li>
					<li class="car"> 43 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 34.06</li>
					<li> Vaitheeswarankovil</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Vaitheeswaran Temple or Pullirukkuvelur  (Vaitheeswarankovil) </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
                    <tr>
						<td>From Trichy Temple:</td>								
						<td>153 KM </td>								
						<td>2 h 51 min</td>								
					</tr>
					<tr>
						<td>From Suriyanaar Temple:</td>								
						<td>90.2 KM </td>								
						<td>1 h 53 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>815 KM </td>								
						<td>1 h 44 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>64.9 KM </td>								
						<td>1 h 21 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>45.3 KM </td>								
						<td>0 h 57 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>34.8 KM </td>								
						<td>0 h 45 min</td>								
					</tr>
                    
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>15.2 KM </td>								
						<td>0 h 22 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>19.8 KM </td>								
						<td>0 h 29 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>296 KM </td>								
						<td>5 h 25 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>

				</div>
				
				</div>
			</li>
<!--------->       

<li id="thiruvengadu">
					<ul class="route">
					<li> Vaitheeswarankovil </li>
					<li class="car"> 59 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 44.05</li>
					<li> Thiruvengadu</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Swetharanyeswarar Temple (Thiruvengadu) </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Trichy Temple:</td>								
						<td>163 KM </td>								
						<td>3 h 05 min</td>								
					</tr>
                    <tr>
						<td>From Suriyanaar Temple:</td>								
						<td>100 KM </td>								
						<td>2 h 07 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>915 KM </td>								
						<td>1 h 58 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>74.9 KM </td>								
						<td>1 h 36 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>55.3 KM </td>								
						<td>1 h 12 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>44.9 KM </td>								
						<td>0 h 59 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>15.2 KM </td>								
						<td>0 h 22 min</td>								
					</tr> 
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>8.5 KM </td>								
						<td>0 h 16 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>306 KM </td>								
						<td>5 h 37 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>

				</div>
				
				</div>
			</li>
            
<!-------->    

<li id="keezhaperumpallam">
					<ul class="route">
					<li> Thiruvengadu  </li>
					<li class="car"> 0 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 8.5</li>
					<li> Keezhaperumpallam</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Arulmigu Naganathaswamy Temple or Arulmigu Kethu Sthalam (Keezhaperumpallam)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Trichy Temple:</td>								
						<td>166 KM </td>								
						<td>3 h 3 min</td>								
					</tr>
                    
                    <tr>
						<td>From Suriyanaar Temple:</td>								
						<td>104 KM </td>								
						<td>2 h 5 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>94.9 KM </td>								
						<td>1 h 56 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>78.2 KM </td>								
						<td>1 h 34 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>58.6 KM </td>								
						<td>1 h 10 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>48.2 KM </td>								
						<td>0 h 57 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>19.8 KM </td>								
						<td>0 h 30 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>8.5 KM </td>								
						<td>0 h 15 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>309 KM </td>								
						<td>5 h 35 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>

				</div>
				
				</div>
			</li>
<!--------->      

<li id="thirunallar ">
					<ul class="route">
					<li> Keezhaperumpallam </li>
					<li class="car"> 5 Hour 35 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 309</li>
					<li> Thirunallar </li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Tirunallar Saniswaran Temple or Dharbaranyeswarar Temple (Thirunallar)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Trichy Temple:</td>								
						<td>195 KM </td>								
						<td>3 h 34 min</td>								
					</tr>
                    <tr>
						<td>From Suriyanaar Temple:</td>								
						<td>215 KM </td>								
						<td>3 h 52 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>235 KM </td>								
						<td>4 h 15 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>170 KM </td>								
						<td>3 h 1 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>245 KM </td>								
						<td>4 h 38 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>259 KM </td>								
						<td>4 h 57 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>295 KM </td>								
						<td>5 h 26 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>304 KM </td>								
						<td>5 h 39 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>307 KM </td>								
						<td>5 h 36 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>195 KM </td>								
						<td>3 h 34 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>

				</div>
				
				</div>
			</li>
            
<!--------->            
       <li id="thirunallar1">
					<ul class="route">
					<li> Thirunallar </li>
					<li class="car"> 0 Hour 35 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 195</li>
					<li> Trichy </li>
					</ul>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	-->
				
				<!--<table class=" ">
				<thead><tr><h5>Tirunallar Saniswaran Temple or Dharbaranyeswarar Temple (Thirunallar)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Trichy Temple:</td>								
						<td>195 KM </td>								
						<td>3 h 34 min</td>								
					</tr>
                    <tr>
						<td>From Suriyanaar Temple:</td>								
						<td>215 KM </td>								
						<td>3 h 52 min</td>								
					</tr>
                       
					<tr>
						<td>From Chandra Navagrahastalam Temple:</td>								
						<td>235 KM </td>								
						<td>4 h 15 min</td>								
					</tr>
                    <tr>
						<td>From Sri Aabhatsakay Eswarar:</td>								
						<td>170 KM </td>								
						<td>3 h 1 min</td>								
					</tr>
                    
                      <tr>
						<td>From Naganatha Swami temple:</td>								
						<td>245 KM </td>								
						<td>4 h 38 min</td>								
					</tr>
                    <tr>
						<td>From Arulmighu Agneeswarar Temple:</td>								
						<td>259 KM </td>								
						<td>4 h 57 min</td>								
					</tr>
                    
                      <tr>
						<td>From Vaitheeswaran Temple or Pullirukkuvelur :</td>								
						<td>295 KM </td>								
						<td>5 h 26 min</td>								
					</tr> 
                    <tr>
						<td>From Swetharanyeswarar Temple:</td>								
						<td>304 KM </td>								
						<td>5 h 39 min</td>								
					</tr>
                    <tr>
						<td>From Arulmigu Naganathaswamy Temple or <br>
							Arulmigu Kethu Sthalam:</td>								
						<td>307 KM </td>								
						<td>5 h 36 min</td>								
					</tr>
                    
                      <tr>
						<td>From Tirunallar Saniswaran Temple or <br>
							Dharbaranyeswarar Temple:</td>								
						<td>195 KM </td>								
						<td>3 h 34 min</td>								
					</tr> 
                    
					
				</tbody>
				</table>-->

			<!--	</div>
				
				</div>-->
			</li>
            
<!--------->         
            
            
            



					
		</ul>	
		
	</div>
<!--	<style>
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul#issues li{width:800px;}
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul li:before{display:none}
			div#timeline  ul#issues li{width:800px;text-align:center;overflow: hidden;}
			div#timeline ul li ul li {height: auto;}
	</style>
	-->
    
<!--Desktop content end-->
	
<?php 

}
?>
		
    
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  
		  <?php include'footer.php' ?>
		  </div>
		  </body>
		  </html>