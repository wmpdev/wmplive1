
<!DOCTYPE html>
<?php include'inner_head.php';?>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			Chaar Dhaam Yatra
	  			</h2>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/cd1.png" />
                <img u="thumb" src="images/slider/cd1-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/cd2.png" />
                <img u="thumb" src="images/slider/cd2-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/cd3.png" />
                <img u="thumb" src="images/slider/cd3-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/cd4.png" />
                <img u="thumb" src="images/slider/cd4-tn.png" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>The four heavenly abodes are known as Chaar Dhaam. These are the four  pilgrimage sites in India- Badrinath, Dwarka, Puri and Rameswaram. </p>
				 <p>These are greatly revered by Hindus. They are considered highly sacred  by the Hindu devotees. One should surely go on a Chaar Dhaam Yatra once in his  entire lifetime. </p>
				<p>As  defined by Aadi Shankaracharya, a Chaar Dhaam consists of three Vaishnavite and  one Shaivite pilgrimages.</p>
<p></p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
					<li><strong>Travel Plan</strong> : Chaar Dhaam,    </li><br/>
					<li><strong>Minimum Duration</strong> : 15 Days,  </li><br/>
					<!--<li><strong>Day 3 </strong>: CHINTAMANI , GIRIJMATMAJ  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
					<li><strong>Best Season To Visit</strong> : May to Oct.  </li>
				</ul><br/><br/>
					
				 <a href="images/chardham.jpg"><img class="img-map" src="images/chardham.jpg" width="400" height="450"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->

						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>
						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>To perform austerities and penance in order to attain salvation and  ultimate happiness, people undertake this tour on a really large scale.</p> 
                            <p>These holy and sacred pilgrimages are a must.</p> 
                            <p>The term Chaar Dhaam was coined by the great Guru Aadi Shankaracharya  which means the four abodes of God. It is known that the Lord resides here eternally.</p>
                            						
					  </div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul>
								<li>Badrinath became prominent when when Nar-Narayan performed penance here. This took place in the ‘Satyuga’ and hence it is the first Dhaam. In ‘Tretayuga’, Rameshwaram gained importance.</li>
								<li>It is believed that Lord Rama built a Shivlinga here and worshipped it faithfully. ‘Dwaparyuga’ made Dwarka popular. Lord Krishna resisded here for a long time leaving his birthplace, Mathura. </li>
                                <li>The fourth dhaam, Jagannathpuri is also extremely famous for its Rath Yatra. </li>
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong>Dwarka</strong> Temple Location </li>
								<li><strong>Rameshwaram</strong> Temple Location </li>
								</ul>
								
								<ul class="span3">
								<li><strong>Badrinath</strong> Temple Location </li><br>
								</ul>

								<ul class="span3">
								<li><strong>Puri</strong> Temple Location </li><br>
								</ul>
						</div>	

					<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Jamnagar</li>
<li><span class="city">Dwarkadesh Temple</span><span>131 kms </span><span>2h 16mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Dehradun</li>
<li><span class="city">Jamnagar</span><span>1436 kms </span><span>21h 21mins</span></li>
<li><span class="city">Badrinath Temple	</span><span>336 kms </span><span>5h 32mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Bhubneshwar </li>
<li><span class="city">Dehradun </span><span>1849 kms </span><span>27h 00mins</span></li>
<li><span class="city">Jagannath Puri temple</span><span>62.7 kms </span><span>1h 12mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Madurai</li>
<li><span class="city">Bhubneshwar </span><span>1683 kms </span><span>25h 0mins</span></li>
<li><span class="city">Rameshwaram Temple (Arulmigu Ramanathaswamy Temple)	</span><span>169  kms </span><span> 9mins</span></li>
</ul>
</div>

<hr/>


<!--Mobile content-->


<?php

}
else
{
?>

<!--Desktop content-->
						
	<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#jamnagar">Jamnagar</a></li>
			
			<li><a href="#dehradun">Dehradun</a></li>
			<li><a href="#bhubneshwar">Bhubneshwar</a></li>
			<li><a href="#madurai">Madurai</a></li>
			<!--<li><a href="#kedarnath">Kedarnath</a></li>-->
		</ul>
		<ul id="issues">
      
			<li id="Jamnagar">
					<ul class="route">
					<li> -</li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hour 0 min</li>
					<li  class="flight">0 Hours 0 min </li>
					<li class="km"> 0</li>
					<li>Jamnagar</li>
					</ul>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				
			</li>-->
			<li id="dehradun"> 
					<ul class="route">
					<li>Jamnagar</li>
					<li class="car"> 28 Hours</li>
					<li class="train"> 32 Hours 13 Min</li>
					<li  class="flight"> 4 Hours 25 Min </li>
					<li class="km"> 1866 </li>
					<li> Dehradun</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Dwarka Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>Jamnagar:</td>								
						<td>127  KM </td>								
						<td> 2 h 12 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
					<!--<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> -->
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
					
			</li>
			
			<li id="ujjain"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>-->
				
			</li>
			
			<li id="bhubneshwar"> 
					<ul class="route">
					<li> Dehradun</li>
					<li class="car"> 2 Hours 50 Min</li>
					<li class="train"> 44 Hours 35 Min </li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 1738</li>
					<li> Bhubneshwar</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Badrinath Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>Dehradun</td>								
						<td>311 KM </td>								
						<td> 5 Hour 5 min</td>								
					</tr>
				
				<!--	<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				
                <!--<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>-->
			
			<li id="kedarnath">
					<ul class="route">
					<li> Bhubneshwar</li>
					<li class="car"> 24 Hours</li>
					<li class="train"> 36 Hours</li>
					<li  class="flight"> 05 Hours 35 Min</li>
					<li class="km"> 1683</li>
					<li> Madurai</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Puri Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>Bhubneshwar</td>								
						<td>58 KM </td>								
						<td> 1 h 3 min</td>								
					</tr>
				
					<!--<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				
                <!--<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>-->
				
			</li>
			
					
		</ul>	
		
	</div>
	<?php 

}
?>
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  
		  <?php include'footer.php' ?>
		  </div>
		  </body>
		  </html>