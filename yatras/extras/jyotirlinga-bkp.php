
<!DOCTYPE html>
<?php include'inner_head.php';?>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  		<!-- TOP NAVIGATION 
	  		<div id="top-navigation">
		  		<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
			  		<div class="summary">
        	
         	<h2 class="classy f-left"><span>Shopping Cart -</span>
                        	<a href="#">0</a><span class="Itext"> item</span>                                    </h2>
           
        </div>
		  		</ul>
	  		</div>
	  	
	  		<!-- MOBILE NAVIGATION -->
	  		<nav id="navigation-mobile"></nav>
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			<a href="#" id="show-mobile-menu"><i class="fa fa-bars"></i></a>
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		<!-- TEXT SLIDER -->
			<!--<div id="ticker" class="animate-me zoomIn">
			
				<div class=" temple-search-block">
					<div class="temple-search">
						<form action="result.php">
							<input type="text" placeholder="Temple Name"/>
							<input type="text" placeholder="Location "/>
							<button type="submit" title="Search" class="button" id="searchtemp"><span><i class="fa fa-search"></i> Search </span></button>
						</form>  
					</div>
				</div>
				
			</div>  -->		
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  	<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
		  	<!--<div id="header-video"
			    data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>-->
	  		
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			12 Jyotirlinga Yatra
	  			</h2>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/1.png" />
                <img u="thumb" src="images/slider/1-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/2.png" />
                <img u="thumb" src="images/slider/2-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/3.png" />
                <img u="thumb" src="images/slider/3-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/4.png" />
                <img u="thumb" src="images/slider/4-tn.png" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>A devotional object that represents the Almighty Lord Shiva is called a Jyotirlinga or Jyotirling or Jyotirlingam. Basically, the Jyotirlingas are a set of twelve Shiva temples.  </p>
				 <p>These are spread all over India and are considered the holiest by Shiva devotees. ‘Jyoti’ means ‘radiance’ and ‘lingam’ means a ‘mark’ or a ‘sign’ of Shiva. </p>
				<p> The pineal gland is also a symbol of it. Thus, the meaning of Jyotirlinga is The Radiant Sign of the Almighty. </p>
				 <p>It is an old belief that Lord Shiva first manifested himself as a Jyotirlinga on the night of the Aridra Nakshatra. And hence the Jyotirlinga holds so much of significance.  </p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
					<li><strong>Travel Plan</strong> : 12 Jyotirlinga Yatra ,    </li><br/>
					<li><strong>Minimum Duration</strong> : 17 Days,  </li><br/>
					<!--<li><strong>Day 3 </strong>: CHINTAMANI , GIRIJMATMAJ  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
					<li><strong>Best Season To Visit</strong> : Sept to Feb.  </li>
				</ul><br/><br/>
					
				 <a href="images/jyotirlinga.jpg"><img class="img-map" src="images/jyotirlinga.jpg" width="400" height="400"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
							<p>The true essence of the greatness and auspiciousness of the Jyotirlingas is sung in the Puranas with utmost reverence. </p>
							<p>All the sins or unacceptable actions are eliminated by reciting the name of Lord. For those who attain the Sadhaka level, calmness and purity become their characteristics. </p>
							<p>He gets illuminated and enlightened by indulging himself in the supreme knowledge that’s imparted. </p>
							<p>There is no distinguishing feature as such, but it is rightly believed that a person can see these lingas as columns of fire piercing through the earth after he reaches a higher level of spiritual attainment.</p>
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul>
								<li>As per the mythological stories, the Shivapurana says that once there was an argument between Brahma and Vishnu regarding the supremacy of creation.  </li>
								<li>To resolve this, Shiva produced a test according to which all the three worlds got pierced and emerged as a huge endless pillar of light, the Jyotirlinga. </li>
								<li>To search for the end of the lighted portion, both Brahma and Vishnu went upwards and downwards respectively. To shine out, Brahma lied to the lord that he had found the end and Vishnu put the reality forward. </li>
								<li>Then lord Shiva emerged as a second pillar and cursed Brahma for lying and said that only Vishnu will be worshipped till the end of eternity! All the twelve Jyotirlingas have their own manifestations of Lord Shiva. </li>
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<!--<div class="tab-pane bounceInUp" id="skill4">
							<h2><i class="more-details"></i> Best Season for Yatra</h2>
							<p class="text-justify">Best months to travel are November to March and avoid April & May as it is too hot. One can travel in rainy 
season as this complete region is low rain belt. </p>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill5">
							<h2><i class="architecture"></i>Itinerary</h2>
							
							<div class="span4">
							
								<ul>
								<li><h3>Bhimashankar Temple, <span class="location">Pune</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 110 kms from Pune.<br/>
								
								Q:How much time to reach the temple?<br/>
								02.36 Hrs From Pune To reach Bhimashankar.<br/>
								
								
								</p></span>
								</li>
								
								</ul>
							</div>
						
							<div class="span4">
								<ul class="temple">
								<li><h3>Trimbakeshwar Temple, <span class="location">Nashik</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 233 km from Pune.<br/>
								Q:How much time to reach the temple?<br/>
								04.32 Hrs From Pune to reach Trimbakeshwar Temple.<br/>
							    </p>
								</span></li>
								</ul>
							</div>
						
						
							<div class="span4">
							
								<ul>
								<li><h3>Grishneshwar Temple, <span class="location">Aurangabad</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 215 kms from Pune<br/>
								
								Q:How much time to reach the temple?<br/>
								03.44 Hrs From Pune.<br/>
								
								</p>
								</span></li>
								
								</ul>
							</div>
						
						
						
						<div class="span4">
						<h2>Day 4</h2>
								<ul>
								<li><h3>Ujjain, <span class="location">Madhya Pradesh</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 779 kms from Veraval.<br/>
								Q:How to reach there?	<br/>	
								1.Via Train.<br/>
								Q:How much time to reach the temple?<br/>
								17.50 Hrs From Veraval.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						
						
						<div class="span4">
						<h2>Day 5</h2>
								<ul class="temple">
								<li><h3>Omkareshwar Temple, <span class="location">Madhya Pradesh</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 140 kms from Ujjain.<br/>
								Q:How to reach there?	<br/>	
								1.Via MP SH 27 and Indore-Icchapur Rd/Khandwa Rd.<br/>
								Q:How much time to reach the temple?<br/>
								2.19 Hrs From Ujjain.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								<li><h3>Mahakaleshwar Temple, <span class="location">Madhya Pradesh</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 140 kms from Ujjain.<br/>
								Q:How to reach there?	<br/>	
								1.Via MP SH 27 and Indore-Icchapur Rd/Khandwa Rd.<br/>
								Q:How much time to reach the temple?<br/>
								2.19 Hrs From Ujjain.<br/>
								Q:How much time will it take for the Darshan?<br/>
								2.00 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								<li><h3>Bhairav Baba & Ma Shaktipeeth, <span class="location">Madhya Pradesh</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 140 kms from Ujjain.<br/>
								Q:How to reach there?	<br/>	
								1.Via MP SH 27 and Indore-Icchapur Rd/Khandwa Rd.<br/>
								Q:How much time to reach the temple?<br/>
								2.19 Hrs From Ujjain.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						
						
						<div class="span4">
						<h2>Day 6</h2>
								<ul>
								<li><h3>Guptakashi, <span class="location">Haridwar</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 205 kms from Ujjain.<br/>
								Q:How to reach there?	<br/>	
								1.Via Natioanal Highway 58.<br/>
								Q:How much time to reach the temple?<br/>
								3.42 Hrs From Ujjain.<br/>
								Q:How much time will it take for the Darshan?<br/>
								30 min Approximately.
								
								
								
							</p>
								</span></li>
								<li><h3>Kedarnath Temple, <span class="location">Uttarakhand</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 32.6 kms from Haridwar.<br/>
								Q:How to reach there?	<br/>	
								1.Via National Highway 109.<br/>
								Q:How much time to reach the temple?<br/>
								51 min From Haridwar.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						
						
						<div class="span4">
						<h2>Day 7</h2>
								<ul>
								
								<li><h3>Varanasi, <span class="location">Varanasi</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 1043 kms from Uttarakhand.<br/>
								Q:How to reach there?	<br/>	
								1.Kedarnath to Dehradun Via Road ways & then Via Train.<br/>
								Q:How much time to reach the temple?<br/>
								18.42 Hrs From Uttarakhand.<br/>
								
								
								
							</p>
								</span></li>
								<li><h3>Kashi Vishawanath & Sankatmochan Temple, <span class="location">Varanasi</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 6.3 kms from Varanasi.<br/>
								Q:How to reach there?	<br/>	
								1.Via National Highway 29.<br/>
								Q:How much time to reach the temple?<br/>
								12 Min From Varanasi.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
				
						
						<div class="span4">
						<h2>Day 8</h2>
								<ul>
								<li><h3>Deoghar-Kolkata-Chennai, <span class="location">Kolkata-Chennai</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 1997 kms from Varanasi.<br/>
								Q:How to reach there?	<br/>	
								1.Via National Highway 2.<br/>
								Q:How much time to reach the temple?<br/>
								5.20 Hrs From Varanasi.<br/>
								
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						
						
						<div class="span4">
						<h2>Day 10</h2>
								<ul>
								<li><h3>Rameshwaram, <span class="location">Tamilnadu</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 548 kms from Chennai.<br/>
								Q:How to reach there?	<br/>	
								1.via NH45 and National Highway 210.<br/>
								Q:How much time to reach the temple?<br/>
								9.25 Hrs From Chennai.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
					
						
						<div class="span4">
						<h2>Day 11</h2>
								<ul>
								<li><h3>Madurai, <span class="location">Tamilnadu</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 169 kms from Rameshwaram.<br/>
								Q:How to reach there?	<br/>	
								1.National Highway 49.<br/>
								Q:How much time to reach the temple?<br/>
								3.9 Hrs From Rameshwaram.<br/>
								
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						<div class="span4">
						<h2>Day 12</h2>
								<ul>
								<li><h3>Pune, <span class="location">Maharashtra</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 1279 Kms from Madurai.<br/>
								Q:How to reach there?	<br/>	
								1.Madurai to Pune Via Train.<br/>
								Q:How much time to reach the temple?<br/>
								18.00 Hrs From Madurai.<br/>
								
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						<div class="span4">
						<h2>Day 13</h2>
								<ul>
								<li><h3>Bhimashankar Temple, <span class="location">Maharashtra</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 110 kms from Pune.<br/>
								Q:How to reach there?	<br/>	
								1.via MH SH 54 and NH 50.<br/>
								Q:How much time to reach the temple?<br/>
								2.33 Hrs From Pune.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
					
						
						<div class="span4">
						<h2>Day 14</h2>
								<ul>
								<li><h3>Parashuram & Trimbakeshwar, <span class="location">Nashik</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 234 kms from Bhimashankar.<br/>
								Q:How to reach there?	<br/>	
								1.Via MH SH 112 and NH 50.<br/>
								Q:How much time to reach the temple?<br/>
								4.31 Hrs From Bhimashankar.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.30 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								<li><h3>Shirdi,Shani Signapur & Grishneshwar, <span class="location">Aurangabad</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 254 kms from Nashik.<br/>
								Q:How to reach there?	<br/>	
								1.Via NH 50.<br/>
								Q:How much time to reach the temple?<br/>
								4.20 Hrs From Nashik.<br/>
								Q:How much time will it take for the Darshan?<br/>
								3.00 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						
						
						<div class="span4">
						<h2>Day 15</h2>
								<ul>
								<li><h3>Srisailam Mallikaarjun Temple, <span class="location">Andra Pradesh</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 213 kms from Aurangabad.<br/>
								Q:How to reach there?	<br/>	
								1.Via SH 5.<br/>
								Q:How much time to reach the temple?<br/>
								4.16 Hrs From Aurangabad.<br/>
								Q:How much time will it take for the Darshan?<br/>
								1.00 Hrs Approximately.
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						<div class="span4">
						<h2>Day 16</h2>
								<ul>
								<li><h3>Hydrabad, <span class="location">Andra Pradesh</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 213 kms from Maliikaarjun.<br/>
								Q:How to reach there?	<br/>	
								1.Via SH 5.<br/>
								Q:How much time to reach the temple?<br/>
								17.50 Hrs From Mallikaarjun<br/>
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
					
						
						<div class="span4">
						<h2>Day 17</h2>
								<ul>
								<li><h3>Mumbai, <span class="location">Maharashtra</span></h3>
								<span class="loc-image"><p class="img-gan">
								This place is 701 kms from Hydrabad.<br/>
								Q:How to reach there?	<br/>	
								1.Via NH9/ Train.<br/>
								Q:How much time to reach the temple?<br/>
								11.00 Hrs From Hydrabad.<br/>
								
								
								
								
							</p>
								</span></li>
								
								
								</ul>
						</div>
						
						
						
						
						</div>
						</div>
						
					</div>
					<script>
					  $(function () {
					    	$('#SkillsTab a').click(function (e) {
						  		e.preventDefault()
						  		$(this).tab('show')
							});
					  });
					</script>
	  			</div>
<style>

</style>
	  		</div>
	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	<div class="with-separation-top"></div>
    <!-- SCRIPTS -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  
		  <?php include'footer.php' ?>
		  </div>
		  </body>
		  </html>