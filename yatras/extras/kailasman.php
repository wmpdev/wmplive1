
<!DOCTYPE html>
<?php include'inner_head.php';?>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			Kailash Manasarovar Yatra
	  			</h2>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/km1.png" />
                <img u="thumb" src="images/slider/km1-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/km2.png" />
                <img u="thumb" src="images/slider/km2-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/km3.png" />
                <img u="thumb" src="images/slider/km3-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/km4.png" />
                <img u="thumb" src="images/slider/km4-tn.png" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>In Tibet, Kailash parvat is a peak in the Kailash range which forms a part of the Transhimalaya. It is positioned near the source of some longest rivers that flow in Asia. </p>
				 <p>These rivers are- the Indus River, the Sutlej River (a major tributary of the Indus River), the Brahmaputra River, and the Karnali River (a tributary of the River Ganga).  </p>
				<p>It is considered to be holy by four religions- Bon, Buddhism, Jainism and Hinduism. The mountain lies near two beautiful lakes namely Lake Lake Manasarovar and Lake Rakshastal in Tibet. </p>
				 <p></p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
					<li><strong>Travel Plan</strong> : Kailash Manasarovar Yatra,</li><br/>
					<li><strong>Minimum Duration</strong> : 15 Days,  </li><br/>
					<!--<li><strong>Day 3 </strong>: CHINTAMANI , GIRIJMATMAJ  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
					<li><strong>Best Season To Visit</strong> : Mid-May to mid-Oct.  </li>
				</ul><br/><br/>
					
				 <a href="images/jyotirlinga.jpg"><img class="img-map" src="images/jyotirlinga.jpg" width="400" height="450"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>In Hinduism- Lord Shiva, the destroyer of ignorance and illusion, resides in this legendary mountain, Kailash. Here, he sits in a perpetual meditation state with Goddess Parvati. </p> 
                            <p>In Jainism- The Jain religion calls it Meru Parvat or Sumeru. The place where Rishabhdev attained moksha lies next to Mt. Kailash and is called Ashtpada.</p> 
                            <p>In Buddhism- Tantrik Buddhists believe that Kailash is the home of the Buddha Demchok, who represents ultimate bliss. </p>
                            <p></p>
							
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul>
								<li>The Hindu mythology says that the Creator Brahma formed this lake. According to him, it was appropriate place to perform religious rituals.  </li>
								<li>As per the Buddhist mythology, Mansarovar is related with Anotatta Lake. Lord Buddha was conceived at this lake. </li>
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								
								</ul>
								
								<ul class="span3">
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								
								</ul>
								<ul class="span3">
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								
								</ul>
						</div>	
						<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#pune">Pune</a></li>
			
			<li><a href="#rajkot">Rajkot</a></li>
			<li><a href="#ujjain">Ujjain</a></li>
			<li><a href="#varanasi">Varanasi</a></li>
			<li><a href="#kedarnath">Kedarnath</a></li>
		</ul>
		<ul id="issues">
			<li id="pune">
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				
			</li>
			<li id="rajkot"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
					<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
					
			</li>
			
			<li id="ujjain"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			
			<li id="varanasi"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			
			<li id="kedarnath">
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
			</li>
			
					
		</ul>	
		
	</div>
	<style>
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul#issues li{width:800px;}
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul li:before{display:none}
			div#timeline  ul#issues li{width:800px;text-align:center;overflow: hidden;}
			div#timeline ul li ul li {height: auto;}
	</style>
	
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  
		  <?php include'footer.php' ?>
		  </div>
		  </body>
		  </html>