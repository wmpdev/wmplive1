
<!DOCTYPE html>
<?php include'inner_head.php';?>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		

	  	
	  		<!-- MOBILE NAVIGATION /-->
	  		<nav id="navigation-mobile"></nav>
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			<a href="#" id="show-mobile-menu"><i class="fa fa-bars"></i></a>
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		<!-- TEXT SLIDER -->
			<div id="ticker" class="animate-me zoomIn">
			
				<div class=" temple-search-block">
					<div class="temple-search">
						<form action="result.php">
							<input type="text" placeholder="Temple Name"/>
							<input type="text" placeholder="Location "/>
							<button type="submit" title="Search" class="button" id="searchtemp"><span><i class="fa fa-search"></i> Search </span></button>
						</form>  
					</div>
				</div>
				
			</div>  		
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  	<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
		  	<!--<div id="header-video"
			    data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>-->
	  		
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h2 class="with-breaker animate-me fadeInUp">
		  			Sri Bhagavathi Temple<span>Kanyakumari</span>
	  			</h2>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 300px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/1.png" />
                <img u="thumb" src="images/slider/1-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/2.png" />
                <img u="thumb" src="images/slider/2-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/3.png" />
                <img u="thumb" src="images/slider/3-tn.png" />
            </div>
            <div>
                <img u="image" src="images/slider/4.png" />
                <img u="thumb" src="images/slider/4-tn.png" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
        <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 0px;">
            <div style=" background-color: #000; filter:alpha(opacity=30); opacity:.3; width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            <style>
               
            </style>
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
       
    </div>
					</div>
						 <br/>
				 <br/>
				
				 <div class="temple-description"><p>Dedicated to the Kumari (virgin) form of Goddess Parvati, Kanyakumari is situated in the state of Tamil Nadu at the southernmost end of the Indian sub-continent.</p>
				 <p>During the British period, Kanyakumari was known as Cape Comorin.</p>
				 <p>In 1892 A.D., Swami Vivekanand meditated on the sea rock over here. India's ?rst and only museum of wax statues is situated here.</p>
				 <p>The location of Kanyakumari is such that on a full moon day (especially of the Chaitra month), sunset and the rising of the moon can be seen at the same time at opposite ends while on the next day, the picturesque view of sunrise and the moon setting can be seen.</p>
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>
				 <br/>
					<ul class="location-special">
					<li><strong>Locality/village</strong> : Kanyakumari </li>
					<li><strong>State</strong> : Tamil Nadu  </li>
					<li><strong>Country </strong> : India  </li>
					<li><strong>Nearest City/Town</strong> : Kanyakumari  </li>
					<li><strong>Best Season To visit</strong> : Oct to march  </li>
					<li><strong>Languages</strong> : Malyalam Tamil English </li>
					</ul>
					
				
				 <br/>
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
					<div class="row">
						<ul class="temple-services">
						<li class="orange"><i class="fa fa-video-camera"></i> Live Darshan</li>
						<li class="yellow"><i class="fa fa-th-large"></i> Pooja in Temple</li>
						<li class="orange"><i class="fa fa-fire"></i> Online Prasad</li>
						<li class="yellow"><i class="fa fa-bookmark"></i> Book Hotels Now</li>
						</ul>
					</div>
			
			
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Story</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> Facts</a></li>
						<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="architecture"></i> Architecture</a></li>
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="guidance"></i> Guidance</a></li>
						<li><a href="#skill6" role="tab" data-toggle="tab"><i class="accommodation"></i> Accommodation</a></li>
						<li><a href="#skill7" role="tab" data-toggle="tab"><i class="near-place"></i> Near by Places</a></li>
						<li><a href="#skill8" role="tab" data-toggle="tab"><i class="more-details"></i> MORE DETAILS</a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInRight" id="skill1">
							<h2><i class="story"></i>Story</h2>
							<p>Sri Bhagavathi the beautiful temple located at the convergence of the Indian Ocean, The Arabian Sea and the Bay of Bengal giving the most amazing image to the pilgrimages.</p>
							<p>It is believed to be built by the ?rst Pandyas. Also, was later expanded by Nayaks. Three prakars are there of this temple. The goddess image has been displayed in the beautiful world of bene?cence and serenity.</p>
							<p>There lies a necklace in her hand. However, the eastern gate which is facing to the Bay of Bengal is open for only ?ve times in a year that too on special occasion.</p>
							<p>One of the most captivating aspects of Hindu religion is the depiction of the divine love betweent Lord Siva and his consort Parvati.  It has not only been the staple of many a poetic e?ort but also the inspiration for many gnorious shrines.</p>
						</div>
						<div class="tab-pane bounceInRight" id="skill2">
							<h2><i class="facts"></i>Facts</h2>
								<ul>
								<li>Besides Ramayan and Mahabharat, it has also been mentioned in Mani Mekalai, the odyssey of Tamil literature written by Seetalai Satanar. </li>
								<li>In the Sangam literature, Kanyakumari has been described as the southernmost point of Tamilham region that was under the Pandya regime. </li>
								<li>Panuya,  the  king  of  Madurai  is  credited  for the  initial  construction  of  the  temple  in  the eighth century. </li>
								<li> In   the   middle   age,   the   region   constantly developed under the Cholas, the Pandyas, the Vijaynagar Empire, and the Nayaks. Most of the temple construction has been done during the reign of the Nayaks.</li>
								</ul>
						</div>
						<div class="tab-pane bounceInRight" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Pongal</h3>
								<p><img src="images/pongal.jpg">
								This is an agricultural festival which is celebrated in Kanyakumari in mid-January for 3 days just as in the other parts of Tamil Nadu </p></li>
								<li><h3>Rath Yatra</h3>
								<p><img src="images/yatra.jpg"> 
								This is a huge festival celebrated in the Tamil month of Vaikasi (May).  The celebrations last for 10 day, of which the ninth day is the ceremonious procession of the Devi idol around the city and a boating festival held on the tenth day.</p>
								</li>
								</ul>
						</div>
						<div class="tab-pane bounceInRight" id="skill4">
							<h2><i class="architecture"></i> ARCHITECTURE</h2>
							<p class="text-justify">The memory of the father of nation, Mahatma Gandhi, is enshrined in this mandapam erected at the place where his ashes were kept for public view before immersion into the sea. The architecture of the building allows the rays of the sun to fall on the spot where the ashes were kept.<br/>Every year on October 2, his birthday is celebrated here. Timings: 7 am to 7 pm.</p>
						</div>
						<div class="tab-pane bounceInRight" id="skill5">
							<h2><i class="guidance"></i>GUIDANCE</h2>
								<ul>
								<li>Kanyakumari lies on the southernmost tip of the Indian Peninsula, it is geographically a cape and during British rule in India, it was known as Cape Comorin. </li>
								<li>Kanyakumari is home to Kumari Amman or the Kanyakumari Temple and is a Shakti Peetha temple dedicated to a manifestation of Parvati. </li>
								<li>Kanyakumari also the site of Vivekananda Rock Memorial built in 1970 where Swami Vivekananda meditated (in middle of the ocean).  </li>
								<li>Kanyakumari is a pilgrimage and spiritual destination and often day visited from Kovalam. --Kanyakumari is a popular 1-night destination for overseas groups to Tamilnadu. </li>
								<li>Kanyakumari is one those few places from where one can see a clear sunrise and sunset from the same spot</li>
								</ul>
						</div>
						<div class="tab-pane bounceInRight accommodation" id="skill6">
							<h2><i class="accommodation"></i>ACCOMMODATION</h2>
							<div class="row">
							<div class="col-md-6">
							<ul>
							<li><a href="#">Hotel Samudra</a> <span class="rating">4.3</span></li>
							<li> <a href="#">Hotel Srimaniya</a> <span class="rating">4.8</span></li>
							<li><a href="#">Hotel Saravana</a> <span class="rating">3.4</span></li>
							<li> <a href="#">Hotel Tamilnadu </a> <span class="rating">2.2</span></li>
							</ul>
							</div>
							<div class="col-md-6">
							<ul>
							<li><a href="#">Hotel Samudra</a> <span class="rating">4.3</span></li>
							<li> <a href="#">Hotel Srimaniya</a> <span class="rating">4.8</span></li>
							<li><a href="#">Hotel Saravana</a> <span class="rating">3.4</span></li>
							<li> <a href="#">Hotel Tamilnadu </a> <span class="rating">2.2</span></li>
							</ul>
							</div>
							</div>
						</div>
						
						<div class="tab-pane bounceInRight" id="skill7">
							<h2><i class="near-place"></i> NEAR BY PLACES</h2>
							<p><span class="red">Guganathaswamy Temple</span>: This is a 1000-year-old temple believed to have been built by Raja Raja Chola. The architectural style of the Cholas is evident.</p>
							<p><span class="red">Suchindram</span>: 13 km from Kanyakumari, is the famous Sthanumalayan Temple dedicated to Siva, Vishnu and Brahma. This 9th century temple is testimony to the skill of sculptors. Musical pillars, a huge 18ft Hanuman statue and a unique bas-relief of Vainayaki are of note.</p>
						</div>

						<div class="tab-pane bounceInRight details" id="skill8">
							<h2><i class="more-details"></i> MORE DETAILS</h2>
							<ul>
							<li><strong>Temple Administration</strong> <i>-</i></li>
							<li> <strong>Police assistance</strong> <i>91-4652-246224  </i></li>
							<li> <strong>Hospital &amp; Ambulance </strong> <i> Kanyakumari Government Hospital, Tel: +91-4652-248505 </i></li>
							<li> <strong>Currency Exchange Center  </strong>  
							<i>1.Allahabad Bank 24-25, North Car Street, Nagarcoil-629001,  Dist- Kanyakumari, Tamil Nadu Tel: +91-4652-232612 <br>
							2.  Canara Bank, Main Road, Kanyakumari-629702, Tamil Nadu Tel: +91-452-2330335 </i>
							</li>
							<li><strong>ATM Centre </strong><i> SBI, Hotel Shankar's Guest House, 4/75-A, Main Road, Kanyakumari-629701, Tamil Nadu </i></li>
							<li><strong>Govt Tourist Office </strong> <i> Kanyakumari Tourist Information Centre, Tel: +91-4652-246276</i> </li>
							<li><strong>Famous Cuisine &amp; Restaurants </strong><i> Akshaya Restaurant<br>
							Taj Hotel <br>
							Casanova Family Restaurant<br>
							Sea Sun Residency  (Near Bus Stand) </i></li>
							<li><strong>Local Transport </strong> <i>Tourist Taxis, Auto rickshaws are available</i> </li>


							</ul>
						</div>
					</div>
					<script>
					  $(function () {
					    	$('#SkillsTab a').click(function (e) {
						  		e.preventDefault()
						  		$(this).tab('show')
							});
					  });
					</script>
	  			</div>
<style>

.nav-tabs li a i {
    display: inline-block;
    width:50px;
    height:50px;
    background-size: contain !important;
	background-repeat:no-repeat !important;
	float:left;
}
.nav-tabs>li>a{padding:5px;display:inline-block;width:100%;line-height: 50px;}

.nav-tabs li a .story{background:url('images/content/1.png');}
.nav-tabs li a .facts{background:url('images/content/2.png');}
.nav-tabs li a .festivals{background:url('images/content/3.png');}
.nav-tabs li a .architecture{background:url('images/content/4.png');}
.nav-tabs li a .guidance{background:url('images/content/5.png');}
.nav-tabs li a .accommodation{background:url('images/content/6.png');}
.nav-tabs li a .near-place{background:url('images/content/7.png');}
.nav-tabs li a .more-details{background:url('images/content/8.png');}


.tab-pane h2{}
.tab-pane h2 i{
			display: inline-block;
			width: 40px;
			height: 40px;
			background-size: contain !important;
			background-repeat: no-repeat !important;
			vertical-align: bottom;}

.tab-pane h2 .story{background:url('images/content/1.png');}
.tab-pane h2 .facts{background:url('images/content/2.png');}
.tab-pane h2 .festivals{background:url('images/content/3.png');}
.tab-pane h2 .architecture{background:url('images/content/4.png');}
.tab-pane h2 .guidance{background:url('images/content/5.png');}
.tab-pane h2 .accommodation{background:url('images/content/6.png');}
.tab-pane h2 .near-place{background:url('images/content/7.png');}
.tab-pane h2 .more-details{background:url('images/content/8.png');}	
</style>
	  		</div>
	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	<div class="with-separation-top"></div>
    <!-- SCRIPTS -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  
		  <?php include'footer.php' ?>