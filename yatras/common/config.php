<?php
	// Error reporting
	error_reporting(E_ERROR | E_PARSE);
	
	// Timezone
	date_default_timezone_set('IST');

	//Database connection
	$mysql_server = 'localhost';
	$mysql_user = 'wheresmy_dbadmin';
	$mysql_pass = '4,g*u3f*RCDb';
	$mysql_db = 'wheresmy_dbecom';
	$mysql_table_prefix = 'tm_';

	$dbh = new mysqli($mysql_server, $mysql_user, $mysql_pass, $mysql_db);
	if($dbh->connect_errno) {
			echo "Error connecting database server : " . $dbh->connect_error;
			exit();
	}


	//Load configurations
	$result = $dbh->query("SELECT * FROM `" . $mysql_table_prefix . "configurations`");
	while ($record = $result->fetch_object()) {
		switch($record->option) {

			case 'environment':
				$config['environment'] = $record->value;
				break;

			default:
				$config[$record->option] = json_decode($record->value, true);
				break;
		}
	}
?>