<?php
	$mageFilename = '../app/Mage.php';
	include_once($mageFilename);
	//Mage::setIsDeveloperMode(true);
	//ini_set('display_errors', 1);
	umask(0);
	Mage::app('default');
	Mage::getSingleton('core/session', array('name'=>'frontend'));
	
	$session = Mage::getSingleton('customer/session');
	if($session->isLoggedIn()) {
		$userId = $session->getCustomer()->getId();
	} else {
		$userId = 0;
	}

	$block              = Mage::getSingleton('core/layout');
	 
	# HEAD BLOCK
	$headBlock          = $block->createBlock('page/html_head');// this wont give you the css/js inclusion
	// add js
	$headBlock->addJs('prototype/prototype.js');
	$headBlock->addJs('lib/ccard.js');
	$headBlock->addJs('prototype/validation.js');
	$headBlock->addJs('scriptaculous/builder.js');
	$headBlock->addJs('scriptaculous/effects.js');
	$headBlock->addJs('scriptaculous/dragdrop.js');
	$headBlock->addJs('scriptaculous/controls.js');
	$headBlock->addJs('scriptaculous/slider.js');
	$headBlock->addJs('varien/js.js');
	$headBlock->addJs('varien/form.js');
	$headBlock->addJs('varien/menu.js');
	$headBlock->addJs('mage/translate.js');
	$headBlock->addJs('mage/cookies.js');
	# add css
	$headBlock->addCss('css/styles.css');
	$headBlock->getCssJsHtml();
	$headBlock->getIncludes();
	 
	# HEADER BLOCK
	//$headerBlock        = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml')->toHtml();
	 
	# FOOTER BLOCK
	$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
?>
<!DOCTYPE html>
<html>
<head>
<?php echo $headBlock->toHtml(); ?>
    <meta charset="utf-8">
    <!-- PAGE TITLE -->
    <title>Temple Search</title>
    <!-- MAKE IT RESPONSIVE -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- SEO -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <!-- FAVICON -->
	<link rel="icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
	<link rel="shortcut icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
	<!--<link rel="shortcut icon" href="images/favicon.png" />-->
    <!-- STYLESHEETS -->
	<!--<link rel="stylesheet" id="main-styles-css" href="./Salient_files/style.css" type="text/css" media="all">-->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/style.css" rel="stylesheet" media="screen">
    <link href="https://www.wheresmypandit.com/skin/frontend/wmp/default/css/wmplatest/css/main.css" rel="stylesheet" media="screen">
    <!-- <link href="inner_css/styles.css" rel="stylesheet" media="screen"> -->
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link href="css/options.css" rel="stylesheet" media="screen">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
    <!-- FONTS -->
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:900,300,400,200,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="./js/jquery-ui.css">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<!--<script type="text/javascript" src="Salient_files/prettyPhoto.js"></script>-->
	<script src="js/jquery-1.11.1.min.js"></script>
</head>
