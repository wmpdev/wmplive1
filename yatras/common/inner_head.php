<?php
	$mageFilename = '../app/Mage.php';
	require_once $mageFilename;
	//Mage::setIsDeveloperMode(true);
	//ini_set('display_errors', 1);
	umask(0);
	Mage::app();
	Mage::getSingleton('core/session', array('name'=>'frontend'));
	
	$session = Mage::getSingleton('customer/session');
	if($session->isLoggedIn()) {
		$userId = $session->getCustomer()->getId();
	} else {
		$userId = 0;
	}

	$block              = Mage::getSingleton('core/layout');
	 
	# HEAD BLOCK
	$headBlock          = $block->createBlock('page/html_head');// this wont give you the css/js inclusion
	// add js
	$headBlock->addJs('prototype/prototype.js');
	$headBlock->addJs('lib/ccard.js');
	$headBlock->addJs('prototype/validation.js');
	$headBlock->addJs('scriptaculous/builder.js');
	$headBlock->addJs('scriptaculous/effects.js');
	$headBlock->addJs('scriptaculous/dragdrop.js');
	$headBlock->addJs('scriptaculous/controls.js');
	$headBlock->addJs('scriptaculous/slider.js');
	$headBlock->addJs('varien/js.js');
	$headBlock->addJs('varien/form.js');
	$headBlock->addJs('varien/menu.js');
	$headBlock->addJs('mage/translate.js');
	$headBlock->addJs('mage/cookies.js');
	# add css
	$headBlock->addCss('css/styles.css');
	$headBlock->getCssJsHtml();
	$headBlock->getIncludes();
	 
	# HEADER BLOCK
	$headerBlock        = $block->createBlock('page/html_header')->setTemplate('page/html/header.phtml')->toHtml();
	 
	# FOOTER BLOCK
	$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
?>
<!DOCTYPE html>
<html>
<head>

	<?php 	echo $headBlock->toHtml(); ?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Temple Search</title>

	<link rel="icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
	<link rel="shortcut icon" href="<?php echo $config['site']['url']; ?>skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
	<link href="inner_css/home.css" type="text/css" rel="stylesheet">
	<link href="inner_css/styles.css" type="text/css" rel="stylesheet">
	<link href="css/style.css" type="text/css" rel="stylesheet">
	<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
	<!-- STYLESHEETS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="css/animate.min.css" rel="stylesheet" media="screen">
	<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" media="screen">
	<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
	<link href="css/options.css" rel="stylesheet" media="screen">
	<link href="css/responsive.css" rel="stylesheet" media="screen">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

	<script src="js/thumbelina.js"></script>

	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.accordion.js"></script>
	<link href="js/jquery-ui.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="js/customslide.js"></script>

	<script type="text/javascript" src="js/jssor.js"></script>
	<script type="text/javascript" src="js/jssor.slider.js"></script>
</head>

