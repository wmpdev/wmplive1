<?php
	
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
	
umask(0);
Mage::app();
Mage::getSingleton('core/session', array('name'=>'frontend'));
	
$session = Mage::getSingleton('customer/session');
if($session->isLoggedIn()) 
{
		$userId = $session->getCustomer()->getId();
} else {
		$userId = 0;
}

$block              = Mage::getSingleton('core/layout');
	
	
$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
 

?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Kashi Yatra</title>
<meta name="description" content="We provides you comprehensive guidance on Kashi yatra. It includes significance, history, itinerary, nearby temples, etc.">
<meta name="keywords" content="kashi yatra">
<link rel="canonical" href="https://www.wheresmypandit.com/yatras/kashi.php" />
<link rel="icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link rel="shortcut icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="inner_css/home.css" type="text/css" rel="stylesheet">
<!--<link href="inner_css/styles.css" type="text/css" rel="stylesheet">-->

<link href="style.css" type="text/css" rel="stylesheet">
<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
 <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    
   <!--  <link href="https://www.wheresmypandit.com/skin/frontend/wmp/default/css/wmplatest/css/main.css" rel="stylesheet" media="screen"> -->
    <!--<link href="css/options.css" rel="stylesheet" media="screen">-->
<link href="css/custom.css" type="text/css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
<link href="css/main.css" type="text/css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/thumbelina.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<link href="js/jquery-ui.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/customslide.js"></script>
<script type="text/javascript" src="js/jssor.js"></script>
<script type="text/javascript" src="js/jssor.slider.js"></script>
 <link href="css/hor-timeline.css" rel="stylesheet" media="screen">
<script src="js/jquery.timelinr-0.9.54.js"></script> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

</head>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h1 class="with-breaker animate-me fadeInUp">
		  			Kashi Yatra
	  			</h1>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/ks1.png" alt="Kashi Yatra"/>
                <img u="thumb" src="images/slider/ks1-tn.png" alt="Kashi Yatra"/>
            </div>
            <div>
                <img u="image" src="images/slider/ks2.png" alt="Kashi Yatra"/>
                <img u="thumb" src="images/slider/ks2-tn.png" alt="Kashi Yatra"/>
            </div>
            <div>
                <img u="image" src="images/slider/ks3.png"alt="Kashi Yatra - Confluence of Ganga, Yamuna and Saraswati"/>
                <img u="thumb" src="images/slider/ks3-tn.png" alt="Kashi Yatra"/>
            </div>
            <div>
                <img u="image" src="images/slider/ks4.png" alt="Kashi Yatra"/>
                <img u="thumb" src="images/slider/ks4-tn.png" alt="Kashi Yatra"/>
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>Kashi is a very holy destination and a pilgrimage to Kashi is the dream of every devout Hindu. The city of Varanasi is blessed with the presence of this holy temple. To Hindus, it is one of the most important temples and a yatra to the place is a must. Almost every Hindu visits the place once in a lifetime. Its glory and auspiciousness is boundless. </p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
					<li><strong>No. of Places to be visited</strong> : 4</li>
                    <br/>
                    <!--<li><strong>Day 3 </strong>: CHINTAMANI , Girijatmaj  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
                    <li><strong>Total Distance</strong> : 425 KM</li>
                    <br>
                    <li><strong>Best Season to visit</strong> : Oct and April</li>
                    
				</ul><br/><br/>
					
				 <a href="images/kashi_yatra.jpg"><img class="img-map" src="images/kashi_yatra.jpg" alt="Kashi Yatra Map" width="400" height="450"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
                        <li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>
						
						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>The basic interpretation of the ‘<strong>Kashi Yatra</strong>’ is, a visit to the holy place and bathe in the holy Ganges. Devotees also have the darshan of Lord Vishwanath and perform holy rites to satisfy the souls of their ancestors. To most South Indians, 
                            the yatra starts with a visit to Rameshwaram. The devotees collect sand from there and then lead to the Triveni Sangam in Allahbad. From the confluence of three holy rivers - Ganga, Yamuna and Saraswati - they collect water and then while returning, the bathe the idol of Lord with that sacred water in Rameshwaram. In this manner, the significance and ways of worshipping differ a little.</p>
							
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul style="text-align:left">
								Kasha, the holiest place for Hindus, has also faced many attacks from 1033 to 1669 A.D. Temples were brutally demolished and the Muslims built their Masjids. But the immense faith and courage of the Hindus, helped the beautiful and glorious place to develop. In 1777 A.D., Ahilyabai Holkar built the Kashi Vishweshwar Temple. 
                                From 1785 A.D., the development of other temples near Kashi was undertaken by the then King and his son. Thus, the Kashi we see now is owed to many people who have helped.
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong>Mahabodhi Temple (Bodhgaya) </strong> Gaya </li>
								<li><strong>Triveni Sangam (Sangam Dip)</strong> Allahabad </li>
					<!--			<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>-->
								
								</ul>
								
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=32" title="Vishnupad Temple">Vishnupad Temple</a></strong> Varanasi </li>
								<!--<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>-->
								
								</ul>
								<ul class="span3">
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=38" title="Kashi Vishwanath Temple">Kashi Vishwanath Temple</a> </strong>  Varanasi </li>
								<!--<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>
								<li><strong>Temple Name</strong> Temple Location </li>-->
								
								</ul>
						</div>	
						<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Gaya</li>
<li><span class="city">Mahabodhi Temple (Bodhgaya)</span><span>0 kms </span><span>0h 20mins</span></li>
<li><span class="city">Vishnupad Temple</span><span>3.2  kms </span><span>0h 08mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Varanasi</li>
<li><span class="city">Gaya</span><span>271 kms </span><span>3h 33mins</span></li>
<li><span class="city">Kashi Vishwanath Temple </span><span>26.7 kms </span><span>0h 42mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Allahabad</li>
<li><span class="city">Varanasi </span><span>126.2 kms </span><span>1h 48mins</span></li>
<li><span class="city">Triveni Sangam (Sangam Dip)	</span><span>17.8 kms </span><span>0h 26mins</span></li>

</ul>
</div>

<hr/>

<!--Mobile content end-->


<?php

}
else
{
?>

<!--Desktop content-->
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#gaya">Gaya</a></li>
			<li><a href="#varanasi">Varanasi</a></li>
			<li><a href="#allahabad">Allahabad</a></li>
i>
		</ul>
		<ul id="issues">
			<li id="gaya">
					<ul class="route">
					<li> - </li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 0 </li>
					<li> Gaya</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Mahabodhi Temple (Bodhgaya)  </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Gaya:</td>								
						<td>12.4 KM </td>								
						<td>00 h 20 min</td>								
					</tr>
				
					<tr>
						<td>From Vishnupad Temple:</td>								
						<td> 10.6 KM</td>								
						<td> 00 h 18 min</td>							
					</tr>
				
					<!--<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>-->
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Gaya-Mahabodhi Temple-Vishnupad Temple-Gaya
				<strong>Minimum Duration</strong> 0 h 46 min
				<strong>Minimum Distance</strong>26.1 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Vishnupad Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Gaya:</td>								
						<td>3.2  KM </td>								
						<td>00 h 08 min</td>								
					</tr>
				
					<tr>
						<td>From Mahabodhi Temple (Bodhgaya):</td>								
						<td>10.6 KM</td>								
						<th> &nbsp;&nbsp;00 h 18 min</th>							
					</tr>
				
<!--					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
-->				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>-->
				
				
			</li>
			<li id="varanasi"> 
					<ul class="route">
					<li> Gaya</li>
					<li class="car"> 3 Hours 33 Min</li>
					<li class="train"> 4 Hours 7 Min </li>
					<li  class="flight"> 00 Hours 35 Min</li>
					<li class="km"> 271</li>
					<li> Varanasi</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Kashi Vishwanath Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Varanasi:</td>								
						<td>26.7 KM </td>								
						<td> 0 h 42 min</td>								
					</tr>
				<!--
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				-->
				</tbody>
				</table>
				

				</div>
					<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Varanasi-Kashi Vishwanath Temple-Varanasi
				<strong>Minimum Duration</strong>1 h 16 min
				<strong>Minimum Distance</strong>50.7 KM

				</div> 
				</div>
				
				
					
			</li>
			
			<li id="allahabad"> 
					<ul class="route">
					<li> Varanasi</li>
					<li class="car"> 1 Hours 48 Min</li>
					<li class="train"> 3 Hours 7 MIn</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 126.2</li>
					<li> allahabad</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Triveni Sangam (Sangam Dip)</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Allahabad:</td>								
						<td>17.8  KM </td>								
						<td> 0 h 26 min</td>								
					</tr>
				
					
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Allahabad-Triveni Sangam-Allahabad
				<strong>Minimum Duration</strong>0 h 52 min
				<strong>Minimum Distance</strong>35.7 KM

				</div> 
				</div>
				
				<!--<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			
			<li id="varanasi"> 
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			
			<li id="kedarnath">
					<ul class="route">
					<li> Pune</li>
					<li class="car"> 20 Hours</li>
					<li class="train"> 14 Hours</li>
					<li  class="flight"> 02 Hours</li>
					<li class="km"> 520</li>
					<li> Rajkot</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Pune-Trimbakeshwar-Grineshwar-Bhimashankar-Pune
				<strong>Minimum Duration</strong>18 h 58 min
				<strong>Minimum Distance</strong>1071 KM

				</div> 
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Trimbakeshwar Temple </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>1. From Pune:</td>								
						<td>239 KM </td>								
						<td> 4 h 28 min</td>								
					</tr>
				
					<tr>
						<td>From Bhimashankar</td>								
						<td> 428 KM</td>								
						<td> 7 h 57 min</td>							
					</tr>
				
					<tr>
						<td>From Grineshwar</td>	
						<td>215 KM </td>								
						<td> 3 h 47 min</td>
					</tr>
				
				</tbody>
				</table>
				

				</div>
			
				</div>
				
			</li>-->
			
					
		</ul>	
		
	</div>
    
   <?php 

}
?>
		 
	<!--<style>
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul#issues li{width:800px;}
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul li:before{display:none}
			div#timeline  ul#issues li{width:800px;text-align:center;overflow: hidden;}
			div#timeline ul li ul li {height: auto;}
	</style>-->
	
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2oN6IZ8rywS4dflnIdWupkpOY2FLK0Bx';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
 <?php echo $footerBlock; ?>
		  </div>
		  </body>
		  </html>