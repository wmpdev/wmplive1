<?php
	
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
	
umask(0);
Mage::app();
Mage::getSingleton('core/session', array('name'=>'frontend'));
	
$session = Mage::getSingleton('customer/session');
if($session->isLoggedIn()) 
{
		$userId = $session->getCustomer()->getId();
} else {
		$userId = 0;
}

$block              = Mage::getSingleton('core/layout');
	
	
$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
 

?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Maruti Temples Yatra</title>
<meta name="description" content="WheresMyPandit provides you comprehensive guidance on Maruti Temples Yatra. It includes significance, history, itinerary, nearby temples, etc.">
<meta name="keywords" content="maruti temples yatra">
<link rel="canonical" href="https://www.wheresmypandit.com/yatras/maruti-temples.php" />


<link rel="icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link rel="shortcut icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="inner_css/home.css" type="text/css" rel="stylesheet">
<!--<link href="inner_css/styles.css" type="text/css" rel="stylesheet">-->
<link href="style.css" type="text/css" rel="stylesheet">
<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
 <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <!--<link href="css/options.css" rel="stylesheet" media="screen">-->
<link href="css/custom.css" type="text/css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
<link href="css/main.css" type="text/css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/thumbelina.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<link href="js/jquery-ui.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/customslide.js"></script>
<script type="text/javascript" src="js/jssor.js"></script>
<script type="text/javascript" src="js/jssor.slider.js"></script>
 <link href="css/hor-timeline.css" rel="stylesheet" media="screen">
<script src="js/jquery.timelinr-0.9.54.js"></script> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

</head>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h1 class="with-breaker animate-me fadeInUp">
		  			11-Maruti Temples Yatra
	  			</h1>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/mty1.png" alt="Maruti Temples Yatra" />
                Maruti Mandir,
            </div>
            <div>
                <img u="image" src="images/slider/mty2.png" alt="Maruti Temples Yatra" />
                <img u="thumb" src="images/slider/mty2-tn.png" alt="Maruti Temples Yatra" />
            </div>
            <div>
                <img u="image" src="images/slider/mty3.png" alt="Maruti Temples Yatra" />
                <img u="thumb" src="images/slider/mty3-tn.png" alt="Maruti Temples Yatra" />
            </div>
            <div>
                <img u="image" src="images/slider/mty4.png" alt="Maruti Temples Yatra" />
                <img u="thumb" src="images/slider/mty4-tn.png" alt="Maruti Temples Yatra" />
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>The Maruti Temples, spread all over Maharashtra, were built in the 17th century by Samarth Ramdas. This is a collection of eleven temples. These temples were built as an attempt to gather youth initiative in establishing the Maratha Kingdom. These temples are specifically dedicated to Lord Hanuman, and are built in Satara, Karad and Kolhapur area as part of a Circuit.</p>
				 <p>&nbsp;</p>
				 </div>
				
				 </div>
				 <div class="col-md-4">
				 <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.207317601261!2d77.55195373239381!3d8.08032727666765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xa7605878b561ce4b!2sBagavathi+Amman+Temple!5e0!3m2!1sen!2sin!4v1417613405346" width="400" height="300" frameborder="0" style="border:0"></iframe>-->
				 <ul class="location-special">
				   <li><strong>No. of Places to be visited</strong> : 11</li>
                    <br/>
                    <!--<li><strong>Day 3 </strong>: CHINTAMANI , Girijatmaj  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
                    <li><strong>Total Distance</strong> : 241.5 KM</li>
                    <br>
                    <li><strong>Best Season to visit</strong> : Jan to Apr and &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Oct to Dec</li>
				</ul><br/><br/>
					
<!--				 <a href="images/teen_dham.jpg"><img class="img-map" src="images/mtaruti_temple.jpg" alt="Maruti Temples Yatra Map" width="400" height="450"/></a>-->
				 
                 <a href="images/marutitemplesyatra.jpg"><img class="img-map" src="images/marutitemplesyatra.jpg" alt="Maruti Temples Yatra Map" width="400" height="450"/></a>
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>


						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>These eleven temples were built after Samarth Ramdas had travelled across the country and had gained knowledge. They are believed to be Jagrut, or symbols of knowledge and wisdom. All these eleven temples are located on the banks of the river Krishna. It is believed that a pilgrimage to these eleven temples instils courage and strength in people, by setting them on a path of devotion to Maruti (Lord Hanuman). One of these temples, the Baal Maruti temple is also believed to be the spot of meditation for Swami Smarath.</p>
                           
							
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
						  <h2><i class="facts"></i>History</h2>
								<ul style="text-align:left">
								  <p>The first of these eleven temples, Shahapur temple was built in 1644. These eleven temples were built across ten years, with Shirala Veer Maruti temple being built in 1654. All of these eleven temples sport idols of Lord Hanuman, in varying postures of mythological significance. All the idols placed in the temples are about 4-6 feet tall and are made of varying materials, such as stone, sand, lime, jute and the like. The smallest idol is showcased in the Pargao Temple. The Masur temple also has portraits of Shivaji Maharaj and Swami Samrath along its walls. The most significant out of these is probably the Manapadale Temple, which is considered Jagrut due to its magnificent idol.</p>
						  </ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li>Maruti Mandir Majgaon</li>
								</ul>
								<ul class="span3">
								<li>Khadicha Maruti Shinganwadi</li>
								</ul>
								<ul class="span3">
								<li>Veer Maruti Chaphal </li>
							
								</ul>
                                <ul class="span3">
								<li>Daas Maruti Chaphal </li>
							
								</ul>
                             <!--   <ul class="span3">
								<li><strong><a href="#" title="Gangotri Temple">Gangotri Temple</a></strong> Gangotri </li>
							
								</ul>
                                <ul class="span3">
								<li><strong><a href="#" title="Gangotri Temple">Gangotri Temple</a></strong> Gangotri </li>
							
								</ul>-->
                                <ul class="span3">
								<li>Mathatil Maruti Umbraj </li>
							
								</ul>
                                <ul class="span3">
								<li>समर्थस्थापित मारुती Masur </li>
							
								</ul>
                                <ul class="span3">
								<li>पहिला प्रताप मारुती Shahapur </li>
							
								</ul>
                                <ul class="span3">
								<li>Bahe Borgav Maruti Borgaon </li>
							
								</ul>
                                <ul class="span3">
								<li>Samarth Sthapit Maruti Mandir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shirala </li>
							
								</ul>
                                <ul class="span3">
								<li>Ramdas Swami Hanuman Temple June Pargaon June Pargaon </li>
							
								</ul>
                                <ul class="span3">
								<li>Manapadale Maruti Temple &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manpadle </li>
							
								</ul>
                                
                                
                                
                                
						</div>	
	<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Maruti Mandir, Majgaon</li>
<li><span class="city">Karad Airport</span><span>49.9 kms </span><span>00h 48mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Khadicha Maruti, Shinganwadi </li>
<li><span class="city">Majgaon, Satara </span><span>45.0 kms </span><span>00h 55mins</span></li>
<li><span class="city">Karad Airport </span><span>23.8 kms </span><span>00h 38mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Veer Maruti, Chaphal </li>
<li><span class="city">Shinganwadi </span><span>1.7 kms </span><span>00h 09mins</span></li>
<li><span class="city">Karad Airport </span><span>22.3 kms </span><span>00h 31mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Daas Maruti, Chaphal</li>
<li><span class="city">Chaphal</span><span>200 m </span><span>00h 03mins</span></li>
<li><span class="city">Karad Airport </span><span>22.3 kms </span><span>00h 31mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Mathatil Maruti, Umbraj</li>
<li><span class="city">Chaphal</span><span>12.1 kms</span><span>00h 19mins</span></li>
<li><span class="city">Karad Airport </span><span>18.5 kms </span><span>00h 22mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>समर्थस्थापित मारुती, Masur</li>
<li><span class="city">Umbraj</span><span>8.1 kms</span><span>00h 12mins</span></li>
<li><span class="city">Karad Airport </span><span>22.8 kms </span><span>00h 25mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>पहिला प्रताप मारुती, Shahapur(Near Karad)</li>
<li><span class="city">Masur</span><span>3.9 kms</span><span>00h 06mins</span></li>
<li><span class="city">Karad Airport </span><span>15.1 kms </span><span>00h 25mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Bahe Borgav Cha Maruti</li>
<li><span class="city">Shahapur (Near Karad)</span><span>36.0 kms</span><span>00h 51mins</span></li>
<li><span class="city">Karad Airport </span><span>28.9 kms </span><span>00h 36mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Samarth Sthapit Maruti Mandir, Shirala</li>
<li><span class="city">Bahe Borgav</span><span>26.3 kms</span><span>00h 35mins</span></li>
<li><span class="city">Karad Airport </span><span>42.5 kms </span><span>00h 38mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Ramdas Swami Hanuman Temple June Pargaon</li>
<li><span class="city">Shirala</span><span>22.2 kms</span><span>00h 40mins</span></li>
<li><span class="city">Karad Airport </span><span>57.7 kms </span><span>00h 59mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Manapadale Maruti Temple, Manpadle</li>
<li><span class="city">June Pargaon</span><span>6.7 kms</span><span>00h 17mins</span></li>
<li><span class="city">Karad Airport </span><span>63.2 kms </span><span>01h 01mins</span></li>
</ul>
</div>
<hr/>

<div class="itinerary">
<ul>
<li>Kolhapur Airport</li>
<li><span class="city">Manpadle</span><span>29.3 kms</span><span>00h 37mins</span></li>
<li><span class="city">Karad Airport </span><span>76.5 kms </span><span>01h 08mins</span></li>
</ul>
</div>
<hr/>


<!--Mobile content end-->


<?php

}
else
{
?>

<!--Desktop content-->
						
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#karad">Karad Airport</a></li>
			<li><a href="#majgaon">Majgaon </a></li>
			<li><a href="#chaphal">Chaphal</a></li>
			<li><a href="#umbraj">Umbraj</a></li>
   			<li><a href="#masur">Masur</a></li>
   			<li><a href="#shahapur">Shahapur</a></li>            
   			<li><a href="#borgaon">Bahe</a></li>            
   			<li><a href="#shirala">Shirala</a></li>            
   			<li><a href="#pargaon">Pargaon</a></li>
   			<li><a href="#manpadle">Manpadle</a></li>
   			<li><a href="#kolhapur">Kolhapur</a></li>                                    

		</ul>
		<ul id="issues">
			<li id="karad">
					<ul class="route">
					<li> -</li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 0</li>
					<li> Karad</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Karad</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Maruti Mandir, Majgaon:</td>								
						<td>19.9 KM </td>								
						<td> 00 h 26 min</td>								
					</tr>
				
					<tr>
						<td>From Khadicha Maruti, Shinganwadi:</td>								
						<td> 23.8 KM</td>								
						<td> 00 h 38 min</td>							
					</tr>
				
					<tr>
						<td>From Veer Maruti, Chaphal:</td>	
						<td> 22.3 KM </td>								
						<td> 00 h 30 min</td>
					</tr>
                    
                    <tr>
						<td>From Daas Maruti, Chaphal:</td>	
						<td> 22.2 KM </td>								
						<td> 00 h 30 min</td>
					</tr>
                    <tr>
						<td>From Mathatil Maruti, Umbraj:</td>	
						<td> 17.3 KM </td>								
						<td> 00 h 22 min</td>
					</tr>
                    <tr>
						<td>From समर्थस्थापित मारुती, Masur:</td>	
						<td> 22.3 KM </td>								
						<td> 00 h 28 min</td>
					</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती, Shahapur(Near Karad):</td>	
						<td> 15.2 KM </td>								
						<td> 00 h 24 min</td>
					</tr>
                     <tr>
						<td>From Bahe Borgav Cha Maruti:</td>	
						<td> 28.9 KM </td>								
						<td> 00 h 36 min</td>
					</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>	
						<td> 42.5 KM </td>								
						<td> 00 h 36 min</td>
					</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>	
						<td> 62.1 KM </td>								
						<td> 00 h 58 min</td>
					</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>	
						<td> 63.4 KM </td>								
						<td> 00 h 58 min</td>
					</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>	
						<td> 76.7 KM </td>								
						<td> 01 h 05 min</td>
					</tr>                                        				
				</tbody>
				</table>
				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong> Karad Airport-Majgaon, Satara-Chaphal-Umbraj-Masur-Shahapur, Yashwantnagar-Bahe-Borgaon-Shirala-June Pargaon-Manpadle-Kolhapur Airport
				<strong>Minimum Duration</strong>05 h 24 min
				<strong>Minimum Distance</strong>241.5 KM

				</div> 
				</div>
	
			</li>
			<li id="majgaon"> 
					<ul class="route">
					<li> Karad</li>
					<li class="car"> 0 Hours 26 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 19.9</li>
					<li> Majgaon </li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Maruti Mandir, Majgaon</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Karad Airport:</td>								
						<td>19.9 KM </td>								
						<td> 00 h 26 min</td>								
					</tr>
				
					<tr>
						<td>From Khadicha Maruti, Shinganwadi:</td>								
						<td> 3.9 KM</td>								
						<td> 00 h 12 min</td>							
					</tr>
				
					<tr>
						<td>From Veer Maruti, Chaphal:</td>	
						<td> 2.4 KM </td>								
						<td> 00 h 04 min</td>
					</tr>
                    
                    <tr>
						<td>From Daas Maruti, Chaphal:</td>	
						<td> 2.3 KM </td>								
						<td> 00 h 04 min</td>
					</tr>
                    <tr>
						<td>From Mathatil Maruti, Umbraj:</td>	
						<td>8.5 KM </td>								
						<td> 00 h 14 min</td>
					</tr>
                    <tr>
						<td>From समर्थस्थापित मारुती, Masur:</td>	
						<td> 15.9 KM </td>								
						<td> 00 h 22 min</td>
					</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>	
						<td> 19.5 KM </td>								
						<td> 00 h 27 min</td>
					</tr>
                     <tr>
						<td>From Bahe Borgav Cha Maruti:</td>	
						<td> 50.7 KM </td>								
						<td> 00 h 57 min</td>
					</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>	
						<td> 64.3 KM </td>								
						<td> 00 h 57 min</td>
					</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>	
						<td>83.9 KM </td>								
						<td> 01 h 19 min</td>
					</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>	
						<td> 85.2 KM </td>								
						<td> 01 h 20 min</td>
					</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>	
						<td> 98.5 KM </td>								
						<td> 01 h 26 min</td>
					</tr>                                        				
				</tbody>
				</table>
				

				</div>
					</div>
				
					
			</li>
			
			<li id="chaphal"> 
					<ul class="route">
					<li> Majgaon </li>
					<li class="car"> 00 Hours 24 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 17.2</li>
					<li> Chaphal</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Khadicha Maruti, Shinganwadi </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Karad Airport:</td>								
						<td>23.8 KM </td>								
						<td> 00 h 38 min</td>								
					</tr>
				
					<tr>
						<td>From Maruti Mandir, Shinganwadi:</td>								
						<td> 3.9 KM</td>								
						<td> 00 h 11 min</td>							
					</tr>
				
					<tr>
						<td>From Veer Maruti, Chaphal:</td>	
						<td> 1.7 KM </td>								
						<td> 00 h 09 min</td>
					</tr>
                    
                    <tr>
						<td>From Daas Maruti, Chaphal:</td>	
						<td> 1.7 KM </td>								
						<td> 00 h 09 min</td>
					</tr>
                    <tr>
						<td>From Mathatil Maruti, Umbraj:</td>	
						<td> 12.4 KM </td>								
						<td> 00 h 25 min</td>
					</tr>
                    <tr>
						<td>From समर्थस्थापित मारुती, Masur:</td>	
						<td> 19.8 KM </td>								
						<td> 00 h 34 min</td>
					</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>	
						<td> 23.4 KM </td>								
						<td> 00 h 39 min</td>
					</tr>
                     <tr>
						<td>From Bahe Borgav Cha Maruti:</td>	
						<td> 54.6 KM </td>								
						<td> 00 h 09 min</td>
					</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>	
						<td> 68.2 KM </td>								
						<td> 00 h 09 min</td>
					</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>	
						<td> 87.7 KM </td>								
						<td> 00 h 31 min</td>
					</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>	
						<td> 89.0 KM </td>								
						<td> 00 h 31 min</td>
					</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>	
						<td> 102 KM </td>								
						<td> 01 h 38 min</td>
					</tr>                                        				
				</tbody>
				</table>
				</div>
                
			<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Veer Maruti, Chaphal</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Karad Airport:</td>								
						<td>22.3 KM </td>								
						<td> 00 h 30 min</td>								
					</tr>
				
					<tr>
						<td>From Maruti Mandir, Majgaon:</td>								
						<td> 2.4 KM</td>								
						<td> 00 h 04 min</td>							
					</tr>
				
					<tr>
						<td>From Khadicha Maruti, Shinganwadi:</td>	
						<td> 1.7 KM </td>								
						<td> 00 h 09 min</td>
					</tr>
                    
                    <tr>
						<td>From Daas Maruti, Chaphal:</td>	
						<td> 200 KM </td>								
						<td> 00 h 02 min</td>
					</tr>
                    <tr>
						<td>From Mathatil Maruti, Umbraj:</td>	
						<td>10.9 KM </td>								
						<td> 00 h 18 min</td>
					</tr>
                    <tr>
						<td>From समर्थस्थापित मारुती, Masur:</td>	
						<td> 18.3 KM </td>								
						<td> 00 h 27 min</td>
					</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>	
						<td> 21.9 KM </td>								
						<td> 00 h 32 min</td>
					</tr>
                     <tr>
						<td>From Bahe Borgav Cha Maruti:</td>	
						<td> 53.1 KM </td>								
						<td> 00 h 02 min</td>
					</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>	
						<td> 66.7 KM </td>								
						<td> 00 h 01 min</td>
					</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>	
						<td> 86.2 KM </td>								
						<td> 00 h 23 min</td>
					</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>	
						<td> 87.6 KM </td>								
						<td> 00 h 24 min</td>
					</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>	
						<td> 101 KM </td>								
						<td> 01 h 31 min</td>
					</tr>                                        				
				</tbody>
				</table>
			
				</div>	


               <!--<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Daas Maruti, Chaphal</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Karad Airport:</td>
						<td width="60">22.2 KM</td>
						<td>00 h 30    min</td>								
						</tr>
				
					<tr>
						<td>From Khadicha Maruti, Shinganwad:</td>
						<td>2.3 KM</td>
						<td>00 h 04 min</td>								
						</tr>
				
					<tr>
						<td>From Veer Maruti, Chaphal:</td>
						<td>1.7 KM</td>
						<td>00 h 09 min</td>	
						</tr>
                    
                    <tr>
						<td>From Daas Maruti, Chaphal:</td>
						<td>200 M</td>
						<td>00 h 03 min</td>	
						</tr>
                    <tr>
						<td>From Mathatil Maruti, Umbraj:</td>
						<td>10.8 KM</td>
						<td>00 h 18 min</td>	
						</tr>
                    <tr>
						<td>From समर्थस्थापित मारुती, Masur:</td>
						<td>18.2 KM</td>
						<td>00 h 26 min</td>	
						</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>21.8 KM</td>
						<td>00 h 32 min</td>	
						</tr>
                     <tr>
						<td>From Baahe Borgav Cha Maruti:</td>
						<td>53 KM</td>
						<td>01 h 01 min</td>	
						</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>66.6 KM</td>
						<td>01 h 01 min</td>	
						</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>86.2 KM</td>
						<td>01 h 23 min</td>	
						</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>87.5 KM</td>
						<td>01 h 24 min</td>	
						</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>
						<td>101 KM</td>
						<td>01 h 30 min</td>	
						</tr>                                        				
				</tbody>
				</table>
			
				</div>	--> 
               	</div>						
			</li>
			
			<li id="umbraj"> 
					<ul class="route">
					<li> Chaphal</li>
					<li class="car"> 0 Hours 29 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 20</li>
					<li> Umbraj</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Mathatil Maruti, Umbraj</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Karad Airport:</td>
						<td width="60">18.6 KM</td>
						<td>00 h 22    min</td>								
						</tr>
				
					<tr>
						<td>From Maruti Mandir, Majgaon:</td>
						<td>9.8 KM</td>
						<td>00 h 15 min</td>								
						</tr>
				
					<tr>
						<td>From Khadicha Maruti, Shinganwadi:</td>
						<td>13.7 KM</td>
						<td>00 h 27 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>12.2 KM</td>
						<td>00 h 19 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>12.1 KM</td>
						<td>00 h 19 min</td>	
						</tr>
                    <tr>
						<td>From समर्थस्थापित मारुती, Masur:</td>
						<td>9.5 KM</td>
						<td>00 h 16 min</td>	
						</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>13.1 KM</td>
						<td>00 h 21 min</td>	
						</tr>
                     <tr>
						<td>From Bahe Borgav Cha Maruti:</td>
						<td>44.3 KM</td>
						<td>00 h 51 min</td>	
						</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>57.9 KM</td>
						<td>00 h 51 min</td>	
						</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>77.5 KM</td>
						<td>01 h 13 min</td>	
						</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>78.8 KM</td>
						<td>01 h 13 min</td>	
						</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>
						<td>92.1 KM</td>
						<td>01 h 20 min</td>	
						</tr>                                        				
				</tbody>
				</table>
				

				</div>
				</div>
			</li>
			
			<li id="masur">
					<ul class="route">
					<li> Umbraj</li>
					<li class="car"> 0 Hours 12 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 8.1</li>
					<li> Masur</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>समर्थस्थापित मारुती, Masur </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Karad Airport:</td>
						<td width="60">22.8 KM</td>
						<td width="92">00 h 26    min</td>								
						</tr>
				
					<tr>
						<td>From Maruti Mandir, Majgaon:</td>
						<td>15.9 KM</td>
						<td>00 h 21 min</td>								
						</tr>
				
					<tr>
					  <td>From Khadicha Maruti, Shinganwadi:</td>
						<td>19.8 KM</td>
						<td>00 h 33 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>18.3 KM</td>
						<td>00 h 26 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>18.2 KM</td>
						<td>00 h 25 min</td>	
						</tr>
                    <tr>
                      <td>From Mathatil Maruti, Umbraj:</td>
						<td>8.1 KM</td>
						<td>00 h 12 min</td>	
						</tr>
                    <tr>
						<td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>3.9 KM</td>
						<td>00 h 06 min</td>	
						</tr>
                     <tr>
						<td>From Bahe Borgav Cha Maruti:</td>
						<td>38.7 KM</td>
						<td>00 h 54 min</td>	
						</tr>
 					<tr>
						<td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>55.1 KM</td>
						<td>00 h 54 min</td>	
						</tr>                    
					<tr>
						<td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>74.7 KM</td>
						<td>01 h 16 min</td>	
						</tr>
 					<tr>
						<td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>76 KM</td>
						<td>01 h 16 min</td>	
						</tr>  
					<tr>
						<td>From Kolhapur Airport:</td>
						<td>89.3 KM</td>
						<td>01 h 23 min</td>	
						</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>
			<li id="shahapur">
					<ul class="route">
					<li> Masur</li>
					<li class="car"> 0 Hours 6 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 3.9</li>
					<li> Shahapur </li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> पहिला प्रताप मारुती, Shahapur</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
					  <td>From Karad Airport:</td>
						<td width="60">15.1 KM</td>
						<td width="92">00 h 24    min</td>								
						</tr>
				
					<tr>
					  <td>From Maruti Mandir, Majgaon:</td>
						<td>19.5 KM</td>
						<td>00 h 26 min</td>								
						</tr>
				
					<tr>
					  <td>From Khadicha Maruti, Shinganwadi:</td>
						<td>23.4 KM</td>
						<td>00 h 38 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>21.9 KM</td>
						<td>00 h 30 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>21.9 KM</td>
						<td>00 h 30 min</td>	
						</tr>
                    <tr>
                      <td>From Mathatil Maruti, Umbraj:</td>
						<td>11.7 KM</td>
						<td>00 h 17 min</td>	
						</tr>
                    <tr>
                      <td>From समर्थस्थापित मारुती, Masur:</td>
						<td>3.9 KM</td>
						<td>00 h 06 min</td>	
						</tr>
                     <tr>
                       <td>From Bahe Borgav Cha Maruti:</td>
						<td>36.1 KM</td>
						<td>00 h 50 min</td>	
						</tr>
 					<tr>
 					  <td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>52.5 KM</td>
						<td>00 h 50 min</td>	
						</tr>                    
					<tr>
					  <td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>72.1 KM</td>
						<td>01 h 12 min</td>	
						</tr>
 					<tr>
 					  <td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>73.4 KM</td>
						<td>01 h 13 min</td>	
						</tr>  
					<tr>
					  <td>From Kolhapur Airport:</td>
						<td>86.7 KM</td>
						<td>01 h 20 min</td>	
						</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>	
            
			<li id="borgaon">
					<ul class="route">
					<li> Shahapur</li>
					<li class="car">0 Hours 50 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km">36</li>
					<li>Bahe </li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Bahe Borgav Cha Maruti</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
					  <td>From Karad Airport:</td>
						<td width="60">28.9 KM</td>
						<td width="92">00 h 36    min</td>								
						</tr>
				
					<tr>
					  <td>From Maruti Mandir, Majgaon:</td>
						<td>50.7 KM</td>
						<td>00 h 59 min</td>								
						</tr>
				
					<tr>
					  <td>From Khadicha Maruti, Shinganwadi:</td>
						<td>54.6 KM</td>
						<td>01 h 10 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>53.1 KM</td>
						<td>01 h 02 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>53.1 KM</td>
						<td>01 h 02 min</td>	
						</tr>
                    <tr>
                      <td>From Mathatil Maruti, Umbraj:</td>
						<td>42.9 KM</td>
						<td>00 h 49 min</td>	
						</tr>
                    <tr>
                      <td>From समर्थस्थापित मारुती, Masur:</td>
						<td>38.6 KM</td>
						<td>00 h 53 min</td>	
						</tr>
                     <tr>
                       <td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>36 KM</td>
						<td>00 h 50 min</td>	
						</tr>
 					<tr>
 					  <td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>26.4 KM</td>
						<td> 00 h 33 min</td>	
						</tr>                    
					<tr>
					  <td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>42.5 KM</td>
						<td>00 h 53 min</td>	
						</tr>
 					<tr>
 					  <td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>43.8 KM</td>
						<td>00 h 54 min</td>	
						</tr>  
					<tr>
					  <td>From Kolhapur Airport:</td>
						<td>57.2 KM</td>
						<td>01 h 01 min</td>	
						</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>	            
          <li id="shirala">
					<ul class="route">
					<li> Bahe </li>
					<li class="car"> 0 Hours 36 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 26.3</li>
					<li> Shirala</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>  Samarth Sthapit Maruti Mandir, Shirala</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
					  <td>From Karad Airport:</td>
						<td width="60">42.6 KM</td>								
						<td> 00 h 38 min</td>								
					</tr>
				
					<tr>
					  <td>From Maruti Mandir, Majgaon:</td>
						<td>64.4 KM</td>
						<td width="92">01 h 01    min</td>								
						</tr>
				
					<tr>
					  <td>From Khadicha Maruti, Shinganwadi:</td>
						<td>68.3 KM</td>
						<td>01 h 12 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>66.8 KM</td>
						<td>01 h 05 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>66.7 KM</td>
						<td>01 h 04 min</td>	
						</tr>
                    <tr>
                      <td>From Mathatil Maruti, Umbraj:</td>
						<td>56.6 KM</td>
						<td>00 h 51 min</td>	
						</tr>
                    <tr>
                      <td>From समर्थस्थापित मारुती, Masur:</td>
						<td>62.2 KM</td>
						<td>00 h 58 min</td>	
						</tr>
                     <tr>
                       <td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>52.7 KM</td>
						<td>00 h 54 min</td>	
						</tr>
 					<tr>
 					  <td>From Bahe Borgav Cha Maruti:</td>
						<td>26.3 KM</td>
						<td>00 h 36 min</td>	
						</tr>                    
					<tr>
					  <td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>22.1 KM</td>
						<td>00 h 41 min</td>	
						</tr>
 					<tr>
 					  <td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>49.1 KM</td>
						<td>00 h 48 min</td>	
						</tr>  
					<tr>
					  <td>From Kolhapur Airport:</td>
						<td>62.4 KM</td>
						<td>00 h 55 min</td>	
						</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>  
			
				<li id="pargaon">
					<ul class="route">
					<li>Shirala</li>
					<li class="car"> 0 Hours 40 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km">22.2</li>
					<li>June </li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Ramdas Swami Hanuman Temple June Pargaon</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
					  <td>From Karad Airport:</td>
						<td width="60">62.1 KM</td>
						<td width="92">01 h 01    min</td>								
						</tr>
				
					<tr>
					  <td>From Maruti Mandir, Majgaon:</td>
						<td>83.9 KM</td>
						<td>01 h 24 min</td>								
						</tr>
				
					<tr>
					  <td>From Khadicha Maruti, Shinganwadi:</td>
						<td>87.7 KM</td>
						<td>01 h 35 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>86.3 KM</td>
						<td>01 h 27 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>86.2 KM</td>
						<td>01 h 27 min</td>	
						</tr>
                    <tr>
                      <td>From Mathatil Maruti, Umbraj:</td>
						<td>76.1 KM</td>
						<td>01 h 13 min</td>	
						</tr>
                    <tr>
                      <td>From समर्थस्थापित मारुती, Masur:</td>
						<td>81.7 KM</td>
						<td>01 h 20 min</td>	
						</tr>
                     <tr>
                       <td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>72.1 KM</td>
						<td>01 h 16 min</td>	
						</tr>
 					<tr>
 					  <td>From Bahe Borgav Cha Maruti:</td>
						<td>42.5 KM</td>
						<td>00 h 57 min</td>	
						</tr>                    
					<tr>
					  <td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>22.2 KM</td>
						<td>00 h 40 min</td>	
						</tr>
 					<tr>
 					  <td>From Manapadale Maruti Temple, Manpadle:</td>
						<td>6.7 KM</td>
						<td>00 h 17 min</td>	
						</tr>  
					<tr>
					  <td>From Kolhapur Airport:</td>
						<td>31.6 KM</td>
						<td>00 h 38 min</td>	
						</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>
				
			</li>              


			<li id="manpadle">
					<ul class="route">
					<li>Pargaon</li>
					<li class="car"> 0 Hours 17 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 6.7</li>
					<li> Manpadle </li>
					</ul>
	<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5> Manapadale Maruti Temple, Manpadle</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
					  <td>From Karad Airport:</td>
						<td width="60">63.2 KM</td>
						<td width="92">01 h 00    min</td>								
						</tr>
				
					<tr>
					  <td>From Maruti Mandir, Majgaon:</td>
						<td>85.1 KM</td>
						<td>01 h 23 min</td>								
						</tr>
				
					<tr>
					  <td>From Khadicha Maruti, Shinganwadi:</td>
						<td>88.9 KM</td>
						<td>01 h 35 min</td>	
						</tr>
                    
                    <tr>
                      <td>From Veer Maruti, Chaphal:</td>
						<td>87.4 KM</td>
						<td>01 h 27 min</td>	
						</tr>
                    <tr>
                      <td>From Daas Maruti, Chaphal:</td>
						<td>87.4 KM</td>
						<td>01 h 28 min</td>	
						</tr>
                    <tr>
                      <td>From Mathatil Maruti, Umbraj:</td>
						<td>77.3 KM</td>
						<td>01 h 14 min</td>	
						</tr>
                    <tr>
                      <td>From समर्थस्थापित मारुती, Masur:</td>
						<td>82.8 KM</td>
						<td>01 h 21 min</td>	
						</tr>
                     <tr>
                       <td>From पहिला प्रताप मारुती,Shahapur(Near Karad):</td>
						<td>73.3 KM</td>
						<td>01 h 16 min</td>	
						</tr>
 					<tr>
 					  <td>From Bahe Borgav Cha Maruti:</td>
						<td>43.6 KM</td>
						<td>00 h 56 min</td>	
						</tr>                    
					<tr>
					  <td>From Samarth Sthapit Maruti Mandir, Shirala:</td>
						<td>49 KM</td>
						<td>00 h 48 min</td>	
						</tr>
 					<tr>
 					  <td>From Ramdas Swami Hanuman Temple June Pargaon:</td>
						<td>6.7 KM</td>
						<td>00 h 17 min</td>	
						</tr>  
					<tr>
					  <td>From Kolhapur Airport:</td>
						<td>29.3 KM</td>
						<td>00 h 35 min</td>	
						</tr>
                    
				
				</tbody>
				</table>
				

				</div>
				
				</div>			
				
				
			</li> 
			<li id="kolhapur">
					<ul class="route">
					<li> Manpadle</li>
					<li class="car"> 0 Hours 98 Min</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 29.3</li>
					<li> Kolhapur</li>
					</ul>
				
				
				
			</li>                    

            
            		
		</ul>	
		
	</div>
	
<?php 

}
?>


	
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2oN6IZ8rywS4dflnIdWupkpOY2FLK0Bx';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
	<?php echo $footerBlock; ?>
		  </div>
		  </body>
		  </html>