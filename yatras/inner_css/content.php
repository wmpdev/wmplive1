
<?php include'header.php';?>

<div class="container single-temple">
<div class="span10">
<h2>&nbsp;</h2>
<h2>TEMPLE</h2>
<h2>&nbsp;</h2>
 <div id="slider1_container" style="position: relative; margin:0 auto; width: 800px;
        height: 456px;  overflow: hidden;">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(img/gallery/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 800px; height: 356px; overflow: hidden;">
            <div>
                <img u="image" src="img/gallery/1.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_01.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/2.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_02.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/3.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_03.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/4.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_04.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/5.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_05.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/6.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_06.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/7.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_07.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/8.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_08.jpg" />
            </div>
            <div>
                <img u="image" src="img/gallery/9.jpg" />
                <img u="thumb" src="img/gallery/thumb/thumb_09.jpg" />
            </div>
            
        </div>
        
        <!-- Arrow Navigator Skin Begin -->
        <style>
            /* jssor slider arrow navigator skin 05 css */
            /*
            .jssora05l              (normal)
            .jssora05r              (normal)
            .jssora05l:hover        (normal mouseover)
            .jssora05r:hover        (normal mouseover)
            .jssora05ldn            (mousedown)
            .jssora05rdn            (mousedown)
            */
            .jssora05l, .jssora05r, .jssora05ldn, .jssora05rdn
            {
            	position: absolute;
            	cursor: pointer;
            	display: block;
                background: url(../img/a17.png) no-repeat;
                overflow:hidden;
            }
            .jssora05l { background-position: -10px -40px; }
            .jssora05r { background-position: -70px -40px; }
            .jssora05l:hover { background-position: -130px -40px; }
            .jssora05r:hover { background-position: -190px -40px; }
            .jssora05ldn { background-position: -250px -40px; }
            .jssora05rdn { background-position: -310px -40px; }
        </style>
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora05l" style="width: 40px; height: 40px; top: 158px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora05r" style="width: 40px; height: 40px; top: 158px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- Thumbnail Navigator Skin Begin -->
        <div u="thumbnavigator" class="jssort01" style="position: absolute; width: 800px; height: 100px; left:0px; bottom: 0px;">
            <!-- Thumbnail Item Skin Begin -->
            <style>
                /* jssor slider thumbnail navigator skin 01 css */
                /*
                .jssort01 .p           (normal)
                .jssort01 .p:hover     (normal mouseover)
                .jssort01 .pav           (active)
                .jssort01 .pav:hover     (active mouseover)
                .jssort01 .pdn           (mousedown)
                */
                .jssort01 .w {
                    position: absolute;
                    top: 0px;
                    left: 0px;
                    width: 100%;
                    height: 100%;
                }

                .jssort01 .c {
                    position: absolute;
                    top: 0px;
                    left: 0px;
                    width: 68px;
                    height: 68px;
                  
                }

                .jssort01 .p:hover .c, .jssort01 .pav:hover .c, .jssort01 .pav .c {
                    background: url(img/gallery/t01.png) center center;
                    border-width: 0px;
                    top: 2px;
                    left: 2px;
                    width: 68px;
                    height: 68px;
                }

                .jssort01 .p:hover .c, .jssort01 .pav:hover .c {
                    top: 0px;
                    left: 0px;
                    width: 70px;
                    height: 70px;
                    
                }
            </style>
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="position: absolute; width: 72px; height: 72px; top: 0; left: 0;">
                    <div class=w><thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0; left: 0;"></thumbnailtemplate></div>
                    <div class=c>
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Thumbnail Navigator Skin End -->
 
        <!-- Trigger -->
        <script>
            jssor_slider1_starter('slider1_container');
        </script>
    </div>
<p>&nbsp</p>
<p>Shirdi is a town in the Ahmednagar district of the state Maharashtra in India.</p>
<p> It is located at 19° 45' N, 74° 25' E, 83 km north of the city of Ahmednagar and at a distance of 15 km from the town Kopargaon. 
It has acquired fame as a place of pilgrimage because the Shirdi Sai Baba lived there and is buried there.</p>
<p>Sri Sai Baba, a personification of spiritual perfection and an epitome of compassion, lived in the little village of Shirdi in the state of Maharashtra (India) for sixty years. </p>
<p>He reached Shirdi as a nameless entity. </p>
<p>One of the persons who first came in contact with him at Shirdi addressed him spontaneously as ‘Sai’ which means Savior, Master or Saint. ‘Baba’ means father as an expression of reverence. </p>
<p>In the Divine play it was designed as such, that he subtly inspired this ....erson to call him by this name, which was most appropriate for his self-allotted mission.</p>
<p>According to Researchers, Sai Baba’s real name was Haribhau Mulari. His birth place was Pathari village from where he came to Shirdi during his childhood.</p>
<p>Saibaba had immense faith in Ekeshwarvaad (One God) theory which was bases on Adwaitwadi (non-dualist) philosophy. He used to say ‘Sab ka Malik Ek’ which means ‘There is one master (God) for all’. He heard Kabir’s coupletsalso with great devotion.</p>
<p>He died on 15th October 1918.</p>


	<div class="easyui-tabs" style="height:auto">
		<div title="Story" style="padding:10px">
			<p style="font-size:14px">
			A popular website of Sai Baba states that Baba was born in a Hindu family to a couple named Gangabhavadya and Devagiriamma. In the former Nizam's [Maharaj] dominion, there was a remote village called Pathri. In that village there were a couple named Gangabhavadya and Devagiriamma were grieving over the lack of children. In answer to their prayers, a son was born on September 28, 1835 who we today remember as Shirdi Sai Baba. As Gangabhavadya had developed a feeling of total detachment and renunciation, he decided to retire to a forest regardless of the child. Devagiriamma, who looked upon her husband as God, decided to follow the husband leaving the child.</p>
			<p>In the same village a Sufi fakir who was also childless took charge of this child and brought him up in his home. The boy stayed in the fakir's home for four years (1835 to 1839). The fakir passed away in the tide of time. The fakir's wife, who had done for the child, was grief-stricken. To add to her woes, the boy was behaving in a troublesome manner. In those days, Hindu-Muslim differences in that area was growing alarmingly.</p>
			<p>As a child Shirdi Sai Baba was extraordinary.In those days of bitterness between members of the two communities, Shirdi Sai Baba used to sing about Allah in a Hindu temple and about Rama and Shiva in a mosque.This was certainly a puzzle to the public.People started grousing about Shirdi Sai to the Fakir’s wife.Unable to deal with the situation, fakir’s wife handed over the child to a high-souled, pious scholar named Venkusa, who was living near her house. The boy stayed in Venkusa's ashram for 12 years from 1839 to 1851. Venkusa was extremely fond of the boy. He used to give priority to the young Baba's views. Seeing this, members of the ashram developed hatred towards the boy. Finally one night in 1851, the boy left the ashram. He reached Shirdi, a very small village at the time. He stayed there for barely two months and then went about wandering from place to place.</p>
			
		</div>
		<div title="Mythological Beliefs" style="padding:10px">
			<ul class="" style="list-style:circle;text-align:center">
			<li>According to Saibaba’s followrs, Shirdi not only rids a person of the worries of the material world but also shows the path to the divine world.</li>
			<li>Saibaba believedthat he who stepped on the land of Shirdi would be freed of all his sorrows.</li>
			<li>Those visiting the Samadhi of Saibaba are automatically relieved of their worries and they leave the place full of happiness and spiritual bliss.</li>
			<li>Saibaba’s magical powers have helped many people and their desires have been fulfilled</li>
			<li>The Hindu devotees saw Vitthal in him while the Muslims believed him to be Qutub-e-Irshad.</li>
			<li>Shri Bidkar Maharaj conferred the title of Jagatguru to Baba.</li>
			</ul>

			
		</div>
		<div title="LEGEND" data-options="" style="padding:10px">
			<p style="font-size:14px">About Shree Saibaba</p>
			<p>Chandbhai, the headman of a village called Dhoopkhede (in Aurangabad, India), once lost his horse and was looking for it. Suddenly he heard a voice say "You look tired. come here and rest a while". He turned around and saw a young Fakir . The fakir smiled at him and said "What are you looking for in this jungle, Chandbhai". Chandbhai was surprised and he wondered how the fakir knew his name. Slowly, he said "I have lost my horse. I have looked for it everywhere, but cannot seem to find it". The fakir told him to look behind a clump of trees. Chandbhai was pleasantly surprised to find his horse grazing peacefully behind those trees. He thanked the fakir and asked his name. The fakir said "some people call me Sai Baba."</p>
			<p>Sai Baba then invited Chandbhai to have a smoke with him. He got the pipe ready, but there was no fire to light it with. Sai Baba thrust a pair of tongs into the ground and brought out a burning coal. Chandbhai was wonderstruck. He thought "this is no ordinary person" and invited Baba to come to his house and be his guest for a few days.</p>
			<p>Next day Baba went to Chandbhai's house and found everybody in a very joyful mood and festivities going on all around. He found that Chandbhai's wife's nephew was getting married. The bride was from Shirdi and the marriage party was going to Shirdi. Chandbhai invited Baba to accompany the marriage party to Shirdi. At Shirdi they camped in a field next to Khandoba's temple.</p>
			<p>After the wedding, Saibaba stayed on at Shirdi. At first he lived under a neem tree and begged for food whenever he needed it. He then went to Khandoba's temple, intending to stay there, but the temple priest met him at the entrance and told him to go to the mosque. That is how Baba, started staying at the Mosque which was later called Dwarkamayi.</p>
			<p>Baba preached at Shirdi all his life and performed numerous miracles to convince people that God exists. He healed people's diseases, provided moral and material comfort to his devotees. Baba helped bring Unity and Harmony between all communities. He said that God is one, but called by different names. He said follow your own religion and seek the truth.</p>
			<p>One day a rich millionaire named Booty came to Sai Baba and said he was going to construct a stone building for Shri Krishna. Baba helped him plan the building. Before the building was completed Baba fell very ill. On the 15th of October 1918, he breathed his last. His last wish was to be buried in Booty's building.</p>
			<p>Booty's stone building came to be known as the Samadhi Mandir. Shri Sai Baba was buried here and a beautiful shrine was built over it. To this day, people flock to Shirdi to pay homage to Shri Sai Baba</p>
			
			
		</div>
		
		<div title="Historical facts" data-options="" style="padding:10px">
			<ul>
			<li>Since ancient times, the region has been relatively liberal from the religious point of view. Therefore saints and Sufis have been active in this areas.</li>
			<li>In the middle ages (1336 A.D.), the Bahmani dynasty was founded who were followers of the Shia sect and were not influenced by the fundamentalist North Indian Sunnis.</li>
			<li>The region came under the Mughals during the reign of Shahjahan.</li>
			<li>According to local beliefs, Saibaba participated in the first independence struggle of 1857 with Rani Laxmibai and Tatya Tope.</li>
			<li>In 1916 A.D. Govindrao Raghunath Dabholkar wrote a book called Shri Sai Santcharit on the religious beliefs of Saibaba.</li>
			</ul>

			
		</div>
		
		<div title="Reasons to Visit" data-options="" style="padding:10px">
			<ul>
			<li>Shirdi lies on the Ahmednagar-Manmad Highway, 250 km from Mumbai and 90 km from Nashik.</li>
			<li>Shirdi was the house of the saint Sai Baba, who lived here</li>
			<li>Shirdi Sai Baba temple is now one of the most visited temples in India after Tirupati Balaji temple in Andhra Pradesh and Vaishnodevi temple in Jammu area.</li>
			<li>Guru Poornima, Dussehra and Ramnavami are the major festivals celebrated in Shirdi.</li>
			<li>The Samadhi Mandir of Shirdi opens for the whole night only on these occasions</li>
			</ul>

			
		</div>
		
		<div title="Tips and Suggestions  " data-options="" style="padding:10px">
			<p style="font-size:14px">Guidance for Devotees (When in temple Premises)
			<ul>
			<li>The shree saibaba sansthan trust mobile cloakroom" where devotees have to deposit their mobiles at a nominal charge of rs.2/- per mobile. They get a token in return against which they can get their mobiles back, as mobiles / cameras are not allowed inside the temple premises.</li>
			<li>On arrival at Shirdi,devotees should immediately get in touch with one of the Niwassthan Office for booking of their accommodation which will be made available after registration of name and other particulars. No deposit is required for accommodation.</li>
			<li>Occupied room(s) should be locked before going out.</li>
			<li>If the accommodation provided is in the common hall, luggage should always be left in charge of a member of the party to safeguard it from antisocial elements.</li>
			<li>A locker is available at nominal charge, which may be booked at the Niwassthan Office and used for keeping luggage or any other valuables.</li>
			<li>Care should be taken not to leave any belongings behind in the toilets and the bathrooms.</li>
			<li>As the Samadhi Mandir is overcrowded at the time of Aarati, devotees should safeguard their ornaments and purses from pick pocketers and take care of their children.</li>
			<li>Devotees should note that all religious functions and Poojas in the Shri SaiBaba Sansthan Trust, Shirdi premises are to be arranged and performed through the office of the Shri SaiBaba Sansthan Trust, Shirdi. Necessary payments for these are to be made at the office against a receipt. Boxes have been provided by the Shri SaiBaba Sansthan Trust, Shirdi in the Mandir itself to receive the devotees' offerings by way of Dakshina and Donation.</li>
			<li>Offering to Shri Sai Baba in cash or kind can also be made at the donation counter against a receipt.</li>
			<li>Devotees should bear in mind that Baba left no heirs or disciples and should guard themselves against such deception practiced by impostors.</li>
			<li>As all the necessary assistance and guidance is readily available to the devotees at the Niwassthan Office of the Shri SaiBaba Sansthan Trust, Shirdi assistance from unauthorized guides at the S. T. stand, if taken by the devotees, will be at their own risk.</li>
			<li>Devotees are warned against practitioners of black magic professing allegiance to Shri Sai Baba and also against those circulating chain letters, asking the receiver to send a certain number of copies of the letter to his friends.</li>
			<li>Literature about Shri Sai Baba containing authentic information in various languages has been published by the Shri SaiBaba Sansthan Trust, Shirdi at reasonable prices and is readily available at its Book shops near the Samadhi Mandir.</li>
			<li>Donations should always be sent by Money Orders, Postal Orders, Crossed and A/c. Payee Cheques or Drafts to ensure safe delivery of the same. Do not send cash or currency notes in postal envelopes. online donation also accepted through http://online.sai.org.in</li>
			<li>As the various dealers and vendors of Pooja articles are not connected with the Shri SaiBaba Sansthan Trust, Shirdi, devotees should first fix the price before buying these articles to avoid any trouble thereafter. A complaint/suggestion book is always kept in the office for the use by the devotees, in which they are requested to write clearly their complaints/suggestions along with their full names and addresses. The Shri SaiBaba Sansthan Trust, Shirdi authorities take due note of such complaints/suggestions.</li>
			<li>All donations for oil for the Nanda-deep and for firewood for Dhuni in the Dwarkamai are to be given in the Donation office only. Further details regarding these can be obtained from the Temple incharge.</li>
			<li>Devotees desirous of feeding the poor can arrange to do so against cash payments to be made to the Prasadalaya itself or the Account office. Coupons are not accepted for this purpose.</li>
			<li>Devotees desirous of distributing alms to beggers can do so only in the beggar`s shed,near Prasadalaya with the help of the Prasadalaya In charge.</li>
			<li>Devotees making correspondence should give their complete and correct addresses.</li>
			<li>Devotees are requested to avail of the facilities at the Tea canteen and Prasadalaya run by the Shri SaiBaba Sansthan Trust, Shirdi where tea and meals are served at subsidized rates.</li>
			<li>Medical facilities are available at the well-equipped Hospital run by the Shri SaiBaba Sansthan Trust, Shirdi.</li>
			<li>The Shri SaiBaba Sansthan Trust, Shirdi has not appointed any representative for the purpose of collecting donations in cash or otherwise. Devotees are informed that no donations either in cash or otherwise should be given to anybody personally. This is to avoid deceit as well as to prevent devotees from being cheated by unscrupulous elements</li>
			<li>The 'Holy Padukas' (foot-wears) of Lord Shri Sai Baba are only with the Shri SaiBaba Sansthan Trust, Shirdi, kept for darshan in Sai Baba Mandir; and not elsewhere.</li>
			<li>Sai Devotees are requested that they should not keep of their foot wears anywhere except Chappal stand. it is near Darshan 'Q' entrance.</li>
			<li>Sai Devotees are requested not to carry their cell phones in Samadhi Mandir. Please keep it in lockers provided by Sansthan in Mandir Premises.Do not hand over it to unknown Person.</li>
			<li>All the devotees, especially women and diabetic persons, are requested to eat some food before joining the darshan queue as during heavy rush days’ time required for darshan will be from 2 to 3 hours.</li>
			</ul>
</p>
			
		</div>
		
		<div title="Places to Visit" data-options="" style="padding:10px">
			<ul>
			<li><b>Samadhi Mandir </b>:  This is the centre of all spiritual activities in Shirdi. Here, four aartis of Baba are performed every day. On Thursdays, a procession of Baba’s charan-paduka (wooden sandles) is carried out which ends at Dwarka Mai. The temple houses an idol of Baba seated on a silver throne.</li>
			<li><b>Dwarka Mai </b>: It is said that Baba spent most of his time in this old mosque. A wood fire (Dhoomi) in the name of Baba is constantly lit whose ash (Vibhuti) is taken by devotees. The ash is believed to have magical, curative powers against diseases.</li>
			<li><b>Guru Sthan </b>: Close to the Samadhi Mandir is the sacred neem tree under which Baba use to meditate. A small temple called Guru Sthan has been built here. The temple has a Shivling and a photograph of Baba. It is more crowded on Thursdays.</li>
			<li><b>Shri Saibaba Sthan </b>: Here all the information, books and photographs on Baba’s life and teachings are available. The simplicity and cleanliness of the place is something that invites visitors’ attraction.</li>
			<li><b>Lendi Baug</b> : Baba spent a good part of his time in this garden which has the Shiwadi well, used by Baba. Here, a lamp is constantly kept lit under a peepal tree.</li>
			<li><b>Chavadi </b>: The Chavadi is a small house situated close to the Masjid. Sai Baba spent every alternate night over here. One can find here the 'Asan' or seat on which he used to sit, a wooden plank and a wheel chair owned by Sai Baba. His body was bathed here for the last time before his funeral.</li>
			<li><b>Khandoba Temple </b>: The Khandoba temple is situated at the Ahmednagar-Kopergaon road and is dedicated to Lord Shiva. It is among the oldest temples of Shirdi. It was at this temple that the priest called Baba with the name "Aao, Sai" hence he became well known as Sai Baba.</li>
			<li><b>Dixit Wada Museum </b>: The Museum is situated in the center of the Sansthan complex and contains rare black and white photographs of Sai Baba. Other articles like Baba's 'Kafni' (long robes) and his leather Padukas, gramophone records, smoking pipe, water tumblers, bathing stone and cooking utensils which were used by Sai Baba are on display. The museum remains open from 10 am to 6 pm.</li>
			<li><b>Thursday Palki Procession </b>: Shri Sai Baba used to sleep in Chawri every alternate night. This visit at Chawri was prefaced by a 'palki' procession and Sai Baba was accompanied from Dawakamai to Chawri. Even nowadays this palki procession is carried out with the same enthusiasm and adoration. The procession starts from Samadhi Temple to Dwarkamai Masjid from here it goes to Chawri and returns to Samadhi Temple. Shej Aarti is presented to Shri Sai Baba after the procession.</li>
			<li><b>Wet N Joy Water Park</b>: The ‘Wet N’ Joy’ water park at Shirdi is a premier family amusement park for the pilgrims who visit Shirdi to have the darshan of Saibaba For those who have gone visiting several sacred places like Laxmibai wada, Lendi park, Dixit museum etc would be tired of moving from place to place and doing saastanga namaskar to the various gods in the temples and at Sai sannidhi, they really need a place of amusement to get rid of the physical and mental strain or stress. For such needy persons as well as other amusement seekers here is a fortune gift of the authorities to enjoy in ‘Wet N Joy’ water park. It’s just 2 kilometers away from Shirdi.</li>
			<li><b>Maruti Mandir</b>:  Dakshian Mukhi Maruti Mandir is one of the must visit Mandirs at Shirdi. It is a temple facing south,  Priest say that there are very few temples of Hanuman Ji facing South. When you visit Shirdi, it is better to have Darshan of Lord Hanuman at the temple nearby. You have to go down the lane between Dwarka Mai and Chavadi to see the Hanuman Mandir. It is one of the oldest temples in Shirdi.</li>
			</li>

			
		</div>
		
		<div title="How to reach there" data-options="" style="padding:10px">
			<ul>
			<li><b>Nearby Airport</b>: The nearest International Airport is Chhatrapati Shivaji International Airport, Mumbai, roughly four and a half hour drive from Shirdi.</li>
			<li><b>Nearby Railway Station</b>: Shirdi has its own Railway Station named as Shirdi Railway Station which is connected to the major cities of Maharashtra and is 10 Kms from Sai Baba temple. It is well connected to various major cities such as Pune, Mumbai, Ahmedabad, Hyderabad, Nagpur, Chandigarh, Lucknow, Kanpur, Bangalore, Nasik, Thane, Nagpur and Aurangabad.</li>
			<li><b>Road Ways</b>: There are many ways to reach Shirdi. Shirdi is 35 Kms from Pangri, 56 Kms from Sinnar, 86 Kms from Ahmednagar, 216 Kms from Nasik, 188 Kms from Pune, 233 Kms from Vashi and 238 Kms from Mumbai and is very well connected through Maharashtra State Road Transport Corporation (MSRTC) and some private travel services.</li>
			</li>
			

			
		</div>
		
		<div title="Excursion" data-options="" style="padding:10px">
			<ul>
			<li><b>Haregaon Shrine (30 kms)</b>:St. Theresa Church, Haregaon is the sacred place of Mauli Shrine and the annual yaatra held in her honour every September.</li>
			<li><b>Minerals Museum (60 kms)</b>:Gargoti is situated on Nashik - Shirdi road and is the only museum in the country which houses such a large collection of crystalline minerals. It is owned privately and represents a private collection of over 30 years of Mr Pandey.</li>
			<li><b>Shani Shinganapur (73 kms)</b>:Shingnapur is renowned for its Shani Temple popularly known as Shani Shinganapur Temple. It is renowned for the fact that no house in the village has locked doors. Despite this, no theft is reported in the village</li>
			<li><b>Ozar Ashtavinayak Temple (100 kms)</b>:Shrivighnahar, Ozhar is home to one of the eight Ashtavinayak Temples. It is visited alongwith Girijatmaj, Lenyadri Ashtavinayak Temple.</li>
			<li><b>Lenyadri Ashavinayak Temple (100 kms)</b>:Lenyadri Ashtavinayak Temple is one of the eight Ashtavinayak temples. The temple is set in a cave and and only one of the eight temples that requires climbing some steps to reach.</li>
			</ul>

			
		</div>
		
		<div title="General Information" data-options="" style="padding:10px">
			<ul>
			<li><b>STD Code</b> :02423</li>
			<li><b>Puja Timings</b> :	5.00 a.m.-12.00 p.m., 4.00 p.m.-10.00 p.m.</li>
			<li><b>Police Assistance</b> :	+91-2423-255133 / 222333</li>
			<li><b>Ambulance/Hospital</b> :	Civil Hospital, Savedi Road, Tel: +91-241-2431018</li>
			<li><b>Currency Exchange</b> :	Syndicate Bank, Pilgim’s Inn, M.T.D.C. Limited, Pimpalwadi Road, Shirdi-423109 Maharashtra Tel: +91-2423-255114</li>
			<li><b>ATM Centre</b> :	State Bank of India, Kopargaon Tel: +91-2423-222270,221366</li>
			<li><b>Post Office</b> :	Head Post Office, Kopargaon Tel: +91-2423-222270</li>
			<li><b>Tourist Office</b> :	M.T.D.C., Shirdi, Tel: +91-2423-255194-96; Fax: +91-2423-256103</li>
			<li><b>Shopping</b> :	Sai literature, photographs, CDs can be bought from Sai Store near Dwarka Mai.</li>
			</ul>

			
		</div>
		
	</div>

	
</div>
<div class="span2">
		<br/>
		<br/>
	
<h3>Popular Temples</h3>
		
<img src="img/temple/1.jpg" alt="">
<h5>The golden temple</h5>
<br/>
<img src="img/temple/1.jpg" alt="">
<h5>Shree Sidhivinayak temple</h5>
<br/>
<img src="img/temple/1.jpg" alt="">
<h5>Sai Shirdi</h5>
<br/>
<img src="img/temple/1.jpg" alt="">
<h5>Shree Mahalakshmi Temple</h5>
				

</div>

</div>

<?php include'footer.php';?>