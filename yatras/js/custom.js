/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* MAIN JAVASCRIPT 
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
(function($) {
    "use strict";
    /*****************************
    WHEN PAGE IS LOADING
    *****************************/
    $(window).load(function () {
    	//PAGE LOADER
	    $("#loader").hide(1000);
	    //PORTFOLIO
	    var $container = $('#portfolio-container');
		// initialize
		$container.isotope({
		  itemSelector: '.portfolio-item'
		});
		$('#portfolio-filters').on( 'click', 'button', function() {
		  var filterValue = $(this).attr('data-filter');
		  $container.isotope({ filter: filterValue });
		});
		scrollMenu();
	});
    /*****************************
    WHEN PAGE IS SCROLLING
    *****************************/
    $(window).scroll(function() {
    	/*CAROUSEL*/
	    if(window.innerWidth < 910){
	    	if(window.innerWidth < 670){
	    		var numbermaxitems = 1;
	    	}
	    	else{
		   		var numbermaxitems = 2;
		   	}
	    }
	    else{
		    var numbermaxitems = 4;
	    }
	    $('.flexslider').flexslider({
			animation: "slide",
			animationLoop: false,
			maxItems: numbermaxitems,
			selector: ".slides > li.blog-post", 
			itemWidth: 210, 
			itemMargin: 0,
			controlNav: false,    
			minItems: 0,    
			move: 0, 
			slideshow: false
		});
		$('#partners-slider').flexslider({
			animation: "slide",
			animationLoop: false,
			maxItems: numbermaxitems,
			selector: ".slides > li.partners-slide", 
			itemWidth: 210, 
			itemMargin: 0,
			controlNav: false,    
			minItems: 0,    
			move: 0, 
			slideshow: false
		});
		//ANIMATIONS
		scrollMenu();
	    if ($(window).scrollTop() > $("#header").height()) {
	        $('#scroll-top').fadeIn("1000");
	        $('#scroll-bottom').fadeOut();
	    } else {
	        $('#scroll-top').fadeOut();
	        $('#scroll-bottom').fadeIn("1000");
	    }
    });
    /*****************************
    OTHER JQUERY
    *****************************/
    /*BACKGROUND IOS*/
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	    $("#header.big").css('min-height', '600px');
	}
	/*ANIMATIONS*/
	var wow = new WOW({
	    boxClass:     'animate-me',      // animated element css class (default is wow)
	    animateClass: 'animated', // animation css class (default is animated)
	    offset:       0,          // distance to the element when triggering the animation (default is 0)
	    mobile:       false,       // trigger animations on mobile devices (default is true)
	    live:         true        // act on asynchronously loaded content (default is true)
	});
	wow.init();
	/*MENU DROPDOWN 1st LEVEL*/
	$('.menu-item-has-children').on('mouseenter',function(){
			$(this).find('.sub-menu').addClass('open animated');
	});
	$('.menu-item-has-children').on('mouseleave',function(){
			$(this).find('.sub-menu').removeClass('open animated');
	});
	/*MOBILE MENU*/
    $('#show-mobile-menu').on('click',function(){
        if($('#navigation-mobile').hasClass('display-nav-menu')){
            $('body').css('overflow', '');
            $('body').css({'right': '0'});
            $('body').css({'position': 'inherit'});
            $('#navigation-mobile').removeClass('display-nav-menu');
            $('#navigation #show-mobile-menu').removeClass('mobile-button-left');
        } else {
            $('body').css({'overflow': 'hidden'});
            $('body').css({'right': '-200px'});
            $('body').css({'position': 'relative'});
            $('#navigation-mobile').addClass('display-nav-menu');
            $('#navigation #show-mobile-menu').addClass('mobile-button-left');
        }
    });
	var data = $('#navigation').html();
	$('#navigation-mobile').html(data);
	/*HOME SLIDER*/
	var slider = $('#header-slider');
    slider.flexslider({
		animation: "slide",
		animationLoop: true,
		selector: ".slides > li", 
		controlNav: false,    
		slideshowSpeed: 10000,
		slideshow: true,
		keyboard: true,   
		directionNav: false,
    	controlsContainer: ".navigation-slider-container"
	});
	var sliderInfos = slider.data('flexslider');
	$('#sliderNext').on('click',function(){
        sliderInfos.flexAnimate(sliderInfos.getTarget("next"));
    });
    $('#sliderPrev').on('click',function(){
        sliderInfos.flexAnimate(sliderInfos.getTarget("prev"));
    });
    /*CONTENT IMAGE SLIDER*/
    $('.flexslider.image-slider').flexslider({
		animation: "slide",
		animationLoop: false,
		selector: ".slides > li",    
		slideshow: false
	});
    /*SEARCH FORM*/
	$('a#search-toggle').on('click',function() {
		$('#header #search-container').toggleClass("clicked");
		$('#header #search-toggle').toggleClass("clicked");
	});
	/*FAQS*/
	$('.faq').on('click',function() {
        $(this).find("h4.faq-link").toggleClass("faq-active");
        $(this).find(".faq-content").slideToggle(200);
    });
    /*PORTFOLIO*/
	$('.fancybox').fancybox({
        openEffect  : 'elastic'
    });
	/*SCROLL BOTTOM*/
	$('#scroll-bottom a').on('click',function(){
	    $("html, body").animate({ scrollTop: $(window).height()}, 600);
	    return false;
	});
	/*SCROLL TOP*/
	$('#scroll-top').on('click',function(){
	    $("html, body").animate({ scrollTop: 0}, 600);
	    return false;
	});
	/*ON SCROLL*/
	function scrollMenu() {
		var scroll = $(window).scrollTop();
	    if (scroll > ($(window).height() -20)) {
			$("#header.big #navigation").addClass('navigation-fixed');
	    } else {
			$("#header.big #navigation").removeClass('navigation-fixed');
	    }
	    if (scroll > ($("#header").height() -20)) {
			$("#header.small #navigation").addClass('navigation-fixed');
	    } 
	    else{
			$("#header.small #navigation").removeClass('navigation-fixed');
	    }
	    if (scroll > 20 & scroll < ($("#header").height() -20)) {
	        $("#header.small #navigation").fadeOut("100");
	    } else {
		    $('#header.small #navigation').fadeIn("1000"); 
	    }

	    if (scroll > 60 & scroll < $(window).height()) {
	        $("#header.big #navigation").fadeOut("100");
	    } else {
		    $('#header.big #navigation').fadeIn("1000"); 
	    }
	}
})(jQuery);


        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                },

                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $AutoCenter: 3,                                 //[Optional] Auto center thumbnail items in the thumbnail navigator container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 3
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
                    $SpacingX: 3,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 9,                              //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 260,                          //[Optional] The offset position to park thumbnail
                    $Orientation: 1,                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
                    $DisableDrag: false                            //[Optional] Disable drag or not, default value is false
                }
            };

            var jssor_slider2 = new $JssorSlider$("slider2_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider2.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider2.$ScaleWidth(Math.min(parentWidth, 600));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });