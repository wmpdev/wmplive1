
var directionsDisplay = [];
var directionsService = [];
var map = null;


function initialize()
{
	var mapOptions = {
       center: new google.maps.LatLng(21.1289956,82.7792201),
         zoom: -1,
        // mapTypeId: google.maps.MapTypeId.ROADMAP
     };
map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
directionsDisplay.setMap(map);
}
function calcRoute4() {
 // var msg = "22.777783,86.258951:9.2487964,78.6143377";
//  var msg = "30.316495,78.032192:30.743309,79.493763:30.734627,79.066894:30.994695,78.93984:30.639684,77.250366:31.935265,76.777954";
  var msg = "30.652913,79.0256581:30.5229102,79.0777138:30.284414,78.981141:30.6325757,78.9952754:30.5505524,79.5659633:30.5671193,79.542079";


  var input_msg = msg.split(":");
  var locations = new Array();

    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < input_msg.length; i++) {
        var tmp_lat_lng = input_msg[i].split(",");
        locations.push(new google.maps.LatLng(tmp_lat_lng[0], tmp_lat_lng[1]));
        bounds.extend(locations[locations.length-1]);
    }

     var mapOptions = {
       //center: new google.maps.LatLng(41.077354,-81.511337),
         zoom: -1,
         mapTypeId: google.maps.MapTypeId.ROADMAP
     };
   map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    map.fitBounds(bounds);
    google.maps.event.addDomListener(window,'resize',function() {
      google.maps.event.trigger(map,'resize');
      map.fitBounds(bounds);
    });

  var i = locations.length;
  var index = 0;

  while (i != 0) {

    if (i < 3) {
      var tmp_locations = new Array();
      for (var j = index; j < locations.length; j++) {
        tmp_locations.push(locations[index]);
      }
      drawRouteMap(tmp_locations);
      i = 0;
      index = locations.length;
    }

    if (i >= 3 && i <= 10) {
      console.log("before :fun < 10: i value " + i + " index value" + index);
      var tmp_locations = new Array();
      for (var j = index; j < locations.length; j++) {
        tmp_locations.push(locations[j]);
      }
      drawRouteMap(tmp_locations);
      i = 0;
      index = locations.length;
      console.log("after fun < 10: i value " + i + " index value" + index);
    }

    if (i >= 10) {
      console.log("before :fun > 10: i value " + i + " index value" + index);
      var tmp_locations = new Array();
      for (var j = index; j < index + 10; j++) {
        tmp_locations.push(locations[j]);
      }
      drawRouteMap(tmp_locations);
      i = i - 9;
      index = index + 20;
      console.log("after fun > 10: i value " + i + " index value" + index);
    }
  }
}


function drawRouteMap(locations) {

  var start, end;
  var waypts = [];

  for (var k = 0; k < locations.length; k++) {
    if (k >= 1 && k <= locations.length - 2) {
      waypts.push({
        location: locations[k],
        stopover: true
      });
    }
    if (k == 0) start = locations[k];

    if (k == locations.length - 1) end = locations[k];

  }
  var request = {
    origin: start,
    destination: end,
    waypoints: waypts,
    travelMode: google.maps.TravelMode.DRIVING
  };
  console.log(request);

  directionsService.push(new google.maps.DirectionsService());
  var instance = directionsService.length - 1;
  directionsDisplay.push(new google.maps.DirectionsRenderer({
    preserveViewport: true,
	polylineOptions: {
   strokeColor: "#FF0000"
     }
  }));
  directionsDisplay[instance].setMap(map);
  directionsService[instance].route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      console.log(status);
      directionsDisplay[instance].setDirections(response);
    }
  });
}

google.maps.event.addDomListener(window, 'click', calcRoute4() );
   