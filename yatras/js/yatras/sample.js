
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

function initialize() {
  directionsDisplay = new google.maps.DirectionsRenderer();
  var mapOptions = {
    zoom: 7,
    center: new google.maps.LatLng(18.850033, 74.6500523)
  };
  var map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);
  directionsDisplay.setMap(map);
 
}

function calcRoute() {
  var start = new google.maps.LatLng(18.2745698,74.3160724);
  var end = new google.maps.LatLng(18.7566484,74.2512274);
   var waypts = [{location:new google.maps.LatLng(18.44439,74.726416),stopover:true},
				  {location:new google.maps.LatLng(18.5394306,73.2217923),stopover:true},
				  {location:new google.maps.LatLng(18.810536,73.301292),stopover:true},
				  {location:new google.maps.LatLng(18.5219086,74.048504),stopover:true},
				   {location:new google.maps.LatLng(19.188143,73.9559632),stopover:true},
				  {location:new google.maps.LatLng(19.24171,73.887692),stopover:true}];
				 
  var request = {
    origin: start,
    destination: end,
	waypoints: waypts,
	optimizeWaypoints: true,
    travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
	   directionsDisplay.setPanel(document.getElementById('directions_panel'));

  var control = document.getElementById('control_panel');
  control.style.display = 'block';
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);
    }
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
var geocoder;
var map;

var marker;

function initialize() 
{
	var latlng = new google.maps.LatLng(18.44439,74.726416);
	var myOptions = {
      zoom: 9,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.TERRAIN
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	var rendererOptions = { map: map };
	directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

function calcRoute() 
{
	var point1 = new google.maps.LatLng(18.44439,74.726416);
	var point2 = new google.maps.LatLng(18.5394306,73.2217923);
	var point3 = new google.maps.LatLng(18.810536,73.301292);
	var point4 = new google.maps.LatLng(18.5219086,74.048504);
	var point5 = new google.maps.LatLng(19.188143,73.9559632);
	var point6 = new google.maps.LatLng(19.24171,73.887692);
	
	var wps = [{ location: point1 }, { location: point2 }, {location: point3},{location: point4},{location: point5},{location: point6}];

	var org = new google.maps.LatLng(18.2745698,74.3160724);
	var dest = new google.maps.LatLng(18.7566484,74.2512274);

	var request = {
	
			origin: org,
			destination: dest,
			waypoints: wps,
			
			travelMode: google.maps.DirectionsTravelMode.DRIVING
			};

	directionsService = new google.maps.DirectionsService();
	directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
				}
				else
					alert ('failed to get directions');
			});
			
}
