<div class="footer-container f-fix mainFooterPan01">
        <div class="f-block footer-top-wr">
            
            
            
            

            <div class="f-fix footerBottom">

                <footer class="footer container">

                   
                    
                    

                    <div class="footer-links f-fix">
                        <div class="span3 news-letter" style="margin-right: 100px;">
                            <h3>Newsletter</h3>
                                                        <form action="https://www.wheresmypandit.com/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
                                <div class="form-subscribe">
                                    <h4><i class="fa fa-envelope-o"></i> <span>Sign up for our newsletter</span></h4>
                                    <div class="input-box">
                                        <div class="msg-block f-fix" style="margin-bottom: 5px;">
                                            <input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email">
                                        </div>
                                        <button type="submit" title="Submit" class="button"><span><span>Subscribe</span></span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="link-block">
<h3>Information</h3>
<ul>
<li><a title="Privacy Policy" href="https://www.wheresmypandit.com/privacy-policy/?SID=3d886fc9339e1c9b6cf2c77e4217d2d6"><span>Privacy</span> Policy</a></li>
<!--<li><a title="Cancellations &amp; Returns" href="https://www.wheresmypandit.com/cancellations-returns/?SID=3d886fc9339e1c9b6cf2c77e4217d2d6"><span>Cancellations</span> &amp; Returns</a></li>-->
<li><a title="Terms of Use" href="https://www.wheresmypandit.com/terms-use/?SID=3d886fc9339e1c9b6cf2c77e4217d2d6"><span>Terms</span> of Use</a></li>
</ul>
</div>
<div class="link-block">
<h3>Customer Service</h3>
<ul>
<li><a title="Contact Us" href="https://www.wheresmypandit.com/contacts/?SID=3d886fc9339e1c9b6cf2c77e4217d2d6"><span>Contact</span> Us</a></li>
<li><a title="Returns" href="https://www.wheresmypandit.com/cancellations-returns/?SID=3d886fc9339e1c9b6cf2c77e4217d2d6"><span>Cancellation &amp; </span>Returns</a></li>
</ul>
</div>
<div class="link-block">
<h3>Contact</h3>
<ul>
<li><strong>Call :</strong> 022 67079797 | +91 9820573385</li>
<li><strong>Timings :</strong> Mon to Fri |&nbsp;10am to 7pm</li>
<li><strong>E-mail :</strong> <a href="mailto:panditji@wheresmypandit.com">panditji@wheresmypandit.com </a></li>
</ul>
</div>                    </div>
                    
                    <div class="media-payment f-block">
                        <div style="float: left; width:33%; text-align: justify;" class="social_plugins">

                                                   
                            <a href="https://www.facebook.com/wheresmypandit">
                                <img src="https://www.wheresmypandit.com/media/ninad/plugin_icons/plugin_facebook.png" alt="Follow WheresMyPandit on Facebook">
                            </a>
                            <a href="https://twitter.com/wheresmypandit">
                            <img src="https://www.wheresmypandit.com/media/ninad/plugin_icons/plugin_twitter.png" alt="Tweet us on Twitter">
                            </a>
                            <a href="https://plus.google.com/+WheresMyPanditWMP">
                            <img src="https://www.wheresmypandit.com/media/ninad/plugin_icons/plugin_google+.png" alt="Connect with us on Google+">
                            </a>
                            <a href="http://www.youtube.com/c/WheresMyPanditWMP">
                            <img src="https://www.wheresmypandit.com/media/ninad/plugin_icons/plugin_youtube.png" alt="Subscribe us on YouTube">
                            </a>
                            <a href="https://instagram.com/wheresmypandit">
                            <img src="https://www.wheresmypandit.com/media/ninad/plugin_icons/plugin_insta.png" alt="Follow us on Instagram">
                            </a>
                            
                        </div>
                        <div style="float: left; width:33%;margin-top: 10px;">
                            <div align="left">© 2015 Where's My Pandit. All Rights Reserved.</div>                        </div>
                        <div style="float: left; width:33%;">
                            
					<div class="f-right"><img src="https://www.wheresmypandit.com/skin/frontend/nextlevel/default/images/payment.png" alt=""></div>
				                        </div>
                        
                    </div>
                    
                   
                    <div class="copyright f-block">
                                                <p class="copyText f-right"></p>
                    </div>
                   
                    <div class="clear"></div>
                    
                </footer>

            </div>


        </div></div>