<?php include "inner_head.php"; ?>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<link href="https://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="js/yatras/jyotirlinga.js"></script>
	<script type="text/javascript" src="js/yatras/ashtavinayak.js"></script>
	<script type="text/javascript" src="js/yatras/chardham.js"></script>
    <script type="text/javascript" src="js/yatras/dodham.js"></script>
<body onload="initialize()">
  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="big   with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  	
	  		
	  		<!-- TOP NAVIGATION
	  		<div id="top-navigation">
		  		<ul class="animate-me fadeInDown" data-wow-duration="1.2s">
			  		<div class="summary">
        	
         	<h2 class="classy f-left"><span>Shopping Cart -</span>
                        	<a href="#">0</a><span class="Itext"> item</span>                                    </h2>
           
        </div>
		  		</ul>
	  		</div>
	  	
	  		<!-- MOBILE NAVIGATION -->
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		<!-- TEXT SLIDER -->
			
	  		<!-- HEADER SLIDER -->
		  	<div class="" id="header-slider">
		  	
	  		
	  			<!--PORTFOLIO -->
	  		<!--<h2 class="with-breaker animate-me fadeInUp animated" style="visibility: visible; -webkit-animation: fadeInUp;">
		  		Yatras <span>At least once in lifetime, One must do..</span>
	  			</h2>-->
	  			<!--<div id="portfolio-filters" class="animate-me fadeIn animated animated" style="visibility: visible; -webkit-animation: fadeIn;">
	  				
					<button class="btn btn-default" data-filter=".common-all"><i class="fa fa-arrows"></i> All</button>
					<button type="submit" class="btn btn-default" data-filter=".west"><i class="fa fa-arrows-h"></i> West</button>
					<button class="btn btn-default" data-filter=".north"><i class="fa fa-arrows-v"></i>North</button>
					<button class="btn btn-default" data-filter=".east"><i class="fa fa-arrows-h"></i>East</button>
					<button class="btn btn-default" data-filter=".south"><i class="fa fa-arrows-v"></i> South</button>
	  			</div>-->
		
				<div class="yatra-map">
				<div id="map_canvas"></div>
		
				</div></div>
						<div id="control_panel">
					<div>
					<table class="tbk-ytra" border="1">
					<tr>
						<td class="tbl-list" colspan="2" align="centre">List of Yatras</td>
					</tr>
					<tr class="1">
						<td class="tbl-color"><a onclick="calcRoute1();"><img src="images/yatra/1.png"/>Show Route</a></td>
						<td class="tbl-place"><a href="12-jyotirlinga.php">12 Jyotirlinga Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/2.png" />Show Route</a></td>
						<td class="tbl-place"><a href="ashtavinayak.php">Ashtavinayak Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute3();"><img src="images/yatra/3.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="char-dham.php">Char Dham Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute4();"><img src="images/yatra/4.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="do-dham.php">Do Dham Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute5();"><img src="images/yatra/5.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="chotachar-dham.php">Chota Char Dham Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/6.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="kashi.php">Kashi Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/7.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="navagraha-temples.php">Navgraha Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/8.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="jagannath-puri-rath-yatra.php">Puri Rath Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/9.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="sapta-puri.php">Sapta Puri Yatra</a></td>
					</tr>
					<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/10.png" />Show Route</a></td>
                        <td class="tbl-place"><a  href="teen-dham.php">Teen Dham Yatra</a></td>
					</tr>
					<!--<tr>
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/11.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="amarnath.php">Amarnath Yatra</a></td>
					</tr>-->
					<!--<tr class="2">
						<td class="tbl-color"><a onclick="calcRoute2();"><img src="images/yatra/12.png" />Show Route</a></td>
						<td class="tbl-place"><a  href="vaisnodevi.php">Vaishnov Devi Yatra</a></td>
					</tr>-->
					</table>
					
					</div>
					
				</div>
		  	
			<div id="shade"></div>
		  	<!-- OR VIDEO -> https://github.com/VodkaBears/Vide -->
		  	<!--<div id="header-video"
			    data-vide-bg="ogv: images/video/video, webm: images/video/video, poster: images/video/poster" data-vide-options="posterType: jpg, loop: true, muted: true, position: 50% 50%">
			</div>-->
	  		
	  	</header>
	  	<!-- END HEADER -->
		
	
	
		
		<?php echo $footerBlock; ?>
	  	
	  	<!-- SCROLL TOP -->
	  	<a href="#" id="scroll-top" class="fadeInRight animate-me"><i class="fa fa-angle-double-up"></i></a>
  	</div>

    <!-- SCRIPTS -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/plugins.js"></script>
	<script type="text/javascript">
		/*TEXT TICKER (ONLY FOR HOME PAGE)*/
		$('#ticker-text').vTicker('init', {
			speed: 300, 
		    pause: 2000
	    });
	</script>
	<!-- ONLY DEMO -->
	
    <script src="js/custom.js"></script>
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/yatras/sample.js"></script>

<script>
function initialize()
{
	var mapOptions = {
       center: new google.maps.LatLng(20.1289956,78.7792201),
         zoom: 5,
        // mapTypeId: google.maps.MapTypeId.ROADMAP
     };
map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
directionsDisplay.setMap(map);
}
</script>	

	</body>
  <!-- END BODY -->
</html>
