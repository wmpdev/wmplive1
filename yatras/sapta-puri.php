<?php
	
$mageFilename = '../app/Mage.php';
include_once($mageFilename);
	
umask(0);
Mage::app();
Mage::getSingleton('core/session', array('name'=>'frontend'));
	
$session = Mage::getSingleton('customer/session');
if($session->isLoggedIn()) 
{
		$userId = $session->getCustomer()->getId();
} else {
		$userId = 0;
}

$block              = Mage::getSingleton('core/layout');
	
	
$footerBlock        = $block->createBlock('page/html_footer')->setTemplate('page/html/footer.phtml')->toHtml();
 

?><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Sapta Puri yatra</title>
<meta name="description" content="We provides you comprehensive guidance on Sapta Puri yatra. It includes significance, history, itinerary, nearby temples, etc.">
<meta name="keywords" content="sapta puri yatra">
<link rel="canonical" href="https://www.wheresmypandit.com/yatras/sapta-puri.php" />
<link rel="icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link rel="shortcut icon" href="http://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico" type="imgge/x-icon">
<link href="inner_css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="inner_css/home.css" type="text/css" rel="stylesheet">
<!--<link href="inner_css/styles.css" type="text/css" rel="stylesheet">-->
<link href="style.css" type="text/css" rel="stylesheet">
<link href="inner_css/orange-color.css" type="text/css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet" type="text/css">
 <!-- STYLESHEETS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <!--<link href="css/options.css" rel="stylesheet" media="screen">-->
<link href="css/custom.css" type="text/css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" media="screen">
<link href="css/main.css" type="text/css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src = "https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/thumbelina.js"></script>

<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<link href="js/jquery-ui.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/customslide.js"></script>
<script type="text/javascript" src="js/jssor.js"></script>
<script type="text/javascript" src="js/jssor.slider.js"></script>
 <link href="css/hor-timeline.css" rel="stylesheet" media="screen">
<script src="js/jquery.timelinr-0.9.54.js"></script> 

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53702404-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- END GOOGLE ANALYTICS CODE -->

</head>
  <!-- START BODY -->
  <body>

  	<div id="page">
	  	<!-- START HEADER -->
	  	<header id="header" class="small  with-separation-bottom">
	  		<!-- POINTER ANIMATED -->
	  		<canvas id="header-canvas"></canvas>
	  		
	  	
	  		<!-- MAIN MENU -->
	  		<nav id="navigation">
	  			<!-- DISPLAY MOBILE MENU -->
	  			
	  			<!-- CLOSE MOBILE MENU -->
		  		<a href="#" id="close-navigation-mobile"><i class="fa fa-long-arrow-left"></i></a>
	  			
		  		<div  class="animate-me flipInX" data-wow-duration="3s">
		  			<a href="index.php" id="logo-navigation"></a>
		  		</div>
	</nav>
	  		
	  		<!-- SHADOW -->
	  		<div id="shade2"></div>

	  		<!-- HEADER SLIDER -->
		  	<div class="flexslider" id="header-slider">
		  		<ul class="slides">
		  			<li><img src="images/bg1.jpg" alt="SLider Image"></li>
		  			<!--<li><img src="images/bg2.jpg" alt="SLider Image"></li>
		  			<li><img src="images/bg3.jpg" alt="SLider Image"></li>-->
		  		</ul>	
		  	</div>
		  
	  	</header>
	  	<!-- END HEADER -->
	  	
	  	<!-- START MAIN CONTAINER -->
	  	<div class="main-container">
	  		<div class="container">
	  			<!--BLOG -->
	  			<h1 class="with-breaker animate-me fadeInUp">
		  			Sapta Puri Yatra
	  			</h1>
					<div class="row">
					
				<div class="col-md-7">
				<div class="row">
<div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 600px;margin:0 auto; height: 370px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute;margin:0 auto; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div>
                <img u="image" src="images/slider/sp1.png" alt="Sapta Puri Yatra"/>
                <img u="thumb" src="images/slider/sp1-tn.png" alt="Sapta Puri Yatra"/>
            </div>
            <div>
                <img u="image" src="images/slider/sp2.png" alt="SaptaPuri Yatra"/>
                <img u="thumb" src="images/slider/sp2-tn.png" alt="SaptaPuri Yatra"/>
            </div>
            <div>
                <img u="image" src="images/slider/sp3.png" alt="Sapta Puri Yatra - Seven Holy Cities"/>
                <img u="thumb" src="images/slider/sp3-tn.png" alt="Sapta Puri Yatra - Seven Holy Cities"/>
            </div>
            <div>
                <img u="image" src="images/slider/sp4.png" alt="Sapta Puri Yatra"/>
                <img u="thumb" src="images/slider/sp4-tn.png" alt="Sapta Puri Yatra"/>
            </div>
            
        </div>
    
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora02l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora02r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        
        <!-- ThumbnailNavigator Skin Begin -->
       <div u="thumbnavigator" class="jssort03" style="position: absolute; width: 600px; height: 60px; left:0px; bottom: 12px;">
            <div style=" background-color: #fff; filter:alpha(opacity=30); opacity:.3;  width: 100%; height:100%;"></div>

            <!-- Thumbnail Item Skin Begin -->
            
            <div u="slides" style="cursor: move;">
                <div u="prototype" class="p" style="POSITION: absolute; WIDTH: 90px; HEIGHT: 50px; TOP: 0; LEFT: 0;">
                    <div class=w><div u="thumbnailtemplate" style=" WIDTH: 100%; HEIGHT: 100%; border: none;position:absolute; TOP: 0; LEFT: 0;"></div></div>
                    <div class=c style="POSITION: absolute; BACKGROUND-COLOR: #000; TOP: 0; LEFT: 0">
                    </div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
    </div>
					</div>
					 
       
						 <br/>
				
				
				 <div class="temple-description">
				 <p>Literally, Sapta Puri means &lsquo;seven holy cities&rsquo;. These seven cities are - include Ayodhya, Mathura, Haridwar, Kashi, Ujjain,  Dwaraka and Kanchi. Ayodhya is one of the very ancient cities of the country. It is located on the banks of Saryu River. The second holy city of Mathura is situated near Vrindavan. Haridwar is one of those sites where the famous Kumbh Mela is organized. It is situated near the River Ganga. The holy destination of Kashi is also situated near the Ganga River. Kanchi is located in Tamil Nadu near the Vegavathy River. Ujjain also hosts the Kumbha festival. Dwarka is located in Gujarat. </p>
				
				 </div>
				
				 </div>
				 <div class="col-md-4">
				   <ul class="location-special">
				   <li><strong>No. of Places to be visited</strong> : 7 </li>
                    <br/>
                    <!--<li><strong>Day 3 </strong>: CHINTAMANI , Girijatmaj  </li>
					<li><strong>Day 4</strong> : VIGHNESHWAR , MAHAGANPATI  </li>-->
                    <li><strong>Total Distance</strong> : 5362 KM</li>
                    <br>
                    <li><strong>Best Season to visit</strong> : Nov to Mar</li>
				</ul><br/><br/>
					
				 <a href="images/sapta_puri_yatra.jpg"><img class="img-map" src="images/sapta_puri_yatra.jpg" alt="Sapta Puri Yatra Map" width="400" height="450"/></a>
				 
					
					
				
				 
				<!--<p style="display: block !important; width: 100%; text-align: center; font-family: sans-serif; font-size: 12px;"><a href="http://weathertemperature.com/forecast/?q=Kanniyakumari,Tamil+Nadu,India" title="Kanniyakumari, Tamil Nadu, India Weather Forecast" onclick="this.target='_blank'"><img src="http://widget.addgadgets.com/weather/v1/?q=Kanniyakumari,Tamil+Nadu,India&amp;s=2&amp;u=1" alt="Weather temperature in Kanniyakumari, Tamil Nadu, India" width="160" height="102" style="border:0"></a><br><a href="http://weathertemperature.com/" title="Get latest Weather Forecast updates" style="font-family: sans-serif; font-size: 12px" onclick="this.target='_blank'">Weather Forecast</a></p>-->
				 </div>
				 </div>
			
				 
					<div class="tabs-container">
	  				<ul class="nav nav-tabs" role="tablist" id="SkillsTab">
						<li class="active">
							<a href="#skill1" role="tab" data-toggle="tab"><i class="story"></i> Significance</a>
						</li>
						<li><a href="#skill2" role="tab" data-toggle="tab"><i class="facts"></i> History</a></li>
						<!--<li><a href="#skill3" role="tab" data-toggle="tab"><i class="festivals"></i> Festivals</a></li>-->
						<!--<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i>Season</a></li>-->
						<li><a href="#skill5" role="tab" data-toggle="tab"><i class="architecture"></i>Itinerary</a></li>
						<li><a href="#skill4" role="tab" data-toggle="tab"><i class="more-details"></i> Near by Temple</a></li>

						
					</ul>

					<div class="tab-content">
						<div class="tab-pane active bounceInUp" id="skill1">
							<h2><i class="story"></i>Significance</h2>
                            <p>The <strong>Sapta Puri Yatra</strong> is one of the very popular yatras undertaken by Hindus. All seven of these destinations are of real importance of their own. It is said that going on a yatra of Sapta Puri, the individual gets rid of the birth and death cycles and attains the ultimate achievement of Moksha. It is as important as the <a href="https://www.wheresmypandit.com/yatras/char-dham.php" title="Char Dham">Char Dham</a> for the Hindus. </p>
                            <p>&nbsp;</p>
						</div>
						<div class="tab-pane bounceInUp" id="skill2">
							<h2><i class="facts"></i>History</h2>
								<ul style="text-align:left">
								Basically, the Sapta Puri are the birthplaces of spiritual masters and Gods have descended here as Avatars. Lord Rama was born in Ayodhya. 
                                Kanchi is known for Kamakshi Amman Temple. Dwarka is the place where Shri Krishna resided after he left Mathura.
                                Mathura is the place where all the childhood events of Lord Krishna are associated with. Haridwar is the gateway to Uttarakhand. Kasha is Lord Shiva’s favorite residence.
								</ul>
						</div>
						<!--<div class="tab-pane bounceInUp" id="skill3">
							<h2><i class="festivals"></i>FESTIVALS</h2>
								<ul>
								<li><h3>Moreshwar</h3><span class="location">Moregaon</span>
								<span class="loc-image"><img src="images/Ash1.jpg" width="250px" height="300"></span>
								</p></li>
								<li><h3>Siddhivinayak</h3><span class="location">Siddhatek</span>
								<span class="loc-image"><p><img src="images/Ash2.jpg"> 
								
								</p></span>
								</li>
								</ul>
						</div>-->
						<div class="tab-pane bounceInUp" id="skill4" >
							<h2><i class="more-details"></i> All Nearby Temples</h2>
								<ul class="span3">
								<li><strong>Dwarka</strong>Porbandar</li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=33" title="Haridwar">Haridwar</a></strong> Dehradun</li>
								<li><strong><a href="https://www.wheresmypandit.com/temples/content.php?tid=30" title="Kanchipuram">Kanchipuram</a></strong> Chennai</li>

								
								</ul>
								
								<ul class="span3">
								<li><strong>Ujjain </strong> Indore</li>
								<li><strong>Ayodhya</strong> Lucknow </li>

								
								</ul>
								<ul class="span3">
								<li><strong>Mathura</strong> Agra</li>
								<li><strong>Varanasi</strong>Varanasi</li>

								
								</ul>
						</div>	
							<div class="tab-pane bounceInUp" id="skill5">
						<h2><i class="architecture"></i>Itinerary</h2>
						
						<?php

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

if ($detect->isMobile()) {
?> 
<!--Mobile content-->

<div class="itinerary">
<ul>
<li>Porbandar</li>
<li><span class="city">Dwarka</span><span>104  kms </span><span>1h 48mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Indore</li>
<li><span class="city">Porbandar</span><span>771  kms </span><span>12h 33mins</span></li>
<li><span class="city">Ujjain	</span><span>55.80 kms </span><span>0h 55mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Agra</li>
<li><span class="city">Indore </span><span>615 kms </span><span>12h 0mins</span></li>
<li><span class="city">Mathura	</span><span>59 kms </span><span>0h 54mins</span></li>

</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Dehradun</li>
<li><span class="city">Agra</span><span>425 kms </span><span>6h 35mins</span></li>
<li><span class="city">Haridwar</span><span>53 kms </span><span> 53mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Lucknow</li>
<li><span class="city">Dehradun</span><span>547.5 kms </span><span>8h 35mins</span></li>
<li><span class="city">Ayodhya	</span><span>135  kms </span><span>2h 25mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Varanasi</li>
<li><span class="city">Lucknow</span><span>293  kms </span><span>4h 06mins</span></li>
<li><span class="city">Varanasi	</span><span>0 kms </span><span>0h 0mins</span></li>
</ul>
</div>

<hr/>

<div class="itinerary">
<ul>
<li>Chennai</li>
<li><span class="city">Varanasi</span><span>1897  kms </span><span>31h 0mins</span></li>
<li><span class="city">Kanchipuram</span><span>47 kms </span><span>1h 54mins</span></li>
</ul>
</div>
<hr/>

<!--Mobile content end-->


<?php

}
else
{
?>

<!--Desktop content-->
									<script>
		$(function(){
			$().timelinr({
				arrowKeys: 'true'
			})
		});
	</script>

<div id="timeline">	

<ul id="dates">
				
			<li><a href="#porbandar">Porbandar</a></li>
			<li><a href="#indore">Indore</a></li>
			<li><a href="#agra">Agra</a></li>
			<li><a href="#dehradun">Dehradun</a></li>
			<li><a href="#lucknow">Lucknow</a></li>
			<li><a href="#varanasi">Varanasi</a></li>            
			<li><a href="#chennai">Chennai</a></li>                        
		</ul>
		<ul id="issues">
			<li id="porbandar">
					<ul class="route">
					<li>- </li>
					<li class="car"> 0 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 0 Hours</li>
					<li class="km"> 0</li>
					<li> Porbandar</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Dwarka </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Porbandar:</td>								
						<td>104 KM </td>								
						<td> 1 h 48 min</td>								
					</tr>
				
					
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Porbandar-Dwarka-Porbandar
				<strong>Minimum Duration</strong>13 h 39 min
				<strong>Minimum Distance</strong>210 KM

				</div> 
				</div>
				
			<li id="Ujjain"> 
					<ul class="route">
					<li> Porbandar</li>
					<li class="car"> 12 Hours 33 Min</li>
					<li class="train"> 14 Hours 30 Min</li>
					<li  class="flight"> 04 Hours 10 Min</li>
					<li class="km"> 771</li>
					<li> Indore</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Ujjain </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Indore:</td>								
						<td>55.80 KM </td>								
						<td> 0 h 55 min</td>								
					</tr>
			
				</tbody>
				</table>
				

				</div>
					<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Indore-Ujjain-Indore
				<strong>Minimum Duration</strong>1 h 49 min
				<strong>Minimum Distance</strong>111 KM

				</div> 
				</div>
				
				
			
			<li id="agra"> 
					<ul class="route">
					<li> Indore</li>
					<li class="car"> 12 Hours</li>
					<li class="train"> 0 Hours</li>
					<li  class="flight"> 5 Hours 45 Min</li>
					<li class="km"> 615</li>
					<li> Agra</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Mathura </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Agra:</td>								
						<td>59 KM </td>								
						<td> 0 h 54 min</td>								
					</tr>
				
					
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Agra-Mathura-Agra
				<strong>Minimum Duration</strong>1 h 47 min
				<strong>Minimum Distance</strong>117 KM

				</div> 
				</div>
				
				
			
			<li id="dehradun"> 
					<ul class="route">
					<li> Agra</li>
					<li class="car"> 6 Hours 35 Min</li>
					<li class="train"> 11 Hours 30 Min</li>
					<li  class="flight"> 00 Hours  55 Min</li>
					<li class="km">425</li>
					<li> Dehradun</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Haridwar </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Dehradun:</td>								
						<td>53 KM </td>								
						<td> 0 h 53 min</td>								
					</tr>
				
									
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Dehradun-Haridwar-Dehradun
				<strong>Minimum Duration</strong>1 h 48 min
				<strong>Minimum Distance</strong>106 KM

				</div> 
				</div>
				
				
				
			</li>
			
			<li id="lucknow">
					<ul class="route">
					<li> Dehradun</li>
					<li class="car"> 8 Hours 35 Min</li>
					<li class="train"> 10 Hours 30 Min</li>
					<li  class="flight"> 01 Hours</li>
					<li class="km"> 547</li>
					<li> Lucknow</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Ayodhya </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Lucknow:</td>								
						<td>135 KM </td>								
						<td> 2 h 25 min</td>								
					</tr>
				
					
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Lucknow-Ayodhya-Lucknow
				<strong>Minimum Duration</strong>4 h 38 min
				<strong>Minimum Distance</strong>271 KM

				</div> 
				</div>
				
				
			</li>
			
			
			<li id="varanasi">
					<ul class="route">
					<li> Lucknow</li>
					<li class="car"> 04 Hours 06 Min</li>
					<li class="train"> 08 Hours 45 Min</li>
					<li  class="flight"> 03 Hours 05 Min</li>
					<li class="km"> 293</li>
					<li> Varanasi</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Varanasi</h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Varanasi:</td>								
						<td>0 KM </td>								
						<td> 0 h 0 min</td>								
					</tr>
				
					
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Varanasi-Varanasi-Varanasi
				<strong>Minimum Duration</strong>00 h 00 min
				<strong>Minimum Distance</strong>00 KM

				</div> 
				</div>
				
				
			</li>
			
								
	<li id="chennai">
					<ul class="route">
					<li> Varanasi</li>
					<li class="car"> 31 Hours 00 Min</li>
					<li class="train"> 39 Hours 42 Min</li>
					<li  class="flight"> 06 Hours 15 Min</li>
					<li class="km"> 1897</li>
					<li> Chennai</li>
					</ul>
				
				<div class="row shortlist-grid">	
				
				<div class="span5 shortdesc-table">	
				
				<table class=" ">
				<thead><tr><h5>Kanchipuram </h5></tr></thead>
				<tbody>
					<tr>
						<td></td>								
						<td><strong>Distance</strong></td>								
						<td><strong>Time</strong></td>								
					</tr>
					<tr>
						<td>From Chennai:</td>								
						<td>74 KM </td>								
						<td> 1 h 54 min</td>								
					</tr>
				
					
				
				</tbody>
				</table>
				

				</div>
				<div class="span3 shortdesc-list">	
				<strong>Shortest Trip</strong>Chennai-Kanchipuram-Chennai
				<strong>Minimum Duration</strong>2 h 53 min
				<strong>Minimum Distance</strong>147 KM

				</div> 
				</div>
				
				
			</li>

                	
		</ul>	
		
	</div>
	<!--<style>
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul#issues li{width:800px;}
			div#timeline  ul li{min-height:100%;}
			div#timeline  ul li:before{display:none}
			div#timeline  ul#issues li{width:800px;text-align:center;overflow: hidden;}
			div#timeline ul li ul li {height: auto;}
	</style>-->
	
<!--Desktop content end-->
	
<?php 

}
?>
    
</div>
</div>
</div>

		
						

	  	</div>
	  	<!-- END MAIN CONTAINER -->
	  	<!-- START FOOTER -->
	  	
    <!-- SCRIPTS -->

    <script src="js/plugins.js"></script>
    <script src="js/custom.js"></script>
  <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2oN6IZ8rywS4dflnIdWupkpOY2FLK0Bx';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
 <?php echo $footerBlock; ?>
		  </div>
		  </body>
		  </html>