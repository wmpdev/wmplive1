
var directionsDisplay = [];
var directionsService = [];
var map = null;


function initialize()
{
	var mapOptions = {
       center: new google.maps.LatLng(21.1289956,82.7792201),
         zoom: 12,
        // mapTypeId: google.maps.MapTypeId.ROADMAP
     };
map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
directionsDisplay.setMap(map);
}
function calcRoute2() {
  var msg = "18.2745698,74.3160724:18.44439,74.726416:18.5394306,73.2217923:18.810536,73.301292:18.5219086,74.048504:19.188143,73.9559632:19.24171,73.887692:18.7566484,74.2512274";
  var input_msg = msg.split(":");
  var locations = new Array();

    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < input_msg.length; i++) {
        var tmp_lat_lng = input_msg[i].split(",");
        locations.push(new google.maps.LatLng(tmp_lat_lng[0], tmp_lat_lng[1]));
        bounds.extend(locations[locations.length-1]);
    }

     var mapOptions = {
       //center: new google.maps.LatLng(41.077354,-81.511337),
         zoom: 12,
         mapTypeId: google.maps.MapTypeId.ROADMAP
     };
   map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    map.fitBounds(bounds);
    google.maps.event.addDomListener(window,'resize',function() {
      google.maps.event.trigger(map,'resize');
      map.fitBounds(bounds);
    });

  var i = locations.length;
  var index = 0;

  while (i != 0) {

    if (i < 3) {
      var tmp_locations = new Array();
      for (var j = index; j < locations.length; j++) {
        tmp_locations.push(locations[index]);
      }
      drawRouteMap(tmp_locations);
      i = 0;
      index = locations.length;
    }

    if (i >= 3 && i <= 10) {
      console.log("before :fun < 10: i value " + i + " index value" + index);
      var tmp_locations = new Array();
      for (var j = index; j < locations.length; j++) {
        tmp_locations.push(locations[j]);
      }
      drawRouteMap(tmp_locations);
      i = 0;
      index = locations.length;
      console.log("after fun < 10: i value " + i + " index value" + index);
    }

    if (i >= 10) {
      console.log("before :fun > 10: i value " + i + " index value" + index);
      var tmp_locations = new Array();
      for (var j = index; j < index + 10; j++) {
        tmp_locations.push(locations[j]);
      }
      drawRouteMap(tmp_locations);
      i = i - 9;
      index = index + 9;
      console.log("after fun > 10: i value " + i + " index value" + index);
    }
  }
}


function drawRouteMap(locations) {

  var start, end;
  var waypts = [];

  for (var k = 0; k < locations.length; k++) {
    if (k >= 1 && k <= locations.length - 2) {
      waypts.push({
        location: locations[k],
        stopover: true
      });
    }
    if (k == 0) start = locations[k];

    if (k == locations.length - 1) end = locations[k];

  }
  var request = {
    origin: start,
    destination: end,
    waypoints: waypts,
    travelMode: google.maps.TravelMode.DRIVING
  };
  console.log(request);

  directionsService.push(new google.maps.DirectionsService());
  var instance = directionsService.length - 1;
  directionsDisplay.push(new google.maps.DirectionsRenderer({
    preserveViewport: true,
	polylineOptions: {
   strokeColor: "#1E90FF"
     }
  }));
  directionsDisplay[instance].setMap(map);
  directionsService[instance].route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      console.log(status);
      directionsDisplay[instance].setDirections(response);
    }
  });
}

google.maps.event.addDomListener(window, 'click', calcRoute2() );
   