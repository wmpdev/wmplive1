<?php
define('MAGENTO_ROOT', (dirname(__FILE__).'../../../../../../'));
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
umask(0);
Mage::app();

$config = Mage::getStoreConfig('mdloption');
$color_helper = Mage::helper('mdloption/color');

header("Content-type: text/css; charset: UTF-8");
?>

<?php if ( $config['genral_theme_setting']['page-width'] ) : ?>
.page,.headerfix .globle-width .header-container .container, .hideTopNav .header-wrapper02 + .header-wrapper02 .container
{max-width:<?php echo $config['genral_theme_setting']['page-width']; ?>px;}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['theme-color-option'] ) : ?>

<?php if ( $config['genral_theme_setting']['enable_font'] ) : ?>
body{font-family:"<?php echo $config['genral_theme_setting']['font']; ?>"} 
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['font_size'] ) : ?>
body{font-size:<?php echo $config['genral_theme_setting']['font_size']; ?>px;}
<?php endif; ?>
<?php endif; ?>

/*button*/
<?php if ($config['buttonSetting']['enable_font']==1) :?>
<?php if ( $config['buttonSetting']['button_font'] ) : ?>
button.button span, .viewCart, a.btn, .firstViewInner .btn-cart span span{font-family:"<?php echo $config['buttonSetting']['button_font']; ?>"} 
<?php endif; ?>
<?php endif; ?>

/*price*/
<?php if ($config['priceSetting']['enable_font']==1) :?>
<?php if ( $config['priceSetting']['price_font'] ) : ?>
.price-box{font-family:"<?php echo $config['priceSetting']['price_font']; ?>"} 
<?php endif; ?>
<?php endif; ?>









/*color-settings*/
<?php
$config = Mage::getStoreConfig('mdloptioncolor');
$color_helper = Mage::helper('mdloptioncolor/color');
?>

<?php if ( $config['genral_theme_setting']['theme-color-option'] ) : ?>
<?php if ( $config['genral_theme_setting']['page-bg'] ) : ?>
.page,.hideTopNav .header-wrapper02 + .header-wrapper02, .hideTopNav .header-container,
.headingBox + .mix_container .jcarousel-control-prev:before, .headingBox + .mix_container .jcarousel-control-next:before, .headingBox + .mix_container .jcarousel-control-next:after, .headingBox + .jcarousel-wrapper .jcarousel-control-prev:before, .headingBox + .jcarousel-wrapper .jcarousel-control-next:before, .headingBox + .jcarousel-wrapper .jcarousel-control-next:after, .viewAll:before, .viewAll:after,
.content-wrapper, .page .carousel,
.headingBox h2 span,.footer-top-wr
{background-color:<?php echo $config['genral_theme_setting']['page-bg']; ?>;}
<?php endif; ?>


<?php if($config['genral_theme_setting']['block_content_border_color'] ) : ?>
.block-content,
.cart .discount, 
.cart .shipping,  
.cart .totals,
.block-layered-nav .block-content
{border-color:<?php echo $config['genral_theme_setting']['block_content_border_color']; ?>}
<?php endif; ?>

<?php if($config['genral_theme_setting']['header_cart_color'] ) : ?>
.block-cart.header_cart .bag, .block-cart.header_cart .classy a, .block-cart.header_cart .classy span{color:<?php echo $config['genral_theme_setting']['header_cart_color']; ?>}
<?php endif; ?>

<?php if($config['genral_theme_setting']['breadcrumbs_anchore'] ) : ?>
.breadcrumbs ul li a{color:<?php echo $config['genral_theme_setting']['breadcrumbs_anchore']; ?>}
<?php endif; ?>

<?php if($config['genral_theme_setting']['breadcrumbs_text'] ) : ?>
.breadcrumbs ul li{color:<?php echo $config['genral_theme_setting']['breadcrumbs_text']; ?>}
<?php endif; ?>


<?php if ( $config['genral_theme_setting']['color'] ) : ?>
.nav-container.span12, 
.social-link a:hover, 
.headingBox .headingIcons, 
.bullet, .viewAll, 
.headingBox .headingIcons, 
ul.add-to-links li a.link-wishlist, 
ul.add-to-links li a.link-compare, 
.mix_wrapper .jcarousel-prev, 
.mix_wrapper .jcarousel-next, 
.nav-width.scrollNav, 
.direction, 
.sbOptions, 

.dec.add, 
.add, 
.product_custom_content ul li span, 
.mobMenu h1, 
.bottomBox .link-wishlist, 
.bottomBox .compareR, 
.secondViewInner:hover .bottomBox2,
.item.secondViewInner:hover .bottomBox2 .buttonox-center,
.pager .pages li span, .pager .pages li a,
.customNavigation a, 
.compareR, .sliderNab a, .viewAll, .product-view .product-shop .add-to-links a, .email-friend a, .link-wishlist, .testimonial-3 .pagination a,
.quick_product .add-to-cart .qty_pan .add, .product-shop .add-to-cart .qty_pan .add,
.cart-table .btn-empty span, .cart-table .btn-continue span, .cart-table .btn-update span,
.button-b,
.sorter .direction, 
.pro-static-block li .f-left, 
.add-to-cart .qty_pan .add, 
.opc .active .step-title .number, 

.mix_wrapper ul li a.link-wishlist, 
.mix_wrapper ul li a.compareR, 
.bottomBox .link-wishlist, 
#fancybox-close,
.account-login .form-list .input-box:before,
.header-wrapper02 .block-cart .summary,
.onlinesupport-block,
.customNavigation a:hover, .product-view .product-shop .nextPre a:hover, .product-view .more-views .jcarousel-prev:hover, .product-view .more-views .jcarousel-next:hover,
.bottomBox .compareR, .cameraSlide .camera_effected .bannerButton,
.firstViewInner .price-box, .header-wrapper02 .nav-wrapper,.w-btn, .key-feature ul li .fa,
.cms-index-index ul.resp-tabs-list:before, .headingBox:before, .mainFooterPan01 .footer .footer-links li:before,
.page .tp-bannertimer, .badge span, .testimonial-wrapper, .highlighterWrap .view-button:hover, #tabs .headingBox h2 span:hover, .products-grid .price-box,
.btn-cart:hover, .block-layered-nav dt .toggleBtn,
.jcarousel-control-prev:hover, .jcarousel-control-next:hover,
.freeshipping-wrapper ,
.header-wrapper01 header nav ul#nav li:hover a.level-top, .header-wrapper01 header nav li:hover a.level-top, .header-wrapper01 header nav li.active a.level-top, .cms-index-index .header-wrapper01 header nav ul#nav li.home a.level-top, .contacts-index-index .header-wrapper01 header nav ul#nav li.contact-link a.level-top, .contacts-index-index .header-wrapper01 header nav li.contact-link a.level-top,button.button:hover span, .btn-cart:hover,
.blog-list .postContent .post-left .date-block
{
    background-color:<?php echo $config['genral_theme_setting']['color']; ?>;
}

.subtitle, 
.sub-title, 
.sorter , 
.toolbar .sorter .view-mode strong, 
.banner-top .container li em, 
.featured-block li .fa,
.twitter-twets li:before,
.footer-blocks h3,
.block-tags .actions a,
.freeshipping-block h2 strong.price,
.freeshipping-block h3,
.freeshipping-block a,
.footer-blocks .twitter-twets li:before,
.footer-blocks .twitter-twets li a,
.header-wrapper03 .language-switcher .fa, .header-wrapper03 .header_currency .fa,
.block-cart .cart-icon,
.language_detail a:hover, .currency_detail a:hover,
div #magicat li a:before,
#magicat li a:hover,
.header-wrapper03 .search-button,
.header .links a:before,
.secondViewInner:hover .itemInner .fancybox, .secondViewInner:hover .bottomBox2 button.button > span, .secondViewInner:hover .bottomBox2 .fancybox.btn, .secondViewInner:hover .bottomBox2 .link-wishlist, .secondViewInner:hover .bottomBox2 .compareR, .item.secondViewInner:hover .fa.fa-shopping-cart, .item.secondViewInner:hover .fancybox.btn.f-left .fa.fa-shopping-cart,.footer-links ul li a:before, .footer-info-box li .fa,
.header-contact em, .header-info em, .language-switcher .select_lang span:before, .header_currency .currency_pan span:before,
.header-top ul.links li a:before, .header-compare .cart-icon, .search-right .fa-search,
.cms-index-index .resp-tabs-list li, .headingBox h2 span, #tabs .headingBox h2 span a, .products-grid li.item.secView .price-box .price, a,
.jcarousel-control-prev, .jcarousel-control-next, .block-tags .block-content a, .product-view .product-img-box .product-image-zoom .zoomBtn,
.freeshipping-wrapper h2 a:hover, .header-top ul.links li a:hover, .header-compare .classy a:hover,
.block-account .block-content li a:hover, #magicat li a:hover, .block-layered-nav li a:hover, .block.block-list.header-compare #compare-items li a:hover, .block-account .block-content li strong,.mainFooterPan01 .footer .footer-links .link-block p .fa,
.header-top .block-cart .summary .cart-icon

{color:<?php echo $config['genral_theme_setting']['color']; ?>;}


.quick_product button.button span, .product-shop button.button span,
.button-b, button.button span,
#fancybox-close,
.cart-table .btn-empty span, .cart-table .btn-continue span, .cart-table .btn-update span,
.block-poll button.button span,
.block, .footer-blocks .span4,
.headingBox h2,
.secondViewInner:hover .itemInner,
.header-wrapper03 .language_detail, .header-wrapper03 .currency_detail,
.footer-container h3,
.mainFooterPan03,
.header-wrapper03 .drop_search,
.header-top .language_detail .language-inner, .header-top .currency_detail .currency-inner,
.camera_wrap .camera_fakehover .camera_prev, .camera_wrap .camera_fakehover .camera_next,
.page .header-wrapper02 .header_cart,.headingBox h2 span, .block .block-title, .sf-menu ul.level0 li a, .page-title, button.button span, .btn-cart,
button.button:hover span, .btn-cart:hover, .jcarousel-control-prev, .jcarousel-control-next,
.header-wrapper02 .block-cart .summary .bag, .cart .discount button span, .cart .shipping button span,
.my-wishlist .buttons-set .btn-add span, .my-wishlist .buttons-set .btn-share span,
.mainFooterPan02, .w-btn

{border-color:<?php echo $config['genral_theme_setting']['color']; ?>;}

.header-wrapper03 .drop_search:before{border-bottom-color:<?php echo $config['genral_theme_setting']['color']; ?>;}


.flexsliderSide, 
.opc .active .step-title .number, 
.opc .allow .step-title .number
{border-color:<?php echo $config['genral_theme_setting']['color']; ?>;}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['hover_color'] ) : ?>
.mix_wrapper .jcarousel-prev:hover, 
.mix_wrapper .jcarousel-next:hover, 
ul.add-to-links li a.link-wishlist:hover, 
ul.add-to-links li a.link-compare:hover, 
.prod-next:hover, 
.prod-prev:hover, 
.prod-next:hover, 
.prod-prev:hover, 
.more-views .jcarousel-next.jcarousel-next-horizontal:hover, 
.more-views .jcarousel-prev.jcarousel-prev-horizontal:hover, 
.product_custom_content ul li:hover span, 
.viewAll:hover, 
.flexslider .flex-prev:hover, 
.theme-default a.nivo-prevNav:hover, 
.theme-default a.nivo-nextNav:hover, 
.flexslider .flex-next:hover,
.mainFooterPan03 .footer-links .form-subscribe .button span
{background-color:<?php echo $config['genral_theme_setting']['hover_color']; ?>;}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['body_font_color'] ) : ?>
body
{color:<?php echo $config['genral_theme_setting']['body_font_color']; ?>;}
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['color'] ) : ?>
ul.promo li, #nav li.level-top.active > a, #nav li.level-top > a:hover, .searchPan, .compare-content, .remain_cart, #nav li.level-top.over > a {border-bottom-color:<?php echo $config['genral_theme_setting']['color']; ?>;}
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['color'] ) : ?>
.free-shipping span, .footer ul.footer_links.about li a {color:<?php echo $config['genral_theme_setting']['color']; ?>;}
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['anchor_color'] ) : ?>
a{color:<?php echo $config['genral_theme_setting']['anchor_color']; ?>;}
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['anchor_hover'] ) : ?>
a:hover{color:<?php echo $config['genral_theme_setting']['anchor_hover']; ?>;}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['block_heading_color'] ) : ?>
.block .block-title strong, .block-title h2{color:<?php echo $config['genral_theme_setting']['block_heading_color']; ?>; }
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['content_heading'] ) : ?>
.page-title h1, .page-title h2{color:<?php echo $config['genral_theme_setting']['content_heading']; ?>; }
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['new_batch'] ) : ?>
.badge span.new{background-color:<?php echo $config['genral_theme_setting']['new_batch']; ?>;}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['new_batch'] ) : ?>
.badge .new:before{
	border-top-color:<?php echo $config['genral_theme_setting']['new_batch']; ?>;
    border-bottom-color:<?php echo $config['genral_theme_setting']['new_batch']; ?>;
 }
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['new_batch_color'] ) : ?>
.badge span.new strong{color:<?php echo $config['genral_theme_setting']['new_batch_color']; ?>;}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['sale_batch'] ) : ?>
.badge span.sale{background-color:<?php echo $config['genral_theme_setting']['sale_batch']; ?>;}
<?php endif; ?>
<?php if ( $config['genral_theme_setting']['sale_batch'] ) : ?>
.badge .sale:after{
	border-top-color:<?php echo $config['genral_theme_setting']['sale_batch']; ?>;
    border-bottom-color:<?php echo $config['genral_theme_setting']['sale_batch']; ?>;
}
<?php endif; ?>

<?php if ( $config['genral_theme_setting']['sale_batch_color'] ) : ?>
.badge span.sale strong{color:<?php echo $config['genral_theme_setting']['sale_batch_color']; ?>;}
<?php endif; ?>
<?php endif; ?>
/*----*/








/*Navigation*/   
<?php if ( $config['navsettings']['theme-color-option'] ) : ?> 
      <?php if ( $config['navsettings']['main_nav_active_color'] ) : ?>
      .header-wrapper02 .nav-wrapper .container nav ul#nav > li.active > a,.header-wrapper02 .nav-wrapper .container nav ul.sf-menu > li.active > a     
        {color:<?php echo $config['navsettings']['main_nav_active_color']; ?>;}
     <?php endif; ?>
     
      <?php if ( $config['navsettings']['main-bg-color'] ) : ?>
      .header-wrapper02 .nav-wrapper{background:<?php echo $config['navsettings']['main-bg-color']; ?>;}
     <?php endif; ?>
     
      <?php if ( $config['navsettings']['nav_color'] ) : ?>
       .header-wrapper02 .nav-wrapper .container nav ul#nav > li:after, 
        .header-wrapper02 .nav-wrapper .container nav ul.sf-menu > li:after,
        .header-wrapper02 .nav-wrapper .container nav ul#nav > li > a,
        .header-wrapper02 .nav-wrapper .container nav ul.sf-menu > li > a
    	{color:<?php echo $config['navsettings']['nav_color']; ?>;}
    <?php endif; ?>
    
     <?php if ( $config['navsettings']['nav_color_hover'] ) : ?>
        .header-wrapper02 .nav-wrapper .container nav ul#nav > li > a:hover,
        .header-wrapper02 .nav-wrapper .container nav ul.sf-menu > li > a:hover
    	{color:<?php echo $config['navsettings']['nav_color_hover']; ?>;}
     <?php endif; ?>
    
    <?php if ( $config['navsettings']['nav_submenu_background'] ) : ?>
     .sf-menu ul.level0 li a,
     nav ul#nav ul.level0 li.level1 ul.level1 li a
      {background:<?php echo $config['navsettings']['nav_submenu_background']; ?>;}
    <?php endif; ?>
    
     <?php if ( $config['navsettings']['nav_submenu_hover_bg'] ) : ?>
     .sf-menu ul.level0 li:hover > a,
     nav ul#nav ul.level0 li.level1 ul.level1 li:hover a
    	{background:<?php echo $config['navsettings']['nav_submenu_hover_bg']; ?>;}
     <?php endif; ?>
    
    
    <?php if ( $config['navsettings']['nav_submenu_heading_bg'] ) : ?>
      #nav li ul.level0 li.level1 > a 
       {background:<?php echo $config['navsettings']['nav_submenu_heading_bg']; ?>;}
    <?php endif; ?>
    
     <?php if ( $config['navsettings']['nav_submenu_heading_color'] ) : ?>
      #nav li ul.level0 li.level1 > a 
       {color:<?php echo $config['navsettings']['nav_submenu_heading_color']; ?>;}
    <?php endif; ?>
     
    <?php if ( $config['navsettings']['nav_submenu_color'] ) : ?>
     .sf-menu ul.level0 li a,
      nav ul#nav ul.level0 li.level1 ul.level1 li a
     {color:<?php echo $config['navsettings']['nav_submenu_color']; ?>;}
    <?php endif; ?>
     
    <?php if ( $config['navsettings']['nav_submenu_color_hover'] ) : ?>
     .sf-menu ul.level0 li:hover > a,
     nav ul#nav ul.level0 li.level1 ul.level1 li:hover a
     {color:<?php echo $config['navsettings']['nav_submenu_color_hover']; ?>;}
    <?php endif; ?>
     
     <?php if ( $config['navsettings']['main_nav_border'] ) : ?>
     nav ul#nav ul.level0, .sf-menu ul.level0 li a
    	{border-color:<?php echo $config['navsettings']['main_nav_border']; ?>;}
     <?php endif; ?>
        
<?php endif; ?>
/*End Navigation*/

/*footer setting*/
	<?php if ( $config['footer_setting']['theme-color-option'] ) : ?>
    <?php if ( $config['footer_setting']['footer_bg'] ) : ?>
    .footer-container{background-color:<?php echo $config['footer_setting']['footer_bg']; ?>; }
    <?php endif; ?>
    
    <?php if ( $config['footer_setting']['footer_bg_bottom'] ) : ?>
    .mainFooterPan02 .footer-bottom {background-color:<?php echo $config['footer_setting']['footer_bg_bottom']; ?>; }
    <?php endif; ?>
    
    <?php if ( $config['footer_setting']['footer_heading'] ) : ?>
   .footer-container h3, 
   .mainFooterPan02 .footer-links-wr h3,
   .mainFooterPan01 .footer .footer-links h3
    {color:<?php echo $config['footer_setting']['footer_heading']; ?>; }
    <?php endif; ?>
    
    
    <?php if ( $config['footer_setting']['footer_text'] ) : ?>
   .mainFooterPan02 ol li, .mainFooterPan02 .twitter-twets li,
   .mainFooterPan01 .footer .footer-links .link-block p,
   .mainFooterPan01 .footer .footer-links .link-block p .fa
    {color:<?php echo $config['footer_setting']['footer_text']; ?>; }
    <?php endif; ?>
    
    <?php if ( $config['footer_setting']['footer_anchor'] ) : ?>
    .mainFooterPan02 .footer-links-wr .link-block ul li a,
    .mainFooterPan01 .footer .copyright ul li a,
    .footer a
    {color:<?php echo $config['footer_setting']['footer_anchor']; ?>; }
    <?php endif; ?>
    
    <?php if ( $config['footer_setting']['footer_hover'] ) : ?>
    .mainFooterPan02 .footer-links-wr .link-block ul li a:hover
    {color:<?php echo $config['footer_setting']['footer_hover']; ?>; }
    <?php endif; ?>
    
     <?php if ( $config['footer_setting']['footer_cm_border'] ) : ?>
     .mainFooterPan02 .footer-toprow, .mainFooterPan02 .footer-links-wr li + li
    {border-color:<?php echo $config['footer_setting']['footer_cm_border']; ?>; }
    <?php endif; ?>
    
    <?php endif; ?>
/*end footer setting*/

/*price setting*/
<?php if ( $config['priceSetting']['theme-color-option'] ) : ?>

<?php if ( $config['priceSetting']['regular_price'] ) : ?>
.products-grid li.item .price-box .price
{color:<?php echo $config['priceSetting']['regular_price']; ?>;}
<?php endif; ?>

<?php if ( $config['priceSetting']['regular_price_bg'] ) : ?>
.products-grid .price-box
{background-color:<?php echo $config['priceSetting']['regular_price_bg']; ?>;}
<?php endif; ?>

<?php if ( $config['priceSetting']['special_price'] ) : ?>
.products-grid li.item .spBox.price-box .price
{color:<?php echo $config['priceSetting']['special_price']; ?>;}
<?php endif; ?>

<?php if ( $config['priceSetting']['special_price_bg'] ) : ?>
.products-grid .price-box.minimalBox, .products-grid .spBox
{background-color:<?php echo $config['priceSetting']['special_price_bg']; ?>;}
<?php endif; ?>

<?php if ( $config['priceSetting']['old_price'] ) : ?>
.products-grid li.item .price-box.spBox .old-price .price
{color:<?php echo $config['priceSetting']['old_price']; ?>;}
<?php endif; ?>

<?php endif; ?>
/*end price setting*/




/*button setting*/
<?php if ( $config['buttonSetting']['theme-color-option'] ) : ?>

<?php if ( $config['buttonSetting']['button_color_bg'] ) : ?>
button.button span, .btn-cart,
.mainFooterPan02 .footer-toprow .form-subscribe .button span
{background-color:<?php echo $config['buttonSetting']['button_color_bg']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_border'] ) : ?>
button.button span, .btn-cart,
.mainFooterPan02 .footer-toprow .form-subscribe .button span
{border-color:<?php echo $config['buttonSetting']['button_border']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_text'] ) : ?>
button.button span, .btn-cart,
.mainFooterPan02 .footer-toprow .form-subscribe .button span
{color:<?php echo $config['buttonSetting']['button_text']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_hover'] ) : ?>
button.button:hover span, .btn-cart:hover,
.mainFooterPan02 .footer-toprow .form-subscribe .button:hover span
{background-color:<?php echo $config['buttonSetting']['button_hover']; ?>;}
.mainFooterPan02 .footer-toprow .form-subscribe .button:hover span,
button.button:hover span, .btn-cart:hover
{border-color:<?php echo $config['buttonSetting']['button_hover']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_text_hover'] ) : ?>
button.button:hover span, .btn-cart:hover,
.mainFooterPan02 .footer-toprow .form-subscribe .button:hover span
{color:<?php echo $config['buttonSetting']['button_text_hover']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['next_pre_btn'] ) : ?>
.mix_wrapper .jcarousel-prev, .mix_wrapper .jcarousel-next, .slider-box .viewAll, .nextPre .prod-prev, .nextPre .prod-next,
.mix_wrapper .pagination .prev5, .mix_wrapper .pagination .next5
{background-color:<?php echo $config['buttonSetting']['next_pre_btn']; ?>; }
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_bg_porduct'] ) : ?>
.add-to-cart button.button span
{background-color:<?php echo $config['buttonSetting']['button_bg_porduct']; ?>;}
.add-to-cart button.button span
{border-color:<?php echo $config['buttonSetting']['button_bg_porduct']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_bg_porduct_hover'] ) : ?>
.add-to-cart button.button:hover span
{background-color:<?php echo $config['buttonSetting']['button_bg_porduct_hover']; ?>;}
.add-to-cart button.button:hover span
{border-color:<?php echo $config['buttonSetting']['button_bg_porduct_hover']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_text_porduct'] ) : ?>
.add-to-cart button.button span
{color:<?php echo $config['buttonSetting']['button_text_porduct']; ?>;}
<?php endif; ?>

<?php if ( $config['buttonSetting']['button_text_porduct_hover'] ) : ?>
.add-to-cart button.button:hover span
{color:<?php echo $config['buttonSetting']['button_text_porduct_hover']; ?>;}
<?php endif; ?>


<?php endif; ?>
/*-----------*/



/*header setting*/
<?php if ( $config['header_color_setting']['theme-color-option'] ) : ?>
<?php if($config['header_color_setting']['header_bg'] ) : ?>
.mainHeaderPan, .hideTopNav .mainHeaderPan,
.headerfix.hideTopNav .header-wrapper01, .headerfix.hideTopNav .header-wrapper03 
{background-color:<?php echo $config['header_color_setting']['header_bg']; ?>}
<?php endif; ?>
<?php if($config['header_color_setting']['header_anchore_color'] ) : ?>
.header .links li a 
{color:<?php echo $config['header_color_setting']['header_anchore_color']; ?>}
<?php endif; ?>

<?php if($config['header_color_setting']['header_toplink_color'] ) : ?>
.header-top ul.links li a, .header-compare .classy a
{color:<?php echo $config['header_color_setting']['header_toplink_color']; ?>}
<?php endif; ?>

<?php if($config['header_color_setting']['header_cart_bg'] ) : ?>
.page .header-wrapper02 .header_cart, .header-container .block-cart .summary
{background:<?php echo $config['header_color_setting']['header_cart_bg']; ?>}
.page .header-wrapper02 .header_cart
{border-color:<?php echo $config['header_color_setting']['header_cart_bg']; ?>}
<?php endif; ?>

<?php if($config['header_color_setting']['header_cart_color'] ) : ?>
.header.container .header_cart .classy span
{color:<?php echo $config['header_color_setting']['header_cart_color']; ?>}
<?php endif; ?>

<?php if($config['header_color_setting']['header_text_color'] ) : ?>
.mainHeaderPan .header-top, 
.select_lang span, 
.currency_pan span, 
.header .welcome-msg, 
.header_cart .classy a, 
.header_cart .classy span,
.header-container .header-top .language-switcher label, .header-container  .header-top .header_currency label, .header-container .header-top .call-us label, .header-container  .header-top .welcome-msg
{color:<?php echo $config['header_color_setting']['header_text_color']; ?>}
<?php endif; ?>

<?php if($config['header_color_setting']['header_top_bar'] ) : ?>
.header-container .header-top
{background:<?php echo $config['header_color_setting']['header_top_bar']; ?>}
<?php endif; ?>
<?php endif; ?>