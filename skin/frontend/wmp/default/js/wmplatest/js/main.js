 
jQuery(document).ready(function(){ 
     


// jQuery(".slidingDiv1").hide();
//    jQuery(".show_hide1").show();

//    jQuery('.show_hide1').toggle(function(e){
//         e.preventDefault();
//        jQuery("#plus").text("Less");
//        jQuery(".slidingDiv1").slideDown();
       
//    },function(e){
//         e.preventDefault();
//        jQuery("#plus").text("More");
//        jQuery(".slidingDiv1").slideUp();
//    }); 

// Master slider Starts
var slider = new MasterSlider();
slider.setup('masterslider' , {
  width:1024,
  height:600,
  space:5, 
  view:'basic',
  fullwidth:true,
  speed:20
});

slider.control('arrows' ,{insertTo:'#masterslider'}); 
//slider.control('bullets');  



// jQuery('#myTab a').click(function(e) {
//  e.preventDefault();
//  jQuery(this).tab('show');
// }); 
//SyntaxHighlighter.all();

// code for Showcase
 var slider1 = new MasterSlider();
slider1.setup('masterslider1' , {
  width:540,
  height:586,
  space:5,
  view:'scale'
});

slider1.control('arrows'); 
slider1.control('thumblist' , {autohide:false ,dir:'h',arrows:false});

// Master slider Ends


//jQuery('.fa-heart').fadeOut();
jQuery(".heart").click(function(){
  jQuery(this).find(".fa-heart").fadeIn(500);
});


/*jQuery('.listV').click(function(){ 
  jQuery(this).addClass("listVActive"); 
  jQuery(".gridV").removeClass("gridVActive"); 
  jQuery(".prodMainBoxIn").addClass("prodBoxInList"); 
});
jQuery('.gridV').click(function(){ 
  jQuery(this).addClass("gridVActive"); 
  jQuery(".listV").removeClass("listVActive"); 
  jQuery(".prodMainBoxIn").removeClass("prodBoxInList"); 
});*/

// Cart Menu
jQuery(document).click(function() {
  jQuery(".cartMenu").fadeOut();  
});
jQuery('.menuEcom').click(function(e){  
  e.stopPropagation();
  jQuery(".cartMenu").toggle();  
});
/*jQuery('.menuEcom i').mouseover(function(e){  
  e.stopPropagation();
  jQuery(".cartMenu").fadeIn();  
});
jQuery('.menuEcom').mouseout(function(e){  
  e.stopPropagation();
  jQuery(".cartMenu").fadeOut();  
});
jQuery('.cartMenu').mouseover(function(e){  
  e.stopPropagation();
  jQuery(".cartMenu").fadeIn();  
});*/
jQuery('.cartMenu').click(function(e) {  
  e.stopPropagation();
 });

jQuery(".menuLogin").mouseover(function(e){  
  e.stopPropagation();
  jQuery(".cartMenu").fadeOut();  
});

//-------------------------------------
/* 
* @Author : 
* @Updated By : preetesh jain (10th feb 16)
* @Updations  : commented below code to solve the , first name ,last name and login functionality issue
* @Functionality : Customer registration and login form
*/

/*
jQuery(".re-enterChecked").click(function(){

	if(jQuery(this).prop("checked") == true){

       document.getElementById("re-enter").disabled = false;
       document.getElementById("firstname").disabled = false;
       document.getElementById("lastname").disabled = false;

     jQuery(".logReglog").css("display","none") ;

     jQuery(".logReg").css("display","block") ;

     jQuery('#login-form').attr('action', BaseUrl+'customer/account/createpost');

     jQuery('#pass').attr('name', 'password');

     jQuery('#email').attr('name', 'email');

     jQuery("#re-enter").addClass('input-text required-entry validate-cpassword');
     jQuery("#firstname").addClass('input-text required-entry');
     jQuery("#lastname").addClass('input-text required-entry');

    }

    else if(jQuery(this).prop("checked") == false){

      document.getElementById("re-enter").disabled = true;
       document.getElementById("firstname").disabled = true;
       document.getElementById("lastname").disabled = true;


      jQuery(".logReglog").css("display","block") ;

      jQuery(".logReg").css("display","none") ;

      jQuery('#login-form').attr('action', BaseUrl+'customer/account/login');

      jQuery('#pass').attr('name', 'login[password]');

      jQuery('#email').attr('name', 'login[username]');

      jQuery("#re-enter").removeClass('input-text required-entry validate-cpassword');
      jQuery("#firstname").removeClass('input-text required-entry');
      jQuery("#lastname").removeClass('input-text required-entry');
    }

 });
*/


/* 
* @Author    : preetesh jain (10th feb 16)
* @Updation  : updated the code for solving the first name , last name and login/registartion related issue
* @Functionality : Customer registration and login form
*/
jQuery(".re-enterChecked").click(function(){
  if(jQuery(this).prop("checked") == true){
     jQuery(".logReglog").css("display","none") ;
     jQuery(".logReg").css("display","block") ;
     jQuery('#login-form').attr('action', BaseUrl+'customer/account/createpost');
     jQuery('#pass').attr('name', 'password');
     jQuery('#email').attr('name', 'email');

      /* created a variable , which contaions confirm password, first name and last name field for registration form */
      var registration_field =  '<div class="registration_field"><input type="password" class="loginField input-text required-entry validate-cpassword" id="re-enter" name="confirmation" placeholder="Re-enter Password" /><input type="text" class="loginField input-text required-entry" maxlength="255" title="First Name" value="" name="firstname" id="firstname"  placeholder="First Name" /><input type="text" class="loginField input-text required-entry" maxlength="255" title="Last Name" value="" name="lastname" id="lastname"  placeholder="Last Name" /></div>';

      /* add the above registation filed variable before the registation(submit) button */
      jQuery('.logReglog').before(registration_field);
    }
    else if(jQuery(this).prop("checked") == false){
     
      jQuery(".logReglog").css("display","block") ;
      jQuery(".logReg").css("display","none") ;
      jQuery('#login-form').attr('action', BaseUrl+'customer/account/loginPost');
      jQuery('#pass').attr('name', 'login[password]');
      jQuery('#email').attr('name', 'login[username]');
      
      /* remove the registration field block , when customer selects login */
      jQuery('.registration_field').remove();

    }
});



jQuery(".ddd").on("click", function () {

    var $button = jQuery(this);
    var oldValue = $button.closest('.sp-quantity').find("input.quntity-input").val();

    if ($button.text() == "+") {
        var newVal = parseFloat(oldValue) + 1;
    } else {
        // Don't allow decrementing below zero
        if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 0;
        }
    }

    $button.closest('.sp-quantity').find("input.quntity-input").val(newVal);
//-------------------------------------------------------


});

// for the switching the icons on faq page
function toggleChevron(e) {
    jQuery(e.target)
        .prev('.panel-heading')
        .find("em.indicator")
        .toggleClass('glyphicon-minus glyphicon-plus');
}
jQuery('#accordion').on('hidden.bs.collapse', toggleChevron);
jQuery('#accordion').on('shown.bs.collapse', toggleChevron);



});
jQuery( window ).load(function() {
   




if (jQuery(window).width() < 768) {
    jQuery('.panel-collapse').collapse('show');
    jQuery('.accordion-toggle').attr('data-toggle', '');
}
// if (window.screen.width > 767) {
//   // resolution is below 10 x 7
//   jQuery('.panel-collapse:not(".in")')
//     .collapse('show');
// }
  // jQuery(window).resize(function(){     
  //   if(jQuery(window).width() < 767)
  //     {
  //         jQuery('.panel-collapse:not(".in")')
  //   .collapse('show');
  //     } else {
  //        // change functionality for larger screens
  //     }
     
  // });

});

