var count = 1;
var start_input_ids = ["suns","mons","tues","weds","thus","fris","sats"];
var end_input_ids = ["sune","mone","tuee","wede","thue","frie","sate"];
jQuery(document).ready(function(){
	jQuery(".wk_bs_drop_container").trigger("click");
	jQuery(".wk_bs_options").each(function(){
		if(jQuery(this).attr("id") == saved_data.booking_type)
			jQuery(this).trigger("click");
	});
	jQuery("#wk_bs_form").find("#date1").val(saved_data.datefrom);
	jQuery("#wk_bs_form").find("#date2").val(saved_data.dateto);
	if(saved_data.booking_type == 2){
		var counter = 1;
		jQuery.each(otm_from,function(){
			jQuery(".wk_bs_add_booking_button").trigger("click");
			//setting values for from
			var from_input = jQuery("#wk_bs_form").find("#otm_from"+counter);
			from_input.val(otm_from[counter]);
			from_input.next().find("select").val(otm_from[counter].split("-")[0]);
			from_input.next().find(".wk_hour").val(otm_from[counter].split("-")[1].split(":")[0]);
			from_input.next().find(".wk_min").val(otm_from[counter].split("-")[1].split(":")[1]);
			//setting values for to
			var to_input = jQuery("#wk_bs_form").find("#otm_to"+counter);
			to_input.val(otm_to[counter]);
			to_input.next().find("select").val(otm_to[counter].split("-")[0]);
			to_input.next().find(".wk_hour").val(otm_to[counter].split("-")[1].split(":")[0]);
			to_input.next().find(".wk_min").val(otm_to[counter].split("-")[1].split(":")[1]);
			counter++;
		});
	}
	else{	
		jQuery("#wk_bs_form").find(".wk_timeslot").val(saved_data.timeslot);
		jQuery("#wk_bs_form").find(".wk_cache").val(saved_data.cache_time);
		suns_html = jQuery("#wk_bs_form").find("#suns");
		if(saved_data.suns != "closed"){
			suns_html.val(saved_data.suns);suns_html.next().find(".wk_hour").val(saved_data.suns.split(":")[0]);suns_html.next().find(".wk_minute").val(saved_data.suns.split(":")[1]);
		}
		else{
			suns_html.val(saved_data.suns);suns_html.next().find(".wk_hour").val(0);suns_html.next().find(".wk_minute").val(0);suns_html.next().find(".wk_meridian").val("closed");
		}
		sune_html = jQuery("#wk_bs_form").find("#sune");
		if(saved_data.sune != "closed"){
			sune_html.val(saved_data.sune);sune_html.next().find(".wk_hour").val(saved_data.sune.split(":")[0]);sune_html.next().find(".wk_minute").val(saved_data.sune.split(":")[1]);
		}
		else{
			sune_html.val(saved_data.sune);sune_html.next().find(".wk_hour").val(0);sune_html.next().find(".wk_minute").val(0);sune_html.next().find(".wk_meridian").val("closed");
		}
		mons_html = jQuery("#wk_bs_form").find("#mons");
		if(saved_data.mons != "closed"){
			mons_html.val(saved_data.mons);mons_html.next().find(".wk_hour").val(saved_data.mons.split(":")[0]);mons_html.next().find(".wk_minute").val(saved_data.mons.split(":")[1]);
		}
		else{
			mons_html.val(saved_data.mons);mons_html.next().find(".wk_hour").val(0);mons_html.next().find(".wk_minute").val(0);mons_html.next().find(".wk_meridian").val("closed");
		}
		mone_html = jQuery("#wk_bs_form").find("#mone");
		if(saved_data.mone != "closed"){
			mone_html.val(saved_data.mone);mone_html.next().find(".wk_hour").val(saved_data.mone.split(":")[0]);mone_html.next().find(".wk_minute").val(saved_data.mone.split(":")[1]);
		}
		else{
			mone_html.val(saved_data.mone);mone_html.next().find(".wk_hour").val(0);mone_html.next().find(".wk_minute").val(0);mone_html.next().find(".wk_meridian").val("closed");
		}
		tues_html = jQuery("#wk_bs_form").find("#tues");
		if(saved_data.tues != "closed"){
			tues_html.val(saved_data.tues);tues_html.next().find(".wk_hour").val(saved_data.tues.split(":")[0]);tues_html.next().find(".wk_minute").val(saved_data.tues.split(":")[1]);
		}
		else{
			tues_html.val(saved_data.tues);tues_html.next().find(".wk_hour").val(0);tues_html.next().find(".wk_minute").val(0);tues_html.next().find(".wk_meridian").val("closed");
		}
		tuee_html = jQuery("#wk_bs_form").find("#tuee");
		if(saved_data.tuee != "closed"){
			tuee_html.val(saved_data.tuee);tuee_html.next().find(".wk_hour").val(saved_data.tuee.split(":")[0]);tuee_html.next().find(".wk_minute").val(saved_data.tuee.split(":")[1]);
		}
		else{
			tuee_html.val(saved_data.tuee);tuee_html.next().find(".wk_hour").val(0);tuee_html.next().find(".wk_minute").val(0);tuee_html.next().find(".wk_meridian").val("closed");
		}
		weds_html = jQuery("#wk_bs_form").find("#weds");
		if(saved_data.weds != "closed"){
			weds_html.val(saved_data.weds);weds_html.next().find(".wk_hour").val(saved_data.weds.split(":")[0]);weds_html.next().find(".wk_minute").val(saved_data.weds.split(":")[1]);
		}
		else{
			weds_html.val(saved_data.weds);weds_html.next().find(".wk_hour").val(0);weds_html.next().find(".wk_minute").val(0);weds_html.next().find(".wk_meridian").val("closed");
		}
		wede_html = jQuery("#wk_bs_form").find("#wede");
		if(saved_data.wede != "closed"){
			wede_html.val(saved_data.wede);wede_html.next().find(".wk_hour").val(saved_data.wede.split(":")[0]);wede_html.next().find(".wk_minute").val(saved_data.wede.split(":")[1]);
		}
		else{
			wede_html.val(saved_data.wede);wede_html.next().find(".wk_hour").val(0);wede_html.next().find(".wk_minute").val(0);wede_html.next().find(".wk_meridian").val("closed");
		}
		thus_html = jQuery("#wk_bs_form").find("#thus");
		if(saved_data.thus != "closed"){
			thus_html.val(saved_data.thus);thus_html.next().find(".wk_hour").val(saved_data.thus.split(":")[0]);thus_html.next().find(".wk_minute").val(saved_data.thus.split(":")[1]);
		}
		else{
			thus_html.val(saved_data.thus);thus_html.next().find(".wk_hour").val(0);thus_html.next().find(".wk_minute").val(0);thus_html.next().find(".wk_meridian").val("closed");
		}
		thue_html = jQuery("#wk_bs_form").find("#thue");
		if(saved_data.thue != "closed"){
			thue_html.val(saved_data.thue);thue_html.next().find(".wk_hour").val(saved_data.thue.split(":")[0]);thue_html.next().find(".wk_minute").val(saved_data.thue.split(":")[1]);
		}
		else{
			thue_html.val(saved_data.thue);thue_html.next().find(".wk_hour").val(0);thue_html.next().find(".wk_minute").val(0);thue_html.next().find(".wk_meridian").val("closed");
		}
		fris_html = jQuery("#wk_bs_form").find("#fris");
		if(saved_data.fris != "closed"){
			fris_html.val(saved_data.fris);fris_html.next().find(".wk_hour").val(saved_data.fris.split(":")[0]);fris_html.next().find(".wk_minute").val(saved_data.fris.split(":")[1]);
		}
		else{
			fris_html.val(saved_data.fris);fris_html.next().find(".wk_hour").val(0);fris_html.next().find(".wk_minute").val(0);fris_html.next().find(".wk_meridian").val("closed");
		}
		frie_html = jQuery("#wk_bs_form").find("#frie");
		if(saved_data.frie != "closed"){
			frie_html.val(saved_data.frie);frie_html.next().find(".wk_hour").val(saved_data.frie.split(":")[0]);frie_html.next().find(".wk_minute").val(saved_data.frie.split(":")[1]);
		}
		else{
			frie_html.val(saved_data.frie);frie_html.next().find(".wk_hour").val(0);frie_html.next().find(".wk_minute").val(0);frie_html.next().find(".wk_meridian").val("closed");
		}
		sats_html = jQuery("#wk_bs_form").find("#sats");
		if(saved_data.sats != "closed"){
			sats_html.val(saved_data.sats);sats_html.next().find(".wk_hour").val(saved_data.sats.split(":")[0]);sats_html.next().find(".wk_minute").val(saved_data.sats.split(":")[1]);
		}
		else{
			sats_html.val(saved_data.sats);sats_html.next().find(".wk_hour").val(0);sats_html.next().find(".wk_minute").val(0);sats_html.next().find(".wk_meridian").val("closed");
		}
		sate_html = jQuery("#wk_bs_form").find("#sate");
		if(saved_data.sate != "closed"){
			sate_html.val(saved_data.sate);sate_html.next().find(".wk_hour").val(saved_data.sate.split(":")[0]);sate_html.next().find(".wk_minute").val(saved_data.sate.split(":")[1]);
		}
		else{
			sate_html.val(saved_data.sate);sate_html.next().find(".wk_hour").val(0);sate_html.next().find(".wk_minute").val(0);sate_html.next().find(".wk_meridian").val("closed");
		}
	}
});
jQuery(".wk_bs_drop_container").click(function(){
	if(!jQuery(this).hasClass("open"))
		jQuery(this).addClass("open");
	else
		jQuery(this).removeClass("open");
});
jQuery(".wk_bs_options").click(function(){
	jQuery(".wk_bs_opening_text").text(jQuery(this).text());
	jQuery("#booking_type").val(jQuery(this).attr("id"));
	if(jQuery(this).attr("id") == 1)
		many_in_one();
	else if(jQuery(this).attr("id") == 2)
		one_on_many();
	else if(jQuery(this).attr("id") == 0)
		jQuery("#wk_bs_form").html("");
});
var from_to_html = "<div class='wk_bs_from'><label class='wk_bs_lblfrom wk_bs_lbl'>"+from_text+" : </label><div id='wk_bs_datefrom' class='input-append date'><input type='text' class='wk_dp_input required-entry' readonly='' id='date1' name='datefrom'/><span class='wk_dp_addon add-on' id='date_trig1'><i style='background-position:-192px -120px;' class='icon-calendar'></i></span></div></div><div class='wk_bs_to'><label class='wk_bs_lblto wk_bs_lbl'>"+to_text+" : </label><div id='wk_bs_dateto' class='input-append date'><input type='text' class='wk_dp_input required-entry' readonly='' id='date2' name='dateto'/><span class='wk_dp_addon add-on' id='date_trig2'><i style='background-position:-192px -120px;' class='icon-calendar'></i></span></div></div><div style='padding:3px;margin-bottom:5px;border:2px dotted rgb(226, 226, 226);'></div>";
function add_bookings(){
	var booking_html = "<div class='wk_bs_otm_row_container'><div class='wk_bs_otm_row'><div class='wk_bs_otm_row_from'><div class='wk_bs_otm_row_icon'><span onclick='openbookingdetail(this);' class='wk_bs_otm_row_action_span'><i class='wk_bs_otm_row_action'></i></span><div class='wk_bs_otm_row_from_input'><input class='wk_bs_otm_row_from_maininput required-entry' id='otm_from"+count+"' type='text' name='otm_from["+count+"]' readonly='readonly' /><div class='wk_bs_otm_row_input_container'><select><option value=''>Select day</option><option value='sunday'>Sunday</option><option value='monday'>Monday</option><option value='tuesday'>Tuesday</option><option value='wednesday'>Wednesday</option><option value='thursday'>Thursday</option><option value='friday'>Friday</option><option value='saturday'>Saturday</option></select><div class='wk_bs_otm_time'><input value='0' type='text' disabled='disabled' class='wk_hour'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div><input value='0' type='text' disabled='disabled' class='wk_min'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div></div></div></div></div></div><div class='wk_bs_otm_row_to'><div class='wk_bs_otm_row_icon'><span onclick='openbookingdetail(this);' class='wk_bs_otm_row_action_span'><i class='wk_bs_otm_row_action'></i></span><div class='wk_bs_otm_row_from_input'><input class='wk_bs_otm_row_from_maininput required-entry' id='otm_to"+count+"' type='text' name='otm_to["+count+"]' readonly='readonly' /><div class='wk_bs_otm_row_input_container'><select><option value=''>Select day</option><option value='sunday'>Sunday</option><option value='monday'>Monday</option><option value='tuesday'>Tuesday</option><option value='wednesday'>Wednesday</option><option value='thursday'>Thursday</option><option value='friday'>Friday</option><option value='saturday'>Saturday</option></select><div class='wk_bs_otm_time'><input value='0' type='text' disabled='disabled' class='wk_hour'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div><input value='0' type='text' disabled='disabled' class='wk_min'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div></div></div></div></div></div><span onclick='remove_booking(this);' class='wk_bs_remove_booking'><i></i></span></div></div>";
	jQuery("#wk_bs_booking_pannel").append(booking_html);
	var daysignalup,daysignaldn;
	jQuery(".wk_days_up").mousedown(function(){
		var thisthis = jQuery(this);
		daysignalup = setInterval(function(){
			var val = parseInt(thisthis.parent().prev().val());
			if(thisthis.parent().prev().attr("class") == "wk_hour"){
				if(val < 24)
					thisthis.parent().prev().val(val += 1);
			}
			else{
				if(val < 60)
					thisthis.parent().prev().val(val += 1);
			}
		}, 50);
	});
	jQuery(".wk_days_up").bind("mouseup mouseout",function(){clearInterval(daysignalup);});
	jQuery(".wk_days_down").mousedown(function(){
		var thisthis = jQuery(this);
		daysignaldn = setInterval(function(){
			var val = parseInt(thisthis.parent().prev().val());
			if(val > 0)
				thisthis.parent().prev().val(val -= 1);
		}, 50);
	});
	jQuery(".wk_days_down").bind("mouseup mouseout",function(){clearInterval(daysignaldn);});
	count++;
}
function remove_booking(this_this){
	jQuery(this_this).parents(".wk_bs_otm_row_container").remove();
}
function openbookingdetail(this_this){
	var target = jQuery(this_this).next().find(".wk_bs_otm_row_input_container");
	var target_icon = jQuery(this_this).find(".wk_bs_otm_row_action");
	if(!target.hasClass("showing")){
		target_icon.addClass("showing");
		target.fadeIn(500).addClass("showing");
	}
	else{
		target_icon.removeClass("showing");
		target.fadeOut(500).removeClass("showing");
		var select_val = jQuery(this_this).next().find("select").val();
		var hour_val = jQuery(this_this).next().find(".wk_hour").val();
		var min_val = jQuery(this_this).next().find(".wk_min").val();
		if(select_val != "" && hour_val != "" && min_val != "")
			jQuery(this_this).next().find(".wk_bs_otm_row_from_maininput").val(select_val+"-"+hour_val+":"+min_val);
		else
			alert("You didn't select values");
	}
}
function one_on_many(){
	var one_on_many_html = from_to_html;
	one_on_many_html += "<div class='wk_bs_add_booking_container'><div class='wk_bs_add_booking_button' onclick='add_bookings();'>Add Bookings</div></div><div style='padding:3px;margin-bottom:5px;border:2px dotted rgb(226, 226, 226);'></div><div id='wk_bs_booking_pannel'></div>";
	jQuery("#wk_bs_form").html(one_on_many_html);
	Calendar.setup({
		inputField : "date1",
		ifFormat : "%m/%d/%Y",
		button : "date_trig1",
		align : "Br",
		singleClick : true
	});
	Calendar.setup({
		inputField : "date2",
		ifFormat : "%m/%d/%Y",
		button : "date_trig2",
		align : "Br",
		singleClick : true
	});
}
function many_in_one(){	
	var many_in_one_html = from_to_html;
	many_in_one_html += "<div class='wk_bs_days'><label class='wk_bs_daylbl wk_bs_lbl'>"+time_slot_text+" : </label><div id='wk_bs_dateto' class='wk_bs_dayinput input-append date'><input style='border-radius:4px;' readonly='' type='text' class='wk_dp_input required-entry wk_range wk_timeslot' value='0' name='timeslot'></input></div><div class='wk_dur_pnl'><span style='float:right;padding-top:5px;'>"+hint_text+"</span><span class='wk_dur_up'></span><span class='wk_dur_down'></span></div></div><div class='wk_bs_days'><label class='wk_bs_daylbl wk_bs_lbl'>Buffer cache time after each booking: </label><div id='wk_bs_dateto' class='wk_bs_dayinput input-append date'><input style='border-radius:4px;' readonly='' type='text' class='wk_dp_input required-entry wk_range wk_cache' value='0' name='cache_time'></input></div><div class='wk_dur_pnl'><span style='float:right;padding-top:5px;'>"+hint_text+"</span><span class='wk_dur_up'></span><span class='wk_dur_down'></span></div></div>";
	for(var i = 0; i < 7; i++) {
		many_in_one_html += "<div class='wk_bs_days'><label class='wk_bs_daylbl wk_bs_lbl'>"+start_label[i]+" : </label><div class='wk_bs_dayinput' class='input-append date'><input readonly='' type='text' class='wk_dp_input required-entry' id='"+start_input_ids[i]+"' name='"+start_input_ids[i]+"'></input><div class='wk_time_slide_cntnt'><input value='0' type='text' disabled='disabled' class='wk_hour'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div><input value='0' type='text' disabled='disabled' class='wk_min'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div><select class='wk_meridian'><option value=''>"+open+"</option><option value='closed'>"+closed+"</option></select></div><span class='wk_dp_addon add-on wk_time_slide' id='close'><i class='icon-calendar'></i></span></div></div><div class='wk_bs_days'><label class='wk_bs_daylbl wk_bs_lbl'>"+end_label[i]+" : </label><div class='wk_bs_dayinput' class='input-append date'><input readonly='' type='text' class='wk_dp_input required-entry' id='"+end_input_ids[i]+"' name='"+end_input_ids[i]+"'></input><div class='wk_time_slide_cntnt'><input value='0' type='text' disabled='disabled' class='wk_hour'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div><input value='0' type='text' disabled='disabled' class='wk_min'/><div class='wk_days_pnl'><span class='wk_days_up'></span><span class='wk_days_down'></span></div><select class='wk_meridian'><option value=''>"+open+"</option><option value='closed'>"+closed+"</option></select></div><span class='wk_dp_addon add-on wk_time_slide' id='close'><i class='icon-calendar'></i></span></div></div>";
	}
	jQuery("#wk_bs_form").html(many_in_one_html);
	Calendar.setup({
		inputField : "date1",
		ifFormat : "%m/%d/%Y",
		button : "date_trig1",
		singleClick : true
	});
	Calendar.setup({
		inputField : "date2",
		ifFormat : "%m/%d/%Y",
		button : "date_trig2",
		align : "Br",
		singleClick : true
	});
	jQuery(".wk_time_slide").click(function(){
		if(jQuery(this).attr("id") == "close"){
			jQuery(this).attr("id","open");
			jQuery(this).animate({"left":"+=201px"},500,function(){
				jQuery(this).find(".icon-calendar").css({"background-position":"-432px -72px"});
			});
			jQuery(this).prev().animate({"left":"+=201px"},500);
		}
		else{
			var time = "";
			var error_signal = false;var which_is_this = "";
			var this_hour = jQuery(this).prev().find(".wk_hour").val();
			var this_minute = jQuery(this).prev().find(".wk_min").val();
			var next_main_input = jQuery(this).parents(".wk_bs_days").next(".wk_bs_days").find(".wk_dp_input");
			var previous_main_input = jQuery(this).parents(".wk_bs_days").prev(".wk_bs_days").find(".wk_dp_input");
			time += this_hour+":"+this_minute;
			var dayid = jQuery(this).siblings(".wk_dp_input").attr("id");
			if(dayid == "suns" || dayid == "mons" || dayid == "tues" || dayid == "weds" || dayid == "thus" || dayid == "fris" || dayid ==  "sats"){
				if(jQuery(this).prev().find(".wk_meridian").val() == "closed"){
					time = "closed";
					next_main_input.val(time);
				}
				else{
					if(next_main_input.val() != ""){
						var time_of_next = next_main_input.val().split(":");
						if(parseInt(this_hour) > parseInt(time_of_next[0])){
							alert(jQuery(this).parents(".wk_bs_dayinput").siblings(".wk_bs_daylbl").text().split(":")[0]+"must be smaller than "+jQuery(this).parents(".wk_bs_days").next().find(".wk_bs_daylbl").text().split(":")[0]);
							error_signal = true;
						}
						else if(parseInt(this_hour) == parseInt(time_of_next[0])){
							if(parseInt(this_minute) >= parseInt(time_of_next[1])){
								alert(jQuery(this).parents(".wk_bs_dayinput").siblings(".wk_bs_daylbl").text().split(":")[0]+"must be smaller than "+jQuery(this).parents(".wk_bs_days").next().find(".wk_bs_daylbl").text().split(":")[0]);
								error_signal = true;
							}
						}
					}
				}
				if(error_signal == false)
					which_is_this = "start";
			}
			else 
			if(dayid == "sune" || dayid == "mone" || dayid == "tuee" || dayid == "wede" || dayid == "thue" || dayid == "frie" || dayid ==  "sate"){
				if(jQuery(this).prev().find(".wk_meridian").val() == "closed"){
					time = "closed";
					previous_main_input.val(time);
				}
				else{
					if(previous_main_input.val() != ""){
						var time_of_previous = previous_main_input.val().split(":");
						if(parseInt(this_hour) < parseInt(time_of_previous[0])){
							alert(jQuery(this).parents(".wk_bs_dayinput").siblings(".wk_bs_daylbl").text().split(":")[0]+"must be greater than "+jQuery(this).parents(".wk_bs_days").prev().find(".wk_bs_daylbl").text().split(":")[0]);
							error_signal = true;
						}
						else if(parseInt(this_hour) == parseInt(time_of_previous[0])){
							if(parseInt(this_minute) <= parseInt(time_of_previous[1])){
								alert(jQuery(this).parents(".wk_bs_dayinput").siblings(".wk_bs_daylbl").text().split(":")[0]+"must be greater than "+jQuery(this).parents(".wk_bs_days").prev().find(".wk_bs_daylbl").text().split(":")[0]);
								error_signal = true;
							}
						}
					}
				}
				if(error_signal == false)
					which_is_this = "end";
			}
			if(error_signal == false){
				if(jQuery(this).prev().find(".wk_meridian").val() == ""){
					if(which_is_this == "end"){
						if(previous_main_input.val() == "closed"){
							previous_main_input.val("0:0");
							previous_main_input.next().find(".wk_hour").val(0);
							previous_main_input.next().find(".wk_minute").val(0);
						}
					}
					else{
						if(next_main_input.val() == "closed"){
							next_main_input.val("0:0");
							next_main_input.next().find(".wk_hour").val(0);
							next_main_input.next().find(".wk_minute").val(0);
						}
					}
				}
				jQuery(this).siblings(".wk_dp_input").val(time);
				jQuery(this).attr("id","close");
				jQuery(this).animate({"left":"-=201px"},500,function(){
					jQuery(this).find(".icon-calendar").css({"background-position":"-457px -72px"});
				});
				jQuery(this).prev().animate({"left":"-=201px"},500);
			}
		}		
	});	
	var daysignalup,daysignaldn,signalup,signaldn;
	jQuery(".wk_days_up").mousedown(function(){
		var thisthis = jQuery(this);
		daysignalup = setInterval(function(){
			var val = parseInt(thisthis.parent().prev().val());
			if(thisthis.parent().prev().attr("class") == "wk_hour"){
				if(val < 24)
					thisthis.parent().prev().val(val += 1);
			}
			else{
				if(val < 60)
					thisthis.parent().prev().val(val += 1);
			}
		}, 50);
	});
	jQuery(".wk_days_up").bind("mouseup mouseout",function(){clearInterval(daysignalup);});
	jQuery(".wk_days_down").mousedown(function(){
		var thisthis = jQuery(this);
		daysignaldn = setInterval(function(){
			var val = parseInt(thisthis.parent().prev().val());
			if(val > 0)
				thisthis.parent().prev().val(val -= 1);
		}, 50);
	});
	jQuery(".wk_days_down").bind("mouseup mouseout",function(){clearInterval(daysignaldn);});
	jQuery(".wk_dur_up").mousedown(function(){
		var this_this = jQuery(this);
		signalup = setInterval(function(){
			var wk_range = this_this.parents(".wk_dur_pnl").prev().find(".wk_range");
			var val = parseInt(wk_range.val());
			if(!val) val = 0;
			wk_range.val(val+=1);
		}, 50);
	});
	jQuery(".wk_dur_up").bind("mouseup mouseout",function(){clearInterval(signalup);});
	jQuery(".wk_dur_down").mousedown(function(){
		var this_this = jQuery(this);
		signaldn = setInterval(function(){
			var wk_range = this_this.parents(".wk_dur_pnl").prev().find(".wk_range");
			var val = parseInt(wk_range.val());
			if(val > 0)
				wk_range.val(val-=1);
		}, 50);		
	});
	jQuery(".wk_dur_down").bind("mouseup mouseout",function(){clearInterval(signaldn);});
}