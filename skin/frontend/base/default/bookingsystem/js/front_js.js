var from_from;
var to_to;
jQuery(document).ready(function(){
	jQuery("#product-options-wrapper dt").each(function(){
	    if(jQuery(this).text().trim().indexOf(reservation_from) >= 0){
	        jQuery(this).hide();
	        from_from = jQuery(this).next("dd");
	        from_from.hide();
	    }
	    if(jQuery(this).text().trim().indexOf(reservation_to) >= 0){
	        jQuery(this).hide();
	        to_to = jQuery(this).next("dd");
	        to_to.hide();
	    }
	});
	jQuery(".btn-cart").attr("onclick","validate_add_to_cart()");
	jQuery(".wk_bs_footer").html(jQuery(".btn-cart").eq(0).clone());
});
jQuery(".wk_bs_title span").click(function(){
	jQuery("#wk_bs_popup_background").fadeIn(100,function(){
		jQuery("#wk_bs_popup_container").animate({"top":"100px"},1000,"easeOutCubic");
	});
});
jQuery(".wk_bs_booking_dates").click(function(){
	jQuery(".wk_bs_booking_dates").removeClass("selected");
	jQuery(this).addClass("selected");
	var top_top = jQuery(this).index()*200;
	if(top_top != 0)
		top_top *= -1;
	jQuery(".wk_bs_individual_booking_slider").animate({"top":top_top},1000,"easeOutCirc");
});
jQuery(".wk_ofm_bookthis").change(function(){
	from_array = jQuery(this).siblings(".wk_raw_from").text().split("-");
	to_array = jQuery(this).siblings(".wk_raw_to").text().split("-");
	from_day_array = from_array[0].split("/");
	to_day_array = to_array[0].split("/");
	for(var j=0; j<=2; j++){
        from_from.find("select:eq("+j+")").val(from_day_array[j]);
        to_to.find("select:eq("+j+")").val(to_day_array[j]);
    }
    from_time_array = from_array[1].split(":");
	to_time_array = to_array[1].split(":");
    for(var i=0; i<2; i++) {
    	if(i == 0){
	    	hour = from_time_array[0];
		    minute = from_time_array[1];
		    time_container = from_from.find(".time-picker");
		}
		else{
			hour = to_time_array[0];
		    minute = to_time_array[1];
		    time_container = to_to.find(".time-picker");
		}
		if(!minute)
			minute = 0;
	    meridian = "am";
	    if(hour > 12){
	    	hour = hour - 12;
	    	meridian = "pm"
	    }
	    if(hour == 12)
	        meridian = "pm";
	    if(hour == 0)
	        hour = 12;	    
	    time_container.find("select:eq(0)").val(hour);
	    time_container.find("select:eq(1)").val(minute);
	    time_container.find("select:eq(2)").val(meridian);
    }
});
jQuery(".wk_bookthis").change(function(){
	date = jQuery(".wk_bs_booking_dates").eq(jQuery(this).parents(".wk_bs_individual_booking_slider_box").index()).text().split("/");
    for(var j=0; j<=2; j++){
        from_from.find("select:eq("+j+")").val(date[j]);
        to_to.find("select:eq("+j+")").val(date[j]);
    }
    from_time = jQuery(this).parent().siblings().text().split("-")[0];
    to_time = jQuery(this).parent().siblings().text().split("-")[1];
    for(var i=0; i<2; i++) {
    	if(i == 0){
	    	hour = from_time.split(":")[0].replace(/^0+/,"");
		    minute = from_time.split(":")[1].replace(/^0+/,"");
		    time_container = from_from.find(".time-picker");
		}
		else{
			hour = to_time.split(":")[0].replace(/^0+/,"");
		    minute = to_time.split(":")[1].replace(/^0+/,"");
		    time_container = to_to.find(".time-picker");
		}
		if(!minute)
			minute = 0;
	    meridian = "am";
	    if(hour > 12){
	        hour = hour-12;
	        meridian = "pm";
	    }
	    if(hour == 12)
	        meridian = "pm";
	    if(hour == 0)
	        hour = 12;	   
	    time_container.find("select:eq(0)").val(hour);
	    time_container.find("select:eq(1)").val(minute);
	    time_container.find("select:eq(2)").val(meridian);
    }
});
function close_popup(this_this){
	jQuery(this_this).parents("#wk_bs_popup_container").animate({"top":"-1000px"},1000,"easeInCirc",function(){
		jQuery("#wk_bs_popup_background").fadeOut(100);
	});
}
function validate_add_to_cart(){
	var booking = false;
	jQuery(".wk_bookthis,.wk_ofm_bookthis").each(function(){
		if(jQuery(this).is(":checked"))
			booking = true;
	});
	if(booking == true)
		productAddToCartForm.submit(this)
	else{
		alert("Please Select Booking");
		if(jQuery("#wk_bs_popup_container").css("top") == "100px"){
			jQuery("#wk_bs_popup_container").animate({"left":"-20"},50,function(){
				jQuery(this).animate({"left":"40"},50,function(){
					jQuery(this).animate({"left":"-40"},50,function(){
						jQuery(this).animate({"left":"0"},50);
					});
				});
			});
		}
		else{
			jQuery("#wk_bs_main_container").addClass("wk_rs_alert");
			jQuery("#wk_bs_main_container").animate({"left":"-20"},50,function(){
				jQuery(this).animate({"left":"40"},50,function(){
					jQuery(this).animate({"left":"-40"},50,function(){
						jQuery(this).animate({"left":"0"},50,function(){
							jQuery("#wk_bs_main_container").removeClass("wk_rs_alert");
						});
					});
				});
			});
		}
	}
}
function select_prev_choice(){
	if(booking_type == 1){
		jQuery(".wk_bs_booking_dates").each(function(){
			if(jQuery(this).text() == booking_array.from_date){
				jQuery(this).trigger("click");
				date_index = jQuery(this).index();
			}			
		});
		jQuery(".wk_bs_individual_booking_slider_box").eq(date_index).find(".wk_bs_deal_time").each(function(){
			from_time = booking_array.from_time.split(":");
			to_time = booking_array.to_time.split(":");
			if(jQuery(this).text() == from_time[0]+":"+from_time[1]+"-"+to_time[0]+":"+to_time[1])
				jQuery(this).next().find(".wk_bookthis").attr("checked","checked");
		});
	}
	else{
		jQuery(".wk_bs_ofm_individual_bookings").each(function(){
			from_time = booking_array.from_time.split(":");
			to_time = booking_array.to_time.split(":");
			booking_block_from_date = jQuery(this).find(".wk_raw_from").text().split("-")[0];
			booking_block_to_date = jQuery(this).find(".wk_raw_to").text().split("-")[0];
			booking_block_from_time = jQuery(this).find(".wk_ofm_from").text().split("(")[1].split(")")[0];
			booking_block_to_time = jQuery(this).find(".wk_ofm_to").text().split("(")[1].split(")")[0];
			if(booking_block_from_date+"-"+booking_block_from_time == booking_array.from_date+"-"+from_time[0]+":"+from_time[1] && booking_block_to_date+"-"+booking_block_to_time == booking_array.to_date+"-"+to_time[0]+":"+to_time[1])
				jQuery(this).find(".wk_ofm_bookthis").attr("checked","checked");
		});
	}
}