<?php
// instantiate product object
$Main_Menu = "Users";
$Sub_Menu_1 = "list_user";
include_once 'config/auth.php';

$stmt = $user->listUsers();
//$log->LogMsg("Listing Of Users");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title>List Users</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" />-->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content" >

                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>List User </h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            List User Data
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50px">No</th>
                                                            <th style="width: 100px">First Name</th>
                                                            <th style="width: 100px">Last Name</th>
                                                            <th>E-mail</th>
                                                            <th>Phone </th>
                                                            <!--<th>Reference</th>
                                                            <th>Cast</th>
                                                            <th>Country</th> -->

                                                            <th style="width:105px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        $count = 0;
                                                        while ($row = $stmt->fetch_assoc()) {
                                                            $count++;
                                                            ?> 
                                                            <tr class="odd gradeX">
                                                                <td><?php echo $count; ?></td>
                                                                <td><?php echo $row['first_name']; ?></td>
                                                                <td><?php echo $row['last_name']; ?></td>
                                                                <td><?php echo $row['email_1']; ?></td>
                                                                <td><?php echo $row['phone_1']; ?></td>
                                                                <!-- <td><?php echo $row['reference_id']; ?></td>
                                                                <td><?php echo $row['caste_id']; ?></td>
                                                                <td><?php echo $row['country_id']; ?></td> -->





                                                                <td>
                                                                    <a href="add_user.php?edit_id=<?php echo $row['id']; ?>" class="btn text-info btn-xs btn-flat">
                                                                        <i class="icon-list-alt icon-white"></i> Show</a>
                                                                    <!--
                                                                    <div class="btn-group">                    
                                                                        <button type="submit"class="btn btn-primary">Action</button>
                                                                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></button>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a href="add_enquiry.php?user_id=<?php echo $row['id']; ?>">New Enquiry</a></li>
                                                                            <li><a href="list_enquiry.php?user_id=<?php echo $row['id']; ?>">List Enquiry</a></li>
                                                                            <li class="divider"></li>
                                                                            <li><a href="#">Edit</a></li>                        
                                                                            <li><a href="#">Deactivate</a></li>
                                                                        </ul>                  
                                                                    </div>
                                                                    -->
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            //echo '<button type="button" class="btn btn-success btn-circle"><i class="icon-link"></i>';
                                                            //echo '<button type="button" class="btn btn-warning btn-circle"><i class="icon-time"></i>';
                                                            //echo '<option value='.$rowFilms['id'].'>'.$rowFilms['country_name'].'</option>'; 
                                                        }
                                                        ?>





                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <hr />




                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';   ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->    


    </body>
    <!-- END BODY-->

</html>
