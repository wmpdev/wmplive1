<?php
require('Pusher.php');

$api_id=$_REQUEST["api_id"];
$api_key=$_REQUEST["api_key"];
$api_secret=$_REQUEST["api_secret"];
$title=$_REQUEST["title"];
$msg=$_REQUEST["msg"];
$caller_num=$_REQUEST["caller"];

$app_id = $api_id;
$app_key = $api_key;
$app_secret = $api_secret;

/*$app_id = '179396';
$app_key = '142ff267b641e853a453';
$app_secret = 'fcce78df394ac22ea3e4';*/

/*$app_id='181738';
$app_key = '0404f06f46331b843ea6';
$app_secret = 'ab2b0a1705484afb4404';*/

$pusher = new Pusher(
  $app_key,
  $app_secret,
  $app_id,
  array('encrypted' => true)
);

$data['alertTitle'] = $title;
$data['message'] = $msg;
$data['caller_num'] = $caller_num;
$pusher->trigger('test_channel', 'my_event', $data);
?>