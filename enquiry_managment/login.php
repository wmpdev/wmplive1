<?php
//print_r($_POST);
require_once("config/config.php");
if (!$login_user->CheckLogin()) {
    
} else {
    $login_user->urlredirection("index.php");
    exit;
}

if (isset($_POST['login']) == "login") {
    $login_user->username = $_POST['user_name'];
    $login_user->pwd = $_POST['user_password'];
    if ($login_user->Login()) {
        $login_user->urlredirection("index.php");
    }
}



$page_title = "Backend | Login Page";
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD -->
    <head>
        <meta charset="UTF-8" />
        <title>Backend | Login Page</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- PAGE LEVEL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/login.css" />
        <link rel="stylesheet" href="assets/plugins/magic/magic.css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body >

        <!-- PAGE CONTENT --> 
        <div class="container">
            <div class="text-center">
                <img src="assets/img/logo.png" id="" alt=" Logo" style="height: 88px; width: 186px;" />
            </div>

            <div class="inner">                
             
                <div class="tab-content">
                    <div id="login" class="tab-pane active">
                        <form action="" class="form-signin" method="post">
                            <div class="col-lg-13">

                        <?php if (strlen($login_user->SuccessMsg)) { ?>    
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php
                                echo $login_user->SuccessMsg . "Ninad";
                                $login_user->SuccessMsg = "";
                                ?>.
                            </div>
                        <?php } elseif (strlen($login_user->ErrorMsg)) {
                            ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <?php
                                echo $login_user->ErrorMsg;
                                $login_user->ErrorMsg = "";
                                ?>.
                            </div>
                        <?php } ?>

                    </div>
                            <p class="text-muted text-center btn-block btn btn-primary btn-rect">
                                Enter your username and password
                            </p>
                            <input type="text" placeholder="Username" class="form-control"  value="" name="user_name" id="user_name"/>
                            <input type="password" placeholder="Password" class="form-control" value="" name="user_password" id="user_password" />
                            <button class="btn text-muted text-center btn-danger" type="submit" name="login" value="login">Sign in</button>
                        </form>
                    </div>
                    <div id="forgot" class="tab-pane">
                        <form action="index.html" class="form-signin">
                            <p class="text-muted text-center btn-block btn btn-primary btn-rect">Enter your valid e-mail</p>
                            <input type="email"  value="" placeholder="Your E-mail"  class="form-control" />
                            <br />
                            <button class="btn text-muted text-center btn-success" type="submit">Recover Password</button>
                        </form>
                    </div>
                    <div id="signup" class="tab-pane">
                        <form action="index.html" class="form-signin">
                            <p class="text-muted text-center btn-block btn btn-primary btn-rect">Please Fill Details To Register</p>
                            <input type="text" placeholder="First Name" class="form-control" />
                            <input type="text" placeholder="Last Name" class="form-control" />
                            <input type="text" placeholder="Username" class="form-control" />
                            <input type="email" placeholder="Your E-mail" class="form-control" />
                            <input type="password" placeholder="password" class="form-control" />
                            <input type="password" placeholder="Re type password" class="form-control" />
                            <button class="btn text-muted text-center btn-success" type="submit">Register</button>
                        </form>
                    </div>
                </div>
                <div class="text-center">
                    <ul class="list-inline">
                        <li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>
                        <li><a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a></li>
                        <li><a class="text-muted" href="#signup" data-toggle="tab">Signup</a></li>
                    </ul>
                </div>

            </div>
        </div>

        <!--END PAGE CONTENT -->     

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/jquery-2.0.3.min.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.js"></script>
        <script src="assets/js/login.js"></script>
        <!--END PAGE LEVEL SCRIPTS -->

    </body>
    <!-- END BODY -->
</html>
