<?php

//include_once '../config/auth.php';
function logmsg($msg) {

    $log_time = date('Y-m-d H:i:s');

    $log_msg = "<br/>" . $log_time . " = " . $msg;

    $myFile = "sys_ninad_log.html";
    //echo "File = $myFile msg = $log_msg";
    $fh = fopen($myFile, 'a') or die("can't open file");
    $stringData = $log_msg;
    fwrite($fh, $stringData);
    fclose($fh);
}

function validateDate($date, $format = 'DD-MM-YYYY') {
    switch ($format) {
        case 'YYYY/MM/DD':
        case 'YYYY-MM-DD':
            list( $y, $m, $d ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'YYYY/DD/MM':
        case 'YYYY-DD-MM':
            list( $y, $d, $m ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'DD-MM-YYYY':
        case 'DD/MM/YYYY':
            list( $d, $m, $y ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'MM-DD-YYYY':
        case 'MM/DD/YYYY':
            list( $m, $d, $y ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'YYYYMMDD':
            $y = substr($date, 0, 4);
            $m = substr($date, 4, 2);
            $d = substr($date, 6, 2);
            break;

        case 'YYYYDDMM':
            $y = substr($date, 0, 4);
            $d = substr($date, 4, 2);
            $m = substr($date, 6, 2);
            break;

        default:
            throw new Exception("Invalid Date Format");
    }
    return checkdate($m, $d, $y);
}

function validateTime($date, $format = 'h:i a') {
    $date = strtolower($date);
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function validateEmail($email) {
    $error = 0;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = 1;
        logmsg("validateEmail -> Error invalid Email_id ($email)");
        //$msg = "Email id is Invalid format.";
        //$error_msg = getError($error_msg, $msg);
    }
    //echo $error;
    return $error;
}

function validateMobile($mobile_no) {
    $error = 1;
    $regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";

    if (!is_numeric($mobile_no)) {
        $error = 0;
        //$msg = "Customer Mobile No should be Numaric." . is_numeric($cust_no);
        //$error_msg = getError($error_msg, $msg);
    }

    if (!preg_match($regex, $mobile_no)) {
        $error = 0;
        //$msg = "Number 1 is Invalid number.";
        //$error_msg = getError($error_msg, $msg);
    }

    //echo $error;
    return $error;
}

function validatePujaData($puja_id, $puja_date, $puja_time) {
    $status = FALSE;
    //$msg = "";
    $error_msg = "sdfsdf";


    if ($puja_id > 0) {
        $error_msg = "Enter proper puja ";
        if (!(strlen(trim($puja_date)) > 0) || !validateDate($puja_date, $format = 'DD-MM-YYYY')) {
            $status = TRUE;
            //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Date";
            //$error_msg = getError($error_msg, "Enter proper puja Date.");
            $error_msg .= "Date, ";
        } else {
            $puja_date = date("Y-m-d", strtotime($puja_date));
        }
        //list($status, $change_time, $msg)=validateTime($puja_time_1,12,24);
        if (!(strlen(trim($puja_time)) > 0) || !validateTime($puja_time, $format = 'h:i a')) {
            $status = TRUE;
            //$error_msg = getError($error_msg, "Enter proper puja Time.");
            $error_msg .= "Time.";
        } else {
            $puja_time = date("H:i", strtotime($puja_time));
        }




        /*   Check Greater Date only
          $followup_date = date("Y-m-d", strtotime($followup_date));
          $followup_time = date("H:i", strtotime($followup_time));
          $next_date = date("Y-m-d", strtotime($next_date));
          $next_time = date("H:i", strtotime($next_time));
          if (strtotime($followup_date . " " . $followup_time) > strtotime($next_date . " " . $next_time)) {
          $error = TRUE;
          //$error_msg = $error_msg . "<br/>- Select proper Monitor.";
          $error_msg = getError($error_msg, "Followup Date-Time should be greater than current Date-Time.");
          }
         */
    }
//    else{
//        $status = TRUE;
//    }

    return array($status, $puja_date . " " . $puja_time, $error_msg);
}

function validateTime_old($time, $format = 'H:i:s') {
    //$validate = preg_match("/(1[012]|0[0-9]):([0-5][0-9])(AM|PM)/i", $time);
    //$validate = preg_match("(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)", $time); 
    if (strlen(trim($puja_date)) > 0) {
        $p1 = '/^(0?\d|1\d|2[0-3]):[0-5]\d:[0-5]\d$/';
        $p2 = '/^(0?\d|1[0-2]):[0-5]\d\s(am|pm)$/i';
        $status = preg_match($p1, $time) || preg_match($p2, $time);
        if ($status)
            $time = date('H:i:s', strtotime($time));
        else
            $msg = "incorrect time format.";
    }else {
        $status = 0;
        $msg = "Enter Proper Time.";
    }

    //return $res;

    return array($status, $time, $msg);
}

function getActiveTab($tab, $value) {
    if ($tab == $value)
        echo 'active';
}

function getActiveBody($tab, $value) {
    if ($tab == $value)
        echo 'active in';
}

function searchIdInArray($id, $array, $value) {
    $status = 0;
    $msg = "";
    $data = "";
    if (!strlen($value) > 0) {
        $msg = "No Value passed to Search";
        return array($status, $data, $msg);
    }

    foreach ($array as $key => $val) {
        if ($val[$value] === $id) {
            //return $key;
            $data = $key;
            $status = 1;
            $msg = "Record Found.";
            return array($status, $data, $msg);
        }
    }

    $msg = "No Record Found";
    return array($status, $data, $msg);
}

function setFromToDate($start_date, $end_date = "") {
    $status = 0;
    $msg = "";

    if (strlen($start_date) < 9) {
        $msg = "Please Select Proper Date For Followup.";
        return array($status, $from_date, $to_date, $msg);
    }

    $from_date = $start_date . " 00:00:00";   // 2015-11-24 00:00:00 , 2015-11-24 23:59:59
    if (strlen($end_date) > 9) {
        $to_date = $end_date . " 23:59:59";
        //$where_clause = " id = $edit_id And ";
    } else {
        $to_date = $start_date . " 23:59:59";
    }

    $status = 1;
    return array($status, $from_date, $to_date, $msg);
}

function getDataByQuery($query) {
    global $con;

    $status = 0;
    $data = array();
    $sql = $query;
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Result Present";
        return array($status, $id, $msg);
    }
}

function addDataByQuery($query) {
    global $con;

    $status = 0;
    $data = array();
    $msg = "";
    $sql = $query;
    $result = $con->query($sql);

    if ($result) {
        $status = 1; // 1 Means Executed
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
                //$data = $row;
                $id = $row["id"];
                array_push($data, $row);
            }
            $status = 1; // 2 Means Already present
            //$id = 0;
            //$msg = "This Source Is Already Present.";
            return array($status, $id, $data);
        } else {
            $id = $con->insert_id;
            //$msg = "No Result Present";
            return array($status, $id, $msg);
        }
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }
}

/* Enquiry Functions  */

function get_enqMax() {

    global $con;

    $status = 0;

    //$sql = "select max(enq_no) enq_no from tbl_enquiry limit 0,1";
    $sql = "select max((enq_no*0)+enq_no) enq_no from tbl_enquiry";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Max Enquiry No Present";
        return array($status, $id, $msg);
    }
}

function add_enquiry($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = "mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = "add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    logmsg("From add_enquiry -> " . $sql);
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Enquiry $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Enquiry: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }
    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getEnqData($edit_id) {

    global $con;

    $data = array();
    $status = 0;
    $where_clause = "";

    /*
      if (!($edit_id > 0)) {
      $msg = "Please Select Proper Enquiry.";
      return array($status, $id, $msg);
      }
     */

    if ($edit_id > 0) {
        $where_clause = " id = $edit_id And ";
    }

    $sql = "select * from tbl_enquiry where $where_clause status = 0 order by enq_no desc";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function searchEnqNo($enq_no) {

    global $con;

    $status = 0;
    $id = 0;


    $sql = "select * from tbl_enquiry where enq_no = '$enq_no'";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $id = $row["id"];
        }
        $status = 1; // 1 Means Already present
        //$id = 0;
        $msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

/* Enquiry Functions End */

/* Enquiry Puja Functions  */

function getEnqPujaData($edit_id = 0, $enq_id = 0) {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if (($edit_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause = " id = $edit_id And ";
    }
    if (($enq_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause .= " enq_id = $enq_id And ";
    }

    $sql = "select * from tbl_enq_puja where $where_clause status = 0 order by puja_date";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

/* Enquiry Puja Functions End */


/* Enquiry Quotation Functions  */

function addEnqQuotation($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = ", mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = ", add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Enquiry Quotation $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Enquiry Quotation: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }
    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getEnqQuotation($edit_id = 0, $enq_id = 0) {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if (($edit_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause = " id = $edit_id And ";
    }
    if (($enq_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause .= " enq_id = $enq_id And ";
    }

    $sql = "select * from tbl_enq_quotation where $where_clause status = 0 order by id desc";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Quotation Present";
        return array($status, $id, $msg);
    }
}

/* Enquiry Quotation Functions End */

function getDataFromTable($tbl_name) {

    global $con;

    $status = 0;
    $data = array();


    $sql = "select * from $tbl_name where status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
            //print_r($row);
            //$data = $row;
        }
        //print_r($data);
    } else {
        echo $msg = "Error : getting data from $tbl_name: (" . $con->errno . ") " . $con->error;
    }

    return $data;
}

function add_puja($puja_name) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";

    $sql = "select * from tbl_pooja_master where pooja_name = '" . $puja_name . "'";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $id = $row["id"];
        }
        $status = 2; // 2 Means Already present
        //$id = 0;
        $msg = "This Puja Is Already Present.";
        return array($status, $id, $msg);
    }

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name
            . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
    $result_add = $con->query($sql);
    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Puja Added Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error Adding Puja: (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getPujaData($puja_id) {

    global $con;

    $status = 0;
    $data = array();

    $where_clause = "";
    if ($puja_id > 0) {
        //   $msg = "Please Select Proper puja.";
        //    return array($status, $id, $msg);
        $where_clause = " id = $edit_id and ";
    }

    $sql = "select * from tbl_pooja_master where $where_clause status = 0 order by pooja_name";
    $result = $con->query($sql);

    if (!$result) {
        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in <br/>Query : $sql<br/>Error : (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }
    //echo "<br> query = $sql, Count = ".$result->num_rows;
    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Puja Present";
        return array($status, $id, $msg);
    }
}

function add_source($source_name) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";

    $sql = "select * from tbl_source_master where source_name = '" . $source_name . "'";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $id = $row["id"];
        }
        $status = 2; // 2 Means Already present
        //$id = 0;
        $msg = "This Source Is Already Present.";
        return array($status, $id, $msg);
    }

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    echo $sql = "insert into tbl_source_master set source_name = '" . $source_name
    . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
    $result_add = $con->query($sql);
    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Source Added Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error Adding Source: (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function find_Or_Add($action_tag, $find_sql, $add_sql) {


    global $con;


    $status = 0;
    $id = 0;
    $msg = "";



    // $sql = "select * from $tbl_name where medium_name = '" . $name_val . "'";
    $result = $con->query($find_sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $id = $row["id"];
        }
        $status = 2; // 2 Means Already present
        //$id = 0;
        $msg = "This $action_tag Is Already Present.";
        return array($status, $id, $msg);
    }

//    $add_date = getTimestamp();
//    $add_user = $_SESSION['login_id'];
    //$sql = "insert into $tbl_name set medium_name = '" . $medium_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
    $result_add = $con->query($add_sql);
    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "$action_tag Added Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error Adding $action_tag: (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }
}

function add_medium($medium_name) {

    $tbl_name = "tbl_medium_master";
    $name_val = $medium_name;
    $action_tag = "Medium";

    $find_sql = "select * from $tbl_name where medium_name = '" . $name_val . "'";

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];

    $add_sql = "insert into $tbl_name set medium_name = '" . $medium_name
            . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    list($status, $id, $msg) = find_Or_Add($action_tag, $find_sql, $add_sql);
    return array($status, $id, $msg);
    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

/* User Functions  */

function searchCustByAttrib($action_tag, $txt_type, $tbl_name, $attrib_name, $search_val) {

    // Param Details :- $txt_type [1-number,2-text] , 
    global $con;

    $status = 1;
    $id = 0;
    //$search_val = "";

    if (!(strlen(trim($search_val)) > 0)) {
        $status = 0;
        $msg = "Please Enter Proper $action_tag.";
        return array($status, $id, $msg);
    }

    //$search_val = 
    if ($txt_type == 2) {
        $search_val = "'$search_val'";
    }

    $sql = "select * from $tbl_name where $attrib_name = $search_val";
    $result = $con->query($sql);

    if ($result) {
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $id = $row["id"];
            }
            $status = 2; // 2 Means Already present
            //$id = 0;
            $msg = "This $action_tag Is Already Present.";
            return array($status, $id, $msg);
        }
    } else {
        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in <br/>Query : $sql<br/>Error : (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }

    return array($status, $id, $msg);
}

function addUser($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = ",mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = ",add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "User $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action User: (" . $con->errno . ") " . $con->error;
        $msg = $msg . "<br/>Query :- $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getUserData($edit_id) {

    global $con;

    $status = 0;

    if (!($edit_id > 0)) {
        $msg = "Please Select Proper Enquiry.";
        return array($status, $id, $msg);
    }

    $sql = "select * from tbl_customer where id = $edit_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No User Present";
        return array($status, $id, $msg);
    }
}

function getSearchUserData($sql) {

    global $con;

    $status = 0;
    //$sql = "select * from tbl_customer where id = $edit_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        $data = array();
        // output data of each row
        while ($row = $result->fetch_assoc()) {

            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No User Present";
        return array($status, $id, $msg);
    }
}

function getUserEnq($cust_id) {

    global $con;

    $status = 0;
    $sql = "select * from tbl_enquiry where cust_id = $cust_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        $data = array();
        // output data of each row
        while ($row = $result->fetch_assoc()) {

            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present.";
        return array($status, $id, $msg);
    }
}

/* Folloup Function */

function getPanditData($edit_id) {

    global $con;

    $status = 0;
    $data = array();

    if ($edit_id > 0) {
        $where_clause = " id = $edit_id and ";
    }

    $sql = "select * from tbl_pandit where $where_clause  status = 0 ORDER BY `tbl_pandit`.`first_name` ASC";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 1 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Pandit Present";
        return array($status, $id, $msg);
    }

    /*
      if (!($edit_id > 0)) {
      $msg = "Please Select Proper Enquiry.";
      return array($status, $id, $msg);
      }

     */
}

function listPanditData($edit_id) {

    global $con;

    $status = 0;
    $data = array();

    if ($edit_id > 0) {
        $where_clause = "where id = $edit_id ";
    }

    $sql = "select * from tbl_pandit  $where_clause order by status, first_name ";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 1 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Pandit Present";
        return array($status, $id, $msg);
    }

    /*
      if (!($edit_id > 0)) {
      $msg = "Please Select Proper Enquiry.";
      return array($status, $id, $msg);
      }

     */
}

/* Folloup Function End */



/* Folloup Function */

function addFollowup($data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];


    $action = "Added";
    $query_start = "insert into ";
    $query_end = ",add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";



    $sql = $query_start . $data_sql . $query_end;

    logmsg("From addFollowup -> " . $sql);
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        //if($id > 0 )
        $msg = "Followup $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Followup: (" . $con->errno . ") " . $con->error;
        $msg = $msg . "<br/>Query :- $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getFollowup($enq_id) {

    global $con;

    $status = 0;
    $data = array();

    if (!($enq_id > 0)) {
        $msg = "No such Enquiry Present.";
        return array($status, $id, $msg);
    }

    //echo $sql = "select * from tbl_enq_followup where enq_id = $enq_id and status = 0";

    $sql = "select follow.*,admin.name as admin_name,admin.thumb_img,e_status.*,monitor.* from tbl_enq_followup follow 
inner join tbl_admin admin 
inner join tbl_status_master e_status 
inner join tbl_monitors_master monitor

on ( follow.enq_id = $enq_id and follow.status = 0 )
and (follow.add_user = admin.id )
and (follow.enq_status = e_status.id )
and (follow.monitor_by = monitor.id ) order by follow.id desc ";

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Followup Present.";
        return array($status, $id, $msg);
    }
}

function getFollowupByDay($date = "", $state = 0) {

    global $con;

    $status = 0;
    $search_date_clause = "";
    $data = array();

    if (!strlen($date) > 0) {
        $date = date('Y-m-d', time());
        //$msg = "No such Enquiry Present.";
        //return array($status, $id, $msg);
    }


    //echo $sql = "select * from tbl_enq_followup where enq_id = $enq_id and status = 0";

    /* $sql = "select follow.*,admin.name as admin_name,admin.thumb_img,e_status.*,monitor.* from tbl_enq_followup follow 
      inner join tbl_admin admin
      inner join tbl_status_master e_status
      inner join tbl_monitors_master monitor

      on ( follow.next_date = $date and follow.status = 0 )
      and (follow.add_user = admin.id )
      and (follow.enq_status = e_status.id )
      and (follow.monitor_by = monitor.id ) order by follow.id desc ";
     */
    if ($state == 0) {
        $search_date_clause = "follow.next_date = '$date'";
    }

    if ($state == 1) {
        $search_date_clause = "follow.add_enq_date = '$date'";
    }


    $sql = "select follow.enq_id,follow.next_date,follow.next_time,enq.* from tbl_enq_followup follow 
inner join tbl_enquiry enq
on $search_date_clause and follow.enq_id=enq.id and follow.status=0 group by follow.enq_id";



    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Followup Present.";
        return array($status, $id, $msg);
    }
}

function getFollowupByDate($start_date, $end_date = "") {

    global $con;

    $data = array();
    $status = 0;
    $where_clause = "";


    if (strlen($start_date) < 9) {
        $msg = "Please Select Proper Date For Followup.";
        return array($status, $id, $msg);
    }




    $from_date = $start_date . " 00:00:00";   // 2015-11-24 00:00:00 , 2015-11-24 23:59:59
    if (strlen($end_date) > 9) {
        $to_date = $end_date . " 23:59:59";
        //$where_clause = " id = $edit_id And ";
    } else {
        $to_date = $start_date . " 23:59:59";
    }

    /*
      if (strlen($start_date) > 0 ) {

      list($status, $db_date, $msg) = validateDateTime($puja_date_1, $puja_time_1);
      if (!$status) {
      $error = TRUE;
      $msg = "Enter proper Date Time for puja";
      $error_msg = getError($error_msg, $msg);
      } else {
      $puja_date_1 = $db_date;
      }
      }
     */



    //echo $sql = "select * from tbl_enquiry where  followup_date between ($from_date , $to_date) and ( enq_status != 5 or enq_status != 8 ) And  status = 0 order by followup_date ";
    //echo $sql = "SELECT f.*,e.* FROM `tbl_enq_followup` f inner join tbl_enquiry e on f.next_date like '%$start_date%'  and f.enq_id=e.id";  
    //$sql = "SELECT e.*,f.*,e.enq_status e_status FROM `tbl_enq_followup` f inner join tbl_enquiry e on f.next_date BETWEEN '$from_date' and '$to_date' and f.enq_id=e.id and (e.enq_status <> 5 and e.enq_status <> 8)  group by (e.id) order by f.next_date desc, f.id desc";
    //$sql = "SELECT e.*,f.*,e.enq_status e_status FROM `tbl_enq_followup` f inner join tbl_enquiry e on f.next_date BETWEEN '$from_date' and '$to_date' and f.enq_id=e.id and (e.enq_status <> 5 and e.enq_status <> 8)  order by f.next_date desc, f.id desc";

    /* $sql = "SELECT e.enq_no,e.cust_id,e.enq_date,e.quotation_id,e.enq_status e_status,e.followup_date, f.enq_id,f.add_enq_date,f.next_date,f.enq_status
      FROM tbl_enquiry e  inner join  `tbl_enq_followup` f
      on (f.next_date BETWEEN '$from_date' and '$to_date' or e.enq_date BETWEEN '$from_date' and '$to_date' )
      and f.enq_id=e.id and (e.enq_status <> 5 and e.enq_status <> 8)
      order by f.next_date , f.id";
     */
    $sql = "SELECT e.enq_no,e.cust_id,e.enq_date,e.quotation_id,e.enq_status e_status,e.followup_date,e.add_user,
            f.enq_id,f.add_enq_date,f.next_date,f.enq_status
            FROM tbl_enquiry e  inner join  `tbl_enq_followup` f 
            on (f.next_date BETWEEN '$from_date' and '$to_date' )
            and f.enq_id=e.id and (e.enq_status <> 5 and e.enq_status <> 8)  
            order by f.next_date , f.id";




    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Followup Present.";
        return array($status, $id, $msg);
    }
}

///  Status Function

function getStatus($type, $edit_id = 0) {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if ($type > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " rec_type = $type and ";
    }
    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause .= " id = $edit_id and ";
    }

    $sql = "select * from tbl_status_master where $where_clause status = 0 order by status_priority ";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row);
        }
        //print_r($data);
    } else {
        $msg = "Error : getting data from tbl_status_master: (" . $con->errno . ") " . $con->error;
    }

    return $data;
}

function getMonitor($type, $edit_id = 0) {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if (strlen($type) > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " $type = 1 and ";
    }
    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause .= " id = $edit_id and ";
    }

    $sql = "select * from tbl_monitors_master where $where_clause status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row);
        }
        //print_r($data);
    } else {
        echo $msg = "Error : getting data from tbl_status_master: (" . $con->errno . ") " . $con->error;
    }

    return $data;
}

/*
  function add_lang($source_name) {

  global $con;


  $status = 0;
  $id = 0;
  $msg = "";

  $sql = "select * from tbl_source_master where source_name = '" . $source_name . "'";
  $result = $con->query($sql);

  if ($result->num_rows > 0) {
  // output data of each row
  while ($row = $result->fetch_assoc()) {
  //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
  $id = $row["id"];
  }
  $status = 2; // 2 Means Already present
  //$id = 0;
  $msg = "This Puja Is Already Present.";
  return array($status, $id, $msg);
  }

  $add_date = getTimestamp();
  $add_user = $_SESSION['login_id'];
  echo $sql = "insert into tbl_source_master set source_name = '" . $source_name
  . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
  $result_add = $con->query($sql);
  if ($result_add) {
  $status = 1; // 1 Means New record added
  $id = $con->insert_id;
  $msg = "Puja Added Successfully";
  return array($status, $id, $msg);
  } else {

  $status = 0; // 0 Means Error
  $id = 0;
  $msg = "Error Adding Puja: (" . $con->errno . ") " . $con->error;
  return array($status, $id, $msg);
  }




  //printf("New Record has id %d.\n", $mysqli->insert_id);
  }

  function add_cast($source_name) {

  global $con;


  $status = 0;
  $id = 0;
  $msg = "";

  $sql = "select * from tbl_source_master where source_name = '" . $source_name . "'";
  $result = $con->query($sql);

  if ($result->num_rows > 0) {
  // output data of each row
  while ($row = $result->fetch_assoc()) {
  //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
  $id = $row["id"];
  }
  $status = 2; // 2 Means Already present
  //$id = 0;
  $msg = "This Puja Is Already Present.";
  return array($status, $id, $msg);
  }

  $add_date = getTimestamp();
  $add_user = $_SESSION['login_id'];
  echo $sql = "insert into tbl_source_master set source_name = '" . $source_name
  . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
  $result_add = $con->query($sql);
  if ($result_add) {
  $status = 1; // 1 Means New record added
  $id = $con->insert_id;
  $msg = "Puja Added Successfully";
  return array($status, $id, $msg);
  } else {

  $status = 0; // 0 Means Error
  $id = 0;
  $msg = "Error Adding Puja: (" . $con->errno . ") " . $con->error;
  return array($status, $id, $msg);
  }




  //printf("New Record has id %d.\n", $mysqli->insert_id);
  }
 */

function getError($error_msg, $msg) {
    //if (strlen(trim($msg)) > 0) {
    //$error_msg . "<br/>- $msg.";
    //}
    return $error_msg . "<br/>- $msg.";
}

function getTimestamp() {
    date_default_timezone_set('IST');
    //$this->timestamp = date('Y-m-d H:i:s');
    return date('Y-m-d H:i:s');
}

/* Feeds Function */

function addFeeds($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = "mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = "add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Feeds $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Enquiry: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getFeeds($edit_id = 0, $page_no = 0) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " And tf.id = $edit_id";
    }
    if ($page_no > 0) {
        //$min = 0;
        $rec = 10;
        $max = $rec * $page_no;
        $min = $max - $rec;

        $limit_clause = " limit $min,$max";
    }

    //$sql = "select * from tbl_feeds where status = 0 $where_clause $limit_clause order by publish_date desc";
    $sql = "select tf.*,tfm.feed_name,tfm.feed_img from tbl_feeds tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id $where_clause  order by tf.publish_date desc $limit_clause";

    logmsg($sql);

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function getFeedType($edit_id = 0, $page_no = 0) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " where id = $edit_id";
    }
    if ($page_no > 0) {
        $min = 0;
        $max = 10;
        $limit_clause = " limit $min,$max";
    }

    $sql = "select * from tbl_feeds_type_master $where_clause $limit_clause";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function getFeedsNotification($edit_id = 0, $page_no = 0) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " And id = $edit_id";
    }
    if ($page_no > 0) {
        //$min = 0;
        $rec = 10;
        $max = $rec * $page_no;
        $min = $max - $rec;

        $limit_clause = " limit $min,$max";
    }

    $sql = "select * from tbl_feeds_notification where status = 0 $where_clause $limit_clause order by publish_date desc";
    //echo $sql = "select tf.*,tfm.feed_name,tfm.feed_img from tbl_feeds_notification tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id $where_clause  order by tf.publish_date desc $limit_clause";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function App_getRegisterUser($user_id, $user_email, $user_name = "", $user_phone = "") {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    //$limit_clause = "";

    if ($user_id > 0) {
        $where_clause = " id = $user_id and ";
        //$data_clause = "sum(like_status) feed_likes";
    }

    if (strlen($user_email) > 0) {
        if (validateEmail($user_email)) {
            $msg = "Error : invalid Email [$user_email]";
            logmsg("App_getRegisterUser -> " . $msg);
            return array($status, 0, $msg);
        } else {
            $where_clause .= "email_1 = '$user_email' and ";
            //$data_clause = "*";
        }
    }


    $sql = "SELECT * FROM `tbl_customer_app` WHERE $where_clause status = 0";
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "Error : Error in getting Register User : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_getRegisterUser ->  " . $msg);
        return array($status, $id, $msg);
    }
}

function send_feed_notification($feedId, $message) {


    list($status, $id, $data) = App_getRegisterUser();
    //print_r($data);
    $reg_gcm_arr = array();
    $reg_gcm_pointer = 0;

    for ($i = 0; $i < count($data); $i++) {
        //echo "<br/>email = " . $data[$i]['email_1'];
        $gcm_code_str = $data[$i]['gcm_code'];
        $gcm_code_arr = explode(',', $gcm_code_str);
        foreach ($gcm_code_arr as $key => $value) {
            //echo "<br/>gcm_code = key[$key], value[$value]";
            $reg_gcm_arr[$reg_gcm_pointer] = $value;
            $reg_gcm_pointer++;
        }
    }
    $reg_gcm_arr = array_unique($reg_gcm_arr);
    //print_r($reg_gcm_arr);

    /*
      $reg_gcm_arr = array(
      "0" => "APA91bH7AAhk5MvsR0fNb9Z0OZSe-RjeSUSrVsBg7B6kEUjl880BMrOIntAp5gRdWXQWEwRxBtJBJ1RdUvWLdcUi2HxBLHzFfhiT_bcK2rldZt_KyntSKaerAiAgx0DXRVVvIxa_htwJ",
      "1" => "APA91bFh-gB9tp2fxa6OopxwSnCvsIbmJ__sdi-bOeEHElxuJzKLHRabHAsLM1A2L28CMGNrt_M2FVZoZy37VODg-SpAFYHCOb9chP1e0_oCZjnOkDczfYCKvbqYolKwnUO05E0vRDzu",
      "2" => "APA91bFDLREDr30fryBii_7U9sqdC4iOzo4NNBOT7zn5Wlw3GxDP7uzduI3VUQW_1OAJpDoI3SdSZesASlIViMl6W2D6nvnSkWUTqt3QyWhBLy2UZUpfzmf28caUa69BqQGs9kPimh9L",

      );
     */
    foreach ($reg_gcm_arr as $key => $value) {
        list($status, $id, $msg) = send_notification($value, $message);
    }

    //$value = 'APA91bFh-gB9tp2fxa6OopxwSnCvsIbmJ__sdi-bOeEHElxuJzKLHRabHAsLM1A2L28CMGNrt_M2FVZoZy37VODg-SpAFYHCOb9chP1e0_oCZjnOkDczfYCKvbqYolKwnUO05E0vRDzu';
    //list($status, $id, $msg) = send_notification($value, $message);

    $msg = "Notification Send.";
    return array($status, $id, $msg);
}

function send_feed_notification_Test($feedId, $message) {

    /*
      list($status, $id, $data) = App_getRegisterUser();
      //print_r($data);
      $reg_gcm_arr = array();
      $reg_gcm_pointer = 0;

      for ($i = 0; $i < count($data); $i++) {
      //echo "<br/>email = " . $data[$i]['email_1'];
      $gcm_code_str = $data[$i]['gcm_code'];
      $gcm_code_arr = explode(',', $gcm_code_str);
      foreach ($gcm_code_arr as $key => $value) {
      //echo "<br/>gcm_code = key[$key], value[$value]";
      $reg_gcm_arr[$reg_gcm_pointer] = $value;
      $reg_gcm_pointer++;
      }
      }
     */


    $reg_gcm_arr = array(
        "0" => "APA91bF5Rf9f8w2RYm5fqDPcen0DX-NXDpEKW1P0r_9A_BijofcPjc2IBoRjiy0MRKY0UKAdfsVlroYdUrG10Ys20g8_g47oBbiT3JuCIiUEyATdAXibH3PO1n_ePX2YZ460mYA8bwS0",
        "1" => "APA91bFh-gB9tp2fxa6OopxwSnCvsIbmJ__sdi-bOeEHElxuJzKLHRabHAsLM1A2L28CMGNrt_M2FVZoZy37VODg-SpAFYHCOb9chP1e0_oCZjnOkDczfYCKvbqYolKwnUO05E0vRDzu",
        "2" => "APA91bFDLREDr30fryBii_7U9sqdC4iOzo4NNBOT7zn5Wlw3GxDP7uzduI3VUQW_1OAJpDoI3SdSZesASlIViMl6W2D6nvnSkWUTqt3QyWhBLy2UZUpfzmf28caUa69BqQGs9kPimh9L",
    );


    $reg_gcm_arr = array_unique($reg_gcm_arr);
    //print_r($reg_gcm_arr);
    foreach ($reg_gcm_arr as $key => $value) {
        list($status, $id, $msg) = send_notification($value, $message);
    }
    $msg = "Notification Send.";
    return array($status, $id, $msg);
}

function send_notification($regId, $message) {

    $_GET["regId"] = $regId; //"APA91bGk8y4tgeTiNWByFDe64lXnVuYHBw3Wu3cBE6nggyf282ZGPrHUMhb5L9YvlSPNyYcZAo8ZBcEnXrwI4GmvXGtuHHzrxsRkEo2NK_cTPw6VuzoDCVh1Wjmbsp11H8Z3Y4DSNKVpwMFw_zqB9w0wq6NKvU00pA";

    $_GET["message"] = $message; //"Hi This is Notification from Ninad";

    if (isset($_GET["regId"]) && isset($_GET["message"])) {
        $regId = $_GET["regId"];
        $message = $_GET["message"];

        include_once 'GCM.php';

        $gcm = new GCM();

        $registatoin_ids = array($regId);
        $message = array("note" => $message);

        $result = $gcm->send_notification($registatoin_ids, $message);

        //echo $result;
        $gcm_code_arr = explode(',', $result);
        $status = $gcm_code_arr[1];
        $msg = $gcm_code_arr[4];
        $id = $gcm_code_arr[0];

        $log_msg = "Log : Reg_id = $regId, Message = " . print_r($result, TRUE) . ", Status = $status, Msg = $msg, Id = $id";
        echo "<br/>" . $log_msg = "Log : Reg_id = $regId, Message = " . print_r($result, TRUE) . "";
        logmsg("send_notification ->  " . $log_msg);

        return array($status, $id, $msg);
    } else {
        return array(0, 0, "Found Some empty Param");
    }
}

/* Feeds Function End */


/* Order Temp Function Start */

function searchOrderTmpEnqBy($key, $value) {

    global $con;

    $status = 0;
    $id = 0;


    $sql = "select * from tbl_order_tmp where $key = $value and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $id = $row["id"];
        }
        $status = 1; // 1 Means Already present
        //$id = 0;
        $msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function addOrderTmp($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = ", mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = ", add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Details $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Details: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getOrderTmpData($edit_id = 0) {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if (($edit_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause = " id = $edit_id And ";
    }

    $sql = "select * from tbl_order_tmp where $where_clause status = 0 order by enq_date desc";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function getOrderTmp_CollectedBy() {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";



    echo $sql = "select DISTINCT collect_by from tbl_order_puja_tmp where status = 0 union select DISTINCT collect_by from tbl_order_tmp where status = 0 ";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function addOrderPujaTmp($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = ", mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = ", add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Puja Details $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Details: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getOrderPujaTmpData($edit_id = 0, $enq_id = 0) {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if (($edit_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause = " id = $edit_id And ";
    }
    if (($enq_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause .= " order_id = $enq_id And ";
    }

    $sql = "select * from tbl_order_puja_tmp where $where_clause status = 0 order by puja_date desc";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function getOrderPujaTmpByDate($edit_id = 0, $enq_id = 0, $from_date = '', $to_date = '') {

    global $con;

    $status = 0;
    $data = array();
    $where_clause = "";

    if (($edit_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause = " ord_puja.id = $edit_id And ";
    }
    if (($enq_id > 0)) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        $where_clause .= " ord_puja.order_id = $enq_id And ";
    }

    if (strlen($from_date) > 0) {
        //$msg = "Please Select Proper Order.";
        //return array($status, $id, $msg);
        list($status, $from_date, $to_date, $msg) = setFromToDate($from_date, $to_date);
        $where_clause .= " ord_puja.puja_date BETWEEN '$from_date' and '$to_date' And ";
    }


    /* echo $sql = "select ord_puja.*,
      ord.*,
      cust.*
      from tbl_order_puja_tmp ord_puja
      inner join tbl_order_tmp ord
      inner join tbl_customer cust
      on $where_clause
      ord.id = ord_puja.order_id
      and ord.cust_id = cust.id
      order by ord_puja.puja_date desc"; */

    $sql = "select ord_puja.*,
        ord.id ord_id,cust_id      
        from tbl_order_puja_tmp ord_puja 
        inner join tbl_order_tmp ord            
        on $where_clause 
                ord.id = ord_puja.order_id             
            order by ord_puja.puja_date desc";


    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Puja Present";
        return array($status, $id, $msg);
    }
}

function validateDateTime($enq_date, $enq_time = "") {
    $status = 1;
    $msg = "Enter proper Enquiry ";
    if (!(strlen(trim($enq_date)) > 0) || !validateDate($enq_date, $format = 'DD-MM-YYYY')) {
        $status = 0;
        //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Date";
        $msg .= "Date [" . validateDate($enq_date, $format = 'DD-MM-YYYY') . "] ";
    }
    $enq_date = date("Y-m-d", strtotime($enq_date));

    if (strlen(trim($enq_time)) > 0) {
        if (!validateTime($enq_time, $format = 'h:i a')) {
            $status = 0;
            //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Date";
            $msg .= "Time [" . validateTime($enq_time, $format = 'h:i a') . "]";
        }
        $enq_time = date("H:i", strtotime($enq_time));
    } else {
        $enq_time = "00:00:00";
    }

    //$enq_date = $enq_date . " " . $enq_time;



    $enq_date = $enq_date . " " . $enq_time;

    return array($status, $enq_date, $msg);
}

function validateDbDateTime($date) {
    $tmp_date = explode(" ", $date);
    if ($tmp_date[0] != "0000-00-00") {
        $order_date = date("d-m-Y", strtotime($tmp_date[0]));
        //$order_time = date("h:i a", strtotime($date));
    } else {
        $order_date = "";
        //$order_time = "";
    }

    if ($tmp_date[1] != "00:00:00") {
        $order_time = date("h:i a", strtotime($tmp_date[1]));
        //$order_time = date("h:i a", strtotime($date));
    } else {
        $order_time = "";
        //$order_time = "";
    }

    return array($order_date, $order_time);
}

/* Order Temp Function End */
?>