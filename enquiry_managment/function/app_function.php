<?php

//include_once '../config/auth.php';

function logmsg($msg) {

    $log_time = date('Y-m-d H:i:s');

    $log_msg = "<br/>" . $log_time . " = " . $msg;

    $myFile = "app_log.html";
    $fh = fopen($myFile, 'a') or die("can't open file");
    $stringData = $log_msg;
    fwrite($fh, $stringData);
    fclose($fh);
}

function validateDate($date, $format = 'DD-MM-YYYY') {
    switch ($format) {
        case 'YYYY/MM/DD':
        case 'YYYY-MM-DD':
            list( $y, $m, $d ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'YYYY/DD/MM':
        case 'YYYY-DD-MM':
            list( $y, $d, $m ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'DD-MM-YYYY':
        case 'DD/MM/YYYY':
            list( $d, $m, $y ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'MM-DD-YYYY':
        case 'MM/DD/YYYY':
            list( $m, $d, $y ) = preg_split('/[-\.\/ ]/', $date);
            break;

        case 'YYYYMMDD':
            $y = substr($date, 0, 4);
            $m = substr($date, 4, 2);
            $d = substr($date, 6, 2);
            break;

        case 'YYYYDDMM':
            $y = substr($date, 0, 4);
            $d = substr($date, 4, 2);
            $m = substr($date, 6, 2);
            break;

        default:
            throw new Exception("Invalid Date Format");
    }
    return checkdate($m, $d, $y);
}

function validateTime($date, $format = 'h:i a') {
    $date = strtolower($date);
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function validateEmail($email) {
    $error = 0;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = 1;
        logmsg("validateEmail -> Error invalid Email_id ($email)");
        //$msg = "Email id is Invalid format.";
        //$error_msg = getError($error_msg, $msg);
    }
    //echo $error;
    return $error;
}

function getDataByQuery($query) {
    global $con;

    $status = 0;
    $data = array();
    $sql = $query;
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Result Present";
        return array($status, $id, $msg);
    }
}

function addDataByQuery($query) {
    global $con;

    $status = 0;
    $data = array();
    $msg = "";
    $sql = $query;
    $result = $con->query($sql);

    if ($result) {
        $status = 1; // 1 Means Executed
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
                //$data = $row;
                $id = $row["id"];
                array_push($data, $row);
            }
            $status = 1; // 2 Means Already present
            //$id = 0;
            //$msg = "This Source Is Already Present.";
            return array($status, $id, $data);
        } else {
            $id = $con->insert_id;
            //$msg = "No Result Present";
            return array($status, $id, $msg);
        }
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }
}




function validatePujaData($puja_id, $puja_date, $puja_time) {
    $status = FALSE;
    //$msg = "";
    $error_msg = "Ninad";


    if ($puja_id > 0) {
        $error_msg = "Enter proper puja ";
        if (!(strlen(trim($puja_date)) > 0) || !validateDate($puja_date, $format = 'DD-MM-YYYY')) {
            $status = TRUE;
            //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Date";
            //$error_msg = getError($error_msg, "Enter proper puja Date.");
            $error_msg .= "Date, ";
        } else {
            $puja_date = date("Y-m-d", strtotime($puja_date));
        }
        //list($status, $change_time, $msg)=validateTime($puja_time_1,12,24);
        if (!(strlen(trim($puja_time)) > 0) || !validateTime($puja_time, $format = 'h:i a')) {
            $status = TRUE;
            //$error_msg = getError($error_msg, "Enter proper puja Time.");
            $error_msg .= "Time.";
        } else {
            $puja_time = date("H:i", strtotime($puja_time));
        }
    }


    return array($status, $puja_date . " " . $puja_time, $error_msg);
}

function add_enquiry($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = "mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = "add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Enquiry $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Enquiry: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getEnqData($edit_id) {

    global $con;

    $status = 0;

    if (!($edit_id > 0)) {
        $msg = "Please Select Proper Enquiry.";
        return array($status, $id, $msg);
    }

    $sql = "select * from tbl_enquiry where id = $edit_id";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function searchEnqNo($enq_no) {

    global $con;

    $status = 0;
    $id = 0;


    $sql = "select * from tbl_enquiry where enq_no = '$enq_no'";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $id = $row["id"];
        }
        $status = 1; // 1 Means Already present
        //$id = 0;
        $msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function getDataFromTable($tbl_name) {

    global $con;

    $status = 0;
    $data = array();


    $sql = "select * from $tbl_name where status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
            //print_r($row);
            //$data = $row;
        }
        //print_r($data);
    } else {
        echo $msg = "Error : getting data from $tbl_name: (" . $con->errno . ") " . $con->error;
    }

    return $data;
}

function add_puja($puja_name) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";

    $sql = "select * from tbl_pooja_master where pooja_name = '" . $puja_name . "'";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $id = $row["id"];
        }
        $status = 2; // 2 Means Already present
        //$id = 0;
        $msg = "This Puja Is Already Present.";
        return array($status, $id, $msg);
    }

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name
            . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
    $result_add = $con->query($sql);
    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Puja Added Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error Adding Puja: (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getPujaData($puja_id) {

    global $con;

    $status = 0;

    if (!($puja_id > 0)) {
        $msg = "Please Select Proper puja.";
        return array($status, $id, $msg);
    }

    $sql = "select * from tbl_pooja_master where id = $puja_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Puja Present";
        return array($status, $id, $msg);
    }
}

function add_source($source_name) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";

    $sql = "select * from tbl_source_master where source_name = '" . $source_name . "'";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $id = $row["id"];
        }
        $status = 2; // 2 Means Already present
        //$id = 0;
        $msg = "This Source Is Already Present.";
        return array($status, $id, $msg);
    }

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    echo $sql = "insert into tbl_source_master set source_name = '" . $source_name
    . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
    $result_add = $con->query($sql);
    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Source Added Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error Adding Source: (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function find_Or_Add($action_tag, $find_sql, $add_sql) {


    global $con;


    $status = 0;
    $id = 0;
    $msg = "";



    // $sql = "select * from $tbl_name where medium_name = '" . $name_val . "'";
    $result = $con->query($find_sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            $id = $row["id"];
        }
        $status = 2; // 2 Means Already present
        //$id = 0;
        $msg = "This $action_tag Is Already Present.";
        return array($status, $id, $msg);
    }

//    $add_date = getTimestamp();
//    $add_user = $_SESSION['login_id'];
    //$sql = "insert into $tbl_name set medium_name = '" . $medium_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
    $result_add = $con->query($add_sql);
    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "$action_tag Added Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error Adding $action_tag: (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }
}

function add_medium($medium_name) {

    $tbl_name = "tbl_medium_master";
    $name_val = $medium_name;
    $action_tag = "Medium";

    $find_sql = "select * from $tbl_name where medium_name = '" . $name_val . "'";

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];

    $add_sql = "insert into $tbl_name set medium_name = '" . $medium_name
            . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    list($status, $id, $msg) = find_Or_Add($action_tag, $find_sql, $add_sql);
    return array($status, $id, $msg);
    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

/* User Functions  */

function searchCustByAttrib($action_tag, $txt_type, $tbl_name, $attrib_name, $search_val) {

    // Param Details :- $txt_type [1-number,2-text] , 
    global $con;

    $status = 1;
    $id = 0;
    //$search_val = "";

    if (!(strlen(trim($search_val)) > 0)) {
        $status = 0;
        $msg = "Please Enter Proper $action_tag.";
        return array($status, $id, $msg);
    }

    //$search_val = 
    if ($txt_type == 2) {
        $search_val = "'$search_val'";
    }

    $sql = "select * from $tbl_name where $attrib_name = $search_val";
    $result = $con->query($sql);

    if ($result) {
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $id = $row["id"];
            }
            $status = 2; // 2 Means Already present
            //$id = 0;
            $msg = "This $action_tag Is Already Present.";
            return array($status, $id, $msg);
        }
    } else {
        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in <br/>Query : $sql<br/>Error : (" . $con->errno . ") " . $con->error;
        return array($status, $id, $msg);
    }

    return array($status, $id, $msg);
}

function addUser($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = ",mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = ",add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "User $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action User: (" . $con->errno . ") " . $con->error;
        $msg = $msg . "<br/>Query :- $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getUserData($edit_id) {

    global $con;

    $status = 0;

    if (!($edit_id > 0)) {
        $msg = "Please Select Proper Enquiry.";
        return array($status, $id, $msg);
    }

    $sql = "select * from tbl_customer where id = $edit_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No User Present";
        return array($status, $id, $msg);
    }
}

function getSearchUserData($sql) {

    global $con;

    $status = 0;
    //$sql = "select * from tbl_customer where id = $edit_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        $data = array();
        // output data of each row
        while ($row = $result->fetch_assoc()) {

            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No User Present";
        return array($status, $id, $msg);
    }
}

function getUserEnq($cust_id) {

    global $con;

    $status = 0;
    $sql = "select * from tbl_enquiry where cust_id = $cust_id and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        $data = array();
        // output data of each row
        while ($row = $result->fetch_assoc()) {

            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present.";
        return array($status, $id, $msg);
    }
}

/* Folloup Function */

function addFollowup($data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];


    $action = "Added";
    $query_start = "insert into ";
    $query_end = ",add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";



    $sql = $query_start . $data_sql . $query_end;


    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        //if($id > 0 )
        $msg = "Followup $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Followup: (" . $con->errno . ") " . $con->error;
        $msg = $msg . "<br/>Query :- $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getFollowup($enq_id) {

    global $con;

    $status = 0;
    $data = array();

    if (!($enq_id > 0)) {
        $msg = "No such Enquiry Present.";
        return array($status, $id, $msg);
    }

    //echo $sql = "select * from tbl_enq_followup where enq_id = $enq_id and status = 0";

    $sql = "select follow.*,admin.name as admin_name,admin.thumb_img,e_status.*,monitor.* from tbl_enq_followup follow 
inner join tbl_admin admin 
inner join tbl_status_master e_status 
inner join tbl_monitors_master monitor

on ( follow.enq_id = $enq_id and follow.status = 0 )
and (follow.add_user = admin.id )
and (follow.enq_status = e_status.id )
and (follow.monitor_by = monitor.id ) order by follow.id desc ";

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Followup Present.";
        return array($status, $id, $msg);
    }
}

function getFollowupByDay($date = "", $state = 0) {

    global $con;

    $status = 0;
    $search_date_clause = "";
    $data = array();

    if (!strlen($date) > 0) {
        $date = date('Y-m-d', time());
        //$msg = "No such Enquiry Present.";
        //return array($status, $id, $msg);
    }


    //echo $sql = "select * from tbl_enq_followup where enq_id = $enq_id and status = 0";

    /* $sql = "select follow.*,admin.name as admin_name,admin.thumb_img,e_status.*,monitor.* from tbl_enq_followup follow 
      inner join tbl_admin admin
      inner join tbl_status_master e_status
      inner join tbl_monitors_master monitor

      on ( follow.next_date = $date and follow.status = 0 )
      and (follow.add_user = admin.id )
      and (follow.enq_status = e_status.id )
      and (follow.monitor_by = monitor.id ) order by follow.id desc ";
     */
    if ($state == 0) {
        $search_date_clause = "follow.next_date = '$date'";
    }

    if ($state == 1) {
        $search_date_clause = "follow.add_enq_date = '$date'";
    }


    $sql = "select follow.enq_id,follow.next_date,follow.next_time,enq.* from tbl_enq_followup follow 
inner join tbl_enquiry enq
on $search_date_clause and follow.enq_id=enq.id and follow.status=0 group by follow.enq_id";



    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row); //$data = $row;
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Followup Present.";
        return array($status, $id, $msg);
    }
}

///  Status Function

function getStatus($type) {

    global $con;

    $status = 0;
    $data = array();


    $sql = "select * from tbl_status_master where rec_type = $type and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row);
        }
        //print_r($data);
    } else {
        echo $msg = "Error : getting data from tbl_status_master: (" . $con->errno . ") " . $con->error;
    }

    return $data;
}

function getMonitor($type) {

    global $con;

    $status = 0;
    $data = array();


    $sql = "select * from tbl_monitors_master where $type = 1 and status = 0";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row);
        }
        //print_r($data);
    } else {
        echo $msg = "Error : getting data from tbl_status_master: (" . $con->errno . ") " . $con->error;
    }

    return $data;
}

/*
  function add_lang($source_name) {

  global $con;


  $status = 0;
  $id = 0;
  $msg = "";

  $sql = "select * from tbl_source_master where source_name = '" . $source_name . "'";
  $result = $con->query($sql);

  if ($result->num_rows > 0) {
  // output data of each row
  while ($row = $result->fetch_assoc()) {
  //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
  $id = $row["id"];
  }
  $status = 2; // 2 Means Already present
  //$id = 0;
  $msg = "This Puja Is Already Present.";
  return array($status, $id, $msg);
  }

  $add_date = getTimestamp();
  $add_user = $_SESSION['login_id'];
  echo $sql = "insert into tbl_source_master set source_name = '" . $source_name
  . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
  $result_add = $con->query($sql);
  if ($result_add) {
  $status = 1; // 1 Means New record added
  $id = $con->insert_id;
  $msg = "Puja Added Successfully";
  return array($status, $id, $msg);
  } else {

  $status = 0; // 0 Means Error
  $id = 0;
  $msg = "Error Adding Puja: (" . $con->errno . ") " . $con->error;
  return array($status, $id, $msg);
  }




  //printf("New Record has id %d.\n", $mysqli->insert_id);
  }

  function add_cast($source_name) {

  global $con;


  $status = 0;
  $id = 0;
  $msg = "";

  $sql = "select * from tbl_source_master where source_name = '" . $source_name . "'";
  $result = $con->query($sql);

  if ($result->num_rows > 0) {
  // output data of each row
  while ($row = $result->fetch_assoc()) {
  //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
  $id = $row["id"];
  }
  $status = 2; // 2 Means Already present
  //$id = 0;
  $msg = "This Puja Is Already Present.";
  return array($status, $id, $msg);
  }

  $add_date = getTimestamp();
  $add_user = $_SESSION['login_id'];
  echo $sql = "insert into tbl_source_master set source_name = '" . $source_name
  . "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";
  $result_add = $con->query($sql);
  if ($result_add) {
  $status = 1; // 1 Means New record added
  $id = $con->insert_id;
  $msg = "Puja Added Successfully";
  return array($status, $id, $msg);
  } else {

  $status = 0; // 0 Means Error
  $id = 0;
  $msg = "Error Adding Puja: (" . $con->errno . ") " . $con->error;
  return array($status, $id, $msg);
  }




  //printf("New Record has id %d.\n", $mysqli->insert_id);
  }
 */

function getError($error_msg, $msg) {
    //if (strlen(trim($msg)) > 0) {
    //$error_msg . "<br/>- $msg.";
    //}
    return $error_msg . "<br/>- $msg.";
}

function getTimestamp() {
    date_default_timezone_set('IST');
    //$this->timestamp = date('Y-m-d H:i:s');
    return date('Y-m-d H:i:s');
}

/* Feeds Function */

function addFeeds($edit_id, $data_sql) {

    global $con;


    $status = 0;
    $id = 0;
    $msg = "";


    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];




    if ($edit_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = "mod_date = '$add_date', mod_user=$add_user where id = $edit_id";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = "add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    }

    //echo $sql = "insert into tbl_pooja_master set pooja_name = '" . $puja_name. "', add_date = '$add_date', mod_date = '$add_date',description='', rec_type=1, add_user=$add_user";

    $sql = $query_start . $data_sql . $query_end;

    // echo "<br/><br/> $edit_id Ganerated query =======  ";
    //echo $sql;
    //echo "======= Ganerated query End  <br/><br/>";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        if ($edit_id > 0) {
            $id = $edit_id;
        }
        $msg = "Feeds $action Successfully";
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error in $action Enquiry: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        return array($status, $id, $msg);
    }




    //printf("New Record has id %d.\n", $mysqli->insert_id);
}

function getFeeds($edit_id = 0, $page_no = 0) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " And tf.id = $edit_id";
    }
    if ($page_no > 0) {
        //$min = 0;
        $rec = 10;
        $max = $rec * $page_no;
        $min = $max - $rec;

        $limit_clause = " limit $min,$max";
    }

    //$sql = "select * from tbl_feeds where status = 0 $where_clause $limit_clause order by publish_date desc";
    $sql = "select tf.*,tfm.feed_name,tfm.feed_img from tbl_feeds tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id $where_clause  order by tf.publish_date desc $limit_clause";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

function getFeedType($edit_id = 0, $page_no = 0) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " where id = $edit_id";
    }
    if ($page_no > 0) {
        $min = 0;
        $max = 10;
        $limit_clause = " limit $min,$max";
    }

    $sql = "select * from tbl_feeds_type_master $where_clause $limit_clause";
    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}

/* Feeds Function End */


/* App Feeds Function  */

function App_getFeeds($edit_id = 0, $page_no = 0, $lang = 1, $order_by = "publish") {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " And tf.id = $edit_id";
    }
    if ($page_no > 0) {
        //$min = 0;
        $rec = 5;
        $max = $rec * $page_no;
        $min = $max - $rec;
        $max = 5;


        $limit_clause = " limit $min,$max";
    }

    if ($lang > 0) {

        if ($lang == 1) {
            $feed_lang_data = " tf.image post_image,tf.description post_desc ";
            $where_clause_lang = "( tf.image <> '' or tf.description <> '' )";
        } elseif ($lang == 2) {
            $feed_lang_data = " tf.hindi_img post_image,tf.hindi_desc post_desc ";
            $where_clause_lang = "( tf.hindi_img <> '' or tf.hindi_desc <> '' )";
        } else {
            $feed_lang_data = " tf.image post_image,tf.description post_desc ";
            $where_clause_lang = "( tf.image <> '' or tf.description <> '' )";
        }
    } else {
        $feed_lang_data = " tf.image post_image,tf.description post_desc ";
        $where_clause_lang = "( tf.image <> '' or tf.description <> '' )";
    }


    //$sql = "select * from tbl_feeds where status = 0 $where_clause $limit_clause order by publish_date desc";
    //echo $sql = "select tf.id,tf.title post_title,$feed_lang_data,tf.url post_url,tf.publish_date post_publish_date,tfm.feed_name type_name,tfm.feed_img type_img,tf.feed_likes post_likes from tbl_feeds tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id and publish_date <= now() $where_clause  order by tf.publish_date desc $limit_clause";

    $sql = "select tf.id,tf.title post_title,$feed_lang_data,tf.url post_url,tf.publish_date post_publish_date,tfm.feed_name type_name,tfm.feed_img type_img,tf.feed_likes post_likes from tbl_feeds tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id and publish_date <= now() $where_clause and $where_clause_lang order by tf.publish_date desc $limit_clause";

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}


function App_getFeeds_tmp($edit_id = 0, $page_no = 0, $lang = 1, $order_by = "publish") {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($edit_id > 0) {
        //$msg = "Please Select Proper Enquiry.";
        //return array($status, $id, $msg);
        $where_clause = " And tf.id = $edit_id";
    }
    if ($page_no > 0) {
        //$min = 0;
        $rec = 5;
        $max = $rec * $page_no;
        $min = $max - $rec;
        $max = 5;


        $limit_clause = " limit $min,$max";
    }

    if ($lang > 0) {

        if ($lang == 1) {
            $feed_lang_data = " tf.title post_title, tf.date_english feed_date, tf.image post_image,tf.description post_desc, tfm.feed_name type_name ";
            $where_clause_lang = "( tf.image <> '' or tf.description <> '' )";
        } elseif ($lang == 2) {
            //$feed_lang_data = " tf.hindi_img post_image,tf.hindi_desc post_desc ";
            $feed_lang_data = " tf.title_hindi post_title, tf.date_hindi feed_date, tf.img_hindi post_image,tf.desc_hindi post_desc, tfm.feed_name_hindi type_name  ";
            $where_clause_lang = "( tf.img_hindi <> '' or tf.desc_hindi <> '' )";
        } else {
            $feed_lang_data = " tf.title post_title, tf.date_english feed_date, tf.image post_image,tf.description post_desc, tfm.feed_name type_name ";
            $where_clause_lang = "( tf.image <> '' or tf.description <> '' )";
        }
    } else {
        $feed_lang_data = " tf.title post_title, tf.date_english feed_date, tf.image post_image,tf.description post_desc, tfm.feed_name type_name  ";
        $where_clause_lang = "( tf.image <> '' or tf.description <> '' )";
    }


    //$sql = "select * from tbl_feeds where status = 0 $where_clause $limit_clause order by publish_date desc";
    //echo $sql = "select tf.id,tf.title post_title,$feed_lang_data,tf.url post_url,tf.publish_date post_publish_date,tfm.feed_name type_name,tfm.feed_img type_img,tf.feed_likes post_likes from tbl_feeds tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id and publish_date <= now() $where_clause  order by tf.publish_date desc $limit_clause";

    $sql = "select tf.id,tf.url post_url,tf.publish_date post_publish_date,$feed_lang_data,tfm.feed_img type_img,tf.feed_likes post_likes from tbl_feeds tf inner join tbl_feeds_type_master tfm on tf.status = 0 and tf.type = tfm.id and publish_date <= now() $where_clause and $where_clause_lang order by tf.publish_date desc $limit_clause";

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "No Enquiry Present";
        return array($status, $id, $msg);
    }
}


function App_addFeedLikes($feed_id, $user_email, $like_status, $mob_no = 0, $user_id = 0) {

    global $con;

    $Error = FALSE;

    $feed_status = 0;
    $data = array();
    $where_clause = "";
    $limit_clause = "";

    //if (!($feed_id > 0) || !validateEmail($user_email) || $like_status >1) {

    if (!($feed_id > 0) || validateEmail($user_email) || !isset($like_status)) {
        $msg = "Error : feed_id =  $feed_id, validateEmail = (" . validateEmail($user_email) . "),like status = $like_status";
        logmsg("App_addFeedLikes -> " . $msg);
        return array(0, 0, $msg);
    }


    //return array(0, 0, "Pass");

    list($status, $id, $data) = App_getFeeds($feed_id);
    if (!$status) {
        $msg = "Error : No such Feeds Present with id ($feed_id)";
        logmsg("App_addFeedLikes -> " . $msg);
        return array($status, 0, $msg);
    }

    list($status, $id, $data) = App_getUserFeedsLike($feed_id, $user_email);

    //echo $sql = "insert into tbl_feeds_likes set feed_id = $feed_id , user_email = '$user_email', like_status = $like_status, user_id = $user_id, mob_id = '$mob_no' ";
    logmsg("App_addFeedLikes -> status = $status And like_status = $like_status");

    if ($status == 0 && $like_status > 0) {
        $sql = "insert into tbl_feeds_likes  (feed_id, user_email, user_id, like_status, mob_id) values ($feed_id,'$user_email',$user_id,$like_status,'$mob_no') ";
    } elseif (($status == 1) && $like_status == 0) {
        $sql = "delete from tbl_feeds_likes where  feed_id = $feed_id and user_email = '$user_email' ";
    }

    //logmsg("App_addFeedLikes -> sql => $sql");

    if (!strlen($sql) > 0) {
        $msg = "Error : App_addFeedLikes -> No Action To Perform ";
        logmsg($msg);
        return array($feed_status, $id, $msg);
    }

    $result_add = $con->query($sql);

    if ($result_add) {
        $feed_status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Updated Feeds Like Successfully with id = $id and like_status = $like_status";
        logmsg("App_addFeedLikes -> " . $msg);
        list($status, $id, $feed_update_msg) = App_UpdateFeedLikes($feed_id);
        return array($feed_status, $id, $msg);
    } else {

        $feed_status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : in Addeding Feeds_like: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_addFeedLikes -> " . $msg);
        return array($feed_status, $id, $msg);
    }
}

function App_getUserFeedsLike($feed_id = 0, $user_email) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";

    if ($feed_id > 0) {
        $where_clause = " feed_id = $feed_id and ";
        $data_clause = "sum(like_status) feed_likes";
    } else {
        $msg = "Error : Feed id ($feed_id) Not specified";
        logmsg("App_getUserFeedsLike -> " . $msg);
        return array($status, 0, $msg);
    }

    if (strlen($user_email) > 0) {
        if (validateEmail($user_email)) {
            $msg = "Error : invalid Email [$user_email]";
            logmsg("App_getUserFeedsLike -> " . $msg);
            return array($status, 0, $msg);
        } else {
            $where_clause = $where_clause . "user_email = '$user_email' and ";
            $data_clause = "*";
        }
    }


    $sql = "SELECT $data_clause FROM `tbl_feeds_likes` WHERE $where_clause status = 0";
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);

    //echo "<br/> App_getUserFeedsLike Query = $sql <br/>";

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: ".$row["firstname"]." ".$row["lastname"]."<br>";  //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "Error : App_getUserFeedsLike -> Error in getting User Feeds likes : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_getUserFeedsLike ->  " . $msg);
        return array($status, $id, $msg);
    }
}

function App_UpdateFeedLikes($feed_id, $like_status = 1) {

    global $con;

    $Error = FALSE;
    $status = 0;

    $data = array();
    $where_clause = "";
    $limit_clause = "";


    list($status, $id, $data) = App_getUserFeedsLike($feed_id);
    $feed_likes = $data[0]["feed_likes"];
    $msg = "feed_likes = $feed_likes data Array = " . print_r($feed_likes, TRUE);
    logmsg("App_addFeedLikes -> " . $msg);

    if (!isset($feed_likes)) {
        $msg = "Error : No Count Present";
        logmsg("App_addFeedLikes -> " . $msg);
        return array($status, 0, $msg);
    }

    $sql = "Update tbl_feeds set feed_likes = $feed_likes where id = $feed_id and status = 0 ";
    $result_add = $con->query($sql);

    if ($result_add) {
        $status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Feeds $feed_id Updated with count [$feed_likes] Successfully";
        logmsg("App_UpdateFeedLikes -> " . $msg);
        return array($status, $id, $msg);
    } else {

        $status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : Updating Feeds Count : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_UpdateFeedLikes -> " . $msg);
        return array($status, $id, $msg);
    }
}

function App_addRegisterUser($user_id, $user_email, $gcm_code = 0, $device_id = 0, $user_name = "", $user_phone = "") {

    global $con;
    $Error = FALSE;
    $feed_status = 0;
    $data = array();
    $where_clause = "";
    $limit_clause = "";
    //$query_start = "insert into ";
    //if (!($feed_id > 0) || !validateEmail($user_email) || $like_status >1) {


    if (empty($user_id) && empty($user_email) && empty($gcm_code) && empty($user_name) && empty($user_phone)) {
        $msg = "Error : No Data For Processing";
        logmsg("App_addRegisterUser -> " . $msg);
        return array($status, 0, $msg);
    }

    if (strlen(trim($user_email)) > 0) {
        if (validateEmail($user_email)) {
            $msg = "Error : invalid Email [$user_email]";
            logmsg("App_addRegisterUser -> " . $msg);
            return array($status, 0, $msg);
        } else {
            $where_clause .= "email_1 = '$user_email' and ";
            $data_clause = "email_1 = '$user_email', ";
        }
    }




    list($status, $id, $data) = App_getRegisterUser($user_id, $user_email, $user_name, $user_phone);

    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    if (!($add_user > 0)) {
        $add_user = 1;
    }

    if (strlen(trim($gcm_code)) > 0) {
        if (strlen($data[0]['gcm_code']) > 5) {
            $mystring = $data[0]['gcm_code'];
            $findme = $gcm_code;
            $pos = strpos($mystring, $findme);
// Note our use of ===.  Simply == would not work as expected
// because the position of 'a' was the 0th (first) character.
            if ($pos === false) {
                //echo "The string '$findme' was not found in the string '$mystring'";
                //$gcm_code = $data[0]['gcm_code'] . "," . $gcm_code;
                //$gcm_code = "send_dates=concat(send_dates, ',$current_date'),";$data[0]['gcm_code'] . "," . $gcm_code;
                //$gcm_code = $data[0]['gcm_code'];
                
                //$data_clause .= " gcm_code=concat(gcm_code, ',$gcm_code'), ";
                $data_clause .= " gcm_code='$gcm_code', ";
            } else {
                //echo "The string '$findme' was found in the string '$mystring'";
                //echo " and exists at position $pos";
                //$gcm_code = "";
            }
            //$gcm_code = $data[0]['gcm_code'] . "," . $gcm_code;
        } else {
            $data_clause .= " gcm_code='$gcm_code', ";
        }
    }

    if (strlen(trim($user_name)) > 0) {
        $data_clause .= "first_name = '$user_name', ";
    }

    if (strlen(trim($user_phone)) > 0) {
        $data_clause .= "phone_1 = '$user_phone', ";
    }

    if (strlen(trim($device_id)) > 0) {
        $data_clause .= "device_id = '$device_id', ";
    }


    $user_id = $id;
    if ($user_id > 0) {
        $action = "Updated";
        $query_start = "Update ";
        $query_end = " mod_date = '$add_date', mod_user=$add_user where id = $user_id and ";
    } else {
        $action = "Added";
        $query_start = "insert into ";
        $query_end = " add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user, ";
    }

    $sql = "$query_start `tbl_customer_app` set $data_clause $query_end status = 0";
    //echo "query = ".$sql;
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);
    //$result = $con->query($sql);

    $result_add = $con->query($sql);

    if ($result_add) {
        $feed_status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "User Added Successfully with id = $id ";
        logmsg("App_addRegisterUser -> " . $msg);
        //list($status, $id, $feed_update_msg) = App_UpdateFeedLikes($feed_id, 1, $like_status);
        return array($feed_status, $id, $msg);
    } else {

        $feed_status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : in Addeding User: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_addRegisterUser -> " . $msg);
        return array($feed_status, $id, $msg);
    }
}

function App_getRegisterUser($user_id, $user_email, $user_name = "", $user_phone = "") {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    //$limit_clause = "";

    if ($user_id > 0) {
        $where_clause = " id = $user_id and ";
        //$data_clause = "sum(like_status) feed_likes";
    }

    if (strlen($user_email) > 0) {
        if (validateEmail($user_email)) {
            $msg = "Error : invalid Email [$user_email]";
            logmsg("App_getRegisterUser -> " . $msg);
            return array($status, 0, $msg);
        } else {
            $where_clause .= "email_1 = '$user_email' and ";
            //$data_clause = "*";
        }
    }


    $sql = "SELECT * FROM `tbl_customer_app` WHERE $where_clause status = 0";
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "Error : Error in getting Register User : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_getRegisterUser ->  " . $msg);
        return array($status, $id, $msg);
    }
}

function App_getDeviceUser($user_email = "", $device_id = 0) {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    //$limit_clause = "";

    if (empty($user_email) || empty($device_id)) {
        $msg = "Error : No Data For Processing email : [$user_email], Device : [$device_id] ";
        logmsg("App_getDeviceUser -> " . $msg);
        return array($status, 0, $msg);
    }

    if (strlen($user_email) > 0) {
        if (validateEmail($user_email)) {
            $msg = "Error : invalid Email [$user_email]";
            logmsg("App_getDeviceUser -> " . $msg);
            return array($status, 0, $msg);
        } else {
            $where_clause .= "email_1 = '$user_email' ";
            //$data_clause = "*";
            if (strlen($device_id) > 0) {
                $where_clause .= "or device_id = '$device_id' and ";
            } else {
                $where_clause .= " and ";
            }
        }
    }



    $sql = "SELECT * FROM `tbl_customer_app` WHERE $where_clause status = 0";
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "Error : Error in getting Register User : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_getDeviceUser ->  " . $msg);
        return array($status, $id, $msg);
    }
}

function App_validatePromoCode($promo_code = "") {

    global $con;

    $status = 0;

    $data = array();
    $where_clause = "";
    //$limit_clause = "";


    if (!strlen($promo_code) > 0) {
        $msg = "Error : Empty Promo Code [$promo_code]";
        logmsg("App_getPromoUser -> " . $msg);
        return array($status, 0, $msg);
    }



    $sql = "SELECT * FROM `tbl_cust_app_promo` WHERE promo_code = '$promo_code' and status = 0";
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            //echo "id: " . $row["id"] . " - Name: " . $row["firstname"] . " " . $row["lastname"] . "<br>";
            //$data = $row;
            $id = $row["id"];
            array_push($data, $row);
        }
        $status = 1; // 2 Means Already present
        //$id = 0;
        //$msg = "This Source Is Already Present.";
        return array($status, $id, $data);
    } else {
        $msg = "Error : Error in getting Promo Code : (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_getDeviceUser ->  " . $msg);
        return array($status, $id, $msg);
    }
}

function App_addPromoUser($user_id, $promo_code, $device_id=0) {

    global $con;
    $Error = FALSE;
    $feed_status = 0;
    $data = array();
    $where_clause = "";
    $limit_clause = "";
    //$query_start = "insert into ";
    //if (!($feed_id > 0) || !validateEmail($user_email) || $like_status >1) {


    if (empty($user_id) && empty($device_id) && empty($promo_code)) {
        $msg = "Error : No Data For Processing";
        logmsg("App_addPromoUser -> " . $msg);
        return array($status, 0, $msg);
    }

    if (!$user_id > 0) {
        $msg = "Error : invalid User id = [$user_id]";
        logmsg("App_addPromoUser -> " . $msg);
        return array($status, 0, $msg);
    }
    if (!strlen($promo_code) > 0) {
        $msg = "Error : invalid Promo Code = [$promo_code]";
        logmsg("App_addPromoUser -> " . $msg);
        return array($status, 0, $msg);
    } 
    
    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    if (!($add_user > 0)) {
        $add_user = 1;
    }

    
    $sql = "insert into tbl_cust_app_promo set user_id = $user_id, promo_child = '$promo_code', promo_device = '$device_id', add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user, status = 0 ";
    
    $result_add = $con->query($sql);

    if ($result_add) {
        $feed_status = 1; // 1 Means New record added 
        $id = $con->insert_id;
        $msg = "Promocode Added Successfully with id = $id For user id [$user_id] and promo_code [$promo_code]";
        logmsg("App_addPromoUser -> " . $msg);
        //list($status, $id, $feed_update_msg) = App_UpdateFeedLikes($feed_id, 1, $like_status);
        return array($feed_status, $id, $msg);
    } else {

        $feed_status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : in Addeding Promocode: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_addPromoUser -> " . $msg);
        return array($feed_status, $id, $msg);
    }
}


function App_getPromoCode($user_id) {

    global $con;
    $Error = FALSE;
    $feed_status = 0;
    $data = array();
    $where_clause = "";
    $limit_clause = "";
    //$query_start = "insert into ";
    //if (!($feed_id > 0) || !validateEmail($user_email) || $like_status >1) {


    if (empty($user_id)) {
        $msg = "Error : No Data For Processing";
        logmsg("App_getPromoCode -> " . $msg);
        return array($status, 0, $msg);
    }


    
    $add_date = getTimestamp();
    $add_user = $_SESSION['login_id'];
    if (!($add_user > 0)) {
        $add_user = 1;
    }
    
    $promo_code = "wmp$user_id";

    
    
    $sql = "SELECT * FROM `tbl_cust_app_promo` WHERE user_id = $user_id and status = 0";
    //$sql = "SELECT * FROM `tbl_feeds_likes` WHERE user_email = '$user_email' and feed_id = $feed_id";
    //logmsg("App_getUserFeedsLike -> Sql = " . $sql);

    $result = $con->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
           //INSERT INTO table (id, name, age) VALUES(1, "A", 19) ON DUPLICATE KEY UPDATE  name="A", age=19
    //$sql = "insert into tbl_cust_app_promo set user_id = $user_id, promo_code = '$promo_code', add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user, status = 0 ON DUPLICATE KEY UPDATE  promo_code = '$promo_code', mod_date = '$add_date', mod_user=$add_user, status = 0";
    
   $sql = "update tbl_cust_app_promo set promo_code = '$promo_code', mod_date = '$add_date', mod_user=$add_user, status = 0 where user_id = $user_id";
       
    } else {
        
        $sql = "insert into tbl_cust_app_promo set user_id = $user_id, promo_code = '$promo_code', add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user, status = 0";
        
    }
    
    
    //echo "$sql";
     $result_add = $con->query($sql);

    if ($result_add) {
        $feed_status = 1; // 1 Means New record added 
        $id = $promo_code ; //$con->insert_id;
        $msg = "Promocode Added Successfully with id = $id For user id [$user_id] and promo_code [$promo_code]";
        logmsg("App_getPromoCode -> " . $msg);
        //list($status, $id, $feed_update_msg) = App_UpdateFeedLikes($feed_id, 1, $like_status);
        return array($feed_status, $id, $msg);
    } else {

        $feed_status = 0; // 0 Means Error
        $id = 0;
        $msg = "Error : in Addeding Promocode: (" . $con->errno . ") " . $con->error;
        $msg .= "<br>Query = $sql";
        logmsg("App_addPromoUser -> " . $msg);
        return array($feed_status, $id, $msg);
    }
    
    
    
    
    
    
 
}

/* App Feeds Function End */
?>