<?php
$Main_Menu = "Followup";
$Sub_Menu_1 = "enquiry_followup";

$page = "Enquiry Followup";
$page_title = "Enquiry Followup";
$edit_id = 0;

//print_r($_GET);


include_once 'config/auth.php';

$date = strtotime(date("Y-m-d"));
$day_1 = date("d-m-Y", $date);

if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    //$start_date = date("Y-m-d", strtotime($start_date));

    if (strlen($start_date) == 10) {
        $start_date = date("Y-m-d", strtotime($start_date));
    }
} else {
    $start_date = "";
    $start_date = date("Y-m-d", strtotime($day_1));  //$date;
}
if (isset($_GET["to_date"])) {
    $end_date = $_GET["to_date"];
    if (strlen($end_date) == 10) {
        $end_date = date("Y-m-d", strtotime($end_date));
    } else {
        $end_date = "";
    }
} else {
    $end_date = "";
}


if (isset($_GET["tab"])) {
    $tab = $_GET["tab"];
    //$end_date = date("Y-m-d", strtotime($end_date));
} else {
    $tab = "other";
}


//echo "Start Date :- ".$start_date;
if ($tab == 'critical') {
    $query = "SELECT e.enq_no,e.cust_id,e.enq_date,e.quotation_id,e.enq_status e_status,e.followup_date next_date,id enq_id
            FROM tbl_enquiry e  
            where e.followup_date < '$start_date'
            and (e.enq_status != 5 and e.enq_status != 8)
            order by e.followup_date desc";


//$query = "SELECT * from tbl_enquiry";
    list($status, $id, $result_arr) = getDataByQuery($query);
    if ($status == 0) {
        $err_msg = $result_arr;
    }
} else {
    list($status, $id, $result_arr) = getFollowupByDate($start_date, $end_date); //getEnqData(); //getOrderTmpData();
    if ($status == 0) {
        $err_msg = $result_arr;
    }
}

$query = "SELECT id,name from tbl_admin";
list($status, $id, $admin_arr) = getDataByQuery($query);


//print_r($result_arr);
//$date = "Mar 03, 2011";
//$date = date("Y-m-d");

$day_1_url = "list_enquiry_followup_tmp.php?from_date=$day_1";
$day_2 = date("d-m-Y", strtotime("+1 day", $date));
$day_2_url = "list_enquiry_followup_tmp.php?from_date=$day_2";
$day_3 = date("d-m-Y", strtotime("+2 day", $date));
$day_3_url = "list_enquiry_followup_tmp.php?from_date=$day_3";
$day_4 = date("d-m-Y", strtotime("+3 day", $date));
$day_4_url = "list_enquiry_followup_tmp.php?from_date=$day_4";
$day_5 = date("d-m-Y", strtotime("+4 day", $date));
$day_5_url = "list_enquiry_followup_tmp.php?from_date=$day_5";

$critical_url = "list_enquiry_followup_tmp.php?tab=critical";

$day_selected = date("d-m-Y", strtotime($start_date));

//echo "<br/>$day_1 , $day_2, $day_3, $day_4, $day_5<br/>";

if ($tab == 'critical') {
    $tab = "critical";
    //$day_5_url = "#";
} elseif ($day_selected == $day_1) {
    $tab = "day_1";
    //$day_1_url = "#";
} elseif ($day_selected == $day_2) {
    $tab = "day_2";
    //$day_2_url = "#";
} elseif ($day_selected == $day_3) {
    $tab = "day_3";
    //$day_3_url = "#";
} elseif ($day_selected == $day_4) {
    $tab = "day_4";
    //$day_4_url = "#";
} elseif ($day_selected == $day_5) {
    $tab = "day_5";
    //$day_5_url = "#";
} else {
    $tab = "other";
}



//echo "[$day_selected]==[$day_1]  -> Tab = $tab";

/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> 
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    -->

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">


                                        <li class=""><a data-toggle="tab" href="#User-Details">Choose Date</a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'day_1'); ?>">
                                            <a href="<?php echo $day_1_url; ?>"><span> Today </span></a>
                                        </li>

                                        <?php //if ($enq_id > 0) {     ?>
                                        <li class="<?php getActiveTab($tab, 'day_2'); ?>">
                                            <a  href="<?php echo $day_2_url; ?>"><span> Tomorrow </span></a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'day_3'); ?>">
                                            <a  href="<?php echo $day_3_url; ?>"><?php echo date("D j", strtotime($day_3)); ?></a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'day_4'); ?>">
                                            <a  href="<?php echo $day_4_url; ?>"><span>  <?php echo date("D j", strtotime($day_4)); ?> </span></a>
                                        </li>

                                        <li class="<?php getActiveTab($tab, 'day_5'); ?>">
                                            <a  href="<?php echo $day_5_url; ?>"><?php echo date("D j", strtotime($day_5)); ?></a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'critical'); ?>">
                                            <a  href="<?php echo $critical_url; ?>">Critical</a>
                                        </li>
                                        <?php if ($tab == "other") { ?>
                                            <li class="<?php getActiveTab($tab, 'other'); ?>">
                                                <a data-toggle="tab" href="#other">Other</a>
                                            </li>
                                            <?php
                                        }
//}
                                        ?>




                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                        <div id="enquiry-details" class="tab-pane fade active in">

                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="panel panel-default">
                                                        <div class="box" id="order_tmp_list_msg" >
                                                            <div class="col-lg-12">
                                                                <?php if ($err_msg) { ?>

                                                                    <div class="alert alert-danger alert-dismissable">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                                        <?php echo $err_msg; ?>. <!-- <a class="alert-link" href="#">Alert Link</a>. -->
                                                                    </div>

                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                        <?php if ($status == 1) { ?>
                                                            <!-- <div class="panel-body">  -->  <!-- </div> -->                                        
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="font-size: 12px !important;">
                                                                    <thead>
                                                                        <tr>

                                                                            <th>No</th>  
                                                                            <th style="width: 70px">User</th>
                                                                            <th style="width: 70px">Enquiry</th>
                                                                            <th>Date </th>
                                                                            <th>Customer Name</th>                                                            
                                                                            <th>Enq Status </th>
                                                                            <th>Followup </th>
                                                                            <th>Status </th>
                                                                            <th>Next Followup </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                        <?php
                                                                        $count = 0;
//while ($row = $stmt->fetch_assoc()) {
                                                                        for ($i = 0; $i < count($result_arr); $i++) {

                                                                            $count++;
                                                                            $enq_id = $result_arr[$i]['enq_id'];

                                                                            /*  Show Enq Date Proper Format */
                                                                            if (strlen($result_arr[$i]["enq_date"]) > 18) {
                                                                                list($enq_date, $enq_time) = validateDbDateTime($result_arr[$i]["enq_date"]);
                                                                                $enq_date = $enq_date . "<br/>" . $enq_time;
                                                                            } else {
                                                                                $enq_date = "";
                                                                            }



                                                                            /*  Show User Data */
                                                                            $cust_id = $result_arr[$i]['cust_id'];
                                                                            list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);

                                                                            if ($cust_status == 1) {
                                                                                $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
                                                                                $cust_contact = "<br/> " . $cust_data_arr["phone_1"] . " , " . $cust_data_arr["phone_2"];
                                                                                $cust_contact .= "<br/>" . $cust_data_arr["email_1"];

                                                                                $cust_city = $cust_data_arr["city"];
                                                                                $cust_area = $cust_data_arr["area"];
                                                                            } else {
                                                                                $cust_name = $result_arr[$i]['customer_name'] . "<br/>" . $result_arr[$i]['customer_contact'];
                                                                                $cust_city = $result_arr[$i]["customer_city"];
                                                                                $cust_area = $result_arr[$i]["customer_area"];
                                                                            }


                                                                            $enq_status = getStatus(0, $result_arr[$i]['e_status']);
                                                                            $enq_status = (isset($enq_status[0]["status_name"]) ? $enq_status[0]["status_name"] : $result_arr[$i]['e_status']);




                                                                            /*  Show Followup Date */
                                                                            $follow_up_date = "";
                                                                            $next_follow_up_date = "";
                                                                            if (strlen($result_arr[$i]["next_date"]) > 9) {
                                                                                list($follow_up_date, $follow_up_time) = validateDbDateTime($result_arr[$i]["next_date"]);
                                                                                $follow_up_date = date('Y-m-d', strtotime($follow_up_date));
                                                                                $follow_up_date_DB = $follow_up_date . "<br/>" . $follow_up_time;
                                                                                //$follow_up_date =  $result_arr[$i]["next_date"];
                                                                            } else {
                                                                                $follow_up_date_DB = "";
                                                                            }

                                                                            /*  Show Followup Date End */

                                                                            if (strtotime($result_arr[$i]["followup_date"]) > strtotime($result_arr[$i]["next_date"])) {
                                                                                $followup_status = "Done";

                                                                                /*  Show Next Followup Date */
                                                                                if (strlen($result_arr[$i]["followup_date"]) > 9) {
                                                                                    list($follow_up_date, $follow_up_time) = validateDbDateTime($result_arr[$i]["followup_date"]);
                                                                                    if ($follow_up_date)
                                                                                        $follow_up_date = date('Y-m-d', strtotime($follow_up_date));
                                                                                    $next_follow_up_date = $follow_up_date . "<br/>" . $follow_up_time;
                                                                                    //$next_follow_up_date = date(, strtotime($result_arr[$i]["followup_date"]));
                                                                                } else {
                                                                                    $next_follow_up_date = " - " . $result_arr[$i]["followup_date"];
                                                                                }
                                                                                //$next_follow_up_date = $next_follow_up_date." - ".strlen($result_arr[$i]["followup_date"]);
                                                                                /*  Show Next Followup Date End */
                                                                            } elseif (strtotime(date("Y-m-d H:i:s")) > strtotime($result_arr[$i]["followup_date"])) {
                                                                                //$followup_status = "Critical - " . $result_arr[$i]["followup_date"] . " => " . date("Y-m-d H:i:s");
                                                                                // strtotime(date("Y-m-d H:i:s")) - strtotime($result_arr[$i]["followup_date"]) 
                                                                                //new DateTime() > new DateTime("2010-05-15 16:00:00")
                                                                                $followup_status = "Critical";
                                                                            } elseif (strtotime($result_arr[$i]["followup_date"]) == strtotime($result_arr[$i]["next_date"])) {
                                                                                $followup_status = "";
                                                                            }


                                                                            list($status, $user_id, $msg) = searchIdInArray($result_arr[$i]["add_user"], $admin_arr, 'id');
                                                                            if ($status == 1) {
                                                                                $enq_user = $admin_arr[$user_id]["name"];
                                                                            } else {
                                                                                $enq_user = $result_arr[$i]["add_user"] . "_No User".$result_arr[$i]["add_user"];
                                                                            }
                                                                            
                                                                            ?> 
                                                                            <tr class="odd gradeX">

                                                                                <td><?php echo $count; ?></td>
                                                                                <td><?php echo $enq_user; ?></td>
                                                                                <td>
                                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>" target="_blank" Title = "Click Here to Edit" class="btn text-info btn-xs btn-flat"><?php echo $result_arr[$i]['enq_no']; ?></a>
                                                                                </td>
                                                                                <td><?php echo $enq_date; ?></td>

                                                                                <td>
                                                                                    <a href="add_user.php?edit_id=<?php echo $cust_id; ?>" target="_blank" Title = "Click Here to Edit" class="btn text-info btn-xs btn-flat"><?php echo $cust_name; ?></a>
                                                                                    <?php echo $cust_contact; ?>
                                                                                </td>


                                                                                <td><?php echo $enq_status; ?></td>


                                                                                <td>
                                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>&tab=Followup-Tab" target="_blank" Title = "Click Here to Followup" class="btn text-info btn-xs btn-flat">
                                                                                        <?php echo $follow_up_date_DB; ?></a>
                                                                                </td> 
                                                                                <td><?php echo $followup_status; ?></td>
                                                                                <td>
                                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>&tab=Followup-Tab" target="_blank" Title = "Click Here to Followup" class="btn text-info btn-xs btn-flat">
                                                                                        <?php echo $next_follow_up_date; ?></a>
                                                                                </td>


                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>





                                                                    </tbody>
                                                                </table>
                                                            </div>                                            

                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>



                                        <div id="User-Details" class="tab-pane fade <?php getActiveBody($tab, 'User-Details'); ?>">

                                            <form name="search_followup" id="search_followup" action="list_enquiry_followup_tmp.php" method="get" enctype="multipart/form-data">
                                                <div class="col-lg-12">

                                                    <div class="box">                                 
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading"> User Details </div>
                                                            <div class="panel-body">
                                                                <table class="table table-bordered sortableTable responsive-table">

                                                                    <tbody>


                                                                        <tr>
                                                                            <td>From Date </td>
                                                                            <td>
                                                                                <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="from_date" id="from_date" title="From Date">
                                                                            </td>
                                                                            <td>To Date </td>
                                                                            <td>
                                                                                <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="to_date" id="to_date" title="To Date">                                    
                                                                            </td>


                                                                        </tr>



                                                                    </tbody>
                                                                </table>




                                                            </div>
                                                            <div class="panel-footer" align="right"> 
                                                                <span class="input-group-btn">
                                                                    <!-- <input class="btn btn-success " type="submit" value="Save" name="Save" > -->
                                                                    <button id="btn-chat" class="btn btn-success btn-sm" > Search</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';                    ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->







        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <!-- <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>


        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>

        <script src="assets/plugins/daterangepicker/moment.min.js"></script>

        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script> -->
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>

        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>       
        <script src="assets/js/formsInit.js"></script>
        <script>

            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
            $(function () {
                formInit();
            });

        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
