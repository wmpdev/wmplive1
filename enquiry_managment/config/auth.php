<?php
//echo "in Auth";
// Report all errors
error_reporting(E_ALL);
include_once 'config.php';        
        
if(!$login_user->CheckLogin() )
{
    $login_user->urlredirection("login.php");
    exit;
}
 
    //echo "<br/>in Auth : user login success = path = $include_path";
    include_once $include_path.'model/Log.php';
    include_once $include_path.'model/Master.php';
    include_once $include_path.'model/User.php';
    include_once $include_path.'model/Location.php';
    include_once $include_path.'model/Enquiry.php';
    include_once $include_path.'model/Product.php';
    include_once $include_path.'model/Vendor.php';  
    include_once $include_path.'model/Order.php';
    
    include_once $include_path.'model/sms.php';
    
    
    include_once $include_path.'function/function.php';
    
    //echo "<br/>in Auth : Pass All Include";
    
    $log = new Log($con);
    $master = new Master($con);
    $user = new User($con);
    $location = new Location($con);
    $Enquiry = new Enquiry($con);
    $Product = new Product($con);
    $Vendor = new Vendor($con);
    $Order = new Order($con);
    
    date_default_timezone_set('Asia/Calcutta');
    
    //echo "<br/>in Auth : Auth Pass";

//echo "welcome,".$_SESSION['name_of_user'];

?>