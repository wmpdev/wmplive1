<?php

class Order extends Master {

    private $conn;
    private $tbl_order = "tbl_order";
    private $tbl_order_pooja = "tbl_order_pooja";
    private $tbl_order_product = "tbl_order_product";
    private $tbl_order_vendor = "tbl_order_vendor";
    //private $tbl_order_followup = "tbl_order_followup";
    // object properties for tbl_order
    public $cust_order_id = 0;
    public $id;
    public $cust_id = "";
    public $budget = "";
    public $pooja_date = "";
    public $pooja_time = "";
    public $service_id = "";
    public $medium = "";
    public $address_1 = "";
    public $address_2 = "";
    public $country_id = 0;
    public $state_id = 0;
    public $city_id = 0;
    public $area_id = 0;
    public $pincode = 0;
    public $caste_id = 0;
    public $comment = "";
    public $order_status = 1;
    public $followup_date = "";
    public $date = "";
    public $add_date = "";
    public $mod_date = "";
    public $add_user = 0;
    public $mod_user = 0;
    public $status = 0;
    public $timestamp = "";
    public $userMsg = "";
    public $SuccessMsg = "";
    public $ErrorMsg = "";
    public $ErrorFlag = 0;
    // object properties for order_details
    public $order_id;
    public $order_type_id = 0;
    public $product_id = 0;
    public $pooja_id = 0;
    public $vendor_id = 0;    
    public $quantity = 0;
    public $actual_cost = 0;
    public $selling_cost = 0;
    public $discount = 0;
    public $transport_cost = 0;
    public $tax_id = 0;
    //public $transport_cost="";
// Object properties for Followups
    public $add_time = "";
    public $next_date = "";
    public $next_time = "";

    public function __construct($db) {
        $this->conn = $db;
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    // used for the 'created' field when creating a product
    /*
      function getTimestamp() {
      //date_default_timezone_set('Asia/Manila');
      $this->timestamp = date('Y-m-d H:i:s');
      }
     */

    function inputValidation() {
        $this->ErrorFlag = 0;

        //if(strlen($this->phone_1)>0)   
        //$this->phone_1 =  str_replace(array('+', '(', ')', ' ','-'), '', $this->phone_1);
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad Tilkar
     */
    public function prepareOrderQuery() {
        $query = "";

        $query = "" . $this->tbl_order
                . " set cust_id = '" . $this->cust_id
                . "', enq_id = '" . $this->enq_id
                . "', budget = '" . $this->budget
                . "', pooja_date = '" . $this->pooja_date
                . "', pooja_time = '" . $this->pooja_time
                . "', service_id = '" . $this->service_id
                . "', medium = '" . $this->medium
                . "', address_1 = '" . $this->address_1
                . "', country_id = '" . $this->country_id
                . "', state_id = '" . $this->state_id
                . "', city_id = '" . $this->city_id
                . "', area_id = '" . $this->area_id
                . "', pincode = '" . $this->pincode
                . "', caste_id = '" . $this->caste_id
                . "', comment = '" . $this->comment
                . "', order_status = " . $this->order_status
                . ", followup_date = '" . $this->followup_date
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', status = 0";

        return $query;
    }

    public function addNewOrder($order_details) {

        global $log;
        $log->logMsg("Add New Order");


        $this->inputValidation();
        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add Order";
            return 0;
        }

        $this->timestamp = $this->getTimestamp();
        $insert_row = 0;
        $last_id = 0;

        $query = $this->prepareOrderQuery();
        $query = "INSERT INTO " . $query;


        $insert_row = $this->conn->query($query);
        //echo "<br/>New Order = " . $query . " <br/>result = $insert_row";

        if ($insert_row) {

            $last_id = $this->conn->insert_id;
            $this->SuccessMsg = "New Order Added Successfully. = $last_id =";
            $log->logMsg("Success : New Order Id = " . $last_id);

            /* Add Order Details in order 
              $order_details_id = $this->addOrderDetails($this->cust_id, $last_id, $order_details);
              $last_id = $order_details_id;
             */
            //unset($_SESSION[$this->cust_order_id]);
            //printf("Add User Error message: %s\n", $mysqli->error);
        } else {
            $Error_msg = $this->conn->error;
            $this->ErrorMsg = "Error : Fail To Add Order";
            $log->LogError("New Order add Error = " . $Error_msg . " Query = $query");
        }


        // else echo "<br/>Add User Error message:".$this->conn->error."<br/>";
        //echo "User Add query = $query";
        //echo " order result = " . $last_id;
        return $last_id;


        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
    }

    public function addOrderDetails($cust_id = 0, $order_id = 0, $order_details = "") {

        global $log;

        $this->getTimestamp();
        $log->logMsg("Add New Order Details");

        $this->inputValidation();
        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add Order Details" . $this->ErrorMsg;
            return 0;
        }


        if ((count($order_details) > 0) && ($cust_id > 0) && ($order_id > 0)) {
            //$count = count($_SESSION[$cust_id]);
            //print_r($_SESSION[$cust_order_id]);
            for ($i = 0; $i < count($order_details); $i++) {
                //print_r($order_details[$i]);
                //echo "<br/>Type_id = " . $order_details[$i]["type_id"] . " item_id = " . $order_details[$i]["item_id"];
                /*
                  echo "<pre>";
                  print_r($order_details[$i]);
                  echo "</pre>";
                 */
                $this->order_id = $order_id;


                $tbl_order_details = "";
                $query = "";
                //echo "<br/>order_type_id = " . $this->order_type_id;
                if (isset($order_details[$i]["pooja_id"])) {
                    //$tbl_orderuiry_details = $this->tbl_order_pooja;
                    //$tbl_orderuiry_col = "pooja_id = " . $this->$product_id;
                    $this->pooja_id = $order_details[$i]["pooja_id"];
                    $query = $this->getPoojaQuery($order_details[$i]);
                } elseif (isset($order_details[$i]["product_id"])) {
                    //$tbl_orderuiry_details = $this->tbl_order_product;
                    //$tbl_orderuiry_col = "order_type_id = " . $this->$order_type_id . ", product_id = " . $this->$product_id;
                    $this->order_type_id = $order_details[$i]["enq_type_id"];
                    $this->product_id = $order_details[$i]["product_id"];
                    $this->quantity = $order_details[$i]["quantity"];
                    $this->actual_cost = $order_details[$i]["actual_cost"];
                    $this->selling_cost = $order_details[$i]["selling_cost"];
                    $this->discount = $order_details[$i]["discount"];
                    $this->transport_cost = $order_details[$i]["transport_cost"];
                    $this->tax_id = $order_details[$i]["tax_id"];

                    $query = $this->getProductQuery($order_details[$i]);
                } elseif (isset($order_details[$i]["vendor_id"])) {
                    //$tbl_orderuiry_details = $this->tbl_order_vendor;
                    //$tbl_orderuiry_col = "order_type_id = " . $this->$order_type_id . ", product_id = " . $this->$product_id;

                    $this->order_type_id = $order_details[$i]["enq_type_id"];
                    $this->vendor_id = $order_details[$i]["vendor_id"];
                    $this->quantity = $order_details[$i]["quantity"];
                    $this->actual_cost = $order_details[$i]["actual_cost"];
                    $this->selling_cost = $order_details[$i]["selling_cost"];
                    $this->discount = $order_details[$i]["discount"];
                    $this->transport_cost = $order_details[$i]["transport_cost"];
                    $this->tax_id = $order_details[$i]["tax_id"];



                    $query = $this->getVendorQuery($order_details[$i]);
                }

                if (strlen($query) > 0) {


                    $query = "INSERT INTO " . $query;


                    $insert_row = 0;
                    $last_id = 0;
                    //echo "<br/>New order Details Query = " . $query;
                    $insert_row = $this->conn->query($query);
                    //echo "<br/>New order Details Query = " . $query . " result = $insert_row <br/>";

                    if ($insert_row) {

                        $last_id = $this->conn->insert_id;
                        $this->SuccessMsg = "New Order Added Successfully";
                        $log->logMsg("Success : New Order Detail Id = " . $last_id);
                        //printf("Add User Error message: %s\n", $mysqli->error);
                    } else {
                        $Error_msg = $this->conn->error;
                        $this->ErrorMsg = "Error : Fail To Add Order";
                        $log->LogError("New Order Detail add Error = " . $Error_msg . " Query = $query");
                        echo "New Order Detail add Error = " . $Error_msg;
                    }
                } else {
                    echo "No Query";
                }
            }
        }







        return $last_id;
    }

    public function getPoojaQuery() {

        //$row

        $query = " " . $this->tbl_order_pooja
                . " set order_id = '" . $this->order_id
                . "', pooja_id = '" . $this->pooja_id
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', status = 0";

        //echo "<br/> into getPoojaQuery Query = $query";
        return $query;
    }

    public function getProductQuery() {    // order_type_id = " . $this->$order_type_id . ", product_id = " . $this->$product_id;
        $query = " " . $this->tbl_order_product
                . " set order_id = '" . $this->order_id
                . "', order_type_id = '" . $this->order_type_id
                . "', product_id = '" . $this->product_id
                . "', quantity = '" . $this->quantity
                . "', actual_cost = '" . $this->actual_cost
                . "', selling_cost = '" . $this->selling_cost
                . "', discount = '" . $this->discount
                . "', transport_cost = '" . $this->transport_cost
                . "', tax_id = '" . $this->tax_id
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', status = 0";

        //echo "<br/> into getProductQuery Query = $query";

        return $query;
    }

    public function getVendorQuery() {    // order_type_id = " . $this->$order_type_id . ", product_id = " . $this->$product_id;
        $query = " " . $this->tbl_order_vendor
                . " set order_id = '" . $this->order_id
                . "', order_type_id = '" . $this->order_type_id // Vendor is different than product only vendor type is defined 
                . "', vendor_id = '" . $this->vendor_id
                . "', quantity = '" . $this->quantity
                . "', actual_cost = '" . $this->actual_cost
                . "', selling_cost = '" . $this->selling_cost
                . "', discount = '" . $this->discount
                . "', transport_cost = '" . $this->transport_cost
                . "', tax_id = '" . $this->tax_id
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', status = 0";

        //echo "<br/> into getVendorQuery Query = $query";

        return $query;
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    public function listOrderByCustId($cust_id) {

        $query = "Select * from " . $this->tbl_order . "  where cust_id = $cust_id and status = 0 order by id desc";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listOrderById($id) {

        $query = "Select * from " . $this->tbl_order . "  where id = $id and status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listOrderPoojaDetails($order_id) {

        $result_arr = array();


        //$query = "Select * from ".$this->tbl_order_pooja."  where order_id = $order_id and status = 0";//echo "<br/>Query = $query<br/>"; 
        $query = "SELECT * FROM " . ENQ_POOJA . " ep INNER JOIN " . POOJA_MASTER . " pm ON pm.id = ep.pooja_id and ep.order_id=$order_id";
        //echo "<br/>Query = $query<br/>"; 
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listorderDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

    public function listOrderProductDetails($order_id) {

        $result_arr = array();

        //$query = "Select * from ".$this->tbl_order_product."  where order_id = $order_id and status = 0";//echo "<br/>Query = $query<br/>"; 
        $query = "SELECT * FROM " . ENQ_PRODUCT . " ep INNER JOIN " . PRODUCT_MASTER . " pm ON pm.id = ep.product_id and ep.order_id=$order_id";
        //echo "<br/>Query = $query<br/>"; 
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listorderDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

    public function listOrderVendorDetails($order_id) {

        $result_arr = array();

        //$query = "Select * from ".$this->tbl_order_vendor."  where order_id = $order_id and status = 0";//echo "<br/>Query = $query<br/>"; 
        $query = "SELECT * FROM " . ENQ_VENDOR . " ep INNER JOIN " . VENDOR_TYPE_MASTER . " pm ON pm.id = ep.order_type_id and ep.order_id=$order_id";
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listorderDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

    public function getOrderDetailsByCustId($cust_id) {

        $tbl_rows = array();

        $orderData = $this->listOrderByCustId($cust_id);
        $count = 0;

        while ($Order_row = $orderData->fetch_assoc()) {

            $order_id = $Order_row["id"];
            $result = $this->getOrderDetailsByOrderId($order_id);
            if (!empty($result)) {
                array_push($tbl_rows, $Order_row);
                for ($i = 0; $i < count($result); $i++) {
                    array_push($tbl_rows, $result[$i]);
                }
            }
        }


        return $tbl_rows;
    }

    public function getOrderDetailsByOrderId($order_id) {

        $tbl_rows = array();

        $OrderPoojaDetailsData = $this->listOrderPoojaDetails($order_id);
        while ($Order_Pooja_details_row = $OrderPoojaDetailsData->fetch_assoc()) {
            //echo "<br/> order Pooja Details id = " . $Order_Pooja_details_row["id"] . "<br/>";
            array_push($tbl_rows, $Order_Pooja_details_row);
            //print_r($Order_Pooja_details_row);
            $content_flag++;
        }

        $OrderProductDetailsData = $this->listOrderProductDetails($order_id);
        while ($Order_Product_details_row = $OrderProductDetailsData->fetch_assoc()) {
            //echo "<br/> order Product Details id = " . $Order_Product_details_row["id"] . "<br/>";
            array_push($tbl_rows, $Order_Product_details_row);
            //print_r($Order_Product_details_row);
            $content_flag++;
        }

        $OrderVendorDetailsData = $this->listOrderVendorDetails($order_id);
        while ($Order_Vendor_details_row = $OrderVendorDetailsData->fetch_assoc()) {
            //echo "<br/> order Vendor Details id = " . $Order_Vendor_details_row["id"] . "<br/>";
            array_push($tbl_rows, $Order_Vendor_details_row);
            //print_r($Order_Vendor_details_row);
            $content_flag++;
        }


        return $tbl_rows;
    }

    public function addNewFollowup() {

        global $log;
        $log->logMsg("Add New FollowUp");

        //if (empty($values)) return FALSE;
        //if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
        //if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
        //global $PDO;
        // to get time-stamp for 'created' field
        $this->inputValidation();



        if (!$this->order_id > 0) {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Please Select Order.</li>";
            //$order->add_date = "0000-00-00 00:00:00";
        }

        if (!strlen($this->comment) > 0) {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Please Enter Comments.</li>";
            //$order->add_date = "0000-00-00 00:00:00";
        }



        $add_date = $this->add_date;    //$_POST['current_date'] . " " . $_POST['current_time'];
        $next_date = $this->next_date;  //$_POST['next_date'] . " " . $_POST['next_time'];

        if (DateTime::createFromFormat('d-m-Y H:i a', $this->add_date) !== FALSE) {
            // it's a date
            $this->add_date = DateTime::createFromFormat('d-m-Y H:i a', $this->add_date)->format('Y-m-d H:i:s');
        } else {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Comment date and Time is not proper</li>";
            //$order->add_date = "0000-00-00 00:00:00";
        }


        if (DateTime::createFromFormat('d-m-Y H:i a', $this->next_date) !== FALSE) {
            // it's a date
            $this->next_date = DateTime::createFromFormat('d-m-Y H:i a', $this->next_date)->format('Y-m-d H:i:s');
        } else {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Next Date And Time is not proper</li>";
        }

        if (!$this->order_status > 0) {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Please Select Order Status.</li>";
            //$order->add_date = "0000-00-00 00:00:00";
        }

        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add Order Followup<ul>" . $this->ErrorMsg . "</ul>";
            $this->ErrorFlag = FALSE;
            return 0;
        }


        $this->getTimestamp();
        $query = "INSERT INTO " . $this->tbl_order_followup
                . " set order_id = '" . $this->order_id
                . "', comment = '" . $this->comment
                . "', add_date = '" . $this->add_date
                . "', next_date = '" . $this->next_date
                . "', add_user = '" . $_SESSION['login_id']
                . "', status = 0";

        $insert_row = 0;
        $last_id = 0;

        $insert_row = $this->conn->query($query);
        // echo "<br/>New order = " . $query . " <br/>result = $insert_row";

        if ($insert_row) {

            $last_id = $this->conn->insert_id;

            $order_status_result = $this->setOrderStatus($this->order_id, $this->order_status, $this->next_date);
            if (!$order_status_result) {
                return 0;
            }

            $this->SuccessMsg = "Your Order Followup Added Successfully";
            $log->logMsg("Success : New Order Id = " . $last_id);
            $order_details_id = $this->addOrderDetails($this->cust_id, $last_id, $this->cust_order_id);
            $last_id = $order_details_id;
            //unset($_SESSION[$this->cust_order_id]);
            //printf("Add User Error message: %s\n", $mysqli->error);
        } else {
            $Error_msg = $this->conn->error;
            $this->ErrorMsg = "Error : Fail To Add Order Followup";
            $log->LogError("New Order add Error = " . $Error_msg . " Query = $query");
        }

        return $last_id;
    }

    public function setOrderStatus($order_id, $order_status, $next_date) {

        $query = "update " . $this->tbl_order . " set followup_date = '$next_date', order_status = $order_status where id = $order_id";
        $insert_row = $this->conn->query($query);
        if (!$insert_row) {
            $Error_msg = $this->conn->error;
            $this->ErrorFlag = TRUE;
            echo "query = $query";
            $this->ErrorMsg .= "<br/>Error : Fail To Update Order Status <br/>SqlErr: = $Error_msg";
        }
        /*
          if ($order_status == 7) {
          $insert_row = $this->convertIntoOrder();
          }
         */

        return $insert_row;
    }

    public function convertIntoOrder($order_id, $order_status, $next_date) {

        /*
          $query = "update " . $this->tbl_order . " set followup_date = '$next_date', order_status = $order_status where id = $order_id";
          $insert_row = $this->conn->query($query);
          if (!$insert_row) {
          $Error_msg = $this->conn->error;
          $this->ErrorFlag = TRUE;
          echo "query = $query";
          $this->ErrorMsg .= "<br/>Error : Fail To Update Order Status <br/>SqlErr: = $Error_msg";
          }
         */
    }

    public function getOrderFollowup($order_id) {

        $result_arr = array();

//        $query = "Select * from ".$this->tbl_order_vendor."  where order_id = $order_id and status = 0";//echo "<br/>Query = $query<br/>"; 
//        SELECT f.*,a.name FROM `tbl_order_followup` f inner join tbl_admin a on f.order_id = 18 and f.status = 0 and (f.add_user = a.id) ORDER BY add_date desc
        //$query = "SELECT * FROM " . ENQ_VENDOR . " ep INNER JOIN " . VENDOR_TYPE_MASTER . " pm ON pm.id = ep.order_type_id and ep.order_id=$order_id";
        $query = "SELECT f.*,a.name,a.thumb_img FROM " . $this->tbl_order_followup . " f inner join " . Admin . " a on f.order_id = $order_id and f.status = 0 and (f.add_user = a.id) ORDER BY id desc";
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listorderDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

}
