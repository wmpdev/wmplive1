<?php

class Admin {

    var $username;
    var $pwd;
    var $database;
    var $tablename = "tbl_admin";
    var $connection;
    var $random_key;
    var $error_message;
    var $SuccessMsg;
    var $ErrorMsg;

    public function __construct($db) {
        $this->connection = $db;
    }

    function GetErrorMessage() {
        if (empty($this->error_message)) {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }

    function HandleError($err) {
        $this->error_message .= $err . "\r\n";
    }

    function HandleDBError($err) {
        $this->HandleError($err . "\r\n mysqlerror:" . mysql_error());
    }

    function DBinitialize($host, $uname, $pwd, $database, $tablename) {
        $this->db_host = $host;
        $this->username = $uname;
        $this->pwd = $pwd;
        $this->database = $database;
        $this->tablename = $tablename;
    }

    function SetRandomKey($key) {
        $this->random_key = $key;
    }

    function Login() {
        if (empty($this->username)) {
            $this->HandleError("Username is empty!");
            return false;
        }

        if (empty($this->pwd)) {
            $this->HandleError("Password is empty!");
            return false;
        }

        $username = trim($this->username);
        $password = trim($this->pwd);

        if (!isset($_SESSION)) {
            session_start();
        }
        if (!$this->query_db($username, $password)) {
            return false;
        }

        $_SESSION[$this->GetLoginSessionVar()] = $username;

        return true;
    }

    function CheckLogin() {
        if (!isset($_SESSION)) {
            //ini_set('session.gc_maxlifetime', 3600); //set session for 1hr
            // server should keep session data for AT LEAST 1 hour
            ini_set('session.gc_maxlifetime', 3600);
            // each client should remember their session id for EXACTLY 1 hour
            session_set_cookie_params(3600);

            session_start();
        }

        $sessionvar = $this->GetLoginSessionVar();

        if (empty($_SESSION[$sessionvar])) {
            return false;
        }
        return true;
    }

    function GetSelfScript() {
        return htmlentities($_SERVER['PHP_SELF']);
    }

    function SafeDisplay($value_name) {
        if (empty($_POST[$value_name])) {
            return'';
        }
        return htmlentities($_POST[$value_name]);
    }

    function urlredirection($url) {
        header("Location: $url");
        exit;
    }

    function GetLoginSessionVar() {
        $retvar = md5($this->random_key);
        $retvar = 'usr_' . substr($retvar, 0, 10);
        return $retvar;
    }

    function query_db($username, $password) {
        /*
          if(!$this->databaselogincheck())
          {
          $this->HandleError("Database login failed!");
          return false;
          }
         */
        //$username = $this->SanitizeForSQL($username);
        //$username = mysql_real_escape_string($username);

        $username = $this->connection->real_escape_string($username);

        $query = "Select * from $this->tablename where username='$username' and password='$password'";
        $result = $this->connection->query($query);
        $row_cnt = $result->num_rows;
        //echo " Login Query = $query, Row_cnt = $row_cnt, name = ".$_SESSION['name_of_user'].", id = ".$_SESSION['userid'];
        //$result = mysql_query($qry,$this->connection);

        if (!$result || $row_cnt <= 0) {
            $this->ErrorMsg = "Invalid User Name Or Password.";
            $this->HandleError("Error logging in. The username or password does not match");
            return false;
        }

        $row = $result->fetch_assoc();
        //$row = mysql_fetch_assoc($result);
        $_SESSION['login_name'] = $row['name'];
        $_SESSION['login_id'] = $row['id'];
        if (strlen($row['thumb_img']) > 0) {
            $_SESSION['login_img_path'] = $row['thumb_img'];
        } else {
            $_SESSION['login_img_path'] = "assets/img/admin.png";
        }
        //echo " Login Query = $query, Row_cnt = $row_cnt, name = ".$_SESSION['name_of_user'].", id = ".$_SESSION['userid'];

        return true;
    }

    function LogOut() {
        session_start();

        $sessionvar = $this->GetLoginSessionVar();

        $_SESSION[$sessionvar] = NULL;

        unset($_SESSION[$sessionvar]);
        unset($_SESSION);
        /*         * ** New Logout **** */
        unset($_SESSION['login_name']);
        unset($_SESSION['login_id']);
        //unset($_SESSION['login_id']);
        session_destroy();
        /*         * ** New Logout End **** */
    }

    function databaselogincheck() {

        //$this->connection = mysql_connect($this->db_host,$this->username,$this->pwd);
        /*
          if(!$this->connection)
          {
          $this->HandleDBError("Database Login failed! Please make sure that the DB login credentials provided are correct");
          return false;
          }
          if(!mysql_select_db($this->database, $this->connection))
          {
          $this->HandleDBError('Failed to select database: '.$this->database.' Please make sure that the database name provided is correct');
          return false;
          }
         */

        if (!mysql_query("SET NAMES 'UTF8'", $this->connection)) {
            $this->HandleDBError('Error setting utf8 encoding');
            return false;
        }
        return true;
    }

    function SanitizeForSQL($str) {
        if (function_exists("mysql_real_escape_string")) {
            $ret_str = mysql_real_escape_string($str);
        } else {
            $ret_str = addslashes($str);
        }
        return $ret_str;
    }

    function Sanitize($str, $remove_nl = true) {
        $str = $this->StripSlashes($str);

        if ($remove_nl) {
            $injections = array('/(\n+)/i',
                '/(\r+)/i',
                '/(\t+)/i',
                '/(%0A+)/i',
                '/(%0D+)/i',
                '/(%08+)/i',
                '/(%09+)/i'
            );
            $str = preg_replace($injections, '', $str);
        }

        return $str;
    }

    function StripSlashes($str) {
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }
        return $str;
    }

}
