<?php

class User {

    private static $userId;
    private $conn;
    private $tbl_user = "tbl_customer";
    // object properties
    public $id;
    public $first_name = "";
    public $last_name = "";
    public $email_1 = "";
    public $email_2 = "";
    public $phone_1 = "";
    public $phone_2 = "";
    public $address_1 = "";
    public $address_2 = "";
    public $country_id = 0;
    public $state_id = 0;
    public $city_id = 0;
    public $area_id = 0;
    public $pincode = 0;
    public $cast_id = 0;
    public $comment = "";
    public $date = "";
    public $add_date = "";
    public $mod_date = "";
    public $add_user = 0;
    public $mod_user = 0;
    public $status = 0;
    public $timestamp = "";
    public $userMsg = "";
    public $SuccessMsg = "";
    public $ErrorMsg = "";
    public $ErrorFlag = 0;

    /**
     * Gets the current logged in user.
     *
     * @return object
     */
    public static function getUserLoggedIn() {
        $userId = trim(Session::get('user'));
        if (empty($userId))
            return NULL;

        return static::getUser($userId);
    }

    public function __construct($db) {
        $this->conn = $db;
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    public function addNewUser() {

        global $log;
        $log->logMsg("Add New User");

        //if (empty($values)) return FALSE;
        //if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
        //if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
        //global $PDO;
        // to get time-stamp for 'created' field
        $this->inputValidation();
        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add User";
            return 0;
        }
        $this->getTimestamp();

        /*
          //write query
          $query = "INSERT INTO " . $this->table_name . " SET first_name = ?, last_name = ?, email_1 = ?, email_2 = ?, phone_1 = ?, phone_2 = ?, address_1 = ?, address_2 = ?, country_id = ?, state_id = ?, city_id = ?, area_id = ?, pincode = ?, cast_id = ?, other = ?, date = ?, add_date = ?, mod_date = ?, status = ?";
          $stmt = $this->conn->prepare($query);

          $stmt->bindParam(1, $this->first_name);
          $stmt->bindParam(2, $this->last_name);
         */



        $query = "INSERT INTO " . $this->tbl_user
                . " set first_name = '" . $this->first_name
                . "', last_name = '" . $this->last_name
                . "', email_1 = '" . $this->email_1
                . "', email_2 = '" . $this->email_2
                . "', phone_1 = '" . $this->phone_1
                . "', phone_2 = '" . $this->phone_2
                . "', address_1 = '" . $this->address_1
                . "', country_id = '" . $this->country_id
                . "', state_id = '" . $this->state_id
                . "', city_id = '" . $this->city_id
                . "', area_id = '" . $this->area_id
                . "', pincode = '" . $this->pincode
                . "', caste_id = '" . $this->cast_id
                . "', reference_id = '" . $this->reference_id
                . "', comment = '" . $this->comment
                . "', date = '" . date('Y-m-d')
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', status = 0";



        $last_id = 0;
        if ($insert_row = $this->conn->query($query)) {

            $last_id = $this->conn->insert_id;
            $this->SuccessMsg = "New User Added Successfully";
            $log->logMsg("Success : New User Id = " . $last_id);
            //printf("Add User Error message: %s\n", $mysqli->error);
        } else {
            $Error_msg = $this->conn->error;
            $this->ErrorMsg = "Error : Fail To Add User";
            $log->LogError("New User add Error = " . $Error_msg . " Query = $query");
        }
        // else echo "<br/>Add User Error message:".$this->conn->error."<br/>";
        //echo "User Add query = $query";

        return $last_id;


        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
    }

    public function updateUser($id) {
        global $log;

        $log->logMsg("Update User $id");
        $this->id = $id;

        $this->inputValidation();
        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Update User";
            return 0;
        }
        $this->getTimestamp();

        $query = "update " . $this->tbl_user
                . " set first_name = '" . $this->first_name
                . "', last_name = '" . $this->last_name
                . "', email_1 = '" . $this->email_1
                . "', email_2 = '" . $this->email_2
                . "', phone_1 = '" . $this->phone_1
                . "', phone_2 = '" . $this->phone_2
                . "', address_1 = '" . $this->address_1
                . "', country_id = '" . $this->country_id
                . "', state_id = '" . $this->state_id
                . "', city_id = '" . $this->city_id
                . "', area_id = '" . $this->area_id
                . "', pincode = '" . $this->pincode
                . "', caste_id = '" . $this->cast_id
                . "', reference_id = '" . $this->reference_id
                . "', comment = '" . $this->comment
                . "', date = '" . date('Y-m-d')
                . "', mod_date = '" . $this->timestamp
                . "', mod_user = '" . $_SESSION['login_id']
                . "', status = 0";


        $query .= " where id = " . $this->id;




        //$last_id = 0;
        if ($insert_row = $this->conn->query($query)) {

            //$last_id = $this->conn->insert_id;
            $this->SuccessMsg = "User Updated Successfully";
            $log->logMsg("Success : New User Id = " . $last_id);
            $log->logMsg("Query for Update = " . $query);
            //printf("Add User Error message: %s\n", $mysqli->error);
        } else {
            $Error_msg = $this->conn->error;
            $this->ErrorMsg = "Error : Fail To Update User";
            $log->LogError("New User add Error = " . $Error_msg . " Query = $query");
        }
        // else echo "<br/>Add User Error message:".$this->conn->error."<br/>";
        //echo "User Add query = $query";

        return $insert_row;


        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
    }

    public function listUsers($page = 0, $total_rec = 0) {

        $status = "";
        $this->getTimestamp();
        $limit = "";
        //$this->country_name = $this->conn->real_escape_string($this->country_name);                


        $query = "Select * from " . $this->tbl_user . "  where status = 0 " . $limit;
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function getUserEmaillist($page = 0, $total_rec = 0) {

        $status = "";
        //$this->getTimestamp();
        $limit = "";
        //$this->country_name = $this->conn->real_escape_string($this->country_name);                


        $query = "Select email_1,email_2 from " . $this->tbl_user . "  where status = 0 " . $limit;
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    // used for the 'created' field when creating a product
    function getTimestamp() {
        //date_default_timezone_set('Asia/Manila');
        $this->timestamp = date('Y-m-d H:i:s');
    }

    function inputValidation() {
        $this->ErrorFlag = 0;

        if (strlen($this->phone_1) > 0)
            $this->phone_1 = str_replace(array('+', '(', ')', ' ', '-'), '', $this->phone_1);
    }

    /**
     * Checks whether user exists.
     *
     * @param string $userId
     *
     * @return object
     */
    public function getUserByID($userId = '') {

        $status = "";
        $this->getTimestamp();

        //$this->country_name = $this->conn->real_escape_string($this->country_name);                
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_user . "  where id = $userId and status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;

        /*
          if (empty($userId)) return NULL;

          global $PDO;
          $statement = $PDO->prepare('
          SELECT *
          FROM ' . USER_TABLE . '
          WHERE id = ?
          LIMIT 1
          ');
          $statement->execute(array($userId));

          if ($statement->rowCount() > 0) {
          static::$userId = $userId;

          return $statement->fetch(PDO::FETCH_OBJ);
          }

          return NULL;
         */
    }

    public function searchByEmailAndPhone($email = '', $phone = '') {

        $status = "";
        //$this->getTimestamp();
        //$this->country_name = $this->conn->real_escape_string($this->country_name);                
        //MySqli Insert Query
        //echo " $email $phone ";
        $where_cond_email = "";
        $where_cond_phone = "";

        if (strlen(trim($email)) > 0) {
            $where_cond_email = " email_1 like '%" . $email . "%' or email_2 like '%" . $email . "%' ";
        }

        if (strlen(trim($phone)) > 0) {
            $where_cond_phone = " phone_1 like '%" . $phone . "%' or phone_2 like '%" . $phone . "%' ";
            if (strlen(trim($where_cond_email)) > 0) $where_cond_phone = "or " . $where_cond_phone;
        }


        $query = "Select * from " . $this->tbl_user . " where $where_cond_email $where_cond_phone and status = 0";
        //echo "Query = $query and Result = $result";
        $result = $this->conn->query($query);

        return $result;
    }

    /**
     * Gets all the information about the user.
     *
     * @param string $userId
     *
     * @return object
     */
    public static function getUserComplete($userId = '') {
        if (empty($userId))
            return NULL;

        global $PDO;
        $statement = $PDO->prepare('
			SELECT u.first_name, u.last_name, u.mobile, u.email, u.last_login_ip, u.active, sch.name AS school_name, c
			.name AS
			 city_name
			FROM ' . USER_TABLE . ' AS u
			INNER JOIN ' . SCHOOL_TABLE . ' AS sch ON (u.school = sch.id)
			INNER JOIN ' . CITY_TABLE . ' AS c ON (u.city = c.id)
			WHERE u.id = ?
			LIMIT 1
		');

        $statement->execute(array($userId));

        return ($statement->rowCount() > 0) ? $statement->fetch(PDO::FETCH_OBJ) : null;
    }

    /**
     * Gets all the information about the user.
     *
     * @param string $userId
     *
     * @return object
     */
    public static function getUserAllComplete($limits = true, $offset = 0, $limit = 30) {
        global $PDO;

        $statement = null;
        if ($limits) {
            $statement = $PDO->prepare('
				SELECT u.id, u.first_name, u.last_name, u.email, u.last_login_ip, u.active, sch.name AS school_name, c.name AS city_name
				FROM ' . USER_TABLE . ' AS u
				INNER JOIN ' . SCHOOL_TABLE . ' AS sch ON (u.school = sch.id)
				INNER JOIN ' . CITY_TABLE . ' AS c ON (u.city = c.id)
				LIMIT :offset, :limit
			');

            $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
            $statement->bindValue(':limit', $limit, PDO::PARAM_INT);
        } else {
            $statement = $PDO->prepare('
				SELECT u.id, u.first_name, u.last_name, u.email, u.last_login_ip, u.active, sch.name AS school_name, c.name AS city_name
				FROM ' . USER_TABLE . ' AS u
				INNER JOIN ' . SCHOOL_TABLE . ' AS sch ON (u.school = sch.id)
				INNER JOIN ' . CITY_TABLE . ' AS c ON (u.city = c.id)
			');
        }

        $statement->execute();

        return ($statement->rowCount() > 0) ? $statement : null;
    }

    /**
     * Checks whether user with email exists
     *
     * @param string $email Email address
     *
     * @return bool Returns true, if email exists
     */
    public static function checkUserExistsByEmail($email = '') {
        if (empty($email))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;
        $statement = $PDO->prepare('
			SELECT email
			FROM ' . USER_TABLE . '
			WHERE email = ?
			LIMIT 1
		');
        $statement->execute(array($email));

        return ($statement->rowCount() > 0);
    }

    /**
     * Checks whether user with email and password exists
     *
     * @param string $email    Email address
     * @param string $password Password
     *
     * @return bool Returns true, if user exists
     */
    public static function checkUserExistsByEmailAndPassword($email = '', $password = '') {
        if (empty($email) || empty($password))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        /* echo "SELECT email
          FROM ' . USER_TABLE . '
          WHERE email = ? && password = SHA1(?)
          LIMIT 1"; */

        global $PDO;
        $statement = $PDO->prepare('
			SELECT email
			FROM ' . USER_TABLE . '
			WHERE email = ? && password = SHA1(?)
			LIMIT 1
		');
        $statement->execute(array($email, $password));

        return ($statement->rowCount() > 0);
    }

    /**
     * Checks the activation status of the given user
     *
     * @param string $email    Email address
     * @param string $password Password
     *
     * @return bool Activation status
     */
    public static function checkActivated($email = '', $password = '') {
        if (empty($email) || empty($password))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;
        $statement = $PDO->prepare('
			SELECT active
			FROM ' . USER_TABLE . '
			WHERE email = ? && password = SHA1(?)
			LIMIT 1
		');

        $statement->execute(array($email, $password));
        if ($statement->rowCount() == 0)
            return FALSE;

        return ($statement->fetch(PDO::FETCH_OBJ)->active != 0);
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     */
    public static function registerUser($values = array()) {
        if (empty($values))
            return FALSE;
        if (!Booklet::isRegistrationKeyValid($values['registration-key']))
            return FALSE;
        if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;


        $statement = $PDO->prepare(
                'INSERT INTO ' . USER_TABLE . '
			(first_name, last_name, email,
			password, city, mobile,
			role, school, board,
			grade, active, activation_hash,
			created_at) ' .
                'VALUES (?, ?, ?, SHA1(?), ?, ?, ?, ?, ?, ?, 0, SHA1(UNIX_TIMESTAMP()), UNIX_TIMESTAMP())'
        );

        $userSuccess = $statement->execute(array(
            ucwords($values['first-name']),
            ucwords($values['last-name']),
            $values['email'],
            $values['password'],
            $values['city'],
            $values['mobile-number'],
            $values['role'],
            $values['school-id'],
            BoardGrade::getBoardIdFromSlug($values['board']),
            BoardGrade::getGradeIdFromSlug($values['grade'])
        ));

        $userId = $PDO->lastInsertId();

        return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
    }

    /**
     * Performs logout
     */
    public static function logout() {
        static::$userId = NULL;
    }

    /**
     * Sends an activation email to the user
     *
     * @param $userId User ID or Email
     *
     * @return bool If true, the mail was sent successfully
     */
    public static function sendActivationEmail($userIdOrEmail) {
        if (empty($userIdOrEmail))
            return FALSE;

        $user = static::getUser($userIdOrEmail);
        /* echo "<pre>";
          print_r($user);
          echo "</pre>"; */
        if ($user == NULL) {
            if (!filter_var($userIdOrEmail, FILTER_VALIDATE_EMAIL))
                return NULL;
            $user = static::getUserByEmail($userIdOrEmail);
            if ($user == NULL)
                return NULL;
        }
        $grade = $user->board;
        if ($grade == 1) {
            $gradeName = "ICSE";
        } else {
            $gradeName = "ISC";
        }
//echo "########".$gradeName;
        $to = $user->email;
        $from = 'TeacherNI <connect@teacherni.com>';

        $subject = 'TeacherNI Booklets Activation Mail';
        /* $body = '
          <h3>Greetings, ' . $user->first_name . '</h3>
          <p>Thank you for purchasing our booklet. We assure you that you\'re going to have an amazing learning experience ahead!</p>
          <p>Your account is created but it\'s only short of activation. Click the link below to activate your
          account.</p>
          <p><a href = "http://teacherni.com/testprep_new/activate.php?hash=' . $user->activation_hash . '&email=' .
          $user->email . '">http://teacherni.com/testprep_new/activate.php?hash=' . $user->activation_hash . '&email=' .
          $user->email . '</a></p>
          <p>Regards,<br />TeacherNI Team</p>

          '; */

        $body = '<table align="center" border="0" cellpadding="0" cellspacing="0" width=
"700">
<tr><td>&nbsp;</td></tr>
<tr><td><img height="112" src="http://www.teacherni.com/ibee/images/registration_success.jpg" width="700"></td></tr>
<tr>
<td style="background-color: #F4F5F5; color: #58595b; font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 20px">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="92%">
<tr><td align="left" valign="top">&nbsp;</td></tr>
<tr>
<h3 style="margin-left:20px;">Greetings, ' . $user->first_name . '</h3>

</tr>
<tr>
<td align="left" valign="top">&nbsp;</td>
</tr>
<tr>
<td align="left"  valign="top">

<table border="0" cellpadding="0" cellspacing="0">

<b>Congratulations! You have successfully registered with ' . $gradeName . ' Test Preparation Predictive </br>
Question Paper 2015.</b>
</br></br>
			<p>Thank you for purchasing our booklet. We assure you that you are going to have an amazing learning experience ahead!</p>
			<p>Your account is created but it is only short of activation. Click the link below to activate your
			account.</p>
<p style="color: #0000FF; font-size: 11pt">
<a href = "http://teacherni.com/testprep_new/activate.php?hash=' . $user->activation_hash . '&email=' . $user->email . '">http://teacherni.com/testprep_new/activate.php?hash=' . $user->activation_hash . '&email=' . $user->email . '</a>
</p>
			<p>Regards,<br />TeacherNI Team</p>
	</table>
</td>
</tr>

<tr><td align="left" valign="top">&nbsp;</td></tr>
<tr>
<td align="left" valign="top">

Subscribe to our TeacherNI annual packages for ICSE/ISC and enjoy unlimited access to all our features.<br />Call us at +91 76667 12345 / 022 3208 3398, email at <a href="mailto:testprep@teacherni.com">testprep@teacherni.com</a> or<br />sign-up directly on: <a href="http://teacherni.com">www.teacherni.com</a>
</td>
</tr>
<tr><td align="left" valign="top">&nbsp;</td></tr>
<tr><td align="center"><img height="4" src="http://www.teacherni.com/ibee/images/divider.png" width="650"></td></tr>
</table>
</td>
</tr>

<tr>
<td style="background-color: #F4F5F5">
<table align="left" border="0" cellpadding="0" cellspacing="0"
width="95%">
<tr>
<td align="left" width="333">
<a href="http://teacherni.com/" target=
"_blank"><img border="0" height="103" src=
"http://www.teacherni.com/ibee/images/logo2.jpg" width="333"></a>
</td>

<td>&nbsp;</td>

<td align="right" style="padding-top:35px;" valign=
"middle"><img border="0" height="18" src="http://www.teacherni.com/ibee/images/social.png" usemap="#Map" width="103">
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td align="left" style=
"font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#58595b; text-align:justify;"
valign="top"><strong>Disclaimer-*</strong> This email is intended
solely for the addressee(s) and may be legally privileged and/or
confidential. Any review, retransmission, dissemination or other
use is prohibited. If received in error please delete it and all
copies of it from your system, destroy any hard copies of it and
contact the sender. Any unauthorized use or disclosure may be
unlawful. Any opinion expressed in this email may not necessarily
reflect the opinions of S &amp; D Emerging Entrepreneurs. The
information is not warranted as to completeness or accuracy and is
subject to change without notice.</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
</table>';

//echo $body;exit;
        $mail = new Mail();
        $mail->to($to);
        $mail->from($from);
        $mail->subject($subject);
        $mail->body($body);
//echo $this->to; echo $this->subject; echo $this->body;
//exit;
        return $mail->send();
    }

    /**
     * Checks whether user with email exists.
     *
     * @param string $userId Email address
     *
     * @return object
     */
    public static function getUserByEmail($email = '') {
        if (empty($email))
            return NULL;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;
        $statement = $PDO->prepare('
			SELECT *
			FROM ' . USER_TABLE . '
			WHERE email = ?
			LIMIT 1
		');
        $statement->execute(array($email));

        return ($statement->rowCount() > 0) ? $statement->fetch(PDO::FETCH_OBJ) : NULL;
    }

    /**
     * Sends an forgot password email to the user
     *
     * @param $userId User ID or Email
     *
     * @return bool If true, the mail was sent successfully
     */
    public static function sendForgotPasswordEmail($email) {
        if (empty($email))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return NULL;

        $user = static::getUserByEmail($email);
        if ($user == NULL)
            return NULL;
        $to = $user->email;
        $from = 'TeacherNI <connect@teacherni.com>';
        $subject = 'TeacherNI Booklets Forgot Password';
        $body = '
			<h3>Greetings, ' . $user->first_name . '</h3>
			<p>It seems you have sent us a request to reset your password. Just open the link below to continue.</p>
			<p><a href = "http://teacherni.com/testprep_new/reset.php?hash=' . $user->activation_hash . '&email=' .
                $user->email . '">http://teacherni.com/testprep_new/reset.php?hash=' . $user->activation_hash . '&email=' .
                $user->email . '</a></p>
			<p>Regards,<br />TeacherNI Team</p>
		';

        $mail = new Mail();
        $mail->to($to);
        $mail->from($from);
        $mail->subject($subject);
        $mail->body($body);

        return $mail->send();
    }

    /**
     * Activates the user using email address and hash
     *
     * @param $email Email address
     * @param $hash  Activation hash
     *
     * @return bool Returns true, if activated
     */
    public static function activateUser($email, $hash) {
        if (empty($email) || empty($hash))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;


        $statement = $PDO->prepare('
			UPDATE ' . USER_TABLE . '
			SET active = 1, activation_hash = SHA1(UNIX_TIMESTAMP())
			WHERE email = ? && activation_hash = ?
		');

        $statement->execute(array($email, $hash));

        return ($statement->rowCount() > 0);
    }

    /**
     * Deactivates the user using email address and hash
     *
     * @param $email Email address
     *
     * @return bool Returns true, if deactivated
     */
    public static function deactivateUser($email) {
        if (empty($email))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;
        $statement = $PDO->prepare('
			UPDATE ' . USER_TABLE . '
			SET active = 0, password = SHA1(RAND()), activation_hash = SHA1(UNIX_TIMESTAMP())
			WHERE email = ?
		');

        $statement->execute(array($email));

        return ($statement->rowCount() > 0);
    }

    /**
     * Resets the password of the user based on the email address and hash
     *
     * @param $email    Email address
     * @param $hash     Activation hash
     * @param $password Password
     *
     * @return bool Returns true if password is reset
     */
    public static function resetPassword($email, $hash, $password) {
        if (empty($email) || empty($hash) || empty($password))
            return FALSE;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return FALSE;

        global $PDO;
        $statement = $PDO->prepare('
			UPDATE ' . USER_TABLE . '
			SET active = 1, password = SHA1(?), activation_hash = SHA1(UNIX_TIMESTAMP())
			WHERE email = ? && activation_hash = ?
		');

        $statement->execute(array($password, $email, $hash));

        return ($statement->rowCount() > 0);
    }

    /**
     * Returns all users
     *
     * @param int $offset Start index of users
     * @param int $limit Maximum number of users per page
     *
     * @return array|null PDO object array of Users
     */
    public static function getAllUsers($limits = true, $offset = 0, $limit = 30) {
        global $PDO;

        $statement = null;
        if ($limits) {
            $statement = $PDO->prepare('
				SELECT *
				FROM ' . USER_TABLE . ' AS s
				LIMIT :offset, :limit
			');

            $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
            $statement->bindValue(':limit', $limit, PDO::PARAM_INT);
        } else {
            $statement = $PDO->prepare('
				SELECT *
				FROM ' . USER_TABLE . ' AS s
			');
        }

        $statement->execute();

        return ($statement->rowCount() > 0) ? $statement : null;
    }

    /**
     * Deletes users
     *
     * @param int $user User ID
     *
     * @return bool Returns true if user is deleted
     */
    public static function removeUser($user = '') {
        if (empty($user))
            return false;

        global $PDO;
        $statement = $PDO->prepare('
			DELETE FROM ' . USER_TABLE . '
			WHERE id = :user
		');

        $statement->bindValue(':user', $user, PDO::PARAM_INT);
        $statement->execute();

        return ($statement->rowCount() > 0);
    }

}
