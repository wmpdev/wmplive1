<?php

class Log {

    var $login_user;
    var $login_id;
    var $timestamp;
    var $file_activity = "Activity_log.html";
    var $file_error = "Error_log.html";
    
    
            
    var $database;
    var $tablename = "tbl_admin";
    var $connection;
    var $random_key;
    var $error_message;
	
    
     public function __construct($db){
        $this->connection = $db;
    }
    

    function LogMsg($msg){

        $this->PrepareMsg($msg);
        $this->writeInFile($this->file_activity);
        
    } 
    function LogError($msg){

        $this->PrepareMsg($msg);
        $this->writeInFile($this->file_activity);
        $this->writeInFile($this->file_error);
        
        
    } 
    function PrepareMsg($msg){
        $this->getLoginDetails();
        $this->log_message = "<br/>".$this->timestamp."|".$this->login_id."|".$this->login_user." = ".$msg;
    }    

    function writeInFile(){

        $myFile = "Activity_log.html";
        $fh = fopen($myFile, 'a') or die("can't open file");
        $stringData = $this->log_message;
        fwrite($fh, $stringData);
        fclose($fh);
        
    }
    
    
    

    function getLoginDetails(){
        
        $this->login_user = "No User";
        $this->login_id = "No User";
        
        if(isset($_SESSION["login_name"]))  $this->login_user = $_SESSION["login_name"];
        if(isset($_SESSION["login_id"]))  $this->login_id = $_SESSION["login_id"];
        //date_default_timezone_set('Asia/Manila');
        $this->timestamp = date('Y-m-d H:i:s');

    }
    
    
    function GetErrorMessage(){
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }   

    function HandleError($err){
        $this->error_message .= $err."\r\n";
    }
  
    function HandleDBError($err)
    {
        $this->HandleError($err."\r\n mysqlerror:".mysql_error());
    }
  
	 function DBinitialize($host,$uname,$pwd,$database,$tablename)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;
      
    }
   function SetRandomKey($key)
    {
        $this->random_key = $key;
    }
    
    
    
    
    
}



