<?php

class Master {

    private static $countryId;
    private $conn;
    private $tbl_reference = "tbl_reference_master";
    private $tbl_caste = "tbl_caste_master";
    private $tbl_enq_type = "tbl_enq_type_master";
    private $tbl_pooja = "tbl_pooja_master";
    private $tbl_service = "tbl_service_master";
    private $tbl_status = "tbl_status_master";
    private $tbl_monitor = "tbl_monitors_master";
    private $tbl_enq_puja = "tbl_enq_puja";
    //private $tbl_city = "tbl_city_master";
    //private $tbl_area = "tbl_area_master";
    // object properties
    public $reference_id;
    public $reference_name = "";
    public $caste_id;
    public $caste_name = "";
    public $add_date = "";
    public $mod_date = "";
    public $rec_type = 0;
    public $status = 0;
    public $timestamp = "";
    public $status_name = "";
    public $status_color = "";

    public function __construct($db) {
        $this->conn = $db;
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    // used for the 'created' field when creating a product
    function getTimestamp() {
        //date_default_timezone_set('Asia/Manila');
        $this->timestamp = date('Y-m-d H:i:s');
        return $this->timestamp;
    }

    public function list_reference() {

        $status = "";
        $this->getTimestamp();

        //$this->reference_name = $this->conn->real_escape_string($this->country_name); 
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_reference . "  where status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function list_caste() {

        $status = "";
        $this->getTimestamp();

        //$this->reference_name = $this->conn->real_escape_string($this->country_name); 
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_caste . "  where status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listService() {

        $status = "";
        $this->getTimestamp();

        //$this->reference_name = $this->conn->real_escape_string($this->country_name); 
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_service . "  where status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listStatusByType($type) {

        $status = "";
        $this->getTimestamp();

        //$this->reference_name = $this->conn->real_escape_string($this->country_name); 
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_status . "  where rec_type = $type and status = 0 order by status_priority";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listMonitorsByType($type) {

        $status = "";
        $this->getTimestamp();

        //$this->reference_name = $this->conn->real_escape_string($this->country_name); 
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_monitor . "  where rec_type = $type and status = 0 order by monitor_priority";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function getReferenceNameByID($Id = '') {
        $status = "";
        $this->getTimestamp();
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_reference . "  where id = $Id and status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query";
        $row = $result->fetch_assoc();
        //print_r($result);
        $this->reference_name = $row["reference_name"];
    }

    public function getCasteNameByID($Id = '') {
        $status = "";
        $this->getTimestamp();
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_caste . "  where id = $Id and status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query";
        $row = $result->fetch_assoc();
        //print_r($result);
        $this->caste_name = $row["caste_name"];
    }

    public function getStatusByID($id = '') {
        $status = "";
        $this->getTimestamp();
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_status . "  where id = $id and status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query";
        //print_r($result);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $this->status_name = $row["status_name"];
            $this->status_color = $row["status_color"];
        } else {
            $this->status_name = "Invalid Status";
            $this->status_color = "#FFF";
        }
    }

    public function listEnqType() {

        $status = "";
        //$this->getTimestamp();            
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_enq_type . "  where status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        //$result_data = $result->fetch_assoc();
        //$result_data = $this->getDataAsArray($result);
        return $result;
    }

    public function listPoojawithCount() {

        $status = "";
        //$this->getTimestamp();            
        //MySqli Insert Query 
        //$query = "Select * from " . $this->tbl_pooja . "  where status = 0";
/*         $query = "SELECT p.id,p.pooja_name,count(e.puja_name) p_count 
            FROM `" . $this->tbl_pooja . "` p 
            inner join " . $this->tbl_enq_puja . " e 
            on p.id = e.puja_name group by 
            e.puja_name ORDER BY p.pooja_name ASC ";
*/
         $query = "SELECT p.*
                ,(SELECT count(e.puja_name) FROM " . $this->tbl_enq_puja . " e WHERE p.id = e.puja_name) as p_count
                FROM " . $this->tbl_pooja . " p ORDER BY p.pooja_name ASC";

        $result = $this->conn->query($query);
        return $result;
    }

    public function listPooja() {

        $status = "";
        //$this->getTimestamp();            
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_pooja . "  where status = 0";
        $result = $this->conn->query($query);
        return $result;
    }

    public function getPoojaById($id) {

        $status = "";
        //$this->getTimestamp();            
        //MySqli Insert Query
        $query = "Select * from " . $this->tbl_pooja . "  where id=$id and status = 0";
        $result = $this->conn->query($query);
        return $result;
    }

    public function getDBAsArray($data) {
        $data_array = array();
        while ($row = $data->fetch_assoc()) {
            array_push($data_array, $row);
        }
        return $data_array;
    }

    public function getDBAsRow($data) {
        $data_array = array();
        while ($row = $data->fetch_assoc()) {
            //array_push($data_array, $row);
            $data_array = $row;
        }
        return $data_array;
    }

}
