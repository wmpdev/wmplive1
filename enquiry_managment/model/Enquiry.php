<?php

class Enquiry extends Master {

    private $conn;
    private $tbl_enquiry = "tbl_enquiry";
    private $tbl_enq_pooja = "tbl_enq_pooja";
    private $tbl_enq_product = "tbl_enq_product";
    private $tbl_enq_vendor = "tbl_enq_vendor";
    private $tbl_enq_followup = "tbl_enq_followup";
    // object properties for tbl_enquiry
    public $cust_enq_id = 0;
    public $id;
    public $cust_id = "";
    public $budget = "";
    public $pooja_date = "";
    public $pooja_time = "";
    public $service_id = "";
    public $medium = "";
    public $address_1 = "";
    public $address_2 = "";
    public $country_id = 0;
    public $state_id = 0;
    public $city_id = 0;
    public $area_id = 0;
    public $pincode = 0;
    public $cast_id = 0;
    public $comment = "";
    public $enq_status = 1;
    public $date = "";
    public $add_date = "";
    public $mod_date = "";
    public $add_user = 0;
    public $mod_user = 0;
    public $status = 0;
    public $timestamp = "";
    public $userMsg = "";
    public $SuccessMsg = "";
    public $ErrorMsg = "";
    public $ErrorFlag = 0;
    // object properties for enquiry_details
    public $enq_id;
    public $enq_type_id = 0;
    public $product_id = 0;
    public $quantity = 0;
    public $actual_cost = 0;
    public $selling_cost = 0;
    public $discount = 0;
    public $transport_cost = 0;
    public $tax_id = 0;
    //public $transport_cost="";
// Object properties for Followups
    public $add_time = "";
    public $next_date = "";
    public $next_time = "";

    public function __construct($db) {
        $this->conn = $db;
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    // used for the 'created' field when creating a product
    function getTimestamp() {
        //date_default_timezone_set('Asia/Manila');
        $this->timestamp = date('Y-m-d H:i:s');
    }

    function inputValidation() {
        $this->ErrorFlag = 0;

        //if(strlen($this->phone_1)>0)   
        //$this->phone_1 =  str_replace(array('+', '(', ')', ' ','-'), '', $this->phone_1);
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    public function addNewEnquiry() {

        global $log;
        $log->logMsg("Add New Enquiry");

        //if (empty($values)) return FALSE;
        //if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
        //if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
        //global $PDO;
        // to get time-stamp for 'created' field
        $this->inputValidation();
        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add Enquiry";
            return 0;
        }
        $this->getTimestamp();

        /*
          //write query
          $query = "INSERT INTO " . $this->table_name . " SET first_name = ?, last_name = ?, email_1 = ?, email_2 = ?, phone_1 = ?, phone_2 = ?, address_1 = ?, address_2 = ?, country_id = ?, state_id = ?, city_id = ?, area_id = ?, pincode = ?, cast_id = ?, other = ?, date = ?, add_date = ?, mod_date = ?, status = ?";
          $stmt = $this->conn->prepare($query);

          $stmt->bindParam(1, $this->first_name);
          $stmt->bindParam(2, $this->last_name);
         */



        $query = "INSERT INTO " . $this->tbl_enquiry
                . " set cust_id = '" . $this->cust_id
                . "', budget = '" . $this->budget
                . "', pooja_date = '" . $this->pooja_date
                . "', pooja_time = '" . $this->pooja_time
                . "', service_id = '" . $this->service_id
                . "', medium = '" . $this->medium
                . "', address_1 = '" . $this->address_1
                . "', country_id = '" . $this->country_id
                . "', state_id = '" . $this->state_id
                . "', city_id = '" . $this->city_id
                . "', area_id = '" . $this->area_id
                . "', pincode = '" . $this->pincode
                . "', caste_id = '" . $this->cast_id
                . "', comment = '" . $this->comment
                . "', enq_status = " . $this->enq_status
                . ", add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', status = 0";

        $insert_row = 0;
        $last_id = 0;

        $insert_row = $this->conn->query($query);
        //echo "<br/>New Enquiry = " . $query . " <br/>result = $insert_row";

        if ($insert_row) {

            $last_id = $this->conn->insert_id;
            $this->SuccessMsg = "New Enquiry Added Successfully";
            $log->logMsg("Success : New Enquiry Id = " . $last_id);
            $enq_details_id = $this->addEnquiryDetails($this->cust_id, $last_id, $this->cust_enq_id);
            $last_id = $enq_details_id;
            //unset($_SESSION[$this->cust_enq_id]);
            //printf("Add User Error message: %s\n", $mysqli->error);
        } else {
            $Error_msg = $this->conn->error;
            $this->ErrorMsg = "Error : Fail To Add Enquiry";
            $log->LogError("New Enquiry add Error = " . $Error_msg . " Query = $query");
        }


        // else echo "<br/>Add User Error message:".$this->conn->error."<br/>";
        //echo "User Add query = $query";


        return $last_id;


        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
    }

    public function addEnquiryDetails($cust_id = 0, $enq_id = 0, $cust_enq_id = 0) {

        global $log;
        //global $master;
        //global $Product;
        //global $Vendor;



        $this->getTimestamp();
        $log->logMsg("Add New Enquiry Details");

        $this->inputValidation();
        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add Enquiry Details";
            return 0;
        }

        //if (empty($values)) return FALSE;


        if ((count($_SESSION[$cust_enq_id]) > 0) && ($cust_id > 0) && ($enq_id > 0)) {
            //$count = count($_SESSION[$cust_id]);
//print_r($_SESSION[$cust_enq_id]);
            for ($i = 0; $i < count($_SESSION[$cust_enq_id]); $i++) {
//print_r($_SESSION[$cust_enq_id][$i]);
                //echo "<br/>Type_id = " . $_SESSION[$cust_enq_id][$i]["type_id"] . " item_id = " . $_SESSION[$cust_enq_id][$i]["item_id"];
                $this->enq_id = $enq_id;
                $this->enq_type_id = $_SESSION[$cust_enq_id][$i]["item_id"]; //$_SESSION[$cust_enq_id][$i]["type_id"];
                $this->product_id = $_SESSION[$cust_enq_id][$i]["item_id"];

                $this->quantity = $_SESSION[$cust_enq_id][$i]["item_qty"];
                $this->actual_cost = 0;
                $this->selling_cost = 0;

                $this->discount = 0;
                $this->transport_cost = 0;
                $this->tax_id = 0;

                $tbl_enquiry_details = "";
                $query = "";
                if ($this->enq_type_id == 1) {
                    //$tbl_enquiry_details = $this->tbl_enq_pooja;
                    //$tbl_enquiry_col = "pooja_id = " . $this->$product_id;
                    $query = $this->getPoojaQuery();
                } elseif ($this->enq_type_id == 2) {
                    //$tbl_enquiry_details = $this->tbl_enq_product;
                    //$tbl_enquiry_col = "enq_type_id = " . $this->$enq_type_id . ", product_id = " . $this->$product_id;
                    $query = $this->getProductQuery();
                } elseif ($this->enq_type_id == 3) {
                    //$tbl_enquiry_details = $this->tbl_enq_vendor;
                    //$tbl_enquiry_col = "enq_type_id = " . $this->$enq_type_id . ", product_id = " . $this->$product_id;
                    $query = $this->getVendorQuery();
                }


                $query = "INSERT INTO " . $query;

                /*
                  $query = "INSERT INTO " . $tbl_enquiry_details
                  . " set enq_id = '" . $this->$enq_id
                  . " , $tbl_enquiry_col"
                  . "', quantity = '" . $this->budget
                  . "', actual_cost = '" . $this->pooja_date
                  . "', selling_cost = '" . $this->pooja_time
                  . "', discount = '" . $this->service_id
                  . "', transport_cost = '" . $this->medium
                  . "', tax_id = '" . $this->address_1
                  . "', add_user = '" . $_SESSION['login_id']
                  . "', mod_user = '" . $_SESSION['login_id']
                  . "', add_date = '" . $this->timestamp
                  . "', mod_date = '" . $this->timestamp
                  . "', status = 0";
                 */

                $insert_row = 0;
                $last_id = 0;
                //echo "<br/>New Enquiry Details Query = " . $query;
                $insert_row = $this->conn->query($query);

                if ($insert_row) {

                    $last_id = $this->conn->insert_id;
                    $this->SuccessMsg = "New Enquiry Added Successfully";
                    $log->logMsg("Success : New Enquiry Detail Id = " . $last_id);
                    //printf("Add User Error message: %s\n", $mysqli->error);
                } else {
                    $Error_msg = $this->conn->error;
                    $this->ErrorMsg = "Error : Fail To Add Enquiry";
                    $log->LogError("New Enquiry Detail add Error = " . $Error_msg . " Query = $query");
                }
            }
        }







        return $last_id;
    }

    /**
     * Registers a user with given parameters
     *
     * @param array $values Array of input values
     *
     * @return bool Returns true if successful
     * 
     * Developer :- Ninad 
     */
    public function listEnquiryByCustId($cust_id) {

        $query = "Select * from " . $this->tbl_enquiry . "  where cust_id = $cust_id and status = 0 order by id desc";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listEnquiryById($id) {

        $query = "Select * from " . $this->tbl_enquiry . "  where id = $id and status = 0";
        $result = $this->conn->query($query);
        //echo "Query = $query"; 
        return $result;
    }

    public function listEnqPoojaDetails($enq_id) {

        $result_arr = array();


        //$query = "Select * from ".$this->tbl_enq_pooja."  where enq_id = $enq_id and status = 0";//echo "<br/>Query = $query<br/>"; 
        $query = "SELECT * FROM " . ENQ_POOJA . " ep INNER JOIN " . POOJA_MASTER . " pm ON pm.id = ep.pooja_id and ep.enq_id=$enq_id";
        //echo "<br/>Query = $query<br/>"; 
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listEnquiryDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

    public function listEnqProductDetails($enq_id) {

        $result_arr = array();

        //$query = "Select * from ".$this->tbl_enq_product."  where enq_id = $enq_id and status = 0";//echo "<br/>Query = $query<br/>"; 
        $query = "SELECT * FROM " . ENQ_PRODUCT . " ep INNER JOIN " . PRODUCT_MASTER . " pm ON pm.id = ep.product_id and ep.enq_id=$enq_id";
        //echo "<br/>Query = $query<br/>"; 
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listEnquiryDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

    public function listEnqVendorDetails($enq_id) {

        $result_arr = array();

        //$query = "Select * from ".$this->tbl_enq_vendor."  where enq_id = $enq_id and status = 0";//echo "<br/>Query = $query<br/>"; 
        $query = "SELECT * FROM " . ENQ_VENDOR . " ep INNER JOIN " . VENDOR_TYPE_MASTER . " pm ON pm.id = ep.enq_type_id and ep.enq_id=$enq_id";
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listEnquiryDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

    public function getPoojaQuery() {

        $query = " " . $this->tbl_enq_pooja
                . " set enq_id = '" . $this->enq_id
                . "', pooja_id = '" . $this->product_id
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', status = 0";

        //echo "<br/> into getPoojaQuery Query = $query";
        return $query;
    }

    public function getProductQuery() {    // enq_type_id = " . $this->$enq_type_id . ", product_id = " . $this->$product_id;
        $query = " " . $this->tbl_enq_product
                . " set enq_id = '" . $this->enq_id
                . "', enq_type_id = '" . $this->enq_type_id
                . "', product_id = '" . $this->product_id
                . "', quantity = '" . $this->quantity
                . "', actual_cost = '" . $this->actual_cost
                . "', selling_cost = '" . $this->selling_cost
                . "', discount = '" . $this->discount
                . "', transport_cost = '" . $this->transport_cost
                . "', tax_id = '" . $this->tax_id
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', status = 0";

        //echo "<br/> into getProductQuery Query = $query";

        return $query;
    }

    public function getVendorQuery() {    // enq_type_id = " . $this->$enq_type_id . ", product_id = " . $this->$product_id;
        $query = " " . $this->tbl_enq_vendor
                . " set enq_id = '" . $this->enq_id
                . "', enq_type_id = '" . $this->product_id // Vendor is different than product only vendor type is defined 
                . "', vendor_id = '0"
                . "', quantity = '" . $this->quantity
                . "', actual_cost = '" . $this->actual_cost
                . "', selling_cost = '" . $this->selling_cost
                . "', discount = '" . $this->discount
                . "', transport_cost = '" . $this->transport_cost
                . "', tax_id = '" . $this->tax_id
                . "', add_user = '" . $_SESSION['login_id']
                . "', mod_user = '" . $_SESSION['login_id']
                . "', add_date = '" . $this->timestamp
                . "', mod_date = '" . $this->timestamp
                . "', status = 0";

        //echo "<br/> into getVendorQuery Query = $query";

        return $query;
    }

    public function getEnqDetailsByCustId($cust_id) {

        $tbl_rows = array();

        $EnquiryData = $this->listEnquiryByCustId($cust_id);
        $count = 0;

        while ($Enq_row = $EnquiryData->fetch_assoc()) {

            $enq_id = $Enq_row["id"];
            $result = $this->getEnqDetailsByEnqId($enq_id);
            if (!empty($result)) {
                array_push($tbl_rows, $Enq_row);
                for ($i = 0; $i < count($result); $i++) {
                    array_push($tbl_rows, $result[$i]);
                }
            }
        }


        return $tbl_rows;
    }

    public function getEnqDetailsByEnqId($enq_id) {

        $tbl_rows = array();

        $EnqPoojaDetailsData = $this->listEnqPoojaDetails($enq_id);
        while ($Enq_Pooja_details_row = $EnqPoojaDetailsData->fetch_assoc()) {
            //echo "<br/> enq Pooja Details id = " . $Enq_Pooja_details_row["id"] . "<br/>";
            array_push($tbl_rows, $Enq_Pooja_details_row);
            //print_r($Enq_Pooja_details_row);
            $content_flag++;
        }

        $EnqProductDetailsData = $this->listEnqProductDetails($enq_id);
        while ($Enq_Product_details_row = $EnqProductDetailsData->fetch_assoc()) {
            //echo "<br/> enq Product Details id = " . $Enq_Product_details_row["id"] . "<br/>";
            array_push($tbl_rows, $Enq_Product_details_row);
            //print_r($Enq_Product_details_row);
            $content_flag++;
        }

        $EnqVendorDetailsData = $this->listEnqVendorDetails($enq_id);
        while ($Enq_Vendor_details_row = $EnqVendorDetailsData->fetch_assoc()) {
            //echo "<br/> enq Vendor Details id = " . $Enq_Vendor_details_row["id"] . "<br/>";
            array_push($tbl_rows, $Enq_Vendor_details_row);
            //print_r($Enq_Vendor_details_row);
            $content_flag++;
        }


        return $tbl_rows;
    }

    public function addNewFollowup() {

        global $log;
        $log->logMsg("Add New FollowUp");

        //if (empty($values)) return FALSE;
        //if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
        //if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
        //global $PDO;
        // to get time-stamp for 'created' field
        $this->inputValidation();



        if (!$this->enq_id > 0) {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Please Select Enquiry.</li>";
            //$Enquiry->add_date = "0000-00-00 00:00:00";
        }

        if (!strlen($this->comment) > 0) {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Please Enter Comments.</li>";
            //$Enquiry->add_date = "0000-00-00 00:00:00";
        }



        $add_date = $this->add_date;    //$_POST['current_date'] . " " . $_POST['current_time'];
        $next_date = $this->next_date;  //$_POST['next_date'] . " " . $_POST['next_time'];

        if (DateTime::createFromFormat('d-m-Y H:i a', $this->add_date) !== FALSE) {
            // it's a date
            $this->add_date = DateTime::createFromFormat('d-m-Y H:i a', $this->add_date)->format('Y-m-d H:i:s');
        } else {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Comment date and Time is not proper</li>";
            //$Enquiry->add_date = "0000-00-00 00:00:00";
        }


        if (DateTime::createFromFormat('d-m-Y H:i a', $this->next_date) !== FALSE) {
            // it's a date
            $this->next_date = DateTime::createFromFormat('d-m-Y H:i a', $this->next_date)->format('Y-m-d H:i:s');
        } else {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Next Date And Time is not proper</li>";
        }

        if (!$this->enq_status > 0) {
            $this->ErrorFlag = TRUE;
            $this->ErrorMsg .= "<li>Please Select Enquiry Status.</li>";
            //$Enquiry->add_date = "0000-00-00 00:00:00";
        }

        if ($this->ErrorFlag) {
            $this->ErrorMsg = "Error : Fail To Add Enquiry Followup<ul>" . $this->ErrorMsg . "</ul>";
            $this->ErrorFlag = FALSE;
            return 0;
        }


        $this->getTimestamp();
        $query = "INSERT INTO " . $this->tbl_enq_followup
                . " set enq_id = '" . $this->enq_id
                . "', comment = '" . $this->comment
                . "', add_date = '" . $this->add_date
                . "', next_date = '" . $this->next_date
                . "', add_user = '" . $_SESSION['login_id']
                . "', status = 0";

        $insert_row = 0;
        $last_id = 0;

        $insert_row = $this->conn->query($query);
        // echo "<br/>New Enquiry = " . $query . " <br/>result = $insert_row";

        if ($insert_row) {

            $last_id = $this->conn->insert_id;

            $enq_status_result = $this->setEnqStatus($this->enq_id, $this->enq_status, $this->next_date);
            if (!$enq_status_result) {
                return 0;
            }

            $this->SuccessMsg = "Enquiry Followup Added Successfully";
            $log->logMsg("Success : New Enquiry Id = " . $last_id);
            $enq_details_id = $this->addEnquiryDetails($this->cust_id, $last_id, $this->cust_enq_id);
            $last_id = $enq_details_id;
            //unset($_SESSION[$this->cust_enq_id]);
            //printf("Add User Error message: %s\n", $mysqli->error);
        } else {
            $Error_msg = $this->conn->error;
            $this->ErrorMsg = "Error : Fail To Add Enquiry Followup";
            $log->LogError("New Enquiry add Error = " . $Error_msg . " Query = $query");
        }

        return $last_id;
    }

    public function setEnqStatus($enq_id, $enq_status, $next_date) {

        $query = "update " . $this->tbl_enquiry . " set followup_date = '$next_date', enq_status = $enq_status where id = $enq_id";
        $insert_row = $this->conn->query($query);
        if (!$insert_row) {
            $Error_msg = $this->conn->error;
            $this->ErrorFlag = TRUE;
            echo "query = $query";
            $this->ErrorMsg .= "<br/>Error : Fail To Update Enquiry Status <br/>SqlErr: = $Error_msg";
        }
        /*
          if ($enq_status == 7) {
          $insert_row = $this->convertIntoOrder();
          }
         */

        return $insert_row;
    }

    public function convertIntoOrder($enq_id, $enq_status, $next_date) {

        $query = "update " . $this->tbl_enquiry . " set followup_date = '$next_date', enq_status = $enq_status where id = $enq_id";
        $insert_row = $this->conn->query($query);
        if (!$insert_row) {
            $Error_msg = $this->conn->error;
            $this->ErrorFlag = TRUE;
            echo "query = $query";
            $this->ErrorMsg .= "<br/>Error : Fail To Update Enquiry Status <br/>SqlErr: = $Error_msg";
        }
    }

    public function getEnqFollowup($enq_id) {

        $result_arr = array();

//        $query = "Select * from ".$this->tbl_enq_vendor."  where enq_id = $enq_id and status = 0";//echo "<br/>Query = $query<br/>"; 
//        SELECT f.*,a.name FROM `tbl_enq_followup` f inner join tbl_admin a on f.enq_id = 18 and f.status = 0 and (f.add_user = a.id) ORDER BY add_date desc
        //$query = "SELECT * FROM " . ENQ_VENDOR . " ep INNER JOIN " . VENDOR_TYPE_MASTER . " pm ON pm.id = ep.enq_type_id and ep.enq_id=$enq_id";
        $query = "SELECT f.*,a.name,a.thumb_img FROM " . $this->tbl_enq_followup . " f inner join " . Admin . " a on f.enq_id = $enq_id and f.status = 0 and (f.add_user = a.id) ORDER BY id desc";
        $result = $this->conn->query($query);
        //array_push($result_arr,$result);
        //echo "<br/>listEnquiryDetails Query = $query<br/>";
        //print_r($result);
        return $result;
    }

}
