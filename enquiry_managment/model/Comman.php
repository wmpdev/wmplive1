<?php

class Comman {

    private static $userId;
    private $conn;
    private $tbl_vendor_type = "tbl_vendor_type_master";
    private $tbl_vendor = "tbl_vendor_master";
    private $tbl_vendor_profession = "tbl_vendor_profession";
    // object properties
    public $id;
    public $first_name = "";
    public $last_name = "";
    public $email_1 = "";
    public $email_2 = "";
    public $phone_1 = "";
    public $phone_2 = "";
    public $address_1 = "";
    public $address_2 = "";
    public $country_id = 0;
    public $state_id = 0;
    public $city_id = 0;
    public $area_id = 0;
    public $pincode = 0;
    public $cast_id = 0;
    public $comment = "";
    public $date = "";
    public $add_date = "";
    public $mod_date = "";
    public $add_user = 0;
    public $mod_user = 0;
    public $status = 0;
    public $timestamp = "";
    public $userMsg = "";
    public $SuccessMsg = "";
    public $ErrorMsg = "";
    public $ErrorFlag = 0;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function calcPercent($item_cost,$discount) {
        $percent = $discount/100;
        return $item_cost*$percent;        
    }
    public function viewEnqData($enq_cache_id) {
        
        if (count($_SESSION[$enq_cache_id]) > 0) { ?>    
                                    <table class="table table-bordered sortableTable responsive-table">
                                        <thead>
                                            <tr align="center">
                                                <th>#</th>
                                                <th>Type</th>
                                                <th>Item</th>
                                                <!--
                                                <th>Actual</th>
                                                <th>Selling</th>
                                                -->
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Disc %</th>
                                                <th>Transport</th>
                                                <th>Tax %</th>
                                                <th>Total</th>
                                                <th><a href=""><i class="icon-refresh"></i></a></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $sub_total = 0;
                                            for ($i = 0; $i < count($_SESSION[$enq_cache_id]); $i++) {

                                                $actual_cost = $_SESSION[$enq_cache_id][$i]["actual_cost"];
                                                $selling_cost = $_SESSION[$enq_cache_id][$i]["selling_cost"];

                                                $item_qty = $_SESSION[$enq_cache_id][$i]["item_qty"];
                                                $item_cost = $_SESSION[$enq_cache_id][$i]["item_cost"];
                                                $discount = $_SESSION[$enq_cache_id][$i]["discount"];
                                                
                                                $transport_cost = $_SESSION[$enq_cache_id][$i]["transport_cost"];
                                                $tax_class_id = $_SESSION[$enq_cache_id][$i]["tax_class_id"];
                                                
                                                $item_cost_calc = $item_qty * $item_cost;
                                                $discount_calc = $this->calcPercent($item_cost_calc,$discount);
                                                $tax_calc = $this->calcPercent(($item_cost_calc+$discount_calc),$tax_class_id);
                                                
                                                        //($item_cost*$discoun);
                                                
                                                
                                                $item_total = (($item_cost_calc - $discount_calc) + $transport_cost + $tax_calc);
                                                
                                                $sub_total = $sub_total + $item_total;
                                                
                                                
                                                ?>
                                                <tr>
                                                    <td><?php echo $i + 1; ?></td>
                                                    <td><?php echo $_SESSION[$enq_cache_id][$i]["item_type_name"] ?></td>
                                                    <td>
                                                        <?php echo $_SESSION[$enq_cache_id][$i]["item_name"] ?>
                                                        <?php if (($actual_cost > 0) || ($selling_cost > 0)|| ($item_cost > 0)) { ?>
                                                            <!--Second tooltip-->
                                                            <a href="#" class="enq_tooltip">
                                                                <i class="icon-info-sign"></i>
                                                                <span>                                                                            
                                                                    <strong>Actual Cost : </strong><?php echo $actual_cost; ?><br />
                                                                    <strong>Selling Cost : </strong><?php echo $selling_cost; ?><br/>
                                                                    <strong>Price : </strong>
                                                                        <?php echo "(($item_qty * $item_cost) - $discount_calc + $transport_cost + $tax_calc)"; ?>
                                                                </span>
                                                            </a>
                                                            <!--Second tooltip-->                                                                
                                                        <?php } ?>
                                                    </td>
                                                    <!--
                                                    <td><?php echo $_SESSION[$enq_cache_id][$i]["actual_cost"]; ?></td>
                                                    <td><?php echo $_SESSION[$enq_cache_id][$i]["selling_cost"]; ?></td>
                                                    -->
                                                    <td><input id="qty_<?php echo $i; ?>" value="<?php echo $item_qty; ?>"  onfocusout="refresh_cart(<?php echo $i; ?>)" size="1" class="form-control" type="text"></td>
                                                    <td><input id="price_<?php echo $i; ?>" value="<?php echo $item_cost; ?>" size="7"class="form-control" type="text"></td>
                                                    <td><input id="disc_<?php echo $i; ?>" value="<?php echo $discount; ?>" size="7"class="form-control" type="text"></td>
                                                    <td><input id="trans_<?php echo $i; ?>" value="<?php echo $transport_cost; ?>" size="7"class="form-control" type="text"></td>
                                                    <td><input id="tax_<?php echo $i; ?>" value="<?php echo $tax_class_id; ?>" size="7"class="form-control" type="text"></td>
                                                    <td align="right"><?php echo $item_total ?></td>
                                                    <td align="center">
                                                        
                                                        <a class="btn text-warning btn-xs btn-flat" href="#" onclick="refresh_cart(<?php echo $i; ?>)" title="Edit">
                                                            <i class="icon-edit-sign icon-white"></i>
                                                        </a>
                                                        <!-- -->
                                                        <a class="btn text-danger btn-xs btn-flat" href="add_user_15-may.php?rec_id=2" title="Remove">
                                                            <i class="icon-remove-sign icon-white"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr align="right">
                                                <td colspan="8" align="right"><strong>Sub Total</strong></td>
                                                <td><?php echo $sub_total; ?></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr align="right">
                                                <td colspan="8" align="right"><strong>Discount</strong></td>
                                                <td><input id="enq_discount" value="0" size="7" class="form-control" type="text"></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr align="right">
                                                <td colspan="8" align="right"><strong>Total</strong></td>
                                                <td><?php echo $sub_total; ?></td>
                                                <td>&nbsp;</td>
                                            </tr>



                                        </tbody>
                                    </table>
                                <?php } else {
                                    ?>
                                    <div class="alert alert-danger">
                                        No Item Found. <a class="alert-link" href="#">X</a>.
                                    </div>
                                    <?php
                                    exit();
                                }
        
    }

}
