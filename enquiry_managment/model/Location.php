<?php

class Location {
	private static $countryId;
        private $conn;
        private $tbl_country = "tbl_country_master";
        private $tbl_state = "tbl_state_master";
        private $tbl_city = "tbl_city_master";
        private $tbl_area = "tbl_area_master";
        

        // object properties
        public $country_id;
        public $country_name="";
        
        
        
        
        public $add_date="";
        public $mod_date="";
        public $rec_type=0;
        public $status=0;
        public $timestamp="";
    
        

        public function __construct($db){
            $this->conn = $db;
        }
    
    
    
        /**
	 * Registers a user with given parameters
	 *
	 * @param array $values Array of input values
	 *
	 * @return bool Returns true if successful
         * 
         * Developer :- Ninad 
	 */
	public function add_New_Country() {
            
		//if (empty($values)) return FALSE;
		//if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
		//if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
		//global $PDO;
	
		
		// to get time-stamp for 'created' field
                $status = "";
                $this->getTimestamp();

                
                //values to be inserted in database table
                $this->country_name = $this->conn->real_escape_string($this->country_name);                

                //MySqli Insert Query
                $query = "INSERT INTO ".$this->tbl_country." set country_name = '".$this->country_name."', add_date = '".$this->timestamp."', mod_date = '".$this->timestamp."', status = 0";
                $insert_row = $this->conn->query($query);

                if($insert_row){
                    $status='Country Added Successfully'; 
                }else{
                    $status='Error : ('. $mysqli->errno .') '. $mysqli->error;  //return false;
                }

                return   $status;  
                
                
                //$stmt->bindParam(5, $this->timestamp);

                

                        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
        }
        
        // used for the 'created' field when creating a product
        function getTimestamp(){
            //date_default_timezone_set('Asia/Manila');
            $this->timestamp = date('Y-m-d H:i:s');
        }
        
        
        public function list_Country($id) {

            //if (empty($values)) return FALSE;
            //if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
            //if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
            //global $PDO;


            // to get time-stamp for 'created' field
            $status = "";
            $this->getTimestamp();


            //values to be inserted in database table
            $id = $this->conn->real_escape_string($id);                

            //MySqli Insert Query
            $query = "Select * from ".$this->tbl_country." where status = 0";
            $result = $this->conn->query($query);

            /*if($insert_row){
                $status='Country Added Successfully'; 
            }else{
                $status='Error : ('. $mysqli->errno .') '. $mysqli->error;  //return false;
            }
            */
             

            return   $result;  


            //$stmt->bindParam(5, $this->timestamp);



                    //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
        }
      

        public function list_State($id=1) {
     
            $status = "";
            $this->getTimestamp();
            //if(!isset($id)) $id=1;
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                

            //MySqli Insert Query
            $query = "Select * from ".$this->tbl_state."  where country_id = $id and status = 0";
            $result = $this->conn->query($query);
            //$log->logMsg("Success : State Query = ".$query);
            //echo "Query = $query"; 
            return   $result;  

        }
        
        public function list_City($id=1) {

            $status = "";
            $this->getTimestamp();
            
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                

            //MySqli Insert Query
            $query = "Select * from ".$this->tbl_city."  where state_id = $id and status = 0";
            $result = $this->conn->query($query);
            //echo "Query = $query"; 
            return   $result;  

        }

        public function list_Area($id=1) {

            $status = "";
            $this->getTimestamp();
            
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                

            //MySqli Insert Query
            $query = "Select * from ".$this->tbl_area."  where city_id = $id and status = 0";
            $result = $this->conn->query($query);
            //echo "Query = $query"; 
            return   $result;  

        }        

        public function trace_Area($id=1) {

            $status = "";
            $this->getTimestamp();
            
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                

            //MySqli Insert Query
            $query = "Select * from ".$this->tbl_area."  where city_id = $id and status = 0";
            
            $query = "SELECT * FROM ".$this->tbl_area." a
                    INNER JOIN ".$this->tbl_city." c ON c.id = a.city_id
                    INNER JOIN ".$this->tbl_state." s ON s.id = c.state_id
                    INNER JOIN ".$this->tbl_country." co ON co.id = s.country_id
                    where a.id = $id";
                    
                    
                    
            $result = $this->conn->query($query);
            //echo "Query = $query"; 
            return   $result;  

        }
        

} 
