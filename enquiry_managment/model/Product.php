<?php

class Product {
	private static $userId;
        private $conn;
        private $tbl_product = "tbl_product_master";
        
        
        // object properties
        public $id;
        public $first_name="";
        public $last_name="";        
        public $email_1="";
        public $email_2="";
        public $phone_1="";
        public $phone_2="";
        public $address_1="";        
        public $address_2="";
        
        public $country_id=0;
        public $state_id=0;
        public $city_id=0;
        public $area_id=0;
        public $pincode=0;
        public $cast_id=0;
        public $comment="";        
        
        public $date="";
        public $add_date="";
        public $mod_date="";
        public $add_user=0;
        public $mod_user=0;
        
        public $status=0;
        public $timestamp="";
        
        public $userMsg="";
        public $SuccessMsg="";
        public $ErrorMsg="";
        public $ErrorFlag=0;
        
    
        



        public function __construct($db){
            $this->conn = $db;            
        }
    
    
    
        /**
	 * Registers a user with given parameters
	 *
	 * @param array $values Array of input values
	 *
	 * @return bool Returns true if successful
         * 
         * Developer :- Ninad 
	 */
	public function addNewUser() {
            
            global $log;  
            $log->logMsg("Add New User");
            
		//if (empty($values)) return FALSE;
		//if (!Booklet::isRegistrationKeyValid($values['registration-key'])) return FALSE;
		//if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) return FALSE;
		//global $PDO;
	
		
		// to get time-stamp for 'created' field
                $this->inputValidation();
                if($this->ErrorFlag) {  $this->ErrorMsg = "Error : Fail To Add User";  return 0; }
                $this->getTimestamp();

                /*
                //write query
                $query = "INSERT INTO " . $this->table_name . " SET first_name = ?, last_name = ?, email_1 = ?, email_2 = ?, phone_1 = ?, phone_2 = ?, address_1 = ?, address_2 = ?, country_id = ?, state_id = ?, city_id = ?, area_id = ?, pincode = ?, cast_id = ?, other = ?, date = ?, add_date = ?, mod_date = ?, status = ?"; 
                $stmt = $this->conn->prepare($query);

                $stmt->bindParam(1, $this->first_name);
                $stmt->bindParam(2, $this->last_name);
                */
                

        
                $query = "INSERT INTO ".$this->tbl_user
                        . " set first_name = '".$this->first_name
                        ."', last_name = '".$this->last_name
                        ."', email_1 = '".$this->email_1
                        ."', email_2 = '".$this->email_2
                        ."', phone_1 = '".$this->phone_1
                        ."', phone_2 = '".$this->phone_2
                        ."', address_1 = '".$this->address_1
                        ."', country_id = '".$this->country_id
                        ."', state_id = '".$this->state_id
                        ."', city_id = '".$this->city_id
                        ."', area_id = '".$this->area_id
                        ."', pincode = '".$this->pincode
                        ."', caste_id = '".$this->cast_id
                        ."', reference_id = '".$this->reference_id
                        ."', comment = '".$this->comment
                        
                        ."', date = '".date('Y-m-d')
                        ."', add_date = '".$this->timestamp
                        ."', mod_date = '".$this->timestamp
        

                        ."', add_user = '".$_SESSION['login_id']
                        ."', mod_user = '".$_SESSION['login_id']
                        
                        ."', status = 0";



                $last_id = 0;
                if ($insert_row = $this->conn->query($query)) {
                    
                    $last_id = $this->conn->insert_id;
                    $this->SuccessMsg = "New User Added Successfully";
                    $log->logMsg("Success : New User Id = ".$last_id);
                    //printf("Add User Error message: %s\n", $mysqli->error);
                }
                else
                {
                    $Error_msg = $this->conn->error;
                    $this->ErrorMsg = "Error : Fail To Add User";
                    $log->LogError("New User add Error = ".$Error_msg." Query = $query");                    
                }
                // else echo "<br/>Add User Error message:".$this->conn->error."<br/>";
                //echo "User Add query = $query";
                
                return   $last_id;


                        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
        }

       public function updateUser($id) {
           global $log; 
           
           $log->logMsg("Update User $id");
           $this->id = $id;

                $this->inputValidation();
                if($this->ErrorFlag) {  $this->ErrorMsg = "Error : Fail To Update User";  return 0; }
                $this->getTimestamp();
                
                $query = "update ".$this->tbl_user
                        . " set first_name = '".$this->first_name
                        ."', last_name = '".$this->last_name
                        ."', email_1 = '".$this->email_1
                        ."', email_2 = '".$this->email_2
                        ."', phone_1 = '".$this->phone_1
                        ."', phone_2 = '".$this->phone_2
                        ."', address_1 = '".$this->address_1
                        ."', country_id = '".$this->country_id
                        ."', state_id = '".$this->state_id
                        ."', city_id = '".$this->city_id
                        ."', area_id = '".$this->area_id
                        ."', pincode = '".$this->pincode
                        ."', caste_id = '".$this->cast_id
                        ."', reference_id = '".$this->reference_id
                        ."', comment = '".$this->comment
                        
                        ."', date = '".date('Y-m-d')                        
                        ."', mod_date = '".$this->timestamp
                        
                        ."', mod_user = '".$_SESSION['login_id']                        
                        ."', status = 0";
                
                
                $query .= " where id = ".$this->id;



                
                //$last_id = 0;
                if ($insert_row = $this->conn->query($query)) {
                    
                    //$last_id = $this->conn->insert_id;
                    $this->SuccessMsg = "User Updated Successfully";
                    $log->logMsg("Success : New User Id = ".$last_id);
                    $log->logMsg("Query for Update = ".$query);
                    //printf("Add User Error message: %s\n", $mysqli->error);
                }
                else
                {
                    $Error_msg = $this->conn->error;
                    $this->ErrorMsg = "Error : Fail To Update User";
                    $log->LogError("New User add Error = ".$Error_msg." Query = $query");                    
                }
                // else echo "<br/>Add User Error message:".$this->conn->error."<br/>";
                //echo "User Add query = $query";
                
                return   $insert_row;


                        //return ($userSuccess && Booklet::addRegistrationKey($values['registration-key'], $userId));
        }

        
        public function listProducts($page=0,$total_rec=0) {

            $status = "";
            $this->getTimestamp();
            $limit = "";
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                


            $query = "Select * from ".$this->tbl_product."  where status = 0 ".$limit;
            $result = $this->conn->query($query);
            //echo "Query = $query"; 
            return   $result;  

        }

        public function getUserEmaillist($page=0,$total_rec=0) {

            $status = "";
            //$this->getTimestamp();
            $limit = "";
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                


            $query = "Select email_1,email_2 from ".$this->tbl_user."  where status = 0 ".$limit;
            $result = $this->conn->query($query);
            //echo "Query = $query"; 
            return   $result;  

        }
 
       
        
        // used for the 'created' field when creating a product
        function getTimestamp(){
            //date_default_timezone_set('Asia/Manila');
            $this->timestamp = date('Y-m-d H:i:s');
        }

        function inputValidation(){
            $this->ErrorFlag=0;
            
            if(strlen($this->phone_1)>0)   $this->phone_1 =  str_replace(array('+', '(', ')', ' ','-'), '', $this->phone_1);
            
            
            
            
        }
        
        
        /**
	 * Checks whether user exists.
	 *
	 * @param string $userId
	 *
	 * @return object
	 */
	public function getProductByID($Id = '') {
            
            $status = "";
            $this->getTimestamp();
            
            //$this->country_name = $this->conn->real_escape_string($this->country_name);                

            //MySqli Insert Query
            $query = "Select * from ".$this->tbl_product."  where id = $Id and status = 0";
            $result = $this->conn->query($query);
            //echo "Query = $query"; 
            return   $result;     
 
	}
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
} 
