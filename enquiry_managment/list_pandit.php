<?php
// instantiate product object
$Main_Menu = "Users";
$Sub_Menu_1 = "list_pandit";
include_once 'config/auth.php';

//list($status, $id, $pandit_arr) = getPanditData();
list($status, $id, $pandit_arr) = listPanditData();

//$log->LogMsg("Listing Of Users");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title>List Pandit</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" />-->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content" >

                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>List Pandit </h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            List Pandit Data
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50px">No</th>
                                                            <th style="width: 100px">First Name</th>
                                                            <th style="width: 100px">Last Name</th>
                                                            <th>E-mail</th>
                                                            <th>Phone </th>
                                                            <th>Reference</th>
                                                            <th>Status</th>
                                                            <!--<th>Cast</th>
                                                            <th>Country</th> -->

                                                            <th style="width:205px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        $count = 0;

                                                        for ($i = 0; $i < count($pandit_arr); $i++) {
                                                            $count++;
                                                            $pandit_email = $pandit_arr[$i]['email_1'] . "<br/>" . $pandit_arr[$i]['email_2'];
                                                            $pandit_phone = $pandit_arr[$i]['phone_1'] . "<br/>" . $pandit_arr[$i]['phone_2'];
                                                            $pandit_reference = $pandit_arr[$i]['reference_id'];
                                                            $pandit_reference_name = "-";
                                                            if ($pandit_reference > 0) {
                                                                list($status, $id, $ref_arr) = getPanditData($pandit_reference);
                                                                if ($status == 1) {
                                                                    $pandit_reference_name = $ref_arr[0]['first_name'] . " " . $ref_arr[0]['last_name'];
                                                                }
                                                            }
                                                            $status = "";
                                                            if ($pandit_arr[$i]['status'] == 0) {
                                                                $status = "Enable";
                                                            } else {
                                                                $status = "Disable";
                                                            }
                                                            //$status = $pandit_arr[$i]['status'];
                                                            $login_id = $_SESSION['login_id'];
                                                            $show_edit = FALSE;

                                                            if ($login_id == 1 || $login_id == 8) {
                                                                $show_edit = TRUE;
                                                            }
                                                            ?> 
                                                            <tr class="odd gradeX">
                                                                <td><?php echo $count; ?></td>
                                                                <td><?php echo $pandit_arr[$i]['first_name']; ?></td>
                                                                <td><?php echo $pandit_arr[$i]['last_name']; ?></td>
                                                                <td><?php echo $pandit_email; ?></td>
                                                                <td><?php echo $pandit_phone; ?></td>
                                                                <td><?php echo $pandit_reference_name; ?></td>
                                                                <td><?php echo $status; ?></td>
                                                                 <!--<td><?php echo $pandit_arr[$i]['caste_id']; ?></td>
                                                                <td><?php echo $pandit_arr[$i]['country_id']; ?></td> -->





                                                                <td>
                                                                    <?php if ($show_edit) { ?>
                                                                        <a href="add_pandit.php?edit_id=<?php echo $pandit_arr[$i]['id']; ?>" class="btn text-info btn-xs btn-flat">
                                                                            <i class="icon-list-alt icon-white"></i> Edit</a>                                                                                                   <?php } ?>
                                                                </td>



                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>





                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <hr />




                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';     ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->    


    </body>
    <!-- END BODY-->

</html>
