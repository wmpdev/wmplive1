<?php
include_once 'config/auth.php';


$date = strtotime(date("Y-m-d"));
$day_1 = date("d-m-Y", $date);

$datetime = new DateTime('tomorrow');
//echo "<br/>tomorrow = ".$datetime->format('Y-m-d');
$date_2 = $datetime->format('d-m-Y');


if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    //$start_date = date("Y-m-d", strtotime($start_date));

    if (strlen($start_date) == 10) {
        $start_date = date("Y-m-d", strtotime($start_date));
    }
} else {
    $start_date = "";
    $start_date = date("Y-m-d", strtotime($day_1));  //$date;
}
if (isset($_GET["to_date"])) {
    $end_date = $_GET["to_date"];
    if (strlen($end_date) == 10) {
        $end_date = date("Y-m-d", strtotime($end_date));
    } else {
        $end_date = "";
    }
} else {
    $end_date = "";
}

list($status, $from_date, $to_date, $msg) = setFromToDate($start_date, $end_date);


$report_type = $_GET["type"];

function array2csv(array &$array) {
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}

function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

$source_arr = getDataFromTable("tbl_source_master");
$medium_arr = getDataFromTable("tbl_medium_master"); //var_dump($medium_arr);
$status_arr = getDataFromTable("tbl_status_master");


$query = "SELECT COUNT( e.puja_name ) Puja_count, p.id, p.pooja_name
FROM  `tbl_enq_puja` e
JOIN tbl_pooja_master p ON e.puja_name = p.id
GROUP BY e.puja_name
ORDER BY Puja_count DESC ";

$query = "SELECT p.pooja_name,count(ep.puja_name) p_count,GROUP_CONCAT(e.enq_no) 
FROM `tbl_pooja_master` p 
inner join tbl_enq_puja ep 
inner join tbl_enquiry e
on p.id = ep.puja_name 
and ep.enq_id = e.id
group by ep.puja_name 
ORDER BY `p_count` desc ";

$query = "SELECT e.id, e.enq_no, e.enq_date, e.medium_id, e.quotation_id, e.enq_status, e.order_no, GROUP_CONCAT( p.pooja_name ) , s.status_name, c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_enquiry e
INNER JOIN tbl_enq_puja ep
INNER JOIN tbl_pooja_master p
INNER JOIN tbl_customer c
INNER JOIN tbl_status_master s ON p.id = ep.puja_name
AND ep.enq_id = e.id
AND e.cust_id = c.id
AND e.enq_status = s.id
GROUP BY e.id ";

// ------------------------
$query = "SELECT e.id, e.enq_no, e.enq_date, e.medium_id, e.quotation_id, e.enq_status, e.order_no, GROUP_CONCAT( p.pooja_name ) , s.status_name, c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_enquiry e
INNER JOIN tbl_enq_puja ep
INNER JOIN tbl_pooja_master p
INNER JOIN tbl_customer c
INNER JOIN tbl_status_master s ON p.id = ep.puja_name
AND ep.enq_id = e.id
AND e.cust_id = c.id
AND e.enq_status = s.id
GROUP BY e.id ";




//Enquiry
//Enquiry_Puja

$result_arr_2 = array();

if ($report_type == "Enquiry") {

    if (strlen($from_date) > 9 && strlen($to_date) > 9 && isset($_GET["from_date"])) {
        $where_clause = "(e.enq_date BETWEEN '$from_date' and '$to_date' ) and ";
    }

    $query = "SELECT e.id, e.enq_no, e.enq_date, e.medium_id, e.quotation_id, e.enq_status, e.order_no,
    c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_enquiry e
INNER JOIN tbl_customer c
ON $where_clause e.cust_id = c.id
order by e.enq_date desc, e.enq_no    
";

    list($status, $id, $result_arr) = getDataByQuery($query);

    for ($i = 0; $i < count($result_arr); $i++) {

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["medium_id"], $medium_arr, 'id');
        $medium_name = $medium_arr[$data]['medium_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["source_id"], $source_arr, 'id');
        $source_name = $source_arr[$data]['source_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["enq_status"], $status_arr, 'id');
        $enq_status_name = $status_arr[$data]['status_name'];

        //list($status, $data, $msg) = searchIdInArray($result_arr[$i]["order_status"], $status_arr, 'id');
        //$order_status_name = $status_arr[$data]['status_name'];

        $result_arr_2[$i]['id'] = $result_arr[$i]['id'];
        $result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        $result_arr_2[$i]['enq_date'] = $result_arr[$i]['enq_date'];

        $result_arr_2[$i]['medium_id'] = $result_arr[$i]['medium_id'];
        $result_arr_2[$i]['medium_name'] = $medium_name;

        $result_arr_2[$i]['quotation_id'] = $result_arr[$i]['quotation_id'];

        $result_arr_2[$i]['enq_status'] = $result_arr[$i]['enq_status'];
        $result_arr_2[$i]['enq_status_name'] = $enq_status_name;

        $result_arr_2[$i]['order_no'] = $result_arr[$i]['order_no'];

        $result_arr_2[$i]['first_name'] = $result_arr[$i]['first_name'];
        $result_arr_2[$i]['last_name'] = $result_arr[$i]['last_name'];
        $result_arr_2[$i]['city'] = $result_arr[$i]['city'];
        $result_arr_2[$i]['area'] = $result_arr[$i]['area'];

        $result_arr_2[$i]['source_id'] = $result_arr[$i]['source_id'];
        $result_arr_2[$i]['source_name'] = $source_name;

        //$result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        //c.first_name, c.last_name, c.city, c.area, c.source_id, c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
    }




//download_send_headers("data_export_" . date("Y-m-d") . ".csv");
//echo array2csv($result_arr);
    echo "Total = " . count($result_arr_2);
//echo "<pre>";
//var_dump($result_arr_2);
//echo "</pre>";
//die();
    ?>

    <table id="example" class="display" cellspacing="0" width="100%" border="1px;">
        <thead>
            <tr>
                <th>No</th>
                <th>Enq No</th>
                <th>Enq Date</th>
                <th>Medium</th>
                <th>Status</th>
                <th>Order</th>

                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>Area</th>
                <th>Source</th>

            </tr>
        </thead>

        <tbody>
            <?php for ($i = 0; $i < count($result_arr_2); $i++) { ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_no']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['medium_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_status_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_no']; ?></td>

                    <td><?php echo $result_arr_2[$i]['first_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['last_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['city']; ?></td>
                    <td><?php echo $result_arr_2[$i]['area']; ?></td>
                    <td><?php echo $result_arr_2[$i]['source_name']; ?></td>
                </tr>
            <?php } ?>
        <tbody>
    </table>

    <?php
} 
else if ($report_type == "Enquiry_Puja") {

    $puja_arr = getDataFromTable("tbl_pooja_master");
//print_r($puja_arr);

    if (strlen($from_date) > 9 && strlen($to_date) > 9 && isset($_GET["from_date"])) {
        $where_clause = "(e.enq_date BETWEEN '$from_date' and '$to_date' ) and ";
    }

    $query = "SELECT ep.puja_name,ep.puja_date,ep.puja_address,
    e.id, e.enq_no, e.enq_date, e.medium_id, e.quotation_id, e.enq_status, e.order_no,
    c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_enq_puja ep
INNER JOIN tbl_enquiry e
INNER JOIN tbl_customer c
ON  $where_clause ep.enq_id = e.id 
AND e.cust_id = c.id
order by e.enq_date desc, e.id   
";

    list($status, $id, $result_arr) = getDataByQuery($query);


    for ($i = 0; $i < count($result_arr); $i++) {

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["medium_id"], $medium_arr, 'id');
        $medium_name = $medium_arr[$data]['medium_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["source_id"], $source_arr, 'id');
        $source_name = $source_arr[$data]['source_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["enq_status"], $status_arr, 'id');
        $enq_status_name = $status_arr[$data]['status_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["puja_name"], $puja_arr, 'id');
        $puja_name = $puja_arr[$data]['pooja_name'];

        // Puja Details 
        $result_arr_2[$i]['puja_id'] = $result_arr[$i]['puja_name'];
        $result_arr_2[$i]['puja_name'] = $puja_name;

        $result_arr_2[$i]['puja_date'] = $result_arr[$i]['puja_date'];
        $result_arr_2[$i]['puja_address'] = $result_arr[$i]['puja_address'];


        // Enquiry Details 
        $result_arr_2[$i]['id'] = $result_arr[$i]['id'];
        $result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        $result_arr_2[$i]['enq_date'] = $result_arr[$i]['enq_date'];

        $result_arr_2[$i]['medium_id'] = $result_arr[$i]['medium_id'];
        $result_arr_2[$i]['medium_name'] = $medium_name;

        $result_arr_2[$i]['quotation_id'] = $result_arr[$i]['quotation_id'];

        $result_arr_2[$i]['enq_status'] = $result_arr[$i]['enq_status'];
        $result_arr_2[$i]['enq_status_name'] = $enq_status_name;

        $result_arr_2[$i]['order_no'] = $result_arr[$i]['order_no'];

        // Customer Details 
        $result_arr_2[$i]['first_name'] = $result_arr[$i]['first_name'];
        $result_arr_2[$i]['last_name'] = $result_arr[$i]['last_name'];
        $result_arr_2[$i]['city'] = $result_arr[$i]['city'];
        $result_arr_2[$i]['area'] = $result_arr[$i]['area'];

        $result_arr_2[$i]['source_id'] = $result_arr[$i]['source_id'];
        $result_arr_2[$i]['source_name'] = $source_name;

        //$result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        //c.first_name, c.last_name, c.city, c.area, c.source_id, c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
    }


    echo "Total = " . count($result_arr_2);
//    echo "<pre>";
//    var_dump($result_arr_2);
//    echo "</pre>";
//die();
    ?>

    <table id="example" class="display" cellspacing="0" width="100%" border="1px;">
        <thead>
            <tr>
                <th>No</th>

                <th>Puja Name</th>
                <th>Puja Date</th>

                <th>Enq No</th>
                <th>Enq Date</th>
                <th>Medium</th>
                <th>Enq Status</th>
                <th>Order No</th>

                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>Area</th>
                <th>Source</th>

                <th>Puja Address</th>

            </tr>
        </thead>

        <tbody>
            <?php for ($i = 0; $i < count($result_arr_2); $i++) { ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>

                    <td><?php echo $result_arr_2[$i]['puja_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['puja_date']; ?></td>

                    <td><?php echo $result_arr_2[$i]['enq_no']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['medium_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_status_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_no']; ?></td>

                    <td><?php echo $result_arr_2[$i]['first_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['last_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['city']; ?></td>
                    <td><?php echo $result_arr_2[$i]['area']; ?></td>
                    <td><?php echo $result_arr_2[$i]['source_name']; ?></td>

                    <td><?php echo $result_arr_2[$i]['puja_address']; ?></td>
                </tr>
            <?php } ?>
        <tbody>
    </table>

    <?php
} 
else if ($report_type == "Order") {

    if (strlen($from_date) > 9 && strlen($to_date) > 9 && isset($_GET["from_date"])) {
        $where_clause = "(e.order_date BETWEEN '$from_date' and '$to_date' ) and ";
    }

     $query = "SELECT o.id,o.order_no,o.order_date,o.order_status,o.total_amt,o.pandit_payment_total,o.paid_to_pandit,o.payment_mode,
        o.cust_pay_receive,o.pandit_pay_status,o.comments,
        e.enq_no, e.enq_date, e.medium_id, e.enq_status, e.order_no e_order_no, 
    c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_order_tmp o
INNER JOIN tbl_enquiry e
INNER JOIN tbl_customer c
ON $where_clause o.enq_id = e.id  
AND o.cust_id = c.id    
order by o.order_date desc   
";

    list($status, $id, $result_arr) = getDataByQuery($query);

    for ($i = 0; $i < count($result_arr); $i++) {

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["medium_id"], $medium_arr, 'id');
        $medium_name = $medium_arr[$data]['medium_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["source_id"], $source_arr, 'id');
        $source_name = $source_arr[$data]['source_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["enq_status"], $status_arr, 'id');
        $enq_status_name = $status_arr[$data]['status_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["order_status"], $status_arr, 'id');
        $order_status_name = $status_arr[$data]['status_name'];

        $result_arr_2[$i]['id'] = $result_arr[$i]['id'];

        $result_arr_2[$i]['order_no'] = $result_arr[$i]['order_no'];
        $result_arr_2[$i]['order_date'] = $result_arr[$i]['order_date'];
        $result_arr_2[$i]['order_status'] = $result_arr[$i]['order_status'];
        $result_arr_2[$i]['order_status_name'] = $order_status_name;

        $result_arr_2[$i]['total_amt'] = $result_arr[$i]['total_amt'];
        $result_arr_2[$i]['pandit_payment_total'] = $result_arr[$i]['pandit_payment_total'];
        $result_arr_2[$i]['paid_to_pandit'] = $result_arr[$i]['paid_to_pandit'];
        $result_arr_2[$i]['payment_mode'] = $result_arr[$i]['payment_mode'];
        $result_arr_2[$i]['order_comments'] = $result_arr[$i]['comments'];
        //$result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        // Enquiry Details 

        $result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        $result_arr_2[$i]['enq_date'] = $result_arr[$i]['enq_date'];

        $result_arr_2[$i]['medium_id'] = $result_arr[$i]['medium_id'];
        $result_arr_2[$i]['medium_name'] = $medium_name;

        $result_arr_2[$i]['quotation_id'] = $result_arr[$i]['quotation_id'];

        $result_arr_2[$i]['enq_status'] = $result_arr[$i]['enq_status'];
        $result_arr_2[$i]['enq_status_name'] = $enq_status_name;

        $result_arr_2[$i]['order_no'] = $result_arr[$i]['order_no'];

        // Customer Details 
        $result_arr_2[$i]['first_name'] = $result_arr[$i]['first_name'];
        $result_arr_2[$i]['last_name'] = $result_arr[$i]['last_name'];
        $result_arr_2[$i]['city'] = $result_arr[$i]['city'];
        $result_arr_2[$i]['area'] = $result_arr[$i]['area'];

        $result_arr_2[$i]['source_id'] = $result_arr[$i]['source_id'];
        $result_arr_2[$i]['source_name'] = $source_name;

        //$result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        //c.first_name, c.last_name, c.city, c.area, c.source_id, c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
    }




//download_send_headers("data_export_" . date("Y-m-d") . ".csv");
//echo array2csv($result_arr);
    echo "Total = " . count($result_arr_2);
//echo "<pre>";
//var_dump($result_arr_2);
//echo "</pre>";
//die();
    ?>

    <table id="example" class="display" cellspacing="0" width="100%" border="1px;">
        <thead>
            <tr>
                <th>No</th>

                <th>Order No</th>
                <th>Order Date</th>
                <th>Order Status</th>
                <th>Total Amt</th>
                <th>Pandit Payment</th>
                <th>Paid To Pandit</th>
                <th>Payment Mode</th>

                <th>Enq No</th>
                <th>Enq Date</th>
                <th>Medium</th>
                <th>Enq Status</th>
                <th>Order No</th>

                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>Area</th>
                <th>Source</th>
                <th>Feedback</th>

            </tr>
        </thead>

        <tbody>
            <?php for ($i = 0; $i < count($result_arr_2); $i++) { ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>


                    <td><?php echo $result_arr_2[$i]['order_no']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_status_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['total_amt']; ?></td>
                    <td><?php echo $result_arr_2[$i]['pandit_payment_total']; ?></td>
                    <td><?php echo $result_arr_2[$i]['paid_to_pandit']; ?></td>
                    <td><?php echo $result_arr_2[$i]['payment_mode']; ?></td>



                    <td><?php echo $result_arr_2[$i]['enq_no']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['medium_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_status_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_no']; ?></td>

                    <td><?php echo $result_arr_2[$i]['first_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['last_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['city']; ?></td>
                    <td><?php echo $result_arr_2[$i]['area']; ?></td>
                    <td><?php echo $result_arr_2[$i]['source_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_comments']; ?></td>   
                </tr>
            <?php } ?>
        <tbody>
    </table>

    <?php
} 
else if ($report_type == "Order_Puja") {

    $puja_arr = getDataFromTable("tbl_pooja_master");
    $pandit_arr = getDataFromTable("tbl_pandit");

    if (strlen($from_date) > 9 && strlen($to_date) > 9 && isset($_GET["from_date"])) {
        $where_clause = "(op.puja_date BETWEEN '$from_date' and '$to_date' ) and ";
    }

     $query = "SELECT o.id,o.order_no,o.order_date,o.order_status,
        op.puja_id,op.puja_date,op.puja_address,op.samagri_type,op.pandit_id,
        op.total_amt,op.pandit_payment_total,op.paid_to_pandit,op.payment_mode,op.cust_pay_receive,op.pandit_pay_status,op.comments,op.puja_status,
        e.enq_no, e.enq_date, e.medium_id, e.enq_status, e.order_no e_order_no, 
        c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_order_puja_tmp op
INNER JOIN tbl_order_tmp o
INNER JOIN tbl_enquiry e
INNER JOIN tbl_customer c
ON $where_clause op.order_id = o.id
And o.enq_id = e.id  
AND o.cust_id = c.id    
order by op.puja_date desc   
";

    list($status, $id, $result_arr) = getDataByQuery($query);

    for ($i = 0; $i < count($result_arr); $i++) {

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["medium_id"], $medium_arr, 'id');
        $medium_name = $medium_arr[$data]['medium_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["source_id"], $source_arr, 'id');
        $source_name = $source_arr[$data]['source_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["enq_status"], $status_arr, 'id');
        $enq_status_name = $status_arr[$data]['status_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["order_status"], $status_arr, 'id');
        $order_status_name = $status_arr[$data]['status_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["puja_id"], $puja_arr, 'id');
        $puja_name = $puja_arr[$data]['pooja_name'];

        list($status, $data, $msg) = searchIdInArray($result_arr[$i]["pandit_id"], $pandit_arr, 'id');
        $pandit_name = $pandit_arr[$data]['first_name'] . " " . $pandit_arr[$data]['last_name'];


        $result_arr_2[$i]['id'] = $result_arr[$i]['id'];

        $result_arr_2[$i]['order_no'] = $result_arr[$i]['order_no'];
        $result_arr_2[$i]['order_date'] = $result_arr[$i]['order_date'];
        $result_arr_2[$i]['order_status'] = $result_arr[$i]['order_status'];
        $result_arr_2[$i]['order_status_name'] = $order_status_name;

        // Puja Details
        $result_arr_2[$i]['puja_id'] = $result_arr[$i]['puja_id'];
        $result_arr_2[$i]['puja_name'] = $puja_name;
        $result_arr_2[$i]['puja_date'] = $result_arr[$i]['puja_date'];
        $result_arr_2[$i]['puja_address'] = $result_arr[$i]['puja_address'];
        $result_arr_2[$i]['samagri_type'] = $result_arr[$i]['samagri_type'];
        $result_arr_2[$i]['puja_status'] = $result_arr[$i]['puja_status'];

        $result_arr_2[$i]['pandit_id'] = $result_arr[$i]['pandit_id'];
        $result_arr_2[$i]['pandit_name'] = $pandit_name;


        $result_arr_2[$i]['total_amt'] = $result_arr[$i]['total_amt'];
        $result_arr_2[$i]['pandit_payment_total'] = $result_arr[$i]['pandit_payment_total'];
        $result_arr_2[$i]['paid_to_pandit'] = $result_arr[$i]['paid_to_pandit'];
        $result_arr_2[$i]['payment_mode'] = $result_arr[$i]['payment_mode'];
        $result_arr_2[$i]['order_comments'] = $result_arr[$i]['comments'];
        //$result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        // Enquiry Details 

        $result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        $result_arr_2[$i]['enq_date'] = $result_arr[$i]['enq_date'];

        $result_arr_2[$i]['medium_id'] = $result_arr[$i]['medium_id'];
        $result_arr_2[$i]['medium_name'] = $medium_name;

        $result_arr_2[$i]['quotation_id'] = $result_arr[$i]['quotation_id'];

        $result_arr_2[$i]['enq_status'] = $result_arr[$i]['enq_status'];
        $result_arr_2[$i]['enq_status_name'] = $enq_status_name;

        $result_arr_2[$i]['order_no'] = $result_arr[$i]['order_no'];

        // Customer Details 
        $result_arr_2[$i]['first_name'] = $result_arr[$i]['first_name'];
        $result_arr_2[$i]['last_name'] = $result_arr[$i]['last_name'];
        $result_arr_2[$i]['city'] = $result_arr[$i]['city'];
        $result_arr_2[$i]['area'] = $result_arr[$i]['area'];

        $result_arr_2[$i]['source_id'] = $result_arr[$i]['source_id'];
        $result_arr_2[$i]['source_name'] = $source_name;

        //$result_arr_2[$i]['enq_no'] = $result_arr[$i]['enq_no'];
        //c.first_name, c.last_name, c.city, c.area, c.source_id, c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
    }




//download_send_headers("data_export_" . date("Y-m-d") . ".csv");
//echo array2csv($result_arr);
    echo "Total = " . count($result_arr_2);
//echo "<pre>";
//var_dump($result_arr_2);
//echo "</pre>";
//die();
    ?>

    <table id="example" class="display" cellspacing="0" width="100%" border="1px;">
        <thead>
            <tr>
                <th>No</th>

                <th>Order No</th>
                <th>Order Date</th>
                <th>Order Status</th>

                <th>Puja Status</th>
                <th>Puja Name</th>
                <th>Puja_date</th>
                <th>Samagri Type</th>
                <th>Pandit Name</th>

                <th>Total Amt</th>
                <th>Pandit Payment</th>
                <th>Paid To Pandit</th>
                <th>Payment Mode</th>

                <th>Puja Add</th>

                <th>Enq No</th>
                <th>Enq Date</th>
                <th>Medium Id</th>
                <th>Enq Status</th>
                <th>Order No</th>

                <th>First Name</th>
                <th>Last Name</th>
                <th>City</th>
                <th>Area</th>
                <th>Source</th>
                <th>Feedback</th>

            </tr>
        </thead>

        <tbody>
            <?php for ($i = 0; $i < count($result_arr_2); $i++) { ?>
                <tr>
                    <td><?php echo $i + 1; ?></td>


                    <td><?php echo $result_arr_2[$i]['order_no']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_status_name']; ?></td>


                    <td><?php echo $result_arr_2[$i]['puja_status']; ?></td>
                    <td><?php echo $result_arr_2[$i]['puja_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['puja_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['samagri_type']; ?></td>

                    <td><?php echo $result_arr_2[$i]['pandit_name']; ?></td>

                    <td><?php echo $result_arr_2[$i]['total_amt']; ?></td>
                    <td><?php echo $result_arr_2[$i]['pandit_payment_total']; ?></td>
                    <td><?php echo $result_arr_2[$i]['paid_to_pandit']; ?></td>
                    <td><?php echo $result_arr_2[$i]['payment_mode']; ?></td>

                    <td><?php echo $result_arr_2[$i]['puja_address']; ?></td>

                    <td><?php echo $result_arr_2[$i]['enq_no']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_date']; ?></td>
                    <td><?php echo $result_arr_2[$i]['medium_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['enq_status_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_no']; ?></td>

                    <td><?php echo $result_arr_2[$i]['first_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['last_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['city']; ?></td>
                    <td><?php echo $result_arr_2[$i]['area']; ?></td>
                    <td><?php echo $result_arr_2[$i]['source_name']; ?></td>
                    <td><?php echo $result_arr_2[$i]['order_comments']; ?></td>   
                </tr>
            <?php } ?>
        <tbody>
    </table>

    <?php
}

else if ($report_type == "Custome") {

    
    $query = "SELECT e.id, e.enq_no, e.enq_date, e.medium_id, e.quotation_id, e.enq_status, e.order_no,
    c.first_name, c.last_name, c.city, c.area, c.source_id, e.cancellation_reason
FROM tbl_enquiry e
INNER JOIN tbl_customer c
ON  e.cust_id = c.id
order by e.enq_date desc, e.enq_no    
";

    list($status, $id, $result_arr) = getDataByQuery($query);

    




//download_send_headers("data_export_" . date("Y-m-d") . ".csv");
//echo array2csv($result_arr);
    //echo "Total = " . count($result_arr_2);
//echo "<pre>";
//var_dump($result_arr_2);
//echo "</pre>";
//die();
    
} 
else{
    echo "Select Proper Type";
    exit;
}

//header("Content-type: application/vnd-ms-excel");
?>


