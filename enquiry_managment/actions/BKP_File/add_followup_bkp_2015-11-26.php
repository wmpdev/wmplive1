<?php

//include_once '../function/function.php';

include_once '../config/auth.php';

global $con;
//print_r($_POST);
//exit;
$error = FALSE;
$error_msg = "";
$regex = "/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i";

for ($i = 0; $i < count($_POST); $i++) {
    $_POST[$key] = trim($_POST[$key]);
}

// Array ( [enq_id] => 6 [followup_date] => 20-08-2015 [followup_time] => 12:10 PM [comment] => sdf [next_date] => 20-08-2015 [next_time] => 12:15 PM [enq_status] => 3 [monitor_by] => 2 )  


$edit_id = $_POST["edit_id"];

$enq_id = $_POST["enq_id"];
$followup_date = $_POST["followup_date"];
$followup_time = $_POST["followup_time"];
$comment = $_POST["comment"];
$next_date = $_POST["next_date"];
$next_time = $_POST["next_time"];
$enq_status = $_POST["enq_status"];
$monitor_by = $_POST["monitor_by"];






if (!strlen(trim($enq_id)) > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Select proper Enquiry.";
    $error_msg = getError($error_msg, "Select proper Enquiry.");
}

if (!(strlen(trim($followup_date)) > 0) || !validateDate($followup_date, $format = 'DD-MM-YYYY')) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Followup date.";
    $error_msg = getError($error_msg, "Enter proper Followup date.");
}

if (!strlen(trim($followup_time)) > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Followup time.";
    $error_msg = getError($error_msg, "Enter proper Followup time.");
}

if (!$enq_status > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Select proper Status Followup time.";
    $error_msg = getError($error_msg, "Select proper Status.");
}

if ($enq_status == 5) {
    $next_date = $followup_date;
    $next_time = $followup_time;
} else {
    if (!(strlen(trim($next_date)) > 0) || !validateDate($next_date, $format = 'DD-MM-YYYY')) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup date.";
        $error_msg = getError($error_msg, "Enter proper Next Followup date.");
    }

    if (!strlen(trim($next_time)) > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
        $error_msg = getError($error_msg, "Enter proper Next Followup time.");
    }
}




if (!strlen(trim($comment)) > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
    $error_msg = getError($error_msg, "Enter proper Comments.");
}



/*
  if (!$monitor_by > 0) {
  $error = TRUE;
  //$error_msg = $error_msg . "<br/>- Select proper Monitor.";
  $error_msg = getError($error_msg, "Select proper Monitor.");
  }
 */

//$comment = mysql_real_escape_string($comment);
//$comment = $con->real_escape_string($comment);
$comment = htmlspecialchars($comment, ENT_QUOTES);


//$followup_date = date("Y-m-d", strtotime($followup_date));
//$followup_time = date("H:i", strtotime($followup_time));
//$next_date = date("Y-m-d", strtotime($next_date));
//$next_time = date("H:i", strtotime($next_time));


if (strlen($followup_date) > 0 || strlen($followup_time) > 0) {
    list($status, $db_date, $msg) = validateDateTime($followup_date, $followup_time);
    if (!$status) {
        $error = TRUE;
        $msg = "Enter proper Followup Date Time";
        $error_msg = getError($error_msg, $msg);
    } else {
        $followup_date = $db_date;
    }
}

if (strlen($next_date) > 0 || strlen($next_time) > 0) {
    list($status, $db_date, $msg) = validateDateTime($next_date, $next_time);
    if (!$status) {
        $error = TRUE;
        $msg = "Enter proper Next Followup Date Time";
        $error_msg = getError($error_msg, $msg);
    } else {
        $next_date = $db_date;
    }
}

if (strtotime($followup_date) > strtotime($next_date)) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Select proper Monitor.";
    $error_msg = getError($error_msg, "Followup Date-Time should be greater than current Date-Time.");
}






if ($error == TRUE) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
    echo $error_msg;
    return 0;
}



$data_sql = " tbl_enq_followup set "
        . "enq_id = " . $enq_id
        . ", add_enq_date = '" . $followup_date
        //. "', add_enq_time = '" . $followup_time
        . "', next_date = '" . $next_date
        //. "', next_time = '" . $next_time
        . "', comment = '" . $comment
        . "', enq_status = " . $enq_status
        . ", monitor_by = " . $monitor_by;



//        . "add_date = '" . $cust_id
//        . "', mod_date = '" . $cust_id
//        . "', add_user = " . $cust_id
//        . "mod_user = '" . $cust_id


list($status, $id, $msg) = addFollowup($data_sql);
//echo "status[$status], id[$id], msg[$msg]";
/*
$next_date = date("d-m-Y", strtotime($next_date));
$next_time = date("h:i a", strtotime($next_time));

list($status, $db_date, $msg) = validateDateTime($next_date, $next_time);
if (!$status) {
    $error = TRUE;
    $msg = "Enter proper Followup Date Time";
    $error_msg = getError($error_msg, $msg);
} else {
    $enq_date = $db_date;
}
*/

$data_sql = " tbl_enquiry set "
        . "enq_status = " . $enq_status
        . ",followup_date = '" . $next_date
        . "', ";

//echo "<br/> enquiry_query = " . $data_sql;

list($status, $id, $msg) = add_enquiry($enq_id, $data_sql); // addEnqQuotation($edit_id, $data_sql)F


if ($status == 0) {
    //echo $error_msg;
    $error = TRUE;
    $error_msg = getError($error_msg, $msg);

    $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
    echo $error_msg;
    return 0;
}

if ($status == 1) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-success alert-dismissable">        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $msg . '                        
                </div>';
    echo $error_msg;
    return 0;
}


echo "<br/>Enquiry Success = enq_no = [$enq_no], puja = [$puja_name], source = [$enq_source], medium = [$enq_medium], lang = [$puja_name], cast = [$puja_name]";
return 0;
?>

