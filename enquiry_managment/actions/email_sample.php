

<!DOCTYPE html>
<html>
    <body>
        <div align="center" style="width:100%">
            <div align="center" style="background-color:#FFF;max-width:700px;min-width:200px">

                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>General Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Order </td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">0000001</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">28-10-2015 11:51 am</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Invoice Generated</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Enquiry </td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">0000001</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">28-10-2015 11:51 am</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">&nbsp;</td>
                    </tr>

                    </tbody>
                </table>

                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Customer Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Name </td>
                        <td colspan="3" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">0000001</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Contact </td>

                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">ninad.tilkar@choiceindia.com</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">8108869247</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Address </td>
                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top; width: 200px;">0000001</td>

                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">kalyan <br/>Mumbai</td>
                    </tr>

                    </tbody>
                </table>




                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Puja Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Puja Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Satyanarayanan</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">28-10-2015 11:51 AM</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Samagri </td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Pandit Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Mohan Shastri</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">8108869247</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Type :- Gila + Sukha   </td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Address </td>

                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Paid</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;"> Arranged By :- Pandi   </td>
                    </tr>
                    <tr><td colspan="4" class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;line-height: 5px;vertical-align: top;">&nbsp;</td></tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Puja Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Satyanarayanan</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">28-10-2015 11:51 AM</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Samagri </td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Pandit Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Mohan Shastri</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">8108869247</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Type :- Gila + Sukha   </td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Address </td>

                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Paid</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;"> Arranged By :- Pandi   </td>
                    </tr>


                    </tbody>
                </table>




                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Payment Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Payment Mode</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Online</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Collected By</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Ninad</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Total Amount </td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">100000</td>
                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Paid</td>

                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Pandit Payment</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">100000</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Pending</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">500 Paid</td>
                    </tr>

                    </tbody>
                </table>


                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Other Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Handled By</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Prakash</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Enquiry By</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Kritika</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Feed Back </td>                        
                        <td colspan="3" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;"></td>

                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Comments</td>
                        <td colspan="3" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">olhasdl aoihda sihjoasdn ;aoshd o</td>

                    </tr>

                    </tbody>
                </table>



            </div>
        </div>
    </body>
</html>


