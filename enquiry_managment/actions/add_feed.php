<?php

//include_once '../function/function.php';

include_once '../config/auth.php';


//print_r($_POST);
//echo "FILES =" . json_encode($_FILES) . "<br><br>";
//echo "POST =" . json_encode($_POST) . "<br>";
//exit;

$error = FALSE;
$error_msg = "";

for ($i = 0; $i < count($_POST); $i++) {
    $_POST[$key] = trim($_POST[$key]);
}

// Array ( [edit_id] => 70 [feed_type] => 6 [pub_date] => 14-12-2015 [feed_url] => [feed_title] => Mahadev and Parvati Wedding [date_english] => English date [feed_desc] => English Content [feed_title_hindi] => Hindi Title [date_hindi] => Hindi date [feed_desc_hindi] => Hindi Content ) 
//$cust_id = $_POST["cust_id"];



$edit_id = $_POST["edit_id"];

$feed_type = htmlspecialchars($_POST["feed_type"], ENT_QUOTES); //$_POST["feed_type"];
$pub_date = htmlspecialchars($_POST["pub_date"], ENT_QUOTES); //$_POST["pub_date"];
$feed_url = htmlspecialchars($_POST["feed_url"], ENT_QUOTES); //$_POST["feed_url"];


$feed_title = htmlspecialchars($_POST["feed_title"], ENT_QUOTES); 
$date_english = htmlspecialchars($_POST["date_english"], ENT_QUOTES); 
$feed_desc = htmlspecialchars($_POST["feed_desc"], ENT_QUOTES); //$_POST["feed_desc"];


$feed_title_hindi = htmlspecialchars($_POST["feed_title_hindi"], ENT_QUOTES); 
$date_hindi = htmlspecialchars($_POST["date_hindi"], ENT_QUOTES);
$feed_desc_hindi = htmlspecialchars($_POST["feed_desc_hindi"], ENT_QUOTES);


$upload_file_path_db ="";

/*
$data_sql = " tbl_feeds set type = " . $feed_type . ", url = '" . $feed_url. "', publish_date = '" . $pub_date  
        . " title = '" . $feed_title . "', date_english = '" . $date_english. "', description = '" . $feed_desc."' ".$upload_file_path_db 
        . " title_hindi = '" . $feed_title_hindi . "', date_hindi = '" . $date_hindi. "', desc_hindi = '" . $feed_desc_hindi."' ".$upload_file_path_hindi
        ." , ";
        //. "', publish_date = '" . $pub_date        
        //. "', description = '" . $feed_desc
        //. "', url = '" . $feed_url
        //. "', type = " . $feed_type
        //." ".$upload_file_path_db 
        //_db 
        //." , hindi_desc = '" . $feed_desc_hindi."'$upload_file_path_hindi_db" 
        
*/




if (!strlen(trim($feed_title)) > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Number";
    $error_msg = getError($error_msg, "Enter proper Feed Title");
}


if (!(strlen(trim($pub_date)) > 0) || !validateDate($pub_date, $format = 'DD-MM-YYYY')) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Date";
    $error_msg = getError($error_msg, "Enter proper Publish Date");
}

if (!($feed_type > 0)) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Number";
    $error_msg = getError($error_msg, "Select proper Feed Type");
}

if (!strlen(trim($feed_desc)) > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Number";
    $error_msg = getError($error_msg, "Enter proper Feed Description");
}


//if (!strlen(trim($feed_url)) > 0) {
//    $error = TRUE;
//    //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Number";
//    $error_msg = getError($error_msg, "Enter proper Feed Url");
//}



if (strlen($_FILES['feed_img']['name'])>3 && ($error == FALSE)) {
    $errors = array();
    $file_name = $_FILES['feed_img']['name'];
    $file_size = $_FILES['feed_img']['size'];
    $file_tmp = $_FILES['feed_img']['tmp_name'];
    $file_type = $_FILES['feed_img']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['feed_img']['name'])));

    $current_time = time();
    $upload_file_path_db = "/feeds/feeds_img/".$current_time.".".$file_ext;
    $upload_file_path = MEDIA_PATH.$upload_file_path_db;
    
    
    $expensions = array("jpeg", "jpg", "png");

    if (in_array($file_ext, $expensions) === false) {
        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
        $error = TRUE;
        $error_msg = getError($error_msg, "Extension not allowed, please choose a JPEG or PNG file.");
    }

    if ($file_size > 2097152) {
        $errors[] = 'File size must be excately 2 MB';
        $error = TRUE;
        $error_msg = getError($error_msg, "File size must be excately 2 MB.");
    }

    if (empty($errors) == true) {
        //move_uploaded_file($file_tmp, "images/" . $file_name);
        //echo "Success";
        if (move_uploaded_file($file_tmp, $upload_file_path)) {
            //echo "File is valid, and was successfully uploaded.\n";
            $error_msg = getError($error_msg, "Wornning :- Memory_use -> File is valid, and was successfully uploaded.");
        } else {
            //echo "Possible file upload attack!\n";
            $error = TRUE;
            $error_msg = getError($error_msg, "Possible file upload attack! Upload_Path = ".$upload_file_path);
        }
        
    } else {
        //print_r($errors);
        $error = TRUE;
    }
}

if (strlen($_FILES['feed_img_hindi']['name'])>3 && ($error == FALSE)) {
    $errors = array();
    $file_name = $_FILES['feed_img_hindi']['name'];
    $file_size = $_FILES['feed_img_hindi']['size'];
    $file_tmp = $_FILES['feed_img_hindi']['tmp_name'];
    $file_type = $_FILES['feed_img_hindi']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['feed_img_hindi']['name'])));

    $current_time = time();
    $upload_file_path_hindi_db = "/feeds/feeds_img/".$current_time."_hindi.".$file_ext;
    $upload_file_path = MEDIA_PATH.$upload_file_path_hindi_db;
    
    
    $expensions = array("jpeg", "jpg", "png");

    if (in_array($file_ext, $expensions) === false) {
        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
        $error = TRUE;
        $error_msg = getError($error_msg, "Extension not allowed, please choose a JPEG or PNG file.");
    }

    if ($file_size > 2097152) {
        $errors[] = 'File size must be excately 2 MB';
        $error = TRUE;
        $error_msg = getError($error_msg, "File size must be excately 2 MB.");
    }

    if (empty($errors) == true) {
        //move_uploaded_file($file_tmp, "images/" . $file_name);
        //echo "Success";
        if (move_uploaded_file($file_tmp, $upload_file_path)) {
            //echo "File is valid, and was successfully uploaded.\n";
            $error_msg = getError($error_msg, "Wornning :- Memory_use -> Hindi File is valid, and was successfully uploaded.");
        } else {
            //echo "Possible file upload attack!\n";
            $error = TRUE;
            $error_msg = getError($error_msg, "Possible file upload attack! Hindi_Upload_Path = ".$upload_file_path);
        }
        
    } else {
        //print_r($errors);
        $error = TRUE;
    }
}



/*
  if (!(strlen(trim($puja_date)) > 0) || !validateDate($puja_date, $format = 'DD-MM-YYYY')) {
  $error = TRUE;
  //$error_msg = $error_msg . "<br/>- Enter proper Puja Date";
  $error_msg = getError($error_msg, "Enter proper Puja Date");
  }
 */

$puja_query = "";







if ($error == TRUE) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
    echo $error_msg;
    return 0;
}


$pub_date = date("Y-m-d H:i:s", strtotime($pub_date));
//$puja_date = date("Y-m-d", strtotime($puja_date));
//$puja_time = date("H:i", strtotime($puja_time));

if(strlen($upload_file_path_db)>2){
    
   $upload_file_path_db = htmlspecialchars($upload_file_path_db, ENT_QUOTES);
   $upload_file_path_db = ", image = '" . $upload_file_path_db."' ";
}
else{
    $upload_file_path_db = "";
}


if(strlen($upload_file_path_hindi_db)>2){
    
   $upload_file_path_hindi_db = htmlspecialchars($upload_file_path_hindi_db, ENT_QUOTES);
   $upload_file_path_hindi_db = ", img_hindi = '" . $upload_file_path_hindi_db."' ";
}
else{
    $upload_file_path_hindi_db = "";
}




/*
$data_sql = " tbl_feeds set "
        . " title = '" . $feed_title
        . "', publish_date = '" . $pub_date        
        . "', description = '" . $feed_desc
        . "', url = '" . $feed_url
        . "', type = " . $feed_type
        ." ".$upload_file_path_db 
        ." , hindi_desc = '" . $feed_desc_hindi."'$upload_file_path_hindi_db" 
        ." , "; */


$data_sql = " tbl_feeds set type = " . $feed_type . ", url = '" . $feed_url. "', publish_date = '" . $pub_date  
    . "', title = '" . $feed_title . "', date_english = '" . $date_english. "', description = '" . $feed_desc."' ".$upload_file_path_db 
    . ", title_hindi = '" . $feed_title_hindi . "', date_hindi = '" . $date_hindi. "', desc_hindi = '" . $feed_desc_hindi."' ".$upload_file_path_hindi_db
    ." , ";




list($status, $id, $msg) = addFeeds($edit_id, $data_sql);
$error_msg = getError($error_msg, $msg);
//echo "status[$status], id[$id], msg[$msg]";


if ($status == 0) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
    echo $error_msg;
    return 0;
}

if ($status == 1) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-success alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $msg . '
                        <a class="alert-link" href="add_feeds.php?edit_id=' . $id . '" style="margin-left:20px;">Refresh</a>
                </div>';
    echo $error_msg;
    return 0;
}


echo "<br/>Enquiry Success = Short SQL = $data_sql";
return 0;
?>


