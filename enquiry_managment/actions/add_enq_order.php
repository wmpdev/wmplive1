<?php

//include_once '../function/function.php';

include_once '../config/auth.php';

global $con;
//print_r($_POST);
//exit;

$error = FALSE;
$error_msg = "";


for ($i = 0; $i < count($_POST); $i++) {
    $_POST[$key] = trim($_POST[$key]);
}

// Array ( [order_enq_id] => 20 [ACTION] => AddOrder [invoice_no] => Invoice No 123 [invoice_prepared] => Invoice Prepared By [invoice_mailed] => No [link_sent] => Yes [link_url] => Link Url 123 [order_comments] => Link Url 123 comments [order_no] => order 123 [order_status] => 15 [payment_status] => Pending [approved_by] => 4 )

if ($_POST["ACTION"] == "AddOrder") {


    $enq_id = $_POST["order_enq_id"];
    $order_status = $_POST["order_status"];
    $link_sent = $_POST["link_sent"];
    $link_url = $_POST["link_url"];
    $order_no = $_POST["order_no"];
    $invoice_no = $_POST["invoice_no"];
    $invoice_prepared = $_POST["invoice_prepared"];
    $invoice_mailed = $_POST["invoice_mailed"];
    $order_comments = $_POST["order_comments"];
    $payment_status = $_POST["payment_status"];
    $approved_by = $_POST["approved_by"];


    if (!$enq_id > 0) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Select proper Enquiry.");
    }

    if (!$order_no > 0) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Enter proper Order No");
    }

    if ($enq_order_no != $order_no) {
        list($status, $id, $msg) = searchOrderTmpEnqBy("order_no", "'$order_no'");
        //echo "$status, $id, $msg";
        if ($status == 1 && $edit_id != $id) {
            $error = TRUE;
            $error_msg = getError($error_msg, "Order No is Already present, Select other Order No");
        }
    }

    $order_comments = htmlspecialchars($order_comments, ENT_QUOTES);


    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }


    $data_sql = " tbl_enquiry set "
            . "order_status_id = " . $order_status
            . ", order_no = '" . $order_no
            . "', payment_status = '" . $payment_status
            . "', link_sent = '" . $link_sent
            . "', link_url = '" . $link_url
            . "', Invoice_prepared_by = '" . $invoice_prepared
            . "', invoice_no = '" . $invoice_no
            . "', invoice_mailed = '" . $invoice_mailed
            . "', feedback = '" . $order_comments
            . "', approved_by_id = " . $approved_by
            . ", ";



//        . "add_date = '" . $cust_id
//        . "', mod_date = '" . $cust_id
//        . "', add_user = " . $cust_id
//        . "mod_user = '" . $cust_id
//list($status, $id, $msg) = addFollowup($data_sql);
    list($status, $id, $msg) = add_enquiry($enq_id, $data_sql); // addEnqQuotation($edit_id, $data_sql)
//echo "status[$status], id[$id], msg[$msg]";


    if ($status == 0) {
        //echo $error_msg;
        $error = TRUE;
        $error_msg = getError($error_msg, $msg);

        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }


    if ($status == 1) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-success alert-dismissable">        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $msg . ',
                        To Convert Order Click <a class="alert-link" href="add_enquiry.php?edit_id=' . $enq_id . '&tab=Final-Details">Refresh</a>
                </div>';
        echo $error_msg;
        return 0;
    }

    // End
} elseif ($_POST["ACTION"] == "ConvertOrder") {



    $enq_id = $_POST["order_enq_id"];






    // if ($status == 1) {
    //$enq_id = $_POST["order_enq_id"];
    list($enq_status, $id, $enq_data_arr) = getEnqData($enq_id);
    if ($enq_status == 1) {

        //echo "<br/><br/>Enquiry Data<br/>"; print_r($enq_data_arr);

        $enq_no = $enq_data_arr[0]["enq_no"];
        $enq_date = $enq_data_arr[0]["enq_date"];
        //$enq_time = $_POST["enq_time"];
        $order_no = $enq_data_arr[0]["order_no"];
        //$order_date = $_POST["order_date"];
        //$order_time = $_POST["order_time"];
        $order_status = $enq_data_arr[0]["order_status_id"];
        //$order_status_arr = getStatus(0, $order_status);
        //$order_status = $order_status_arr[0]["status_name"];

        $cust_id = $enq_data_arr[0]["cust_id"];
        $quotation_id = $enq_data_arr[0]["quotation_id"];

        //list($status, $id, $enq_puja_quotation_arr) = getEnqQuotation(0, $enq_id);
    } else {
        $error = TRUE;
        $error_msg = getError($error_msg, "Enquiry Details Not Present to convert order");
    }

    list($order_no_status, $id, $msg) = searchOrderTmpEnqBy("order_no", "'$order_no'");
    //echo "$status, $id, $msg";
    if ($order_no_status == 1 && $edit_id != $id) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Order No is Already present, Not Able to convert Again");
    }

    /*
      list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);
      if ($cust_status == 1) {
      //echo "<br/><br/>Customer Data<br/>"; print_r($cust_data_arr);

      $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
      $cust_no = $cust_data_arr["phone_1"];
      $cust_email = $cust_data_arr["email_1"];
      $cust_city = $cust_data_arr["city"];
      $cust_area = $cust_data_arr["area"];
      $cust_address = $cust_data_arr["address_1"];
      } else {
      $error = TRUE;
      $error_msg = getError($error_msg, "Customer Details Not Present to convert order");
      }
     */

    list($quotation_status, $id, $quotation_data_arr) = getEnqQuotation($quotation_id, $enq_id);
    if ($quotation_status == 1) {
        //echo "<br/><br/>Quotation Data<br/>";
        //print_r($quotation_data_arr);

        $cust_service_request = $quotation_data_arr[0]["samagri_type"];
        $total_amount = $quotation_data_arr[0]["total"];
        $pandit_payment = $quotation_data_arr[0]["actual_cost"];
        //$paid_to_pandit = $_POST["paid_to_pandit"];
        //$payment_mode = $_POST["payment_mode"];
        //$cust_payment_receive_status = $_POST["cust_payment_receive_status"];
        //$pandit_payment_receive_status = $_POST["pandit_payment_receive_status"];
        //$collected_by = $_POST["collected_by"];
        //list($status, $id, $enq_puja_arr) = getEnqPujaData(0, $enq_id);
    } else {
        $error = TRUE;
        $error_msg = getError($error_msg, "Quotation Details Not Present to convert order");
    }



    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }



    $order_sql = " tbl_order_tmp set order_no = '$order_no', order_status = '$order_status', enq_id = $enq_id, enq_no = '$enq_no', enq_date = '$enq_date', cust_id = $cust_id, service_req = '$cust_service_request', total_amt = $total_amount, pandit_payment_total = $pandit_payment";

    //echo "<br/><br/><br/> Order_query = " . $order_sql;


    list($order_status, $order_id, $msg) = addOrderTmp(0, $order_sql); // addEnqQuotation($edit_id, $data_sql)
//echo "status[$status], id[$id], msg[$msg]";


    if ($order_status == 0) {
        //echo $error_msg;
        $error = TRUE;
        $error_msg = getError($error_msg, $msg);

        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }



    $data_sql = " tbl_enquiry set "
            . "order_id = " . $order_id
            . ", ";

    //echo "<br/> enquiry_query = " . $data_sql;
    
    list($enq_status, $id, $msg) = add_enquiry($enq_id, $data_sql); // addEnqQuotation($edit_id, $data_sql)

    if ($enq_status == 0) {
        //echo $error_msg;
        $error = TRUE;
        $error_msg = getError($error_msg, $msg);

        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- Not Able to Update enquiry while Converting to Order Details : enquiry id [' . $enq_id . '], Order No [' . $order_no . '], New Order id [' . $order_id . ']<br/> 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }





    list($puja_status, $id, $puja_data_arr) = getEnqPujaData(0, $enq_id);
    if ($puja_status == 1) {
        //echo "<br/><br/>Puja Data<br/>"; print_r($puja_data_arr);

        for ($i = 0; $i < count($puja_data_arr); $i++) {

            $puja_id = $puja_data_arr[$i]["puja_name"];
            //list($status, $id, $puja_list_arr) = getPujaData($puja_name);    //print_r($puja_list_arr);                
            //$puja_name = $puja_list_arr["pooja_name"];

            $puja_date = $puja_data_arr[$i]["puja_date"];
            //list($puja_date, $puja_time) = validateDbDateTime($puja_date);
            //$puja_time = $_POST["puja_time_1"];
            //$samagri_arranged_by_1 = $_POST["samagri_arranged_by_1"];
            $puja_address = $puja_data_arr[$i]["puja_address"];
            $samagri_type = $puja_data_arr[$i]["samagri_type"];


            $puja_sql = " tbl_order_puja_tmp set order_id = $order_id, puja_id = $puja_id, puja_date = '$puja_date', puja_address = '$puja_address',  samagri_type = '$samagri_type'";

            //echo "<br/> Puja_query = " . $puja_sql;

            list($puja_status, $id, $msg) = addOrderPujaTmp(0, $puja_sql);
            if ($puja_status == 0) {
                //echo $error_msg;
                $error = TRUE;
                $error_msg = getError($error_msg, $msg);
            }
        }
        //end
    }

    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }


    if ($error == FALSE) {
        //echo $error_msg;
        $msg = "Enquiry Is successfully converted into the order, ";
        $error_msg = '<div class="alert alert-success alert-dismissable">        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $msg . '
                        To check Order Click <a class="alert-link" href="add_order_tmp.php?edit_id=' . $order_id . '">Refresh</a>
                </div>';
        echo $error_msg;
        return 0;
    }



    //end
} else {

    $error = TRUE;
    $error_msg = getError($error_msg, "No Action Specified");

    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }
}




//echo "<br/>Enquiry Success = enq_no = [$enq_no], puja = [$puja_name], source = [$enq_source], medium = [$enq_medium], lang = [$puja_name], cast = [$puja_name]";
return 0;
?>

