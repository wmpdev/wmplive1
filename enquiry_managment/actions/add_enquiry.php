<?php

//include_once '../function/function.php';

include_once '../config/auth.php';


//print_r($_POST);
//exit();
$error = FALSE;
$error_msg = "";

for ($i = 0; $i < count($_POST); $i++) {
    $_POST[$key] = trim($_POST[$key]);
}

// Array ( [enq_no] => 00001363 [cust_id] => 5 [edit_id] => [enq_date] => 16-11-2015 [enq_medium] => 7 [enq_muhurat] => [enq_comment] => [enq_pandit_lang] => 0 [enq_pandit_cast] => 0 [enq_pandit_specification] => [imp_points] => ) 
//$cust_id = $_POST["cust_id"];

$edit_id = $_POST["edit_id"];
$cust_id = $_POST["cust_id"];
$enq_no = $_POST["enq_no"];
$enq_date = $_POST["enq_date"];
$enq_time = $_POST["enq_time"];
//$puja_name = $_POST["puja_name"];
//$puja_name_txt = $_POST["puja_name_txt"];
//$puja_date = $_POST["puja_date"];
//$puja_time = $_POST["puja_time"];
$enq_source = $_POST["enq_source"];
//$enq_source_txt = $_POST["enq_source_txt"];
$enq_medium = $_POST["enq_medium"];
//$enq_medium_txt = $_POST["enq_medium_txt"];
$enq_comment = $_POST["enq_comment"];
$enq_muhurat = $_POST["enq_muhurat"];
$enq_pandit_lang = $_POST["enq_pandit_lang"];
//$enq_pandit_lang_txt = $_POST["enq_pandit_lang_txt"];
$enq_pandit_cast = $_POST["enq_pandit_cast"];
//$enq_pandit_cast_txt = $_POST["enq_pandit_cast_txt"];
$enq_pandit_specification = $_POST["enq_pandit_specification"];
//$samagri_type = $_POST["samagri_type"];
$imp_points = $_POST["imp_points"];

$enq_puja_1 = $_POST["enq_puja_1"];
$enq_puja_2 = $_POST["enq_puja_2"];
$enq_puja_3 = $_POST["enq_puja_3"];
$enq_puja_4 = $_POST["enq_puja_4"];


$puja_date_1 = $_POST["puja_date_1"];
$puja_date_2 = $_POST["puja_date_2"];
$puja_date_3 = $_POST["puja_date_3"];
$puja_date_4 = $_POST["puja_date_4"];

$puja_time_1 = $_POST["puja_time_1"];
$puja_time_2 = $_POST["puja_time_2"];
$puja_time_3 = $_POST["puja_time_3"];
$puja_time_4 = $_POST["puja_time_4"];


if ((!strlen(trim($cust_id)) > 0) && !($cust_id > 0)) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Please Select proper customer";
    $error_msg = getError($error_msg, "Please Select proper customer");
}

if (!trim($enq_no) > 0) {
    $error = TRUE;
    //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Number";
    $error_msg = getError($error_msg, "Enter proper Enquiry Number");
} else {
    list($status, $id, $msg) = searchEnqNo($enq_no);
    //echo "$status, $id, $msg";
    if ($status == 1 && $edit_id != $id) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Enquiry No is Already present");
    }
}

list($status, $db_date, $msg) = validateDateTime($enq_date, $enq_time);
if (!$status) {
    $error = TRUE;
    $msg = "Enter proper Enquiry Date Time";
    $error_msg = getError($error_msg, $msg);
} else {
    $enq_date = $db_date;
}



/*

  if (!(strlen(trim($enq_date)) > 0) || !validateDate($enq_date, $format = 'DD-MM-YYYY')) {
  $error = TRUE;
  //$error_msg = $error_msg . "<br/>- Enter proper Enquiry Date";
  $error_msg = getError($error_msg, "Enter proper Enquiry Date");
  }


  if (!(strlen(trim($puja_date)) > 0) || !validateDate($puja_date, $format = 'DD-MM-YYYY')) {
  $error = TRUE;
  //$error_msg = $error_msg . "<br/>- Enter proper Puja Date";
  $error_msg = getError($error_msg, "Enter proper Puja Date");
  }
 */

$puja_query = "";
/*
  if (!($enq_puja_1 > 0) && !($enq_puja_2 > 0) && !($enq_puja_3 > 0) && !($enq_puja_4 > 0)) {
  $error = TRUE;
  $error_msg = getError($error_msg, "Select Atleast One Puja");
  } else {

  list($status, $date, $msg) = validatePujaData($enq_puja_1, $puja_date_1, $puja_time_1);

  if ($status == 1) {
  $error = TRUE;
  $error_msg = getError($error_msg, "Error In Puja 1 - $msg");
  } else {
  if ($enq_puja_1 > 0)
  $puja_query .= ", puja_id_1 = $enq_puja_1,puja_date_1 = '$date'";
  }
  //echo "<br/>[$status], [$date], [$msg], [$puja_query]<br/>";

  list($status, $date, $msg) = validatePujaData($enq_puja_2, $puja_date_2, $puja_time_2);
  //echo "<br/>[$status], [$date], [$msg]<br/>";
  if ($status == 1) {
  $error = TRUE;
  $error_msg = getError($error_msg, "Error In Puja 2 - $msg");
  //$error_msg = getError($error_msg, $msg);
  } else {
  if ($enq_puja_2 > 0)
  $puja_query .= ", puja_id_2 = $enq_puja_2,puja_date_2 = '$date'";
  }
  //echo "<br/>[$status], [$date], [$msg], [$puja_query]<br/>";

  list($status, $date, $msg) = validatePujaData($enq_puja_3, $puja_date_3, $puja_time_3);
  if ($status == 1) {
  $error = TRUE;
  $error_msg = getError($error_msg, "Error In Puja 3 - $msg");
  //$error_msg = getError($error_msg, $msg);
  } else {
  if ($enq_puja_3 > 0)
  $puja_query .= ", puja_id_3 = $enq_puja_3,puja_date_3 = '$date'";
  }

  list($status, $date, $msg) = validatePujaData($enq_puja_4, $puja_date_4, $puja_time_4);
  if ($status == 1) {
  $error = TRUE;
  $error_msg = getError($error_msg, "Error In Puja 4 - $msg");
  //$error_msg = getError($error_msg, $msg);
  } else {
  if ($enq_puja_4 > 0)
  $puja_query .= ", puja_id_4 = $enq_puja_4,puja_date_4 = '$date'";
  }
  }
 */

//echo "<br/>Puja Query = [$puja_query]<br/> error = [$error_msg]. <br/>";

/*  Time Validation
  list($status, $time, $msg) = validateTime($puja_time, $format = 'H:i:s');
  if ($status == 0) {
  $error = TRUE;
  //$error_msg = $error_msg . "<br/>- Enter proper Puja Date.";
  $error_msg = getError($error_msg, "Enter proper Puja Time.");
  }
 */






// New Value added 
/*
  if (strlen(trim($puja_name_txt)) > 0) {

  list($status, $id, $msg) = add_puja($puja_name_txt);
  if ($status == 0) {
  $error = TRUE;
  //$error_msg = $error_msg . "<br/>- $msg";
  $error_msg = getError($error_msg, $msg);
  } else {
  $puja_name = $id;
  }
  }



  if (strlen(trim($enq_source_txt)) > 0) {

  list($status, $id, $msg) = add_puja($enq_source_txt);
  if ($status == 0) {
  $error = TRUE;
  $error_msg = $error_msg . "<br/>- $msg";
  } else {
  $enq_source = $id;
  }
  }

  if (($enq_medium == 0)) {

  //$status=0; $id=0; $msg = "";
  if (strlen($enq_medium_txt) > 0) {
  list($status, $id, $msg) = add_medium($enq_medium_txt);
  $enq_medium = $id;
  $error_msg = getError($error_msg, $msg);

  if ($status == 0)
  $error = TRUE;
  }
  if ($enq_medium == 0) {
  $error = TRUE;
  $error_msg = getError($error_msg, "Select Proper Medium");
  }

  //echo "<br/>$status, $id, $msg<br/>";
  }

 */









if ($error == TRUE) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
    echo $error_msg;
    return 0;
}


//$enq_date = date("Y-m-d", strtotime($enq_date));
//$puja_date = date("Y-m-d", strtotime($puja_date));
//$puja_time = date("H:i", strtotime($puja_time));

/* Collected Data For First Followup End */
$followup_date = $enq_date;
$next_date = date('Y-m-d H:i:s', strtotime($followup_date . ' +1 minutes'));
$comment = "I have Added This Enquiry";
$enq_status = 3;
$monitor_by = 1;
/* Collected Data For First Followup End */


$data_sql = " tbl_enquiry set "
        . "cust_id = " . $cust_id
        . ",enq_no = '" . $enq_no
        . "', enq_date = '" . $enq_date
        . "' " . $puja_query
        . ",medium_id = " . $enq_medium
        . ",muhurat_advice = '" . $enq_muhurat
        . "', other_specification = '" . $enq_pandit_specification
        . "', pandit_lange_id = " . $enq_pandit_lang
        //. ",samagri_type = " . $samagri_type
        . ",pandit_id = 0"
        . ",quotation_id = 0"
        . ",Important_points = '" . $imp_points
        . "', caste_id = " . $enq_pandit_cast
        . ",comment = '" . $enq_comment . "',";


if (!$edit_id) {
$data_sql = $data_sql." enq_status = 3"
        . ",followup_date = '',
    ";
}

list($status, $id, $msg) = add_enquiry($edit_id, $data_sql);
$error_msg = getError($error_msg, $msg);
//echo "status[$status], id[$id], msg[$msg]";


if ($status == 0) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
    echo $error_msg;
    return 0;
}

if (!$edit_id) {

    $enq_id = $id;


    $data_sql = " tbl_enq_followup set "
            . "enq_id = " . $enq_id
            . ", add_enq_date = '" . $followup_date
//. "', add_enq_time = '" . $followup_time
            . "', next_date = '" . $next_date
//. "', next_time = '" . $next_time
            . "', comment = '" . $comment
            . "', enq_status = " . $enq_status
            . ", monitor_by = " . $monitor_by;

    list($Followup_status, $Followup_id, $Followup_msg) = addFollowup($data_sql);
}



if ($status == 1) {
    //echo $error_msg;
    $error_msg = '<div class="alert alert-success alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $msg . '
                        <a class="alert-link" href="add_enquiry.php?edit_id=' . $id . '&tab=puja-details" style="margin-left:20px;">+ Add Puja</a>
                        <a class="alert-link" href="add_enquiry.php?edit_id=' . $id . '&tab=Followup-Tab" style="margin-left:20px;">+ Add Followup</a>
                </div>';
    echo $error_msg;
    return 0;
}


echo "<br/>Enquiry Success = enq_no = [$enq_no], puja = [$puja_name], source = [$enq_source], medium = [$enq_medium], lang = [$puja_name], cast = [$puja_name]";
return 0;
?>


