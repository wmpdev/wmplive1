<?php

// instantiate product object
$Main_Menu = "Feeds";
$Sub_Menu_1 = "list_feeds";


$error = FALSE;
//include_once 'App_data/auth.php';
//include_once 'config/auth.php';
include_once '../App_data/auth.php';

//print_r($_REQUEST);
/* if(!$login_user->CheckLogin() )
  {
  $login_user->urlredirection("login.php");
  exit;
  }
 */

// Array ( [client_feedback] => 1901152541 [feedback_action] => ADD [pandit_rating] => 3 [samagri_rating] => 6 [backend_rating] => 7 [user-commnets] => aaaaaaaaaaaaaaaaa ) 
//print_r($_SESSION);
$action = $_REQUEST["Action"];

$feeback_update_status = FALSE;

if ($action == "ADD") {

    $executives_comments = "";

    $uniq_identifier = $_REQUEST["client_feedback"];


    if ($_REQUEST["pandit_rating"] > 0) {
        $pandit_rating = $_REQUEST["pandit_rating"];
    } else {
        $pandit_rating = 0;
    }
    if ($_REQUEST["backend_rating"] > 0) {
        $backend_rating = $_REQUEST["backend_rating"];
    } else {
        $backend_rating = 0;
    }
    if ($_REQUEST["samagri_rating"] > 0) {
        $samagri_rating = $_REQUEST["samagri_rating"];
    } else {
        $samagri_rating = 0;
    }



    if (isset($_REQUEST["executives_comments"])) {
        $executives_comments = ", comments = '" . htmlspecialchars(trim($_REQUEST["executives_comments"]), ENT_QUOTES) . "'";

        if (strlen($_FILES['feed_img']['name']) > 3) {
            $errors = array();
            $file_name = $_FILES['feed_img']['name'];
            $file_size = $_FILES['feed_img']['size'];
            $file_tmp = $_FILES['feed_img']['tmp_name'];
            $file_type = $_FILES['feed_img']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['feed_img']['name'])));

            $current_time = time();
            $upload_file_path_db = "/feedback/" . $current_time . "." . $file_ext;
            $upload_file_path = MEDIA_PATH . $upload_file_path_db;


            $expensions = array("mp4", "mp3", "wav");  // "jpeg", "jpg", "png"   // "mp4", "mp3", "wav"

            if (in_array($file_ext, $expensions) === false) {
                $errors[] = "extension not allowed, please choose a MP3 Or Wav file.";
                $error = TRUE;
                $error_msg = getError($error_msg, "Extension not allowed, please choose a JPEG or PNG file.");
            }
            /*
              if ($file_size > 2097152) {
              $errors[] = 'File size must be excately 2 MB';
              $error = TRUE;
              $error_msg = getError($error_msg, "File size must be excately 2 MB.");
              }
             */

            if (empty($errors) == true) {
                //move_uploaded_file($file_tmp, "images/" . $file_name);
                //echo "Success";
                if (move_uploaded_file($file_tmp, $upload_file_path)) {
                    //echo "File is valid, and was successfully uploaded.\n";
                    $error_msg = getError($error_msg, "Wornning :- Memory_use -> File is valid, and was successfully uploaded.");
                    $executives_comments .= ", feedback_file = '$upload_file_path_db'";
                } else {
                    //echo "Possible file upload attack!\n";
                    $error = TRUE;
                    $error_msg = getError($error_msg, "Possible file upload attack! Upload_Path = " . $upload_file_path);
                }
            } else {
                //print_r($errors);
                $error = TRUE;
            }
        }
    } else {
        $executives_comments = "";
    }

    if (isset($_REQUEST["user_commnets"])) {
        $user_commnets = ", customer_feedback = '" . htmlspecialchars(trim($_REQUEST["user_commnets"]), ENT_QUOTES) . "'";
    } else {
        $user_commnets = "";
    }



    $add_date = getTimestamp();
    if (($_SESSION['login_id']) > 0) {
        $add_user = $_SESSION['login_id'];
        $query_end = ", add_date = '$add_date', mod_date = '$add_date', add_user=$add_user, mod_user=$add_user";
    } else {
        $query_end = "";
    }

    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        //echo $error_msg;
        //return 0;
    }


    $query = "update tbl_feedback set rate_pandit=$pandit_rating,rate_samagri=$samagri_rating,rate_team=$backend_rating
        $user_commnets $executives_comments $query_end
        where uniq_identifier = '$uniq_identifier'";
    //echo "</br> $query";
    list($query_status, $query_id, $query_arr) = addDataByQuery($query);  //  Apply Feedback to Order Puja
    if ($query_status) {
        //echo "</br> Successfully Updated Feedback";
        //$error_msg = getError($error_msg, "Your Feedback is Saved");
        $error_msg = "Your Feedback is Saved";
        $feeback_update_status = TRUE;
    } else {
        $error = TRUE;
        $error_msg = getError($error_msg, "Not able to Update Feedback");
        //echo "</br> Error :- Not able to Update Feedback </br> $query_arr ";
    }

    if ($query_status == 0) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        //echo $error_msg;
        //return 0;
    }

    if ($query_status == 1) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-success alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $error_msg . '
                </div>';
        //echo $error_msg;
        //return 0;
    }
}


if ($action == "Generate") {

    $order_id = $_REQUEST["order_id"];
    $puja_id = $_REQUEST["puja_id"];
    $cust_id = $_REQUEST["cust_id"];


    if (!$order_id > 0) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Order Not Specified");
    }

    if (!$puja_id > 0) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Order Puja Not Specified");
    }
    if (!$cust_id > 0) {
        $error = TRUE;
        $error_msg = getError($error_msg, "Customer Not Specified");
    }


    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = "Error : </br>" . $error_msg;
        echo $error_msg;
        return 0;
    }

    echo "</br> -  Looking For Feed Back";

    // Search For Feed back already Present 
    $query = "SELECT * from tbl_feedback where order_id = $order_id and order_puja_id = $puja_id and status = 0";
    list($status, $id, $result_arr) = getDataByQuery($query);
    if ($id == 0) {
        // If Already Presnt  
        $uniq_identifier = date('Hmids', strtotime(getTimestamp()));  // For User
        // Create New Feedback Record 
        echo "</br> -  Registring Your Feedback Request ";
        $data_sql = " tbl_feedback set order_id = $order_id, order_puja_id = $puja_id, cust_id = $cust_id, uniq_identifier = '$uniq_identifier', ";
        echo "</br> $data_sql";
        list($feedback_status, $feedback_id, $feedback_arr) = add_enquiry(0, $data_sql);  // Add Feedback Record
        if ($feedback_status) {

            echo "</br> -  Registration is completed with id [$feedback_id] ";
            echo "</br> -  Started linking Feedback To Puja ";

            $order_puja_query = "update tbl_order_puja_tmp set feedback_id = '$uniq_identifier' where id = $puja_id";
            echo "</br> $order_puja_query";
            list($order_puja_status, $order_puja_id, $order_puja_arr) = addDataByQuery($order_puja_query);  //  Apply Feedback to Order Puja
            if ($order_puja_status) {
                echo "</br> Successfully Linked Your Record <a href='feed_back/index.php?client_feedback=$uniq_identifier'>click here</a> for link";
                header("Location: feed_back/index.php?client_feedback=$uniq_identifier");
            } else {
                echo "</br> Error :- Not able to Link Feedback To Puja </br> $order_puja_arr ";
            }
        } else {
            echo "error  = $add_arr";
            echo "</br> Error :- Not able to Register Feedback </br> $feedback_arr ";
        }
    } else {
        print_r($result_arr);
        $uniq_identifier = $result_arr[0]['uniq_identifier'];
        $puja_id = $result_arr[0]['order_puja_id'];
        /*
          $order_puja_query = "update tbl_order_puja_tmp set feedback_id = '$uniq_identifier' where id = $puja_id";
          echo "</br> $order_puja_query";
          list($order_puja_status, $order_puja_id, $order_puja_arr) = addDataByQuery($order_puja_query);  //  Apply Feedback to Order Puja
          if ($order_puja_status) {
          echo "</br> Successfully Linked Your Record <a href='feed_back/index.php?client_feedback=$uniq_identifier'>click here</a> for link";
          //header("Location: feed_back/index.php?client_feedback=$uniq_identifier");
          } else {
          echo "</br> Error :- Not able to Link Feedback To Puja </br> $order_puja_arr ";
          }
         */
        echo "</br> Feed Back Already Present. <a href='feed_back/index.php?client_feedback=$uniq_identifier'>click here</a> for link";
        header("Location: feed_back/index.php?client_feedback=$uniq_identifier");
    }


    var_dump($result_arr);
    return 0;
}


$uniq_identifier = $_REQUEST["client_feedback"];

$query = "SELECT o.id order_id,o.cust_id,o.order_no, 
p.id order_puja_id,p.puja_id,p.puja_date,p.puja_address, c.first_name,c.last_name,c.phone_1,c.email_1, 
pm.pooja_name,
f.* FROM `tbl_feedback` f inner join tbl_order_puja_tmp p 
inner join tbl_order_tmp o 
inner join tbl_customer c 
inner join tbl_pooja_master pm
on f.uniq_identifier = '$uniq_identifier' 
and f.order_puja_id = p.id 
and p.order_id = o.id 
and o.cust_id = c.id 
and p.puja_id = pm.id";
list($status, $id, $result_arr) = getDataByQuery($query);


$cust_order_no = $result_arr[0]["order_no"];
$cust_name = $result_arr[0]["first_name"];
$cust_phone_1 = $result_arr[0]["phone_1"];
$cust_pooja_name = $result_arr[0]["pooja_name"];
$cust_puja_date = $result_arr[0]["puja_date"];
$cust_puja_address = $result_arr[0]["puja_address"];


$pandit_rating = $result_arr[0]["rate_pandit"];
$samagri_rating = $result_arr[0]["rate_samagri"];
$backend_rating = $result_arr[0]["rate_team"];

$user_commnets = $result_arr[0]['customer_feedback'];
$executives_comments = $result_arr[0]['comments'];

$feedback_file = $result_arr[0]['feedback_file'];
//var_dump($result_arr);
//echo "Status = $status, id = $id, data = $result_arr, ";

//echo "<br/> Geeting data";

if ($feeback_update_status == TRUE) {

    //echo "<br/> Inside Feedback Success";
    
    include("../include/email_blocks/feedback_updates_email_block.php");
    $message = "";
    //$tmp_msg = (include "include/email_blocks/no_contact.php"); //include("include/email_blocks/no_contact.php");
    //$tmp_msg = file_get_contents('include/email_blocks/no_contact.php'); //$no_contact_template;
    $message = $feedback_updates_template;


    //echo "Message = ".$message;
    //$to = 'ninad.tilkar@choiceindia.com';
    $cc = 'ninad.tilkar@choiceindia.com';
    $to = 'panditji@wheresmypandit.com';
    $from = "panditji@wheresmypandit.com";
    $subject = "Puja Details of Enquiry $enq_no and Order $order_no";
    $subject = 'Feedback Updates For Order No #'.$cust_order_no ;

    $headers = "From: $from \r\n";
    $headers .= "Reply-To: $from \r\n";
    $headers .= "CC: $cc \r\n"; //,info@wheresmypandit.com
    $headers .= "BCC: ninad.tilkar@choiceindia.com \r\n"; //,info@wheresmypandit.com
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";



    if (mail($to, $subject, $message, $headers, '-fpanditji@wheresmypandit.com')) {
        $mail_msg = "mail Send Successfully to $cust_name $to";
        $status = 1;
    } else {
        $mail_msg = "There is some problem to send mail";
    }
    //echo "<br/>Status = [$mail_msg]";
}




return 0;
//$stmt = $user->listUsers();
//print_r($result_arr);
//$log->LogMsg("Listing Of Users");
?>


