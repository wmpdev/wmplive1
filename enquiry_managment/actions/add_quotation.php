<?php

//include_once '../function/function.php';

include_once '../config/auth.php';

global $con;
//print_r($_POST); 
//exit;
$error = FALSE;
$error_msg = "";


for ($i = 0; $i < count($_POST); $i++) {
    $_POST[$key] = trim($_POST[$key]);
}

// Array ( [pandit_cost] => 1500 [quotation_add_enq_id] => 20 [pandit_selling] => 2005 [samagri_sukhi] => 320 [samagri_gili] => 520 [extra_cost] => 521 [discount_cost] => 532 [discount_reason] => aaaaaaaaa [comment] => cccccccccccccccc [pandit_name] => 3 [co_ordinator] => 3 [samagri_type] => Pandit [approved_by] => 1 ) 
//$edit_id = $_POST["edit_id"];

if ($_POST["ACTION"] == "AddQuotation") {




    $enq_id = $_POST["quotation_add_enq_id"];
    $pandit_cost = $_POST["pandit_cost"];
    $pandit_selling = $_POST["pandit_selling"];

    $samagri_sukhi = $_POST["samagri_sukhi"];
    $samagri_gili = $_POST["samagri_gili"];
    $extra_cost = $_POST["extra_cost"];
    $conveyance_cost = $_POST["conveyance"];
    $discount_cost = $_POST["discount_cost"];

    $discount_reason = $_POST["discount_reason"];
    $pandit_id = $_POST["pandit_id"];
    $co_ordinator = $_POST["co_ordinator"];
    $samagri_type = $_POST["samagri_type"];
    $approved_by = $_POST["approved_by"];

    $comment = $_POST["comment"];




    if (!$enq_id > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Select proper Enquiry.";
        $error_msg = getError($error_msg, "Select proper Enquiry.");
    }

    if (!$pandit_cost > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
        $error_msg = getError($error_msg, "Enter proper Pandit Actual Cost");
    }
    if (!$pandit_selling > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
        $error_msg = getError($error_msg, "Enter proper Pandit Selling Cost");
    }

    if (!$samagri_sukhi > 0) {
        $samagri_sukhi = 0;
    }
    if (!$samagri_gili > 0) {
        $samagri_gili = 0;
    }
    if (!$extra_cost > 0) {
        $extra_cost = 0;
    }
    if (!$conveyance_cost > 0) {
        $conveyance_cost = 0;
    }
    if (!$discount_cost > 0) {
        $discount_cost = 0;
    }


    $sub_tatal = ($pandit_selling + $samagri_sukhi + $samagri_gili + $extra_cost);
    $total = ($sub_tatal - $discount_cost);


    if (!$sub_tatal > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
        $error_msg = getError($error_msg, "Sub total not calculated properly");
    }
    if (!$total > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
        $error_msg = getError($error_msg, "Total not calculated properly");
    }



    /*

      if (!strlen(trim($comment)) > 0) {
      $error = TRUE;
      //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
      $error_msg = getError($error_msg, "Enter proper Comments.");
      }

      if (!$enq_status > 0) {
      $error = TRUE;
      //$error_msg = $error_msg . "<br/>- Select proper Status Followup time.";
      $error_msg = getError($error_msg, "Select proper Status.");
      }

      if (!$monitor_by > 0) {
      $error = TRUE;
      //$error_msg = $error_msg . "<br/>- Select proper Monitor.";
      $error_msg = getError($error_msg, "Select proper Monitor.");
      }
     */

//$comment = mysql_real_escape_string($comment);
//$comment = $con->real_escape_string($comment);
    $comment = htmlspecialchars($comment, ENT_QUOTES);

    /*
      $followup_date = date("Y-m-d", strtotime($followup_date));
      $followup_time = date("H:i", strtotime($followup_time));
      $next_date = date("Y-m-d", strtotime($next_date));
      $next_time = date("H:i", strtotime($next_time));


      if (strtotime($followup_date . " " . $followup_time) > strtotime($next_date . " " . $next_time)) {
      $error = TRUE;
      //$error_msg = $error_msg . "<br/>- Select proper Monitor.";
      $error_msg = getError($error_msg, "Followup Date-Time should be greater than current Date-Time.");
      }
     */






    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }



    $data_sql = " tbl_enq_quotation set "
            . "enq_id = " . $enq_id
            . ", pandit_id = " . $pandit_id
            . ", actual_cost = " . $pandit_cost
            . ", selling_cost = " . $pandit_selling
            . ", samagri_sukhi = " . $samagri_sukhi
            . ", samagri_gili = " . $samagri_gili
            . ", extra = " . $extra_cost
            . ", conveyance = " . $conveyance_cost
            . ", co_ordinator = " . $co_ordinator
            . ", approved_by = " . $approved_by
            . ", discount_comments = '" . $discount_reason
            . "', samagri_type = '" . $samagri_type
            . "', comments = '" . $comment
            . "', sub_total = " . $sub_tatal
            . ", discount = " . $discount_cost
            . ", total = " . $total;



//        . "add_date = '" . $cust_id   $conveyance_cost
//        . "', mod_date = '" . $cust_id
//        . "', add_user = " . $cust_id
//        . "mod_user = '" . $cust_id
//list($status, $id, $msg) = addFollowup($data_sql);
    list($status, $id, $msg) = addEnqQuotation(0, $data_sql); // addEnqQuotation($edit_id, $data_sql)
//echo "status[$status], id[$id], msg[$msg]";


    if ($status == 0) {
        //echo $error_msg;
        $error = TRUE;
        $error_msg = getError($error_msg, $msg);

        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }

    if ($status == 1) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-success alert-dismissable">        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' . $msg . '
                        Click <a class="alert-link" href="add_enquiry.php?edit_id=' . $enq_id . '&tab=quotation-details" >Refresh</a>
                </div>';
        echo $error_msg;
        return 0;
    }

    // End add_enquiry.php?edit_id=22
} elseif ($_POST["ACTION"] == "ConfirmQuotation") {

    $enq_id = $_POST["quotation_add_enq_id"];
    $quotation_id = $_POST["quotation_id"];

    if (!$enq_id > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Select proper Enquiry.";
        $error_msg = getError($error_msg, "Select proper Enquiry");
    }
    if (!$quotation_id > 0) {
        $error = TRUE;
        //$error_msg = $error_msg . "<br/>- Enter proper Next Followup time.";
        $error_msg = getError($error_msg, "Select proper Quotation");
    }

    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }



    $data_sql = " tbl_enquiry set "
            . "quotation_id = " . $quotation_id
    ;



//        . "add_date = '" . $cust_id
//        . "', mod_date = '" . $cust_id
//        . "', add_user = " . $cust_id
//        . "mod_user = '" . $cust_id
//list($status, $id, $msg) = addFollowup($data_sql);
    list($status, $id, $msg) = addEnqQuotation($enq_id, $data_sql); // addEnqQuotation($edit_id, $data_sql)
//echo "status[$status], id[$id], msg[$msg]";


    if ($status == 0) {
        //echo $error_msg;
        $error = TRUE;
        $error_msg = getError($error_msg, $msg);

        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }

    if ($status == 1) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-success alert-dismissable">        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>  Quotation Is confirmed You can Place Order Now, To place order <a class="alert-link" href="add_enquiry.php?edit_id=' . $enq_id . '&tab=Final-Details" >Click Here</a>                        
                </div>';
        echo $error_msg;
        return 0;
    }

    // End
} else {

    $error = TRUE;
    $error_msg = getError($error_msg, "No Action Specified");

    if ($error == TRUE) {
        //echo $error_msg;
        $error_msg = '<div class="alert alert-danger alert-dismissable">
        
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Error :- 
                    ' . $error_msg . '
                </div>';
        echo $error_msg;
        return 0;
    }
}




//echo "<br/>Enquiry Success = enq_no = [$enq_no], puja = [$puja_name], source = [$enq_source], medium = [$enq_medium], lang = [$puja_name], cast = [$puja_name]";
return 0;
?>

