<?php
$Main_Menu = "Feeds";
$Sub_Menu_1 = "add_feeds";

//$page = "Enquiry Details";
$page_title = "Feeds Details";
$edit_id = 0;

//print_r($_GET);


if (isset($_POST['rec_id']))
    $edit_id = trim($_POST['rec_id']);

if (isset($_GET['edit_id']))
    $edit_id = trim($_GET['edit_id']);

if (isset($_GET['user_id']))
    $cust_id = trim($_GET['user_id']);


include_once 'config/auth.php';

/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->
        <!-- PAGE LEVEL STYLES -->
        <link rel="stylesheet" href="assets/css/bootstrap-fileupload.min.css" />
        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">

                                        <li class="active">
                                            <a data-toggle="tab" href="#enquiry_details"><span> Feed Details</span></a>
                                        </li>
                                        <?php if ($enq_id > 0) { ?>
                                            <li class="">
                                                <a data-toggle="tab" href="#Followup-Tab">Followup</a>
                                            </li>
                                            <li class="">
                                                <a data-toggle="tab" href="#home-pills"><span>  Quotation Details</span></a>
                                            </li>
                                            <li class="">
                                                <a data-toggle="tab" href="#settings-pills">Final Details</a>
                                            </li>

                                            <li class=""><a data-toggle="tab" href="#User-Details">User Details</a>
                                            </li>
                                        <?php } ?>


                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                        <div id="enquiry_details" class="tab-pane fade active in">

                                            <?php include 'include/blocks/add_feed_block.php'; ?>

                                        </div>

                                        <div id="Followup-Tab" class="tab-pane fade">  

                                            <div class="box" id="followup_msg" >
                                                <div class="col-lg-12">            

                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div id="enq_comments">
                                                        <?php //include 'include/blocks/list_followup.php'; ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div class="body">
                                                        <?php //include 'include/blocks/add_followup.php'; ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>





                                        <div id="home-pills" class="tab-pane fade">

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <!--
                                                    <header>
                                                        <h5>Details</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    -->
                                                    <div class="body collapse in" id="sortableTable">
                                                        <table class="table table-bordered sortableTable responsive-table">

                                                            <tbody>


                                                                <tr>
                                                                    <td>Pooja Date</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Pooja Time</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Medium</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <td>Budget</td>
                                                                    <td>
                                                                        <input id="budget" class="form-control" type="text" value="" placeholder="Budget" name="budget">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cast</td>
                                                                    <td>
                                                                        <input id="cast" class="form-control" type="text" value="" placeholder="Cast" name="cast">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Service Requested</td>
                                                                    <td>
                                                                        <input id="service" class="form-control" type="text" value="" placeholder="Service Request Id" name="service">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Order Status Id</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address</td>
                                                                    <td>
                                                                        <textarea id="comment" class="form-control" placeholder="Enter Comments" name="comment"></textarea>
                                                                    </td>
                                                                </tr>





                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">



                                                    <div class="body">

                                                        <?php // include 'add_user_comments_view.php';   ?>      
                                                        <?php //include './address_details.php';       ?>
                                                    </div>

                                                </div>

                                            </div>



                                        </div>



                                        <div id="profile-pills" class="tab-pane fade">

                                            <div class="panel-body">

                                                <div class="col-lg-6">
                                                    <div class="box">

                                                        <div class="body">
                                                            <?php include './address_details.php'; ?>
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div id="settings-pills" class="tab-pane fade">

                                            <div class="col-lg-6">

                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> Add New Payment </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">

                                                                <tbody>


                                                                    <tr>
                                                                        <td>Date</td>
                                                                        <td>
                                                                            <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Payment Mode</td>
                                                                        <td>
                                                                            <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Payment</td>
                                                                        <td>
                                                                            <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                        </td>
                                                                    </tr>







                                                                </tbody>
                                                            </table>

                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="col-lg-6">

                                                <div class="box">
                                                    <header>
                                                        <h5>Payment History</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="body collapse in" id="sortableTable">
                                                        <table class="table table-bordered sortableTable responsive-table">

                                                            <tbody>


                                                                <tr>
                                                                    <td>25-March-2015</td>
                                                                    <td>Online</td>
                                                                    <td>2000</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>17-August-2015</td>
                                                                    <td>Online</td>
                                                                    <td>5000</td>
                                                                </tr>






                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>



                                        <div id="User-Details" class="tab-pane fade">

                                            <div class="col-lg-6">

                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> User Details </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">

                                                                <tbody>


                                                                    <tr>
                                                                        <td>Number</td>
                                                                        <td><?php echo $phone_1 . "<br/>" . $phone_2; ?></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Email Id</td>
                                                                        <td><?php echo $email_1 . "<br/>" . $email_2; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td><?php echo $first_name . " " . $last_name; ?></td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address</td>
                                                                        <td><?php echo $address_1; ?></td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>




                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <!-- <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button> -->
                                                                <a class="btn text-warning btn-xs btn-flat" href="add_user.php?edit_id=<?php echo $cust_id; ?>">
                                                                    <i class="icon-edit-sign icon-white"></i> Edit</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="col-lg-6">

                                                <div class="box">
                                                    <header>
                                                        <h5>Payment History</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="body collapse in" id="sortableTable">
                                                        <table class="table table-bordered sortableTable responsive-table">

                                                            <tbody>


                                                                <tr>
                                                                    <td>25-March-2015</td>
                                                                    <td>Online</td>
                                                                    <td>2000</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>17-August-2015</td>
                                                                    <td>Online</td>
                                                                    <td>5000</td>
                                                                </tr>






                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';        ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->


        <script>
            $(document).ready(function ()
            {


                function getDoc(frame) {
                    var doc = null;

                    // IE8 cascading access check
                    try {
                        if (frame.contentWindow) {
                            doc = frame.contentWindow.document;
                        }
                    } catch (err) {
                    }

                    if (doc) { // successful getting content
                        return doc;
                    }

                    try { // simply checking may throw in ie8 under ssl or mismatched protocol
                        doc = frame.contentDocument ? frame.contentDocument : frame.document;
                    } catch (err) {
                        // last attempt
                        doc = frame.document;
                    }
                    return doc;
                }

                $("#multiform").submit(function (e)
                {
                    $("#enq_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({ scrollTop: 0 }, 600);
    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_feed.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#enq_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#enq_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#enq_msg").html('' + data + '');
                        });

                    }

                });


                $("#multi-post").click(function ()
                {

                    $("#multiform").submit();

                });

            });
        </script>





        





        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
        <script src="assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/daterangepicker/moment.min.js"></script>
        <script src="assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-fileupload.js"></script>
        <script src="assets/js/formsInit.js"></script>
        <script>
            $(function () {
                formInit();
            });
        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
