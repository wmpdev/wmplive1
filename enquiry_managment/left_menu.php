<?php

//echo "page = $page";
//echo "  Tab = $tab";
//echo "Main_Menu = $Main_Menu";
//$Main_Menu = "Services";
//$Sub_Menu_1 = "list_pooja";
//print_r($_SESSION);

function active_page($page_name) {
    if (isset($page) && ($page == $page_name))
        echo "active";
}

function active_tab($page_name) {
    if (isset($tab) && ($tab == $page_name))
        echo "active";
}

function checkActiveMenu($Menu_name, $Main_Menu) {
    if (isset($Main_Menu) && ($Main_Menu == $Menu_name)) {
        echo "active";
        //$Sub_Menu_Status = "in";
        //return 1;
    }
    //echo " Main_Menu = $Main_Menu and  = Menu_name = $Menu_name";
    //return 0;    
}

function checkActiveMenuStatus($Menu_name, $Main_Menu) {
    if (isset($Main_Menu) && ($Main_Menu == $Menu_name)) {
        echo "in";
        //$Sub_Menu_Status = "in";
        //return 1;
    } else {
        echo "collapse";
    }
    //echo " Main_Menu = $Main_Menu and  = Menu_name = $Menu_name";
    //return 0;    
}
?>
<!-- MENU SECTION -->
<div id="left">
    <div class="media user-media well-small">
        <a class="user-link" href="#">
            <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?php echo $_SESSION['login_img_path']; ?>" style="width: 170px; height: 100px;" />
        </a>        
        <div class="media-body">
            <h5 class="media-heading"> <?php echo $_SESSION['login_name'] ?></h5>
            <ul class="list-unstyled user-info">
                <li><a class="btn btn-success btn-xs btn-circle" style="width: 10px;height: 12px;"></a> Online</li>
            </ul>
        </div>
        <br />
    </div>

    <ul id="menu" class="collapse">

        <!-- Dashboard Menu ---------------------------------------------------------------------------------------------------- -->
        <li class="panel <?php checkActiveMenu("Dashboard", $Main_Menu); ?>">
            <a href="index.php" >
                <i class="icon-table"></i> Dashboard
            </a>                   
        </li>


        <!-- User Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Users", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                <i class="icon-user"> </i> Users
                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-default">2</span>&nbsp;
            </a>

            <ul class="<?php checkActiveMenuStatus("Users", $Main_Menu); ?>" id="component-nav">

                <li class="<?php checkActiveMenu("Add_User", $Sub_Menu_1); ?>"><a href="add_user.php"><i class="icon-angle-right"></i> Add User </a></li>
                <li class="<?php checkActiveMenu("list_user", $Sub_Menu_1); ?>"><a href="list_user.php"><i class="icon-angle-right"></i> List Users </a></li>

                <li class="<?php checkActiveMenu("Add_Pandit", $Sub_Menu_1); ?>"><a href="add_pandit.php"><i class="icon-angle-right"></i> Add Pandit </a></li>         
                <li class="<?php checkActiveMenu("list_pandit", $Sub_Menu_1); ?>"><a href="list_pandit.php"><i class="icon-angle-right"></i> List Pandit </a></li>                       


                <!--
                <li class="<?php checkActiveMenu("search_user", $Sub_Menu_1); ?>"><a href="search_user.php"><i class="icon-angle-right"></i> Search User </a></li>
                <li class="<?php checkActiveMenu("List_Enq", $Sub_Menu_1); ?>"><a href="list_enquiry.php"><i class="icon-angle-right"></i> List Enquiry </a></li>
                 <li class="<?php active_tab("add_user"); ?>"><a href="add_enquiry.php"><i class="icon-angle-right"></i> New Enquiry </a></li>  -->     

            </ul>
        </li>

        <!-- Order Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Enquiry", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#Enquiry-nav">
                <i class="icon-user"> </i> Enquiry     

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-default">1</span>&nbsp;
            </a>
            <ul class="<?php checkActiveMenuStatus("Enquiry", $Main_Menu); ?>" id="Enquiry-nav">                
                <!-- <li class="<?php checkActiveMenu("add_order_form_temp", $Sub_Menu_1); ?>"><a href="add_order_tmp.php"><i class="icon-angle-right"></i> New Order </a></li> -->
                <li class="<?php checkActiveMenu("add_enquiry", $Sub_Menu_1); ?>"><a href="add_user.php"><i class="icon-angle-right"></i> Add Enquiry </a></li>
                <li class="<?php checkActiveMenu("list_enquiry", $Sub_Menu_1); ?>"><a href="list_enquiry.php"><i class="icon-angle-right"></i> List Enquiry </a></li>


            </ul>
        </li>

        <!-- Order Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Order", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#Order-nav">
                <i class="icon-user"> </i> Order     

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-default">1</span>&nbsp;
            </a>
            <ul class="<?php checkActiveMenuStatus("Order", $Main_Menu); ?>" id="Order-nav">                
                <!-- <li class="<?php checkActiveMenu("add_order_form_temp", $Sub_Menu_1); ?>"><a href="add_order_tmp.php"><i class="icon-angle-right"></i> New Order </a></li> -->
                <li class="<?php checkActiveMenu("list_order_form_temp", $Sub_Menu_1); ?>"><a href="list_order_tmp.php"><i class="icon-angle-right"></i> List Order </a></li>
                <li class="<?php checkActiveMenu("list_order_puja_form_temp", $Sub_Menu_1); ?>"><a href="list_order_puja_tmp.php"><i class="icon-angle-right"></i> List Order Puja </a></li>


            </ul>
        </li>

        <!-- Services Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Services", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#Services-nav">
                <i class="icon-pencil"></i> Services

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-success">1</span>&nbsp;
            </a>
            <ul class="<?php checkActiveMenuStatus("Services", $Main_Menu); ?>" id="Services-nav">
                <li class="<?php checkActiveMenu("list_pooja", $Sub_Menu_1); ?>">
                    <a href="list_pooja.php"><i class="icon-angle-right"></i> List Poojas </a></li>




            </ul>
        </li>

        <!-- Today Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Today", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#Today-nav">
                <i class="icon-user"> </i> Followup     

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-default">1</span>&nbsp;
            </a>
            <ul class="<?php checkActiveMenuStatus("Followup", $Main_Menu); ?>" id="Today-nav">                
                <li class="<?php checkActiveMenu("enquiry_followup", $Sub_Menu_1); ?>"><a href="list_enquiry_followup_tmp.php"><i class="icon-angle-right"></i> Enquiry Followups </a></li>


            </ul>
        </li>

        <!-- 
         <li class="panel">
             <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#DDL-nav">
                 <i class=" icon-sitemap"></i> 3 Level Menu
 
                 <span class="pull-right">
                     <i class="icon-angle-left"></i>
                 </span>
             </a>
             <ul class="collapse" id="DDL-nav">
                 <li>
                     <a href="#" data-parent="#DDL-nav" data-toggle="collapse" class="accordion-toggle" data-target="#DDL1-nav">
                         <i class="icon-sitemap"></i>&nbsp; Demo Link 1
 
                         <span class="pull-right" style="margin-right: 20px;">
                             <i class="icon-angle-left"></i>
                         </span>
 
 
                     </a>
                     <ul class="collapse" id="DDL1-nav">
                         <li>
                             <a href="#"><i class="icon-angle-right"></i> Demo Link 1 </a>
 
                         </li>
                         <li>
                             <a href="#"><i class="icon-angle-right"></i> Demo Link 2 </a></li>
                         <li>
                             <a href="#"><i class="icon-angle-right"></i> Demo Link 3 </a></li>
 
                     </ul>
 
                 </li>
                 <li><a href="#"><i class="icon-angle-right"></i> Demo Link 2 </a></li>
                 <li><a href="#"><i class="icon-angle-right"></i> Demo Link 3 </a></li>
                 <li><a href="#"><i class="icon-angle-right"></i> Demo Link 4 </a></li>
             </ul>
         </li>
        -->
        <!-- Feeds Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Feeds", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#Feed-nav">
                <i class="icon-user"> </i> Feeds     

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-default">1</span>&nbsp;
            </a>
            <ul class="<?php checkActiveMenuStatus("Feeds", $Main_Menu); ?>" id="Feed-nav">                


                <li class="<?php checkActiveMenu("add_feeds", $Sub_Menu_1); ?>"><a href="add_feeds.php"><i class="icon-angle-right"></i> Add Feeds </a></li>           
                <li class="<?php checkActiveMenu("list_feeds", $Sub_Menu_1); ?>"><a href="list_feeds.php"><i class="icon-angle-right"></i> List Feeds </a></li>

                <li class="<?php checkActiveMenu("send_notification", $Sub_Menu_1); ?>"><a href="send_feed_notification.php"><i class="icon-angle-right"></i> Send Notification </a></li>           

            </ul>
        </li>


        <!-- Masters Menu ---------------------------------------------------------------------------------------------------------  -->
        <li class="panel <?php checkActiveMenu("Master", $Main_Menu); ?>">
            <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#master-nav">
                <i class="icon-pencil"></i> Masters

                <span class="pull-right">
                    <i class="icon-angle-left"></i>
                </span>
                &nbsp; <span class="label label-success">1</span>&nbsp;
            </a>
            <ul class="<?php checkActiveMenuStatus("Master", $Main_Menu); ?>" id="master-nav">
                <li class="<?php checkActiveMenu("list_country", $Sub_Menu_1); ?>"><a href="list_country.php"><i class="icon-angle-right"></i> Country </a></li>


            </ul>
        </li>

        <!-- Download Menu ---------------------------------------------------------------------------------------------------------  -->
        <?php
        $user_login_id = $_SESSION['login_id'];
        //$export_user_arr = array(0 => '1', 1 => '6', 2 => '7');
        $export_user_arr = array("1", "6", "7");
        if (in_array($user_login_id, $export_user_arr)) {            
            ?>
            <li class="panel <?php checkActiveMenu("Export", $Main_Menu); ?>">
                <a href="export_data.php" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#export-nav">
                    <i class="icon-list-alt"></i> Export Reports

                    <span class="pull-right">
                        <i class="icon-angle-left"></i>
                    </span>
                    &nbsp; <span class="label label-success">1</span>&nbsp;
                </a>
                <!--
                <ul class="<?php checkActiveMenuStatus("Master", $Main_Menu); ?>" id="export-nav">
                    <li class="<?php checkActiveMenu("list_country", $Sub_Menu_1); ?>"><a href="list_country.php"><i class="icon-angle-right"></i> Country </a></li>


                </ul>
                -->
            </li>

        <?php } ?>
    </ul>

</div>
<!--END MENU SECTION -->