<?php
// instantiate product object
$Main_Menu = "Enquiry";
$Sub_Menu_1 = "list_enquiry";

include_once 'config/auth.php';

//$stmt = $user->listUsers();
list($status, $id, $result_arr) = getEnqData(); //getOrderTmpData();

$query = "
SELECT e.id,e.enq_no,e.enq_date,e.medium_id,e.quotation_id,e.enq_status,e.order_no,
,GROUP_CONCAT(p.pooja_name), 
c.first_name,c.last_name,c.city,c.area,c.source_id
FROM tbl_enquiry e  
inner join tbl_enq_puja ep 
inner join `tbl_pooja_master` p
on p.id = ep.puja_name 
and ep.enq_id = e.id
and e.cust_id = c.id
group by e.id 
ORDER BY `e.enq_date` desc ";


//$query = "SELECT * from tbl_enquiry";
list($status, $id, $result_arr) = getDataByQuery($query);

echo "<pre>";
var_dump($result_arr);
echo "</pre>";

exit;
//$log->LogMsg("Listing Of Users");
//print_r($result_arr);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title>List Enquiry</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" />-->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style>
            .showme{ 
                display: none;
            }
            .showhim:hover .showme{
                display : block;
            }
        </style>

    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content" >

                <div class="inner">


                    <div class="row">
                        <div class="col-lg-12">
                            <h2>List Enquiry </h2>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="box" id="order_tmp_list_msg" >
                                            <div class="col-lg-12">
                                                <!--
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
                                                </div>
                                                -->
                                            </div>
                                        </div>

                                        <div class="panel-body">                                            
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="font-size: 12px !important;">
                                                    <thead>
                                                        <tr>

                                                            <th style="width: 35px">No</th>
                                                            <th style="width: 70px">Enquiry</th>
                                                            <th style="width: 70px">Date </th>
                                                            <th>Customer Name</th>
                                                            <th>Area </th>
                                                            <th>City </th>
                                                            <th>Follow up </th>
                                                            <th>Status </th>

<!--<th>Reference</th>
<th>Cast</th>
<th>Country</th> -->

                                                            <th style="width:105px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        $count = 0;
//while ($row = $stmt->fetch_assoc()) {
                                                        for ($i = 0; $i < count($result_arr); $i++) {

                                                            $count++;
                                                            $enq_id = $result_arr[$i]['id'];
                                                            if (strlen($result_arr[$i]["enq_date"]) > 18) {
                                                                list($enq_date, $enq_time) = validateDbDateTime($result_arr[$i]["enq_date"]);
                                                                $enq_date = date('Y-m-d', strtotime($enq_date));
                                                                $enq_date = $enq_date . "<br/>" . $enq_time;
                                                            } else {
                                                                $enq_date = "";
                                                            }

                                                            $cust_id = $result_arr[$i]['cust_id'];
                                                            list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);
                                                            if ($cust_status == 1) {
                                                                $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
                                                                $cust_contact = "<br/> " . $cust_data_arr["phone_1"] . " , " . $cust_data_arr["phone_2"];
                                                                $cust_contact .= "<br/>" . $cust_data_arr["email_1"];

                                                                $cust_city = $cust_data_arr["city"];
                                                                $cust_area = $cust_data_arr["area"];
                                                            } else {
                                                                $cust_name = $result_arr[$i]['customer_name'] . "<br/>" . $result_arr[$i]['customer_contact'];
                                                                $cust_city = $result_arr[$i]["customer_city"];
                                                                $cust_area = $result_arr[$i]["customer_area"];
                                                            }


                                                            $enq_status = getStatus(0, $result_arr[$i]['enq_status']); //$result_arr[$i]['order_status']; 
                                                            $enq_status = (isset($enq_status[0]["status_name"]) ? $enq_status[0]["status_name"] : $result_arr[$i]['enq_status']);

                                                            if (strlen($result_arr[$i]["followup_date"]) > 18) {
                                                                list($follow_up_date, $follow_up_time) = validateDbDateTime($result_arr[$i]["followup_date"]);
                                                                $follow_up_date = $follow_up_date . "<br/>" . $follow_up_time;
                                                            } else {
                                                                $follow_up_date = "";
                                                            }

                                                            //if()?  : ;
                                                            //list($puja_date, $puja_time) = validateDbDateTime($result_arr[$i]["puja_date1"]);
                                                            //$puja_date = $puja_date . " " . $puja_time;
                                                            ?> 
                                                            <tr class="odd gradeX">

                                                                <td><?php echo $count; ?></td>
                                                                <td>
                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>" target="_blank" Title = "Click Here to Edit" class="btn text-info btn-xs btn-flat"><?php echo $result_arr[$i]['enq_no']; ?></a>
                                                                </td>
                                                                <td><?php echo $enq_date; ?></td>

                                                                <td>
                                                                    <a href="add_user.php?edit_id=<?php echo $cust_id; ?>" target="_blank" Title = "Click Here to Edit" class="btn text-info btn-xs btn-flat"><?php echo $cust_name; ?></a>
                                                                    <?php echo $cust_contact; ?>
                                                                </td>


                                                                <td><?php echo $cust_area; ?></td>
                                                                <td><?php echo $cust_city; ?></td>
                                                                <td>
                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>&tab=Followup-Tab" target="_blank" Title = "Click Here to Followup" class="btn text-info btn-xs btn-flat">
                                                                        <?php echo $follow_up_date; ?></a>
                                                                </td> 

                                                                <td><?php echo $enq_status; ?></td> 
                                                                <td>
                                                                    <a href="add_enquiry.php?edit_id=<?php echo $result_arr[$i]['id']; ?>" class="btn text-info btn-xs btn-flat">
                                                                        <i class="icon-list-alt icon-white"></i> Edit</a>

                                                                    <!--
                                                                <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>,'SMS','info')" class="btn text-info btn-xs btn-flat">
                                                                    <i class="icon-list-alt icon-white"></i> Send SMS info</a>
                                                                    

                                                                    <?php if (strlen($result_arr[$i]['customer_contact']) > 8) { ?>
                                                                                                    <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>,'SMS','Customer')" class="btn text-info btn-xs btn-flat">
                                                                                                    <i class="icon-list-alt icon-white"></i> Send SMS Customer</a>
                                                                    <?php } ?>
                                                                    
                                                                    -->

                                                                    <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>, 'EMAIL', 'info')" class="btn text-info btn-xs btn-flat">
                                                                        <i class="icon-list-alt icon-white"></i> Email To Info</a>

                                                                    <?php if (strlen($result_arr[$i]['customer_email']) > 5) { ?>
                                                                        <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>, 'EMAIL', 'Customer')" class="btn text-info btn-xs btn-flat">
                                                                            <i class="icon-list-alt icon-white"></i> Email To Customer</a>
                                                                        <?php } ?>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>





                                                    </tbody>
                                                </table>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <hr />




                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';        ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
                                                                    $(document).ready(function () {
                                                                        $('#dataTables-example').dataTable();
                                                                    });
        </script>

        <script>
            function send_Data(edit_id, format, user) {

                //alert("i am in send_SMS = "+user);


                //var enq_id = document.getElementById("edit_id").value;
                //var enq_id = edit_id;
                //alert("i am in send_SMS = " + enq_id);
                //return 0;
                //var cust_id = document.getElementById("cust_id").value;
                var param = "enq_id=" + edit_id + "&format=" + format + "&user=" + user;

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("order_tmp_list_msg").innerHTML = xmlhttp.responseText;
                        //document.getElementById("order_tmp_list_msg").innerHTML = "After Output ..... ";

                    }
                    else {
                        document.getElementById("order_tmp_list_msg").innerHTML = "Loadding ..... ";
                    }
                }
                xmlhttp.open("POST", "actions/sms_order_tmp.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }
        </script>

        <!-- END PAGE LEVEL SCRIPTS -->    


    </body>
    <!-- END BODY-->

</html>
