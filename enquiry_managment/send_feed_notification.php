<?php
$Main_Menu = "Feeds";
$Sub_Menu_1 = "send_notification";

$page = "Enquiry Details";
$page_title = "Feeds Notification";
$edit_id = 0;

//print_r($_GET);


if (isset($_POST['rec_id']))
    $edit_id = trim($_POST['rec_id']);

if (isset($_GET['edit_id']))
    $edit_id = trim($_GET['edit_id']);

if (isset($_GET['user_id']))
    $cust_id = trim($_GET['user_id']);


include_once 'config/auth.php';

$result_arr = "";

if ($edit_id > 0) {

    //list($status, $id, $result_arr) = getEnqData($edit_id);
    //[id] => 1 
    //[title] => Feed 2 
    //[image] => /feeds_img/1444127174_calendate-iphone.jpg 
    //[description] => Description 2 [type] => 2 
    //[url] => http://localhost/Ninad_app_feed/add_enquiry.php 
    //[publish_date] => 2015-10-06 00:00:00 
    //[add_date] => 2015-10-06 15:56:14 [mod_date] => 2015-10-06 15:56:14 [add_user] => 1 [mod_user] => 1 [status] => 0 


    list($status, $id, $data) = getFeedsNotification($edit_id);
    $result_arr = $data;
    //print_r($result_arr);

    if ($status == 1) {
        //print_r($result_arr);
        $feed_id = $result_arr[0]["id"];
        $feed_title = $result_arr[0]["title"];
        $feed_desc = $result_arr[0]["description"];
        $feed_type = $result_arr[0]["type"];
        $feed_img = $result_arr[0]["image"];
        $feed_url = $result_arr[0]["url"];
        $feed_date = $result_arr[0]["publish_date"];
        $feed_type_img = "";

        $feed_date = date("d-m-Y", strtotime($feed_date));
    } else {
        echo " Error :- $result_arr";
    }
}

list($status, $id, $feed_type_arr) = getFeedType();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->
        <!-- PAGE LEVEL STYLES -->
        <link rel="stylesheet" href="assets/css/bootstrap-fileupload.min.css" />
        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="nav nav-pills">

                                        <li class="active">
                                            <a data-toggle="tab" href="#enquiry_details"><span> Notification</span></a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">
                                        <div id="enquiry_details" class="tab-pane fade active in">
                                            <?php
                                            $result_arr = "";
                                            list($status, $id, $feed_type_arr) = getFeedType();
//print_r($feed_type_arr);
                                            ?>
                                            <div class="box" id="enq_msg" >
                                                <div class="col-lg-12">            
                                                    <!--
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
                                                    </div>
                                                    -->
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="col-lg-12">
                                                        <div class="box">

                                                            <div class="body collapse in" id="sortableTable">
                                                                <form name="multiform" id="multiform" action="multi-form-submit.php" method="POST" enctype="multipart/form-data">
                                                                    <table class="table table-bordered sortableTable responsive-table">

                                                                        <tbody>


                                                                            <tr>
                                                                                <td width="150px">Title</td>
                                                                                <td>
                                                                                    <input id="feed_title" class="form-control" type="text" value="<?php echo $feed_title; ?>" placeholder="Feed Title" name="feed_title">                                    
                                                                                    <input id="edit_id" class="form-control" type="hidden" value="<?php echo $feed_id; ?>" placeholder="Enquiry Number " name="edit_id">
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td>Publish Date</td>
                                                                                <td>
                                                                                    <input id="pub_date" class="form-control" type="text" value="<?php echo $feed_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="pub_date">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Type</td>
                                                                                <td>
                                                                                    <select tabindex="2" class="form-control" data-placeholder="Choose Type" id="feed_type" name="feed_type">  
                                                                                        <option selected="" value="-">-</option>                                                                                        
                                                                                        <?php
                                                                                        for ($i = 0; $i < count($feed_type_arr); $i++) {

                                                                                            //echo "$key->$value";
                                                                                            $selected = "";
                                                                                            $key = $feed_type_arr[$i]["id"];
                                                                                            $value = $feed_type_arr[$i]["feed_name"];
                                                                                            if ($key == $feed_type) {
                                                                                                $selected = "selected";
                                                                                                $feed_type_img = $feed_type_arr[$i]["feed_img"];
                                                                                            }
                                                                                            ?>
                                                                                            <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                                                        <?php } ?>
                                                                                        <!--                                            <option value="1">value 1</option> 
                                                                                                                                    <option value="2">value 2</option> 
                                                                                                                                    <option value="3">value 3</option> -->
                                                                                    </select>
                                                                                </td>
                                                                                <td width="200px">
                                                                                    <span class="fileupload-preview" style="margin-top: 10px;">
                                                                                        <img src="<?php echo "media/feeds/feeds_icons/" . $feed_type_img; ?>" alt="" border="0" style="display:block; width: 35px; " />
                                                                                    </span>
                                                                                </td>
                                                                            </tr>


                                                                            <tr>
                                                                                <td>Flag</td>
                                                                                <td>
                                                                                    <select tabindex="2" class="form-control" data-placeholder="Choose Type" id="feed_url" name="feed_url">  

                                                                                                                                                                            
                                                                                        <?php
                                                                                        $feed_flag_arr = array(
                                                                                            "home" => "Home",
                                                                                            "feeds" => "Feeds",
                                                                                            "services" => "Services",
                                                                                            "book_a_pandit" => "Book A Pandit",
                                                                                            "organize_a_puja" => "Organize A Puja",
                                                                                            "e_puja" => "e_puja",
                                                                                            "calendar_month" => "Calendar month",
                                                                                            "calendar_day" => "Calendar day",
                                                                                            "compass" => "compass",
                                                                                            "update_user_profile" => "Update User Profile"
                                                                                        );



                                                                                        foreach ($feed_flag_arr as $key => $value) {

                                                                                            //echo "$key->$value";
                                                                                            $selected = "";
                                                                                            //$key = $feed_flag_arr["key"];
                                                                                            //$value = $feed_flag_arr[$i]["feed_name"];
                                                                                            if ($key == $feed_type) {
                                                                                                $selected = "selected";
                                                                                                //$feed_type_img = $feed_flag_arr[$i]["feed_img"];
                                                                                            }
                                                                                            ?>
                                                                                            <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                                                        <?php } ?>
                                                                                    </select>

                                                                                </td>
                                                                            </tr>
                                                                            <!--
                                                                            <tr>
                                                                                <td>Url</td>
                                                                                <td>
                                                                                    <input id="feed_url" class="form-control" type="text" value="<?php echo $feed_url; ?>" placeholder="Feed URL" name="feed_url">
    
                                                                                </td>
                                                                            </tr>
                                                                            -->

                                                                            <tr>
                                                                                <td>Description</td>
                                                                                <td>
                                                                                    <textarea id="feed_desc" class="form-control" placeholder="Description" name="feed_desc"><?php echo $feed_desc; ?></textarea>
                                                                                </td>
                                                                                <td rowspan="2">
                                                                                    <span class="fileupload-preview" style="margin-top: 10px;">
                                                                                        <img src="media/<?php echo $feed_img; ?>" alt="" border="0" style="display:block; width: 200px; " />
                                                                                    </span>
                                                                                </td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td>Image</td>
                                                                                <td>
                                                                                    <div class="col-lg-8">
                                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                                            <span class="btn btn-file btn-default">
                                                                                                <span class="fileupload-new">Select file</span>
                                                                                                <span class="fileupload-exists">Change</span>                                                  
                                                                                                <input type="file" name="feed_img" />
                                                                                            </span>
                                                                                            <span class="fileupload-preview" style="margin-top: 10px;"></span>
                                                                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">x</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>

                                                                            </tr>






                                                                        </tbody>
                                                                    </table>
                                                                </form>
                                                            </div>
                                                        </div>

                                                    </div>



                                                </div>

                                            </div>



                                            <div class="form-actions" style="text-align:center">
                                                <input class="btn btn-primary btn-lg" type="button" id="multi-post" value="Save Changes" name="submit" onclick="add_enquiry()" >

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>                           


                    <hr/><br/><br/>     


                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';          ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->


        <script>
            $(document).ready(function ()
            {


                function getDoc(frame) {
                    var doc = null;

                    // IE8 cascading access check
                    try {
                        if (frame.contentWindow) {
                            doc = frame.contentWindow.document;
                        }
                    } catch (err) {
                    }

                    if (doc) { // successful getting content
                        return doc;
                    }

                    try { // simply checking may throw in ie8 under ssl or mismatched protocol
                        doc = frame.contentDocument ? frame.contentDocument : frame.document;
                    } catch (err) {
                        // last attempt
                        doc = frame.document;
                    }
                    return doc;
                }

                $("#multiform").submit(function (e)
                {
                    $("#enq_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_feed_notification.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#enq_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#enq_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#enq_msg").html('' + data + '');
                        });

                    }

                });


                $("#multi-post").click(function ()
                {

                    $("#multiform").submit();

                });

            });
        </script>











        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
        <script src="assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/daterangepicker/moment.min.js"></script>
        <script src="assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-fileupload.js"></script>
        <script src="assets/js/formsInit.js"></script>
        <script>
            $(function () {
                formInit();
            });
        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
