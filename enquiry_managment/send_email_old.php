<?php
$Main_Menu = "Services";
$Sub_Menu_1 = "list_pooja";




$page = "Pooja";

$page_title = "Send Mail";
$edit_id = 0;
// instantiate product object
//echo "in enquiry";
include_once 'config/auth.php';
//$enqType = $master->listEnqType();
//$enqStatus = $master->listStatus(1);
//$list_pooja = $master->listPooja($type_id);

$error = FALSE;
$error_msg = "";

for ($i = 0; $i < count($_POST); $i++) {
    $_POST[$key] = trim($_POST[$key]);
}




$enq_id = $_POST["enq_id"];
$format = $_POST["format"];
$user = $_POST["user"];
$puja_id = $_POST["puja_id"];

$enq_id = 5;


if ($enq_id > 0) {

    list($status, $id, $result_arr) = getOrderTmpData($enq_id);
    //print_r($result_arr);
    if ($status == 1) {
        //print_r($result_arr);
        //echo "<br/><br/>";
        if ($puja_id > 0) {
            list($puja_status, $id, $puja_arr) = getOrderPujaTmpData($puja_id, $enq_id);
        } else {
            list($puja_status, $id, $puja_arr) = getOrderPujaTmpData(0, $enq_id);
        }


        //print_r($puja_arr);

        /*
          if ($format == "SMS") {
          list($status, $id, $msg) = send_SMS($result_arr, $puja_arr, $user);
          } elseif ($format == "EMAIL") {
          //echo "inside Email Condition";
          list($status, $id, $msg) = send_Email($result_arr, $puja_arr, $user);
          }
         */
    } else {
        $msg = $result_arr;
        echo " Error :- $msg";
    }
}

function send_Email($result_arr, $puja_data, $user) {
    $status = 0;
    $msg = "";
    //echo "inside Email Function";
    //print_r($result_arr);
    //echo "<br/><br/>";
    //print_r($puja_data);  

    $edit_id = $result_arr[0]["id"];
    $order_no = $result_arr[0]["order_no"];

    list($order_date, $order_time) = validateDbDateTime($result_arr[0]["order_date"]);
    $order_status = $result_arr[0]["order_status"];
    $enq_no = $result_arr[0]["enq_no"];

    list($enq_date, $enq_time) = validateDbDateTime($result_arr[0]["enq_date"]);

    $cust_name = $result_arr[0]["customer_name"];
    $cust_no = $result_arr[0]["customer_contact"];
    $cust_email = $result_arr[0]["customer_email"];
    $cust_city = $result_arr[0]["customer_city"];
    $cust_area = $result_arr[0]["customer_area"];
    $cust_service_request = $result_arr[0]["service_req"];
    $cust_address = $result_arr[0]["customer_address"];

    $total_amount = $result_arr[0]["total_amt"];
    $pandit_payment = $result_arr[0]["pandit_payment_total"];
    $paid_to_pandit = $result_arr[0]["paid_to_pandit"];
    $payment_mode = $result_arr[0]["payment_mode"];
    $cust_payment_receive_status = $result_arr[0]["cust_pay_receive"];
    $pandit_payment_receive_status = $result_arr[0]["pandit_pay_status"];
    $collected_by = $result_arr[0]["collect_by"];

    $enquiry_handled_by = $result_arr[0]["enq_handle_by"];
    $order_handled_by = $result_arr[0]["ord_handle_by"];

    $feedback_type = $result_arr[0]["customer_feedback"];
    $comments = $result_arr[0]["comments"];





    $mail_head = <<<EOD

<!DOCTYPE html>
<html>
    <body>
        <div align="center" style="width:100%">
            <div align="center" style="background-color:#FFF;max-width:700px;min-width:200px">

                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>General Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Order </td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$order_no</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$order_date $order_time</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$order_status</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Enquiry </td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$enq_no</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$enq_date $enq_time</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;"> </td>
                    </tr>

                    </tbody>
                </table>

                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Customer Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Name </td>
                        <td colspan="3" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$cust_name</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Contact </td>

                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$cust_email</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$cust_no</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Address </td>
                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top; width: 200px;">$cust_address</td>

                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$cust_area <br/>$cust_city</td>
                    </tr>

                    </tbody>
                </table>

           
EOD;




    $puja_details = "";

    if ($puja_data[0]["id"] > 0) {


        for ($i = 0; $i < count($puja_data); $i++) {

            $puja_name = $puja_data[$i]["puja_name"];

            list($puja_date, $puja_time) = validateDbDateTime($puja_data[$i]["puja_date"]);


            $puja_address = $puja_data[$i]["puja_address"];
            $samagri_arranged_by = $puja_data[$i]["samagri_arrange_by"];
            $samagri_type = $puja_data[$i]["samagri_type"];
            $pandit_name = $puja_data[$i]["pandit_name"];
            $pandit_contact = $puja_data[$i]["pandit_contact"];
            $pandit_reference = $puja_data[$i]["pandit_refrance"];

            if ($user == "Customer") {
                $pandit_contact = "";
            }

            $puja_details .= <<<EOD


                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Puja Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$puja_name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$puja_date $puja_time</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Samagri </td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Pandit Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$pandit_name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$pandit_contact</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Type :- $samagri_type   </td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Address </td>

                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$puja_address</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;"> Arranged By :- $samagri_arranged_by   </td>
                    </tr>
                    <tr><td colspan="4" class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;line-height: 5px;vertical-align: top;"> </td></tr>
                    
   
   
   
           
EOD;
        }


        $puja_details = <<<EOD

<table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Puja Details</h3>

                    </th>
                    </tr>
            $puja_details
            
            </tbody>
                </table>
           
EOD;
    }



    if ($user != "Customer") {


        $mail_pandit_payment = <<<EOD

<tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Pandit Payment</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$pandit_payment $pandit_payment_receive_status</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Paid To Pandit</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$paid_to_pandit</td>
                    </tr>
           
EOD;

        $mail_private = <<<EOD




                <table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Other Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Handled By</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$order_handled_by</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Enquiry By</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$enquiry_handled_by</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Feed Back </td>                        
                        <td colspan="3" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$feedback_type</td>

                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Comments</td>
                        <td colspan="3" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$comments</td>

                    </tr>

                    </tbody>
                </table>
           
EOD;
    }



    $mail_Payment = <<<EOD

<table class="tbl_comparison" style="width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="4" style="color: #333;font-weight: bold; border-bottom: 1px solid #dce3eb;color: #7b7b79;vertical-align: top;text-align: center;"> 
                    <h3>Payment Details</h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Payment Mode</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$payment_mode</td>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Collected By</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$collected_by</td>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;border-left: 1px solid #dce3eb; border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">Total Amount </td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">$total_amount $cust_payment_receive_status</td>
                        <td colspan="2" class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 5px;vertical-align: top;">&nbsp;</td>

                    </tr>
            
                    $mail_pandit_payment

                    </tbody>
                </table>


               $mail_private 
           
EOD;








    $mail_footer = <<<EOD


            </div>
        </div>
    </body>
</html>
           
EOD;





    $message = $mail_head . $puja_details . $mail_Payment . $mail_footer;

    echo $message;



    $to = $cust_email;


    if ($user != "Customer") {
        $to = 'ninad.tilkar@choiceindia.com';
    }
    if ($user == "info") {
        $to = 'info@wheresmypandit.com';
    }

    $to = 'ninad.tilkar@choiceindia.com';

    //$from = "wheresmypandit.com";
    //$from = "choiceuae.ae";
    $from = "panditji@wheresmypandit.com";
    $subject = "Puja Details of Enquiry $enq_no and Order $order_no";

    $headers = "From: $from \r\n";
    $headers .= "Reply-To: $from \r\n";
    $headers .= "CC: ninad.tilkar@choiceindia.com\r\n"; //,info@wheresmypandit.com
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


    /*
      if (mail($to, $subject, $message, $headers, '-fpanditji@wheresmypandit.com')) {
      $mail_msg = "mail Send Successfully to $user $to";
      $status = 1;
      } else {
      $mail_msg = "There is some problem to send mail";
      }
     */




    return array($status, $id, $mail_msg);
}

$cust_id = 0;

$enq_date = date("d-m-Y"); //date(d-m-Y);
$enq_time = date("h:i a"); //date(d-m-Y);
//$stmt = $location->list_Country();
//$stmt = $master->listPooja();

$client_mail_body = <<< END


<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top" style="padding:20px 0 20px 0">
                <!-- [ header starts here] -->
                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
                    <tr>
                        <td valign="top">
                            <a href="https://www.wheresmypandit.com/"><img src="https://www.wheresmypandit.com/media/logo/default/logo.png" alt="Where's My Pandit" style="margin-bottom:10px; width:100px;" border="0"/></a>
                        </td>
                    </tr>
                    <!-- [ middle starts here] -->
                    <tr>
                        <td valign="top">
                            <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Registration Form Details</h1>
                        </td>
                    </tr>
        
                    <tr>
                        <td>
                            <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #E0E0E0;">

                                <tr>
                                    <td> Order <td/> <td> 100000980 <td/> <td> Name </td> <td> Ninad Tilkar </td>
                                </tr>
                                <tr>
                                    <td> Order Date <td/> <td> 29-11-2015 07:52 pm <td/> <td> Contact Number </td> <td> 8108869247 </td>
                                </tr>
                                <tr>
                                    <td> Order Status <td/>  <td> Invoice Generated <td/> <td> Email </td> <td> ninad.tilkar@choiceindia.com </td>
                                </tr>
                                <tr>
                                    <td> Payment Mod <td/>  <td> Online <td/>  <td> Address </td> 
                                    <td> D 214, Sungrace, raheja vihar, chandivali, Mumbai - 400072  </td>
                                </tr>
                                <tr>
                                    <td> Total Amount <td/>  <td> 5000 <td/> <td> City </td> <td> Ninad Tilkar </td>
                                </tr>
                                    
                                    
                                    
                                    
                                        
                                    
                                </tr>
                               
                            </table>
                        </td>
                    </tr>
        
                    <tr>
                        <td>
                            <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">

                                <tr><td> Name </td><td> $enq_name </td></tr>
                                <tr><td> E-mail </td><td> $enq_email </td></tr>
                                <tr><td> Phone </td><td> $enq_ext - $enq_phone </td></tr>
                               
                                
                                <tr><td> Area </td><td> $enq_area </td></tr>
                                <tr><td> City </td><td> $enq_city </td></tr>
                                <tr><td> Pincode </td><td> $enq_pincode </td></tr>
                                
        
                                <tr><td> Service Required </td><td> $enq_service_type </td></tr>
                                <tr><td> Language, Caste of Pandit Required </td><td> $enq_cast </td></tr>
                                <tr><td> Pujas to be performed </td><td> $puja_performed </td></tr>
                                <tr><td> Puja Date </td><td> $enq_puja_date </td></tr>
                                <tr><td> Pandit's from </td><td> $enq_pandit_from </td></tr>
        
                                <tr><td> Other Specifications </td><td> $enq_comments </td></tr>
         
  
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>        

END;
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES -->
        <!-- <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">


                                        <li class=""><a data-toggle="tab" href="#User-Details">User Details</a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'enquiry-details'); ?>">
                                            <a data-toggle="tab" href="#enquiry-details"><span> Enquiry Details</span></a>
                                        </li>

                                        <?php if ($enq_id > 0) { ?>
                                            <li class="<?php getActiveTab($tab, 'puja-details'); ?>">
                                                <a data-toggle="tab" href="#puja-details"><span> Puja Details</span></a>
                                            </li>
                                            <li class="<?php getActiveTab($tab, 'Followup-Tab'); ?>">
                                                <a data-toggle="tab" href="#Followup-Tab">Followup</a>
                                            </li>
                                            <li class="<?php getActiveTab($tab, 'quotation-details'); ?>">
                                                <a data-toggle="tab" href="#quotation-details"><span>  Quotation Details</span></a>
                                            </li>
                                            <?php if ($enq_quotation_id > 0) { ?>
                                                <li class="<?php getActiveTab($tab, 'Final-Details'); ?>">
                                                    <a data-toggle="tab" href="#Final-Details">Final Details</a>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>




                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                        <div id="enquiry-details" class="tab-pane fade <?php getActiveBody($tab, 'enquiry-details'); ?>">
                                            <form name="enquiry_form" id="enquiry_form" action="" method="POST" enctype="multipart/form-data">
                                                <?php //echo $client_mail_body; ?>




                                                <body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
                                                    <div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
                                                        <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
                                                            <tr>
                                                                <td align="center" valign="top" style="padding:20px 0 20px 0">
                                                                    <!-- [ header starts here] -->
                                                                    <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
                                                                        <tr>
                                                                            <td>
                                                                                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #E0E0E0;">
                                                                                    <tr>
                                                                                        <td valign="top" style="padding-left: 10px;">
                                                                                            <a href="https://www.wheresmypandit.com/">
                                                                                                <img src="https://www.wheresmypandit.com/media/logo/default/logo.png" alt="Where's My Pandit" style="margin-bottom:10px; width:100px;" border="0"/>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td valign="center">
                                                                                            <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Welcome Ninad</h1>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>                            
                                                                                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #E0E0E0;">
                                                                                    <tr style="padding-bottom: 10px;">
                                                                                        <td valign="top" style="padding-left: 10px; margin-bottom: 10px;">
                                                                                            <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="300" style="">
                                                                                                <tr>                                    
                                                                                                    <td style="width: 75px;"> Name </td> <td> Ninad Tilkar </td>
                                                                                                </tr>
                                                                                                <tr>                                    
                                                                                                    <td> Number </td> <td> 8108869247 </td>
                                                                                                </tr>
                                                                                                <tr>                                    
                                                                                                    <td> Email </td> <td> ninad.tilkar@choiceindia.com </td>
                                                                                                </tr>
                                                                                                <tr>                                    
                                                                                                    <td valign="top"> Address </td> <td> D 214, Sungrace, raheja vihar, chandivali, Mumbai - 400072  </td>
                                                                                                </tr>
                                                                                                <tr>                                    
                                                                                                    <td> City </td> <td> Mumbai </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="270" style="">
                                                                                                <tr>
                                                                                                    <td> Order <td/> <td> 100000980 <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Order Date <td/> <td> 29-11-2015 07:52 pm <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Order Status <td/>  <td> Invoice Generated <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Payment Mod <td/>  <td> Online <td/>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Total Amount <td/>  <td> 5000 <td/> 
                                                                                                </tr>

                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td>                            
                                                                                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #E0E0E0;">

                                                                                    <tr style="padding-bottom: 10px;">
                                                                                        <td valign="top" style="padding-left: 10px; margin-bottom: 10px;">

                                                                                            <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="100%" style="">

                                                                                                <tr>
                                                                                                    <td> Puja Name <td/> <td>  Griha Pravesh Puja  <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Date  <td/> <td> 29-11-2015 10:30 am <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Address  <td/>  <td> Flat no 304, Sai Sangam, sector 5, ulve, Belapur, Navi mumbai  <td/> 
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td> Samagri By <td/>  <td> All WMP <td/>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Pandit Name <td/> <td>  Griha Pravesh Puja  [No Pandit contact] <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Payment Mode <td/> <td> Online  <td/> 
                                                                                                </tr>                                
                                                                                                <tr>
                                                                                                    <td> Customer Payment <td/>  <td> 5000 <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Pandit Payment  <td/> <td> 3000  <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Executive <td/>  <td> Prakash  <td/> 
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>

                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>                            
                                                                                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #E0E0E0;">

                                                                                    <tr style="padding-bottom: 10px;">
                                                                                        <td valign="top" style="padding-left: 10px; margin-bottom: 10px;">

                                                                                            <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="100%" style="">

                                                                                                <tr>
                                                                                                    <td> Puja Name <td/> <td>  Griha Pravesh Puja  <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Date  <td/> <td> 29-11-2015 10:30 am <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Address  <td/>  <td> Flat no 304, Sai Sangam, sector 5, ulve, Belapur, Navi mumbai  <td/> 
                                                                                                </tr>

                                                                                                <tr>
                                                                                                    <td> Samagri By <td/>  <td> All WMP <td/>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Pandit Name <td/> <td>  Griha Pravesh Puja  [No Pandit contact] <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Payment Mode <td/> <td> Online  <td/> 
                                                                                                </tr>                                
                                                                                                <tr>
                                                                                                    <td> Customer Payment <td/>  <td> 5000 <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Pandit Payment  <td/> <td> 3000  <td/> 
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td> Executive <td/>  <td> Prakash  <td/> 
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>

                                                                                </table>
                                                                            </td>
                                                                        </tr>

                                                                        <!--
                                                                        <tr>
                                                                                      <td>
                                                                                
                                                                                
                                                                                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #E0E0E0;">
                                                    
                                                                                    <tr>
                                                                                        <td> Order <td/> <td> 100000980 <td/> 
                                                                                        <td> Name </td> <td> Ninad Tilkar </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td> Order Date <td/> <td> 29-11-2015 07:52 pm <td/> 
                                                                                        <td> Contact Number </td> <td> 8108869247 </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td> Order Status <td/>  <td> Invoice Generated <td/> 
                                                                                        <td> Email </td> <td> ninad.tilkar@choiceindia.com </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td> Payment Mod <td/>  <td> Online <td/>  
                                                                                        <td> Address </td> <td> D 214, Sungrace, raheja vihar, chandivali, Mumbai - 400072  </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td> Total Amount <td/>  <td> 5000 <td/> 
                                                                                        <td> City </td> <td> Ninad Tilkar </td>
                                                                                    </tr>
                                                                                   
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                            
                                                                        <tr>
                                                                            <td>
                                                                                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
                                                    
                                                                                    <tr><td> Name </td><td> $enq_name </td></tr>
                                                                                    <tr><td> E-mail </td><td> $enq_email </td></tr>
                                                                                    <tr><td> Phone </td><td> $enq_ext - $enq_phone </td></tr>
                                                                                   
                                                                                    
                                                                                    <tr><td> Area </td><td> $enq_area </td></tr>
                                                                                    <tr><td> City </td><td> $enq_city </td></tr>
                                                                                    <tr><td> Pincode </td><td> $enq_pincode </td></tr>
                                                                                    
                                                            
                                                                                    <tr><td> Service Required </td><td> $enq_service_type </td></tr>
                                                                                    <tr><td> Language, Caste of Pandit Required </td><td> $enq_cast </td></tr>
                                                                                    <tr><td> Pujas to be performed </td><td> $puja_performed </td></tr>
                                                                                    <tr><td> Puja Date </td><td> $enq_puja_date </td></tr>
                                                                                    <tr><td> Pandit's from </td><td> $enq_pandit_from </td></tr>
                                                            
                                                                                    <tr><td> Other Specifications </td><td> $enq_comments </td></tr>
                                                             
                                                      
                                                                                    
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        -->


                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </body>        


















                                            </form>

                                        </div>
                                        <div id="puja-details" class="tab-pane fade <?php getActiveBody($tab, 'puja-details'); ?>">

                                            <form name="puja_details" id="puja_details" action="" method="POST" enctype="multipart/form-data">
                                                <div id="show_puja_details"> 
                                                    <?php include 'include/blocks/add_enq_puja_block.php'; ?>
                                                </div>
                                            </form>


                                        </div>

                                        <div id="Followup-Tab" class="tab-pane fade <?php getActiveBody($tab, 'Followup-Tab'); ?>">  

                                            <div class="box" id="followup_msg" >
                                                <div class="col-lg-12">            

                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div id="enq_comments">
                                                        <?php include 'include/blocks/list_followup.php'; ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div class="body">
                                                        <?php include 'include/blocks/add_followup.php'; ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div id="quotation-details" class="tab-pane fade <?php getActiveBody($tab, 'quotation-details'); ?>">

                                            <div id="show_puja_quotation_details">
                                                <?php include 'include/blocks/add_quotation_block.php'; ?>
                                            </div>


                                        </div>






                                        <div id="Final-Details" class="tab-pane fade <?php getActiveBody($tab, 'Final-Details'); ?>">


                                            <div id="show_order_details"> 
                                                <?php include 'include/blocks/add_order_block.php'; ?>
                                            </div>




                                        </div>



                                        <div id="User-Details" class="tab-pane fade <?php getActiveBody($tab, 'User-Details'); ?>">

                                            <div class="col-lg-6">

                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> User Details </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">

                                                                <tbody>


                                                                    <tr>
                                                                        <td>Number</td>
                                                                        <td><?php echo $phone_1 . "<br/>" . $phone_2; ?></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Email Id</td>
                                                                        <td><?php echo $email_1 . "<br/>" . $email_2; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td><?php echo $first_name . " " . $last_name; ?></td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address</td>
                                                                        <td><?php echo $address_1; ?></td>
                                                                <input type="hidden" value="<?php echo $address_1; ?>" id="cust_puja_address" name="cust_puja_address">
                                                                </tr>

                                                                </tbody>
                                                            </table>




                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <!-- <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button> -->
                                                                <a class="btn text-warning btn-xs btn-flat" href="add_user.php?edit_id=<?php echo $cust_id; ?>">
                                                                    <i class="icon-edit-sign icon-white"></i> Edit</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="col-lg-6">

                                                <!--                                             
                                                                                             <div class="box">
                                                                                                 <header>
                                                                                                     <h5>Payment History</h5>
                                                                                                     <div class="toolbar">
                                                                                                         <div class="btn-group">
                                                                                                             <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                                                                 <i class="icon-chevron-up"></i>
                                                                                                             </a>
                                                                                                         </div>
                                                                                                     </div>
                                                                                                 </header>
                                                                                                 <div class="body collapse in" id="sortableTable">
                                                                                                     <table class="table table-bordered sortableTable responsive-table">
                                             
                                                                                                         <tbody>
                                             
                                             
                                                                                                             <tr>
                                                                                                                 <td>25-March-2015</td>
                                                                                                                 <td>Online</td>
                                                                                                                 <td>2000</td>
                                                                                                             </tr>
                                             
                                                                                                             <tr>
                                                                                                                 <td>17-August-2015</td>
                                                                                                                 <td>Online</td>
                                                                                                                 <td>5000</td>
                                                                                                             </tr>
                                             
                                             
                                             
                                             
                                             
                                             
                                                                                                         </tbody>
                                                                                                     </table>
                                                                                                 </div>
                                                                                             </div>
                                                -->
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';  ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script src="include/js/ninad.functions.enquiry.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->    

    </body>
    <!-- END BODY-->

</html>



