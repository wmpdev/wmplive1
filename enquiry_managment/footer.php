   





<!-- FOOTER -->
<div id="footer">
    <p>&copy;  Where's My Pandit. All Rights Reserved. &nbsp;2015 &nbsp;</p>
</div>
<!--END FOOTER -->
<!-- GLOBAL SCRIPTS -->
<script src="assets/plugins/jquery-2.0.3.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js"></script>

<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/bootstrap-timepicker.js"></script>
<!-- END GLOBAL SCRIPTS -->

<script type="text/javascript">
    $(document).ready(function(){
        $(".datepicker").datepicker();
    })
    
</script>


<!-- pusher api -->


<script src="pusher.min.js"></script>
<script>

    var api_key = '<?php echo $_SESSION["api_key"] ?>';
    // Enable pusher logging - don't include this in production
    Pusher.log = function (message) {
        if (window.console && window.console.log) {
            window.console.log(message);
        }
    };

    //get key of specific agent after login from session
    //142ff267b641e853a453
    //d3f736e90fc24d1891c9
    var pusher = new Pusher(api_key, {
        encrypted: true
    });
    var channel = pusher.subscribe('test_channel');
    channel.bind('my_event', function (data) {
        //alert(data.alertTitle+" "+data.message);
        notifyMe(data);
    });




    function notifyMe(res) {
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.');
            return;
        }

        if (Notification.permission !== "granted")
            Notification.requestPermission();
        else {
            body = "\n" + res.message + "\n\nClick Here...";
            var notification = new Notification(res.alertTitle, {
                icon: 'phone.png',
                body: body,
            });

            notification.onshow = function () {
                //alert();
            }

            /*notification.onclose = function() {
             //ajax call
             
             }*/

            notification.onclick = function () {
                //window.open("https://www.wheresmypandit.com/enquiry_managment/add_user.php?caller="+res.caller_num);

                //ajax call
                $.ajax({
                    url: 'fetchUser.php',
                    data: {number: res.caller_num},
                    dataType: 'json',
                    method: 'POST',
                    success: function (datas)
                    {
                        //var d=$.makeArray(datas);
                        //console.log(datas);
                        var param;
                        if (datas != "")
                            param = "edit_id=" + datas[0].id;
                        else
                            param = "caller=" + res.caller_num;

                        window.open("http://localhost/enquiry_managment/add_user.php?" + param);
                        notification.close();
                    }
                })
            };

        }

    }
</script>