<?php
include_once 'config/auth.php';

class ClassA {

    public $var;

    function __construct() {
        $this->setVar();
        $this->getVar();
    }

    protected function getVar() {
        echo $this->var;
        echo "<br/> ClassA GetVar<br/>";
    }

    public function setVar() {
        $this->var = 1000;
        echo "<br/> ClassA setVar<br/>";
    }

}

class ClassAChildA extends ClassA {

    function __construct() {
        $this->setVar();
        $this->getVar();
    }

    public function setVar() {
        $this->var = 1001;
        echo "<br/> ClassAChildA setVar<br/>";
    }

}

class ClassAChildB extends ClassA {

    function __construct() {
        $this->var = 1002;
        $this->getVar();
    }

    public function getVar() {
        echo $this->var;
        echo "<br/> ClassAChildB getVar<br/>";
    }

}

//prints 1000
echo "<br/> Create New ClassA <br/>";
$parent = new ClassA();
echo "<br/> Create New ClassA End<br/>";

//prints 1001
echo "<br/> Create New ClassAChildA <br/>";
$childA = new ClassAChildA();
echo "<br/> Create New ClassAChildA End<br/>";

//prints 1002
echo "<br/> Create New ClassAChildB <br/>";
$childB = new ClassAChildB();
echo "<br/> Create New ClassAChildB End<br/>";





print_r($_SESSION);
echo "<br/> time = " . time();
echo $date = "<br/>" . date('d/m/Y h:i:s a', time());
echo $date = "<br/>" . date('d/m/Y', "23/05/2015");


$time = strtotime('23/05/2015');
$newformat = date('Y-m-d', $time);
echo "<br/>" . $newformat;

echo "<br/>" . $time_in_24_hour_format = date("H:i", strtotime("1:30 PM"));

echo "<br/>" . $ymd = DateTime::createFromFormat('d-m-Y', '23-05-2015')->format('Y-m-d');

echo "<br/>ymd = " . $ymd = DateTime::createFromFormat('d-m-Y h:i a', '23-05-2015 2:15 PM')->format('Y-m-d h:i:s');

echo "<br/>ymd = " . $ymd = DateTime::createFromFormat('d-m-Y H:i:s', '06-05-2015 15:30:00')->format('Y-m-d H:i:s');

echo "<br/>";
echo "<br/>";

$add_date = "06-05-2015 15:30:00";
$next_date = "23-05-2015 02:15 pm";

echo "<br/> add = " . $add_date = DateTime::createFromFormat('d-m-Y H:i:s', $add_date)->format('Y-m-d H:i:s');
echo "<br/> next = " . $next_date = DateTime::createFromFormat('d-m-Y H:i a', $next_date)->format('Y-m-d H:i:s');

$data = 5;
$val_1 = 3;
$val_2 = 2;

if ($data > $val_1) {
    echo "$data is greater than $val_1 <br/>";
} elseif ($data > $val_2) {
    echo "$data is greater than $val_2 <br/>";
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title>BCORE Admin Dashboard Template | Blank Page</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES -->
        <link href="assets/css/layout2.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner" style="min-height:1200px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Admin Dashboard </h2>



                        </div>
                    </div>

                    <hr />




                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php include 'right_menu.php'; ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->

    </body>
    <!-- END BODY-->

</html>






<script type="text/javascript" src="/skin/frontend/nextlevel/default/js/jquery.datetimepicker.js"></script>
<div class="col-main">
    <h1>ENQUIRY FORM For Rudrabhishek </h1>
    <p style="text-align: justify;">
        <span style="text-align: justify;">
            Rudrabhishek puja is an age old practice followed in India since time immemorial. ‘Rudra’ signifies ‘Shiv - the Benevolent', ' the Destroyer of Evil'. Rudrabhishek is the best way of offering prayers to Lord Shankar. Lord Shiva is worshipped in his Rudra form. It is also referred to as 'Lagho Rudrabhishek', 'Maha Rudrabhishek' or 'Ati Rudrabhishek'. Rudrabhishek puja helps to bring prosperity, good health, peaceful wedded life, graha shanti etc. Through this puja one can aim for inner peace and fulfillment.
        </span>
        <br/><br/>
        <strong style="text-align: justify;">The Puja will be conducted by Pandit Ramdevji Mishra</strong><br/>
        <strong style="text-align: justify;">Date: 2nd August, 2015, Sunday</strong><br/>
        <strong style="text-align: justify;">Time: 8 A.M onwards</strong><br/>
        <strong style="text-align: justify;">Address: Goenka Hall, J. B. Nagar, Andheri East, Mumbai- 400059</strong><br/>
        
        <br />        
    </p>
    <p style="text-align: center;">
        To cater to your religious requirements with the best 
        <strong>Pandits service in India</strong>, 
        <strong>you can connect with us through the form given below :</strong>
    </p>
</div>
<div id="webform_3_form">
    <div id="feedback" style="display: block;"></div>
    <iframe id="webform_3_iframe" style="width: 0; height: 0; border: 0; position: absolute;" name="webform_3_iframe" width="320" height="240"></iframe>
    <form id="webform_3" class="webforms-contact-us" action="/index.php/eventmanager/index/eventRequest/?isAjax=true" method="post" name="webform_3" enctype="application/x-www-form-urlencoded" target="webform_3_iframe">
        <p class="required">* Required Fields</p>
        <input id="request" class="input-text" type="hidden" name="request" value="Pandit Booking" />
        <div class="fieldset fieldset-5">
            <ul class="form-list">
                <li class="fields" style="margin-bottom: 20px;">
                    <div class="field webforms-fields-"><label class="required" for="name"> <em>*</em> Name </label>
                        <div class="input-box"><input id="name" class="input-text required-entry" type="text" name="name" value="" /></div>
                    </div>
                </li>
                <li class="fields" style="margin-bottom: 20px;">
                    <div class="field webforms-fields-"><label class="required" for="email"> <em>*</em> Email ID </label>
                        <div class="input-box"><input id="email" class="input-text required-entry validate-email" type="text" name="email" value="" /></div>
                    </div>
                </li>
                <li class="fields" style="margin-bottom: 20px;">
                    <div class="field webforms-fields-"><label class="required" for="phone"> <em>*</em> Phone Number </label>
                        <div class="input-box"><input id="phone" class="input-text required-entry validate-number" type="text" name="phone" value="" /></div>
                    </div>
                </li>
                
                <li class="fields" style="margin-bottom: 20px;">
                    <div class="field webforms-fields-"><label class="required" for="comments"> <em></em> Comments </label>
                        <div class="input-box"><textarea id="comments" class="input-text" name="comments" cols="3" rows="3"></textarea></div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="buttons-set">
            <button id="webform_3_submit_button" class="button" title="submit" onclick="" type="button"> <span> <span>Submit</span> </span> </button> 
            <span id="webform_3_sending_data" class="please-wait" style="display: none;"> 
                <img class="v-middle" title="Sending..." src="/skin/frontend/nextlevel/default/images/loading.gif" alt="Sending..." /> 
                <span id="webform_3_progress_text">Sending...</span> 
            </span>
        </div>
    </form></div>










<script type="text/javascript" language="javascript">// <![CDATA[
		jQuery.noConflict();
		

    function sendRequest() {




    var enq_name = document.getElementById("name").value
    var enq_email = document.getElementById("email").value
    var enq_phone = document.getElementById("phone").value
    var enq_comments = document.getElementById("comments").value


//alert(" enq_type_id = " + enq_type_id+" , enq_item_id = "+enq_item_id+" , enq_quantity = "+enq_quantity);
//alert("i am in state value = "+country_name);
    if (enq_name == "") {
        //document.getElementById("enq_comments").innerHTML = "No Comments";
document.getElementById("feedback").innerHTML = "Thank You Ninad";
        return;
    } else {
        //alert(" country = " + enq_type_id);
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        //Set the loading image
        //document.getElementById("load").innerHTML = '<img src="Images/ajax-loader.gif" border="0" alt="Loading, please wait..." />';
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("enq_comments").innerHTML = "";
                document.getElementById("enq_comments").innerHTML = xmlhttp.responseText;
                //document.getElementById('enq_type').se   .value = 'bike';??????????
                //refresh_enquiry();
            }
        }
        var url = "ajax_followup.php";
        var param = "type=list&enq_id=" + enq_id;
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(param);

    }

}

    
    
    
    
    
    function getUrlParameter(sParam) {
		    var sPageURL = window.location.search.substring(1);
		    var sURLVariables = sPageURL.split('&');
		    for (var i = 0; i < sURLVariables.length; i++) 
		    {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
			    return sParameterName[1];
			}
		    }
		    return '';
		}
		
		addOption = function(selectbox, text, value, selected_value) {
			var optn = document.createElement("OPTION");
			optn.text = text.replace(/\s*\(\s*[\d+\,]+\s*\)\s*/, '');
			optn.value = value;
			if (selected_value != '' && selected_value != 'null' && selected_value != null && selected_value.toLowerCase().replace(/\W+/g, '').replace(/\d/g, '') == optn.text.toLowerCase().replace(/\W+/g, '').replace(/\d/g, '')) {
				optn.selected = true;
			}
			selectbox.options.add(optn);  
		}

		emptySelect = function(selectbox) {
			var length = selectbox.options.length;
			for (i = 0; i <= length; i++) {
				selectbox.remove(i);
			}
			selectbox.options.length = 0;
		}

		new Ajax.Request('/index.php/eventmanager/index/getCategories/?isAjax=true', {
			method: 'get',
			onComplete: function(req) {
				emptySelect(document.getElementById('type'));
				addOption(document.getElementById('type'), 'Select type of Pooja', '', '');
				var parsedObj = JSON.parse(req.responseText);
				//selected_value = getUrlParameter('c1').replace('-', ' ');
				selected_value = '';
				for (i=0; i<parsedObj.length; i++) {
					addOption(document.getElementById('type'), parsedObj[i].label, parsedObj[i].value, selected_value);
					if (selected_value != '' && selected_value != 'null' && selected_value != null && selected_value.toLowerCase() == parsedObj[i].label.toLowerCase()) {
						getSubCategories(parsedObj[i].value);
					}
				}
				addOption(document.getElementById('type'), 'Other', '-1', '');
			}
		});

		var selectedCategory = '';
		var selectedSubCategory = '';		
		function getSubCategories(category_id) {
			if (category_id > 0) {
				jQuery("#lioccasion").show();
				jQuery("#liother_occasion").hide();
				var length = document.getElementById('type').options.length;
				for (i = 0; i <= length; i++) {
					if (document.getElementById('type').options[i].value == category_id) {
						selectedCategory = document.getElementById('type').options[i].label;
						break;
					}
				}
				new Ajax.Request('/index.php/eventmanager/index/getCategories/?isAjax=true', {
					method: 'post',
					parameters: 'id=' + category_id + '&level=1',
					onLoading: function() { jQuery('#select_type').show(); },
					onComplete: function(req) {
						jQuery('#select_type').hide(); 
						emptySelect(document.getElementById('occasion'));
						addOption(document.getElementById('occasion'), 'Select an occasion', '', '');
						if (req.responseText == '' || req.responseText == 'null' || req.responseText == null) {
							jQuery("#occasion").attr('disabled', 'disabled');
							jQuery("#lioccasion").hide();
						} else {
							var parsedObj = JSON.parse(req.responseText);
							for (i=0; i<parsedObj.length; i++) {
								jQuery("#occasion").attr('disabled', false);
								jQuery("#lioccasion").show();
								//addOption(document.getElementById('occasion'), parsedObj[i].label, parsedObj[i].value, getUrlParameter('c2').replace('-', ' '));
								addOption(document.getElementById('occasion'), parsedObj[i].label, parsedObj[i].value, '');
							}
						}
					}
				});
			} else if (category_id == -1) {
				jQuery("#lioccasion").hide();
				jQuery("#liother_occasion").show();
			} else {
				jQuery("#lioccasion").show();
				jQuery("#liother_occasion").hide();
				emptySelect(document.getElementById('occasion'));
				addOption(document.getElementById('occasion'), 'Select an occasion', '', '');
			}
		}

		var webform_3 = new VarienForm('webform_3', 0);
		var webform_3_submit = function(){
			var form = webform_3;
			if(form.validator && form.validator.validate()){
				/*form.submit();
				jQuery('#webform_3_submit_button').hide();
				jQuery('#webform_3_sending_data').show();*/
				new Ajax.Updater(
					{success:'webform_3'}, 
					'/index.php/eventmanager/index/eventRequest/?isAjax=true', 
					{method:'post', action:"/index.php/eventmanager/index/eventRequest/?isAjax=true", asynchronous:true, evalScripts:true, onComplete:function(request, json){jQuery('#feedback').show().innerHTML(json);}, onLoading:function(request, json){jQuery('#webform_3_submit_button').hide();jQuery('#webform_3_sending_data').show();}, 
					parameters:Form.serialize($('webform_3'))});
			}
		}

		var iframe = $('webform_3_iframe');
		iframe.observe('load',function(){
			var doc = this.contentDocument ? this.contentDocument: this.contentWindow.document;
			var json = {success:false};
			if(doc.body.innerHTML.unfilterJSON())
				json = doc.body.innerHTML.evalJSON();
			else return;
			if(json.success > 0){
				if(json.script){
					eval(json.script);
					return;
				}
				if(json.redirect_url){
					jQuery('#webform_3_progress_text').update('Redirecting');
					window.location = json.redirect_url;
					return;
				}
				jQuery('#webform_3_progress_text').update('Complete');
				Effect.Fade('webform_3_form',{
					duration: 0.5, from:1, to:0,
					afterFinish: function(){
						jQuery('#webform_3_success_text').update(json.success_text.unescapeHTML()).show();
						Effect.Fade('webform_3_success_text',{
							duration:0.5, from:0, to:1
						});
					}
			
				});
			} else {
				if(jQuery('#webform_3_sending_data'))
					jQuery('#webform_3_sending_data').hide();
				if(jQuery('#webform_3_submit_button'))
					jQuery('#webform_3_submit_button').show();
				if(json.errors && typeof(json.errors) == "string"){
					Dialog.alert(json.errors.unescapeHTML(),{
						title: "Error(s) occured",
						className: "magento",
						width:300, 
						buttonClass: "button",
						okLabel: "Close",
						destroyOnClose: true, 
						recenterAuto:true
					})
				} else {
					alert('Error(s) occured');
				}
				if(json.script){
					eval(json.script);
				}
			}
		});

		jQuery( document ).ready(function( $ ) {
			jQuery("#occasion_on").datetimepicker({
				timepicker: false,
				format: 'd-m-Y',
				minDate: 0,
				closeOnDateSelect: true,
			});
			jQuery("#occasion_on_time").datetimepicker({
				datepicker: false,
				format: 'h:i A',
				formatTime: 'h:i A',
				step: 30,
			});
		});
// ]]></script>

