<div class="col-lg-12">

    <div class="box">
        <header>

            <!--
<div class="col-lg-6">
    <h5>Pooja </h5>
    <input type="hidden" value="" name="followup_enq_id" id="followup_enq_id"> 
    <div class="input-group">
        <input type="text" value="09-06-2015" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="current_date" id="current_date">
    </div>
</div>       

<div class="col-lg-5">
    <h5>Enquiry </h5>
    <input type="hidden" value="" name="followup_enq_id" id="followup_enq_id"> 
    <div class="input-group">
        <input type="text" value="09-06-2015" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="current_date" id="current_date">
    </div>
</div>       
            -->

            <div class="col-lg-5">
                <a href="edit_enquiry_details.php?enq_id=<?php echo $enq_cache_id;?>"><h5>Edit Details</h5></a>
                <input type="hidden" value="" name="followup_enq_id" id="followup_enq_id"> 
                
            </div> 


            <div class="toolbar">
                <div class="btn-group">
                    <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
        </header>
        <div class="body in" id="sortableTable" style="height: auto;">



            <?php if (count($_SESSION[$enq_cache_id]) > 0) { ?>    
                <table class="table table-bordered sortableTable responsive-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Item</th>
                            <th>Actual</th>
                            <th>Selling</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th style="width:50px">Disc</th>
                            <th>Transport</th>
                            <th>Tax</th>
                            <th>Total</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php for ($i = 0; $i < count($_SESSION[$enq_cache_id]); $i++) {
                            ?>
                            <tr>
                                <td><?php echo $i + 1; ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["item_type_name"] ?></td>
                                <td>
                                    <?php echo $_SESSION[$enq_cache_id][$i]["item_name"] ?><br/>
                                </td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["actual_cost"] ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["selling_cost"] ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["item_qty"] ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["item_cost"] ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["Discount"]; ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["transport_cost"] ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["tax_class_id"] ?></td>
                                <td><?php echo $_SESSION[$enq_cache_id][$i]["tax_class_id"] ?></td>
                                <td>
                                    <!--
                                    <a class="btn text-warning btn-xs btn-flat" href="add_user_15-may.php?rec_id=2" title="Edit">
                                        <i class="icon-edit-sign icon-white"></i>
                                    </a>
                                    -->
                                    <a class="btn text-danger btn-xs btn-flat" href="add_user_15-may.php?rec_id=2"  title="Remove">
                                        <i class="icon-remove  icon-white"></i>

                                    </a>
                                </td>
                            </tr>
                        <?php } ?>  



                    </tbody>
                </table>
            <?php } else {
                ?>
                <div class="alert alert-danger">
                    No Item Found. <a class="alert-link" href="#">X</a>.
                </div>
                <?php
                exit();
            }
            ?>
        </div>
    </div>

</div>