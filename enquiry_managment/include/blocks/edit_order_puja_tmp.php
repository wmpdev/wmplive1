<div class="box" id="add_order_tmp_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-12">
            <div class="box">

                <div class="body collapse in" id="sortableTable">
                    <table class="table table-bordered sortableTable responsive-table">

                        <tbody>

    


                            <tr class="success">
                                <td colspan="6"><b>Puja Details</b></td>
                            </tr>
                            <tr>
                                <td>Puja Name</td>
                                <td>
                                    <input id="puja_name_1" class="form-control" type="text" value="<?php echo $puja_name_1; ?>" placeholder="Puja Name 1" name="puja_name_1">
                                    <input id="enq_id" class="form-control" type="hidden" value="<?php echo $edit_id; ?>" placeholder="Enquiry Number " name="enq_id">
                                    <input id="puja_id" class="form-control" type="hidden" value="<?php echo $edit_puja_id; ?>" placeholder="Edit Puja Id" name="puja_id">
                                </td>

                                <td>Puja Date</td>
                                <td>
                                    <!--
                                    <input id="puja_date_1" class="form-control" type="text" value="<?php echo $enq_date; ?>" placeholder="dd-mm-yyyy hh:mm AM" data-mask="99-99-9999 99:99 aa" name="puja_date_1"> -->
                                    <table style="width: 200px;" >
                                        <tr>
                                            <td>
                                                <input id="puja_date_1" class="form-control" type="text" value="<?php echo $puja_date_1; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="puja_date_1" width="10px" >
                                            </td>
                                            <td style="width: 95px;">
                                                <input id="puja_time_1" class="form-control" type="text" value="<?php echo $puja_time_1; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="puja_time_1">
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td>Samagri Arranged By</td>
                                <td>
                                    <select tabindex="2" onchange="" class="form-control" data-placeholder="Choose Samagri Arranged By" id="samagri_arranged_by_1" name="samagri_arranged_by_1">
                                        <option <?php if ("-" == $samagri_arranged_by_1) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pandit" == $samagri_arranged_by_1) echo "selected"; ?> value="Pandit">Pandit</option>
                                        <option <?php if ("WMP" == $samagri_arranged_by_1) echo "selected"; ?> value="WMP">WMP</option>
                                        <option <?php if ("Client" == $samagri_arranged_by_1) echo "selected"; ?> value="Client">Client</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td>Puja Address</td>
                                <td colspan="3">
                                    <textarea id="puja_address_1" class="form-control" placeholder="Puja Address" name="puja_address_1"><?php echo $puja_address_1; ?></textarea>
                                </td>



                                <td>Samagri Type</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose Samagri Type" id="samagri_type_1" name="samagri_type_1">
                                        <option <?php if ("-" == $samagri_type_1) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Gila+sukha" == $samagri_type_1) echo "selected"; ?> value="Gila+sukha">Gila+sukha</option>
                                        <option <?php if ("Sukha" == $samagri_type_1) echo "selected"; ?> value="Sukha">Sukha</option>
                                    </select>
                                </td>

                            </tr>
                            
                            
                            <tr class="success">
                                <td colspan="6"><b>Pandit Details</b></td>
                            </tr>
                            
                            <tr>
                                <td>Pandit Name 1</td>
                                <td>
                                    <input id="pandit_name_1" class="form-control" type="text" value="<?php echo $pandit_name_1; ?>" placeholder="Pandit Name 1" name="pandit_name_1">
                                </td>

                                <td>Pandit Contact</td>
                                <td>
                                    <input id="pandit_contact_1" class="form-control" type="text" value="<?php echo $pandit_contact_1; ?>" placeholder="Pandit Contact" name="pandit_contact_1">
                                </td>
                                <td>Pandit Reference</td>
                                <td>
                                    <input id="pandit_reference_1" class="form-control" type="text" value="<?php echo $pandit_reference_1; ?>" placeholder="Pandit Reference" name="pandit_reference_1">
                                </td>
                            </tr>


                            
                            
        



                            <tr class="success">
                                <td colspan="6"><b>Payment Details</b></td>
                            </tr>
                            <tr>
                                <td>Total Amount</td>
                                <td>
                                    <input id="total_amount" class="form-control" type="text" value="<?php echo $total_amount; ?>" placeholder="Total Amount" name="total_amount">
                                </td>


                                <td>Pandit Payment</td>
                                <td>
                                    <input id="pandit_payment" class="form-control" type="text" value="<?php echo $pandit_payment; ?>" placeholder="Pandit Payment" name="pandit_payment">
                                </td>
                                <td>Paid to Pandit </td>
                                <td>
                                    <input id="paid_to_pandit" class="form-control" type="text" value="<?php echo $paid_to_pandit; ?>" placeholder="Paid to Pandit " name="paid_to_pandit">
                                </td>
                            </tr>

                            <tr>

                                <td>Payment Mode<br/>Status</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Payment Mode Status" id="payment_mode" name="payment_mode">                    
                                        <option <?php if ("-" == $payment_mode) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("COD" == $payment_mode) echo "selected"; ?> value="COD">COD</option>
                                        <option <?php if ("Online" == $payment_mode) echo "selected"; ?> value="Online">Online</option>
                                    </select>
                                </td>
                                <td>Customer Payment<br/>Received Status</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Customer Payment Received Status" id="cust_payment_receive_status" name="cust_payment_receive_status">                    
                                        <option <?php if ("-" == $cust_payment_receive_status) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pending" == $cust_payment_receive_status) echo "selected"; ?> value="Pending">Pending</option>
                                        <option <?php if ("Paid" == $cust_payment_receive_status) echo "selected"; ?> value="Paid">Paid</option>

                                    </select>
                                </td>

                                <td>Pandit Payment<br/>Status</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Pandit Payment Status" id="pandit_payment_receive_status" name="pandit_payment_receive_status">                    
                                        <option <?php if ("-" == $pandit_payment_receive_status) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pending" == $pandit_payment_receive_status) echo "selected"; ?> value="Pending">Pending</option>
                                        <option <?php if ("Paid" == $pandit_payment_receive_status) echo "selected"; ?> value="Paid">Paid</option>

                                    </select>
                                </td>
                            <tr>
                                <td>Collected By</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose Collected By" id="collected_by" name="collected_by">                    
                                        <option <?php if ("-" == $collected_by) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Manual" == $collected_by) echo "selected"; ?> value="Manual">Manual</option> 
                                        <?php
                                        list($status, $id, $collected_by_arr) = getOrderTmp_CollectedBy();
                                        if (count($collected_by_arr) > 0) {


                                            for ($i = 0; $i < count($collected_by_arr); $i++) {
                                                $collected_by_value = $collected_by_arr[$i]["collect_by"];
                                                if(strlen($collected_by_value)>3){
                                                ?>
                                                <option <?php if ("$collected_by_value" == $collected_by) echo "selected"; ?> value="<?php echo $collected_by_value; ?>"><?php echo $collected_by_value; ?></option>
                                                <?php }}
                                        } ?>

                                        <option <?php if ("Suggestions" == $collected_by) echo "selected"; ?> value="Suggestions">Suggestions</option> 

                                    </select>
                                </td>
                                <td colspan="2">
                                    <input id="collected_by_txt" class="form-control" type="text" value="<?php echo $order_handled_by; ?>" placeholder="Enter Collected By" name="collected_by_txt">
                                </td>
                            </tr>

                            </tr>


                            <tr class="success">
                                <td colspan="6"><b>Monitors Details</b></td>
                            </tr>
                            <tr>

                                <td>Order Handled<br/>By</td>
                                <td colspan="2">
                                    <input id="order_handled_by" class="form-control" type="text" value="<?php echo $order_handled_by; ?>" placeholder="Order Handled By" name="order_handled_by">
                                </td>

                                <td>Enquiry Handled<br/>By</td>
                                <td colspan="2">
                                    <input id="enquiry_handled_by" class="form-control" type="text" value="<?php echo $enquiry_handled_by; ?>" placeholder="Enquiry Handled By" name="enquiry_handled_by">
                                </td>


                            </tr>


                            <tr class="success">
                                <td colspan="6"><b>Feedback Details</b></td>
                            </tr>

                            <tr>
                                <td>Feedback</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose a Country" id="feedback_type" name="feedback_type">                    
                                        <option <?php if ("-" == $feedback_type) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Yes" == $feedback_type) echo "selected"; ?> value="Yes">Yes</option>
                                        <option <?php if ("No" == $feedback_type) echo "selected"; ?> value="No">No</option>

                                    </select>
                                </td>
                                <td>Comments</td>
                                <td colspan="3">
                                    <textarea id="comments" class="form-control" placeholder="Comments" name="comments"><?php echo $comments; ?></textarea>
                                </td>
                            </tr>




                        </tbody>
                    </table>
                </div>
            </div>

        </div>



    </div>

</div>





<div class="form-actions" style="text-align:center">
    <input class="btn btn-primary btn-lg" type="submit" value="Save Changes" name="submit" >
</div>


