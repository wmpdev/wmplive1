<div class="box" id="order_tmp_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-12">
            <div class="box">

                <div class="body collapse in" id="sortableTable">
                    <table class="table table-bordered sortableTable responsive-table">

                        <tbody>

                            <tr class="success">
                                <td colspan="6"><b>Order And Enquiry Details</b></td>
                            </tr>
                            <tr>
                                <td>Order Number</td>
                                <td>

                                    <input id="order_no" class="form-control" type="hidden" value="<?php echo $order_no; ?>" placeholder="Order Number " name="order_no"> 
                                    <?php echo $order_no; ?>

                                    <input id="edit_id" class="form-control" type="hidden" value="<?php echo $edit_id; ?>" placeholder="Enquiry Number " name="edit_id">
                                </td>
                                <td>Order Date Time</td>
                                <td>
                                    <!--
                                    <input id="order_date" class="form-control" type="text" value="<?php echo $order_date; ?>" placeholder="dd-mm-yyyy hh:mm AM" data-mask="99-99-9999 99:99 aa" name="order_date">
                                    -->
                                    <table style="width: 200px;" >
                                        <tr>
                                            <td>
                                                <input id="order_date" class="form-control" type="text" value="<?php echo $order_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="order_date" width="10px" >
                                            </td>
                                            <td style="width: 95px;">
                                                <input id="order_time" class="form-control" type="text" value="<?php echo $order_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="order_time">
                                            </td>
                                        </tr>
                                    </table>


                                </td>
                                <td>Order Status</td>
                                <td>
                                    <select tabindex="2" class="form-control" data-placeholder="Order Status" id="order_status" name="order_status">                    
                                        <option selected="" value="0">-</option>
                                        <?php
                                        $order_status_arr = getStatus(2);
                                        //$Co_ordinator_arr = getMonitor("pandit_cordination");
                                        for ($i = 0; $i < count($order_status_arr); $i++) {

                                            //echo "$key->$value";
                                            $selected = "";
                                            $key = $order_status_arr[$i]["id"];
                                            $value = $order_status_arr[$i]["status_name"];
                                            if ($key == $order_status)
                                                $selected = "selected";
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                        <?php } ?>  
                                    </select>
                                </td>

                            </tr>

                            <tr>
                                <td>Enquiry Number</td>
                                <td>

                                    <input id="enq_no" class="form-control" type="hidden" value="<?php echo $enq_no; ?>" placeholder="Enquiry Number " name="enq_no">
                                    
                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_no_id; ?>"><?php echo $enq_no; ?></a>
                                </td>

                                <td>Enquiry Date</td>
                                <td>
                                    <!--
                                    <input id="enq_date" class="form-control" type="text" value="<?php echo $enq_date; ?>" placeholder="dd-mm-yyyy hh:mm AM" data-mask="99-99-9999 99:99 aa" name="enq_date">
                                    -->
                                    <table style="width: 200px;" >
                                        <tr>
                                            <td>
                                                <!-- <input id="enq_date" class="form-control" type="text" value="<?php echo $enq_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="enq_date" width="10px" > --> <?php echo $enq_date; ?>
                                            </td>
                                            <td style="width: 95px;">
                                                <!-- <input id="enq_time" class="form-control" type="text" value="<?php echo $enq_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="enq_time"> --> <?php echo $enq_time; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td>Service Request</td>

                                <td><?php echo $cust_service_request; ?>
                                    <!--
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose Service Request" id="cust_service_request" name="cust_service_request">                    
                                        <option <?php if ("-" == $cust_service_request) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pandit" == $cust_service_request) echo "selected"; ?> value="Pandit">Pandit</option>
                                        <option <?php if ("Pandit+Samagri" == $cust_service_request) echo "selected"; ?> value="Pandit+Samagri">Pandit+Samagri</option>
                                        <option <?php if ("Samagri" == $cust_service_request) echo "selected"; ?> value="Samagri">Samagri</option>
                                        <option <?php if ("Product" == $cust_service_request) echo "selected"; ?> value="Product">Product</option>
                                        <option <?php if ("Epuja" == $cust_service_request) echo "selected"; ?> value="Epuja">Epuja</option>

                                    </select>
                                    -->
                                </td>

                            </tr>





                            <tr class="success">
                                <td colspan="6"><b>Customer Details</b></td>
                            </tr>

                            <tr>
                                <td>Customer Name</td>
                                <td>
                                    <?php if (strlen($cust_name) > 0) {
                                        echo $cust_name;
                                    } else { ?>
                                        <input id="cust_name" class="form-control" type="hidden" value="<?php echo $cust_name; ?>" placeholder="Customer Name" name="cust_name"><?php } ?>
                                </td>

                                <td>Contact Number</td>
                                <td>
                                    <input id="cust_no" class="form-control" type="hidden" value="<?php echo $cust_no; ?>" placeholder="Contact Number" name="cust_no"><?php echo $cust_no; ?>
                                </td>
                                <td>Email Address</td>
                                <td>
                                    <input id="cust_email" class="form-control" type="hidden" value="<?php echo $cust_email; ?>" placeholder="Email Address" name="cust_email"><?php echo $cust_email; ?>
                                </td>
                            </tr>

                            <tr>


                                <td>City</td>
                                <td>
                                    <input id="cust_city" class="form-control" type="hidden" value="<?php echo $cust_city; ?>" placeholder="City" name="cust_city"><?php echo $cust_city; ?>
                                </td>
                                <td>Sub Area</td>
                                <td>
                                    <input id="cust_area" class="form-control" type="hidden" value="<?php echo $cust_area; ?>" placeholder="Sub Area" name="cust_area"><?php echo $cust_area; ?>
                                </td>

                                <!--
                                <td>Service Request</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose Service Request" id="cust_service_request" name="cust_service_request">                    
                                        <option <?php if ("-" == $cust_service_request) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pandit" == $cust_service_request) echo "selected"; ?> value="Pandit">Pandit</option>
                                        <option <?php if ("Pandit+Samagri" == $cust_service_request) echo "selected"; ?> value="Pandit+Samagri">Pandit+Samagri</option>
                                        <option <?php if ("Samagri" == $cust_service_request) echo "selected"; ?> value="Samagri">Samagri</option>
                                        <option <?php if ("Product" == $cust_service_request) echo "selected"; ?> value="Product">Product</option>
                                        <option <?php if ("Epuja" == $cust_service_request) echo "selected"; ?> value="Epuja">Epuja</option>

                                    </select>
                                </td>
                                -->

                            </tr>
                            <tr>
                                <td>Address</td>
                                <td colspan="5">
                                    <!-- <textarea id="cust_address"  class="form-control" placeholder="Address" name="cust_address"><?php echo $cust_address; ?></textarea> -->

                                    <input id="cust_address" class="form-control" type="hidden" value="<?php echo $cust_address; ?>" placeholder="Sub Area" name="cust_address">

<?php echo $cust_address; ?>
                                </td>
                            </tr>





                            <tr class="success">
                                <td colspan="6"><b>Payment Details</b></td>
                            </tr>
                            <tr>
                                <td>Total Amount</td>
                                <td>
                                    <input id="total_amount" class="form-control" type="text" value="<?php echo $total_amount; ?>" placeholder="Total Amount" name="total_amount">
                                </td>

                                <td>Customer Payment<br/>Received Status</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Customer Payment Received Status" id="cust_payment_receive_status" name="cust_payment_receive_status">                    
                                        <option <?php if ("-" == $cust_payment_receive_status) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pending" == $cust_payment_receive_status) echo "selected"; ?> value="Pending">Pending</option>
                                        <option <?php if ("Paid" == $cust_payment_receive_status) echo "selected"; ?> value="Paid">Paid</option>

                                    </select>
                                </td>
                                <td>Payment Mode<br/>Status</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Payment Mode Status" id="payment_mode" name="payment_mode">                    
                                        <option <?php if ("-" == $payment_mode) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Online" == $payment_mode) echo "selected"; ?> value="Online">Online</option>
                                        <option <?php if ("Pandit COD" == $payment_mode) echo "selected"; ?> value="Pandit COD">Pandit COD</option>
                                        <option <?php if ("Executive COD" == $payment_mode) echo "selected"; ?> value="Executive COD">Executive COD</option>
                                    </select>
                                </td>


                            </tr>

                            <tr>

                                <td>Pandit Cost</td>
                                <td>
                                    <input id="pandit_payment" class="form-control" type="text" value="<?php echo $pandit_payment; ?>" placeholder="Pandit Payment" name="pandit_payment">
                                </td>




                                <td>Pandit Payment<br/>Status</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Pandit Payment Status" id="pandit_payment_receive_status" name="pandit_payment_receive_status">                    
                                        <option <?php if ("-" == $pandit_payment_receive_status) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Pending" == $pandit_payment_receive_status) echo "selected"; ?> value="Pending">Pending</option>
                                        <option <?php if ("Paid" == $pandit_payment_receive_status) echo "selected"; ?> value="Paid">Paid</option>

                                    </select>
                                </td>
                                <td>Paid to Pandit </td>
                                <td>
                                    <input id="paid_to_pandit" class="form-control" type="text" value="<?php echo $paid_to_pandit; ?>" placeholder="Paid to Pandit " name="paid_to_pandit">
                                </td>
                            <tr>
                                <td>Collected By</td>
                                <td><?php //list($status, $id, $collected_by_arr) = getOrderTmp_CollectedBy(); print_r($collected_by_arr);        ?>
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose Collected By" id="collected_by" name="collected_by">                    
                                        <option <?php if ("-" == $collected_by) echo "selected"; ?> value="-">-</option>
                                        <!-- <option <?php if ("Manual" == $collected_by) echo "selected"; ?> value="Manual">Manual</option>  -->
                                        <?php
                                        list($status, $id, $collected_by_arr) = getOrderTmp_CollectedBy();

                                        if (count($collected_by_arr) > 0) {


                                            for ($i = 0; $i < count($collected_by_arr); $i++) {
                                                $collected_by_value = $collected_by_arr[$i]["collect_by"];
                                                if (strlen($collected_by_value) > 3) {
                                                    ?>
                                                    <option <?php if ($collected_by_value == $collected_by) echo "selected"; ?> value="<?php echo $collected_by_value; ?>"><?php echo $collected_by_value; ?></option>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>

                                        <option <?php if ("Add_New" == $collected_by) echo "selected"; ?> value="Add_New">Add New</option> 

                                    </select>
                                </td>
                                <td colspan="2">
                                    <input id="collected_by_txt" class="form-control" type="text" value="" placeholder="Enter Collected By" name="collected_by_txt">
                                </td>
                            </tr>

                            </tr>


                            <tr class="success">
                                <td colspan="6"><b>Monitors Details</b></td>
                            </tr>
                            <tr>

                                <td>Order Handled<br/>By</td>
                                <td colspan="2">
                                    <input id="order_handled_by" class="form-control" type="text" value="<?php echo $order_handled_by; ?>" placeholder="Order Handled By" name="order_handled_by">
                                </td>

                                <td>Enquiry Handled<br/>By</td>
                                <td colspan="2">
                                    <input id="enquiry_handled_by" class="form-control" type="text" value="<?php echo $enquiry_handled_by; ?>" placeholder="Enquiry Handled By" name="enquiry_handled_by">
                                </td>


                            </tr>


                            <tr class="success">
                                <td colspan="6"><b>Feedback Details</b></td>
                            </tr>

                            <tr>
                                <td>Feedback</td>
                                <td>
                                    <select tabindex="2"  class="form-control" data-placeholder="Choose a Country" id="feedback_type" name="feedback_type">                    
                                        <option <?php if ("-" == $feedback_type) echo "selected"; ?> value="-">-</option>
                                        <option <?php if ("Yes" == $feedback_type) echo "selected"; ?> value="Yes">Yes</option>
                                        <option <?php if ("No" == $feedback_type) echo "selected"; ?> value="No">No</option>

                                    </select>
                                </td>
                                <td>Comments</td>
                                <td colspan="3">
                                    <textarea id="comments" class="form-control" placeholder="Comments" name="comments"><?php echo $comments; ?></textarea>
                                </td>
                            </tr>




                        </tbody>
                    </table>
                </div>
            </div>

        </div>



    </div>

</div>





<div class="form-actions" style="text-align:center">
    <input class="btn btn-primary btn-lg" type="submit" value="Save Changes" name="submit" >
</div>


