<?php
include_once '../../config/auth.php';
//print_r($_POST);

if (strlen($_POST["enq_id"]) > 0) {
    $enq_id = $_POST["enq_id"];
}
//$enq_id
$puja_arr = getDataFromTable("tbl_pooja_master");
list($status, $id, $enq_puja_quotation_arr) = getEnqQuotation(0, $enq_id);

$pandit_arr = getDataFromTable("tbl_pandit");

//print_r($enq_puja_quotation_arr);
//$enq_no = "";            
?>
<div class="box" id="order_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>


<form name="order_details" id="order_details" action="" method="POST" enctype="multipart/form-data">
    <div class="row">                           

        <div class="col-lg-6">
            <div class="box">
                <input id="order_enq_id"  class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="order_enq_id">
                <input id="ACTION"  class="form-control" type="hidden" value="AddOrder" placeholder="Enquiry Number " name="ACTION">
                <div class="body collapse in" id="sortableTable">
                    <table class="table table-bordered sortableTable responsive-table">

                        <tbody>


                            <tr>
                                <td style="width: 145px;">Invoice No</td>
                                <td>
                                    <input id="invoice_no" class="form-control" type="text" value="<?php echo $invoice_no; ?>" placeholder="Invoice No" name="invoice_no" >                                                                                      

                                </td>

                            </tr>


                            <tr>
                                <td>Invoice Prepared By</td>
                                <td>
                                    <input id="invoice_prepared" class="form-control" type="text" value="<?php echo $Invoice_prepared_by; ?>" placeholder="Invoice Prepared By" name="invoice_prepared">                                    
                                </td>

                            </tr>

                            <tr>
                                <td>Invoice Mailed</td>
                                <td>
                                    <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="invoice_mailed" name="invoice_mailed">                    
                                        <option selected="" value="-">-</option>
                                        <option <?php if ("Yes" == $invoice_mailed) echo "selected"; ?> value="Yes">Yes</option>
                                        <option <?php if ("No" == $invoice_mailed) echo "selected"; ?> value="No">No</option>
                                    </select>
                                </td>

                            </tr>


                            <tr>
                                <td> Link Sent</td>
                                <td>
                                    <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="link_sent" name="link_sent">                    
                                        <option selected="" value="-">-</option>
                                        <option  <?php if ("Yes" == $link_sent) echo "selected"; ?> value="Yes">Yes</option>
                                        <option  <?php if ("No" == $link_sent) echo "selected"; ?> value="No">No</option>
                                    </select>                                 
                                </td>

                            </tr>
                            <tr>
                                <td> Link Url</td>
                                <td >
                                    <input id="link_url" class="form-control" type="text" value="<?php echo $link_url; ?>" placeholder="Specify Link Url" name="link_url">                                    
                                </td>
                            </tr>










                            <tr>
                                <td>Comment</td>
                                <td>
                                    <textarea id="order_comments" class="form-control" placeholder="Order Comment" name="order_comments"><?php echo $feedback; ?></textarea>
                                </td>
                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>

        </div>


        <div class="col-lg-6">
            <div class="box">
                <div class="body">
                    <div class="panel panel-primary">
                        <div class="panel-heading"> Order Details </div>
                        <div class="panel-body">

                            <table class="table table-bordered sortableTable responsive-table">

                                <tbody>




                                    <tr>
                                        <td>Order No</td>
                                        <td>
                                            <input id="order_no" class="form-control" type="text" value="<?php echo $enq_order_no; ?>" placeholder="Order No" name="order_no">                                    
                                        </td>

                                    </tr>

                                    <tr>
                                        <td>Order Status </td>
                                        <td>
                                            <select tabindex="2" class="form-control" data-placeholder="Order Status" id="order_status" name="order_status">                    
                                                <option selected="" value="0">-</option>
                                                <?php
                                                
                                                $order_status_arr = getStatus(2);
                                                //$Co_ordinator_arr = getMonitor("pandit_cordination");
                                                for ($i = 0; $i < count($order_status_arr); $i++) {

                                                    //echo "$key->$value";
                                                    $selected = "";
                                                    $key = $order_status_arr[$i]["id"];
                                                    $value = $order_status_arr[$i]["status_name"];
                                                    if ($key == $order_status_id)
                                                    $selected = "selected";
                                                        
                                                        ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                <?php } ?>  
                                            </select>
                                        </td>
                                    </tr>




                                    <tr>
                                        <td>Payment Status </td>
                                        <td colspan="2">
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="payment_status" name="payment_status">                    
                                                <option selected="" value="0">-</option>
                                                <option  <?php if ("Pending" == $payment_status) echo "selected"; ?> value="Pending">Pending</option>
                                                <option  <?php if ("Paid" == $payment_status) echo "selected"; ?> value="Paid">Paid</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Approved By </td>
                                        <td>
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="approved_by" name="approved_by">                    
                                                <option selected="" value="0"> - </option>
                                                <?php
                                                $quote_approval_arr = getMonitor("quote_approval");
                                                for ($i = 0; $i < count($quote_approval_arr); $i++) {

                                                    //echo "$key->$value";
                                                    $selected = "";
                                                    $key = $quote_approval_arr[$i]["id"];
                                                    $value = $quote_approval_arr[$i]["monitor_name"];
                                                    if ($key == $approved_by_id)
                                                    $selected = "selected";
                                                        
                                                        ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"> <?php echo $value; ?> </option> 
                                                <?php } ?>  
                                            </select>
                                        </td>
                                    </tr>

                                    <!--
                                    <tr>
                                        <td>Client Approved</td>
                                        <td colspan="2">
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">No</option>
                                                <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>
                                    -->


                                </tbody>
                            </table>

                        </div>
                        <div class="panel-footer"> 
                            <span class="input-group-btn">
                                <input class="btn btn-success " type="submit" value="Save" name="Save" >
                            </span>
                            <?php if(($enq_order_no > 0) && $enq_order_id == 0){ ?>
                            <span class="input-group-btn" align="right">
                                <!-- <input class="btn btn-success " type="submit" value="Convert To Order" name="Convert" > -->
                                <a class="btn btn-warning btn-sm" onclick="convertOrder(<?php echo $enq_id; ?>)" href="javascript:void(0)">Convert To Order</a>
                            </span>
                            <?php }elseif(($enq_order_no > 0) && $enq_order_id > 0){ ?>
                            <span class="input-group-btn" align="right">
                                <!-- <input class="btn btn-success " type="submit" value="Convert To Order" name="Convert" > -->
                                <a class="btn btn-success btn-sm" target="_blank" href="add_order_tmp.php?edit_id=<?php echo $enq_order_id; ?>">Check Order</a>
                            </span>
                            <?php } ?>
                        </div>
                    </div>
                </div>


            </div>





        </div>  
    </div>
</form>










