<?php
include_once '../../config/auth.php';
//print_r($_POST);

if (strlen($_POST["enq_id"]) > 0) {
    $enq_id = $_POST["enq_id"];
}
//$enq_id
//$puja_arr = getDataFromTable("tbl_pooja_master");  
//$puja_arr = getDataFromTable("tbl_pooja_master");
list($status, $id, $puja_arr) = getPujaData();
list($status, $id, $enq_puja_arr) = getEnqPujaData(0, $enq_id);
//print_r($enq_puja_arr);
//$enq_no = "";            
?>
<div class="box" id="puja_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>




<div class="row">


    <div class="col-lg-12">
        <div class="box">

            <div class="body collapse in" id="sortableTable">
                <table class="table table-bordered sortableTable responsive-table">

                    <tbody>

                    <input id="puja_edit_id" class="form-control" type="hidden" value="" placeholder="Enquiry Number " name="puja_edit_id">
                    <input id="puja_enq_id"  class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="puja_enq_id">

                    <tr>
                        <td>
                            <select tabindex="2" class="form-control" data-placeholder="Select Puja" id="enq_puja" name="enq_puja">                    
                                <option selected="" value="0">-</option>
                                <?php
                                for ($i = 0; $i < count($puja_arr); $i++) {

                                    //echo "$key->$value";
                                    $selected = "";
                                    $key = $puja_arr[$i]["id"];
                                    $value = $puja_arr[$i]["pooja_name"];
                                    if ($key == $puja_id)
                                    //$selected = "selected";
                                        
                                        ?>
                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                <?php } ?>    
                            </select>
                        </td>
                        <td>
                            <table style="width: 200px;">
                                <tbody><tr>
                                        <td>
                                            <input width="10px" type="text" name="puja_date" data-mask="99-99-9999" placeholder="dd-mm-yyyy" value="" class="form-control" id="puja_date" tabindex="3">
                                        </td>
                                        <td style="width: 95px;">
                                            <input type="text" name="puja_time" data-mask="99:99 aa" placeholder="hh:mm AM" value="" class="form-control" id="puja_time" tabindex="4">
                                        </td>
                                    </tr>
                                </tbody></table>
                        </td>
                        <td>
                            <select tabindex="5"  class="form-control" data-placeholder="Choose Samagri Type" id="samagri_type" name="samagri_type">
                                <option <?php if ("-" == $samagri_type_1) echo "selected"; ?> value="-">-</option>                                
                                <!--                                
                                <option  <?php if ("All - WMP" == $samagri_type_1) echo "selected"; ?> value="All - WMP">All - WMP</option>
                                <option  <?php if ("All - WMP" == $samagri_type_1) echo "selected"; ?> value="Both - WMP">Both - WMP</option>
                                <option  <?php if ("Sukha - WMP" == $samagri_type_1) echo "selected"; ?> value="Sukha - WMP">Sukha - WMP</option>
                                <option  <?php if ("All - Pandit" == $samagri_type_1) echo "selected"; ?> value="All - Pandit">All - Pandit</option> 
                                <option  <?php if ("Both - Pandit" == $samagri_type_1) echo "selected"; ?> value="Both - Pandit">Both - Pandit</option>
                                <option  <?php if ("Sukha - Pandit" == $samagri_type_1) echo "selected"; ?> value="Sukha - Pandit">Sukha - Pandit</option>
                                -->
                                
                                <!--   New Updated Modification Solution Not required Now  
                                <?php if ("All - WMP" == $samagri_type_1) { ?> <option value="All - WMP" selected >All - WMP</option> <?php } ?>
                                <?php if ("All - WMP" == $samagri_type_1) {?> <option  value="Both - WMP">Both - WMP</option> <?php } ?>
                                <?php if ("Sukha - WMP" == $samagri_type_1) { ?> <option  value="Sukha - WMP">Sukha - WMP</option> <?php } ?>
                                <?php if ("All - Pandit" == $samagri_type_1) { ?> <option  value="All - Pandit">All - Pandit</option> <?php } ?> 
                                <?php if ("Both - Pandit" == $samagri_type_1) { ?> <option  value="Both - Pandit">Both - Pandit</option> <?php } ?>
                                <?php if ("Sukha - Pandit" == $samagri_type_1) { ?> <option  value="Sukha - Pandit">Sukha - Pandit</option> <?php } ?>
                                -->

                                <option  <?php if ("Client" == $samagri_type_1) echo "selected"; ?> value="Client">Client</option>
                                <option  <?php if ("WMP" == $samagri_type_1) echo "selected"; ?> value="WMP">WMP</option>
                                <option  <?php if ("Pandit" == $samagri_type_1) echo "Pandit"; ?> value="Pandit">Pandit</option>

                            </select>
                        </td>
                        <td>
                            <textarea id="puja_address" class="form-control" placeholder="Puja Address" name="puja_address"  tabindex="6"><?php echo $address_1; ?></textarea>
                        </td>
                        <td>
                            <button id="btn-chat" class="btn btn-success btn-sm" > Add</button>
                            <!--
                            <button class="btn">
                                <i class="icon-eye-open"></i>Add                                    
                            </button>
                            -->
                        </td>
                    </tr>

                    <tr>
                        <th width="270px">Puja Name</th>
                        <th width="110px">Puja Date Time</th>
                        <th width="160px">Samagri Type</th>
                        <th>Address</th>
                        <th width="85px">Action</th>
                    </tr>

                    <?php
                    if ($enq_puja_arr[0]["id"] > 0) {
                        for ($enq_puja_index = 0; $enq_puja_index < count($enq_puja_arr); $enq_puja_index++) {

                            $enq_puja_id = trim($enq_puja_arr[$enq_puja_index]["id"]);
                            $puja_id = trim($enq_puja_arr[$enq_puja_index]["puja_name"]);
                            $puja_date = trim($enq_puja_arr[$enq_puja_index]["puja_date"]);
                            list($puja_date, $puja_time) = validateDbDateTime($puja_date);
                            //$puja_date = $puja_date . " " . $puja_time;

                            $samagri_type = trim($enq_puja_arr[$enq_puja_index]["samagri_type"]);
                            $puja_address = trim($enq_puja_arr[$enq_puja_index]["puja_address"]);
                            ?>

                            <tr>
                                <td>


                                    <?php
                                    for ($i = 0; $i < count($puja_arr); $i++) {

                                        //echo "$key->$value";
                                        $selected = "";
                                        $key = $puja_arr[$i]["id"];
                                        $value = $puja_arr[$i]["pooja_name"];
                                        if ($key == $puja_id) {
                                            //$selected = "selected";
                                            echo $value;
                                        }
                                    }
                                    ?>    

                                </td>
                                <td>
                                    <table style="width: 200px;">
                                        <tbody><tr>
                                                <td>
                                                    <?php echo $puja_date; ?>
                                                </td>
                                                <td style="width: 95px;">
                                                    <?php echo $puja_time; ?>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </td>
                                <td>
                                    <?php echo $samagri_type; ?>
                                </td>
                                <td>
                                    <?php echo $puja_address; ?>  
                                </td>
                                <td>
                                    <a href="javascript:void(0)" onclick="editEnqPuja(<?php echo $enq_puja_id . "," . $puja_id . ",'" . $puja_date . "','" . $puja_time . "','" . $samagri_type . "','" . $puja_address . "'" ?>)"  class="btn text-warning btn-xs btn-flat"><i class="icon-pencil icon-white"></i> Edit</a>
                                    <!--
                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>&puja_id=<?php echo $puja_id; ?>&action=delete&tab=puja-details"  class="btn text-warning btn-xs btn-flat"><i class="icon-pencil icon-white"></i> Delete</a> -->

        <!-- <a href="javascript:void(0)" onclick="addEnquiry('<?php echo $row['id']; ?>')"  class="btn text-danger btn-xs btn-flat"><i class="icon-trash icon-white"></i> Delete</a> -->

                                    <!--
                                    <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_user();"> X</button>
                                    
                                    <button class="btn">
                                        <i class="icon-eye-open"></i>Add                                    
                                    </button>
                                    -->
                                </td>
                            </tr>

                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="5">No Pujas Present</td>
                        </tr>
                        <?php
                    }
                    ?>







                    </tbody>
                </table>
            </div>
        </div>

    </div>







</div>




<script>
    function editEnqPuja(enq_puja_id, puja_id, puja_date, puja_time, samagri_type, puja_address) {

        //alert("i am in add_followup");
        console.log("enq_puja_id = [" + enq_puja_id + "] , puja_id = [" + puja_id + "] , puja_date = [" + puja_date + "] , puja_time = [" + puja_time + "] , samagri_type = [" + samagri_type + "] , puja_address = [" + puja_address);

        document.getElementById("puja_edit_id").value = enq_puja_id;

        var element = document.getElementById('enq_puja');
        element.value = puja_id;

        //document.getElementById("puja_edit_id").value = enq_puja_id;

        document.getElementById("puja_date").value = puja_date;
        document.getElementById("puja_time").value = puja_time;

        var element = document.getElementById('samagri_type');
        element.value = samagri_type;

        document.getElementById("puja_address").value = puja_address;


    }
</script>



