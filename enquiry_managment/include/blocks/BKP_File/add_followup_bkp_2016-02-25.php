<?php
$today_date = date('d-m-Y', time());
$today_time = date('h:i A', time());

$monitor_arr = getMonitor("monitor_auth");
?>

<div class="panel panel-primary">

    <div class="panel-heading">
        <i class="icon-comments"></i>
         Enter New Followup 
        <div class="btn-group pull-right">
            <button data-toggle="dropdown" type="button"> 
                <i class="icon-chevron-down"></i>
            </button>
            <ul class="dropdown-menu slidedown">
                <?php if(strlen($phone_1)>0){?>
                <li>
                    <a href="send_sms.php?rec_type=Enquiry&enq_id=<?php echo $enq_id; ?>&puja_id=0&cust_id=<?php echo $cust_id; ?>&tag=no_contact&Tab=Customer-Tab">
                        <i class="icon-phone"></i> No Contact SMS 
                    </a>
                </li>
                <?php } ?>
                <?php if(strlen($email_1)>0){?>
                <li>
                    <a href="send_email.php?rec_type=Enquiry&enq_id=<?php echo $enq_id; ?>&puja_id=0&cust_id=<?php echo $cust_id; ?>&tag=no_contact&Tab=Customer-Tab">
                        <i class="icon-envelope"></i> No Contact Email
                    </a>
                </li>
                <?php } ?>
                <?php /* if(strlen($phone_1)>0){?>
                <li class="divider"></li>
                <li>
                    <a href="send_sms.php?rec_type=Enquiry&enq_id=<?php echo $enq_id; ?>&puja_id=0&tag=custom"> 
                        <i class="icon-signout"></i> Custom SMS
                    </a>
                </li>
                
                <?php } */ ?>
                <!--
                <li>
                    <a href="#">
                        <i class="icon-refresh"></i> Refresh
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class=" icon-comment"></i> Available
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-time"></i> Busy
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="icon-tint"></i> Away
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <i class="icon-signout"></i> Sign Out
                    </a>
                </li>
                -->
            </ul>
        </div>
    </div>




    <div id="followup_status">

    </div>
    <form name="add_followup_form" id="add_followup_form" action="" method="POST" enctype="multipart/form-data">


        <div class="panel-body">
            <table class="table table-bordered sortableTable responsive-table">

                <tbody>



                    <tr>


                        <td>
                            <!-- <label class="control-label">Followup Date</label> -->
                            <input type="hidden" value="<?php echo $enq_id; ?>" name="followup_enq_id" id="followup_enq_id"> 

                            <input type="text" value="<?php echo $today_date; ?>" placeholder="Followup Date" class="form-control" data-mask="99-99-9999" name="followup_date" id="followup_date" title="Followup Date"> 
                            <!-- <span class="input-group-addon">dd/mm/yyyy</span> -->


                        </td>
                        <td>
                            <!-- <label class="control-label">Followup Time</label> -->

                            <input type="text" value="<?php echo $today_time; ?>" placeholder="Followup Time" class="form-control" data-mask="99:99 aa" name="followup_time" id="followup_time" title="Followup Time">
                            <!-- <span class="input-group-addon">hh:mm AM</span> -->

                        </td>
                    </tr>


                    <tr>

                        <td colspan="2">
                            <!-- <label class="control-label">Comments</label> -->
                            <textarea placeholder="Enter Comments" name="followup_comment" id="followup_comment" class="form-control" rows="5"></textarea>
                        </td>

                    </tr>

                    <tr>

                        <td>
                            <!-- <label class="control-label">Next Date And Time</label> -->
                            <input type="text" value="" placeholder="Next Followup Date" class="form-control" data-mask="99-99-9999" name="next_date" id="next_date" title="Next Followup Date">                                    
                        </td>
                        <td>
                            <input type="text" value="" placeholder="Next Followup Time" class="form-control" data-mask="99:99 aa" name="next_time" id="next_time" title="Next Followup Time">            
                        </td>
                    </tr>

                    <tr>

                        <td>
                            <!-- <label class="control-label">Select Status</label> -->
                            <select class="form-control" tabindex="2" name="enq_status" id="enq_status" data-placeholder="Select Status" title="Select Status">                    
                                <option selected="" value="0">Select Status</option>
                                <?php
                                for ($i = 0; $i < count($status_arr); $i++) {

                                    //echo "$key->$value";
                                    $selected = "";
                                    $key = $status_arr[$i]["id"];
                                    $value = $status_arr[$i]["status_name"];
                                    if ($key == $enq_status)
                                        $selected = "selected";
                                    ?>
                                    <option <?php //echo $selected;            ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                <?php } ?>  
                            </select> 
                        </td>
                        <td>
                            <!-- <label class="control-label">Monitor By</label> -->
                            <select class="form-control" tabindex="2" name="monitor_by" id="monitor_by" data-placeholder="Monitor By" title="Select Monitor By">            
                                <option selected="" value="0">Monitor By</option>
                                <?php
                                for ($i = 0; $i < count($monitor_arr); $i++) {

                                    //echo "$key->$value";
                                    $selected = "";
                                    $key = $monitor_arr[$i]["id"];
                                    $value = $monitor_arr[$i]["monitor_name"];
                                    if ($key == $enq_status)
                                        $selected = "selected";
                                    ?>
                                    <option <?php //echo $selected;            ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                <?php } ?>  
                            </select>                        
                        </td>
                    </tr>






                </tbody>
            </table>
        </div>
        <div class="panel-footer" align="right"> 
            <span class="input-group-btn">
                <!-- <input class="btn btn-success " type="submit" value="Save" name="Save" > -->
                <button id="btn-chat" class="btn btn-success btn-sm" > Add Followup</button>
            </span>
        </div>









        <?php /* ?>

          <div class="panel-body">

          <div class="form-group">


          <div class="col-lg-7"><label class="control-label">Followup date and Time</label></div>

          <div class="col-lg-7">
          <input type="hidden" value="<?php echo $enq_id; ?>" name="followup_enq_id" id="followup_enq_id">
          <div class="input-group">
          <input type="text" value="<?php echo $today_date; ?>" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="followup_date" id="followup_date">
          <!-- <span class="input-group-addon">dd/mm/yyyy</span> -->
          </div>



          <!--
          <div class="input-group input-append date" id="dp3" data-date="19-08-2015"
          data-date-format="dd-mm-yyyy">
          <input class="form-control" type="text" id="pooja_date" name="pooja_date" value="Next Date" readonly="" />
          <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
          </div>
          -->

          </div>

          <div class="col-lg-5">
          <div class="input-group">
          <input type="text" value="<?php echo $today_time; ?>" placeholder="hh:mm AM" class="form-control" data-mask="99:99 aa" name="followup_time" id="followup_time">
          <!-- <span class="input-group-addon">hh:mm AM</span> -->
          </div>
          <!--
          <div class="input-group bootstrap-timepicker">
          <input class="form-control timepicker-default" id="pooja_time" name="pooja_time"  value="Next Time" />
          <span class="input-group-addon add-on"><i class="icon-time"></i></span>
          </div>
          -->
          </div>

          </div>

          <label class="control-label">&nbsp;</label>
          <div class="form-group">
          <div class="col-lg-12">
          <textarea placeholder="Enter Comments" name="followup_comment" id="followup_comment" class="form-control"></textarea>
          </div>
          </div>


          <div class="col-lg-7"><label class="control-label">Next Date And Time</label></div>
          <div class="form-group">

          <div class="col-lg-7">

          <div class="input-group">
          <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="next_date" id="next_date">
          <!-- <span class="input-group-addon">dd/mm/yyyy</span> -->
          </div>
          </div>

          <div class="col-lg-5">
          <div class="input-group">
          <input type="text" value="" placeholder="hh:mm AM" class="form-control" data-mask="99:99 aa" name="next_time" id="next_time">
          <!-- <span class="input-group-addon">hh:mm AM</span> -->
          </div>
          </div>


          <!--
          <div class="col-lg-6">
          <div data-date-format="dd-mm-yyyy" data-date="19-08-2015" id="dp3" class="input-group input-append date">
          <input type="text" readonly="" value="" name="next_date" id="next_date" class="form-control">
          <span class="input-group-addon add-on"><i class="icon-calendar"></i></span>
          </div>
          </div>

          <div class="col-lg-6">
          <div class="input-group bootstrap-timepicker">
          <div class="bootstrap-timepicker-widget dropdown-menu">
          <table>
          <tbody>
          <tr>
          <td><a data-action="incrementHour" href="#"><i class="icon-chevron-up"></i></a></td>
          <td class="separator">&nbsp;</td>
          <td><a data-action="incrementMinute" href="#"><i class="icon-chevron-up"></i></a></td>
          <td class="separator">&nbsp;</td>
          <td class="meridian-column">
          <a data-action="toggleMeridian" href="#"><i class="icon-chevron-up"></i></a>
          </td>
          </tr>
          <tr>
          <td><input type="text" maxlength="2" class="bootstrap-timepicker-hour"></td>
          <td class="separator">:</td>
          <td><input type="text" maxlength="2" class="bootstrap-timepicker-minute"></td>
          <td class="separator">&nbsp;</td>
          <td><input type="text" maxlength="2" class="bootstrap-timepicker-meridian"></td>
          </tr>
          <tr>
          <td>
          <a data-action="decrementHour" href="#"><i class="icon-chevron-down"></i></a>
          </td>
          <td class="separator"></td>
          <td><a data-action="decrementMinute" href="#"><i class="icon-chevron-down"></i></a></td>
          <td class="separator">&nbsp;</td>
          <td><a data-action="toggleMeridian" href="#"><i class="icon-chevron-down"></i></a></td>
          </tr>
          </tbody>
          </table>
          </div>
          <input type="text" value="" name="next_time" id="next_time" class="form-control timepicker-default">
          <span class="input-group-addon add-on"><i class="icon-time"></i></span>
          </div>
          </div>
          -->

          </div>



          <div class="form-group">
          <div class="col-lg-6">

          <label class="control-label">Select Status</label>
          <select class="form-control" tabindex="2" name="enq_status" id="enq_status" data-placeholder="Select Status">
          <option selected="" value="0">-</option>
          <?php
          for ($i = 0; $i < count($status_arr); $i++) {

          //echo "$key->$value";
          $selected = "";
          $key = $status_arr[$i]["id"];
          $value = $status_arr[$i]["status_name"];
          if ($key == $enq_status)
          $selected = "selected";
          ?>
          <option <?php //echo $selected;           ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
          <?php } ?>
          </select>

          <!--
          <select class="form-control" tabindex="2" name="enq_status" id="enq_status" data-placeholder="Select Status">
          <option value="0">Select Status</option>
          <option value="1">Waiting</option>
          <option value="3">Followup</option>
          <option value="4">Close</option>
          <option value="8">No Response</option>
          <option value="2">Complete</option>
          <option value="7">Convert to Order</option>
          </select>
          -->
          </div>
          <div class="col-lg-6">

          <label class="control-label">Monitor By</label>
          <select class="form-control" tabindex="2" name="monitor_by" id="monitor_by" data-placeholder="Monitor By">
          <option selected="" value="0">-</option>
          <?php
          for ($i = 0; $i < count($monitor_arr); $i++) {

          //echo "$key->$value";
          $selected = "";
          $key = $monitor_arr[$i]["id"];
          $value = $monitor_arr[$i]["monitor_name"];
          if ($key == $enq_status)
          $selected = "selected";
          ?>
          <option <?php //echo $selected;           ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
          <?php } ?>
          </select>

          <!--
          <select class="form-control" tabindex="2" name="monitor_by" id="monitor_by" data-placeholder="Monitor By">
          <option value="0">Monitor By</option>
          <option value="1">Monitor 1</option>
          <option value="2">Monitor 2</option>
          </select>
          -->
          </div>
          </div>

          </div>

          <div class="panel-footer">
          <span class="input-group-btn">
          <button  id="btn-chat" class="btn btn-success btn-sm"> Save </button>
          </span>
          </div>
          <?php */ ?>


    </form>
</div>



<?php
/*


  <div class="panel-body">
  <table class="table table-bordered sortableTable responsive-table">

  <tbody>



  <tr>


  <td>
  <label class="control-label">Followup date and Time</label>
  <input type="hidden" value="<?php echo $enq_id; ?>" name="followup_enq_id" id="followup_enq_id">
  <div class="input-group">
  <input type="text" value="<?php echo $today_date; ?>" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="followup_date" id="followup_date">
  <!-- <span class="input-group-addon">dd/mm/yyyy</span> -->
  </div>

  </td>
  <td>
  <div class="input-group">
  <input type="text" value="<?php echo $today_time; ?>" placeholder="hh:mm AM" class="form-control" data-mask="99:99 aa" name="followup_time" id="followup_time">
  <!-- <span class="input-group-addon">hh:mm AM</span> -->
  </div>
  </td>
  </tr>


  <tr>

  <td colspan="2">
  <label class="control-label">Comments</label>
  <textarea placeholder="Enter Comments" name="followup_comment" id="followup_comment" class="form-control"></textarea>
  </td>

  </tr>

  <tr>

  <td>
  <label class="control-label">Next Date And Time</label>
  <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="next_date" id="next_date">
  </td>
  <td>
  <input type="text" value="" placeholder="hh:mm AM" class="form-control" data-mask="99:99 aa" name="next_time" id="next_time">
  </td>
  </tr>

  <tr>

  <td>
  <label class="control-label">Select Status</label>
  <select class="form-control" tabindex="2" name="enq_status" id="enq_status" data-placeholder="Select Status">
  <option selected="" value="0">-</option>
  <?php
  for ($i = 0; $i < count($status_arr); $i++) {

  //echo "$key->$value";
  $selected = "";
  $key = $status_arr[$i]["id"];
  $value = $status_arr[$i]["status_name"];
  if ($key == $enq_status)
  $selected = "selected";
  ?>
  <option <?php //echo $selected;           ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
  <?php } ?>
  </select>
  </td>
  <td>
  <label class="control-label">Monitor By</label>
  <select class="form-control" tabindex="2" name="monitor_by" id="monitor_by" data-placeholder="Monitor By">
  <option selected="" value="0">-</option>
  <?php
  for ($i = 0; $i < count($monitor_arr); $i++) {

  //echo "$key->$value";
  $selected = "";
  $key = $monitor_arr[$i]["id"];
  $value = $monitor_arr[$i]["monitor_name"];
  if ($key == $enq_status)
  $selected = "selected";
  ?>
  <option <?php //echo $selected;           ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
  <?php } ?>
  </select>
  </td>
  </tr>






  </tbody>
  </table>
  </div>
  <div class="panel-footer" align="right">
  <span class="input-group-btn">
  <!-- <input class="btn btn-success " type="submit" value="Save" name="Save" > -->
  <button id="btn-chat" class="btn btn-success btn-sm" > Save Quotation</button>
  </span>
  </div>


 */
?>
