<div class="box" id="enq_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-6">
            <div class="box">

                <div class="body collapse in" id="sortableTable">
                    <table class="table table-bordered sortableTable responsive-table">

                        <tbody>


                            <tr>
                                <td>Enquiry Number</td>
                                <td>
                                    <input id="enq_no" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Enquiry Number " name="enq_no">
                                    <input id="cust_id" class="form-control" type="hidden" value="<?php echo $cust_id; ?>" placeholder="Enquiry Number " name="cust_id">
                                    <input id="edit_id" class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="edit_id">
                                </td>
                            </tr>

                            <tr>
                                <td>Enquiry Date</td>
                                <td>
                                    <input id="enq_date" class="form-control" type="text" value="<?php echo $enq_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="enq_date">
                                </td>
                            </tr>
                            <!--
                            <tr>
                                <td>Source</td>
                                <td>
                                    <select tabindex="2" class="form-control" data-placeholder="Choose Source" id="enq_source" name="enq_source">  
                                        <option selected="" value="0">-</option>
                            <?php
                            for ($i = 0; $i < count($source_arr); $i++) {

                                //echo "$key->$value";
                                $selected = "";
                                $key = $source_arr[$i]["id"];
                                $value = $source_arr[$i]["source_name"];
                                if ($key == $enq_source)
                                    $selected = "selected";
                                ?>
                                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                            <?php } ?>

                                    </select>
                                </td>
                            </tr>
                            -->

                            <tr>
                                <td>Medium</td>
                                <td>
                                    <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="enq_medium" name="enq_medium">                    
                                        <option selected="" value="0">-</option>
                                        <?php
                                        for ($i = 0; $i < count($medium_arr); $i++) {

                                            //echo "$key->$value";
                                            $selected = "";
                                            $key = $medium_arr[$i]["id"];
                                            $value = $medium_arr[$i]["medium_name"];
                                            if ($key == $enq_medium)
                                                $selected = "selected";
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                        <?php } ?>  
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <td>Muhurat advice</td>
                                <td>
                                    <textarea id="enq_muhurat" class="form-control" placeholder="Preferred Panchang, tithi, etc." name="enq_muhurat"><?php echo $enq_muhurat; ?></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td>Comment</td>
                                <td>
                                    <textarea id="enq_comment" class="form-control" placeholder="Comment" name="enq_comment"><?php echo $enq_comment; ?></textarea>
                                </td>
                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>

        </div>


        <div class="col-lg-6">
            <div class="box">
                <div class="body">
                    <div class="panel panel-primary">
                        <div class="panel-heading"> Puja Details </div>
                        <div class="panel-body">

                            <table class="table table-bordered sortableTable responsive-table">

                                <tbody>

                                    <?php
                                    for ($puja_count = 1; $puja_count < 5; $puja_count++) {

                                        $puja_id = $puja_db[$puja_count]["puja_id"];
                                        $puja_date = $puja_db[$puja_count]["puja_date"];
                                        $puja_time = $puja_db[$puja_count]["puja_time"];
                                        ?>
                                        <tr>
                                            <td width="210px">
                                                <select tabindex="2" class="form-control" data-placeholder="Select Puja <?php echo $puja_count; ?>" id="enq_puja_<?php echo $puja_count; ?>" name="enq_puja_<?php echo $puja_count; ?>">                    
                                                    <option selected="" value="0">-</option>
                                                    <?php
                                                    for ($i = 0; $i < count($puja_arr); $i++) {

                                                        //echo "$key->$value";
                                                        $selected = "";
                                                        $key = $puja_arr[$i]["id"];
                                                        $value = $puja_arr[$i]["pooja_name"];
                                                        if ($key == $puja_id)
                                                            $selected = "selected";
                                                        ?>
                                                        <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                    <?php } ?>    
                                                </select>
                                            </td>
                                            <td>
                                                <input id="puja_date_<?php echo $puja_count; ?>" name="puja_date_<?php echo $puja_count; ?>" class="form-control" type="text" value="<?php echo $puja_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999">
                                            </td>
                                            <td width="110px">
                                                <input id="puja_time_<?php echo $puja_count; ?>" name="puja_time_<?php echo $puja_count; ?>" class="form-control" type="text" value="<?php echo $puja_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" >
                                            </td>
                                        </tr>

                                    <?php } ?>

                                    <tr>
                                        <td>Type of Samagri</td>
                                        <td colspan="2">
                                            <select tabindex="2" onchange="refresh_state();" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">No</option>
                                                <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Kit</option>
                                                <option <?php if (2 == $samagri_type) echo "selected"; ?> value="2">Extra</option>
                                                <option <?php if (3 == $samagri_type) echo "selected"; ?> value="3">Both</option>

                                            </select>
                                        </td>
                                    </tr>


                                    <!--                                    
                                                                        
                                                                        <tr>
                                                                            <td>
                                                                                <select tabindex="2"  class="form-control" data-placeholder="Select Puja 1" id="enq_puja_1" name="enq_puja_1">                    
                                                                                    <option selected="" value="0">-</option>
                                    <?php
                                    for ($i = 0; $i < count($puja_arr); $i++) {

                                        //echo "$key->$value";
                                        $selected = "";
                                        $key = $puja_arr[$i]["id"];
                                        $value = $puja_arr[$i]["pooja_name"];
                                        if ($key == $pooja_id_1)
                                            $selected = "selected";
                                        ?>
                                                                                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                    <?php } ?>    
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input id="puja_date" class="form-control" type="text" value="<?php echo $puja_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="puja_date">
                                                                            </td>
                                                                            <td>
                                                                                <input id="puja_time" class="form-control" type="text" value="<?php echo $puja_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="puja_time">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <select tabindex="2"  class="form-control" data-placeholder="Select Puja 1" id="enq_puja_1" name="enq_puja_1">                    
                                                                                    <option selected="" value="0">-</option>
                                    <?php
                                    for ($i = 0; $i < count($puja_arr); $i++) {

                                        //echo "$key->$value";
                                        $selected = "";
                                        $key = $puja_arr[$i]["id"];
                                        $value = $puja_arr[$i]["pooja_name"];
                                        if ($key == $pooja_id_1)
                                            $selected = "selected";
                                        ?>
                                                                                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                    <?php } ?>    
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input id="puja_date" class="form-control" type="text" value="<?php echo $puja_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="puja_date">
                                                                            </td>
                                                                            <td>
                                                                                <input id="puja_time" class="form-control" type="text" value="<?php echo $puja_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="puja_time">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <select tabindex="2"  class="form-control" data-placeholder="Select Puja 1" id="enq_puja_1" name="enq_puja_1">                    
                                                                                    <option selected="" value="0">-</option>
                                    <?php
                                    for ($i = 0; $i < count($puja_arr); $i++) {

                                        //echo "$key->$value";
                                        $selected = "";
                                        $key = $puja_arr[$i]["id"];
                                        $value = $puja_arr[$i]["pooja_name"];
                                        if ($key == $pooja_id_1)
                                            $selected = "selected";
                                        ?>
                                                                                                <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                    <?php } ?>    
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input id="puja_date" class="form-control" type="text" value="<?php echo $puja_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="puja_date">
                                                                            </td>
                                                                            <td>
                                                                                <input id="puja_time" class="form-control" type="text" value="<?php echo $puja_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="puja_time">
                                                                            </td>
                                                                        </tr>
                                    
                                    -->

















                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>








        </div>

        <!--
                <div class="col-lg-6">
                    <div class="box">
                        <div class="body">
                            <div class="panel panel-primary">
                                <div class="panel-heading"> Puja Details </div>
                                <div class="panel-body">
        
                                    <table class="table table-bordered sortableTable responsive-table">
        
                                        <tbody>
        
        
                                            <tr>
                                                <td>Pooja Date</td>
                                                <td>
                                                    <input id="puja_date" class="form-control" type="text" value="<?php echo $puja_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="puja_date">
                                                </td>
                                            </tr>
        
                                            <tr>
                                                <td>Pooja Time</td>
                                                <td>
                                                    <input id="puja_time" class="form-control" type="text" value="<?php echo $puja_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="puja_time">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Puja</td>
                                                <td>
                                                    <select tabindex="2"  class="form-control" data-placeholder="Select Puja 1" id="enq_puja_1" name="enq_puja_1">                    
                                                        <option selected="" value="0">-</option>
        <?php
        for ($i = 0; $i < count($puja_arr); $i++) {

            //echo "$key->$value";
            $selected = "";
            $key = $puja_arr[$i]["id"];
            $value = $puja_arr[$i]["pooja_name"];
            if ($key == $pooja_id_1)
                $selected = "selected";
            ?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
        <?php } ?>    
                                                    </select>
                                                </td>
                                            </tr>
        
        
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <select tabindex="2"  class="form-control" data-placeholder="Select Puja 2" id="enq_puja_2" name="enq_puja_2">                    
                                                        <option selected="" value="0">-</option>
        <?php
        for ($i = 0; $i < count($puja_arr); $i++) {

            //echo "$key->$value";
            $selected = "";
            $key = $puja_arr[$i]["id"];
            $value = $puja_arr[$i]["pooja_name"];
            if ($key == $pooja_id_2)
                $selected = "selected";
            ?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
        <?php } ?>    
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <select tabindex="2"  class="form-control" data-placeholder="Select Puja 3" id="enq_puja_3" name="enq_puja_3">                    
                                                        <option selected="" value="0">-</option>
        <?php
        for ($i = 0; $i < count($puja_arr); $i++) {

            //echo "$key->$value";
            $selected = "";
            $key = $puja_arr[$i]["id"];
            $value = $puja_arr[$i]["pooja_name"];
            if ($key == $pooja_id_3)
                $selected = "selected";
            ?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
        <?php } ?>    
                                                    </select>
                                                </td>
                                            </tr>
        
                                            <tr>
                                                <td>Type of Samagri</td>
                                                <td>
                                                    <select tabindex="2" onchange="refresh_state();" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                        <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">No</option>
                                                        <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Kit</option>
                                                        <option <?php if (2 == $samagri_type) echo "selected"; ?> value="2">Extra</option>
                                                        <option <?php if (3 == $samagri_type) echo "selected"; ?> value="3">Both</option>
        
                                                    </select>
                                                </td>
                                            </tr>
        
        
        
        
        
                                        </tbody>
                                    </table>
        
                                </div>
                            </div>
                        </div>
                    </div>
        
        
        
        
        
        
        
        
                </div>
        -->

    </div>

</div>


<div class="row">
    <div class="col-lg-12">


        <div class="box">

            <div class="body collapse in" id="sortableTable">
                <table class="table table-bordered sortableTable responsive-table">

                    <tbody>


                        <tr>
                            <td>Pandit Language </td>
                            <td>
                                <select tabindex="2"  class="form-control" data-placeholder="Choose Language of the Pandit" id="enq_pandit_lang" name="enq_pandit_lang">                    
                                    <option selected="" value="0">-</option>
                                    <?php
                                    for ($i = 0; $i < count($language_arr); $i++) {

                                        //echo "$key->$value";
                                        $selected = "";
                                        $key = $language_arr[$i]["id"];
                                        $value = $language_arr[$i]["language_name"];
                                        if ($key == $enq_pandit_lang)
                                            $selected = "selected";
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                    <?php } ?>
                                </select>
                            </td>

                            <td>Pandit <br/>Caste / Religion </td>
                            <td>
                                <select tabindex="2"  class="form-control" data-placeholder="Choose Caste, sub-caste" id="enq_pandit_cast" name="enq_pandit_cast">                    
                                    <option selected="" value="0">-</option>
                                    <?php
                                    for ($i = 0; $i < count($caste_arr); $i++) {

                                        //echo "$key->$value";
                                        $selected = "";
                                        $key = $caste_arr[$i]["id"];
                                        $value = $caste_arr[$i]["caste_name"];
                                        if ($key == $enq_pandit_cast)
                                            $selected = "selected";
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                    <?php } ?>    
                                </select>
                            </td>
                        </tr>



                        <tr>
                            <td>Pandit Specification </td>
                            <td>
                                <textarea id="enq_pandit_specification" class="form-control" placeholder="If he/she says something else related to the Pandit" name="enq_pandit_specification"><?php echo $enq_pandit_specification; ?></textarea>
                            </td>

                            <td>Important Points</td>
                            <td>
                                <textarea id="imp_points" class="form-control" placeholder="Miscellaneous Comments/Feedback/Important Points from Client" name="imp_points"><?php echo $imp_points; ?></textarea>
                            </td>
                        </tr>



                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>



<div class="form-actions" style="text-align:center">
    <input class="btn btn-primary btn-lg" type="submit" value="Save Changes" name="submit" onclick="add_enquiry()" >
</div>



<?php /* ?>

  <div class="col-lg-12">
  <div class="box">

  <header>
  <h5>Enquiry Details</h5>
  <div class="toolbar">
  <div class="btn-group">
  <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
  <i class="icon-chevron-up"></i>
  </a>
  </div>
  </div>
  </header>

  <div class="body collapse in" id="sortableTable">
  <table class="table table-bordered sortableTable responsive-table">

  <tbody>

  <tr>
  <td>Enquiry Number</td>
  <td>
  <input id="enq_no" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Enquiry Number " name="enq_no">
  <input id="cust_id" class="form-control" type="hidden" value="<?php echo $cust_id; ?>" placeholder="Enquiry Number " name="cust_id">
  <input id="edit_id" class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="edit_id">
  </td>


  <td>Enquiry Date</td>
  <td>
  <input id="enq_date" class="form-control" type="text" value="<?php echo $enq_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="enq_date">
  </td>
  </tr>
  <tr>
  <td>Name of Pooja</td>

  <td>
  <select tabindex="2" class="form-control" data-placeholder="Choose Puja" id="puja_name" name="puja_name">
  <option value="0"> - </option>
  <option value="2">Australia</option>
  <option value="3">Russia</option>
  <option value="11">country _ 2</option>
  <option value="12">Country _5</option>
  <option value="13">Country_6</option>
  </select>
  </td>
  <td>
  <select tabindex="2" class="form-control" data-placeholder="Choose Puja" id="puja_name" name="puja_name">
  <option value="0"> - </option>
  <option value="2">Australia</option>
  <option value="3">Russia</option>
  <option value="11">country _ 2</option>
  <option value="12">Country _5</option>
  <option value="13">Country_6</option>
  </select>
  </td>
  <td>
  <select tabindex="2" class="form-control" data-placeholder="Choose Puja" id="puja_name" name="puja_name">
  <option value="0"> - </option>
  <option value="2">Australia</option>
  <option value="3">Russia</option>
  <option value="11">country _ 2</option>
  <option value="12">Country _5</option>
  <option value="13">Country_6</option>
  </select>
  </td>
  <!--
  <td>
  <input id="puja_name_txt" name="puja_name_txt" class="form-control" type="text" value="" placeholder="Name of Pooja" >
  </td>
  -->
  </tr>

  <tr>
  <td>Date for Pooja</td>
  <td>
  <input id="puja_date" class="form-control" type="text" value="<?php echo $puja_date; ?>" placeholder="dd-mm-yyyy" data-mask="99-99-9999" name="puja_date">
  </td>

  <td>Timings for Pooja</td>
  <td>
  <input id="puja_time" class="form-control" type="text" value="<?php echo $puja_time; ?>" placeholder="hh:mm AM" data-mask="99:99 aa" name="puja_time">
  </td>
  </tr>


  <tr>
  <td>Source</td>
  <td>
  <select tabindex="2" class="form-control" data-placeholder="Choose Source" id="enq_source" name="enq_source">
  <option selected="" value="0">-</option>
  <?php
  for ($i = 0; $i < count($source_arr); $i++) {

  //echo "$key->$value";
  $selected = "";
  $key = $source_arr[$i]["id"];
  $value = $source_arr[$i]["source_name"];
  if ($key == $enq_source)
  $selected = "selected";
  ?>
  <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
  <?php } ?>

  </select>
  </td>
  <!--
  <td>
  <input id="enq_source_txt" class="form-control" type="text" value="" placeholder="Where did you get to know about us from?" name="enq_source_txt">
  </td>
  -->

  <td>Medium</td>
  <td>
  <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="enq_medium" name="enq_medium">
  <option selected="" value="0">-</option>
  <?php
  for ($i = 0; $i < count($medium_arr); $i++) {

  //echo "$key->$value";
  $selected = "";
  $key = $medium_arr[$i]["id"];
  $value = $medium_arr[$i]["medium_name"];
  if ($key == $enq_medium)
  $selected = "selected";
  ?>
  <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
  <?php } ?>
  </select>
  </td>
  <!--
  <td>
  <input id="enq_medium_txt" class="form-control" type="text" value="" placeholder="Phone/Custom/JD etc." name="enq_medium_txt">
  </td>
  -->
  </tr>

  <tr>
  <td>Comment</td>
  <td>
  <textarea id="enq_comment" class="form-control" placeholder="Comment" name="enq_comment"><?php echo $enq_comment; ?></textarea>
  </td>

  <td>Muhurat advice</td>
  <td>
  <textarea id="enq_muhurat" class="form-control" placeholder="Preferred Panchang, tithi, etc." name="enq_muhurat"><?php echo $enq_muhurat; ?></textarea>
  </td>
  </tr>





  </tbody>
  </table>
  </div>
  </div>

  </div>

  <div class="col-lg-12">
  <div class="box">

  <header>
  <h5>Pandit Details</h5>
  <div class="toolbar">
  <div class="btn-group">
  <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#Pandit_Details">
  <i class="icon-chevron-up"></i>
  </a>
  </div>
  </div>
  </header>

  <div class="body collapse in" id="Pandit_Details">
  <table class="table table-bordered sortableTable responsive-table">

  <tbody>


  <tr>
  <td>Language of the Pandit</td>

  <td>
  <select tabindex="2"  class="form-control" data-placeholder="Choose Language of the Pandit" id="enq_pandit_lang" name="enq_pandit_lang">
  <option selected="" value="0">-</option>
  <?php
  for ($i = 0; $i < count($language_arr); $i++) {

  //echo "$key->$value";
  $selected = "";
  $key = $language_arr[$i]["id"];
  $value = $language_arr[$i]["language_name"];
  if ($key == $enq_pandit_lang)
  $selected = "selected";
  ?>
  <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
  <?php } ?>
  </select>
  </td>
  <td>
  <input id="enq_pandit_lang_txt" class="form-control" type="text" value="" placeholder="Choose Language of the Pandit" name="enq_pandit_lang_txt">
  </td>
  </tr>

  <tr>
  <td>Caste and Religion of Pandit</td>
  <td>
  <select tabindex="2"  class="form-control" data-placeholder="Choose Caste, sub-caste" id="enq_pandit_cast" name="enq_pandit_cast">
  <option selected="" value="0">-</option>
  <?php
  for ($i = 0; $i < count($caste_arr); $i++) {

  //echo "$key->$value";
  $selected = "";
  $key = $caste_arr[$i]["id"];
  $value = $caste_arr[$i]["caste_name"];
  if ($key == $enq_pandit_cast)
  $selected = "selected";
  ?>
  <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
  <?php } ?>
  </select>
  </td>
  <td>
  <input id="enq_pandit_cast_txt" class="form-control" type="text" value="" placeholder="Caste, sub-caste, religion, etc." name="enq_pandit_cast_txt">
  </td>
  </tr>
  <tr>
  <td>Type of Samagri</td>
  <td>
  <select tabindex="2" onchange="refresh_state();" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">
  <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">No</option>
  <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Kit</option>
  <option <?php if (2 == $samagri_type) echo "selected"; ?> value="2">Extra</option>
  <option <?php if (3 == $samagri_type) echo "selected"; ?> value="3">Both</option>

  </select>
  </td>

  </tr>


  <tr>


  <td>Any other specification for Pandit</td>
  <td>
  <textarea id="enq_pandit_specification" class="form-control" placeholder="If he/she says something else related to the Pandit" name="enq_pandit_specification"><?php echo $enq_pandit_specification; ?></textarea>
  </td>

  <td>Important Points</td>
  <td>
  <textarea id="imp_points" class="form-control" placeholder="Miscellaneous Comments/Feedback/Important Points from Client" name="imp_points"><?php echo $imp_points; ?></textarea>
  </td>
  </tr>







  </tbody>
  </table>
  </div>
  </div>

  </div>

  <?php */ ?>