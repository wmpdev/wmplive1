<?php
include_once '../../config/auth.php';
//print_r($_POST);

if (strlen($_POST["enq_id"]) > 0) {
    $enq_id = $_POST["enq_id"];
}
//$enq_id
$puja_arr = getDataFromTable("tbl_pooja_master");
list($status, $id, $enq_puja_quotation_arr) = getEnqQuotation(0, $enq_id);

$pandit_arr = getDataFromTable("tbl_pandit");

//print_r($enq_puja_quotation_arr);
//$enq_no = "";            
?>
<div class="box" id="quotation_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab"><i class="icon-list-ul"></i>  List Quotation</a>
                    </li>
                    <li><a href="#profile" data-toggle="tab"> <i class="icon-plus"></i>  Add Quotation</a>
                    </li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home">                        
                        <div class="box">

                            <div class="body collapse in" id="sortableTable">
                                <table class="table table-bordered sortableTable responsive-table">

                                    <tbody>                                    
                                    <input id="quotation_list_enq_id"  class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="puja_enq_id">
                                    <?php if ($enq_puja_quotation_arr[0]["id"] > 0) { ?>
                                        <tr>
                                            
                                            <th width="215px">Type</th>
                                            <th width="270px">Pandit</th>
                                            <th width="85px">Pandit <br/>Actual</th>
                                            <th width="85px">Pandit <br/>Selling</th>
                                            <th width="75px">Samagri <br/>Sukhi</th>
                                            <th width="75px">Samagri <br/>Gili</th>
                                            <th width="70px">Extra</th>
                                            <th width="75px">Sub <br/>Total</th>
                                            <th width="75px">Disc</th>
                                            <th width="105px">Total</th>


                                            <th width="60px">Action</th>
                                        </tr>

                                        <?php
                                        $quotation_count = 0;
                                        for ($enq_puja_quotation_index = 0; $enq_puja_quotation_index < count($enq_puja_quotation_arr); $enq_puja_quotation_index++) {
                                            $quotation_count ++;
                                            $quotation_id = $enq_puja_quotation_arr[$enq_puja_quotation_index]["id"];
                                            $pandit_id = $enq_puja_quotation_arr[$enq_puja_quotation_index]["pandit_id"];
                                            list($status, $id, $quotation_pandit) = getPanditData($pandit_id);
                                            $pandit_name = $quotation_pandit[0]["first_name"] . " " . $quotation_pandit[0]["last_name"];

                                            $actual_cost = $enq_puja_quotation_arr[$enq_puja_quotation_index]["actual_cost"];
                                            $selling_cost = $enq_puja_quotation_arr[$enq_puja_quotation_index]["selling_cost"];
                                            $samagri_sukhi = $enq_puja_quotation_arr[$enq_puja_quotation_index]["samagri_sukhi"];
                                            $samagri_gili = $enq_puja_quotation_arr[$enq_puja_quotation_index]["samagri_gili"];
                                            $extra = $enq_puja_quotation_arr[$enq_puja_quotation_index]["extra"];


                                            $sub_total = $enq_puja_quotation_arr[$enq_puja_quotation_index]["sub_total"];
                                            $discount = $enq_puja_quotation_arr[$enq_puja_quotation_index]["discount"];
                                            $discount_comments = $enq_puja_quotation_arr[$enq_puja_quotation_index]["discount_comments"];
                                            $total = $enq_puja_quotation_arr[$enq_puja_quotation_index]["total"];
                                            $co_ordinator = $enq_puja_quotation_arr[$enq_puja_quotation_index]["co_ordinator"];
                                            $samagri_type = $enq_puja_quotation_arr[$enq_puja_quotation_index]["samagri_type"];

                                            $comments = $enq_puja_quotation_arr[$enq_puja_quotation_index]["comments"];

                                            //list($puja_date, $puja_time) = validateDbDateTime($puja_date);
                                            //$puja_date = $puja_date . " " . $puja_time;
                                            $row_color = "";
                                            //echo "<br/> $enq_quotation_id - $quotation_id ";
                                            if ($enq_quotation_id == $quotation_id) {
                                                $row_color = "success";
                                            }
                                            ?>
                                            <?php if (strlen(trim($discount_comments)) > 0 || strlen(trim($comments)) > 0) { ?>
                                                <tr>
                                                    <td colspan="2"><?php echo "<code> Discount : </code> $discount_comments"; ?></td>
                                                    <td colspan="9"><?php echo "<code> Comments : </code> $comments"; ?></td>
                                                </tr>
                                            <?php } ?>
                                            <tr class="<?php echo $row_color; ?>">
                                                <!-- <td><?php echo $quotation_count; ?></td> -->
                                                <td><?php echo $quotation_count.". ".$samagri_type; ?>
                                                    <br/>                                                    
                                                    <!--
                                                    <a href="javascript:void(0);" onclick="send_Puja_D(<?php echo $enq_id; ?>, 'EMAIL', 'Customer',<?php echo $quotation_id; ?>)" class="btn text-info btn-xs btn-flat">
                                                        <i class="icon-list-alt icon-white"></i> Email To Customer</a>
                                                    -->

                                                </td>
                                                <td><?php echo $pandit_name ; ?></td>
                                                <td><?php echo $actual_cost; ?></td>
                                                <td><?php echo $selling_cost; ?></td>
                                                <td><?php echo $samagri_sukhi; ?></td>
                                                <td><?php echo $samagri_gili; ?></td>
                                                <td><?php echo $extra; ?></td>
                                                <td>
                                                    <?php echo $sub_total; ?>
                                                </td>
                                                <td>
                                                    <?php echo $discount; ?>
                                                </td>
                                                <td>
                                                    <?php echo $total; ?>
                                                </td>
                                                <td>
                                                    <!-- <button id="btn-chat" class="btn btn-success btn-sm" > Add</button> -->
                                                    <a href="javascript:void(0);" onclick="confirmQuotation(<?php echo $enq_id . "," . $quotation_id; ?>)" class="btn text-info btn-xs btn-flat">
                                                        <i class="icon-thumbs-up-alt icon-large"></i> </a>


                                                    <!--
                                                    <button class="btn">
                                                        <i class="icon-eye-open"></i>Add                                    
                                                    </button>
                                                    -->
                                                </td>
                                            </tr>


                                            <?php
                                        }
                                    } else {
                                        echo "<h2> NO Quotation Present.<h2>";
                                    }
                                    ?>










                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="profile">

                        <form name="quotation_form" id="quotation_form" action="" method="POST" enctype="multipart/form-data">
                            <div class="row">                           

                                <div class="col-lg-6">
                                    <div class="box">
                                        <input id="quotation_add_enq_id"  class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="quotation_add_enq_id">
                                        <input id="ACTION"  class="form-control" type="hidden" value="AddQuotation" placeholder="Enquiry Number " name="ACTION">
                                        <div class="body collapse in" id="sortableTable">
                                            <table class="table table-bordered sortableTable responsive-table">

                                                <tbody>





                                                    <tr>

                                                        <td  style="text-align: right;">
                                                            Sub Total<h4><i class="icon-inr icon-white"></i> <span id="td_sub_total_amount"></span></h4>
                                                        </td>
                                                        <td style="text-align: right;">
                                                            Total <h4><i class="icon-inr icon-white"></i> <span id="td_total_amount"></span></h4>

                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td>Pandit Name</td>
                                                        <td>
                                                            <select tabindex="2" class="form-control" data-placeholder="Choose Source" id="pandit_id" name="pandit_id">  
                                                                <option selected="" value="0">-</option>
                                                                <?php
                                                                for ($i = 0; $i < count($pandit_arr); $i++) {

                                                                    //echo "$key->$value";
                                                                    $selected = "";
                                                                    $key = $pandit_arr[$i]["id"];
                                                                    $value = $pandit_arr[$i]["first_name"] . " " . $pandit_arr[$i]["last_name"];
                                                                    if ($key == $enq_source)
                                                                        $selected = "selected";
                                                                    ?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                                <?php } ?>

                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <!--
                                                    <tr>
                                                        <td>Pandit Actual Cost</td>
                                                        <td>
                                                            <input id="pandit_cost" class="form-control" type="text" value="" placeholder="Pandit Actual Cost" name="pandit_cost" onkeyup="calculate_amount()">      
                                                        </td>
                                                        
                                                    </tr>
                                                    -->
                                                    <tr>
                                                        <td>Co-Ordinator</td>
                                                        <td>
                                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="co_ordinator" name="co_ordinator">                    
                                                                <option selected="" value="0">-</option>
                                                                <?php
                                                                $Co_ordinator_arr = getMonitor("pandit_cordination");
                                                                for ($i = 0; $i < count($Co_ordinator_arr); $i++) {

                                                                    //echo "$key->$value";
                                                                    $selected = "";
                                                                    $key = $Co_ordinator_arr[$i]["id"];
                                                                    $value = $Co_ordinator_arr[$i]["monitor_name"];
                                                                    if ($key == $enq_medium)
                                                                    //$selected = "selected";
                                                                        
                                                                        ?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                                <?php } ?>  
                                                            </select>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Type of Service</td>
                                                        <td colspan="2">
                                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                                <!-- <option selected="" value="0">-</option> -->
                                                                <option  selected="" value="Pandit">Pandit</option>
                                                                <option  value="Samagri">Samagri</option>
                                                                <option  value="Pandit + Samagri">Pandit + Samagri</option>

                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Approved By</td>
                                                        <td>
                                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="approved_by" name="approved_by">                    
                                                                <option selected="" value="0"> - </option>
                                                                <?php
                                                                $quote_approval_arr = getMonitor("quote_approval");
                                                                for ($i = 0; $i < count($quote_approval_arr); $i++) {

                                                                    //echo "$key->$value";
                                                                    $selected = "";
                                                                    $key = $quote_approval_arr[$i]["id"];
                                                                    $value = $quote_approval_arr[$i]["monitor_name"];
                                                                    if ($key == $enq_medium)
                                                                    //$selected = "selected";
                                                                        
                                                                        ?>
                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"> <?php echo $value; ?> </option> 
                                                                <?php } ?>  
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Discount Reason</td>
                                                        <td colspan="2">
                                                            <input id="discount_reason" class="form-control" type="text" value="" placeholder="Discount Reason" name="discount_reason">                             
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Comment</td>
                                                        <td colspan="2">
                                                            <textarea id="comment" class="form-control" placeholder="Comment" name="comment"></textarea>
                                                        </td>
                                                    </tr>

                                                    <!--
                                                    <tr>
                                                        <td>Client Approved</td>
                                                        <td colspan="2">
                                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                                <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">No</option>
                                                                <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Yes</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    -->


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-lg-6">
                                    <div class="box">
                                        <div class="body">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading"> Payment Details </div>
                                                <div class="panel-body">



                                                    <table class="table table-bordered sortableTable responsive-table">

                                                        <tbody>



                                                            <tr>
                                                                <td style="width: 100px;">Pandit Cost</td>

                                                                <td>
                                                                    <input id="pandit_selling" class="form-control" type="text" value="" placeholder=" Selling Cost" name="pandit_selling" onkeyup="calculate_amount()" title="Selling Cost">                          
                                                                </td>
                                                                <td>
                                                                    <input id="pandit_cost" class="form-control" type="text" value="" placeholder="Actual Cost" name="pandit_cost" onkeyup="calculate_amount()" title="Actual Cost">          
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td>Samagri</td>
                                                                <td>
                                                                    <input id="samagri_sukhi" class="form-control" type="text" value="" placeholder="Samagri Sukhi" name="samagri_sukhi" onkeyup="calculate_amount()" title="Samagri Sukhi">                                    
                                                                </td>
                                                                <td>
                                                                    <input id="samagri_gili" class="form-control" type="text" value="" placeholder="Samagri Gili" name="samagri_gili" onkeyup="calculate_amount()" title="Samagri Gili">                                    
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Other</td>
                                                                <td>
                                                                    <input id="conveyance" class="form-control" type="text" value="" placeholder="Conveyance" name="conveyance" onkeyup="calculate_amount()">                                    
                                                                </td>
                                                                <td>
                                                                    <input id="extra_cost" class="form-control" type="text" value="" placeholder="Extra" name="extra_cost" onkeyup="calculate_amount()">                      
                                                                    <!-- <input id="extra_cost_percent" class="form-control" type="text" value="" placeholder="By Percent" name="extra_cost_percent">                        -->            
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td style="width: 100px;">Discount</td>
                                                                <td>
                                                                    <input id="discount_cost" class="form-control" type="text" value="" placeholder="Discount Amount" name="discount_cost" onkeyup="calculate_amount()">  
                                                                </td>
                                                                <td>
                                                                    &nbsp;                          
                                                                </td>
                                                            </tr>






                                                        </tbody>
                                                    </table>

                                                </div>
                                                <div class="panel-footer" align="right"> 
                                                    <span class="input-group-btn">
                                                        <input class="btn btn-success " type="submit" value="Save" name="Save" >
                                                    </span>


                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <!--
                                                                <div class="form-actions" style="text-align:center">
                                                                    <input class="btn btn-primary btn-lg" type="submit" value="Save Changes" name="submit" onclick="add_quotation()" >
                                                                </div>
                                -->


                            </div>      
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>










