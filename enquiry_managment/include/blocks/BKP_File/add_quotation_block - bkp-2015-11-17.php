<?php $enq_no = ""; ?>
<div class="box" id="quotation_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-6">
            <div class="box">

                <div class="body collapse in" id="sortableTable">
                    <table class="table table-bordered sortableTable responsive-table">

                        <tbody>


                            <tr>
                                <td>Pandit Cost</td>
                                <td>
                                    <input id="pandit_cost" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Pandit Actual Cost" name="pandit_cost">                          
                                    <input id="cust_id" class="form-control" type="hidden" value="<?php echo $cust_id; ?>" placeholder="Enquiry Number " name="cust_id">
                                    <input id="enq_id"  class="form-control" type="hidden" value="<?php echo $enq_id; ?>" placeholder="Enquiry Number " name="edit_id">
                                </td>
                                <td>
                                    <input id="pandit_selling" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Pandit Selling Cost" name="pandit_selling">                          
                                </td>
                            </tr>


                            <tr>
                                <td>Samagri</td>
                                <td>
                                    <input id="samagri_sukhi" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Samagri Sukhi" name="samagri_sukhi">                                    
                                </td>
                                <td>
                                    <input id="samagri_gili" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Samagri Gili" name="samagri_gili">                                    
                                </td>
                            </tr>

                            <tr>
                                <td>Extra</td>
                                <td>
                                    <input id="extra_cost" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="By Value" name="extra_cost">                                    
                                </td>
                                <td>
                                    <input id="extra_cost_percent" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="By Percent" name="extra_cost_percent">                                    
                                </td>
                            </tr>


                            <tr>
                                <td>Sub Total</td>
                                <td colspan="2">
                                    <input id="sub_total" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Sub Total " name="sub_total">                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td>Discount</td>
                                <td>
                                    <input id="discount_cost" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="By Value" name="discount_cost">                                    
                                </td>
                                <td>
                                    <input id="discount_cost_percent" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="By Percent" name="discount_cost_percent">                                    
                                </td>
                            </tr>

                            
                            <tr>
                                <td>Total</td>
                                <td colspan="2">
                                    <input id="total" class="form-control" type="text" value="<?php echo $enq_no; ?>" placeholder="Enquiry Number " name="total">                                    
                                </td>
                            </tr>

                            



                            <tr>
                                <td>Comment</td>
                                <td colspan="2">
                                    <textarea id="comment" class="form-control" placeholder="Comment" name="comment"><?php echo $enq_comment; ?></textarea>
                                </td>
                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>

        </div>


        <div class="col-lg-6">
            <div class="box">
                <div class="body">
                    <div class="panel panel-primary">
                        <div class="panel-heading"> Pandit Details </div>
                        <div class="panel-body">

                            <table class="table table-bordered sortableTable responsive-table">

                                <tbody>





                                    <tr>
                                        <td>Pandit Name</td>
                                        <td>
                                            <select tabindex="2" class="form-control" data-placeholder="Choose Source" id="pandit_name" name="pandit_name">  
                                                <option selected="" value="0">-</option>
                                                <?php
                                                for ($i = 0; $i < count($pandit_arr); $i++) {

                                                    //echo "$key->$value";
                                                    $selected = "";
                                                    $key = $pandit_arr[$i]["id"];
                                                    $value = $pandit_arr[$i]["first_name"];
                                                    if ($key == $enq_source)
                                                        $selected = "selected";
                                                    ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                <?php } ?>

                                            </select>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Co-Ordinator</td>
                                        <td>
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="co_ordinator" name="co_ordinator">                    
                                                <option selected="" value="0">-</option>
                                                <?php
                                                $Co_ordinator_arr = getMonitor("pandit_cordination");
                                                for ($i = 0; $i < count($Co_ordinator_arr); $i++) {

                                                    //echo "$key->$value";
                                                    $selected = "";
                                                    $key = $Co_ordinator_arr[$i]["id"];
                                                    $value = $Co_ordinator_arr[$i]["monitor_name"];
                                                    if ($key == $enq_medium)
                                                        $selected = "selected";
                                                    ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                <?php } ?>  
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Type of Samagri</td>
                                        <td colspan="2">
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">Pandit</option>
                                                <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Samagri</option>
                                                <option <?php if (2 == $samagri_type) echo "selected"; ?> value="2">Pandit + Samagri</option>

                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Approved By</td>
                                        <td>
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="approved_by" name="approved_by">                    
                                                    <option selected="" value="0"> - </option>
                                                <?php
                                                $quote_approval_arr = getMonitor("quote_approval");
                                                for ($i = 0; $i < count($medium_arr); $i++) {

                                                    //echo "$key->$value";
                                                    $selected = "";
                                                    $key = $quote_approval_arr[$i]["id"];
                                                    $value = $quote_approval_arr[$i]["monitor_name"];
                                                    if ($key == $enq_medium)
                                                        $selected = "selected";
                                                    ?>
                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"> <?php echo $value; ?> </option> 
                                                <?php } ?>  
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Client Approved</td>
                                        <td colspan="2">
                                            <select tabindex="2" class="form-control" data-placeholder="Choose a Country" id="samagri_type" name="samagri_type">                    
                                                <option <?php if (0 == $samagri_type) echo "selected"; ?> value="0">No</option>
                                                <option <?php if (1 == $samagri_type) echo "selected"; ?> value="1">Yes</option>
                                            </select>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>


        </div>



    </div>

</div>




<div class="form-actions" style="text-align:center">
    <input class="btn btn-primary btn-lg" type="submit" value="Save Changes" name="submit" onclick="add_quotation()" >
</div>



