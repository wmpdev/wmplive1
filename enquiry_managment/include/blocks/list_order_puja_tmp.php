<?php
list($status, $id, $puja_arr) = getOrderPujaTmpData(0, $enq_id);
//print_r($puja_arr);
//echo "Data = $status = ".count($puja_arr);
//$puja_data_arr = getDataFromTable("tbl_pooja_master");
//$pandit_arr = getDataFromTable("tbl_pandit");
?>

<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
            <div class="box" id="list_order_puja_tmp_msg" >
                <div class="col-lg-12">
                    <?php if ($status == 0) { ?>

                        <div class="alert alert-danger alert-dismissable">
                            <!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
                            <?php echo "No Puja Available."; ?>
                        </div>

                    <?php } ?>
                </div>
            </div>

            <?php if ($status == 1) { ?>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th style="width: 50px">No</th>
                                    <th>Puja Name</th>
                                    <th style="width: 200px">Puja Date </th>
                                    <th>Pandit Name </th>
                                    <th style="width: 100px">Type </th>
                                    <!-- <th style="width: 100px">Arranged</th> -->
                                    <th style="width:105px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $count = 0;
//while ($row = $stmt->fetch_assoc()) {
                                for ($i = 0; $i < count($puja_arr); $i++) {

                                    if ($puja_arr[$i]["id"] == $edit_puja_id) {
                                        $pointer = $i;
                                    }
                                    $count++;
                                    //list($enq_date, $enq_time) = validateDbDateTime($puja_arr[$i]["enq_date"]);
                                    //$enq_date = $enq_date . " " . $enq_time;
                                    $puja_name = "";
                                    $pandit_name = "";
                                    if ($puja_arr[$i]['puja_id'] > 0) {
                                        for ($j = 0; $j < count($puja_data_arr); $j++) {
                                            if ($puja_data_arr[$j]["id"] == $puja_arr[$i]['puja_id'])
                                                $puja_name = $puja_data_arr[$j]["pooja_name"];
                                        }
                                    }else {
                                        $puja_name = $puja_arr[$i]['puja_name'];
                                    }

                                    for ($j = 0; $j < count($pandit_arr); $j++) {
                                        if ($pandit_arr[$j]["id"] == $puja_arr[$i]['pandit_id']) {
                                            $pandit_name = $pandit_arr[$j]["first_name"] . " " . $pandit_arr[$j]["last_name"];
                                            $pandit_name .= "<br/> Phone :- " . $pandit_arr[$j]["phone_1"] . " , " . $pandit_arr[$j]["phone_2"];
                                            $pandit_name .= "<br/> Email :- " . $pandit_arr[$j]["email_1"];
//$pandit_contact = $pandit_arr[$j]["phone_1"];
                                            //$pandit_email = $pandit_arr[$j]["email_1"];
                                        }
                                    }

                                    list($puja_date, $puja_time) = validateDbDateTime($puja_arr[$i]["puja_date"]);
                                    $puja_date = $puja_date . " " . $puja_time;
                                    ?> 
                                    <tr class="odd gradeX">

                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $puja_name . " - " . $puja_arr[$i]['puja_id']; ?></td> 

                                        <td><?php echo $puja_date; ?></td>
                                        <td><?php echo $pandit_name; ?></td>
                                        <td><?php echo $puja_arr[$i]['samagri_type']; ?></td>
                                        <!-- <td><?php echo $puja_arr[$i]['samagri_arrange_by']; ?></td> -->
                                        <td>
                                            <a href="add_order_tmp.php?edit_id=<?php echo $enq_id; ?>&puja_id=<?php echo $puja_arr[$i]['id']; ?>&tab=edit" class="btn text-info btn-xs btn-flat">
                                                <i class="icon-list-alt icon-white"></i> Edit</a>

                                            <a href="javascript:void(0);" onclick="send_Puja_Data(<?php echo $enq_id; ?>, 'EMAIL', 'info',<?php echo $puja_arr[$i]['id']; ?>)" class="btn text-info btn-xs btn-flat">
                                                <i class="icon-list-alt icon-white"></i> Email To Info</a>

                                            <?php if (strlen($cust_email) > 5) { ?>
                                                <a href="javascript:void(0);" onclick="send_Puja_Data(<?php echo $enq_id; ?>, 'EMAIL', 'Customer',<?php echo $puja_arr[$i]['id']; ?>)" class="btn text-info btn-xs btn-flat">
                                                    <i class="icon-list-alt icon-white"></i> Email To Customer</a>
                                            <?php } ?>


                                            <a href="javascript:void(0);" onclick="send_Puja_Data(<?php echo $enq_id; ?>, 'SMS', 'info',<?php echo $puja_arr[$i]['id']; ?>)" class="btn text-info btn-xs btn-flat">
                                                <i class="icon-list-alt icon-white"></i> SMS To Info</a>

                                            <?php if (strlen($cust_no) > 8) { ?>
                                                <a href="javascript:void(0);" onclick="send_Puja_Data(<?php echo $enq_id; ?>, 'SMS', 'Customer',<?php echo $puja_arr[$i]['id']; ?>)" class="btn text-info btn-xs btn-flat">
                                                    <i class="icon-list-alt icon-white"></i> SMS To Customer</a>
                                            <?php } ?>
                                            <?php if (strlen($puja_arr[$i]['pandit_contact']) > 8) { ?>
                                                <a href="javascript:void(0);" onclick="send_Puja_Data(<?php echo $enq_id; ?>, 'SMS', 'Pandit',<?php echo $puja_arr[$i]['id']; ?>)" class="btn text-info btn-xs btn-flat">
                                                    <i class="icon-list-alt icon-white"></i> SMS To Pandit</a>
                                            <?php } ?>




                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>





                            </tbody>
                        </table>
                    </div>

                </div>
            <?php } ?>
        </div>





    </div>

</div>





<div class="form-actions" style="text-align:center">
    <input class="btn btn-primary btn-lg" type="submit" value="Save Changes" name="submit" >
</div>

<script>

</script>
