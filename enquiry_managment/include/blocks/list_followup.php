<?php

include_once '../../config/auth.php';

if(!empty($_POST["enq_id"])){
    $enq_id = $_POST["enq_id"];
}

list($status, $id, $followup_arr) = getFollowup($enq_id);
$status_arr = getStatus(1);

//$caste_arr = getDataFromTable("tbl_caste_master");
//$caste_arr = getDataFromTable("tbl_caste_master");
//echo "<pre>";
//print_r($followup_arr);
//echo "</pre>";
?>

<div class="chat-panel panel panel-success">
    <div class="panel-heading">
        <i class="icon-comments"></i>
        Previous Followups of Enquiry

    </div>

    <div class="panel-body" style="height: auto;">
        <ul class="chat">
            <?php
            if (count($followup_arr) > 0 && $status == 1 ) {

                for ($i = 0; $i < count($followup_arr); $i++) {

                    //$followup_date = date("d-m-Y", strtotime($followup_arr[$i]["add_enq_date"]));
                    //$followup_time = date("h:i A", strtotime($followup_arr[$i]["add_enq_time"]));
                    $followup_date = date("d-m-Y h:i a", strtotime($followup_arr[$i]["add_enq_date"]));
                    //$next_date = date("d-m-Y", strtotime($followup_arr[$i]["next_date"]));
                    //$next_time = date("h:i A", strtotime($followup_arr[$i]["next_time"]));
                    $next_date = date("d-m-Y h:i a", strtotime($followup_arr[$i]["next_date"]));
                    $admin_name = $followup_arr[$i]["admin_name"];
                    $admin_img = $followup_arr[$i]["thumb_img"];
                    $comment = $followup_arr[$i]["comment"];
                    
                    /*
                    $key = array_search($followup_arr[$i]["enq_status"], array_column($status_arr, 'id'));   //enq_status
                    if(isset($status_arr[$key]["status_name"])){
                        $enq_status = $status_arr[$key]["status_name"];
                    }
                    */
                    
                    list($status, $id, $msg) = searchIdInArray($followup_arr[$i]["enq_status"], $status_arr,'id');
                    if($status == 1){
                        $enq_status = $status_arr[$id]["status_name"];
                    }
                    else{
                        $enq_status =  $followup_arr[$i]["enq_status"];
                    }
                    
                    
                    ?>
                    <li class="left clearfix">
                        <span class="chat-img pull-left">
                            <img style="width: 50px;height: 50px;" class="img-circle" alt="Ninad Tilkar" src="<?php echo "..../../".$admin_img; ?>">
                        </span>
                        <div class="chat-body clearfix">
                            <div class="header">
                                <strong class="primary-font "> <?php echo $admin_name; ?> </strong>
                                <small class="pull-right text-muted label label-info">
                                    <i class="icon-time"></i> <?php echo $followup_date  ; ?>
                                </small>
                            </div>
                            <div><small class="pull-right"><code><?php echo $enq_status; ?></code></small></div>
                            <br/>
                            <p><?php echo $comment; ?></p>
                            
                            <div>
                                <br/>
                                
                                
                                <small class="pull-right">
                                    <b>Next Followup </b> : <?php echo $next_date  ; ?>
                                </small>
                            </div>
                        </div>
                    </li>
                    <?php
                }
            } else {
                ?>

                <li class="left clearfix">
                    
                    <div class="chat-body clearfix">
                        <div class="header">
                            <strong class="primary-font "> <?php echo $followup_arr; ?> </strong>
                            
                        </div>
                        
                    </div>
                </li>

            <?php } ?>
        </ul>
    </div>
</div>