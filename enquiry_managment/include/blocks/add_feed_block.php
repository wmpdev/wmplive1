<?php
$result_arr = "";

if ($edit_id > 0) {

    //list($status, $id, $result_arr) = getEnqData($edit_id);
    //[id] => 1 
    //[title] => Feed 2 
    //[image] => /feeds_img/1444127174_calendate-iphone.jpg 
    //[description] => Description 2 [type] => 2 
    //[url] => http://localhost/Ninad_app_feed/add_enquiry.php 
    //[publish_date] => 2015-10-06 00:00:00 
    //[add_date] => 2015-10-06 15:56:14 [mod_date] => 2015-10-06 15:56:14 [add_user] => 1 [mod_user] => 1 [status] => 0 


    list($status, $id, $data) = getFeeds($edit_id);
    $result_arr = $data;
    //print_r($result_arr);

    if ($status == 1) {
        //print_r($result_arr);
                
        $feed_id = $result_arr[0]["id"];
        $feed_type = $result_arr[0]["type"];
        $feed_url = $result_arr[0]["url"];
        $feed_date = $result_arr[0]["publish_date"];        
        $feed_date = date("d-m-Y h:i a", strtotime($feed_date));
        
        
                
        $feed_title = $result_arr[0]["title"];
        $date_english = $result_arr[0]["date_english"];
        $feed_desc = $result_arr[0]["description"];        
        $feed_img = $result_arr[0]["image"];
        
        
        $title_hindi = $result_arr[0]["title_hindi"];
        $date_hindi = $result_arr[0]["date_hindi"];
        $desc_hindi = $result_arr[0]["desc_hindi"];        
        $img_hindi = $result_arr[0]["img_hindi"];
        
        
        $feed_type_img = "";

        //$feed_hindi_desc = $result_arr[0]["hindi_desc"];
        //$feed_hindi_img = $result_arr[0]["hindi_img"];


        
    } else {
        echo " Error :- $result_arr";
    }
}

list($status, $id, $feed_type_arr) = getFeedType();
//print_r($feed_type_arr);
?>
<div class="box" id="enq_msg" >
    <div class="col-lg-12">            
        <!--
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
        </div>
        -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-12">
            <div class="box">

                <div class="body collapse in" id="sortableTable">
                    <form name="multiform" id="multiform" action="multi-form-submit.php" method="POST" enctype="multipart/form-data">
                        <table class="table table-bordered sortableTable responsive-table">

                            <tbody>


                            <input id="edit_id" class="form-control" type="hidden" value="<?php echo $feed_id; ?>" placeholder="Enquiry Number " name="edit_id">

                            <tr>
                                <td>Type</td>
                                <td>
                                    <select tabindex="2" class="form-control" data-placeholder="Choose Type" id="feed_type" name="feed_type">  
                                        <option selected="" value="0">-</option>
                                        <?php
                                        for ($i = 0; $i < count($feed_type_arr); $i++) {

                                            //echo "$key->$value";
                                            $selected = "";
                                            $key = $feed_type_arr[$i]["id"];
                                            $value = $feed_type_arr[$i]["feed_name"];
                                            if ($key == $feed_type) {
                                                $selected = "selected";
                                                $feed_type_img = $feed_type_arr[$i]["feed_img"];
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                        <!--                                            <option value="1">value 1</option> 
                                                                                    <option value="2">value 2</option> 
                                                                                    <option value="3">value 3</option> -->
                                    </select>
                                </td>
                                <td width="200px">
                                    <span class="fileupload-preview" style="margin-top: 10px;">
                                        <img src="<?php echo "media/feeds/feeds_icons/" . $feed_type_img; ?>" alt="" border="0" style="display:block; width: 35px; " />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>Publish Date</td>
                                <td>
                                    <input id="pub_date" class="form-control" type="text" value="<?php echo $feed_date; ?>" placeholder="dd-mm-yyyy hh:mm am" data-mask="99-99-9999 99:99 aa" name="pub_date">
                                </td>
                            </tr>



                            <tr>
                                <td>Url</td>
                                <td>
                                    <input id="feed_url" class="form-control" type="text" value="<?php echo $feed_url; ?>" placeholder="Feed URL" name="feed_url">

                                </td>
                            </tr>

                            <!--  For English  -->
                            <tr>
                                <th colspan="3" width="150px">English</th>                                
                            </tr>
                            <tr>
                                <td width="150px">Title</td>
                                <td>
                                    <input id="feed_title" class="form-control" type="text" value="<?php echo $feed_title; ?>" placeholder="English Feed Title" name="feed_title">                                    

                                </td>
                                <td>
                                    <input id="date_english" class="form-control" type="text" value="<?php echo $date_english; ?>" placeholder="dd mm. YYYY" name="date_english">
                                </td>
                            </tr>
                            
                            <tr>
                                <td>English Content</td>
                                <td>
                                    <textarea id="feed_desc" class="form-control" placeholder="Description" name="feed_desc"><?php echo $feed_desc; ?></textarea>
                                </td>
                                <td rowspan="2">
                                    <span class="fileupload-preview" style="margin-top: 10px;">
                                        <img src="media/<?php echo $feed_img; ?>" alt="" border="0" style="display:block; width: 200px; " />
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td>Image</td>
                                <td>
                                    <div class="col-lg-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-file btn-default">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>                                                  
                                                <input type="file" name="feed_img" />
                                            </span>
                                            <span class="fileupload-preview" style="margin-top: 10px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">x</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <!--  For Hindi  -->
       
        
        
                            <tr>
                                <th colspan="3" width="150px">Hindi</th>                                
                            </tr>
                            <tr>
                                <td width="150px">Title</td>
                                <td>
                                    <input id="feed_title_hindi" class="form-control" type="text" value="<?php echo $title_hindi; ?>" placeholder="Hindi Feed Title" name="feed_title_hindi">                                    

                                </td>
                                <td>
                                    <input id="date_hindi" class="form-control" type="text" value="<?php echo $date_hindi; ?>" placeholder="dd-mm-yyyy"  name="date_hindi">
                                </td>
                            </tr>
                            <tr>
                                <td>Hindi Content</td>
                                <td>
                                    <textarea id="feed_desc_hindi" class="form-control" placeholder="Hindi Description" name="feed_desc_hindi"><?php echo $desc_hindi; ?></textarea>
                                </td>
                                <td rowspan="2">
                                    <span class="fileupload-preview" style="margin-top: 10px;">
                                        <img src="media/<?php echo $img_hindi; ?>" alt="" border="0" style="display:block; width: 200px; " />
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td>Image</td>
                                <td>
                                    <div class="col-lg-8">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-file btn-default">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>                                                  
                                                <input type="file" name="feed_img_hindi" />
                                            </span>
                                            <span class="fileupload-preview" style="margin-top: 10px;"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">x</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>







                            </tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>



    </div>

</div>



<div class="form-actions" style="text-align:center">
    <input class="btn btn-primary btn-lg" type="button" id="multi-post" value="Save Changes" name="submit" >
    <!-- <input  type="button" id="multi-post" value="Run Code"></input> -->
</div>


