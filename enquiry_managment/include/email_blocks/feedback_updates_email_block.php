<?php
/*
$cust_order_no = 100001152;
$cust_name = "Ninad Tilkar";
$cust_phone_1 = "8108869247";
$cust_pooja_name = "Griha Pravesh Puja + Havan";
$cust_puja_date = "03-Mar-2016";
$cust_puja_address = "197, 1st Floor, 4th Main, Near Vinayaka Bakery Bangalore, Karnataka, 560066 India ";


$pandit_rating = "4";
$samagri_rating = "3";
$backend_rating = "4";

$user_commnets = "user_commnets 197, 1st Floor, 4th Main, Near Vinayaka Bakery Bangalore, Karnataka, 560066 India ";
$executives_comments = "executives_comments 197, 1st Floor, 4th Main, Near Vinayaka Bakery Bangalore, Karnataka, 560066 India ";

$feedback_file = "executives_comments";
*/


$feedback_updates_template = '
    
<table width="100%" cellspacing="0" cellpadding="0" border="0" background="images/background.jpg" style="background-image: url(images/background.jpg); background-position: 0 0; background-repeat: no-repeat repeat-y; background-color: #fcfcfc">
    <tbody><tr>
            <td valign="top" align="center">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                    <tbody><tr>
                            <td align="center">
                                <div style="font-size:0pt; line-height:0pt; height:12px"></div>

                                <table width="620" cellspacing="0" cellpadding="0" border="0">
                                    <tbody><tr>
                                            <td style="font-size:0pt; line-height:0pt; text-align:left" class="img">
                                                <a target="_blank" href="https://www.wheresmypandit.com">
                                                    <img width="128" height="51" border="0" alt="" src="https://www.wheresmypandit.com/media/logo/default/logo.png"></a></td>
                                            <td align="right">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tbody><tr>

                                                            <td style="color:#82837a; font-family:Trebuchet MS; font-size:11px; line-height:15px; text-align:left" class="top"><a href="#" target="_blank" style="color:#3ec7f3; text-decoration:none" class="link-top">&nbsp;</a></td>


                                                        </tr>
                                                    </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                <div style="font-size:0pt; line-height:0pt; height:5px">
                                </div>

                            </td>
                        </tr>
                    </tbody></table>


                <div style="font-size:0pt; line-height:0pt; height:5px; background:#ffffff; "></div>
                <div style="font-size:0pt; line-height:0pt; height:1px; background:#e1e1e1; "></div>
                <div style="font-size:0pt; line-height:0pt; height:1px; background:#e8e8e8; "></div>
                <div style="font-size:0pt; line-height:0pt; height:1px; background:#f4f4f4; "></div>
                <div style="font-size:0pt; line-height:0pt; height:1px; background:#f8f8f8; "></div>
                <div style="font-size:0pt; line-height:0pt; height:40px"></div>

                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                            <td align="center">
                                <table width="620" cellspacing="0" cellpadding="0" border="0">
                                    <tbody><tr>
                                            <td width="20" style="font-size:0pt; line-height:0pt; text-align:left" class="img"></td>
                                            <td width="360" valign="top" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:20px; text-align:left" class="text">
                                                <div>
                                                    <div style="color:#B28F76; font-family:Trebuchet MS; font-size:22px; line-height:26px; text-align:left; font-weight:bold" class="h2-blue"><div>Feedback Updates For Order No #' . $cust_order_no . ' </div></div>
                                                    <div style="font-size:0pt; line-height:0pt; height:10px"></div>
                                                    <div style="font-size:0pt; line-height:0pt; text-align:left" class="img"></div>
                                                    <div style="font-size:0pt; line-height:0pt; height:15px"></div>

                                                    <div>
                                                        <div class="h2" style="color:#b28f76; font-family:Trebuchet MS; font-size:20px; line-height:24px; text-align:left; font-weight:normal">
                                                            <div>Customer Details</div>
                                                        </div>
                                                        <div style="font-size:0pt; line-height:0pt; height:12px"> </div>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" width="35%"><div>Name </div></td>
                                                                <td bgcolor="#f5f5f5" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . $cust_name . '</div></td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" ><div>Contact No</div></td>

                                                                <td bgcolor="#ffffff" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . $cust_phone_1 . '</div></td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" width="35%"><div>Puja Name</div></td>
                                                                <td bgcolor="#f5f5f5" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . $cust_pooja_name . '</div></td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" ><div>Puja Date</div></td>

                                                                <td bgcolor="#ffffff" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . $cust_puja_date . '</div></td>
                                                            </tr>

                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" width="35%">
                                                                    <div valign="top">Puja Address </div>
                                                                </td>
                                                                <td bgcolor="#f5f5f5" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px">
                                                                    <div>' . $cust_puja_address . '</div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div style="font-size:0pt; line-height:0pt; height:10px"></div>
                                                    <div style="font-size:0pt; line-height:0pt; text-align:left" class="img"></div>
                                                    <div style="font-size:0pt; line-height:0pt; height:15px"></div>

                                                    <div>
                                                        <div class="h2" style="color:#b28f76; font-family:Trebuchet MS; font-size:20px; line-height:24px; text-align:left; font-weight:normal">
                                                            <div>Feedback Details</div>
                                                        </div>
                                                        <div style="font-size:0pt; line-height:0pt; height:12px"> </div>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" width="35%"><div> Pandit </div></td>
                                                                <td bgcolor="#f5f5f5" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . ($pandit_rating/2) . '</div></td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" ><div> Samagri</div></td>

                                                                <td bgcolor="#ffffff" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . ($samagri_rating/2) . '</div></td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" width="35%"><div> Our Team </div></td>
                                                                <td bgcolor="#f5f5f5" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . ($backend_rating/2) . '</div></td>
                                                            </tr>
                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" ><div>Customer FeedBack</div></td>

                                                                <td bgcolor="#ffffff" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . $user_commnets . '</div></td>
                                                            </tr>

                                                            <tr>
                                                                <td bgcolor="#f5f5f5" class="td-first" style="color:#7d7d7d; font-family:Trebuchet MS; font-size:17px; line-height:16px; text-align:left; padding:5px 0 5px 15px; border-right:1px solid #e9e9e9" width="35%"><div>Executive feedback </div></td>
                                                                <td bgcolor="#f5f5f5" class="td" style="color:#7d7d7d; font-family:Arial; font-size:15px; line-height:16px; text-align:left; padding:5px 0 5px 15px"><div>' . $executives_comments . '</div></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div style="font-size:0pt; line-height:0pt; height:40px"></div>
                                                </div>
                                            </td>
                                            <td width="31" style="font-size:0pt; line-height:0pt; text-align:left" class="img"></td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>

                <div style="font-size:0pt; line-height:0pt; height:35px"></div>


            </td>
        </tr>
    </tbody></table>
    
    
    
    
    
    ';
//echo $no_contact_template;
?>






