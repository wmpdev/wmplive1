<?php
include_once '../actions/feedback_update.php';

function checkRatting($Menu_name, $Main_Menu) {
    if (isset($Main_Menu) && ($Main_Menu == $Menu_name)) {
        echo "checked";
        //$Sub_Menu_Status = "in";
        //return 1;
    }
    //echo " Main_Menu = $Main_Menu and  = Menu_name = $Menu_name";
    //return 0;    
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Wheresmypandit &amp; Feedback Form</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="https://www.wheresmypandit.com/skin/frontend/nextlevel/default/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">


        <style>
            /* Base setup */
            @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
            body {
                margin: 0 5%; 
                text-align: center;
                //background: #111;
                //color: #ddd; //#333; #ddd
            }
            h1 {
                font-size: 2em; 
                margin-bottom: .5rem;
            }

            /* Ratings widget */
            .rate {
                display: inline-block;
                border: 0;
            }
            /* Hide radio */
            .rate > input {
                display: none;
            }
            /* Order correctly by floating highest to the right */
            .rate > label {
                float: right;
            }
            /* The star of the show */
            .rate > label:before {
                display: inline-block;
                font-size: 2.1rem;
                padding: .3rem .2rem;
                margin: 0;
                cursor: pointer;
                font-family: FontAwesome;
                content: "\f005 "; /* full star */
            }
            /* Zero stars rating */
            .rate > label:last-child:before {
                content: "\f006 "; /* empty star outline */
            }
            /* Half star trick */
            .rate .half:before {
                content: "\f089 "; /* half star no outline */
                position: absolute;
                padding-right: 0;
            }
            /* Click + hover color */
            input:checked ~ label, /* color current and previous stars on checked */
            label:hover, label:hover ~ label { color: #f7a61f;   } /* color previous stars on hover */  //#73B100;

            /* Hover highlights */
            input:checked + label:hover, input:checked ~ label:hover, /* highlight current and previous stars */
            input:checked ~ label:hover ~ label, /* highlight previous selected stars for new rating */
            label:hover ~ input:checked ~ label /* highlight previous selected stars */ { color: #A6E72D;  } //#A6E72D;

        </style>        



    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong><a href="#" style="color: #fff;">Where's My Pandit</a></strong> - Feedback Form </h1>
                            <div class="description">
                                <p>
                                    <!--
                                    This is a free responsive <strong>"login and register forms"</strong> template made with Bootstrap. 
                                    Download it on <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a>, 
                                    customize and use it as you like!
                                    -->
                                    
                                    Help us improve upon our services with your <strong>"valuable Feedback here"</strong>. Kindly give us star ratings from <strong>"0-5"</strong> on the below mentioned parameters and <strong>"write us"</strong> an apt testimonial.
                                    
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">

                            <div class="description">
                                <p>
                                    <?php
                                    if(strlen($error_msg) > 0){
                                        echo $error_msg;
                                    }
                                    
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-sm-5">

                            <div class="form-box">
                                <div class="form-top">
                                    <div class="form-top-left">
                                        <h3>Rate us </h3>
                                        <p>on the following parameters:</p>
                                    </div>
                                    <div class="form-top-right">
                                        <i class="fa fa-pencil"></i>
                                    </div>
                                </div>
                                <div class="form-bottom">
                                    <form role="form" action="" method="post" class="login-form" enctype="multipart/form-data">

                                        <input type="hidden" id="Action" name="Action" value="ADD" />

                                        <div  style="text-align: left;float: left;">
                                            <label for="form-username">Pandit</label>
                                        </div>
                                        <div style="text-align: right;">
                                            <fieldset class="rate">
                                                <input type="radio" id="pandit_rating10" name="pandit_rating" value="10" <?php checkRatting("10", $pandit_rating); ?>/>
                                                <label for="pandit_rating10" title="5 stars"></label>
                                                <input type="radio" id="pandit_rating9" name="pandit_rating" value="9" <?php checkRatting("9", $pandit_rating); ?> />
                                                <label class="half" for="pandit_rating9" title="4 1/2 stars"></label>
                                                <input type="radio" id="pandit_rating8" name="pandit_rating" value="8" <?php checkRatting("8", $pandit_rating); ?> />
                                                <label for="pandit_rating8" title="4 stars"></label>
                                                <input type="radio" id="pandit_rating7" name="pandit_rating" value="7" <?php checkRatting("7", $pandit_rating); ?> />
                                                <label class="half" for="pandit_rating7" title="3 1/2 stars"></label>
                                                <input type="radio" id="pandit_rating6" name="pandit_rating" value="6" <?php checkRatting("6", $pandit_rating); ?> />
                                                <label for="pandit_rating6" title="3 stars"></label>
                                                <input type="radio" id="pandit_rating5" name="pandit_rating" value="5" <?php checkRatting("5", $pandit_rating); ?>/>
                                                <label class="half" for="pandit_rating5" title="2 1/2 stars"></label>
                                                <input type="radio" id="pandit_rating4" name="pandit_rating" value="4" <?php checkRatting("4", $pandit_rating); ?>/>
                                                <label for="pandit_rating4" title="2 stars"></label>
                                                <input type="radio" id="pandit_rating3" name="pandit_rating" value="3" <?php checkRatting("3", $pandit_rating); ?>/>
                                                <label class="half" for="pandit_rating3" title="1 1/2 stars"></label>
                                                <input type="radio" id="pandit_rating2" name="pandit_rating" value="2" <?php checkRatting("2", $pandit_rating); ?>/>
                                                <label for="pandit_rating2" title="1 star"></label>
                                                <input type="radio" id="pandit_rating1" name="pandit_rating" value="1" <?php checkRatting("1", $pandit_rating); ?>/>
                                                <label class="half" for="pandit_rating1" title="1/2 star"></label>
                                                <input type="radio" id="pandit_rating0" name="pandit_rating" value="0" <?php checkRatting("0", $pandit_rating); ?>/>
                                                <label for="pandit_rating0" title="No star"></label>
                                            </fieldset>
                                        </div>


                                        <div  style="text-align: left;float: left;">
                                            <label for="form-username">Samagri</label>
                                        </div>
                                        <div style="text-align: right;">
                                            <fieldset class="rate">
                                                <input type="radio" id="samagri_rating10" name="samagri_rating" value="10"  <?php checkRatting("10", $samagri_rating); ?>  />
                                                <label for="samagri_rating10" title="5 stars"></label>
                                                <input type="radio" id="samagri_rating9" name="samagri_rating" value="9"  <?php checkRatting("9", $samagri_rating); ?> />
                                                <label class="half" for="samagri_rating9" title="4 1/2 stars"></label>
                                                <input type="radio" id="samagri_rating8" name="samagri_rating" value="8"  <?php checkRatting("8", $samagri_rating); ?> />
                                                <label for="samagri_rating8" title="4 stars"></label>
                                                <input type="radio" id="samagri_rating7" name="samagri_rating" value="7"  <?php checkRatting("7", $samagri_rating); ?> />
                                                <label class="half" for="samagri_rating7" title="3 1/2 stars"></label>
                                                <input type="radio" id="samagri_rating6" name="samagri_rating" value="6"  <?php checkRatting("6", $samagri_rating); ?> />
                                                <label for="samagri_rating6" title="3 stars"></label>
                                                <input type="radio" id="samagri_rating5" name="samagri_rating" value="5"  <?php checkRatting("5", $samagri_rating); ?> />
                                                <label class="half" for="samagri_rating5" title="2 1/2 stars"></label>
                                                <input type="radio" id="samagri_rating4" name="samagri_rating" value="4"  <?php checkRatting("4", $samagri_rating); ?> />
                                                <label for="samagri_rating4" title="2 stars"></label>
                                                <input type="radio" id="samagri_rating3" name="samagri_rating" value="3"  <?php checkRatting("3", $samagri_rating); ?> />
                                                <label class="half" for="samagri_rating3" title="1 1/2 stars"></label>
                                                <input type="radio" id="samagri_rating2" name="samagri_rating" value="2"  <?php checkRatting("2", $samagri_rating); ?> />
                                                <label for="samagri_rating2" title="1 star"></label>
                                                <input type="radio" id="samagri_rating1" name="samagri_rating" value="1"  <?php checkRatting("1", $samagri_rating); ?> />
                                                <label class="half" for="samagri_rating1" title="1/2 star"></label>
                                                <input type="radio" id="samagri_rating0" name="samagri_rating" value="0"  <?php checkRatting("0", $samagri_rating); ?> />
                                                <label for="samagri_rating0" title="No star"></label>
                                            </fieldset>
                                        </div>


                                        <div  style="text-align: left;float: left;">
                                            <label for="form-username">Our Team</label>
                                        </div>
                                        <div style="text-align: right;">
                                            <fieldset class="rate">
                                                <input type="radio" id="backend_rating10" name="backend_rating" value="10"  <?php checkRatting("10", $backend_rating); ?> />
                                                <label for="backend_rating10" title="5 stars"></label>
                                                <input type="radio" id="backend_rating9" name="backend_rating" value="9"  <?php checkRatting("9", $backend_rating); ?> />
                                                <label class="half" for="backend_rating9" title="4 1/2 stars"></label>
                                                <input type="radio" id="backend_rating8" name="backend_rating" value="8"  <?php checkRatting("8", $backend_rating); ?> />
                                                <label for="backend_rating8" title="4 stars"></label>
                                                <input type="radio" id="backend_rating7" name="backend_rating" value="7"  <?php checkRatting("7", $backend_rating); ?> />
                                                <label class="half" for="backend_rating7" title="3 1/2 stars"></label>
                                                <input type="radio" id="backend_rating6" name="backend_rating" value="6"  <?php checkRatting("6", $backend_rating); ?> />
                                                <label for="backend_rating6" title="3 stars"></label>
                                                <input type="radio" id="backend_rating5" name="backend_rating" value="5"  <?php checkRatting("5", $backend_rating); ?> />
                                                <label class="half" for="backend_rating5" title="2 1/2 stars"></label>
                                                <input type="radio" id="backend_rating4" name="backend_rating" value="4"  <?php checkRatting("4", $backend_rating); ?> />
                                                <label for="backend_rating4" title="2 stars"></label>
                                                <input type="radio" id="backend_rating3" name="backend_rating" value="3"  <?php checkRatting("3", $backend_rating); ?> />
                                                <label class="half" for="backend_rating3" title="1 1/2 stars"></label>
                                                <input type="radio" id="backend_rating2" name="backend_rating" value="2"  <?php checkRatting("2", $backend_rating); ?> />
                                                <label for="backend_rating2" title="1 star"></label>
                                                <input type="radio" id="backend_rating1" name="backend_rating" value="1"  <?php checkRatting("1", $backend_rating); ?>/>
                                                <label class="half" for="backend_rating1" title="1/2 star"></label>
                                                <input type="radio" id="backend_rating0" name="backend_rating" value="0"  <?php checkRatting("0", $backend_rating); ?> />
                                                <label for="backend_rating0" title="No star"></label>
                                            </fieldset>
                                        </div>                                        



                                        <div class="form-group">
                                            <label class="sr-only" for="form-about-yourself">Write us an appropriate Testimonial</label>
                                            <textarea name="user_commnets" placeholder="Write us an appropriate Testimonial" 
                                                      class="form-about-yourself form-control" id="user_commnets" ><?php echo $user_commnets; ?></textarea>
                                        </div>



                                        <?php if ($_SESSION["login_id"]) { ?>
                                            <div class="form-group">
                                                <label class="sr-only" for="form-about-yourself">Executives Comments</label>
                                                <textarea name="executives_comments" placeholder="Executives Comments" 
                                                          class="form-about-yourself form-control" id="executives_comments"><?php echo $executives_comments; ?></textarea>
                                            </div>

                                            <div class="col-lg-8">
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <span class="btn btn-file btn-default">

                                                        <span class="fileupload-exists"> 
                                                            <?php 
                                                            if(strlen($feedback_file)>0){
                                                                echo "File :- ".$feedback_file;  
                                                            }
                                                            else{
                                                                echo "No File Present.";  
                                                            }
                                                            
                                                            ?> 
                                                        </span>                                                  
                                                        <input type="file" name="feed_img" />
                                                    </span>
                                                    <span class="fileupload-preview" style="margin-top: 10px;"></span>
                                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">x</a>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <button type="submit" class="btn">Submit</button>
                                    </form>
                                </div>
                            </div>



                        </div>

                        <div class="col-sm-1 middle-border"></div>
                        <div class="col-sm-1"></div>

                        <div class="col-sm-5">

                            <div class="form-box">
                                <div class="form-top">
                                    <div class="form-top-left">
                                        <h3>Customer Details</h3>
                                        <p>Verify Your Details</p>
                                    </div>
                                    <div class="form-top-right">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                <div class="form-bottom">

                                    <div class="form-group">
                                        <div style="text-align: right;">
                                            <span style="font-size: 14px;font-weight:500;">Order NO :</span>
                                            <?php echo "#" . $result_arr[0]["order_no"]; ?>
                                        </div>

                                        <div style="text-align: left;">
                                            <span style="font-size: 14px;font-weight:500;">Name : </span>
                                            <?php echo $result_arr[0]["first_name"] . " " . $result_arr[0]["last_name"]; ?>
                                        </div>
                                        <div style="text-align: left;">
                                            <span style="font-size: 14px;font-weight:500;">Contact No : </span><?php echo $result_arr[0]["phone_1"]; ?>
                                        </div>
                                        <div style="text-align: left;">
                                            <span style="font-size: 14px;font-weight:500;">Puja Name : </span><?php echo $result_arr[0]["pooja_name"]; ?>
                                        </div>
                                        <div style="text-align: left;">
                                            <span style="font-size: 14px;font-weight:500;">Puja Date : </span><?php echo date('d-M-Y', strtotime($result_arr[0]["puja_date"])); ?>
                                        </div> 
                                        <div style="text-align: left;">
                                            <span style="font-size: 14px;font-weight:500;">Puja Address : </span><br/>
                                            <?php echo $result_arr[0]["puja_address"]; ?>
                                        </div>

                                        <!--
                                        <input type="text" name="form-first-name" placeholder="First name..." class="form-first-name form-control" id="form-first-name"> -->
                                    </div>
                                    <!--
                                    <div class="form-group">
                                        <label class="sr-only" for="form-last-name">Last name</label>
                                        <input type="text" name="form-last-name" placeholder="Last name..." class="form-last-name form-control" id="form-last-name">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-email">Email</label>
                                        <input type="text" name="form-email" placeholder="Email..." class="form-email form-control" id="form-email">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-about-yourself">About yourself</label>
                                        <textarea name="form-about-yourself" placeholder="About yourself..." 
                                                  class="form-about-yourself form-control" id="form-about-yourself"></textarea>
                                    </div>
                                    <button type="submit" class="btn">Sign me up!</button>
                                    -->
                                    <form role="form" action="" method="post" class="registration-form"></form>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">

                    <div class="col-sm-8 col-sm-offset-2">

                        <div class="footer-border"></div>
                        <div class="social-login">
                            <h3>Connect with Us:</h3>
                            <div class="social-login-buttons">
                                <a class="btn btn-link-1 btn-link-1-facebook" href="https://www.facebook.com/wheresmypandit">
                                    <i class="fa fa-facebook"></i> Facebook
                                </a>
                                <a class="btn btn-link-1 btn-link-1-twitter" href="https://twitter.com/wheresmypandit">
                                    <i class="fa fa-twitter"></i> Twitter
                                </a>
                                <a class="btn btn-link-1 btn-link-1-twitter" style="background: #3f729b none repeat scroll 0 0" href="https://www.instagram.com/wheresmypandit/">
                                    <i class="fa fa-instagram"></i> Instagram
                                </a>
                                <!--
                                <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                                    <i class="fa fa-google-plus"></i> Google Plus
                                </a>
                                -->
                            </div>
                        </div>
                        <!--
                        <p>Made by Anli Zaimi at <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a> 
                            having a lot of fun. <i class="fa fa-smile-o"></i></p>
                        -->
                    </div>

                </div>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>