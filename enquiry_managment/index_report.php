<?php
$Main_Menu = "Dashboard";

$page_title = "Admin Dashboard";  //Admin Dashboard Template | Blank Page


include_once 'config/auth.php';


$date = strtotime(date("Y-m-d"));
$day_1 = date("d-m-Y", $date);

$datetime = new DateTime('tomorrow');
//echo "<br/>tomorrow = ".$datetime->format('Y-m-d');
$date_2 = $datetime->format('d-m-Y');

if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    //$start_date = date("Y-m-d", strtotime($start_date));

    if (strlen($start_date) == 10) {
        $start_date = date("Y-m-d", strtotime($start_date));
    }
} else {
    $start_date = "";
    $start_date = date("Y-m-d", strtotime($day_1));  //$date;
}
if (isset($_GET["to_date"])) {
    $end_date = $_GET["to_date"];
    if (strlen($end_date) == 10) {
        $end_date = date("Y-m-d", strtotime($end_date));
    } else {
        $end_date = "";
    }
} else {
    $end_date = "";
}

list($status, $from_date, $to_date, $msg) = setFromToDate($start_date, $end_date);

if ($status == 0) {
    $msg = "Please Select Proper Date For Followup.";
    //return array($status, $id, $msg);    
}

if ($status == 1) {

    $query = "SELECT e.enq_no,e.cust_id,e.enq_date,e.quotation_id,e.enq_status e_status,e.followup_date, f.id,f.enq_id,f.add_enq_date,f.next_date,f.enq_status
            FROM tbl_enquiry e  inner join  `tbl_enq_followup` f 
            on (f.next_date BETWEEN '$from_date' and '$to_date' )
            and f.enq_id=e.id
            order by f.next_date desc";

//$query = "SELECT * from tbl_enquiry";
    list($status, $id, $result_arr) = getDataByQuery($query);
//var_dump($result_arr);




    $enq_no_arr = array();
    for ($i = 0; $i < count($result_arr); $i++) {

        $enq_status_id = $result_arr[$i]["enq_no"];
        //echo "<br/> enq No = $enq_status_id";


        if (isset($enq_no_arr[$enq_status_id][0])) {
            $value = $enq_no_arr[$enq_status_id][0];
            $value++;
            $enq_no_arr[$enq_status_id][0] = $value;

            $enq_no_arr[$enq_status_id][1] = $enq_no_arr[$enq_status_id][1] . "," . $result_arr[$i]["enq_no"];
            $result_arr[$i] = array();
        } else {
            $enq_no_arr[$enq_status_id][0] = 1;
            $enq_no_arr[$enq_status_id][1] = $result_arr[$i]["enq_no"];
        }
    }

    //echo "<pre>";
    //print_r($enq_no_arr);
    //echo "</pre> count = [" . count($enq_no_arr) . "]";



    $new_enq = 0;
    $followup_count = 0;
    $enq_status_id = 0;
    $enq_status = array();
    $followup_status = array('done' => 0, 'critcal' => 0, 'pending' => 0, 'cancel' => 0, 'close' => 0);
    $followup_status_data = array();
//var_dump($followup_status);
    for ($i = 0; $i < count($result_arr); $i++) {

        if (isset($result_arr[$i]["e_status"])) {

            $enq_new = FALSE;
            
            
            $followup_count++;
            if ((date("d-m-Y", strtotime($result_arr[$i]["enq_date"])) >= date("d-m-Y", strtotime($from_date))) && (date("d-m-Y", strtotime($result_arr[$i]["enq_date"])) <= date("d-m-Y", strtotime($to_date)))  ) {
                $new_enq++;
                $enq_new = TRUE;
            }

            $enq_status_id = $result_arr[$i]["e_status"];
            if (isset($enq_status[$enq_status_id])) {
                $value = $enq_status[$enq_status_id];
                $value++;
                $enq_status[$enq_status_id] = $value;
            } else {
                $enq_status[$enq_status_id] = 1;
            }



            //echo "<br/>" . date("d-m-Y", strtotime($result_arr[$i]["followup_date"])) . " = " . date("d-m-Y", strtotime("+1 day", $date));

            if (strtotime($result_arr[$i]["followup_date"]) > strtotime($result_arr[$i]["next_date"])) {
                $followup_status_code = "Done";
                $followup_status['done'] = $followup_status['done'] + 1;

                $followup_status_data[0][0] = "Done";
                $followup_status_data[0][1] = $followup_status['done'];
                $followup_status_data[0][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
            } elseif (strtotime($result_arr[$i]["followup_date"]) < strtotime("+1 day", $date)) {
                if ($enq_status_id <> 5 && $enq_status_id <> 8) {


                    $followup_status['pending'] = $followup_status['pending'] + 1;
                    //echo "<br/> I am in ";
                    $followup_status_data[2][0] = "Pending";
                    $followup_status_data[2][1] = $followup_status['pending'];
                    $followup_status_data[2][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
                }
            }




            /*
              elseif (strtotime(date("Y-m-d H:i:s")) > strtotime($result_arr[$i]["followup_date"])) {
              $followup_status_code = "Critical";
              $followup_status['critcal'] = $followup_status['critcal'] + 1;
              //$followup_status['pending'] = $followup_status['pending'] + 1;

              $followup_status_data[1][0] = "Critical";
              $followup_status_data[1][1] = $followup_status['critcal'];
              $followup_status_data[1][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
              } elseif (strtotime($result_arr[$i]["followup_date"]) == strtotime($result_arr[$i]["next_date"])) {
              $followup_status_code = "";
              $followup_status['pending'] = $followup_status['pending'] + 1;

              $followup_status_data[2][0] = "Pending";
              $followup_status_data[2][1] = $followup_status['pending'];
              $followup_status_data[2][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
              }

             */

            /*
              if ($followup_status_code == "") {
              if (strtotime($result_arr[$i]["enq_date"]) >= strtotime(date("Y-m-d"))) {
              if (strtotime($result_arr[$i]["followup_date"]) > strtotime($result_arr[$i]["enq_date"])) {
              //$followup_status = "Done";
              $followup_status['done'] = $followup_status['done'] + 1;

              $followup_status_data[3][0] = "Done";
              $followup_status_data[3][1] = $followup_status['done'];
              $followup_status_data[3][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
              }
              }
              }
             */
            if ($enq_status_id == 5) {
                //$followup_status_code = "Cancel";
                $followup_status['cancel'] = $followup_status['cancel'] + 1;

                $followup_status_data[4][0] = "Cancel";
                $followup_status_data[4][1] = $followup_status['cancel'];
                $followup_status_data[4][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
            }

            if ($enq_status_id == 8) {
                //$followup_status_code = "Close";
                $followup_status['close'] = $followup_status['close'] + 1;

                $followup_status_data[5][0] = "Close";
                $followup_status_data[5][1] = $followup_status['close'];
                $followup_status_data[5][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
            }
        }
    }

    //echo "<pre>" . print_r($followup_status_data,true) . "</pre>";
//echo "<br/>Total = $followup_count , New Enq = $new_enq";
    //var_dump($followup_status_data);
//var_dump($enq_status);
//var_dump($followup_status);

    $query = "SELECT id,status_name from tbl_status_master where rec_type = 1";
    list($status, $id, $status_arr) = getDataByQuery($query);

    for ($i = 0; $i < count($status_arr); $i++) {

        $enq_status_id = $status_arr[$i]["id"];
        if (isset($enq_status[$enq_status_id])) {
            $status_arr[$i]['count'] = $enq_status[$enq_status_id];
        } else {
            $enq_status[$enq_status_id] = 1;
            $status_arr[$i]['count'] = 0;
        }
    }
}





//var_dump($_SESSION);
//var_dump($status_arr);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner" style="min-height:1200px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Welcome  <?php echo $_SESSION['login_name']; ?>  </h2>
                        </div>
                    </div>

                    <hr />





                    <!-- COMMENT AND NOTIFICATION  SECTION -->
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="box">
                                <input type="hidden" name="quotation_add_enq_id" placeholder="Enquiry Number " value="521" class="form-control" id="quotation_add_enq_id">
                                <input type="hidden" name="ACTION" placeholder="Enquiry Number " value="AddQuotation" class="form-control" id="ACTION">
                                <div id="sortableTable" class="body collapse in">
                                    <table class="table table-bordered sortableTable responsive-table">

                                        <tbody>
                                            <tr><td colspan="3">
                                                    <span class="pull-right text-muted small">
                                                        <em><a href="send_email.php?rec_type=daily_report_1&from_date=<?php echo $date_2; ?>&tag=order_puja_details" ><?php echo "Email Tommorow Puja"; ?></a></em>
                                                    </span>
                                                </td></tr>
                                            <tr>


                                                <td style="text-align: center;">                                                    

                                                    <h2>
                                                        <img class="pull-center img-circle" alt="Total Followup" src="assets/img/dashboard_img/add_user_plus_male.png" style="width:80px;">
                                                        <span id="td_sub_total_amount"><?php echo "  " . $new_enq ?></span>
                                                    </h2>
                                                    <small class="pull-center text-muted"> <h3>New Users</h3> </small> 
                                                </td>
                                                <td style="text-align: center;" colspan="2">                                                    

                                                    <h2>
                                                        <img class="pull-center img-circle" alt="Total Followup" src="assets/img/dashboard_img/add_user_group.png" style="width:80px;">
                                                        <span id="td_sub_total_amount"><?php echo "  " . $followup_count ?></span>
                                                    </h2>
                                                    <small class="pull-center text-muted"> <h3>Total Followups</h3> </small> 
                                                </td>


                                            </tr>

                                            <tr>
                                                <td style="text-align: center;">                                                    

                                                    <h2>
                                                        <img class="pull-center img-circle" alt="Total Followup" src="assets/img/dashboard_img/previous_user_man.png" style="width:80px;">
                                                        <span id="td_sub_total_amount"><?php echo "  " . ($followup_count - $new_enq); ?></span>
                                                    </h2>
                                                    <small class="pull-center text-muted"> <h3>Previous Users</h3> </small> 
                                                </td>



                                                <td style="text-align: center;">                                                    

                                                    <h2><?php echo $followup_status['done']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Done </small> </h4>
                                                    <br/>
                                                    <h2><?php echo $followup_status['cancel']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Cancel </small> </h4>
                                                </td>
                                                <td style="text-align: center;"> 
                                                    <h2><?php echo $followup_status['pending']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Pending </small> </h4>
                                                    <br/>
                                                    <h2><?php echo $followup_status['close']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Close </small> </h4>
                                                </td>

                                            </tr>



                                        </tbody>
                                    </table>





                                </div>
                            </div>

                        </div>



                        <div class="col-lg-5">

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <i class="icon-bell"></i> Status Report
                                    <span class="pull-right text-muted small">
                                        <em><a href="send_email.php?rec_type=daily_report_1&from_date=<?php echo $day_1; ?>&tag=order_puja_details" ><?php echo "Email"; ?></a></em>
                                    </span>
                                </div>

                                <div class="panel-body">
                                    <div class="list-group">
                                        <?php

                                        function sortByOrder($a, $b) {
                                            return $b['count'] - $a['count'];
                                        }

                                        usort($status_arr, 'sortByOrder');

                                        for ($i = 0; $i < count($status_arr); $i++) {
                                            $total_count = $total_count + $status_arr[$i]['count'];
                                            ?>
                                            <a href="#" class="list-group-item">
                                                <i class="icon-ok"></i> <?php echo $status_arr[$i]['status_name']; ?>
                                                <span class="pull-right text-muted small"><em><?php echo $status_arr[$i]['count']; ?></em>
                                                </span>
                                            </a>
                                        <?php } ?>




                                    </div>

                                    <a href="#" class="btn btn-default btn-block btn-primary"><?php echo "Total = $total_count"; ?></a>
                                </div>

                            </div>



                        </div>
                    </div>
                    <!-- END COMMENT AND NOTIFICATION  SECTION -->


                    <hr />
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>List Enquiry </h2>

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="box" id="order_tmp_list_msg" >
                                            <div class="col-lg-12">
                                                <!--
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
                                                </div>
                                                -->
                                            </div>
                                        </div>

                                        <div class="panel-body">                                            
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="font-size: 12px !important;">
                                                    <thead>
                                                        <tr>

                                                            <th style="width: 35px">No</th>
                                                            <th style="width: 70px">Enquiry</th>
                                                            <th style="width: 70px">Date </th>
                                                            <th>Customer Name</th>
                                                            <th>Area </th>
                                                            <th>City </th>
                                                            <th>Follow up </th>
                                                            <th>Status </th>

<!--<th>Reference</th>
<th>Cast</th>
<th>Country</th> -->

                                                            <th style="width:105px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        $count = 0;
//while ($row = $stmt->fetch_assoc()) {
                                                        for ($i = 0; $i < count($result_arr); $i++) {

                                                            $count++;
                                                            $enq_id = $result_arr[$i]['id'];
                                                            if (strlen($result_arr[$i]["enq_date"]) > 18) {
                                                                list($enq_date, $enq_time) = validateDbDateTime($result_arr[$i]["enq_date"]);
                                                                $enq_date = date('Y-m-d', strtotime($enq_date));
                                                                $enq_date = $enq_date . "<br/>" . $enq_time;
                                                            } else {
                                                                $enq_date = "";
                                                            }

                                                            $cust_id = $result_arr[$i]['cust_id'];
                                                            list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);
                                                            if ($cust_status == 1) {
                                                                $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
                                                                $cust_contact = "<br/> " . $cust_data_arr["phone_1"] . " , " . $cust_data_arr["phone_2"];
                                                                $cust_contact .= "<br/>" . $cust_data_arr["email_1"];

                                                                $cust_city = $cust_data_arr["city"];
                                                                $cust_area = $cust_data_arr["area"];
                                                            } else {
                                                                $cust_name = $result_arr[$i]['customer_name'] . "<br/>" . $result_arr[$i]['customer_contact'];
                                                                $cust_city = $result_arr[$i]["customer_city"];
                                                                $cust_area = $result_arr[$i]["customer_area"];
                                                            }

                                                            
                                                            $enq_status = getStatus(0, $result_arr[$i]['enq_status']); //$result_arr[$i]['order_status']; 
                                                            $enq_status = (isset($enq_status[0]["status_name"]) ? $enq_status[0]["status_name"] : $result_arr[$i]['enq_status']);

                                                            if (strlen($result_arr[$i]["followup_date"]) > 18) {
                                                                list($follow_up_date, $follow_up_time) = validateDbDateTime($result_arr[$i]["followup_date"]);
                                                                $follow_up_date = $follow_up_date . "<br/>" . $follow_up_time;
                                                            } else {
                                                                $follow_up_date = "";
                                                            }

                                                            //if()?  : ;
                                                            //list($puja_date, $puja_time) = validateDbDateTime($result_arr[$i]["puja_date1"]);
                                                            //$puja_date = $puja_date . " " . $puja_time;
                                                            ?> 
                                                            <tr class="odd gradeX">

                                                                <td><?php echo $count; ?></td>
                                                                <td>
                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>" target="_blank" Title = "Click Here to Edit" class="btn text-info btn-xs btn-flat"><?php echo $result_arr[$i]['enq_no']; ?></a>
                                                                </td>
                                                                <td><?php echo $enq_date; ?></td>

                                                                <td>
                                                                    <a href="add_user.php?edit_id=<?php echo $cust_id; ?>" target="_blank" Title = "Click Here to Edit" class="btn text-info btn-xs btn-flat"><?php echo $cust_name; ?></a>
                                                                    <?php echo $cust_contact; ?>
                                                                </td>


                                                                <td><?php echo $cust_area; ?></td>
                                                                <td><?php echo $cust_city; ?></td>
                                                                <td>
                                                                    <a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>&tab=Followup-Tab" target="_blank" Title = "Click Here to Followup" class="btn text-info btn-xs btn-flat">
                                                                        <?php echo $follow_up_date; ?></a>
                                                                </td> 

                                                                <td><?php echo $enq_status; ?></td> 
                                                                <td>
                                                                    <a href="add_enquiry.php?edit_id=<?php echo $result_arr[$i]['id']; ?>" class="btn text-info btn-xs btn-flat">
                                                                        <i class="icon-list-alt icon-white"></i> Edit</a>

                                                                    <!--
                                                                <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>,'SMS','info')" class="btn text-info btn-xs btn-flat">
                                                                    <i class="icon-list-alt icon-white"></i> Send SMS info</a>
                                                                    

                                                                    <?php if (strlen($result_arr[$i]['customer_contact']) > 8) { ?>
                                                                                                <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>,'SMS','Customer')" class="btn text-info btn-xs btn-flat">
                                                                                                <i class="icon-list-alt icon-white"></i> Send SMS Customer</a>
                                                                    <?php } ?>
                                                                    
                                                                    -->

                                                                    <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>, 'EMAIL', 'info')" class="btn text-info btn-xs btn-flat">
                                                                        <i class="icon-list-alt icon-white"></i> Email To Info</a>

                                                                    <?php if (strlen($result_arr[$i]['customer_email']) > 5) { ?>
                                                                        <a href="javascript:void(0);" onclick="send_Data(<?php echo $result_arr[$i]['id']; ?>, 'EMAIL', 'Customer')" class="btn text-info btn-xs btn-flat">
                                                                            <i class="icon-list-alt icon-white"></i> Email To Customer</a>
                                                                        <?php } ?>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>





                                                    </tbody>
                                                </table>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    
                    
                    
                    









                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';        ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        

    </body>
    <!-- END BODY-->

</html>



<?php


/*******EDIT LINES 3-8*******/
$DB_Server = "localhost"; //MySQL Server    
$DB_Username = "wheresmy_ninad"; //MySQL Username     
$DB_Password = "ninad123";             //MySQL Password     
$DB_DBName = "wheresmy_enq_ninad";         //MySQL Database Name  
$DB_TBLName = "tbl_status_master"; //MySQL Table Name   
$filename = "tbl_status_master";         //File Name
/*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/    
//create MySQL connection   
$sql = "Select * from $DB_TBLName";
$Connect = @mysql_connect($DB_Server, $DB_Username, $DB_Password) or die("Couldn't connect to MySQL:<br>" . mysql_error() . "<br>" . mysql_errno());
//select database   
$Db = @mysql_select_db($DB_DBName, $Connect) or die("Couldn't select database:<br>" . mysql_error(). "<br>" . mysql_errno());   
//execute query 
$result = @mysql_query($sql,$Connect) or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());    
$file_ending = "xls";
//header info for browser
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=$filename.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
/*******Start of Formatting for Excel*******/   
//define separator (defines columns in excel & tabs in word)
$sep = "\t"; //tabbed character
//start of printing column names as names of MySQL fields
for ($i = 0; $i < mysql_num_fields($result); $i++) {
echo mysql_field_name($result,$i) . "\t";
}
print("\n");    
//end of printing column names  
//start while loop to get data
    while($row = mysql_fetch_row($result))
    {
        $schema_insert = "";
        for($j=0; $j<mysql_num_fields($result);$j++)
        {
            if(!isset($row[$j]))
                $schema_insert .= "NULL".$sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]".$sep;
            else
                $schema_insert .= "".$sep;
        }
        $schema_insert = str_replace($sep."$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        print(trim($schema_insert));
        print "\n";
    }   



?>