<?php
$Main_Menu = "Order";
$Sub_Menu_1 = "list_order_puja_form_temp";

$page = "Enquiry Followup";
$page_title = "List Order Puja";
$edit_id = 0;

//print_r($_GET);


include_once 'config/auth.php';

$date = strtotime(date("Y-m-d"));
$day_1 = date("d-m-Y", $date);

if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    //$start_date = date("Y-m-d", strtotime($start_date));

    if (strlen($start_date) == 10) {
        $start_date = date("Y-m-d", strtotime($start_date));
    }
} else {
    $start_date = "";
    $start_date = date("Y-m-d", strtotime($day_1));  //$date;
}
if (isset($_GET["to_date"])) {
    $end_date = $_GET["to_date"];
    if (strlen($end_date) == 10) {
        $end_date = date("Y-m-d", strtotime($end_date));
    } else {
        $end_date = "";
    }
} else {
    $end_date = "";
}


if (isset($_GET["tab"])) {
    $tab = $_GET["tab"];
    //$end_date = date("Y-m-d", strtotime($end_date));
} else {
    $tab = "other";
}


//echo "Start Date :- ".$start_date;
if ($tab == 'all') {
//$query = "SELECT * from tbl_enquiry";
    list($status, $id, $result_arr) = getOrderPujaTmpData();
    if ($status == 0) {
        $err_msg = $result_arr;
    }
} else {
    list($status, $id, $result_arr) = getOrderPujaTmpByDate(0, 0, $start_date, $end_date); //getEnqData(); //getOrderTmpData();
    if ($status == 0) {
        $err_msg = $result_arr;
    }
}


//print_r($result_arr);
//$date = "Mar 03, 2011";
//$date = date("Y-m-d");

$tab_page_url = "list_order_puja_tmp.php";

$day_1_url = "$tab_page_url?from_date=$day_1";
$day_2 = date("d-m-Y", strtotime("+1 day", $date));
$day_2_url = "$tab_page_url?from_date=$day_2";
$day_3 = date("d-m-Y", strtotime("+2 day", $date));
$day_3_url = "$tab_page_url?from_date=$day_3";
$day_4 = date("d-m-Y", strtotime("+3 day", $date));
$day_4_url = "$tab_page_url?from_date=$day_4";
$day_5 = date("d-m-Y", strtotime("+4 day", $date));
$day_5_url = "$tab_page_url?from_date=$day_5";

$critical_url = "$tab_page_url?tab=all";

$day_selected = date("d-m-Y", strtotime($start_date));

//echo "<br/>$day_1 , $day_2, $day_3, $day_4, $day_5<br/>";

if ($tab == 'all') {
    $tab = "all";
    //$day_5_url = "#";
} elseif ($day_selected == $day_1) {
    $tab = "day_1";
    //$day_1_url = "#";
} elseif ($day_selected == $day_2) {
    $tab = "day_2";
    //$day_2_url = "#";
} elseif ($day_selected == $day_3) {
    $tab = "day_3";
    //$day_3_url = "#";
} elseif ($day_selected == $day_4) {
    $tab = "day_4";
    //$day_4_url = "#";
} elseif ($day_selected == $day_5) {
    $tab = "day_5";
    //$day_5_url = "#";
} else {
    $tab = "other";
}



//echo "[$day_selected]==[$day_1]  -> Tab = $tab";

/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> 
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    -->

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">


                                        <li class=""><a data-toggle="tab" href="#User-Details">Choose Date</a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'day_1'); ?>">
                                            <a href="<?php echo $day_1_url; ?>"><span> Today </span></a>
                                        </li>

                                        <?php //if ($enq_id > 0) {     ?>
                                        <li class="<?php getActiveTab($tab, 'day_2'); ?>">
                                            <a  href="<?php echo $day_2_url; ?>"><span> Tomorrow </span></a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'day_3'); ?>">
                                            <a  href="<?php echo $day_3_url; ?>"><?php echo date("D j", strtotime($day_3)); ?></a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'day_4'); ?>">
                                            <a  href="<?php echo $day_4_url; ?>"><span>  <?php echo date("D j", strtotime($day_4)); ?> </span></a>
                                        </li>

                                        <li class="<?php getActiveTab($tab, 'day_5'); ?>">
                                            <a  href="<?php echo $day_5_url; ?>"><?php echo date("D j", strtotime($day_5)); ?></a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'all'); ?>">
                                            <a  href="<?php echo $critical_url; ?>">All</a>
                                        </li>
                                        <?php if ($tab == "other") { ?>
                                            <li class="<?php getActiveTab($tab, 'other'); ?>">
                                                <a data-toggle="tab" href="#other">Other</a>
                                            </li>
                                            <?php
                                        }
//}
                                        ?>




                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                        <div id="enquiry-details" class="tab-pane fade active in">

                                            <div class="row">

                                                <div class="col-lg-12">
                                                    <div class="panel panel-default">
                                                        <div class="box" id="order_tmp_list_msg" >
                                                            <div class="col-lg-12">
                                                                <?php if ($err_msg) { ?>

                                                                    <div class="alert alert-danger alert-dismissable">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                                        <?php echo $err_msg; ?>. <!-- <a class="alert-link" href="#">Alert Link</a>. -->
                                                                    </div>

                                                                <?php } ?>
                                                            </div>
                                                        </div>

                                                        <?php if ($status == 1) { ?>
                                                            <!-- <div class="panel-body">  -->  <!-- </div> -->                                        
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example"  style="font-size: 12px !important;">
                                                                    <thead>
                                                                        <tr>

                                                                            <th style="width: 25px;">No</th>
                                                                            <th style="width: 60px">Order </th>
                                                                            <th style="width: 210px">Customer </th>

                                                                            <th style="width: 70px">Puja </th>
                                                                            <th style="width: 100px">Date </th>
                                                                            <th style="width: 80px">Pandit </th>
                                                                            <th>Amount </th>
                                                                            <th>PCost </th> 
                                                                            <th>Samagri </th>
                                                                            <th>Mode </th> 
                                                                            <!-- <th>Area </th>
                                                                            <th>City </th> -->
                                                                            <th>Status </th>


                                                                            <th style="width:80px;">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                        <?php
                                                                        $count = 0;

                                                                        $datetime = new DateTime('tomorrow');
//echo "<br/>tomorrow = ".$datetime->format('Y-m-d');
                                                                        $date_2 = $datetime->format('Y-m-d');

//$datetime = new DateTime('2013-01-22');
                                                                        $datetime->modify('+1 day');
//echo "<br/>Day after tomorrow = ".$datetime->format('Y-m-d');
                                                                        $date_3 = $datetime->format('Y-m-d');

                                                                        $datetime->modify('-2 day');
//echo "<br/>Day after tomorrow = ".$datetime->format('Y-m-d');
                                                                        $date_1 = $datetime->format('Y-m-d');


//echo "<br/>Day 1 = $date_1, Day 2 = $date_2, Day 3 = $date_3 "; 
                                                                        //list($status, $id, $result_arr) = getOrderPujaTmpData();
                                                                        //$puja_data_arr = getDataFromTable("tbl_pooja_master");
                                                                        $pandit_arr = getDataFromTable("tbl_pandit");
                                                                        //var_dump($result_arr);



                                                                        $order_puja_data_arr = array();
                                                                        for ($i = 0; $i < count($result_arr); $i++) {

                                                                            $order_id = $result_arr[$i]['order_id'];
                                                                            list($status, $id, $order_arr) = getOrderTmpData($order_id);
                                                                            if ($order_arr[0]['order_status'] != 18) {


                                                                                $count++;
//                                                            list($enq_date, $enq_time) = validateDbDateTime($result_arr[$i]["puja_date"]);
//                                                            $enq_date = date('Y-m-d', strtotime($enq_date));
//                                                            $enq_date = $enq_date . "<br/>" . $enq_time;

                                                                                $puja_color = "";
                                                                                list($puja_date, $puja_time) = validateDbDateTime($result_arr[$i]["puja_date"]);
                                                                                $puja_date = date('Y-m-d', strtotime($puja_date));

                                                                                $puja_color_date = $puja_date;
                                                                                if ($date_1 == $puja_color_date)
                                                                                    $puja_color = "success";
                                                                                if ($date_2 == $puja_color_date)
                                                                                    $puja_color = "danger";
                                                                                if ($date_3 == $puja_color_date)
                                                                                    $puja_color = "warning";

                                                                                $puja_color = "";

                                                                                if ($result_arr[$i]['puja_status'] == "Done") {
                                                                                    $puja_color = "success";
                                                                                } elseif (strtotime(date("Y-m-d H:i:s")) > strtotime($result_arr[$i]["puja_date"])) {
                                                                                    $puja_color = "danger";
                                                                                }

                                                                                $puja_date = $puja_date . "<br/>" . $puja_time;

                                                                                $puja_name = "";
                                                                                if ($result_arr[$i]['puja_id'] > 0) {
                                                                                    for ($j = 0; $j < count($puja_data_arr); $j++) {
                                                                                        if ($puja_data_arr[$j]["id"] == $result_arr[$i]['puja_id'])
                                                                                            $puja_name = $puja_data_arr[$j]["pooja_name"];
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    $puja_name = $result_arr[$i]['puja_name'];
                                                                                }

                                                                                $pandit_name = "";
                                                                                if ($result_arr[$i]['pandit_id'] > 0) {
                                                                                    for ($j = 0; $j < count($pandit_arr); $j++) {
                                                                                        if ($pandit_arr[$j]["id"] == $result_arr[$i]['pandit_id']) {
                                                                                            $pandit_name = $pandit_arr[$j]["first_name"] . " " . $pandit_arr[$j]["last_name"];
                                                                                            $pandit_name .= "<br/>" . $pandit_arr[$j]["phone_1"] . " , " . $pandit_arr[$j]["phone_2"];
                                                                                            $pandit_name .= "<br/>" . $pandit_arr[$j]["email_1"];
//$pandit_contact = $pandit_arr[$j]["phone_1"];
                                                                                            //$pandit_email = $pandit_arr[$j]["email_1"];
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    $pandit_name = $result_arr[$i]["pandit_name"] . "<br/> " . $result_arr[$i]["pandit_contact"];
                                                                                    //$pandit_name .=  ;
                                                                                }


                                                                                $cust_name = "";
                                                                                $cust_id = $order_arr[0]['cust_id'];
                                                                                list($cust_status, $id, $cust_data_arr) = getUserData($order_arr[0]['cust_id']);
                                                                                if ($cust_status == 1) {
                                                                                    $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
                                                                                    $cust_name .= "<br/>" . $cust_data_arr["phone_1"] . " , " . $cust_data_arr["phone_2"];
                                                                                    $cust_name .= "<br/>" . $cust_data_arr["email_1"];
                                                                                    $cust_name .= "<br/>Area : " . $cust_data_arr["area"];
                                                                                    $cust_name .= "<br/>City : " . $cust_data_arr["city"];


                                                                                    //$cust_city = $cust_data_arr["city"];
                                                                                    //$cust_area = $cust_data_arr["area"];
                                                                                } else {
                                                                                    //$cust_name = $order_arr[0]['customer_name'];
                                                                                    //$cust_city = $order_arr[0]["customer_city"];
                                                                                    //$cust_area = $order_arr[0]["customer_area"];

                                                                                    $cust_name = "No Customer Master";
                                                                                }

                                                                                
                                                                                
                                                                                ?> 
                                                                                <tr class="<?php echo $puja_color; ?>">
                                                                                    <td><?php echo $count; ?></td>
                                                                                    <td>
                                                                                        <a href="add_order_tmp.php?edit_id=<?php echo $order_arr[0]['id']; ?>"><?php echo $order_arr[0]['order_no']; ?></a>                                                                

                                                                                    </td>
                                                                                    <td><?php echo $cust_name; ?></td>
                                                                                    <td>
                                                                                        <a href="add_order_tmp.php?edit_id=<?php echo $order_arr[0]['id']; ?>&puja_id=<?php echo $result_arr[$i]['id']; ?>&tab=edit">
                                                                                            <?php echo $puja_name; //$result_arr[$i]['puja_name'];    ?>
                                                                                        </a>
                                                                                    </td>
                                                                                    <td><?php echo $puja_date; ?></td>
                                                                                    <td><?php echo $pandit_name; // $result_arr[$i]['pandit_name'] . "<br/>" . $result_arr[$i]['pandit_contact'];                          ?></td>
                                                                                    <td><?php echo $result_arr[$i]['total_amt']; ?></td>
                                                                                    <td><?php echo $result_arr[$i]['pandit_payment_total']; ?></td>
                                                                                    <td><?php echo $result_arr[$i]['samagri_type']; ?></td> 
                                                                                    <td><?php echo $result_arr[$i]['payment_mode']; ?></td> 
                                                                                    <!--
                                                                                    <td><?php echo $cust_area; ?></td>
                                                                                    <td><?php echo $cust_city; ?></td>
                                                                                    -->
                                                                                    <td><?php echo $result_arr[$i]['puja_status']; ?></td>

                                                                                    <td>

                                                                                        <a href="add_order_tmp.php?edit_id=<?php echo $order_arr[0]['id']; ?>&puja_id=<?php echo $result_arr[$i]['id']; ?>&tab=edit"   target="_blank" class="btn text-info btn-xs btn-flat">
                                                                                            <i class="icon-list-alt icon-white"></i> Edit</a><br/>

                                                                                        <a href="send_sms.php?rec_type=Order&enq_id=<?php echo $order_arr[0]['id']; ?>&puja_id=<?php echo $result_arr[$i]['id']; ?>&tag=order_puja_details" target="_blank" class="btn text-info btn-xs btn-flat">
                                                                                            <i class="icon-list-alt icon-white"></i> Send SMS</a><br/> 

                                                                                        <?php if ($result_arr[$i]['feedback_id'] > 1) { ?>
                                                                                            <a href="feedback/index.php?client_feedback=<?php echo $result_arr[$i]['feedback_id']; ?>"   target="_blank" class="btn text-info btn-xs btn-flat">
                                                                                                <i class="icon-list-alt icon-white"></i> FeedBack
                                                                                            </a>    


                                                                                        <?php } else { ?>
                                                                                            <a href="actions/feedback_generate.php?order_id=<?php echo $order_arr[0]['id']; ?>&puja_id=<?php echo $result_arr[$i]['id']; ?>&cust_id=<?php echo $cust_id; ?>&Action=Generate"   target="_blank" class="btn text-info btn-xs btn-flat">
                                                                                                <i class="icon-list-alt icon-white"></i> Feed-Back </a>
                                                                                        <?php } ?>


                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>





                                                                    </tbody>
                                                                </table>
                                                            </div>                                            

<?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="User-Details" class="tab-pane fade <?php getActiveBody($tab, 'User-Details'); ?>">

                                            <form name="search_followup" id="search_followup" action="<?php echo $tab_page_url; ?>" method="get" enctype="multipart/form-data">
                                                <div class="col-lg-12">

                                                    <div class="box">                                 
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading"> User Details </div>
                                                            <div class="panel-body">
                                                                <table class="table table-bordered sortableTable responsive-table">

                                                                    <tbody>


                                                                        <tr>
                                                                            <td>From Date </td>
                                                                            <td>
                                                                                <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="from_date" id="from_date" title="From Date">
                                                                            </td>
                                                                            <td>To Date </td>
                                                                            <td>
                                                                                <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="to_date" id="to_date" title="To Date">                                    
                                                                            </td>


                                                                        </tr>



                                                                    </tbody>
                                                                </table>




                                                            </div>
                                                            <div class="panel-footer" align="right"> 
                                                                <span class="input-group-btn">
                                                                    <!-- <input class="btn btn-success " type="submit" value="Save" name="Save" > -->
                                                                    <button id="btn-chat" class="btn btn-success btn-sm" > Search</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
<?php //include 'right_menu.php';                         ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
<?php include 'footer.php'; ?>
        <!--END FOOTER -->







        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <!-- <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>


        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>

        <script src="assets/plugins/daterangepicker/moment.min.js"></script>

        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script> -->
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>

        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>       
        <script src="assets/js/formsInit.js"></script>
        <script>

            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
            $(function () {
                formInit();
            });

        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
