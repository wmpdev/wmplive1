<?php
$Main_Menu = "Users";
$Sub_Menu_1 = "Add_Pandit";

$page = "Pandit Details";
$page_title = "Pandit Details";
$edit_id = 0;

//print_r($_GET);


if (isset($_POST['rec_id']))
    $edit_id = trim($_POST['rec_id']);



if (isset($_GET['edit_id']))
    $edit_id = trim($_GET['edit_id']);


include_once 'config/auth.php';

//Array ( [id] => 5 [first_name] => Ninad [last_name] => Tilkar [email_1] => ninad.nmpl@gmail.com [email_2] => [phone_1] => 8108869247 [phone_2] => [address_1] => Mumbai [address_2] => 0 [reference_id] => 0 [add_date] => 2015-08-19 16:14:20 [mod_date] => 2015-08-19 16:14:20 [add_user] => 1 [mod_user] => 1 [status] => 0 ) 

$result_arr = "";



if ($edit_id > 0) {

    list($status, $id, $result_arr) = listPanditData($edit_id);
    //print_r($result_arr);
    if ($status == 1) {
        //print_r($result_arr);

        $cust_id = $result_arr[0]["id"];

        $first_name = $result_arr[0]["first_name"];
        $last_name = $result_arr[0]["last_name"];
        $email_1 = $result_arr[0]["email_1"];
        $email_2 = $result_arr[0]["email_2"];
        $phone_1 = $result_arr[0]["phone_1"];
        $phone_2 = $result_arr[0]["phone_2"];
        $address_1 = $result_arr[0]["address_1"];
        $reference_id = $result_arr[0]["reference_id"];

        $city = $result_arr[0]["pandit_city"];
        $area = $result_arr[0]["pandit_area"];
        $language = $result_arr[0]["pandit_language"];
        $acc_name = $result_arr[0]["pandit_acc_name"];
        $acc_no = $result_arr[0]["pandit_acc_no"];
        $bank_name = $result_arr[0]["pandit_acc_bank_name"];
        $bank_branch = $result_arr[0]["pandit_acc_bank_branch"];
        $acc_ifsc = $result_arr[0]["pandit_acc_ifsc"];
        $cast_id = $result_arr[0]["pandit_cast"];
        $status_id = $result_arr[0]["status"];
        
        /*
          list($status, $id, $enq_data_arr) = getUserEnq($cust_id);
          if ($status == 0)
          $enq_data_arr = array();
         */
        //print_r($enq_data_arr);
        //echo count($enq_data_arr);
    } else {
        echo " Error :- $result_arr";
    }
}

list($status, $id, $pandit_arr) = getPanditData();
//print_r($pandit_arr);
//echo "status[$status],[id],[$pandit_arr]";
//$source_arr = getDataFromTable("tbl_source_master");
//$medium_arr = getDataFromTable("tbl_medium_master");
//$language_arr = getDataFromTable("tbl_language_master");
$caste_arr = getDataFromTable("tbl_caste_master");




/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />

        <!-- PAGE LEVEL STYLES 
    <link rel="stylesheet" href="assets/css/layout2.css"  />    
    <link rel="stylesheet" href="assets/css/jquery-ui.css"  />
    <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
    <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
    <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
    <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
    <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
    <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
    <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />
        -->
        <!-- END PAGE LEVEL  STYLES -->



        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">
                                        <li class="active"><a data-toggle="tab" href="#User_Details">Pandit Details</a>
                                        </li>
                                        <?php if (count($enq_data_arr) > 0) { ?>
                                            <li class="">
                                                <a data-toggle="tab" href="#enquiry_details">
                                                    <i class="icon-check icon-1x"></i>
                                                    <span> Related Enquires</span>
                                                    <span class="label label-danger"><?php echo count($enq_data_arr); ?></span>
                                                </a>
                                            </li>

                                            <li class="">
                                                <a href="add_enquiry.php?user_id=<?php echo $cust_id; ?>" title="Add New Enquiry">
                                                    <i class="icon-plus-sign icon-1x"></i><span> New Enquiry</span> </a>
                                            </li>
                                        <?php } ?>
                                        <!--
                                        <li class=""><a data-toggle="tab" href="#messages-pills">Followup</a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#home-pills">
                                                <i class="icon-check icon-1x"></i>
                                                <span>  Quotation Details</span>

                                            </a>
                                        </li>
                                        
                                        <li class=""><a data-toggle="tab" href="#profile-pills">Address Details</a>
                                        </li>
                                        -->







                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">


                                        <div id="User_Details" class="tab-pane fade active in">

                                            <div class="box" id="add_user_msg" >
                                                <div class="col-lg-12">            
                                                    <!--
                                                    <div class="alert alert-danger alert-dismissable">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a class="alert-link" href="#">Alert Link</a>.
                                                    </div>
                                                    -->
                                                </div>
                                            </div>

                                            <div class="col-lg-6">

                                                <form name="add_pandit_form" id="add_pandit_form" action="" method="POST" enctype="multipart/form-data">
                                                    <div class="box">                                 
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading"> Add Pandit </div>
                                                            <div class="panel-body">
                                                                <table class="table table-bordered sortableTable responsive-table">

                                                                    <tbody>

                                                                    <input id="edit_id" class="form-control" type="hidden" value="<?php echo $edit_id; ?>" placeholder="edit id" name="edit_id">

                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td>
                                                                            <input id="f_name" class="form-control" type="text" value="<?php echo $first_name; ?>" placeholder="First Name" name="f_name">
                                                                        </td>
                                                                        <td>
                                                                            <input id="l_name" class="form-control" type="text" value="<?php echo $last_name; ?>" placeholder="Last Name" name="l_name">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Number</td>
                                                                        <td>
                                                                            <input id="number_1" class="form-control" type="text" value="<?php echo $phone_1; ?>" placeholder="Number 1" name="number_1"  onkeyup="searchUser();">
                                                                        </td>
                                                                        <td>
                                                                            <input id="number_2" class="form-control" type="text" value="<?php echo $phone_2; ?>" placeholder="Number 2" name="number_2" onkeyup="searchUser();">
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Email Id </td>
                                                                        <td>
                                                                            <input id="email_1" class="form-control" type="text" value="<?php echo $email_1; ?>" placeholder="Email Id" name="email_1"  onkeyup="searchUser();">
                                                                        </td>
                                                                        <td>
                                                                            <input id="email_2" class="form-control" type="text" value="<?php echo $email_2; ?>" placeholder="Alternet Email-Id" name="email_2"  onkeyup="searchUser();">
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Address</td>
                                                                        <td colspan="2">
                                                                            <textarea id="address" name="address" class="form-control" placeholder="Enter Comments" ><?php echo $address_1; ?></textarea>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td>
                                                                            <input id="city" class="form-control" type="text" value="<?php echo $city; ?>" placeholder="City" name="city">
                                                                        </td>
                                                                        <td>
                                                                            <input id="area" class="form-control" type="text" value="<?php echo $city; ?>" placeholder="Area" name="area">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Reference Pandit</td>
                                                                        <td colspan="2">
                                                                            <select class="form-control" tabindex="2" name="reference_id" id="reference_id" data-placeholder="Select Pandit Reference">                    
                                                                                <option selected="" value="0">-</option>
                                                                                <?php
                                                                                for ($i = 0; $i < count($pandit_arr); $i++) {

                                                                                    //echo "$key->$value";
                                                                                    $selected = "";
                                                                                    $key = $pandit_arr[$i]["id"];
                                                                                    $value = $pandit_arr[$i]["first_name"] . " " . $pandit_arr[$i]["last_name"];
                                                                                    if ($key == $reference_id)
                                                                                        $selected = "selected";
                                                                                    ?>
                                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                                                <?php } ?>  
                                                                            </select>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Pandit Cast</td>
                                                                        <td colspan="2">
                                                                            <select class="form-control" tabindex="2" name="cast_id" id="cast_id" data-placeholder="Select Pandit Cast">                    
                                                                                <option selected="" value="0">-</option>
                                                                                <?php
                                                                                for ($i = 0; $i < count($caste_arr); $i++) {

                                                                                    //echo "$key->$value";
                                                                                    $selected = "";
                                                                                    $key = $caste_arr[$i]["id"];
                                                                                    $value = $caste_arr[$i]["caste_name"];
                                                                                    if ($key == $cast_id)
                                                                                        $selected = "selected";
                                                                                    ?>
                                                                                    <option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option> 
                                                                                <?php } ?>  
                                                                            </select>
                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td>Language Known</td>
                                                                        <td colspan="2">
                                                                            <input id="language" class="form-control" type="text" value="<?php echo $language ; ?>" placeholder="Language Known" name="language" title="Language Known">
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Acc Name</td>
                                                                        <td colspan="2">
                                                                            <input id="acc_name" class="form-control" type="text" value="<?php echo $acc_name ; ?>" placeholder="Add Account Name" name="acc_name" title="Add Account Name">
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Bank</td>
                                                                        <td >
                                                                            <input id="bank_name" class="form-control" type="text" value="<?php echo $bank_name ; ?>" placeholder="Bank Name" name="bank_name" title="Bank Name">
                                                                        </td>
                                                                        <td>
                                                                            <input id="bank_branch" class="form-control" type="text" value="<?php echo $bank_branch ; ?>" placeholder="Branch Name" name="bank_branch"  title="Branch Name">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Account</td>
                                                                        <td >
                                                                            <input id="acc_no" class="form-control" type="text" value="<?php echo $acc_no ; ?>" placeholder="Account No" name="acc_no" title="Account No">
                                                                        </td>
                                                                        <td>

                                                                            <input id="acc_ifsc" class="form-control" type="text" value="<?php echo $acc_ifsc ; ?>" placeholder="IFSC code" name="acc_ifsc" title="IFSC code">
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Status</td>
                                                                        <td colspan="2">
                                                                            <select class="form-control" tabindex="2" name="status_id" id="status_id" data-placeholder="Select Pandit Cast">                    
                                                                                <option <?php if($status_id == 0) echo "selected"; ?> value="0">Enable</option>
                                                                                <option  <?php if($status_id == 1) echo "selected"; ?> value="1">Disable</option>

                                                                            </select>
                                                                        </td>

                                                                    </tr>




                                                                    </tbody>
                                                                </table>

                                                            </div>
                                                            <div class="panel-footer"> 
                                                                <span class="input-group-btn">
                                                                    <!-- <button id="btn-chat" class="btn btn-success btn-sm"> Add</button> -->
                                                                    <button id="btn-chat" class="btn btn-success btn-sm" > Add </button>

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>


                                            </div>

                                            <div class="col-lg-6">

                                                <div class="box">
                                                    <header>
                                                        <h5>Search Result</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="body collapse in" id="search_data_block">


                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                        <div id="enquiry_details" class="tab-pane fade ">

                                            <div class="box" id="enq_msg" >
                                                <div class="col-lg-12">            

                                                    <?php if (count($enq_data_arr) > 0) { ?>


                                                        <div class="box">
                                                            <div id="search_data_block" class="body collapse in">

                                                                <table class="table table-bordered sortableTable responsive-table">

                                                                    <tbody>

                                                                        <tr>
                                                                            <th>Enq No</th>
                                                                            <th>Enq Date</th>
                                                                            <th>Puja Name</th>
                                                                            <th>Puja Date</th>
                                                                            <th>Puja Time</th>


                                                                            <th colspan="2">Action</th>
                                                                        </tr>

                                                                        <?php
                                                                        for ($i = 0; $i < count($enq_data_arr); $i++) {
                                                                            //print_r($enq_data_arr[$i]);
                                                                            $enq_id = $enq_data_arr[$i]["id"];
                                                                            $enq_no = $enq_data_arr[$i]["enq_no"];
                                                                            $enq_date = $enq_data_arr[$i]["enq_date"];
                                                                            //$puja_date = $enq_data_arr[$i]["pooja_date"];
                                                                            //$puja_time = $enq_data_arr[$i]["pooja_time"];

                                                                            $puja_name = "";
                                                                            $puja_date_time = "";
                                                                            for ($j = 1; $j < 5; $j++) {
                                                                                //$index_no = $j + 1;
                                                                                $puja = $enq_data_arr[$i]["puja_id_$j"];
                                                                                $status = 0;
                                                                                if ($puja > 0)
                                                                                    //list($status, $id, $puja_name_data) = getPujaData($puja);
                                                                                if ($status == 1) {
                                                                                    $puja_name = $puja_name . $puja_name_data["pooja_name"] . "<br/>";

                                                                                    $puja_date_tmp = $enq_data_arr[$i]["puja_date_$j"];
                                                                                    $puja_time_tmp = $enq_data_arr[$i]["puja_time_$j"];
                                                                                    $puja_date_tmp = date("d-M-Y", strtotime($puja_date_tmp));
                                                                                    $puja_time_tmp = date("h:i A", strtotime($puja_time_tmp));

                                                                                    $puja_date = $puja_date . $puja_date_tmp . "<br/>";
                                                                                    $puja_time = $puja_time . $puja_time_tmp . "<br/>";

                                                                                    //$puja_date_time = $puja_date_time . $puja_date . " " . $puja_time . "<br/>";
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <tr>
                                                                                <td><?php echo $enq_no; ?></td>
                                                                                <td><?php echo $enq_date; ?></td>
                                                                                <td><?php echo $puja_name; ?></td>
                                                                                <td><?php echo $puja_date; ?></td>
                                                                                <td><?php echo $puja_time; ?></td>

                                                                                <td><a href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>" class="btn text-info btn-xs btn-flat">
                                                                                        <i class="icon-list-alt icon-white"></i> Show</a> </td>

                                                                                <!-- 
                                                                                <td><a href="add_enquiry.php?user_id=5" class="btn text-success btn-xs btn-flat">
                                                                                        <i class="icon-plus-sign icon-white"></i> Enquiry</a></td>
                                                                                -->
                                                                            </tr>
                                                                        <?php } ?>



                                                                    </tbody>
                                                                </table>

                                                            </div>
                                                        </div>

                                                    <?php } ?>



                                                </div>
                                            </div>






                                        </div>



                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';            ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->



        <script>


            function add_user() {

                //alert("i am in add_user");


                var edit_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                var number_1 = document.getElementById("number_1").value;
                var number_2 = document.getElementById("number_2").value;
                var email_1 = document.getElementById("email_1").value;
                var email_2 = document.getElementById("email_2").value;
                var f_name = document.getElementById("f_name").value;
                var l_name = document.getElementById("l_name").value;
                var address = document.getElementById("address").value;
                var reference_id = document.getElementById("reference_id").value;


                var param = "edit_id=" + edit_id
                        + "&number_1=" + number_1
                        + "&number_2=" + number_2
                        + "&email_1=" + email_1
                        + "&email_2=" + email_2
                        + "&f_name=" + f_name
                        + "&l_name=" + l_name
                        + "&reference_id=" + reference_id
                        + "&address=" + address;



                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("add_user_msg").innerHTML = xmlhttp.responseText;

                    }
                }
                xmlhttp.open("POST", "actions/add_pandit.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }



            function searchUser() {

                //alert("i am in searchUser");


                //var edit_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                var number_1 = document.getElementById("number_1").value;
                var number_2 = document.getElementById("number_2").value;
                var email_1 = document.getElementById("email_1").value;
                var email_2 = document.getElementById("email_2").value;
                //var f_name = document.getElementById("f_name").value;
                //var l_name = document.getElementById("l_name").value;
                //var address = document.getElementById("address").value;


                var param = "number_1=" + number_1
                        + "&number_2=" + number_2
                        + "&email_1=" + email_1
                        + "&email_2=" + email_2;

                //+ "&f_name=" + f_name
                //+ "&l_name=" + l_name
                //+ "&address=" + address;



                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("search_data_block").innerHTML = xmlhttp.responseText;

                    }
                }
                xmlhttp.open("POST", "actions/search_pandit.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }


            $(document).ready(function ()
            {


                function getDoc(frame) {
                    var doc = null;

                    // IE8 cascading access check
                    try {
                        if (frame.contentWindow) {
                            doc = frame.contentWindow.document;
                        }
                    } catch (err) {
                    }

                    if (doc) { // successful getting content
                        return doc;
                    }

                    try { // simply checking may throw in ie8 under ssl or mismatched protocol
                        doc = frame.contentDocument ? frame.contentDocument : frame.document;
                    } catch (err) {
                        // last attempt
                        doc = frame.document;
                    }
                    return doc;
                }

                $("#add_pandit_form").submit(function (e)
                {

                        //txt = "You pressed OK!";
                        $("#add_user_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                        $("html, body").animate({scrollTop: 0}, 600);
                        //return false;
                        //var enq_id = document.getElementById("edit_id").value;
                        //alert(" i am in quotation_form function");
                        //confirm("You Really want to save Quotation, You will not able to edit it later.");
                        var formObj = $(this);
                        var formURL = formObj.attr("action");


                        if (window.FormData !== undefined)  // for HTML5 browsers
                                //	if(false)
                                {

                                    var formData = new FormData(this);
                                    $.ajax({
                                        url: 'actions/add_pandit.php',
                                        type: 'POST',
                                        data: formData,
                                        mimeType: "multipart/form-data",
                                        contentType: false,
                                        cache: false,
                                        processData: false,
                                        success: function (data, textStatus, jqXHR)
                                        {
                                            //$("#quotation_msg").html('' + data + '');                                        

                                            if (data.indexOf("SUCCESS") >= 0) {
                                                //alert("Puja Added Successfully");
                                                //list_puja_quotation_refresh();
                                                $("#add_user_msg").html('' + data + '');
                                                //window.location.replace("http://stackoverflow.com");


                                            } else {
                                                $("#add_user_msg").html('' + data + '');
                                            }


                                        },
                                        error: function (jqXHR, textStatus, errorThrown)
                                        {
                                            $("#add_user_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                        }
                                    });
                                    e.preventDefault();
                                    //e.unbind();
                                }
                        else  //for olden browsers
                        {
                            //generate a random id
                            var iframeId = 'unique' + (new Date().getTime());

                            //create an empty iframe
                            var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                            //hide it
                            iframe.hide();

                            //set form target to iframe
                            formObj.attr('target', iframeId);

                            //Add iframe to body
                            iframe.appendTo('body');
                            iframe.load(function (e)
                            {
                                var doc = getDoc(iframe[0]);
                                var docRoot = doc.body ? doc.body : doc.documentElement;
                                var data = docRoot.innerHTML;
                                $("#add_user_msg").html('' + data + '');
                            });

                        }

                    
                });


            });



        </script>





        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
        <script src="assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/daterangepicker/moment.min.js"></script>
        <script src="assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
        <script src="assets/js/formsInit.js"></script>
        <script>
            $(function () {
                formInit();
            });
        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
