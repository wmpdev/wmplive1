<?php
// instantiate product object
// $Main_Menu = "Enquiry";
//$Sub_Menu_1 = "list_enquiry";

include_once 'config/auth.php';

//$stmt = $user->listUsers();
// list($status, $id, $result_arr) = getEnqData(); //getOrderTmpData();
//print_r($_SESSION);
//$log->LogMsg("Listing Of Users");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title>List Enquiry</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" />-->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style>
            .showme{ 
                display: none;
            }
            .showhim:hover .showme{
                display : block;
            }
        </style>

    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->

            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content" >
                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Edit Agent Number </h2>
                             <div class="alert alert-success">
                                Number Updated Successfully.
                             </div>
                             <div class="alert alert-danger">
                                Number Unregistered Successfully.
                             </div>
                             <div class="panel-body">  
                             <form id="saveAgentNumber">
                                 <div class="form-group">
                                    <input type="text" class="form-control" name="agent_number" id="agent_number" onkeypress="return isNumber(event)" placeholder="Enter agent number" required maxlength="11"></input>
                                    <input type="hidden" id="id" name="id" value="<?php echo $_SESSION["login_id"]; ?>"></input>
                                 </div>
                                 <div class="form-group">
                                    <button class="btn btn-success submit" type="button">Save</button>
                                 </div>
                             </form>

                                 <div class="form-group">
                                    <button class="btn btn-success" type="button" onclick="setPhone('9869173506','179396','142ff267b641e853a453','fcce78df394ac22ea3e4')">Phone 1</button>
                                    <button class="btn btn-success" type="button" onclick="setPhone('8097388213','181738','0404f06f46331b843ea6','ab2b0a1705484afb4404')">Phone 2</button>
                                    <button class="btn btn-danger" type="button" onclick="setPhone('','','','')">Unregister</button>
                                 </div>

                             </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php include 'footer.php'; ?>

        </body>

    </html>

    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        var id = '<?php echo $_SESSION["login_id"]; ?>';

        $(document).ready(function(){
            
            $(".alert").hide();
            
            $(document).on("click",".submit",function(){

                var id = $("#id").val();
                var agent_number = $("#agent_number").val();
                   if(agent_number == "")
                   {
                    return;
                   }
                $.ajax({
                    url:'saveAgentNumber.php',
                    method:'POST',
                    dataType:'json',
                    data: {id:id,agent_number:agent_number},
                    success:function(data)
                    {
                        if(data = true)
                        {
                            $(".alert").show();
                        }
                    }
                })
            });

            $.ajax({
                url:'fetchAgentNo.php',
                method:'POST',
                dataType:'json',
                data:{id:id},
                success:function(res)
                {
                    $("#agent_number").val(res[0]);
                }
            });
        });



    function setPhone(agent_num,api_id,api_key,api_secret)
    {
        //console.log(agent_num+" "+ api_id+" "+ api_key+" "+ api_secret);
        var id = $("#id").val();
        //var agent_num='<?php echo $_SESSION["agent_num"]; ?>';
        $.ajax({
            url:'saveAgentNumber.php',
            method:'POST',
            dataType:'json',
            data: 
            {
                id:id,
                agent_number:agent_num,
                api_id:api_id,
                api_key:api_key,
                api_secret:api_secret
            },
            success:function(data)
            {
                if(data = true)
                {

                    if(agent_num=="")
                    {
                        $(".alert-success").hide();
                        $(".alert-danger").show();
                    }
                    else

                    {
                        $(".alert-success").show();
                        $(".alert-danger").hide();
                    }
                }
            }
        })
    }
    </script>