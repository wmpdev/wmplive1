<?php
$Main_Menu = "Dashboard";

$page_title = "Admin Dashboard";  //Admin Dashboard Template | Blank Page


include_once 'config/auth.php';


$date = strtotime(date("Y-m-d"));
$day_1 = date("d-m-Y", $date);

$datetime = new DateTime('tomorrow');
//echo "<br/>tomorrow = ".$datetime->format('Y-m-d');
$date_2 = $datetime->format('d-m-Y');

if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    //$start_date = date("Y-m-d", strtotime($start_date));

    if (strlen($start_date) == 10) {
        $start_date = date("Y-m-d", strtotime($start_date));
    }
} else {
    $start_date = "";
    $start_date = date("Y-m-d", strtotime($day_1));  //$date;
}
if (isset($_GET["to_date"])) {
    $end_date = $_GET["to_date"];
    if (strlen($end_date) == 10) {
        $end_date = date("Y-m-d", strtotime($end_date));
    } else {
        $end_date = "";
    }
} else {
    $end_date = "";
}

list($status, $from_date, $to_date, $msg) = setFromToDate($start_date, $end_date);

if ($status == 0) {
    $msg = "Please Select Proper Date For Followup.";
    //return array($status, $id, $msg);    
}

if ($status == 1) {

   /* echo $query = "SELECT e.enq_no,e.cust_id,e.enq_date,e.quotation_id,e.enq_status e_status,e.followup_date, f.id,f.enq_id,f.add_enq_date,f.next_date,f.enq_status
            FROM tbl_enquiry e  inner join  `tbl_enq_followup` f 
            on (f.next_date BETWEEN '$from_date' and '$to_date' )
            and f.enq_id=e.id
            order by f.next_date desc";
    */
    
    echo $query = "SELECT e.enq_no,e.cust_id,e.enq_date,e.quotation_id,e.enq_status e_status,e.followup_date, f.id,f.enq_id,f.add_enq_date,f.next_date,f.enq_status
            FROM tbl_enquiry e  inner join  `tbl_enq_followup` f 
            on (e.enq_date BETWEEN '$from_date' and '$to_date' )
            and f.enq_id=e.id
            order by f.next_date desc";
    
    

//$query = "SELECT * from tbl_enquiry";
    list($status, $id, $result_arr) = getDataByQuery($query);
//var_dump($result_arr);




    $enq_no_arr = array();
    for ($i = 0; $i < count($result_arr); $i++) {

        $enq_status_id = $result_arr[$i]["enq_no"];
        //echo "<br/> enq No = $enq_status_id";


        if (isset($enq_no_arr[$enq_status_id][0])) {
            $value = $enq_no_arr[$enq_status_id][0];
            $value++;
            $enq_no_arr[$enq_status_id][0] = $value;

            $enq_no_arr[$enq_status_id][1] = $enq_no_arr[$enq_status_id][1] . "," . $result_arr[$i]["enq_no"];
            $result_arr[$i] = array();
        } else {
            $enq_no_arr[$enq_status_id][0] = 1;
            $enq_no_arr[$enq_status_id][1] = $result_arr[$i]["enq_no"];
        }
    }

    //echo "<pre>";
    //print_r($enq_no_arr);
    //echo "</pre> count = [" . count($enq_no_arr) . "]";



    $new_enq = 0;
    $followup_count = 0;
    $enq_status_id = 0;
    $enq_status = array();
    $followup_status = array('done' => 0, 'critcal' => 0, 'pending' => 0, 'cancel' => 0, 'close' => 0);
    $followup_status_data = array();
//var_dump($followup_status);
    for ($i = 0; $i < count($result_arr); $i++) {

        if (isset($result_arr[$i]["e_status"])) {

            $enq_new = FALSE;
            $followup_count++;
            if ((date("d-m-Y", strtotime($result_arr[$i]["enq_date"])) >= date("d-m-Y", strtotime($from_date))) && (date("d-m-Y", strtotime($result_arr[$i]["enq_date"])) <= date("d-m-Y", strtotime($to_date)))) {
                $new_enq++;
                $enq_new = TRUE;   //e_status
                if($result_arr[$i]["e_status"]==7 || $result_arr[$i]["e_status"]==8 ){
                    echo "</br>".$result_arr[$i]["enq_no"]." = ".$result_arr[$i]["e_status"];
                }
                //echo "</br>".$result_arr[$i]["enq_no"];
            }

            $enq_status_id = $result_arr[$i]["e_status"];
            if (isset($enq_status[$enq_status_id])) {
                $value = $enq_status[$enq_status_id]['count'];
                $value++;
                $enq_status[$enq_status_id]['count'] = $value;

                if ($enq_new) {
                    $value = $enq_status[$enq_status_id]['new'];
                    $value++;
                    $enq_status[$enq_status_id]['new'] = $value;
                    $enq_status[$enq_status_id]['new_id'] = $enq_status[$enq_status_id]['new_id'].",".$result_arr[$i]["enq_no"];
                } else {
                    $value = $enq_status[$enq_status_id]['old'];
                    $value++;
                    $enq_status[$enq_status_id]['old'] = $value;
                    $enq_status[$enq_status_id]['old_id'] = $enq_status[$enq_status_id]['old_id'].",".$result_arr[$i]["enq_no"];
                }
            } else {
                $enq_status[$enq_status_id]['count'] = 1;
                if ($enq_new) {
                    $enq_status[$enq_status_id]['new'] = 1;
                    $enq_status[$enq_status_id]['new_id'] = $result_arr[$i]["enq_no"];
                } else {
                    $enq_status[$enq_status_id]['old'] = 1;
                    $enq_status[$enq_status_id]['old_id'] = $result_arr[$i]["enq_no"];
                }
            }



            //echo "<br/>" . date("d-m-Y", strtotime($result_arr[$i]["followup_date"])) . " = " . date("d-m-Y", strtotime("+1 day", $date));

            if (strtotime($result_arr[$i]["followup_date"]) > strtotime($result_arr[$i]["next_date"])) {
                $followup_status_code = "Done";
                $followup_status['done'] = $followup_status['done'] + 1;

                $followup_status_data[0][0] = "Done";
                $followup_status_data[0][1] = $followup_status['done'];
                $followup_status_data[0][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
            } elseif (strtotime($result_arr[$i]["followup_date"]) < strtotime("+1 day", $date)) {
                if ($enq_status_id <> 5 && $enq_status_id <> 8) {


                    $followup_status['pending'] = $followup_status['pending'] + 1;
                    //echo "<br/> I am in ";
                    $followup_status_data[2][0] = "Pending";
                    $followup_status_data[2][1] = $followup_status['pending'];
                    $followup_status_data[2][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
                }
            }




            /*
              elseif (strtotime(date("Y-m-d H:i:s")) > strtotime($result_arr[$i]["followup_date"])) {
              $followup_status_code = "Critical";
              $followup_status['critcal'] = $followup_status['critcal'] + 1;
              //$followup_status['pending'] = $followup_status['pending'] + 1;

              $followup_status_data[1][0] = "Critical";
              $followup_status_data[1][1] = $followup_status['critcal'];
              $followup_status_data[1][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
              } elseif (strtotime($result_arr[$i]["followup_date"]) == strtotime($result_arr[$i]["next_date"])) {
              $followup_status_code = "";
              $followup_status['pending'] = $followup_status['pending'] + 1;

              $followup_status_data[2][0] = "Pending";
              $followup_status_data[2][1] = $followup_status['pending'];
              $followup_status_data[2][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
              }

             */

            /*
              if ($followup_status_code == "") {
              if (strtotime($result_arr[$i]["enq_date"]) >= strtotime(date("Y-m-d"))) {
              if (strtotime($result_arr[$i]["followup_date"]) > strtotime($result_arr[$i]["enq_date"])) {
              //$followup_status = "Done";
              $followup_status['done'] = $followup_status['done'] + 1;

              $followup_status_data[3][0] = "Done";
              $followup_status_data[3][1] = $followup_status['done'];
              $followup_status_data[3][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
              }
              }
              }
             */
            if ($enq_status_id == 5) {
                //$followup_status_code = "Cancel";
                $followup_status['cancel'] = $followup_status['cancel'] + 1;

                $followup_status_data[4][0] = "Cancel";
                $followup_status_data[4][1] = $followup_status['cancel'];
                $followup_status_data[4][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
            }

            if ($enq_status_id == 8) {
                //$followup_status_code = "Close";
                $followup_status['close'] = $followup_status['close'] + 1;

                $followup_status_data[5][0] = "Close";
                $followup_status_data[5][1] = $followup_status['close'];
                $followup_status_data[5][2] = $followup_status_data[0][2] . "," . $result_arr[$i]["enq_no"];
            }
        }
    }

    //echo "<pre>" . print_r($followup_status_data,true) . "</pre>";
//echo "<br/>Total = $followup_count , New Enq = $new_enq";
    //var_dump($followup_status_data);
//var_dump($enq_status);
//var_dump($followup_status);

    $query = "SELECT id,status_name from tbl_status_master where rec_type = 1";
    list($status, $id, $status_arr) = getDataByQuery($query);

    for ($i = 0; $i < count($status_arr); $i++) {

        $enq_status_id = $status_arr[$i]["id"];
        if (isset($enq_status[$enq_status_id])) {
            $status_arr[$i]['count'] = $enq_status[$enq_status_id]['count'];
            $status_arr[$i]['new'] = $enq_status[$enq_status_id]['new'];
            $status_arr[$i]['old'] = $enq_status[$enq_status_id]['old'];
            
            $status_arr[$i]['new_id'] = $enq_status[$enq_status_id]['new_id'];
            $status_arr[$i]['old_id'] = $enq_status[$enq_status_id]['old_id'];
            
        } else {
            $enq_status[$enq_status_id] = 1;
            $status_arr[$i]['count'] = 0;
        }
    }
}


//var_dump($_SESSION);
echo "<pre>";
var_dump($status_arr);
echo "</pre>";
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
<?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner" style="min-height:1200px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Welcome  <?php echo $_SESSION['login_name']; ?>  </h2>
                        </div>
                    </div>

                    <hr />





                    <!-- COMMENT AND NOTIFICATION  SECTION -->
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="box">
                                <input type="hidden" name="quotation_add_enq_id" placeholder="Enquiry Number " value="521" class="form-control" id="quotation_add_enq_id">
                                <input type="hidden" name="ACTION" placeholder="Enquiry Number " value="AddQuotation" class="form-control" id="ACTION">
                                <div id="sortableTable" class="body collapse in">
                                    <table class="table table-bordered sortableTable responsive-table">

                                        <tbody>
                                            <tr><td colspan="3">
                                                    <span class="pull-right text-muted small">
                                                        <em><a href="send_email.php?rec_type=daily_report_1&from_date=<?php echo $date_2; ?>&tag=order_puja_details" ><?php echo "Email Tommorow Puja"; ?></a></em>
                                                    </span>
                                                </td></tr>
                                            <tr>


                                                <td style="text-align: center;">                                                    

                                                    <h2>
                                                        <img class="pull-center img-circle" alt="Total Followup" src="assets/img/dashboard_img/add_user_plus_male.png" style="width:80px;">
                                                        <span id="td_sub_total_amount"><?php echo "  " . $new_enq ?></span>
                                                    </h2>
                                                    <small class="pull-center text-muted"> <h3>New Users</h3> </small> 
                                                </td>
                                                <td style="text-align: center;" colspan="2">                                                    

                                                    <h2>
                                                        <img class="pull-center img-circle" alt="Total Followup" src="assets/img/dashboard_img/add_user_group.png" style="width:80px;">
                                                        <span id="td_sub_total_amount"><?php echo "  " . $followup_count ?></span>
                                                    </h2>
                                                    <small class="pull-center text-muted"> <h3>Total Followups</h3> </small> 
                                                </td>


                                            </tr>

                                            <tr>
                                                <td style="text-align: center;">                                                    

                                                    <h2>
                                                        <img class="pull-center img-circle" alt="Total Followup" src="assets/img/dashboard_img/previous_user_man.png" style="width:80px;">
                                                        <span id="td_sub_total_amount"><?php echo "  " . ($followup_count - $new_enq); ?></span>
                                                    </h2>
                                                    <small class="pull-center text-muted"> <h3>Previous Users</h3> </small> 
                                                </td>



                                                <td style="text-align: center;">                                                    

                                                    <h2><?php echo $followup_status['done']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Done </small> </h4>
                                                    <br/>
                                                    <h2><?php echo $followup_status['cancel']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Cancel </small> </h4>
                                                </td>
                                                <td style="text-align: center;"> 
                                                    <h2><?php echo $followup_status['pending']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Pending </small> </h4>
                                                    <br/>
                                                    <h2><?php echo $followup_status['close']; ?></h2>
                                                    <h4> <small class="pull-center text-muted"> Close </small> </h4>
                                                </td>

                                            </tr>



                                        </tbody>
                                    </table>





                                </div>
                            </div>

                        </div>



                        <div class="col-lg-5">

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <i class="icon-bell"></i> Status Report
                                    <span class="pull-right text-muted small">
                                        <em><a href="send_email.php?rec_type=daily_report_1&from_date=<?php echo $day_1; ?>&tag=order_puja_details" ><?php echo "Email"; ?></a></em>
                                    </span>
                                </div>

                                <div class="panel-body">
                                    <div class="list-group">
<?php

function sortByOrder($a, $b) {
    return $b['count'] - $a['count'];
}

usort($status_arr, 'sortByOrder');

for ($i = 0; $i < count($status_arr); $i++) {
    $total_count = $total_count + $status_arr[$i]['count'];
    ?>
                                            <a href="#" class="list-group-item">
                                                <i class="icon-ok"></i> <?php echo $status_arr[$i]['status_name']; ?>
                                                <span class="pull-right text-muted small"><em><?php echo $status_arr[$i]['new']." + ".$status_arr[$i]['old']."  =  ".$status_arr[$i]['count']; ?></em>
                                                </span>
                                            </a>
                                        <?php } ?>




                                    </div>

                                    <a href="#" class="btn btn-default btn-block btn-primary"><?php echo "Total = $total_count"; ?></a>
                                </div>

                            </div>



                        </div>
                    </div>
                    <!-- END COMMENT AND NOTIFICATION  SECTION -->


                    <hr />
<?php /* ?>
  <!--BLOCK SECTION -->
  <div class="row">
  <div class="col-lg-12">
  <div style="text-align: center;">

  <a class="quick-btn" href="#">
  <i class="icon-check icon-2x"></i>
  <span> Products</span>
  <span class="label label-danger">2</span>
  </a>

  <a class="quick-btn" href="#">
  <i class="icon-envelope icon-2x"></i>
  <span>Messages</span>
  <span class="label label-success">456</span>
  </a>
  <a class="quick-btn" href="#">
  <i class="icon-signal icon-2x"></i>
  <span>Profit</span>
  <span class="label label-warning">+25</span>
  </a>
  <a class="quick-btn" href="#">
  <i class="icon-external-link icon-2x"></i>
  <span>value</span>
  <span class="label btn-metis-2">3.14159265</span>
  </a>
  <a class="quick-btn" href="#">
  <i class="icon-lemon icon-2x"></i>
  <span>tasks</span>
  <span class="label btn-metis-4">107</span>
  </a>
  <a class="quick-btn" href="#">
  <i class="icon-bolt icon-2x"></i>
  <span>Tickets</span>
  <span class="label label-default">20</span>
  </a>



  </div>

  </div>

  </div>
  <!--END BLOCK SECTION -->
  <hr />


  <div class="row">
  <div class="col-md-4 col-vlg-3 col-sm-6">
  <div class="tiles green m-b-10">
  <div class="tiles-body">
  <div class="controller"> <a class="reload" href="javascript:;"></a> <a class="remove" href="javascript:;"></a> </div>
  <div class="tiles-title text-black">OVERALL SALES </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Overall Visits</span> <span data-animation-duration="700" data-value="2415" class="item-count animate-number semi-bold">2,415</span>
  </div>
  </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Today's</span> <span data-animation-duration="700" data-value="751" class="item-count animate-number semi-bold">751</span>
  </div>
  </div>
  <div class="widget-stats ">
  <div class="wrapper last">
  <span class="item-title">Monthly</span> <span data-animation-duration="700" data-value="1547" class="item-count animate-number semi-bold">1,547</span>
  </div>
  </div>
  <div style="width:90%" class="progress transparent progress-small no-radius m-t-20">
  <div data-percentage="64.8%" class="progress-bar progress-bar-white animate-progress-bar" style="width: 64.8%;"></div>
  </div>
  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span></div>
  </div>
  </div>
  </div>
  <div class="col-md-4 col-vlg-3 col-sm-6">
  <div class="tiles blue m-b-10">
  <div class="tiles-body">
  <div class="controller"> <a class="reload" href="javascript:;"></a> <a class="remove" href="javascript:;"></a> </div>
  <div class="tiles-title text-black">OVERALL VISITS </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Overall Visits</span> <span animation-duration="700" data-="" data-value="15489" class="item-count animate-number semi-bold">15,489</span>
  </div>
  </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Today's</span> <span data-animation-duration="700" data-value="551" class="item-count animate-number semi-bold">551</span>
  </div>
  </div>
  <div class="widget-stats ">
  <div class="wrapper last">
  <span class="item-title">Monthly</span> <span data-animation-duration="700" data-value="1450" class="item-count animate-number semi-bold">1,450</span>
  </div>
  </div>
  <div style="width:90%" class="progress transparent progress-small no-radius m-t-20">
  <div data-percentage="54%" class="progress-bar progress-bar-white animate-progress-bar" style="width: 54%;"></div>
  </div>
  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span></div>
  </div>
  </div>
  </div>
  <div class="col-md-4 col-vlg-3 col-sm-6">
  <div class="tiles purple m-b-10">
  <div class="tiles-body">
  <div class="controller"> <a class="reload" href="javascript:;"></a> <a class="remove" href="javascript:;"></a> </div>
  <div class="tiles-title text-black">SERVER LOAD </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Overall Load</span> <span data-animation-duration="700" data-value="5695" class="item-count animate-number semi-bold">5,695</span>
  </div>
  </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Today's</span> <span data-animation-duration="700" data-value="568" class="item-count animate-number semi-bold">568</span>
  </div>
  </div>
  <div class="widget-stats ">
  <div class="wrapper last">
  <span class="item-title">Monthly</span> <span data-animation-duration="700" data-value="12459" class="item-count animate-number semi-bold">12,459</span>
  </div>
  </div>
  <div style="width:90%" class="progress transparent progress-small no-radius m-t-20">
  <div data-percentage="90%" class="progress-bar progress-bar-white animate-progress-bar" style="width: 90%;"></div>
  </div>
  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span></div>
  </div>
  </div>
  </div>
  <div class="col-md-4 col-vlg-3 visible-xlg visible-sm col-sm-6">
  <div class="tiles red m-b-10">
  <div class="tiles-body">
  <div class="controller"> <a class="reload" href="javascript:;"></a> <a class="remove" href="javascript:;"></a> </div>
  <div class="tiles-title text-black">OVERALL SALES </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Overall Sales</span> <span data-animation-duration="700" data-value="5669" class="item-count animate-number semi-bold">5,669</span>
  </div>
  </div>
  <div class="widget-stats">
  <div class="wrapper transparent">
  <span class="item-title">Today's</span> <span data-animation-duration="700" data-value="751" class="item-count animate-number semi-bold">751</span>
  </div>
  </div>
  <div class="widget-stats ">
  <div class="wrapper last">
  <span class="item-title">Monthly</span> <span data-animation-duration="700" data-value="1547" class="item-count animate-number semi-bold">1,547</span>
  </div>
  </div>
  <div style="width:90%" class="progress transparent progress-small no-radius m-t-20">
  <div data-percentage="64.8%" class="progress-bar progress-bar-white animate-progress-bar" style="width: 64.8%;"></div>
  </div>
  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span></div>
  </div>
  </div>
  </div>
  </div>






  <!-- CHART & CHAT  SECTION -->
  <div class="row">
  <div class="col-lg-8">
  <div class="panel panel-default">
  <div class="panel-heading">
  Real Time Traffic
  </div>


  <div class="demo-container">
  <div id="placeholderRT" class="demo-placeholder"></div>
  </div>

  </div>

  </div>


  <div class="col-lg-4">

  <div class="chat-panel panel panel-primary">
  <div class="panel-heading">
  <i class="icon-comments"></i>
  Chat
  <div class="btn-group pull-right">
  <button type="button" data-toggle="dropdown">
  <i class="icon-chevron-down"></i>
  </button>
  <ul class="dropdown-menu slidedown">
  <li>
  <a href="#">
  <i class="icon-refresh"></i> Refresh
  </a>
  </li>
  <li>
  <a href="#">
  <i class=" icon-comment"></i> Available
  </a>
  </li>
  <li>
  <a href="#">
  <i class="icon-time"></i> Busy
  </a>
  </li>
  <li>
  <a href="#">
  <i class="icon-tint"></i> Away
  </a>
  </li>
  <li class="divider"></li>
  <li>
  <a href="#">
  <i class="icon-signout"></i> Sign Out
  </a>
  </li>
  </ul>
  </div>
  </div>

  <div class="panel-body">
  <ul class="chat">
  <li class="left clearfix">
  <span class="chat-img pull-left">
  <img src="assets/img/1.png" alt="User Avatar" class="img-circle" />
  </span>
  <div class="chat-body clearfix">
  <div class="header">
  <strong class="primary-font"> Jack Sparrow </strong>
  <small class="pull-right text-muted">
  <i class="icon-time"></i> 12 mins ago
  </small>
  </div>
  <br />
  <p>
  Lorem ipsum dolor sit amet, bibendum ornare dolor, quis ullamcorper ligula sodales.
  </p>
  </div>
  </li>
  <li class="right clearfix">
  <span class="chat-img pull-right">
  <img src="assets/img/2.png" alt="User Avatar" class="img-circle" />
  </span>
  <div class="chat-body clearfix">
  <div class="header">
  <small class=" text-muted">
  <i class="icon-time"></i> 13 mins ago</small>
  <strong class="pull-right primary-font"> Jhony Deen</strong>
  </div>
  <br />
  <p>
  Lorem ipsum dolor sit amet, consectetur a dolor, quis ullamcorper ligula sodales.
  </p>
  </div>
  </li>
  <li class="left clearfix">
  <span class="chat-img pull-left">
  <img src="assets/img/3.png" alt="User Avatar" class="img-circle" />
  </span>
  <div class="chat-body clearfix">
  <div class="header">
  <strong class="primary-font"> Jack Sparrow </strong>
  <small class="pull-right text-muted">
  <i class="icon-time"></i> 12 mins ago
  </small>
  </div>
  <br />
  <p>
  Lorem ipsum dolor sit amet, bibendum ornare dolor, quis ullamcorper ligula sodales.
  </p>
  </div>
  </li>
  <li class="right clearfix">
  <span class="chat-img pull-right">
  <img src="assets/img/4.png" alt="User Avatar" class="img-circle" />
  </span>
  <div class="chat-body clearfix">
  <div class="header">
  <small class=" text-muted">
  <i class="icon-time"></i> 13 mins ago</small>
  <strong class="pull-right primary-font"> Jhony Deen</strong>
  </div>
  <br />
  <p>
  Lorem ipsum dolor sit amet, consectetur a dolor, quis ullamcorper ligula sodales.
  </p>
  </div>
  </li>
  </ul>
  </div>

  <div class="panel-footer">
  <div class="input-group">
  <input id="Text1" type="text" class="form-control input-sm" placeholder="Type your message here..." />
  <span class="input-group-btn">
  <button class="btn btn-warning btn-sm" id="Button1">
  Send
  </button>
  </span>
  </div>
  </div>

  </div>


  </div>
  </div>
  <!--END CHAT & CHAT SECTION -->





  <!--  STACKING CHART  SECTION   -->
  <div class="row">
  <div class="col-lg-12">
  <div class="panel panel-default">
  <div class="panel-heading">
  Sales   Stacking
  </div>

  <div class="panel-body">

  <div class="demo-container">
  <div id="placeholderStack" class="demo-placeholder"></div>
  </div>
  <p class="stackControls">
  <button class="btn btn-primary" >With stacking</button>
  <button class="btn btn-primary">Without stacking</button>
  </p>

  <p class="graphControls">
  <button class="btn btn-primary">Bars</button>
  <button class="btn btn-primary">Lines</button>
  <button class="btn btn-primary">Lines with steps</button>
  </p>
  </div>

  </div>
  </div>

  </div>
  <!--END STACKING CHART SCETION  -->

  <!--TABLE, PANEL, ACCORDION AND MODAL  -->
  <div class="row">
  <div class="col-lg-6">
  <div class="box">
  <header>
  <h5>Simple Table</h5>
  <div class="toolbar">
  <div class="btn-group">
  <a href="#sortableTable" data-toggle="collapse" class="btn btn-default btn-sm accordion-toggle minimize-box">
  <i class="icon-chevron-up"></i>
  </a>
  </div>
  </div>
  </header>
  <div id="sortableTable" class="body collapse in">
  <table class="table table-bordered sortableTable responsive-table">
  <thead>
  <tr>
  <th>#<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
  <th>First Name<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
  <th>Last Name<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
  <th>Score<i class="icon-sort"></i><i class="icon-sort-down"></i> <i class="icon-sort-up"></i></th>
  </tr>
  </thead>
  <tbody>


  <tr>
  <td>1</td>
  <td>Jill</td>
  <td>Smith</td>
  <td>50</td>
  </tr>

  <tr>
  <td>2</td>
  <td>Eve</td>
  <td>Jackson</td>
  <td>94</td>
  </tr>

  <tr>
  <td>3</td>
  <td>John</td>
  <td>Doe</td>
  <td>80</td>
  </tr>

  <tr>
  <td>4</td>
  <td>Adam</td>
  <td>Johnson</td>
  <td>67</td>
  </tr>


  </tbody>
  </table>
  </div>
  </div>
  <div class="panel panel-primary">
  <div class="panel-heading ">
  Collapsible Accordion Panel Group
  </div>
  <div class="panel-body">
  <div class="panel-group" id="accordion">
  <div class="panel panel-default">
  <div class="panel-heading">
  <h4 class="panel-title">
  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Collapsible Group Item #1</a>
  </h4>
  </div>
  <div id="collapseOne" class="panel-collapse collapse in">
  <div class="panel-body">
  Lorem ipsum dolor sit amet, luaute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  </div>
  </div>
  <div class="panel panel-default">
  <div class="panel-heading">
  <h4 class="panel-title">
  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Collapsible Group Item #2</a>
  </h4>
  </div>
  <div id="collapseTwo" class="panel-collapse collapse">
  <div class="panel-body">
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  </div>
  </div>
  </div>
  <div class="panel panel-default">
  <div class="panel-heading">
  <h4 class="panel-title">
  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Collapsible Group Item #3</a>
  </h4>
  </div>
  <div id="collapseThree" class="panel-collapse collapse">
  <div class="panel-body">
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  <div class="col-lg-6">
  <div class="panel panel-primary">
  <div class="panel-heading">
  Primary Panel
  </div>
  <div class="panel-body">
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
  </div>
  <div class="panel-footer">
  Panel Footer
  </div>
  </div>
  <div class="panel panel-default">
  <div class="panel-heading">
  Context Classes
  </div>
  <div class="panel-body">
  <div class="table-responsive">
  <table class="table">
  <thead>
  <tr>
  <th>#</th>
  <th>First Name</th>
  <th>Last Name</th>
  <th>Username</th>
  </tr>
  </thead>
  <tbody>
  <tr class="success">
  <td>1</td>
  <td>Mark</td>
  <td>Otto</td>
  <td>@mdo</td>
  </tr>
  <tr class="info">
  <td>2</td>
  <td>Jacob</td>
  <td>Thornton</td>
  <td>@fat</td>
  </tr>
  <tr class="warning">
  <td>3</td>
  <td>Larry</td>
  <td>the Bird</td>
  <td>@twitter</td>
  </tr>
  <tr class="danger">
  <td>4</td>
  <td>John</td>
  <td>Smith</td>
  <td>@jsmith</td>
  </tr>
  </tbody>
  </table>
  </div>
  </div>
  </div>
  <div class="panel panel-default">
  <div class="panel-heading">
  Modal Example
  </div>
  <div class="panel-body">
  <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch Demo Modal
  </button>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="myModalLabel">Modal title</h4>
  </div>
  <div class="modal-body">
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary">Save changes</button>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>

  </div>
  <!--TABLE, PANEL, ACCORDION AND MODAL  -->


  <?php */ ?>









                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
<?php //include 'right_menu.php';         ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
<?php include 'footer.php'; ?>
        <!--END FOOTER -->

    </body>
    <!-- END BODY-->

</html>
