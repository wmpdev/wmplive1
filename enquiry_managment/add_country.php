<?php

//print_r($_POST);

?>

<form role="form" action="add_country.php" method="post">
    <!--
                                        <div class="form-group">
                                            <label>Text Input</label>
                                            <input class="form-control" />
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Text Input with Placeholder</label>
                                            <input class="form-control" placeholder="Enter text" />
                                        </div>
                                        <div class="form-group">
                                            <label>Static Control</label>
                                            <p class="form-control-static">email@example.com</p>
                                        </div>
    -->
    
    
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="H2">Add Country</h4>
    </div>
            
    <div class="alert alert-success alert-dismissable"  id="txtalert_div" style="display: none">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <div id="txtalert_success">  </div>
    </div>
        
    <div class="modal-body">

        
                <div class="form-group">
                    <label class="control-label col-lg-12">Country Name</label>
                    <input type="text" id="country_name" name="country_name" placeholder="Country Name" class="form-control">

                </div>
            
          
       
        
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-default" onclick="refresh_country()" >Refresh</button>
        <button type="button" class="btn btn-primary" onclick="add_country()" >Save changes</button>
    </div>
    
    
    
                                       
                                    </form>

<script>
function add_country() {
    
    var country_name = document.getElementById("country_name").value;
        if (country_name == "") {
        document.getElementById("country_name").value = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtalert_div").style.display = 'block';
                document.getElementById("txtalert_success").innerHTML = xmlhttp.responseText;
                refresh_country();
            }
            else{
                
            }
        }
        xmlhttp.open("GET","country_ajax.php?type=add&country_name="+country_name,true);
        xmlhttp.send();
    }
    
    
}


</script>

