<?php
$Main_Menu = "Services";
$Sub_Menu_1 = "list_pooja";
$page = "Pooja";
$page_title = "Send SMS";
$edit_id = 0;
include_once 'config/auth.php';
$error = FALSE;
$error_msg = "";
//print_r($_REQUEST);
$get_request = "";
/*
  for ($i = 0; $i < count($_REQUEST); $i++) {
  $_REQUEST[$i] = trim($_REQUEST[$i]);
  //$get_request .=$_REQUEST[$i] . "=>" . $_REQUEST[$i];
  }
 */
foreach ($_REQUEST as $key => $value) {
    $value = trim($value);
    $get_request .= " (" . $key . "->" . $value . ")";
    //echo "Key = $key && Value = $value";
}
//echo $get_request;
$tab = 'Info-Tab';
$rec_type = $_GET["rec_type"];
$enq_id = $_GET["enq_id"];
$puja_id = $_GET["puja_id"];
$rec_tag = $_GET["tag"];
$rec_order_id = 0;
$rec_enq_id = 0;
//$rec_format = "SMS";
$rec_user = "Info";
if ($rec_type == "Order") {
    if ($enq_id > 0) {
        $puja_data_arr = getDataFromTable("tbl_pooja_master");
        //$rec_type = "Order";
        $rec_order_id = $enq_id;
        $sms_type = 5;    // Sms_route:  4=Promostional 5=Tranzactional 
        list($status, $id, $result_arr) = getOrderTmpData($enq_id);
//print_r($result_arr);
        if ($status == 1) {
//print_r($result_arr);
//echo "<br/><br/>";
            if ($puja_id > 0) {
                list($puja_status, $id, $puja_data) = getOrderPujaTmpData($puja_id, $enq_id);
            } else {
                list($puja_status, $id, $puja_data) = getOrderPujaTmpData(0, $enq_id);
            }
//var_dump($puja_arr);
//echo " <br/>puja_array<br/>";
//list($status, $id, $msg) = send_SMS($result_arr, $puja_arr, $user);
            $edit_id = $result_arr[0]["id"];
            $order_no = $result_arr[0]["order_no"];
            list($order_date, $order_time) = validateDbDateTime($result_arr[0]["order_date"]);
            $order_status = $result_arr[0]["order_status"];
            $enq_no = $result_arr[0]["enq_no"];
            list($enq_date, $enq_time) = validateDbDateTime($result_arr[0]["enq_date"]);
            $cust_id = $result_arr[0]["cust_id"];
            if ($cust_id > 0) {
                list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);
//var_dump($cust_data_arr);            //echo " <br/>Cust_array<br/>";
                if ($cust_status) {
                    $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
                    $cust_no = $cust_data_arr["phone_1"];
                    $cust_email = $cust_data_arr["email_1"];
                    $cust_address = $cust_data_arr["address_1"];
                    $cust_city = $cust_data_arr["city"];
                    $cust_area = $cust_data_arr["area"];
                } else {
                    $cust_name = "No Customer - $pandit_id";
                }
            }
            $cust_service_request = $result_arr[0]["service_req"];
            $total_amount = $result_arr[0]["total_amt"];
            $pandit_payment = $result_arr[0]["pandit_payment_total"];
            $paid_to_pandit = $result_arr[0]["paid_to_pandit"];
            $payment_mode = $result_arr[0]["payment_mode"];
            $cust_payment_receive_status = $result_arr[0]["cust_pay_receive"];
            $pandit_payment_receive_status = $result_arr[0]["pandit_pay_status"];
            $collected_by = $result_arr[0]["collect_by"];
            $enquiry_handled_by = $result_arr[0]["enq_handle_by"];
            $order_handled_by = $result_arr[0]["ord_handle_by"];
            $feedback_type = $result_arr[0]["customer_feedback"];
            $comments = $result_arr[0]["comments"];
            $enq_details = "";
            $info_sms_msg = "";
            $user_sms_msg = "";
            $pandit_sms_msg = "";
            $pandit_sms_no = array();
            if ($puja_data[0]["id"] > 0) {
                for ($i = 0; $i < count($puja_data); $i++) {
                    list($puja_date, $puja_time) = validateDbDateTime($puja_data[$i]["puja_date"]);
                    $puja_address = $puja_data[$i]["puja_address"];
                    $samagri_arranged_by = $puja_data[$i]["samagri_arrange_by"];
                    $samagri_type = $puja_data[$i]["samagri_type"];
                    $collected_by = $puja_data[$i]["collected_by"];
                    $total_amount = $puja_data[$i]["total_amt"];
                    $payment_mode = $puja_data[$i]["payment_mode"];
                    $pandit_payment = $puja_data[$i]["pandit_payment_total"];
                    $payment_comments = $puja_data[$i]["payment_comments"];
                    $puja_id = $puja_data[$i]["puja_id"];
                    list($status, $id, $msg) = searchIdInArray($puja_id, $puja_data_arr, 'id');
                    if ($status == 1) {
                        $puja_name = $puja_data_arr[$id]["pooja_name"];  //print_r($puja_data_arr[$id]);                    
                    } else {
                        $puja_name = "No Puja Found - $puja_id";
                    }
                    $pandit_id = $puja_data[$i]["pandit_id"];
                    if ($pandit_id > 0) {
                        list($pandit_status, $id, $pandit_data_arr) = getPanditData($pandit_id);
//var_dump($pandit_data_arr);
                        if ($pandit_status) {
                            $pandit_name = $pandit_data_arr[0]["first_name"] . " " . $pandit_data_arr[0]["last_name"];
                            $pandit_contact = $pandit_data_arr[0]["phone_1"];
                            $pandit_reference = $puja_data[$i]["pandit_refrance"];
                        }
                    } else {
                        $pandit_name = "No Pandit - $pandit_id";
                        $pandit_contact = "No Pandit contact";
                    }
                    $pandit_sms_no[] = $pandit_contact;
                    $info_puja_details_tmp .= '
Puja Name = ' . $puja_name . ' 
Date = ' . $puja_date . ' ' . $puja_time . '
Address = ' . $puja_address . ' 
Samagri By = ' . $samagri_type . '
Pandit Name = ' . $pandit_name . ' ' . $pandit_contact . '
Payment Mode = ' . $payment_mode . '
Customer Payment = ' . $total_amount . '
Pandit Payment = ' . $pandit_payment . '
';
                    $user_puja_details_tmp .= '
Puja Name = ' . $puja_name . ' 
Date = ' . $puja_date . ' ' . $puja_time . '
Address = ' . $puja_address . ' 
Samagri By = ' . $samagri_type . '
Pandit Name = ' . $pandit_name . ' 
Payment Mode = ' . $payment_mode . '
Customer Payment = ' . $total_amount . '
';
                    $pandit_puja_details_tmp .= '
Puja Name = ' . $puja_name . ' 
Date = ' . $puja_date . ' ' . $puja_time . '
Address = ' . $puja_address . ' 
Samagri By = ' . $samagri_type . '
Pandit Name = ' . $pandit_name . ' 
Payment Mode = ' . $payment_mode . '
Customer Payment = ' . $total_amount . '
Pandit Payment = ' . $pandit_payment . '
';
                }
            }
            $info_sms_no = "8108869247";
            $info_sms_msg = 'Hi Team,
Customer = ' . $cust_name . ' ' . $cust_no . '' . $info_puja_details_tmp;
//echo "<br/><br/>$sms_msg<br/><br/>";
            if ($cust_no) {
                $user_sms_no = $cust_no;
                $user_sms_msg = 'Dear ' . $cust_name . ' ' . $cust_no . '' . $user_puja_details_tmp;
//echo "<br/><br/>$sms_msg<br/><br/>";
            }
            if ($pandit_sms_no[0] > 0) {
                $pandit_sms_no = implode(',', $pandit_sms_no);
                $pandit_sms_msg = 'Namaste Panditji,
Customer = ' . $cust_name . ' ' . $cust_no . '' . $pandit_puja_details_tmp;
//echo "<br/><br/>$sms_msg<br/><br/>";
            }
            /*
              if ($format == "SMS") {
              list($status, $id, $msg) = send_SMS($result_arr, $puja_arr, $user);
              } elseif ($format == "EMAIL") {
              //echo "inside Email Condition";
              list($status, $id, $msg) = send_Email($result_arr, $puja_arr, $user);
              }
             */
        } else {
            $msg = $result_arr;
            echo " Error :- $msg";
        }
    }
}
if ($rec_type == "Enquiry") {
    $rec_enq_id = $enq_id;
    $cust_id = $_GET["cust_id"];
    $tab = $_GET["Tab"];
    $rec_tag = $_GET["tag"];
    $sms_type = 4;    // Sms_route:  4=Promostional 5=Tranzactional 
    //Customer-Tab  no_contact
    if ($rec_tag == "no_contact") {
        if ($enq_id > 0) {
            //$cust_id = $result_arr[0]["cust_id"];
            if ($cust_id > 0) {
                list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);
//var_dump($cust_data_arr);            //echo " <br/>Cust_array<br/>";
                if ($cust_status) {
                    $cust_fname = $cust_data_arr["first_name"];
                    //$cust_fname = $cust_data_arr["last_name"];
                    $cust_no = $cust_data_arr["phone_1"];
                    $cust_email = $cust_data_arr["email_1"];
                    $cust_address = $cust_data_arr["address_1"];
                    $cust_city = $cust_data_arr["city"];
                    $cust_area = $cust_data_arr["area"];
                } else {
                    $cust_name = "No Customer - $pandit_id";
                }
            }
            $tmp_msg = "";
            $tmp_msg = "Hi " . trim($cust_fname) . "ji We tried calling you for your Puja need but your number was out of service. Kindly reach us on +91 8100009797.

Regards,
Team Where's My Pandit";
            $info_sms_no = "8108869247";
            $info_sms_msg = $tmp_msg;
//echo "<br/><br/>$sms_msg<br/><br/>";
            if ($cust_no) {
                $user_sms_no = $cust_no;
                $user_sms_msg = $tmp_msg;
//echo "<br/><br/>$sms_msg<br/><br/>";
            }
        } else {
            echo "Error : Enquiry No not Specified";
        }
    }
}
//$cust_id = 0;
$enq_date = date("d-m-Y"); //date(d-m-Y);
$enq_time = date("h:i a"); //date(d-m-Y);
//$stmt = $location->list_Country();
//$stmt = $master->listPooja();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD-->
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->
        <!-- PAGE LEVEL STYLES -->
        <!-- <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >
        <!-- MAIN WRAPPER -->
        <div id="wrap">
            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->
            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->
            <!--PAGE CONTENT -->
            <div id="content">
                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="nav nav-pills">
                                        <li class="<?php getActiveTab($tab, 'Info-Tab'); ?>">
                                            <a data-toggle="tab" href="#Info-Tab"><span> INFO </span></a>
                                        </li>
                                        <?php if (strlen($user_sms_msg) > 0) { ?>
                                            <li class="<?php getActiveTab($tab, 'Customer-Tab'); ?>">
                                                <a data-toggle="tab" href="#Customer-Tab"><span> Customer</span></a>
                                            </li>
                                        <?php } ?>
                                        <?php if (strlen($pandit_sms_msg) > 0) { ?>
                                            <li class="<?php getActiveTab($tab, 'Pandit-Tab'); ?>">
                                                <a data-toggle="tab" href="#Pandit-Tab">Pandit</a>
                                            </li>
                                        <?php } ?>
                                        <?php
                                        if ($enq_id > 0) {/* ?>
                                          <li class=""><a data-toggle="tab" href="#User-Details">User Details</a>
                                          </li>
                                          <li class="<?php getActiveTab($tab, 'quotation-details'); ?>">
                                          <a data-toggle="tab" href="#quotation-details"><span>  Quotation Details</span></a>
                                          </li>
                                          <?php if ($enq_quotation_id > 0) { ?>
                                          <li class="<?php getActiveTab($tab, 'Final-Details'); ?>">
                                          <a data-toggle="tab" href="#Final-Details">Final Details</a>
                                          </li>
                                          <?php
                                          }
                                         */
                                        }
                                        ?>
                                    </ul>
                                    <div class="tab-content" style="padding: 0px !important;">                                        
                                        <div id="Info-Tab" class="tab-pane fade <?php getActiveBody($tab, 'Info-Tab'); ?>">                                           
                                            <div class="box" id="info_msg" > <div class="col-lg-12">    </div>  </div>
                                            <form name="info_sms_form" id="info_sms_form" action="" method="POST" enctype="multipart/form-data"> 
                                                <?php
                                                $rec_user = "Info";
                                                $sms_msg = $info_sms_msg;
                                                $to_no = $info_sms_no;
                                                include 'include/blocks/send_sms_block.php';
                                                ?>                              
                                            </form>
                                        </div>
                                        <div id="Customer-Tab" class="tab-pane fade <?php getActiveBody($tab, 'Customer-Tab'); ?>">
                                            <div class="box" id="customer_msg" > <div class="col-lg-12">    </div>  </div>
                                            <form name="customer_sms_form" id="customer_sms_form" action="" method="POST" enctype="multipart/form-data">
                                                <?php
                                                $rec_user = "User";
                                                $sms_msg = $user_sms_msg;
                                                $to_no = $user_sms_no;
                                                include 'include/blocks/send_sms_block.php';
                                                ?>
                                            </form>
                                        </div>
                                        <div id="Pandit-Tab" class="tab-pane fade <?php getActiveBody($tab, 'Pandit-Tab'); ?>">                                                                      <div class="box" id="pandit_msg" > <div class="col-lg-12">    </div>  </div>
                                            <form name="pandit_sms_form" id="pandit_sms_form" action="" method="POST" enctype="multipart/form-data"> 
                                                <?php
                                                $rec_user = "Pandit";
                                                $sms_msg = $pandit_sms_msg;
                                                $to_no = $pandit_sms_no;
                                                include 'include/blocks/send_sms_block.php';
                                                ?>                                
                                            </form>
                                        </div>
                                        <div id="Followup-Tab" class="tab-pane fade <?php getActiveBody($tab, 'Followup-Tab'); ?>">  
                                            <div class="box" id="followup_msg" >
                                                <div class="col-lg-12">            
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div id="enq_comments">
                                                        <?php //include 'include/blocks/list_followup.php';            ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div class="body">
                                                        <?php //include 'include/blocks/add_followup.php';            ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="quotation-details" class="tab-pane fade <?php getActiveBody($tab, 'quotation-details'); ?>">
                                            <div id="show_puja_quotation_details">
                                                <?php //include 'include/blocks/add_quotation_block.php';            ?>
                                            </div>
                                        </div>
                                        <div id="Final-Details" class="tab-pane fade <?php getActiveBody($tab, 'Final-Details'); ?>">
                                            <div id="show_order_details"> 
                                                <?php include 'include/blocks/add_order_block.php'; ?>
                                            </div>
                                        </div>
                                        <div id="User-Details" class="tab-pane fade <?php getActiveBody($tab, 'User-Details'); ?>">
                                            <div class="col-lg-6">
                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> User Details </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Number</td>
                                                                        <td><?php echo $phone_1 . "<br/>" . $phone_2; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email Id</td>
                                                                        <td><?php echo $email_1 . "<br/>" . $email_2; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td><?php echo $first_name . " " . $last_name; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address</td>
                                                                        <td><?php echo $address_1; ?></td>
                                                                <input type="hidden" value="<?php echo $address_1; ?>" id="cust_puja_address" name="cust_puja_address">
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <!-- <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button> -->
                                                                <a class="btn text-warning btn-xs btn-flat" href="add_user.php?edit_id=<?php echo $cust_id; ?>">
                                                                    <i class="icon-edit-sign icon-white"></i> Edit</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                           
                    <hr/><br/><br/>     
                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';                      ?>
            <!-- END RIGHT STRIP  SECTION -->
        </div>
        <!--END MAIN WRAPPER -->
        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->
        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script src="include/js/ninad.functions.enquiry.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <script>
            $(document).ready(function () {
                function getDoc(frame) {
                    var doc = null;
                    // IE8 cascading access check
                    try {
                        if (frame.contentWindow) {
                            doc = frame.contentWindow.document;
                        }
                    } catch (err) {
                    }
                    if (doc) { // successful getting content
                        return doc;
                    }
                    try { // simply checking may throw in ie8 under ssl or mismatched protocol
                        doc = frame.contentDocument ? frame.contentDocument : frame.document;
                    } catch (err) {
                        // last attempt
                        doc = frame.document;
                    }
                    return doc;
                }
                $("#info_sms_form").submit(function (e)
                {
                    $("#info_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;
                    var formObj = $(this);
                    var formURL = formObj.attr("action");
                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //  if(false)
                            {
                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/sms_order_details.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#info_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#info_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());
                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');
                        //hide it
                        iframe.hide();
                        //set form target to iframe
                        formObj.attr('target', iframeId);
                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#info_msg").html('' + data + '');
                        });
                    }
                });
                $("#customer_sms_form").submit(function (e)
                {
                    $("#customer_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;
                    var formObj = $(this);
                    var formURL = formObj.attr("action");
                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //  if(false)
                            {
                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/sms_order_details.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#customer_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#customer_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());
                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');
                        //hide it
                        iframe.hide();
                        //set form target to iframe
                        formObj.attr('target', iframeId);
                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#customer_msg").html('' + data + '');
                        });
                    }
                });
                $("#pandit_sms_form").submit(function (e)
                {
                    $("#pandit_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;
                    var formObj = $(this);
                    var formURL = formObj.attr("action");
                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //  if(false)
                            {
                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/sms_order_details.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#pandit_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#pandit_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());
                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');
                        //hide it
                        iframe.hide();
                        //set form target to iframe
                        formObj.attr('target', iframeId);
                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#pandit_msg").html('' + data + '');
                        });
                    }
                });
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->    
    </body>
    <!-- END BODY-->
</html>