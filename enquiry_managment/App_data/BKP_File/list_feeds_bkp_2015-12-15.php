<?php
// instantiate product object
$Main_Menu = "Users";
$Sub_Menu_1 = "list_user";
include_once 'auth.php';

$page_no = 0;
if (isset($_GET["page"])) {
    $page_no = $_GET["page"];
}
if (isset($_GET["user_email"])) {
    $user_email = $_GET["user_email"];
}


//print_r($_GET);
//$stmt = $user->listUsers();
list($status, $id, $data) = App_getFeeds(0, $page_no);
$result_arr = $data;

if(strlen($user_email)>0){
    
    if (validateEmail($user_email)) {
        echo "list_feed invalid email.";
        echo "Error";
        return 0;
        //return array($status, 0, "Error - App_UserFeedsLike invalid Email");
    }
    
    
    for ($i = 0; $i < count($result_arr); $i++) {
    $feed_id = $result_arr[$i]["id"];
    
    list($feed_status, $id, $feed_user_data) = App_getUserFeedsLike($feed_id, $user_email);
    //echo "Before : ".$result_arr[$i]["post_publish_date"];
    $result_arr[$i]["post_publish_date"] = date("d M. Y", strtotime($result_arr[$i]["post_publish_date"]) );
    //echo "  After : ".$result_arr[$i]["post_publish_date"];
    //echo "<br/>inside email [$status, $id, $feed_user_data] ";
    if($feed_status == 1){
        //array_push($result_arr[$i], $row);
        $result_arr[$i]["user_liked"] = "1";
    }
    else{
        $result_arr[$i]["user_liked"] = "0";
    }
}
    
    
}
//$user_email = $user_email;



if ($status) {
    //echo json_encode($result_arr) . "<br>";
    //$result_arr = urlencode($result_arr);
    //$result_arr = urlencode(serialize($result_arr));

    $data = array('Feed' => $result_arr);
    //print json_encode($data);
    print json_encode($data);
} else {
    echo "EMPTY";
}
exit();
//print_r($result_data);
//$log->LogMsg("Listing Of Users");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title>List Feeds</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES 
        <link href="assets/css/layout2.css" rel="stylesheet" />-->
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
<?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
<?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content" >

                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>List Feeds </h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            List Feeds Data
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50px">No</th>
                                                            <th style="width: 100px">Date</th>
                                                            <th style="width: 100px">Title</th>
                                                            <th style="width: 100px">Type</th>
                                                            <th>Url</th>                                                            
                                                            <!--<th>Reference</th>
                                                            <th>Cast</th>
                                                            <th>Country</th> -->
                                                            <th style="width: 100px">Image</th>
                                                            <th style="width:105px;">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php
$count = 0;
for ($i = 0; $i < count($result_arr); $i++) {
    $count++;
    $feed_date = date("d-M-Y", strtotime($result_arr[$i]['publish_date']));
    ?> 
                                                            <tr class="odd gradeX">
                                                                <td><?php echo $i + 1; ?></td>
                                                                <td><?php echo $feed_date; ?></td>
                                                                <td><?php echo $result_arr[$i]['title']; ?></td>
                                                                <td><?php echo $result_arr[$i]['type']; ?></td>
                                                                <td><?php echo $result_arr[$i]['url']; ?></td>
                                                                <td>
                                                                    <img src="media/<?php echo $result_arr[$i]['image']; ?>" alt="" border="0" style="display:block; width: 100px; " />


                                                                </td>






                                                                <td>
                                                                    <a href="add_feeds.php?edit_id=<?php echo $result_arr[$i]['id']; ?>" class="btn text-info btn-xs btn-flat">
                                                                        <i class="icon-list-alt icon-white"></i> edit</a>

                                                                </td>
                                                            </tr>
    <?php
}
?>





                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <hr />




                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
<?php //include 'right_menu.php';       ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
<?php include 'footer.php'; ?>
        <!--END FOOTER -->

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->    


    </body>
    <!-- END BODY-->

</html>
