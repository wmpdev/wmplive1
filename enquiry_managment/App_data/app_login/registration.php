<?php
include 'connect.php';
include 'security.php';

$response = array();
if (isset($_POST) && $_POST) {
	$unique_app_id = $_POST['unique_app_id'];

	// var_dump($_POST);

	if($unique_app_id){
		$mysqli->autocommit(FALSE);

		$query = "SELECT * FROM `keys` WHERE `unique_app_id` = '$unique_app_id'";

		$res = $mysqli->query($query);
		$row = $res->fetch_assoc();

		$secret_key = $row['secret_key'];

		$res->close();

		if($_POST['email_address']){
			$decrypted_email = Security::decrypt($_POST['email_address'], $secret_key);
			// $decrypted_email = openssl_decrypt($_POST['email_address'], "AES-256-CBC", $secret_key);
			// $decrypted_email = $mysqli->real_escape_string($_POST['email_address']);
		}else{
			$response['error_code'] = "1";
			$response['error_msg'] = "No email provided";
			$response['entity_id'] = "";
			echo json_encode($response);
			$mysqli->rollback();
			die;
		}

		if($_POST['signup_type'] == 0){
			// password used only for custom login
			$password = Security::decrypt($_POST['password'], $secret_key);
			// $password = openssl_decrypt($_POST['password'], "AES-256-CBC", $secret_key);
			// $password = $_POST['password'];
			if($password){
				$decrypted_password = $password;
			}else{
				$response['error_code'] = "1";
				$response['error_msg'] = "No password provided";
				$response['entity_id'] = "";
				echo json_encode($response);
				$mysqli->rollback();
				die;
			}
			// $decrypted_password = mcrypt_decrypt("MCRYPT_RIJNDAEL_128", $secret_key, $_POST['password'], "MCRYPT_MODE_CBC");
		}

		// check if user exists
		
		$query = "SELECT * FROM `customer_entity` WHERE `email` = '$decrypted_email'";
		
		$res = $mysqli->query($query);
		// $row = $res->fetch_assoc();

		$row_cnt = $res->num_rows;
		if($row_cnt){
			// error - exists
			if($_POST['signup_type'] == 1 || $_POST['signup_type'] == 2){
				$row = $res->fetch_assoc();
				$res->close();

				$entity_id = $row['entity_id'];
				
				$response['error_code'] = "0";
				$response['error_msg'] = "Success";
				$response['entity_id'] = "$entity_id";
				echo json_encode($response);
				$mysqli->commit();
				die;
			}else{
				$response['error_code'] = "1";
				$response['error_msg'] = "User already exists";
				$response['entity_id'] = "";
				echo json_encode($response);
				$mysqli->rollback();
				die;
			}
		}else{
			// does not exist
			$insert_query = "INSERT INTO `customer_entity`
			(`entity_type_id`, `attribute_set_id`, `website_id`, `email`, `group_id`, `store_id`, `created_at`, `is_active`, `disable_auto_group_change`) VALUES 
			('1', '0', '1', '$decrypted_email', '1', '1', NOW(), '1', '0')";

			$mysqli->real_query($insert_query);

			$entity_id = $mysqli->insert_id;

			$values = "";	

			if(isset($decrypted_password) && $decrypted_password){
				$chars = "asd546gr13gh8fd45gd48hj5rew85dsw6gjhjnbvcv";
				for ($i = 0, $salt = '', $lc = strlen($chars)-1; $i < 32; $i++) {
		            $salt .= $chars[mt_rand(0, $lc)];
		        }

				$password_hash = md5($salt.$decrypted_password).":".$salt;
				// $password_hash = md5($decrypted_password);
				$values .= "('1', '12', '$entity_id', '$password_hash'), ";
			}

			if(isset($_POST['first_name']) && $_POST['first_name']){
				$first_name = $mysqli->real_escape_string($_POST['first_name']);
				$values .= "('1', '5', '$entity_id', '$first_name'), ";
			}

			if(isset($_POST['last_name']) && $_POST['last_name']){
				$last_name = $mysqli->real_escape_string($_POST['last_name']);
				$values .= "('1', '7', '$entity_id', '$last_name'), ";
			}

			if(isset($_POST['dob']) && $_POST['dob']){
				$dob = $mysqli->real_escape_string($_POST['dob']);
				$values .= "('1', '11', '$entity_id', '$dob'), ";
			}

			if(isset($_POST['gender']) && $_POST['gender']){
				$gender = $mysqli->real_escape_string($_POST['gender']);
				$values .= "('1', '18', '$entity_id', '$gender'), ";
			}

			if(isset($_POST['telephone']) && $_POST['telephone']){
				$telephone = $mysqli->real_escape_string($_POST['telephone']);

				$customer_entity_address_insert_query = "INSERT INTO `customer_address_entity_varchar`
					(`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES 
					('1', '31', '$entity_id', '$telephone')";

				$mysqli->real_query($customer_entity_address_insert_query);
			}


			if($values){
				$values = rtrim($values, ", ");
				$customer_entity_insert_query = "INSERT INTO `customer_entity_varchar`
				(`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES " . $values;

				$mysqli->real_query($customer_entity_insert_query);
				$response['error_code'] = "0";
				$response['error_msg'] = "success";
				$response['entity_id'] = "$entity_id";
				$response['first_name'] = $_POST['first_name'];
				$response['last_name'] = $_POST['last_name'];
				echo json_encode($response);
				$mysqli->commit();
				die;
			}
		}
		$res->close();
	}else{
		$response['error_code'] = "1";
		$response['error_msg'] = "No unique_app_id provided";
		$response['entity_id'] = "";
		echo json_encode($response);
		$mysqli->rollback();
		// echo "error - empty unique_app_id";
		die;
	}
}else{
	$response['error_code'] = "1";
	$response['error_msg'] = "No post data provided";
	$response['entity_id'] = "";
	echo json_encode($response);
	$mysqli->rollback();
	// echo "error - post data empty";
	die;
}
?>