<?php
include 'connect.php';
include 'security.php';

$response= array();

if (isset($_POST) && $_POST) {
	$unique_app_id = $_POST['unique_app_id'];
	$mysqli->autocommit(FALSE);

	if($unique_app_id){
		$query = "SELECT * FROM `keys` WHERE `unique_app_id` = '$unique_app_id'";

		$res = $mysqli->query($query);
		$row = $res->fetch_assoc();

		$secret_key = $row['secret_key'];

		$res->close();

		if($_POST['email_address']){
			// $decrypted_email = openssl_decrypt($_POST['email_address'], "AES-256-CBC", $secret_key);
			$decrypted_email = Security::decrypt($_POST['email_address'], $secret_key);

			$query = "SELECT * FROM `customer_entity` WHERE `email` = '$decrypted_email'";
			
			$res = $mysqli->query($query);
			$row_cnt = $res->num_rows;

			if($row_cnt){
				$row = $res->fetch_assoc();
				$res->close();
				$entity_id = $row['entity_id'];

				if($_POST['signup_type'] == 1 || $_POST['signup_type'] == 2){
					$response['error_code'] = "0";
					$response['error_msg'] = "Success";
					$response['entity_id'] = "$entity_id";
					echo json_encode($response);
					$mysqli->commit();
					die;
				}
			}else{
				$res->close();
				$response['error_code'] = "1";
				$response['error_msg'] = "User does not exist";
				$response['entity_id'] = "";
				echo json_encode($response);
				$mysqli->rollback();
				die;
			}
		}else{
			$response['error_code'] = "1";
			$response['error_msg'] = "Empty email";
			$response['entity_id'] = "";
			echo json_encode($response);
			$mysqli->rollback();
			die;
		}
		
		if($_POST['signup_type'] == 0){
			// password used only for custom login
			$password = Security::decrypt($_POST['password'], $secret_key);
			// $password = openssl_decrypt($_POST['password'], "AES-256-CBC", $secret_key);
			if($password){
				$decrypted_password = $password;
				$password_hash = md5($decrypted_password);

				$query = "SELECT cev.`value_id`, cev.`value`, ea.`attribute_code` 
					FROM `customer_entity_varchar` AS `cev` 
					JOIN `eav_attribute` AS `ea` 
					ON cev.`attribute_id` = ea.`attribute_id` 
					WHERE cev.`entity_id` = '$entity_id'";

				// $query = "SELECT * FROM `customer_entity_varchar` WHERE `entity_id` = '$entity_id'";

				$res = $mysqli->query($query);

				while($row = $res->fetch_assoc()){
					$result[$row['attribute_code']] = $row;
				}
				// $row = $res->fetch_assoc();

				// var_dump($result);

				$hash = $result['password_hash']['value'];
				$hashArr = explode(':', $hash);

				switch(count($hashArr)){
					case 1:
						if(md5($decrypted_password) === $hash){
							$response['error_code'] = "0";
							$response['error_msg'] = "Success";
							$response['entity_id'] = "$entity_id";
							$response['first_name'] = $result['firstname']['value'];
							$response['last_name'] = $result['lastname']['value'];
							echo json_encode($response);
							$mysqli->commit();
							die;
						}else{
							$response['error_code'] = "1";
							$response['error_msg'] = "Invalid password";
							$response['entity_id'] = "";
							echo json_encode($response);
							$mysqli->rollback();
							die;
						}
						break;

					case 2:
						if(md5($hashArr[1].$decrypted_password) === $hashArr[0]){
							$response['error_code'] = "0";
							$response['error_msg'] = "Success";
							$response['entity_id'] = "$entity_id";
							$response['first_name'] = $result['firstname']['value'];
							$response['last_name'] = $result['lastname']['value'];
							echo json_encode($response);
							$mysqli->commit();
							die;
						}else{
							$response['error_code'] = "1";
							$response['error_msg'] = "Invalid Password";
							$response['entity_id'] = "";
							echo json_encode($response);
							$mysqli->rollback();
							die;
						}
						break;
				}

				if($password_hash == $row['value']){
					$response['error_code'] = "0";
					$response['error_msg'] = "Success";
					$response['entity_id'] = "$entity_id";
					$response['first_name'] = $result['firstname']['value'];
					$response['last_name'] = $result['lastname']['value'];
					echo json_encode($response);
					$mysqli->commit();
					die;
				}else{
					$response['error_code'] = "1";
					$response['error_msg'] = "Invalid Password";
					$response['entity_id'] = "";
					echo json_encode($response);
					$mysqli->rollback();
					die;
				}

			}else{
				$response['error_code'] = "1";
				$response['error_msg'] = "Empty Password";
				$response['entity_id'] = "";
				echo json_encode($response);
				$mysqli->rollback();
				die;
			}
			// $decrypted_password = mcrypt_decrypt("MCRYPT_RIJNDAEL_128", $secret_key, $_POST['password'], "MCRYPT_MODE_CBC");
		}
	}else{
		$response['error_code'] = "1";
		$response['error_msg'] = "Empty unique_app_id";
		$response['entity_id'] = "";
		echo json_encode($response);
		$mysqli->rollback();
		die;
	}
}else{
	$response['error_code'] = "1";
	$response['error_msg'] = "Empty post data";
	$response['entity_id'] = "";
	echo json_encode($response);
	$mysqli->rollback();
	die;
}
?>