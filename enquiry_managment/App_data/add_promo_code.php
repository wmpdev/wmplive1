<?php

// instantiate product object
$Main_Menu = "Users";
$Sub_Menu_1 = "list_user";
include_once 'auth.php';

$page_no = 0;

//feed_id,mob_no,user_email
$user_id = 0;
$user_email = "";
$gcm_code = "";
$user_name = "";
$user_phone = "";



if (isset($_REQUEST["user_id"])) {
    $user_id = $_REQUEST["user_id"];
}
if (isset($_REQUEST["user_email"])) {
    $user_email = $_REQUEST["user_email"];
}
if (isset($_REQUEST["device_id"])) {
    $device_id = $_REQUEST["device_id"];
}
if (isset($_REQUEST["promo_code"])) {
    $promo_code = $_REQUEST["promo_code"];
}



if (isset($_REQUEST["user_name"])) {
    $user_name = $_REQUEST["user_name"];
}
if (isset($_REQUEST["user_phone"])) {
    $user_phone = $_REQUEST["user_phone"];
}
// $user_id, $user_email, $gcm_id = 0, $user_name = "", $user_phone = ""
//print_r($_GET);
//exit();
//echo "<br/> Send Data = [$feed_id,$user_email,$like_status,$mob_no,$user_id] <br/>";
//$stmt = $user->listUsers();

list($status, $id, $data) = App_getDeviceUser($user_email, $device_id);
//var_dump($data);
if ($status) {
    $msg = "Error : This User Is Already Present. Email [$user_email] Device [$device_id]";
    //logmsg("App_addPromoUser -> " . $msg);
    //echo $msg;
    //$data = array('1' => $msg);
    $data = array('error_code' => 1, 'error_msg' => $msg);
    print json_encode($data);
    //var_dump($data);
    exit();
    //return array($status, 0, $msg);
}


list($status, $id, $data) = App_validatePromoCode($promo_code);
if (!$status) {
    $msg = "Error : In-valid Promo code [$promo_code]";
    //logmsg("App_addPromoUser -> " . $msg);
    //echo $msg;
    //$data = array('2' => $msg);
    $data = array('error_code' => 2, 'error_msg' => $msg);
    print json_encode($data);
    //var_dump($data);
    exit();
}

//echo "Promo Code is Valid";

list($status, $id, $data) = App_addRegisterUser(0, $user_email, $gcm_code = 0, $device_id,$user_name,$user_phone);
if (!$status) {
    $msg = "Error : Not able to add User [$data]";
    //logmsg("App_addPromoUser -> " . $msg);
    //echo $msg;
    //$data = array('3' => $msg);
    $data = array('error_code' => 3, 'error_msg' => $msg);
    print json_encode($data);
    //var_dump($data);
    exit();
}

$user_id = $id;

list($status, $id, $data) = App_addPromoUser($user_id, $promo_code, $device_id);
if ($status) {
    $msg = "Success";
    $data = array('error_code' => 0, 'error_msg' => $msg);
     print json_encode($data);
    //var_dump($data);
} else {
    $msg = "Error : Not able to add User [$data]";
    //logmsg("App_addPromoUser -> " . $msg);
    //echo $msg;
    $data = array('error_code' => 4, 'error_msg' => $msg);
    print json_encode($data);
    //var_dump($data);
}


exit();


//print_r($result_data);
//$log->LogMsg("Listing Of Users");
?>