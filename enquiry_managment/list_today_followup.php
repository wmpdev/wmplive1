<?php
$Main_Menu = "Today";
$Sub_Menu_1 = "today_followup";




$page = "Followup";

$page_title = "List Todays $page";
$edit_id = 0;
// instantiate product object
//echo "in enquiry";
include_once 'config/auth.php';
//$enqType = $master->listEnqType();
//$enqStatus = $master->listStatus(1);
//$list_pooja = $master->listPooja($type_id);
//include_once '../../config/auth.php';

if (!empty($_POST["enq_id"])) {
    $enq_id = $_POST["enq_id"];
}

$date = date('Y-m-d', time());
//$date = "2015-08-25";
list($status, $id, $followup_arr) = getFollowupByDay($date, 0);
list($status, $id, $followup_done_arr) = getFollowupByDay($date, 1);
//echo "<pre>";
//print_r($followup_arr);
//echo "</pre>";
//$log->LogMsg("Listing Of Users");
//print_r($enqType);
//print_r($_GET);
//print_r($_POST);
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
////$_SESSION["enq_cache"]="";
////print_r($_SESSION["enq_cache"]);
////exit();
//
//
//$qty_sum = 0;
// foreach($_SESSION['enq_cache'] as $order){
//     $qty_sum ++;
//}
//echo "<br/> count = ".count($_SESSION['enq_cache'])." qty = ".$qty_sum;



$cust_id = 0;

$enq_date = date("d-m-Y"); //date(d-m-Y);
$enq_time = date("h:i a"); //date(d-m-Y);
//$stmt = $location->list_Country();
$stmt = $master->listPooja();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->
        <!-- GLOBAL STYLES -->
        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->

        <!-- PAGE LEVEL STYLES -->
        <link href="assets/css/layout2.css" rel="stylesheet" />
        <link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
        <!-- END PAGE LEVEL  STYLES -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " >

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?></h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">

                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50px;">No</th>
                                                            <th style="width:75px;">Enq Date</th>
                                                            <th style="width:105px;">Puja_date</th>
                                                            <th style="width:160px;">Puja</th>
                                                            <th style="width:60px;">Action</th>
                                                            <th style="width:60px;">Followup</th>
                                                            <th style="width:50px;"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        for ($i = 0; $i < count($followup_arr); $i++) {


                                                            $enq_id = $followup_arr[$i]["id"];
                                                            $enq_no = $followup_arr[$i]["enq_no"];
                                                            $enq_date = $followup_arr[$i]["enq_date"];

                                                            $followup_date = $followup_arr[$i]["next_date"];
                                                            $followup_time = $followup_arr[$i]["next_time"];

                                                            $puja_name = "";
                                                            $puja_date_time = "";
                                                            for ($j = 1; $j < 5; $j++) {
                                                                //$index_no = $j + 1;
                                                                $puja = $followup_arr[$i]["puja_id_$j"];


                                                                $status = 0;
                                                                if ($puja > 0)
                                                                    list($status, $id, $puja_name_data) = getPujaData($puja);
                                                                if ($status == 1) {
                                                                    $puja_name = $puja_name . $puja_name_data["pooja_name"] . "<br/>";

                                                                    $puja_date_tmp = $followup_arr[$i]["puja_date_$j"];
                                                                    $puja_time_tmp = $followup_arr[$i]["puja_time_$j"];
                                                                    $puja_date = date("d-M-Y", strtotime($puja_date_tmp));
                                                                    $puja_time = date("h:i A", strtotime($puja_time_tmp));
                                                                    
                                                                    $puja_date_time = $puja_date_time.$puja_date." ".$puja_time."<br/>";
                                                                }
                                                            }

                                                            $enq_status = "";
                                                            for ($k = 0; $k < count($followup_done_arr); $k++) {
                                                                //echo "<br/>$enq_id == " . $followup_done_arr[$k]["id"];
                                                                if ($enq_id == $followup_done_arr[$k]["id"])
                                                                    $enq_status = "Done";
                                                            }

                                                            $enq_date = date("d-M-Y", strtotime($enq_date));


                                                            $followup_date = date("d-M-y", strtotime($followup_date));
                                                            $followup_time = date("h:i A", strtotime($followup_time));

                                                            echo '<tr class="odd gradeX">';
                                                            echo '<td>' . $enq_no . '</td>';
                                                            echo '<td>' . $enq_date . '</td>';
                                                            echo '<td><code>' . $puja_date_time. '</code></td>';
                                                            echo '<td>' . $puja_name . '</td>';
                                                            //echo '<td>';
                                                            ?> 

                                                        <td>
                                                            <a class="btn text-info btn-xs btn-flat" href="add_enquiry.php?edit_id=<?php echo $enq_id; ?>">
                                                                <i class="icon-list-alt icon-white"></i> Show</a>
                                                        </td>
                                                        <td><?php echo $followup_date . " " . $followup_time; ?></td>
                                                        <td><?php echo $enq_status; ?></td>

                                                        <?php
                                                        //echo '</td>';
                                                        echo '</tr>';
                                                        //echo '<option value='.$rowFilms['id'].'>'.$rowFilms['country_name'].'</option>'; 
                                                    }
                                                    ?>



                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    <hr />




                </div>




            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php include 'right_menu.php'; ?>
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>
        <script src="include/js/ninad.functions.enquiry.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->    

    </body>
    <!-- END BODY-->

</html>



