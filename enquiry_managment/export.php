<?php
include_once 'config/auth.php';
//$report_type = $_GET["type"];

$user_login_id = $_SESSION['login_id'];
$export_user_arr = array("1", "6", "7");
if (!in_array($user_login_id, $export_user_arr)) {
    echo "you are not authorised user to access this page.";
    exit;
}

if (!strlen(trim($_GET["type"])) > 1) {
    echo "Select Proper Type";
    exit;
}

// The function header by sending raw excel
header("Content-type: application/vnd-ms-excel");



$add_user = trim($_SESSION['login_name']);

$report_type = $_GET["type"];
$export_duration = "All";
if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    $end_date = $_GET["to_date"];
    $export_duration = "($start_date $end_date)";
}

$str_tmp = date("Y-m-d") . " $add_user $report_type $export_duration";
//$file_name = $report_type."_".date("Y-m-d")."_".$add_user.".xls";

$str_tmp = str_replace(" ", "-", $str_tmp);
$file_name = $str_tmp . ".xls";
// $file_name = "1234567890123456789012345678901234567890123456789.xsl";
// Defines the name of the export file "codelution-export.xls"
header("Content-Disposition: attachment; filename=$file_name");   //codelution-export.xls
// Add data table
//include 'index_report.php';
include 'list_export_data.php';
?>