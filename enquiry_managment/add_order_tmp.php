<?php
$Main_Menu = "Order";
$Sub_Menu_1 = "add_order_form_temp";

$page = "Order Details";
$page_title = "Order Details";
$edit_id = 0;

$order_tab = "active";
$order_body = "active in";

$edit_puja_tab = "";
$edit_puja_body = "";




//print_r($_GET);




if (isset($_GET['edit_id'])) {
    $edit_id = trim($_GET['edit_id']);
}


if (isset($_GET['puja_id'])) {
    $edit_puja_id = trim($_GET['puja_id']);

    $order_tab = "";
    $order_body = "";

    $edit_puja_tab = "active";
    $edit_puja_body = "active in";
}

if (isset($_GET['tab'])) {
    $tab = trim($_GET['tab']);
    $order_tab = "";
    $order_body = "";
}

include_once 'config/auth.php';

//include_once $include_path.'function/function.php';
//error_reporting(E_ERROR | E_PARSE);
//include_once 'function/function.php';
//print_r($_GET); 
//echo "<pre>";
//print_r($_SESSION[$enq_cache_id]);
//echo "</pre>";
//echo "edit_id :- ".$edit_id." inc path = ".$include_path;



$result_arr = "";

if ($edit_id > 0) {

    $enq_id = $edit_id;

    list($status, $id, $result_arr) = getOrderTmpData($edit_id);
    //print_r($result_arr);
    if ($status == 1) {
        //print_r($result_arr);


        $edit_id = $result_arr[0]["id"];
        $order_no = $result_arr[0]["order_no"];
        //$order_date = date("d-m-Y", strtotime($result_arr[0]["order_date"]));
        //$order_time = date("h:i a", strtotime($result_arr[0]["order_date"]));
        if (strlen($result_arr[0]["order_date"]) > 0) {
            list($order_date, $order_time) = validateDbDateTime($result_arr[0]["order_date"]);
        } else {
            $order_date = "";
            $order_time = "";
        }

        $order_status = $result_arr[0]["order_status"];
        $enq_no = $result_arr[0]["enq_no"];
        $enq_no_id = $result_arr[0]["enq_id"];

        //$enq_date = date("d-m-Y", strtotime($result_arr[0]["enq_date"]));
        //$enq_time = date("h:i a", strtotime($result_arr[0]["enq_date"]));
        list($enq_date, $enq_time) = validateDbDateTime($result_arr[0]["enq_date"]);

        $cust_id = $result_arr[0]["cust_id"];

        list($cust_status, $id, $cust_data_arr) = getUserData($cust_id);
        //print_r($result_arr);
        if ($cust_status == 1) {
            //print_r($result_arr);

            $cust_id = $cust_data_arr["id"];

            $cust_name = $cust_data_arr["first_name"] . " " . $cust_data_arr["last_name"];
            //$last_name = $cust_data_arr["last_name"];
            $cust_email = $cust_data_arr["email_1"] . " " . $cust_data_arr["email_2"];
            //$email_2 = $cust_data_arr["email_2"];
            $cust_no = $cust_data_arr["phone_1"] . " " . $cust_data_arr["phone_2"];
            //$phone_2 = $cust_data_arr["phone_2"];
            $cust_address = $cust_data_arr["address_1"];
            $cust_city = $cust_data_arr["city"];
            $cust_area = $cust_data_arr["area"];
            $source_id = $cust_data_arr["source_id"];

            //list($status, $id, $enq_data_arr) = getUserEnq($cust_id);
            //if ($status == 0)
            //$enq_data_arr = array();
            //print_r($enq_data_arr);
            //echo count($enq_data_arr);
        }

        /* $cust_name = $result_arr[0]["customer_name"];
          $cust_no = $result_arr[0]["customer_contact"];
          $cust_email = $result_arr[0]["customer_email"];
          $cust_city = $result_arr[0]["customer_city"];
          $cust_area = $result_arr[0]["customer_area"];
          $cust_address = $result_arr[0]["customer_address"];
         */
        $cust_service_request = $result_arr[0]["service_req"];



        /*
          $puja_name_1 = $result_arr[0]["puja_name1"];
          //$puja_date_1 = date("d-m-Y", strtotime($result_arr[0]["puja_date1"]));
          //$puja_time_1 = date("h:i a", strtotime($result_arr[0]["puja_date1"]));
          list($puja_date_1, $puja_time_1) = validateDbDateTime($result_arr[0]["puja_date1"]);

          $samagri_arranged_by_1 = $result_arr[0]["samagri_arrange_by1"];
          $puja_address_1 = $result_arr[0]["puja_address_1"];
          $samagri_type_1 = $result_arr[0]["samagri_type1"];
          $pandit_name_1 = $result_arr[0]["pandit_name1"];
          $pandit_contact_1 = $result_arr[0]["pandit_contact1"];
          $pandit_reference_1 = $result_arr[0]["pandit_refrance1"];

          $puja_name_2 = $result_arr[0]["puja_name2"];
          //$puja_date_2 = date("d-m-Y", strtotime($result_arr[0]["puja_date2"])); //$result_arr[0]["puja_date2"];
          //$puja_time_2 = date("h:i a", strtotime($result_arr[0]["puja_date2"]));  //$result_arr[0]["puja_date2"];
          list($puja_date_2, $puja_time_2) = validateDbDateTime($result_arr[0]["puja_date2"]);

          $samagri_arranged_by_2 = $result_arr[0]["samagri_arrange_by2"];
          $puja_address_2 = $result_arr[0]["puja_address_2"];
          $samagri_type_2 = $result_arr[0]["samagri_type2"];
          $pandit_name_2 = $result_arr[0]["pandit_name2"];
          $pandit_contact_2 = $result_arr[0]["pandit_contact2"];
          $pandit_reference_2 = $result_arr[0]["pandit_refrance2"];
         */
        $total_amount = $result_arr[0]["total_amt"];
        $pandit_payment = $result_arr[0]["pandit_payment_total"];
        $paid_to_pandit = $result_arr[0]["paid_to_pandit"];
        $payment_mode = $result_arr[0]["payment_mode"];
        $cust_payment_receive_status = $result_arr[0]["cust_pay_receive"];
        $pandit_payment_receive_status = $result_arr[0]["pandit_pay_status"];
        $collected_by = $result_arr[0]["collect_by"];

        $enquiry_handled_by = $result_arr[0]["enq_handle_by"];
        $order_handled_by = $result_arr[0]["ord_handle_by"];

        $feedback_type = $result_arr[0]["customer_feedback"];
        $comments = $result_arr[0]["comments"];
    } else {
        echo " Error :- $result_arr";
    }
}

//$puja_data_arr = getDataFromTable("tbl_pooja_master");
list($status, $id, $puja_data_arr) = getPujaData();
list($status, $id, $pandit_arr) = getPanditData(); //getDataFromTable("tbl_pandit");
//var_dump($pandit_arr);







/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">

                                        <li class="<?php echo $order_tab; ?>">
                                            <a data-toggle="tab" href="#enquiry_details"><span> Order Details</span></a>
                                        </li>
                                        <?php if ($enq_id > 0) { ?>
                                            <li class="<?php if ($tab == 'list') echo 'active'; ?>">
                                                <a data-toggle="tab" href="#Puja-Details">Puja Details</a>
                                            </li>
                                            <?php if ($edit_puja_id > 0) { ?>
                                                <li class="<?php if ($tab == 'edit') echo 'active'; ?>">
                                                    <a data-toggle="tab" href="#Edit-Puja">Edit Puja</a>
                                                </li>
                                            <?php } ?>
                                            <!--
                                        <li class="<?php if ($tab == 'add') echo 'active'; ?>">
                                            <a data-toggle="tab" href="#New-Puja">Add Puja</a>
                                        </li>
                                            -->
                                        <?php } ?>

                                        <!--
                                        <li class="">
                                            <a data-toggle="tab" href="#Followup-Tab">Followup</a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#quotation-details"><span>  Quotation Details</span></a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#settings-pills">Final Details</a>
                                        </li>

                                        <li class=""><a data-toggle="tab" href="#User-Details">User Details</a>
                                        </li>
                                        -->


                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                        <div id="enquiry_details" class="tab-pane fade <?php echo $order_body; ?>">


                                            <form name="order_tmp_form" id="order_tmp_form" action="" method="POST" enctype="multipart/form-data">
                                                <?php include 'include/blocks/add_order_tmp.php'; ?>
                                            </form>

                                        </div>

                                        <div id="Puja-Details" class="tab-pane fade <?php if ($tab == 'list') echo 'active in'; ?>">
                                            <?php include 'include/blocks/list_order_puja_tmp.php'; ?>
                                        </div>
                                        <?php if ($edit_puja_id > 0) { ?>
                                            <div id="Edit-Puja" class="tab-pane fade <?php if ($tab == 'edit') echo 'active in'; ?>">

                                                <form name="edit_order_puja_tmp_form" id="edit_order_puja_tmp_form" action="" method="POST" enctype="multipart/form-data">
                                                    <?php
                                                    include 'include/blocks/add_order_puja_tmp.php';
                                                    $edit_puja_id = 0;
                                                    ?>
                                                </form>

                                            </div>
                                        <?php } ?>

                                        <div id="New-Puja" class="tab-pane fade <?php if ($tab == 'add') echo 'active in'; ?>">

                                            <form name="add_order_puja_tmp_form" id="add_order_puja_tmp_form" action="" method="POST" enctype="multipart/form-data">
                                                <?php include 'include/blocks/add_order_puja_tmp.php'; ?>
                                            </form>

                                        </div>

                                        <div id="Followup-Tab" class="tab-pane fade">  

                                            <div class="box" id="followup_msg" >
                                                <div class="col-lg-12">            

                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div id="enq_comments">
                                                        <?php include 'include/blocks/list_followup.php'; ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div class="body">
                                                        <?php include 'include/blocks/add_followup.php'; ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div id="quotation-details" class="tab-pane fade">



                                        </div>



                                        <div id="home-pills" class="tab-pane fade">

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <!--
                                                    <header>
                                                        <h5>Details</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    -->
                                                    <div class="body collapse in" id="sortableTable">
                                                        <table class="table table-bordered sortableTable responsive-table">

                                                            <tbody>


                                                                <tr>
                                                                    <td>Pooja Date</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Pooja Time</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Medium</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <td>Budget</td>
                                                                    <td>
                                                                        <input id="budget" class="form-control" type="text" value="" placeholder="Budget" name="budget">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cast</td>
                                                                    <td>
                                                                        <input id="cast" class="form-control" type="text" value="" placeholder="Cast" name="cast">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Service Requested</td>
                                                                    <td>
                                                                        <input id="service" class="form-control" type="text" value="" placeholder="Service Request Id" name="service">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Order Status Id</td>
                                                                    <td>
                                                                        <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Address</td>
                                                                    <td>
                                                                        <textarea id="comment" class="form-control" placeholder="Enter Comments" name="comment"></textarea>
                                                                    </td>
                                                                </tr>





                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">



                                                    <div class="body">

                                                        <?php // include 'add_user_comments_view.php';        ?>      
                                                        <?php //include './address_details.php';            ?>
                                                    </div>

                                                </div>

                                            </div>



                                        </div>



                                        <div id="profile-pills" class="tab-pane fade">

                                            <div class="panel-body">

                                                <div class="col-lg-6">
                                                    <div class="box">

                                                        <div class="body">
                                                            <?php include './address_details.php'; ?>
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div id="settings-pills" class="tab-pane fade">

                                            <div class="col-lg-6">

                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> Add New Payment </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">

                                                                <tbody>


                                                                    <tr>
                                                                        <td>Date</td>
                                                                        <td>
                                                                            <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Payment Mode</td>
                                                                        <td>
                                                                            <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Payment</td>
                                                                        <td>
                                                                            <input id="medium" class="form-control" type="text" value="" placeholder="medium" name="medium">
                                                                        </td>
                                                                    </tr>







                                                                </tbody>
                                                            </table>

                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="col-lg-6">

                                                <div class="box">
                                                    <header>
                                                        <h5>Payment History</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="body collapse in" id="sortableTable">
                                                        <table class="table table-bordered sortableTable responsive-table">

                                                            <tbody>


                                                                <tr>
                                                                    <td>25-March-2015</td>
                                                                    <td>Online</td>
                                                                    <td>2000</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>17-August-2015</td>
                                                                    <td>Online</td>
                                                                    <td>5000</td>
                                                                </tr>






                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>



                                        <div id="User-Details" class="tab-pane fade">

                                            <div class="col-lg-6">

                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> User Details </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">

                                                                <tbody>


                                                                    <tr>
                                                                        <td>Number</td>
                                                                        <td><?php echo $phone_1 . "<br/>" . $phone_2; ?></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Email Id</td>
                                                                        <td><?php echo $email_1 . "<br/>" . $email_2; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td><?php echo $first_name . " " . $last_name; ?></td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address</td>
                                                                        <td><?php echo $address_1; ?></td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>




                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <!-- <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button> -->
                                                                <a class="btn text-warning btn-xs btn-flat" href="add_user.php?edit_id=<?php echo $cust_id; ?>">
                                                                    <i class="icon-edit-sign icon-white"></i> Edit</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="col-lg-6">

                                                <div class="box">
                                                    <header>
                                                        <h5>Payment History</h5>
                                                        <div class="toolbar">
                                                            <div class="btn-group">
                                                                <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                    <i class="icon-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="body collapse in" id="sortableTable">
                                                        <table class="table table-bordered sortableTable responsive-table">

                                                            <tbody>


                                                                <tr>
                                                                    <td>25-March-2015</td>
                                                                    <td>Online</td>
                                                                    <td>2000</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>17-August-2015</td>
                                                                    <td>Online</td>
                                                                    <td>5000</td>
                                                                </tr>






                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';                 ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->






        <script>
            $(document).ready(function ()
            {


                function getDoc(frame) {
                    var doc = null;

                    // IE8 cascading access check
                    try {
                        if (frame.contentWindow) {
                            doc = frame.contentWindow.document;
                        }
                    } catch (err) {
                    }

                    if (doc) { // successful getting content
                        return doc;
                    }

                    try { // simply checking may throw in ie8 under ssl or mismatched protocol
                        doc = frame.contentDocument ? frame.contentDocument : frame.document;
                    } catch (err) {
                        // last attempt
                        doc = frame.document;
                    }
                    return doc;
                }

                $("#order_tmp_form").submit(function (e)
                {
                    $("#order_tmp_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_order_tmp.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#order_tmp_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#order_tmp_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#order_tmp_msg").html('' + data + '');
                        });

                    }

                });

                $("#add_order_puja_tmp_form").submit(function (e)
                {
                    $("#add_order_tmp_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;
//alert("i am in add_order_puja_tmp_form");
                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_order_puja_tmp.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#add_order_tmp_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#add_order_tmp_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#add_order_tmp_msg").html('' + data + '');
                        });

                    }

                });

                $("#edit_order_puja_tmp_form").submit(function (e)
                {
                    $("#add_order_tmp_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;
//alert("i am in add_order_puja_tmp_form");
                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_order_puja_tmp.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#add_order_tmp_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#add_order_tmp_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#add_order_tmp_msg").html('' + data + '');
                        });

                    }

                });



            });


            function send_Puja_Data(edit_id, format, user, puja_id) {

                //alert("i am in send_Puja_Data = "+user);


                //var enq_id = document.getElementById("edit_id").value;
                //var enq_id = edit_id;
                //alert("i am in send_SMS = " + enq_id);
                //return 0;
                //var cust_id = document.getElementById("cust_id").value;
                var param = "enq_id=" + edit_id + "&format=" + format + "&user=" + user + "&puja_id=" + puja_id;

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("list_order_puja_tmp_msg").innerHTML = xmlhttp.responseText;
                        //document.getElementById("order_tmp_list_msg").innerHTML = "After Output ..... ";

                    }
                    else {
                        document.getElementById("list_order_puja_tmp_msg").innerHTML = "Loadding ..... ";
                    }
                }
                xmlhttp.open("POST", "actions/sms_order_tmp.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }

        </script>



        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
        <script src="assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/daterangepicker/moment.min.js"></script>
        <script src="assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
        <script src="assets/js/formsInit.js"></script>
        <script>
            $(function () {
                formInit();
            });
        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
