<?php
$Main_Menu = "Export";
//$Sub_Menu_1 = "enquiry_followup";

$page = "Export Reports";
$page_title = "Export Reports";
$edit_id = 0;

//print_r($_GET);


include_once 'config/auth.php';

$user_login_id = $_SESSION['login_id'];
$export_user_arr = array("1", "6", "7");
if (!in_array($user_login_id, $export_user_arr)) {
    echo "you are not authorised user to access this page.";
    exit;
}

$date = strtotime(date("Y-m-d"));
$day_1 = date("d-m-Y", $date);

if (isset($_GET["from_date"])) {
    $start_date = $_GET["from_date"];
    //$start_date = date("Y-m-d", strtotime($start_date));

    if (strlen($start_date) == 10) {
        $start_date = date("Y-m-d", strtotime($start_date));
    }
} else {
    $start_date = "";
    $start_date = date("Y-m-d", strtotime($day_1));  //$date;
}
if (isset($_GET["to_date"])) {
    $end_date = $_GET["to_date"];
    if (strlen($end_date) == 10) {
        $end_date = date("Y-m-d", strtotime($end_date));
    } else {
        $end_date = "";
    }
} else {
    $end_date = "";
}


if (isset($_GET["tab"])) {
    $tab = $_GET["tab"];
    //$end_date = date("Y-m-d", strtotime($end_date));
} else {
    $tab = "other";
}

//include "export.php";
//echo "Start Date :- ".$start_date;




//echo "[$day_selected]==[$day_1]  -> Tab = $tab";

/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> 
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    -->

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">
                                        
                                        <li class="active"><a data-toggle="tab" href="#User-Details">Choose Date</a>
                                        </li>
                                        
                                        
                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                       



                                        <div id="User-Details" class="tab-pane fade active in">

                                            <form name="search_followup" id="search_followup" action="export.php" method="get" enctype="multipart/form-data"  target="_blank" >
                                                <div class="col-lg-12">

                                                    <div class="box">                                 
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading"> Select Attributes</div>
                                                            <div class="panel-body">
                                                                <table class="table table-bordered sortableTable responsive-table">

                                                                    <tbody>


                                                                        <tr>
                                                                            <td>From Date </td>
                                                                            <td>
                                                                                <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="from_date" id="from_date" title="From Date">
                                                                            </td>
                                                                            <td>To Date </td>
                                                                            <td>
                                                                                <input type="text" value="" placeholder="dd-mm-yyyy" class="form-control" data-mask="99-99-9999" name="to_date" id="to_date" title="To Date">                                    
                                                                            </td>


                                                                        </tr>
                                                                        <tr>
                                                                            <td>Export Type </td>
                                                                            <td>
                                                                                <select class="form-control" tabindex="2" name="type" id="type"  title="Select Export Type">            
                                                                                    <option selected="" value="0">-</option>
                                                                                    <option  value="Enquiry">Enquiry</option> 
                                                                                    <option  value="Enquiry_Puja">Enquiry Puja</option>
                                                                                    <option  value="Order">Order</option>
                                                                                    <option  value="Order_Puja">Order Puja</option>
                                                                                    <option  value="Custome">Custome</option>

                                                                                </select>
                                                                            </td>
                                                                            <td> &nbsp; </td>
                                                                            <td> &nbsp; </td>


                                                                        </tr>



                                                                    </tbody> 
                                                                </table>




                                                            </div>
                                                            <div class="panel-footer" align="right"> 
                                                                <span class="input-group-btn">
                                                                    <!-- <input class="btn btn-success " type="submit" value="Save" name="Save" > -->
                                                                    <button id="btn-chat" class="btn btn-success btn-sm" > Export</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';                    ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->







        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <!-- <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>


        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>

        <script src="assets/plugins/daterangepicker/moment.min.js"></script>

        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script> -->
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>

        <script src="assets/plugins/dataTables/jquery.dataTables.js"></script>
        <script src="assets/plugins/dataTables/dataTables.bootstrap.js"></script>       
        <script src="assets/js/formsInit.js"></script>
        <script>

            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
            $(function () {
                formInit();
            });

        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
