<?php
$Main_Menu = "Users";
$Sub_Menu_1 = "List_Enq";

$page = "Enquiry Details";
$page_title = "Enquiry Details";
$edit_id = 0;

//print_r($_GET); 


if (isset($_POST['rec_id']))
    $edit_id = trim($_POST['rec_id']);

if (isset($_GET['edit_id']))
    $edit_id = trim($_GET['edit_id']);

if (isset($_GET['user_id']))
    $cust_id = trim($_GET['user_id']);


//$order_tab = "active";
//$order_body = "active in";


if (isset($_GET['tab'])) {
    $tab = trim($_GET['tab']);
    //$order_tab = "";
    //$order_body = "";
} else {
    $tab = "enquiry-details";
}


include_once 'config/auth.php';
//include_once $include_path.'function/function.php';
//error_reporting(E_ERROR | E_PARSE);
//include_once 'function/function.php';
//print_r($_GET); 
//echo "<pre>";
//print_r($_SESSION[$enq_cache_id]);
//echo "</pre>";
//echo "edit_id :- ".$edit_id." inc path = ".$include_path;

/*
if($_GET['action']=='delete' && isset($_GET['puja_id'])){ 
    
    $del_puja_id = $_GET['puja_id'];
    $sql = "delete from tbl_enq_puja where puja_name = $del_puja_id"; 
    list($status,$id,$del_data)=getDataByQuery($sql);
    //if($status == 1){ $user->SuccessMsg = "Puja Deleted Successfully"; }
    //if($status == 0){ $user->ErrorMsg = "Puja Not Deleted. Error :- ".$del_data; }
}
*/

$result_arr = "";

if ($edit_id > 0) {

    list($status, $id, $result_arr) = getEnqData($edit_id);

    if ($status == 1) {
        //print_r($result_arr[0]);
        $enq_id = $result_arr[0]["id"];
        $cust_id = $result_arr[0]["cust_id"];
        $enq_no = $result_arr[0]["enq_no"];

        $enq_date = $result_arr[0]["enq_date"];
        list($enq_date, $enq_time) = validateDbDateTime($enq_date);

        //$puja_date = $result_arr[0]["pooja_date"];
        //$puja_time = $result_arr[0]["pooja_time"];
        //$enq_date = date("d-m-Y", strtotime($enq_date));
        //$puja_date = date("d-m-Y", strtotime($puja_date));
        //$puja_time = date("h:i: A", strtotime($puja_time));
        /*
          $puja_db = array();
          for ($puja_count = 1; $puja_count < 5; $puja_count++) {

          $puja_id = 0;
          $puja_date = "";
          $puja_time = "";

          $puja_id = $result_arr[0]["puja_id_$puja_count"];

          if ($puja_id > 0) {
          $puja_date = date("d-m-Y", strtotime($result_arr[0]["puja_date_$puja_count"]));
          $puja_time = date("h:i A", strtotime($result_arr[0]["puja_date_$puja_count"]));
          }
          $puja_db[$puja_count]["puja_id"] = $puja_id;
          $puja_db[$puja_count]["puja_date"] = $puja_date;
          $puja_db[$puja_count]["puja_time"] = $puja_time;
          }
          $puja_id_1 = $result_arr[0]["puja_id_1"];
          $puja_id_2 = $result_arr[0]["puja_id_2"];
          $puja_id_3 = $result_arr[0]["puja_id_3"];

          $puja_date_1 = date("d-m-Y h:s A", strtotime($puja_date_1));
          $puja_date_2 = date("d-m-Y h:s A", strtotime($puja_date_2));
         */




        $enq_medium = $result_arr[0]["medium_id"];
        $enq_comment = $result_arr[0]["comment"];
        $enq_muhurat = $result_arr[0]["muhurat_advice"];
        $enq_pandit_lang = $result_arr[0]["pandit_lange_id"];
        $enq_pandit_cast = $result_arr[0]["caste_id"];
        $enq_pandit_specification = $result_arr[0]["other_specification"];
        $samagri_type = $result_arr[0]["samagri_type"];
        $imp_points = $result_arr[0]["Important_points"];

        $enq_status = $result_arr[0]["enq_status"];
        $enq_quotation_id = $result_arr[0]["quotation_id"];

        $invoice_no = $result_arr[0]["invoice_no"];
        $Invoice_prepared_by = $result_arr[0]["Invoice_prepared_by"];
        $link_sent = $result_arr[0]["link_sent"];
        $link_url = $result_arr[0]["link_url"];

        $invoice_mailed = $result_arr[0]["invoice_mailed"];
        $feedback = $result_arr[0]["feedback"];
        $approved_by_id = $result_arr[0]["approved_by_id"];

        $payment_status = $result_arr[0]["payment_status"];
        $order_status_id = $result_arr[0]["order_status_id"];
        $approved_by_id = $result_arr[0]["approved_by_id"];


        $enq_order_no = $result_arr[0]["order_no"];
        $enq_order_id = $result_arr[0]["order_id"];
    } else {
        echo " Error :- $result_arr";
    }
} else {
    list($status, $id, $enq_no_data) = get_enqMax();
    //print_r($enq_no_data);
    $value = $enq_no_data["enq_no"];
    $value = $value + 1;
    $enq_no = $value;
    $enq_date = date("d-m-Y");
    $enq_time = date("h:i a");
}


//$enq_no = str_pad($enq_no, 8, '0', STR_PAD_LEFT);


$source_arr = getDataFromTable("tbl_source_master");
$medium_arr = getDataFromTable("tbl_medium_master");
$language_arr = getDataFromTable("tbl_language_master");
$caste_arr = getDataFromTable("tbl_caste_master");


$pandit_arr = getDataFromTable("tbl_pandit");

//print_r($source_arr);
//$Service = $master->listService();
//$caste = $master->list_caste();


if ($cust_id > 0) {

    list($status, $id, $result_arr) = getUserData($cust_id);
    //print_r($result_arr);
    if ($status == 1) {
        //print_r($result_arr);

        $cust_id = $result_arr["id"];

        $first_name = $result_arr["first_name"];
        $last_name = $result_arr["last_name"];
        $email_1 = $result_arr["email_1"];
        $email_2 = $result_arr["email_2"];
        $phone_1 = $result_arr["phone_1"];
        $phone_2 = $result_arr["phone_2"];
        $address_1 = $result_arr["address_1"];
    } else {
        echo " Error :- $result_arr";
    }
}



/* The next line is used for debugging, comment or delete it after testing */
//print_r($_SESSION[$enq_cache_id]);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- BEGIN HEAD-->
    <head>

        <meta charset="UTF-8" />
        <title><?php echo $page_title; ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!--[if IE]>
           <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
           <![endif]-->


        <!-- GLOBAL STYLES -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="assets/css/theme.css" />
        <link rel="stylesheet" href="assets/css/MoneAdmin.css" />
        <link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
        <!--END GLOBAL STYLES -->


        <!--  <link href="assets/css/layout2.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="assets/plugins/validationengine/css/validationEngine.jquery.css" />    

        <!-- PAGE LEVEL STYLES -->

        <link href="assets/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/uniform/themes/default/css/uniform.default.css" />
        <link rel="stylesheet" href="assets/plugins/inputlimiter/jquery.inputlimiter.1.0.css" />
        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />
        <link rel="stylesheet" href="assets/plugins/colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/plugins/tagsinput/jquery.tagsinput.css" />
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css" />
        <link rel="stylesheet" href="assets/plugins/timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" href="assets/plugins/switch/static/stylesheets/bootstrap-switch.css" />

        <!-- END PAGE LEVEL  STYLES -->

        <style>
            .form-horizontal .control-label{ text-align: left;}    
            .error_strings{ color:red;}    
        </style>  


        <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css" />



    </head>
    <!-- END  HEAD-->
    <!-- BEGIN BODY-->
    <body class="padTop53 " onload="">

        <!-- MAIN WRAPPER -->
        <div id="wrap">


            <!-- HEADER SECTION -->
            <?php include 'header.php'; ?>
            <!-- END HEADER SECTION -->



            <!-- MENU SECTION -->
            <?php include 'left_menu.php'; ?>
            <!--END MENU SECTION -->


            <!--PAGE CONTENT -->
            <div id="content">

                <div class="inner">

                    <div class="row">
                        <div class="col-lg-12">
                            <h2><?php echo $page_title; ?> </h2>
                            <?php if (strlen($user->SuccessMsg)) { ?>    
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->SuccessMsg;
                                    $user->SuccessMsg = "";
                                    ?>.
                                </div>
                            <?php } elseif (strlen($user->ErrorMsg)) {
                                ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php
                                    echo $user->ErrorMsg;
                                    $user->ErrorMsg = "";
                                    ?>.
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <hr />


                    <div class="row">

                        <div class="col-lg-12">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <ul class="nav nav-pills">


                                        <li class=""><a data-toggle="tab" href="#User-Details">User Details</a>
                                        </li>
                                        <li class="<?php getActiveTab($tab, 'enquiry-details'); ?>">
                                            <a data-toggle="tab" href="#enquiry-details"><span> Enquiry Details</span></a>
                                        </li>

                                        <?php if ($enq_id > 0) { ?>
                                            <li class="<?php getActiveTab($tab, 'puja-details'); ?>">
                                                <a data-toggle="tab" href="#puja-details"><span> Puja Details</span></a>
                                            </li>
                                            <li class="<?php getActiveTab($tab, 'Followup-Tab'); ?>">
                                                <a data-toggle="tab" href="#Followup-Tab">Followup</a>
                                            </li>
                                            <li class="<?php getActiveTab($tab, 'quotation-details'); ?>">
                                                <a data-toggle="tab" href="#quotation-details"><span>  Quotation Details</span></a>
                                            </li>
                                            <?php if ($enq_quotation_id > 0) { ?>
                                                <li class="<?php getActiveTab($tab, 'Final-Details'); ?>">
                                                    <a data-toggle="tab" href="#Final-Details">Final Details</a>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>




                                    </ul>

                                    <div class="tab-content" style="padding: 0px !important;">                                        

                                        <div id="enquiry-details" class="tab-pane fade <?php getActiveBody($tab, 'enquiry-details'); ?>">
                                            <form name="enquiry_form" id="enquiry_form" action="" method="POST" enctype="multipart/form-data">
                                                <?php include 'include/blocks/add_enquiry_block.php'; ?>
                                            </form>

                                        </div>
                                        <div id="puja-details" class="tab-pane fade <?php getActiveBody($tab, 'puja-details'); ?>">

                                            <form name="puja_details" id="puja_details" action="" method="POST" enctype="multipart/form-data">
                                                <div id="show_puja_details"> 
                                                    <?php include 'include/blocks/add_enq_puja_block.php'; ?>
                                                </div>
                                            </form>


                                        </div>

                                        <div id="Followup-Tab" class="tab-pane fade <?php getActiveBody($tab, 'Followup-Tab'); ?>">  

                                            <div class="box" id="followup_msg" >
                                                <div class="col-lg-12">            

                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div id="enq_comments">
                                                        <?php include 'include/blocks/list_followup.php'; ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="box">
                                                    <div class="body">
                                                        <?php include 'include/blocks/add_followup.php'; ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div id="quotation-details" class="tab-pane fade <?php getActiveBody($tab, 'quotation-details'); ?>">

                                            <div id="show_puja_quotation_details">
                                                <?php include 'include/blocks/add_quotation_block.php'; ?>
                                            </div>


                                        </div>






                                        <div id="Final-Details" class="tab-pane fade <?php getActiveBody($tab, 'Final-Details'); ?>">


                                            <div id="show_order_details"> 
                                                <?php include 'include/blocks/add_order_block.php'; ?>
                                            </div>




                                        </div>



                                        <div id="User-Details" class="tab-pane fade <?php getActiveBody($tab, 'User-Details'); ?>">

                                            <div class="col-lg-6">

                                                <div class="box">                                 
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading"> User Details </div>
                                                        <div class="panel-body">
                                                            <table class="table table-bordered sortableTable responsive-table">

                                                                <tbody>


                                                                    <tr>
                                                                        <td>Number</td>
                                                                        <td><?php echo $phone_1 . "<br/>" . $phone_2; ?></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Email Id</td>
                                                                        <td><?php echo $email_1 . "<br/>" . $email_2; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td><?php echo $first_name . " " . $last_name; ?></td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address</td>
                                                                        <td><?php echo $address_1; ?></td>
                                                                <input type="hidden" value="<?php echo $address_1; ?>" id="cust_puja_address" name="cust_puja_address">
                                                                </tr>

                                                                </tbody>
                                                            </table>




                                                        </div>
                                                        <div class="panel-footer"> 
                                                            <span class="input-group-btn">
                                                                <!-- <button id="btn-chat" class="btn btn-success btn-sm" onclick="add_followup();"> Add</button> -->
                                                                <a class="btn text-warning btn-xs btn-flat" href="add_user.php?edit_id=<?php echo $cust_id; ?>">
                                                                    <i class="icon-edit-sign icon-white"></i> Edit</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>




                                            </div>

                                            <div class="col-lg-6">

                                                <!--                                             
                                                                                             <div class="box">
                                                                                                 <header>
                                                                                                     <h5>Payment History</h5>
                                                                                                     <div class="toolbar">
                                                                                                         <div class="btn-group">
                                                                                                             <a class="btn btn-default btn-sm accordion-toggle minimize-box" data-toggle="collapse" href="#sortableTable">
                                                                                                                 <i class="icon-chevron-up"></i>
                                                                                                             </a>
                                                                                                         </div>
                                                                                                     </div>
                                                                                                 </header>
                                                                                                 <div class="body collapse in" id="sortableTable">
                                                                                                     <table class="table table-bordered sortableTable responsive-table">
                                             
                                                                                                         <tbody>
                                             
                                             
                                                                                                             <tr>
                                                                                                                 <td>25-March-2015</td>
                                                                                                                 <td>Online</td>
                                                                                                                 <td>2000</td>
                                                                                                             </tr>
                                             
                                                                                                             <tr>
                                                                                                                 <td>17-August-2015</td>
                                                                                                                 <td>Online</td>
                                                                                                                 <td>5000</td>
                                                                                                             </tr>
                                             
                                             
                                             
                                             
                                             
                                             
                                                                                                         </tbody>
                                                                                                     </table>
                                                                                                 </div>
                                                                                             </div>
                                                -->
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>











                    </div>                           











                    <hr/><br/><br/>     










                </div>
            </div>
            <!--END PAGE CONTENT -->
            <!-- RIGHT STRIP  SECTION -->
            <?php //include 'right_menu.php';            ?>    
            <!-- END RIGHT STRIP  SECTION -->

        </div>

        <!--END MAIN WRAPPER -->

        <!-- FOOTER -->
        <?php include 'footer.php'; ?>
        <!--END FOOTER -->



        <script>


/*
            function add_followup() {

                //alert("i am in add_followup");


                var enq_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                var followup_date = document.getElementById("followup_date").value;
                var followup_time = document.getElementById("followup_time").value;

                var comment = document.getElementById("followup_comment").value;
                var next_date = document.getElementById("next_date").value;
                var next_time = document.getElementById("next_time").value;
                var enq_status = document.getElementById("enq_status").value;
                var monitor_by = document.getElementById("monitor_by").value;


                var param = "enq_id=" + enq_id
                        + "&followup_date=" + followup_date
                        + "&followup_time=" + followup_time
                        + "&comment=" + comment
                        + "&next_date=" + next_date
                        + "&next_time=" + next_time
                        + "&enq_status=" + enq_status
                        + "&monitor_by=" + monitor_by;



                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        var str = xmlhttp.responseText;
                        var n = str.search("success");
                        //alert(str);
                        document.getElementById("followup_msg").innerHTML = xmlhttp.responseText;

                        if (n > 0) {
                            list_followup();

                            document.getElementById("followup_comment").value = "";
                            document.getElementById("next_date").value = "";
                            document.getElementById("next_time").value = "";
                            document.getElementById("enq_status").value = 0;
                            document.getElementById("monitor_by").value = 0;
                        }
                    }
                }
                xmlhttp.open("POST", "actions/add_followup.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }
*/
            function list_followup() {

                //alert("i am in add_followup");


                var enq_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                var param = "enq_id=" + enq_id;

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("enq_comments").innerHTML = xmlhttp.responseText;

                    }
                }
                xmlhttp.open("POST", "include/blocks/list_followup.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }


            function list_puja_refresh() {

                //alert("i am in add_followup");


                var enq_id = document.getElementById("edit_id").value;
                var cust_puja_address = document.getElementById("cust_puja_address").value;
                var param = "enq_id=" + enq_id;

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("show_puja_details").innerHTML = xmlhttp.responseText;
                        document.getElementById("puja_address").value = cust_puja_address;

                    }
                }
                xmlhttp.open("POST", "include/blocks/add_enq_puja_block.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }

            /*
             function list_puja_quotation_refresh() {
             
             //alert("i am in add_followup");
             
             
             var enq_id = document.getElementById("edit_id").value;
             //var cust_id = document.getElementById("cust_id").value;
             var param = "enq_id=" + enq_id;
             
             var xmlhttp;
             if (window.XMLHttpRequest)
             {// code for IE7+, Firefox, Chrome, Opera, Safari
             xmlhttp = new XMLHttpRequest();
             }
             else
             {// code for IE6, IE5
             xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
             }
             xmlhttp.onreadystatechange = function ()
             {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
             {
             document.getElementById("show_puja_quotation_details").innerHTML = xmlhttp.responseText; 
             //document.getElementById("quotation_add_enq_id").value = enq_id;
             }
             }
             xmlhttp.open("POST", "include/blocks/add_quotation_block.php", true);
             xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
             xmlhttp.send(param);
             }
             
             */

            function confirmQuotation(enq_id, quotation_id) {

                //alert("i am in markQuotation = " + enq_id + " , " + quotation_id);
                //var enq_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                var param = "quotation_add_enq_id=" + enq_id + "&quotation_id=" + quotation_id + "&ACTION=ConfirmQuotation";

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        //$("#quotation_msg").html('' + data + '');
                        document.getElementById("quotation_msg").innerHTML = xmlhttp.responseText;

                    }
                }
                xmlhttp.open("POST", "actions/add_quotation.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }

            function convertOrder() {

                //alert("i am in markQuotation = " + enq_id + " , " + quotation_id);
                var enq_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                var param = "order_enq_id=" + enq_id + "&ACTION=ConvertOrder";

                var xmlhttp;
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function ()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        //$("#quotation_msg").html('' + data + '');
                        document.getElementById("order_msg").innerHTML = xmlhttp.responseText;

                    }
                }
                xmlhttp.open("POST", "actions/add_enq_order.php", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send(param);
            }

            function calculate_amount() {

                //alert("i am in add_followup");


                //var enq_id = document.getElementById("edit_id").value;
                //var cust_id = document.getElementById("cust_id").value;
                //var pandit_cost = parseInt(document.getElementById("pandit_cost").value, 10);
                var pandit_selling = parseInt(document.getElementById("pandit_selling").value, 10);

                var samagri_sukhi = parseInt(document.getElementById("samagri_sukhi").value, 10);
                var samagri_gili = parseInt(document.getElementById("samagri_gili").value, 10);
                var extra_cost = parseInt(document.getElementById("extra_cost").value, 10);
                var discount_cost = parseInt(document.getElementById("discount_cost").value, 10);
                var conveyance = parseInt(document.getElementById("conveyance").value, 10);
                //var monitor_by = document.getElementById("monitor_by").value;

                console.log("pandit_selling = " + pandit_selling);
                //console.log("samagri_sukhi " + samagri_sukhi);
                //console.log("discount_cost " + discount_cost);

                var sub_total = 0;
                var total = 0;

                if (!Number.isInteger(pandit_selling)) {
                    pandit_selling = 0;
                }
                if (!Number.isInteger(samagri_sukhi)) {
                    samagri_sukhi = 0;
                }
                if (!Number.isInteger(samagri_gili)) {
                    samagri_gili = 0;
                }
                if (!Number.isInteger(extra_cost)) {
                    extra_cost = 0;
                }
                if (!Number.isInteger(conveyance)) {
                    conveyance = 0;
                }
                if (!Number.isInteger(discount_cost)) {
                    discount_cost = 0;
                }
                console.log("pandit_selling = [" + pandit_selling + "] , samagri_sukhi = [" + samagri_sukhi + "] , samagri_gili = [" + samagri_gili + "] , extra_cost = [" + extra_cost + "], conveyance = [" + conveyance);

                sub_total = parseInt(pandit_selling + samagri_sukhi + samagri_gili + extra_cost + conveyance);
                total = (sub_total - discount_cost);

                console.log("sub_total " + sub_total + " , Total = " + total);
                //console.log("total " + total);

                document.getElementById("td_sub_total_amount").innerHTML = parseInt(sub_total);
                document.getElementById("td_total_amount").innerHTML = parseInt(total);


            }

            $(document).ready(function ()
            {


                function getDoc(frame) {
                    var doc = null;

                    // IE8 cascading access check
                    try {
                        if (frame.contentWindow) {
                            doc = frame.contentWindow.document;
                        }
                    } catch (err) {
                    }

                    if (doc) { // successful getting content
                        return doc;
                    }

                    try { // simply checking may throw in ie8 under ssl or mismatched protocol
                        doc = frame.contentDocument ? frame.contentDocument : frame.document;
                    } catch (err) {
                        // last attempt
                        doc = frame.document;
                    }
                    return doc;
                }

                $("#quotation_form").submit(function (e)
                {
                    var r = confirm("You Really want to save Quotation, You will not able to edit it later.");
                    if (r == true) {
                        //txt = "You pressed OK!";
                        $("#quotation_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                        $("html, body").animate({scrollTop: 0}, 600);
                        //return false;
                        //var enq_id = document.getElementById("edit_id").value;
                        //alert(" i am in quotation_form function");
                        //confirm("You Really want to save Quotation, You will not able to edit it later.");
                        var formObj = $(this);
                        var formURL = formObj.attr("action");


                        if (window.FormData !== undefined)  // for HTML5 browsers
                                //	if(false)
                                {

                                    var formData = new FormData(this);
                                    $.ajax({
                                        url: 'actions/add_quotation.php',
                                        type: 'POST',
                                        data: formData,
                                        mimeType: "multipart/form-data",
                                        contentType: false,
                                        cache: false,
                                        processData: false,
                                        success: function (data, textStatus, jqXHR)
                                        {
                                            //$("#quotation_msg").html('' + data + '');                                        

                                            if (data.indexOf("SUCCESS") >= 0) {
                                                //alert("Puja Added Successfully");
                                                //list_puja_quotation_refresh();
                                                $("#quotation_msg").html('' + data + '');
                                                //window.location.replace("http://stackoverflow.com");


                                            } else {
                                                $("#quotation_msg").html('' + data + '');
                                            }


                                        },
                                        error: function (jqXHR, textStatus, errorThrown)
                                        {
                                            $("#quotation_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                        }
                                    });
                                    e.preventDefault();
                                    //e.unbind();
                                }
                        else  //for olden browsers
                        {
                            //generate a random id
                            var iframeId = 'unique' + (new Date().getTime());

                            //create an empty iframe
                            var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                            //hide it
                            iframe.hide();

                            //set form target to iframe
                            formObj.attr('target', iframeId);

                            //Add iframe to body
                            iframe.appendTo('body');
                            iframe.load(function (e)
                            {
                                var doc = getDoc(iframe[0]);
                                var docRoot = doc.body ? doc.body : doc.documentElement;
                                var data = docRoot.innerHTML;
                                $("#quotation_msg").html('' + data + '');
                            });

                        }

                    }
                });


                $("#enquiry_form").submit(function (e)
                {
                    $("#enq_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_enquiry.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#enq_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#enq_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#enq_msg").html('' + data + '');
                        });

                    }

                });

                $("#add_followup_form").submit(function (e)
                {
                    $("#followup_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_followup.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        //$("#followup_msg").html('' + data + '');
                                        var str = data; //xmlhttp.responseText; 
                                        var n = str.search("success");
                                        //alert(str);
                                        document.getElementById("followup_msg").innerHTML = data; //xmlhttp.responseText;

                                        if (n > 0) {
                                            list_followup();

                                            document.getElementById("followup_comment").value = "";
                                            document.getElementById("next_date").value = "";
                                            document.getElementById("next_time").value = "";
                                            document.getElementById("enq_status").value = 0;
                                            document.getElementById("monitor_by").value = 0;
                                        }


                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#followup_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#followup_msg").html('' + data + '');
                        });

                    }

                });



                $("#puja_details").submit(function (e)
                {
                    $("#puja_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_enq_puja.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        //$("#puja_msg").html('' + data + '');

                                        if (data.indexOf("SUCCESS") >= 0) {
                                            //alert("Puja Added Successfully");
                                            list_puja_refresh();
                                        } else {
                                            $("#puja_msg").html('' + data + '');
                                        }

                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#puja_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                //e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#puja_msg").html('' + data + '');
                        });

                    }

                });

                $("#order_details").submit(function (e)
                {
                    $("#order_msg").html("<img src='loading.gif'/> Processing Data ...... ");
                    $("html, body").animate({scrollTop: 0}, 600);
                    //return false;

                    var formObj = $(this);
                    var formURL = formObj.attr("action");

                    if (window.FormData !== undefined)  // for HTML5 browsers
                            //	if(false)
                            {

                                var formData = new FormData(this);
                                $.ajax({
                                    url: 'actions/add_enq_order.php',
                                    type: 'POST',
                                    data: formData,
                                    mimeType: "multipart/form-data",
                                    contentType: false,
                                    cache: false,
                                    processData: false,
                                    success: function (data, textStatus, jqXHR)
                                    {
                                        $("#order_msg").html('' + data + '');
                                    },
                                    error: function (jqXHR, textStatus, errorThrown)
                                    {
                                        $("#order_msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '</code></pre>');
                                    }
                                });
                                e.preventDefault();
                                e.unbind();
                            }
                    else  //for olden browsers
                    {
                        //generate a random id
                        var iframeId = 'unique' + (new Date().getTime());

                        //create an empty iframe
                        var iframe = $('<iframe src="javascript:false;" name="' + iframeId + '" />');

                        //hide it
                        iframe.hide();

                        //set form target to iframe
                        formObj.attr('target', iframeId);

                        //Add iframe to body
                        iframe.appendTo('body');
                        iframe.load(function (e)
                        {
                            var doc = getDoc(iframe[0]);
                            var docRoot = doc.body ? doc.body : doc.documentElement;
                            var data = docRoot.innerHTML;
                            $("#order_msg").html('' + data + '');
                        });

                    }

                });



            });
        </script>



        <!-- PAGE LEVEL SCRIPT  -->
        <script src="assets/js/jquery-ui.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>
        <script src="assets/plugins/colorpicker/js/bootstrap-colorpicker.js"></script>
        <script src="assets/plugins/tagsinput/jquery.tagsinput.min.js"></script>
        <script src="assets/plugins/validVal/js/jquery.validVal.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/daterangepicker/moment.min.js"></script>
        <script src="assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="assets/plugins/jasny/js/bootstrap-inputmask.js"></script>
        <script src="assets/js/formsInit.js"></script>
        <script>
            $(function () {
                formInit();
            });
        </script>

        <!--END PAGE LEVEL SCRIPT-->






    </body>
    <!-- END BODY-->

</html>
