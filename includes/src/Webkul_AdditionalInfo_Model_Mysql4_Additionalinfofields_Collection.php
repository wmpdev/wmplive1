<?php

	class Webkul_AdditionalInfo_Model_Mysql4_Additionalinfofields_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	    public function _construct() {
	        parent::_construct();
	        $this->_init("additionalinfo/additionalinfofields");
	    }

	}