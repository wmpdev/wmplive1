<?php

/**
 * Search admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Search_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('search_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Search'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Search_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_search',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Search'),
                'title'   => Mage::helper('lionleap_temples')->__('Search'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_search_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve search entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Search
     * @author Ultimate Module Creator
     */
    public function getSearch()
    {
        return Mage::registry('current_search');
    }
}
