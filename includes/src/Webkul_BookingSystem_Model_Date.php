<?php

    class Webkul_BookingSystem_Model_Date extends Mage_Catalog_Model_Product_Option_Type_Date    {

    	public function getYearStart()   {
            $_range = explode(",", $this->getConfigData("year_range"));
            if (isset($_range[0]) && !empty($_range[0]))
                return $_range[0];
            else {
            	if($this->isbooking()){
            		$collection = Mage::getResourceModel("bookingsystem/bookingsystem_collection")->addFieldToFilter("proid",Mage::registry("current_product")->getId())->getData();
            		return date("Y",strtotime($collection[0]["datefrom"]));
            	}
                else
                	return date("Y");
            }
        }

        public function getYearEnd()    {
            $_range = explode(",", $this->getConfigData("year_range"));
            if(isset($_range[1]) && !empty($_range[1]))
                return $_range[1];
            else {
                if($this->isbooking()){
                	$collection = Mage::getResourceModel("bookingsystem/bookingsystem_collection")->addFieldToFilter("proid",Mage::registry("current_product")->getId())->getData();
            		return date("Y",strtotime($collection[0]["dateto"]));
            	}
                else
                	return date("Y");
            }
        }

       	protected function isbooking(){
       		$current_product = Mage::registry("current_product")->getId();
       		$product = Mage::getModel("catalog/product")->load($current_product);
        	$setfrom = false;$setto = false;
    	    foreach($product->getOptions() as $option) {
            	if($option->getType() == "date_time" && $option->getTitle() == "Reservation From")
            		$setfrom = true;
            	if($option->getType() == "date_time" && $option->getTitle() == "Reservation To")
            		$setto = true;
    	    }
    	    if($setfrom == true && $setto == true)
    	    	return true;
    	    else
    	    	return false;
    	}

    }