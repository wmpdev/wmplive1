<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanager_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("eventmanager_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle($this->__("Event Information"));
    }

    protected function _beforeToHtml() {
        $this->addTab("form_section", array(
            "label" => $this->__("Add Event"),
            "alt" => $this->__("Add Event"),
            "content" => $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanager_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}