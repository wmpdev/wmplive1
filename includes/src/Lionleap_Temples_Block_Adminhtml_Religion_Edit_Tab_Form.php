<?php

/**
 * Religion edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Religion_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Religion_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('religion_');
        $form->setFieldNameSuffix('religion');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'religion_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Religion'))
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Name'),
                'name'  => 'name',
				'required'  => true,
				'class' => 'required-entry',
           )
        );
        /*$fieldset->addField(
            'tags',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Tags'),
                'name'  => 'tags',
           )
        );*/
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
				'required'  => true,
				'class' => 'required-entry',
            )
        );
        $formValues = Mage::registry('current_religion')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getReligionData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getReligionData());
            Mage::getSingleton('adminhtml/session')->setReligionData(null);
        } elseif (Mage::registry('current_religion')) {
            $formValues = array_merge($formValues, Mage::registry('current_religion')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
