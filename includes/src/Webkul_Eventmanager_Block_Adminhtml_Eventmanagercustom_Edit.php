<?php

class Webkul_Eventmanager_Block_Adminhtml_Eventmanagercustom_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'eventmanager';
        $this->_controller = 'adminhtml_eventmanagercustom';
        
        $this->_updateButton('save', 'label', Mage::helper('eventmanager')->__('Save Request'));
        $this->_updateButton('delete', 'label', Mage::helper('eventmanager')->__('Delete Request'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('eventmanagercustom_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'eventmanagercustom_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'eventmanagercustom_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('eventmanagercustom_data') && Mage::registry('eventmanagercustom_data')->getId() ) {
            return Mage::helper('eventmanager')->__("Edit Request");
        } else {
            return Mage::helper('eventmanager')->__('Add Custom Event Request');
        }
    }
}