<?php

    class Webkul_BookingSystem_Block_Adminhtml_Catalog_Product_Tab extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface {

        public function _construct()    {
            parent::_construct();
            $this->setTemplate("bookingsystem/catalog/product/bookingsystem.phtml");
        }

        public function getTabLabel()    {
            return $this->__("Reservation & Booking");
        }

        public function getTabTitle()    {
            return $this->__("Click to Configure Reservation & Booking");
        }

        public function canShowTab()    {
            return true;
        }

        public function isHidden()    {
            return false;
        }

    }