<?php 

/**
 * Upload image helper
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Helper_Upload_Image extends Lionleap_Temples_Helper_Image_Abstract
{
    /**
     * image placeholder
     * @var string
     */
    protected $_placeholder = 'images/placeholder/upload.jpg';
    /**
     * image subdir
     * @var string
     */
    protected $_subdir      = 'upload';
}
