<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Deity extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Deity
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/deity_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $deity) {
            $arr[] = array(
                'id'          => 'deity/1/'.$deity->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Deity'),
                'name'        => $deity->getName(),
                'description' => $deity->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_deity/edit',
                    array('id'=>$deity->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
