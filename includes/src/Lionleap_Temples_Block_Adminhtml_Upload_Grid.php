<?php

/**
 * Upload admin grid block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Upload_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('uploadGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Upload_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('lionleap_temples/upload')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Upload_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Id'),
                'index'  => 'id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'temple_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Temple'),
                'index'     => 'temple_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/temple_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getTempleId'
                ),
                'base_link' => 'adminhtml/temples_temple/edit'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Image/File'),
                'align'     => 'left',
                'index'     => 'name',
                'frame_callback' => array($this, 'callback_image'),
            )
        );
        /*$this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Link'),
                'align'     => 'left',
                'index'     => 'name',
				'renderer'	=> 'Lionleap_Temples_Block_Adminhtml_Template_Grid_Renderer_Image',
            )
        );*/
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                    'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'type',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Type Of Upload'),
                'index'  => 'type',
                'type'  => 'options',
                'options' => array(
                    'image' => Mage::helper('lionleap_temples')->__('Image'),
                    'festival image' => Mage::helper('lionleap_temples')->__('Festival Image'),
                    'file' => Mage::helper('lionleap_temples')->__('File'),
                )

            )
        );
        /*$this->addColumn(
            'filesize',
            array(
                'header' => Mage::helper('lionleap_temples')->__('File Size'),
                'index'  => 'filesize',
                'type'=> 'number',

            )
        );*/
        $this->addColumn(
            'is_primary',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Is Primary Image'),
                'index'  => 'is_primary',
                'type'    => 'options',
                    'options'    => array(
                    'yes' => Mage::helper('lionleap_temples')->__('Yes'),
                    'no' => Mage::helper('lionleap_temples')->__('No'),
                )

            )
        );
        /*$this->addColumn(
            'alias',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Alias File Name'),
                'index'  => 'alias',
                'type'=> 'text',

            )
        );*/
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('lionleap_temples')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('lionleap_temples')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('lionleap_temples')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('lionleap_temples')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('lionleap_temples')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Upload_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('upload');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('lionleap_temples')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('lionleap_temples')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Status'),
                        'values' => array(
                            'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                            'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'type',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Type Of Upload'),
                'url'        => $this->getUrl('*/*/massType', array('_current'=>true)),
                'additional' => array(
                    'flag_type' => array(
                        'name'   => 'flag_type',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Type Of Upload'),
                        'values' => array(
                                'image' => Mage::helper('lionleap_temples')->__('Image'),
                                'file' => Mage::helper('lionleap_temples')->__('File'),
                            )
                    )
                )
            )
        );
        $this->getMassactionBlock()->addItem(
            'is_primary',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Is Primary Image'),
                'url'        => $this->getUrl('*/*/massIsPrimary', array('_current'=>true)),
                'additional' => array(
                    'flag_is_primary' => array(
                        'name'   => 'flag_is_primary',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Is Primary Image'),
                        'values' => array(
                                'yes' => Mage::helper('lionleap_temples')->__('Yes'),
                                'no' => Mage::helper('lionleap_temples')->__('No'),
                            )

                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/temple_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'temple_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Temple'),
                'url'        => $this->getUrl('*/*/massTempleId', array('_current'=>true)),
                'additional' => array(
                    'flag_temple_id' => array(
                        'name'   => 'flag_temple_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Temple'),
                        'values' => $values
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Upload
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Upload_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

	public function callback_image($value) {
		if (empty($value)) {
			return '';
		} else {
			return "<a target='_blank' href='". Mage::getBaseUrl('media') . "upload/image" . $value . "'>". $value ."</a>";
		}
	}
}
