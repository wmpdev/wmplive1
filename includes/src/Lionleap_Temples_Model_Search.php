<?php

/**
 * Search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Search extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'lionleap_temples_search';
    const CACHE_TAG = 'lionleap_temples_search';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'lionleap_temples_search';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'search';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/search');
    }

    /**
     * before save search
     *
     * @access protected
     * @return Lionleap_Temples_Model_Search
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save search relation
     *
     * @access public
     * @return Lionleap_Temples_Model_Search
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Deity
     * @author Ultimate Module Creator
     */
    public function getParentDeity()
    {
        if (!$this->hasData('_parent_deity')) {
            if (!$this->getDeityId()) {
                return null;
            } else {
                $deity = Mage::getModel('lionleap_temples/deity')
                    ->load($this->getDeityId());
                if ($deity->getId()) {
                    $this->setData('_parent_deity', $deity);
                } else {
                    $this->setData('_parent_deity', null);
                }
            }
        }
        return $this->getData('_parent_deity');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Religion
     * @author Ultimate Module Creator
     */
    public function getParentReligion()
    {
        if (!$this->hasData('_parent_religion')) {
            if (!$this->getReligionId()) {
                return null;
            } else {
                $religion = Mage::getModel('lionleap_temples/religion')
                    ->load($this->getReligionId());
                if ($religion->getId()) {
                    $this->setData('_parent_religion', $religion);
                } else {
                    $this->setData('_parent_religion', null);
                }
            }
        }
        return $this->getData('_parent_religion');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
