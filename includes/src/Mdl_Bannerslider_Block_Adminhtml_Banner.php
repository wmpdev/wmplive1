<?php
class Mdl_Bannerslider_Block_Adminhtml_Banner extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_banner';
    $this->_blockGroup = 'bannerslider';
    $this->_headerText = Mage::helper('bannerslider')->__('Slide Manager');
    $this->_addButtonLabel = Mage::helper('bannerslider')->__('Add Slide');
    parent::__construct();
  }
}