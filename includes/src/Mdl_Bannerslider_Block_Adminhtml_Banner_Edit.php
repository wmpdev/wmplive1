<?php

class Mdl_Bannerslider_Block_Adminhtml_Banner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'bannerslider';
        $this->_controller = 'adminhtml_banner';
        
        $this->_updateButton('save', 'label', Mage::helper('bannerslider')->__('Save Slide'));
		$this->_updateButton('save', 'onclick', 'submitForm()');
        $this->_updateButton('delete', 'label', Mage::helper('bannerslider')->__('Delete Slide'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
 
        $this->_formScripts[] = "
             var myArray1 = new Array( 'big_white', 'big_orange', 'big_black', 'medium_grey', 'small_text', 'medium_text', 'large_text', 'very_large_text', 'very_big_white', 'very_big_black', 'boxshadow', 'black', 'noshadow' );
            function saveAndContinueEdit(){
			
			var textLayer=jQuery('#layer_text').val();
			var count = jQuery('#divLayers .slide_layer').length;
			var get_top='';
			var get_left='';
			var getstyle_top='';
			var getstyle_left='';
			var getLayerHtml='';
			var finalLayerHtml='';
			var data = [];
			var style='';
			var style_id='';
			var layer_type='';
			var v_id='';
			var v_title=''; 
			var i_title='';
			var img_url='';
			var find_vid;
			var easing='';
			var lay_class='';
			var style_class='';
			var styleLayer='';
			var styleEasing='easeInOutCirc';
			var timeLayer='';
			var depthLayer='';
			var sv=0;
			var sp_val='';
			var sp_val_id='';
			var req_id='';
			var last_val= new Array();
			var last_val_id= new Array();
			jQuery('#divLayers .tp-caption').each(function() {
			sp_val = jQuery(this).attr('id').split('_');
			//last_val = sp_val[sp_val.length-1];
			last_val.push(sp_val[sp_val.length-1]);
			
				sv=sv + 1 ;
			});
			var tot_row=0;
			jQuery('#sortlist li').each(function() { 
				tot_row++;
			});
			var j=0;
			jQuery('#sortlist li').each(function() {
			req_id = jQuery(this).attr('alt');
			easeLayer=jQuery('#slide_layer_ease_'+req_id).val();
			speedLayer=jQuery('#slide_layer_speed_'+req_id).val();
			animationLayer=jQuery('#slide_layer_animation_'+req_id).val();
			timeLayer=jQuery('#layer_sort_'+req_id+' .sortbox_time').val();
			depthLayer=jQuery('#layer_sort_'+req_id+' .sortbox_depth').val();
			
			get_top=jQuery('#slide_layer_'+req_id).css('top');
			getstyle_top = Number(get_top.replace(/\D/g, ''));
			get_left=jQuery('#slide_layer_'+req_id).css('left');
			getstyle_left = Number(get_left.replace(/\D/g, ''));
			getstyle_easing = jQuery('#slide_layer_'+req_id).attr('style');
			geteasing_split=getstyle_easing.split('easing:');
			geteasing=geteasing_split[1];
			easing = jQuery('#layer_easing').val();
				
			if(jQuery('#slide_layer_'+req_id).hasClass('big_white'))
			{
					styleLayer='big_white';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('big_orange'))
			{
					styleLayer='big_orange';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('big_black'))
			{
					styleLayer='big_black';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('medium_grey'))
			{
					styleLayer='medium_grey';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('small_text'))
			{
					styleLayer='small_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('medium_text'))
			{
					styleLayer='medium_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('large_text'))
			{
					styleLayer='large_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('very_large_text'))
			{
					styleLayer='very_large_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('very_big_white'))
			{
					styleLayer='very_big_white';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('very_big_black'))
			{
					styleLayer='very_big_black';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('boxshadow'))
			{
					styleLayer='boxshadow';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('black'))
			{
					styleLayer='black';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('noshadow'))
			{
					styleLayer='noshadow';
			}
			else
			{
					styleLayer='';
			}
				if(jQuery('#slide_layer_'+req_id).hasClass('video'))
				{
				
					layer_type='video';
					getLayerHtml='';
					//v_id=jQuery('#youtube_id').val();
					v_title=jQuery('#slide_layer_'+req_id+' .layer-video-title').html();
					v_title=v_title.replace(/\"/g,' ');
					v_width=jQuery('#slide_layer_'+req_id+' .slide_layer_video').css('width');
					v_width = Number(v_width.replace(/\D/g, ''));
					v_height=jQuery('#slide_layer_'+req_id+' .slide_layer_video').css('height'); 
					v_height = Number(v_height.replace(/\D/g, ''));
					v_img_url=jQuery('#slide_layer_'+req_id+' .slide_layer_video').css('background-image');
					 v_img_url = v_img_url.replace('url(','').replace(')','');
					 find_vid=v_img_url.split('/');
					// v_id=find_vid[find_vid.length-2];
					v_id=jQuery('#slide_layer_vid_'+req_id).val();
					video_type = jQuery('#slide_layer_vid_type_'+req_id).val(); 
					style_id+= '{\"video_id\":\"'+v_id+'\",\"video_type\":\"'+video_type+'\",\"type\":\"'+layer_type+'\",\"video_title\":\"'+v_title+'\",\"text\":\"'+v_title+'\",\"video_image_url\":'+v_img_url+',\"video_width\":'+v_width+',\"video_height\":'+v_height+',\"easing\":\"'+easeLayer+'\",\"speed\":\"'+speedLayer+'\",\"animation\":\"'+animationLayer+'\",\"time\":\"'+timeLayer+'\",\"serial\":\"'+depthLayer+'\",\"left\":'+getstyle_left+',\"top\":'+getstyle_top+'}';
					
					
				}
				else if(jQuery('#slide_layer_'+req_id).hasClass('text'))
				{
					layer_type='text';
					var getLayerHtmlData=jQuery('#slide_layer_'+req_id).html();
					 getLayerHtml = getLayerHtmlData.replace(new RegExp('\"','g'),'\'');
					style_id+= '{\"text\":\"'+getLayerHtml+'\",\"style\":\"'+styleLayer+'\",\"easing\":\"'+easeLayer+'\",\"speed\":\"'+speedLayer+'\",\"animation\":\"'+animationLayer+'\",\"time\":\"'+timeLayer+'\",\"serial\":\"'+depthLayer+'\",\"type\":\"'+layer_type+'\",\"left\":'+getstyle_left+',\"top\":'+getstyle_top+'}';  
				}
				else if(jQuery('#slide_layer_'+req_id).hasClass('image'))
				{
					layer_type='image';
					i_title='layer image';
					img_url=jQuery('#slide_layer_'+req_id+' img').attr('src');
					style_id+= '{\"type\":\"'+layer_type+'\",\"text\":\"'+i_title+'\",\"speed\":\"'+speedLayer+'\",\"time\":\"'+timeLayer+'\",\"serial\":\"'+depthLayer+'\",\"image_url\":\"'+img_url+'\",\"left\":'+getstyle_left+',\"animation\":\"'+animationLayer+'\",\"top\":'+getstyle_top+'}';
				}
				/*sp_val_id = jQuery(this).attr('id').split('_');
				last_val_id.push(sp_val_id[sp_val_id.length-1]);*/
				
				if(j!=(tot_row-1))
				{
					style_id+=';';
				}
				j++;
				
			});
			//alert(style_id);
		//return false;
			style_id+='';
			jQuery('#layer_data').val(style_id);
		editForm.submit($('edit_form').action+'back/edit/');
               
            }
			
			function submitForm() {
			
			var textLayer=jQuery('#layer_text').val();
			var count = jQuery('#divLayers .slide_layer').length;
			var get_top='';
			var get_left='';
			var getstyle_top='';
			var getstyle_left='';
			var getLayerHtml='';
			var finalLayerHtml='';
			var data = [];
			var style='';
			var style_id='';
			var layer_type='';
			var v_id='';
			var v_title=''; 
			var i_title='';
			var img_url='';
			var find_vid;
			var easing='';
			var lay_class='';
			var style_class='';
			var styleLayer='';
			var styleEasing='easeInOutCirc';
			var timeLayer='';
			var depthLayer='';
			var sv=0;
			var sp_val='';
			var sp_val_id='';
			var req_id='';
			var last_val= new Array();
			var last_val_id= new Array();
			jQuery('#divLayers .tp-caption').each(function() {
			sp_val = jQuery(this).attr('id').split('_');
			//last_val = sp_val[sp_val.length-1];
			last_val.push(sp_val[sp_val.length-1]);
			
				sv=sv + 1 ;
			});
			var tot_row=0;
			jQuery('#sortlist li').each(function() { 
				tot_row++;
			});
			var j=0;
			jQuery('#sortlist li').each(function() {
			req_id = jQuery(this).attr('alt');
			easeLayer=jQuery('#slide_layer_ease_'+req_id).val();
			speedLayer=jQuery('#slide_layer_speed_'+req_id).val();
			animationLayer=jQuery('#slide_layer_animation_'+req_id).val();
			timeLayer=jQuery('#layer_sort_'+req_id+' .sortbox_time').val();
			depthLayer=jQuery('#layer_sort_'+req_id+' .sortbox_depth').val();
			
			get_top=jQuery('#slide_layer_'+req_id).css('top');
			getstyle_top = Number(get_top.replace(/\D/g, ''));
			get_left=jQuery('#slide_layer_'+req_id).css('left');
			getstyle_left = Number(get_left.replace(/\D/g, ''));
			getstyle_easing = jQuery('#slide_layer_'+req_id).attr('style');
			geteasing_split=getstyle_easing.split('easing:');
			geteasing=geteasing_split[1];
			easing = jQuery('#layer_easing').val();
				
			if(jQuery('#slide_layer_'+req_id).hasClass('big_white'))
			{
					styleLayer='big_white';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('big_orange'))
			{
					styleLayer='big_orange';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('big_black'))
			{
					styleLayer='big_black';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('medium_grey'))
			{
					styleLayer='medium_grey';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('small_text'))
			{
					styleLayer='small_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('medium_text'))
			{
					styleLayer='medium_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('large_text'))
			{
					styleLayer='large_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('very_large_text'))
			{
					styleLayer='very_large_text';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('very_big_white'))
			{
					styleLayer='very_big_white';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('very_big_black'))
			{
					styleLayer='very_big_black';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('boxshadow'))
			{
					styleLayer='boxshadow';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('black'))
			{
					styleLayer='black';
			}
			else if(jQuery('#slide_layer_'+req_id).hasClass('noshadow'))
			{
					styleLayer='noshadow';
			}
			else
			{
					styleLayer='';
			}
				if(jQuery('#slide_layer_'+req_id).hasClass('video'))
				{
				
					layer_type='video';
					getLayerHtml='';
					//v_id=jQuery('#youtube_id').val();
					v_title=jQuery('#slide_layer_'+req_id+' .layer-video-title').html();
					v_title=v_title.replace(/\"/g,' ');
					v_width=jQuery('#slide_layer_'+req_id+' .slide_layer_video').css('width');
					v_width = Number(v_width.replace(/\D/g, ''));
					v_height=jQuery('#slide_layer_'+req_id+' .slide_layer_video').css('height'); 
					v_height = Number(v_height.replace(/\D/g, ''));
					v_img_url=jQuery('#slide_layer_'+req_id+' .slide_layer_video').css('background-image');
					 v_img_url = v_img_url.replace('url(','').replace(')','');
					 find_vid=v_img_url.split('/');
					// v_id=find_vid[find_vid.length-2];
					v_id=jQuery('#slide_layer_vid_'+req_id).val();
					video_type = jQuery('#slide_layer_vid_type_'+req_id).val(); 
					style_id+= '{\"video_id\":\"'+v_id+'\",\"video_type\":\"'+video_type+'\",\"type\":\"'+layer_type+'\",\"video_title\":\"'+v_title+'\",\"text\":\"'+v_title+'\",\"video_image_url\":'+v_img_url+',\"video_width\":'+v_width+',\"video_height\":'+v_height+',\"easing\":\"'+easeLayer+'\",\"speed\":\"'+speedLayer+'\",\"animation\":\"'+animationLayer+'\",\"time\":\"'+timeLayer+'\",\"serial\":\"'+depthLayer+'\",\"left\":'+getstyle_left+',\"top\":'+getstyle_top+'}';
					
					
				}
				else if(jQuery('#slide_layer_'+req_id).hasClass('text'))
				{
					layer_type='text';
					var getLayerHtmlData=jQuery('#slide_layer_'+req_id).html();
					 getLayerHtml = getLayerHtmlData.replace(new RegExp('\"','g'),'\'');
					style_id+= '{\"text\":\"'+getLayerHtml+'\",\"style\":\"'+styleLayer+'\",\"easing\":\"'+easeLayer+'\",\"speed\":\"'+speedLayer+'\",\"animation\":\"'+animationLayer+'\",\"time\":\"'+timeLayer+'\",\"serial\":\"'+depthLayer+'\",\"type\":\"'+layer_type+'\",\"left\":'+getstyle_left+',\"top\":'+getstyle_top+'}';  
				}
				else if(jQuery('#slide_layer_'+req_id).hasClass('image'))
				{
					layer_type='image';
					i_title='layer image';
					img_url=jQuery('#slide_layer_'+req_id+' img').attr('src');
					style_id+= '{\"type\":\"'+layer_type+'\",\"text\":\"'+i_title+'\",\"speed\":\"'+speedLayer+'\",\"time\":\"'+timeLayer+'\",\"serial\":\"'+depthLayer+'\",\"image_url\":\"'+img_url+'\",\"left\":'+getstyle_left+',\"animation\":\"'+animationLayer+'\",\"top\":'+getstyle_top+'}';
				}
				/*sp_val_id = jQuery(this).attr('id').split('_');
				last_val_id.push(sp_val_id[sp_val_id.length-1]);*/
				
				if(j!=(tot_row-1))
				{
					style_id+=';';
				}
				j++;
				
			});
			//alert(style_id);
		//return false;
			style_id+='';
			jQuery('#layer_data').val(style_id);
		editForm.submit();
}
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('banner_data') && Mage::registry('banner_data')->getId() ) {
            return Mage::helper('bannerslider')->__("Edit Slide '%s'", $this->htmlEscape(Mage::registry('banner_data')->getName()));
        } else {
            return Mage::helper('bannerslider')->__('Add Slide');
        }
    }
}