<?php 

require_once Mage::getBaseDir('lib') . '/Airpay/checksum.php';

class Airpay_TransactController extends Mage_Core_Controller_Front_Action
{
    /**
     * Set the quote Id generated in the table into session
     * and add the block of airpay form which will submit itself
     * to the airpay site
     */
    public function redirectAction() 
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setAirpayQuoteId($session->getQuoteId());
        $this->getResponse()->setBody($this->getLayout()->createBlock('airpay/redirect')->toHtml());
        $session->unsQuoteId();
        $session->unsRedirectUrl();
    }

    public function responseAction() 
    {
        // actual processing
        $postdata = Mage::app()->getRequest()->getPost();
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getAirpayQuoteId(true));
	$airpayConfig = Mage::getStoreConfig('payment/airpay');

	// 	Checksum Verification
	//	Proceed only if checksum matches. Else redirect to error page.
	$checksumReceived   =   $postdata['ap_SecureHash'];
    $TRANSACTIONID      =   $postdata['TRANSACTIONID'];
    $APTRANSACTIONID    =   $postdata['APTRANSACTIONID'];
    $AMOUNT             =   $postdata['AMOUNT'];
    $TRANSACTIONSTATUS  =   $postdata['TRANSACTIONSTATUS'];
    $MESSAGE            =   $postdata['MESSAGE'];

    $mercid             =   $airpayConfig['merchant_id'];
    $username           =   $airpayConfig['username'];

    $allParamsReceived = Checksum::getAllParams($postdata);

	//$checksumCalculated = Checksum::calculateChecksum($airpayConfig['secret_key'], $allParamsReceived);
    $checksumCalculated = sprintf("%u", crc32 ($TRANSACTIONID.':'.$APTRANSACTIONID.':'.$AMOUNT.':'.$TRANSACTIONSTATUS.':'.$MESSAGE.':'.$mercid.':'.$username));

	error_log("Logging response params : " . $allParamsReceived);
	error_log('Logging checksum : ' . $checksumCalculated);
	if ($checksumReceived !== $checksumCalculated) {
		if ($session->getLastRealOrderId()) {
                	$order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
                	if ($order->getId()) {
                    		$order->cancel()->save();
                	}
            	}
		$er = 'Checksum does not match. This response has been compromised. However, transaction might have been successful.';
            	$session->addError($er);
            	$this->_redirect('airpay/transact/failure');
		return;
	}


        // success
        if ($this->_validateResponse()) {
            Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
            // load the order and change the order status
            $airpay = Mage::getModel('airpay/transact');

            //$state = $airpay->airpaySuccessOrderState();

            $order = Mage::getModel('sales/order')
                ->loadByIncrementId($postdata['TRANSACTIONID'])
                ->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
            $payment = $order->getPayment();
            $transaction = Mage::getModel('sales/order_payment_transaction');
            $dummy_txn_id = 'AP_'.$postdata['TRANSACTIONID'];
            $transaction->setOrderPaymentObject($payment)
                ->setTxnId($dummy_txn_id)
                ->setTxnType(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH)
                ->setIsClosed(0)
                ->save();
            $order->save();        
            try
			{
			$order->sendNewOrderEmail();
			} catch (Exception $ex) {  }            
			
            $this->_redirect('checkout/onepage/success', array('_secure'=>true));
        } else {
            // failure/cancel
            if ($session->getLastRealOrderId()) {
                $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
                if ($order->getId()) {
                    $order->cancel()->save();
                }
            }
            $er = 'Airpay could not process your request because of the error "'.$postdata['MESSAGE'] . '"';
            $session->addError($er);
            $this->_redirect('airpay/transact/failure');
        }
    }

    /**
     * Verify the response coming into the server
     * @return boolean
     */
    protected function _validateResponse() 
    {
        $postdata = Mage::app()->getRequest()->getPost();
        $session = Mage::getSingleton('checkout/session');
        $flag = False;
        error_log('Response Code is ' . $postdata['TRANSACTIONSTATUS']);
	if ((int)$postdata['TRANSACTIONSTATUS'] == 200) {
		$flag = True;
	}
        return $flag;
    }

    public function failureAction() 
    {
        $this->loadLayout();        
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }
}
