<?php 

class Airpay_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        // $this->testTransact();
        // $this->testUpdate();
        // $this->testPayment();
        $this->testSession();
    }

    protected function testTransact() {
        $order = Mage::getModel('sales/order')
            ->load(27);
        $config = Mage::getStoreConfig('payment/airpay');
          $airpay = Mage::getModel('airpay/transact');
        $fields = $airpay->getCheckoutFormFields();
        $form = '<form id="airpay_checkout" method="POST" action="' . $airpay->getAirpayTransactAction() . '">';
        foreach($fields as $key => $value) {
            $form .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />'."\n";
        }
        $form .= '</form>';
        $html = '<html><body>';
        $html .= $this->__('You will be redirected to the Airpay website in a few seconds.');
        $html .= $form;
        // $html.= '<script type="text/javascript">document.getElementById("airpay_checkout").submit();</script>';
        $html.= '</body></html>';
        echo $html;
        exit;
    }


    protected function testPayment() {
        $order = Mage::getModel('sales/order')
            ->load(27);
        var_dump($order->getPayment()); exit;
    }

    protected function testSession() {
        $session = Mage::getSingleton('airpay/session');
        var_dump($session->getTransactStatus('100000082')); exit;        
    }
}