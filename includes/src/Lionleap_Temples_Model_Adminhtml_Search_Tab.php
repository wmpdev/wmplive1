<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Tab extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Tab
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/tab_collection')
            ->addFieldToFilter('title', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $tab) {
            $arr[] = array(
                'id'          => 'tab/1/'.$tab->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Tab'),
                'name'        => $tab->getTitle(),
                'description' => $tab->getTitle(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_tab/edit',
                    array('id'=>$tab->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
