<?php

class Mdlext_Mdloption_Model_Config_Stylesheet 
{
    public function toOptionArray()
    {
        return array(
            array('value' => '1', 'label'=>Mage::helper('adminhtml')->__('Sea Green')),
            array('value' => '2', 'label'=>Mage::helper('adminhtml')->__('Pink')),
			array('value' => '3', 'label'=>Mage::helper('adminhtml')->__('Sky Blue')),
			array('value' => '4', 'label'=>Mage::helper('adminhtml')->__('Orange')),
        );
    }
}
?>