<?php

    class Webkul_BookingSystem_Model_Events_Observer {

        public function bookdeals($observer) {
            $order_id = $observer->getOrderIds();
            $order = Mage::getModel("sales/order")->load($order_id[0]);
            foreach($order->getAllItems() as $item) {
                $all_data = $item->getProductOptions();
                $custom_options = $all_data["options"];
                $booking_detail = Mage::getModel("bookingsystem/bookings");
                foreach ($custom_options as $one_option) {
                    if($one_option["label"] == "Reservation From"){
                        $values = explode(" ",$one_option["value"]);
                        $time_value = explode(" ",$one_option["option_value"]);
                        $booking_array["from_date"] = $values[0];
                        $booking_array["from_time"] = $time_value[1];
                    }
                    if($one_option["label"] == "Reservation To"){
                        $values = explode(" ",$one_option["value"]);
                        $time_value = explode(" ",$one_option["option_value"]);
                        $booking_array["to_date"] = $values[0];
                        $booking_array["to_time"] = $time_value[1];
                    }
                }
                $magento = Mage::getVersion();
                if($magento < "1.8.1.0") {
                    $to_date = explode("/",$booking_array["to_date"]);
                    $booking_array["to_date"] = $to_date[0]."/".$to_date[1]."/20".$to_date[2];
                    $from_date = explode("/",$booking_array["from_date"]);
                    $booking_array["from_date"] = $from_date[0]."/".$from_date[1]."/20".$from_date[2];
                }
                $deal_model = Mage::getResourceModel("bookingsystem/bookingsystem_collection")->addFieldToFilter("proid",$item->getProductId());
                $id = $deal_model->getData();
                $booking_array["deal_id"] = $id[0]["rangeid"];
                $booking_array["order_id"] = $order_id[0];

                if($booking_array['to_date']!="" && $booking_array["from_date"]!=""){
                    $booking_detail->setData($booking_array);
                    $booking_detail->save();
                }    
            }
        }

        public function checkbookingdeal($observer)     {
            $signal = false;
            $quote = Mage::getModel("sales/quote")->load($observer->getQuote()->getId());
            foreach($quote->getAllItems() as $item)    {
                $all_data = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
                $custom_options = $all_data["options"];
                foreach ($custom_options as $one_option) {
                    if($one_option["label"] == "Reservation From"){
                        $values = explode(" ",$one_option["value"]);
                        $time_value = explode(" ",$one_option["option_value"]);
                        $booking_array["from_date"] = $values[0];
                        $booking_array["from_time"] = $time_value[1];
                    }
                    if($one_option["label"] == "Reservation To"){
                        $values = explode(" ",$one_option["value"]);
                        $time_value = explode(" ",$one_option["option_value"]);
                        $booking_array["to_date"] = $values[0];
                        $booking_array["to_time"] = $time_value[1];
                    }
                }
                $magento = Mage::getVersion();
                if($magento < "1.8.1.0"){
                    $to_date = explode("/",$booking_array["to_date"]);
                    $booking_array["to_date"] = $to_date[0]."/".$to_date[1]."/20".$to_date[2];
                    $from_date = explode("/",$booking_array["from_date"]);
                    $booking_array["from_date"] = $from_date[0]."/".$from_date[1]."/20".$from_date[2];
                }
                $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$booking_array["from_date"])->addFieldToFilter("to_date",$booking_array["to_date"])->addFieldToFilter("from_time",$booking_array["from_time"])->addFieldToFilter("to_time",$booking_array["to_time"])->getData();
                if($bookings[0]["id"]){
                    $booked_dates .= 'From('.$booking_array["from_date"].' at '.$booking_array["from_time"].') TO ('.$booking_array["to_date"].' at '.$booking_array["to_time"].')\n';
                    $signal = true;
                }
            }
            if($signal == true){
                echo 'alert("Sorry Following Deal/Deals are already been Booked In the mean time, please go for another deal \n'.$booked_dates.'")';
                die();
            }
        }

        public function addcustomoption($observer)  {
            $product = $observer->getEvent()->getProduct();
            $save_from = true;$save_to = true;$total_qty = 0;
            $wholedata = Mage::app()->getRequest()->getParams();

            $checktype = $wholedata["booking_type"];

            if($checktype!=0){
                if($wholedata["booking_type"]) {
                    $otm_from_data = array();
                    $otm_from_count =1;
                    foreach ($wholedata['otm_from'] as $otm_from_value) {
                        $otm_from_data[$otm_from_count] = $otm_from_value;
                        $otm_from_count++;
                    }
                    $wholedata['otm_from'] = $otm_from_data;
                    $otm_to_data = array();
                    $otm_to_count =1;
                    foreach ($wholedata['otm_to'] as $otm_to_value) {
                        $otm_to_data[$otm_to_count] = $otm_to_value;
                        $otm_to_count++;
                    }
                    $wholedata['otm_to'] = $otm_to_data;

                    if($wholedata["booking_type"] == 1){
                        $from_date = strtotime($wholedata["datefrom"]);
                        $to_date = strtotime($wholedata["dateto"]);
                        $date_diff = $to_date - $from_date;
                        $day_count = floor($date_diff/(60*60*24))+1;
                        $format = "n/j/Y";$step = "+1 day";
                        for($i=1; $i<=$day_count; $i++)  {
                            $date = date($format, $from_date);
                            $lowerday = strtolower(date("D",strtotime($date)));
                            if($lowerday == "sun")  {
                                if($wholedata["suns"] != "closed"){
                                    $difference = (strtotime($wholedata["sune"]) - strtotime($wholedata["suns"]))/60;
                                    $diff = round($difference / ($$wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["suns"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            if($lowerday == "mon")  {
                                if($wholedata["mons"] != "closed"){
                                    $difference = (strtotime($wholedata["mone"]) - strtotime($wholedata["mons"]))/60;
                                    $diff = round($difference / ($wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["mons"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            if($lowerday == "tue")  {
                                if($wholedata["tues"] != "closed"){
                                    $difference = (strtotime($wholedata["tuee"]) - strtotime($wholedata["tues"]))/60;
                                    $diff = round($difference / ($wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["tues"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            if($lowerday == "wed")  {
                                if($wholedata["weds"] != "closed"){
                                    $difference = (strtotime($wholedata["wede"]) - strtotime($wholedata["weds"]))/60;
                                    $diff = round($difference / ($wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["weds"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            if($lowerday == "thu")  {
                                if($wholedata["thus"] != "closed"){
                                    $difference = (strtotime($wholedata["thue"]) - strtotime($wholedata["thus"]))/60;
                                    $diff = round($difference / ($wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["thus"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            if($lowerday == "fri")  {
                                if($wholedata["fris"] != "closed"){
                                    $difference = (strtotime($wholedata["frie"]) - strtotime($wholedata["fris"]))/60;
                                    $diff = round($difference / ($wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["fris"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            if($lowerday == "sat")  {
                                if($wholedata["sats"] != "closed"){
                                    $difference = (strtotime($wholedata["sate"]) - strtotime($wholedata["sats"]))/60;
                                    $diff = round($difference / ($wholedata["cache_time"]+$wholedata["timeslot"]));
                                    for($j=1; $j<=$diff; $j++){
                                        $time = strtotime($wholedata["sats"]);
                                        $end_time = date("H:i",strtotime("+".($wholedata["cache_time"]*($j-1))+($wholedata["timeslot"]*$j)." minutes", $time));
                                        $start_time = date("H:i",strtotime("+".(($wholedata["cache_time"]+$wholedata["timeslot"])*($j-1))." minutes",$time));
                                        $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$date)->addFieldToFilter("to_date",$date)->addFieldToFilter("from_time",$start_time.":00")->addFieldToFilter("to_time",$end_time.":00")->getData();
                                        if(!$bookings[0]["id"])
                                            $total_qty++;
                                    }
                                }
                            }
                            $from_date = strtotime($step, $from_date);
                        }
                    }
                    if($wholedata["booking_type"] == 2){
                        $from_date = strtotime($wholedata["datefrom"]);
                        $to_date = strtotime($wholedata["dateto"]);
                        $date_diff = $to_date - $from_date;
                        $day_count = floor($date_diff/(60*60*24))+1;
                        $format = "n/j/Y";$step = "+1 day";
                        $from_array = $wholedata["otm_from"];
                        $to_array = $wholedata["otm_to"];
                        for($j=1; $j<=count($from_array); $j++) {
                            $final_from = array();$final_to = array();
                            $from_date = strtotime($wholedata["datefrom"]);
                            for($i=1; $i<=$day_count; $i++) {
                                $date = date($format, $from_date);
                                $date = date_format(date_create($date), "l jS F Y");
                                $date_parts = explode(" ",$date);
                                $day_name = strtolower($date_parts[0]);
                                $from_day_time = explode("-",$from_array[$j]);
                                $from_day = $from_day_time[0];
                                $from_time = date("H:i", strtotime($from_day_time[1]));
                                $to_day_time = explode("-",$to_array[$j]);
                                $to_day = $to_day_time[0];
                                $to_time = date("H:i", strtotime($to_day_time[1]));
                                if($day_name == $from_day)
                                    $final_from[] = $from_date;
                                if($day_name == $to_day)
                                    $final_to[] = $from_date;
                                $from_date = strtotime($step, $from_date);
                            }
                            for($r=0; $r<count($final_from); $r++) {
                                if($final_from[$r] != "" && $final_to[$r] != ""){
                                    if($final_from[$r] > $final_to[$r])
                                        $final_to[$r] = $final_to[$r+1];
                                    $formated_from_date_raw = date($format, $final_from[$r]);
                                    $formated_to_date_raw = date($format, $final_to[$r]);
                                    $bookings = Mage::getResourceModel("bookingsystem/bookings_collection")->addFieldToFilter("from_date",$formated_from_date_raw)->addFieldToFilter("to_date",$formated_to_date_raw)->addFieldToFilter("from_time",$from_time.":00")->addFieldToFilter("to_time",$to_time.":00")->getData();
                                    if(!$bookings[0]["id"])
                                        $total_qty++;
                                }
                            }
                        }
                    }
                } 
            }

            if($wholedata["booking_type"]!= 0){
                // setting stock(inventory)
                $stockItem = Mage::getModel("cataloginventory/stock_item")->loadByProduct($product->getId());
                if(!$stockItem->getId())
                    $stockItem->setProductId($product->getId())->setStockId(1);
                $stockItem->setData("is_in_stock", 1);
                $savedStock = $stockItem->save();
                $stockItem->load($savedStock->getId())->setQty($total_qty)->save();
            }
            // setting custom options
            if($product->getOptions()) {
                foreach ($product->getProductOptionsCollection() as $opt) {
                    if($opt["title"] == "Reservation From")
                        $save_from = false;
                    if($opt["title"] == "Reservation To")
                        $save_to = false;
                }
            }
            $titlearray = array("Reservation From","Reservation To");
            $order = 1;
            if($wholedata["booking_type"] == 1 || $wholedata["booking_type"] == 2){
                if($save_from == true && $save_to == true)   {
                    foreach($titlearray as $value) {
                        $arrayOption =  array(
                                        "sort_order"    => $order,
                                        "title"         => $value,
                                        "price_type"    => "fixed",
                                        "price"         => "",
                                        "type"          => "date_time",
                                        "is_required"   => 0);
                        $product->setHasOptions(1);
                        $product->getResource()->save($product);
                        $option = Mage::getModel("catalog/product_option")->setProductId($product->getId())->setStoreId($product->getStoreId())->addData($arrayOption);
                        $option->save();
                        $product->addOption($option);
                        $order++;
                    }
                }
            }
            else{
                foreach ($product->getProductOptionsCollection() as $opt)
                    if($opt["title"] == "Reservation From" || $opt["title"] == "Reservation To")
                        $opt->delete();
            }
            $collection = Mage::getResourceModel("bookingsystem/bookingsystem_collection")->addFieldToFilter("proid",$product->getId())->getData();
            $model = Mage::getModel("bookingsystem/bookingsystem");
            
            if($collection[0]["rangeid"]!=""){
                if($wholedata["booking_type"]!=0){
                    $model = $model->load($collection[0]["rangeid"]);
                    $model->setData($wholedata);
                    $model->setId($collection[0]["rangeid"]);
                                        
                    if($wholedata["booking_type"]==2){
                        $model->setOtmFrom(serialize($wholedata["otm_from"]));
                        $model->setOtmTo(serialize($wholedata["otm_to"]));
                    }
                    $model->save();
                    
                }else{
                    $model->setId($collection[0]["rangeid"])->delete();
                }
                    
            }else{
                if($wholedata["booking_type"]!=0){
                    $model->setData($wholedata);
                    $model->setProid($product->getId());
                    $model->setOtmFrom(serialize($wholedata["otm_from"]));
                    $model->setOtmTo(serialize($wholedata["otm_to"]));
                    $model->save(); 
                }
            }
           
        }
    }