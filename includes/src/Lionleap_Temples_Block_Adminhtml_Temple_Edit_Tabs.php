<?php

/**
 * Temple admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('temple_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Temple'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_temple',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Temple'),
                'title'   => Mage::helper('lionleap_temples')->__('Temple'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_temple_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        #$this->addTab(
        #    'tabs',
        #    array(
        #        'label' => Mage::helper('lionleap_temples')->__('Tabs'),
        #        'url'   => $this->getUrl('*/*/tabs', array('_current' => true)),
        #        'class' => 'ajax'
        #    )
        #);
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve temple entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Temple
     * @author Ultimate Module Creator
     */
    public function getTemple()
    {
        return Mage::registry('current_temple');
    }
}
