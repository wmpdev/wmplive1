<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagergroup_Edit_Tab_Grideventmanager extends Mage_Adminhtml_Block_Widget_Container {

    public function __construct() {
        parent::__construct();
        $this->setTemplate("eventmanager/eventmanagers.phtml");
    }

    public function getTabsHtml() {
        return $this->getChildHtml("eventmanagers");
    }

    protected function _prepareLayout() {
        $this->setChild("eventmanagers", $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_edit_tab_eventmanager", "eventmanagergroup.grid.eventmanager"));
        return parent::_prepareLayout();
    }

    public function getEventgroupData() {
        return Mage::registry("eventmanagergroup_data");
    }

    public function getEventsJson() {
        $events = explode(",", $this->getEventgroupData()->getEventIds());
        if (!empty($events) && isset($events[0]) && $events[0] != "") {
            $data = array();
            foreach ($events as $element) 
                $data[$element] = $element;
            return Zend_Json::encode($data);
        }
        return "{}";
    }

}
