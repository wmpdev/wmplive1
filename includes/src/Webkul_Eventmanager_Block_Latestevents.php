<?php
class Webkul_Eventmanager_Block_Latestevents extends Mage_Core_Block_Template{

	public function __construct() {
        parent::__construct();
        $ids=$this->latestEvents();
		$collection = Mage::getModel("eventmanager/eventmanager")->getCollection()
			->addFieldToFilter("status","1")
			->addFieldToFilter("event_id",array("in" => $ids))
			->setOrder("edate","ASC");
        $this->setCollection($collection);
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock("page/html_pager", "custom.pager");
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,"all"=>"all"));
        $pager->setCollection($this->getCollection());
        $this->setChild("pager", $pager);
		$this->getCollection()->setPageSize(10000)->load();
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml("pager");
    }

	public function getTitle() 	{
		return "Latest Events";
	}

	public function latestEvents(){
		$i=0;
		$latestevents=array();
		$latesteventids = array();
		$allevents=$this->getAllEvents();
		foreach($allevents as $event){
			if($event->getStatus()==1){
				$datediff=strtotime($event->getEdate())-time();
				$daysdiff=ceil($datediff/(60*60*24));
				//if($daysdiff>=0){
					$latestevents[$i]["id"]=$event->getId();
					array_push($latesteventids, $event->getId());
					$latestevents[$i]["datediff"]=$daysdiff;
					$i++;
				//}
			}
		}
		return $latesteventids;
	}

	public function getAllEvents()	{
		$collection = Mage::getModel("eventmanager/eventmanager")->getCollection();
		return $collection;
	}

}