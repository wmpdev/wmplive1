<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Temple extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Temple
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/temple_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $temple) {
            $arr[] = array(
                'id'          => 'temple/1/'.$temple->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Temple'),
                'name'        => $temple->getName(),
                'description' => $temple->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_temple/edit',
                    array('id'=>$temple->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
