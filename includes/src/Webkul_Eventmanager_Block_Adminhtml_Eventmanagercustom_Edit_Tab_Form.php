<?php

class Webkul_Eventmanager_Block_Adminhtml_Eventmanagercustom_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
	  $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('eventmanagercustom_form', array('legend'=>Mage::helper('eventmanager')->__('Custom Event Request')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('eventmanager')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	  $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('eventmanager')->__('Email'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'email',
      ));
      
	  $fieldset->addField('phone', 'text', array(
          'label'     => Mage::helper('eventmanager')->__('Phone Number'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'phone',
      ));

	  $fieldset->addField('address', 'textarea', array(
          'label'     => Mage::helper('eventmanager')->__('Address'),
          'required'  => true,
          'name'      => 'address',
      ));

      $fieldset->addField('looking_for', 'select', array(
          'name'      => 'looking_for',
          'label'     => Mage::helper('eventmanager')->__('Looking for'),
          'required'  => true,
		  'values'	=> array('0'=>'Select option', 'product'=>'Product', 'pandit'=>'Pandit'),
		  'class'     => 'required-entry',
      ));

	  $main_categories = array(''=>'Select the type');

	  $categories = Mage::getModel('catalog/category')->getCollection()
		->addAttributeToFilter('parent_id', array('eq' => '2'))
		->addAttributeToSelect('id')
		->addAttributeToSelect('name')
		->addAttributeToSelect('url_key')
		->addAttributeToSelect('url')
		->addAttributeToSelect('is_active');
	  foreach ($categories as $category) {
			if ($category->getIsActive()) { // Only pull Active categories
				$main_categories[] = array(
					'value' => $category->getId(),
					'label' => $category->getName(),
				);
			}
	  }

	  /*foreach ($main_categories as $index=>$main_category) {
		  if (is_array($main_category)) {
			    $sub_categories = Mage::getModel('catalog/category')->getCollection()
				->addAttributeToFilter('parent_id', array('eq' => $main_category['value']))
				->addAttributeToSelect('id')
				->addAttributeToSelect('name')
				->addAttributeToSelect('url_key')
				->addAttributeToSelect('url')
				->addAttributeToSelect('is_active');
			  foreach ($sub_categories as $sub_category) {
					if ($sub_category->getIsActive()) { // Only pull Active categories
						$main_categories[$index] = array(
							'value' => array(array('value'=>$sub_category->getId(), 'label'=> "  - " . $sub_category->getName())),
							'label' => $main_category['label'],
						);
					}
			  }
		  }
	  }*/

	  $fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('eventmanager')->__('Type'),
          'name'      => 'type',
		  'values'    => $main_categories,
		  'onchange'	=> "getSubCategories(document.getElementById('type').value, 0);",
		  'required'  => true,
		  'class'     => 'required-entry',
      ));
?>
<script language="javascript">
	function getSubCategories(category_id, selected_value) {
		new Ajax.Request('/index.php/admin/catalog_category/categoriesJson/?isAjax=true', {
          method: 'post',
		  parameters: 'id=' + category_id + '&node' + category_id + '&store=0',
          onComplete: function(req) {       
                if(req.responseText == 'n') {
                    alert('Invalid Request');
                } else{
					emptySelect(document.getElementById('occasion'));
					addOption(document.getElementById('occasion'), 'Select a occasion', '');
					var parsedObj = JSON.parse(req.responseText);
					for (i=0; i<parsedObj.length; i++) {
						addOption(document.getElementById('occasion'), parsedObj[i].text, parsedObj[i].id, selected_value);
					}
                }
          }
        });
	}

	/*function getTitles(category_id) {
		new Ajax.Request('/index.php/admin/catalog_category/edit/key/105b62413e440629a31b9c47f11b853d/id/' + category_id + '/?isAjax=true', {
			method: 'get',
			//parameters:  + '&node' + category_id + '&store=0',
			onComplete: function(req) {
				if(req.responseText == 'n') {
					alert('Invalid Request');
				} else{
					emptySelect(document.getElementById('title'));
					var parsedObj = JSON.parse(req.responseText);
					for (i=0; i<parsedObj.length; i++) {
						addOption(document.getElementById('title'), parsedObj[i].text, parsedObj[i].id);
					}
                }
			}
        });
	}*/

	addOption = function(selectbox, text, value, selected_value) {
		var optn = document.createElement("OPTION");
		optn.text = text.replace(/\s*\(\s*[\d+\,]+\s*\)\s*/, '');
		optn.value = value;
		if (selected_value != '' && selected_value != 0 && selected_value == value){
			optn.selected = true;
		}
		selectbox.options.add(optn);  
	}

	emptySelect = function(selectbox) {
		var length = selectbox.options.length;
		for (i = 0; i <= length; i++) {
		  selectbox.remove(i);
		}
		selectbox.options.length = 0;
	}
</script>
<?php
	  $fieldset->addField('occasion', 'select', array(
          'label'     => Mage::helper('eventmanager')->__('Occasion'),
          'name'      => 'occasion',
      ));

	$fieldset->addField('occasion_on', 'text', array(
          'label'     => Mage::helper('eventmanager')->__('Occasion Date'),
          'name'      => 'occasion_on',
		  'required'  => true,
		  'class'     => 'required-entry',
      ));
	
	 $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('eventmanager')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 'new',
                  'label'     => Mage::helper('eventmanager')->__('New'),
              ),

              array(
                  'value'     => 'approved',
                  'label'     => Mage::helper('eventmanager')->__('Approved'),
              ),

              array(
                  'value'     => 'rejected',
                  'label'     => Mage::helper('eventmanager')->__('Rejected'),
              ),
          ),
		  'required' => true,
		  'class'     => 'required-entry',
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getEventmanagercustomData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getEventmanagercustomData());
          Mage::getSingleton('adminhtml/session')->setEventmanagercustomData(null);
      } elseif ( Mage::registry('eventmanagercustom_data') ) {
		  $data = Mage::registry('eventmanagercustom_data')->getData();
          $form->setValues($data);
?>
	<script language="javascript">
	getSubCategories(<?php echo $data['type']; ?>, <?php echo $data['occasion']; ?>);
	</script>
<?php
      }
      return parent::_prepareForm();
  }
}