<?php
 
class Creare_OnlineOnly_Model_Observer
{
    public function onlineOnly(Varien_Event_Observer $observer)
    {
       $event           = $observer->getEvent();
	   $method          = $event->getMethodInstance();
	   $result          = $event->getResult();
       $onlineonly        = false;
 
        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getAllVisibleItems() as $item)
        {
            if($item->getProduct()->getOnlineOnly()){
                $onlineonly = true;
            }
        }
 
		if(($method->getCode() == "checkmo" || $method->getCode() == "cashondelivery") && $onlineonly){
            $result->isAvailable = false;
        }
 
    }
}