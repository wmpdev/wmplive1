<?php

class WP_AdvancedSlider_Block_Adminhtml_Widget_Grid_Column_Renderer_Displayed extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $id = trim($row->getData($this->getColumn()->getIndex()));
        $options = unserialize(Mage::getModel('advancedslider/slides')->load($id)->getStyleOptions());
        if (Mage::helper('advancedslider')->isActiveSlide($options))
            return $this->__('yes');
        else
            return $this->__('no');
    }
}
