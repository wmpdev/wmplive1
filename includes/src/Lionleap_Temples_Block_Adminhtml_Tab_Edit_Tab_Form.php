<?php

/**
 * Tab edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('tab_');
        $form->setFieldNameSuffix('tab');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'tab_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Tab'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('lionleap_temples/adminhtml_tab_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $values = Mage::getResourceModel('lionleap_temples/temple_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="tab_temple_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeTempleIdLink() {
                if ($(\'tab_temple_id\').value == \'\') {
                    $(\'tab_temple_id_link\').hide();
                } else {
                    $(\'tab_temple_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_temple/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'tab_temple_id\').value);
                    $(\'tab_temple_id_link\').href = realUrl;
                    $(\'tab_temple_id_link\').innerHTML = text.replace(\'{#name}\', $(\'tab_temple_id\').options[$(\'tab_temple_id\').selectedIndex].innerHTML);
                }
            }
            $(\'tab_temple_id\').observe(\'change\', changeTempleIdLink);
            changeTempleIdLink();
            </script>';

        $fieldset->addField(
            'temple_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Temple'),
                'name'      => 'temple_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html,
				'required'  => true,
				'class' => 'required-entry',
            )
        );

        $fieldset->addField(
            'title',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Title'),
                'name'  => 'title',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'picture',
            'image',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Image'),
                'name'  => 'picture',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'description',
            'editor',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Description'),
                'name'  => 'description',
				'config' => $wysiwygConfig,
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'extra_classes',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Extra CSS Classes'),
                'name'  => 'extra_classes',
           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
				'required'  => true,
				'class' => 'required-entry',
            )
        );
        /*if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_tab')->setStoreId(Mage::app()->getStore(true)->getId());
        }*/
        $formValues = Mage::registry('current_tab')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getTabData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getTabData());
            Mage::getSingleton('adminhtml/session')->setTabData(null);
        } elseif (Mage::registry('current_tab')) {
            $formValues = array_merge($formValues, Mage::registry('current_tab')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
