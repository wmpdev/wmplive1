<?php

/**
 * Temple - Tab relation resource model collection
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Temple_Tab_Collection extends Lionleap_Temples_Model_Resource_Tab_Collection
{
    /**
     * remember if fields have been joined
     * @var bool
     */
    protected $_joinedFields = false;

    /**
     * join the link table
     *
     * @access public
     * @return Lionleap_Temples_Model_Resource_Temple_Tab_Collection
     * @author Ultimate Module Creator
     */
    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('lionleap_temples/temple_tab')),
                'related.tab_id = main_table.id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    /**
     * add temple filter
     *
     * @access public
     * @param Lionleap_Temples_Model_Temple | int $temple
     * @return Lionleap_Temples_Model_Resource_Temple_Tab_Collection
     * @author Ultimate Module Creator
     */
    public function addTempleFilter($temple)
    {
        if ($temple instanceof Lionleap_Temples_Model_Temple) {
            $temple = $temple->getId();
        }
        if (!$this->_joinedFields) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.temple_id = ?', $temple);
        return $this;
    }
}
