<?php

/**
 * Link admin block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Link extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_link';
        $this->_blockGroup         = 'lionleap_temples';
        parent::__construct();
        $this->_headerText         = Mage::helper('lionleap_temples')->__('Link');
        $this->_updateButton('add', 'label', Mage::helper('lionleap_temples')->__('Add Link'));

    }
}
