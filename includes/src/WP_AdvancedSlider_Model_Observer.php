<?php

class WP_AdvancedSlider_Model_Observer
{
    public function sliderInsertion(Varien_Event_Observer $observer)
    {
        $controllerAction = $observer->getEvent()->getAction();
        if (is_object($controllerAction)) {
            $fullActionName = $controllerAction->getFullActionName();
            if ($fullActionName == 'catalog_category_view') {
                $category = Mage::registry('current_category');
                if (is_object($category)) {
                    $categoryId = $category->getId();
                    // --- get a slider ids by the category ---
                    $sliders = Mage::helper('advancedslider')->getInsertedSliders($categoryId);
                    if (is_array($sliders) && count($sliders)) {
                        $category->setData('wp_category_sliders', $sliders);
                        $layout = $controllerAction->getLayout();
                        if (is_object($layout)) {
                            $layout->getBlock('category.products')
                                ->setTemplate('webandpeople/advancedslider/catalog/category/view.phtml');
                        }
                    } else {
                        $category->setData('wp_category_sliders', null);
                    }
                }
            }
        }
    }
}
