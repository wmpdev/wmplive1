<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagergroup_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("eventmanagergroup_form", array("legend" => $this->__("Item information")));

        $fieldset->addField("group_title", "text", array(
					"label"     => $this->__("Group Title"),
					"class"     => "required-entry",
					"required"  => true,
					"name"      => "group_title",
		));

		$fieldset->addField("group_name", "text", array(
					"label"     => $this->__("Group Name"),
					"class"     => "required-entry",
					"required"  => true,
					"name"      => "group_name",
		));
	
        $fieldset->addField("group_code", "text", array(
					"label" => $this->__("Group Code"),
					"class" => "required-entry",
					"name" => "group_code",
					"required" => true,
        ));

  		$statuses = Mage::getSingleton("eventmanager/status")->getOptionArray();

  		array_unshift($statuses, array("label" => "", "value" => ""));

	    $fieldset->addField("status", "select", array(
            "label" => $this->__("Status"),
            "class" => "required-entry",
            "name" => "status",
            "values" => $statuses
        ));

		$fieldset->addField("cms_pages", "multiselect", array(
			"label" => $this->__("Add to Pages"),
			"name" => "cms_pages[]",
			"values" => $this->fetchCMS(),
		));

        if (Mage::getSingleton("adminhtml/session")->getEventgroupData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getEventgroupData());
            Mage::getSingleton("adminhtml/session")->setEventgroupData(null);
        } 
        elseif (Mage::registry("eventmanagergroup_data")) {
            $form->setValues(Mage::registry("eventmanagergroup_data")->getData());
        }
        return parent::_prepareForm();
    }

	public function fetchCMS()	{
		$cms=array();
		$cms_pages = Mage::getModel("cms/page")->getCollection();
		$cms_pages->load();
		foreach($cms_pages as $one_row)	{
			array_push($cms,
			 	array(
			 		"value" => $one_row->getPageId(),
			 		"label"=>Mage::helper("adminhtml")->__($one_row->getTitle()),
			 	)
			);
		}
		return $cms;
	}

}