<?php

/**
 * Tab admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('tab_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Tab'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_tab',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Tab'),
                'title'   => Mage::helper('lionleap_temples')->__('Tab'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_tab_edit_tab_form'
                )
                ->toHtml(),
            )
        );
		#$this->addTab(
        #    'temples',
        #    array(
        #        'label' => Mage::helper('lionleap_temples')->__('Temples'),
        #        'url'   => $this->getUrl('*/*/temples', array('_current' => true)),
        #        'class' => 'ajax'
        #    )
        #);
        /*if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_tab',
                array(
                    'label'   => Mage::helper('lionleap_temples')->__('Store views'),
                    'title'   => Mage::helper('lionleap_temples')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'lionleap_temples/adminhtml_tab_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }*/
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve tab entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Tab
     * @author Ultimate Module Creator
     */
    public function getTab()
    {
        return Mage::registry('current_tab');
    }
}
