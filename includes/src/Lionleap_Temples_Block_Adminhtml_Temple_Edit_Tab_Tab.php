<?php

/**
 * temple - tab relation edit block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Tab extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('tab_grid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getTemple()->getId()) {
            $this->setDefaultFilter(array('in_tabs' => 1));
        }
    }

    /**
     * prepare the tab collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Tab
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('lionleap_temples/tab_collection');
        if ($this->getTemple()->getId()) {
            $constraint = 'related.temple_id='.$this->getTemple()->getId();
        } else {
            $constraint = 'related.temple_id=0';
        }
        $collection->getSelect()->joinLeft(
            array('related' => $collection->getTable('lionleap_temples/temple_tab')),
            'related.tab_id=main_table.id AND '.$constraint,
            array('position')
        );
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * prepare mass action grid
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Tab
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        return $this;
    }

    /**
     * prepare the grid columns
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Tab
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_tabs',
            array(
                'header_css_class'  => 'a-center',
                'type'              => 'checkbox',
                'name'              => 'in_tabs',
                'values'            => $this->_getSelectedTabs(),
                'align'             => 'center',
                'index'             => 'id'
            )
        );
        $this->addColumn(
            'title',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Title'),
                'align'     => 'left',
                'index'     => 'title',
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_relation',
                'params'    => array(
                    'id'    => 'getId'
                ),
                'base_link' => 'adminhtml/temples_tab/edit',
            )
        );
        $this->addColumn(
            'position',
            array(
                'header'         => Mage::helper('lionleap_temples')->__('Position'),
                'name'           => 'position',
                'width'          => 60,
                'type'           => 'number',
                'validate_class' => 'validate-number',
                'index'          => 'position',
                'editable'       => true,
            )
        );
    }

    /**
     * Retrieve selected 
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _getSelectedTabs()
    {
        $tabs = $this->getTempleTabs();
        if (!is_array($tabs)) {
            $tabs = array_keys($this->getSelectedTabs());
        }
        return $tabs;
    }

    /**
     * Retrieve selected {{siblingsLabels}}
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedTabs()
    {
        $tabs = array();
        $selected = Mage::registry('current_temple')->getSelectedTabs();
        if (!is_array($selected)) {
            $selected = array();
        }
        foreach ($selected as $tab) {
            $tabs[$tab->getId()] = array('position' => $tab->getPosition());
        }
        return $tabs;
    }

    /**
     * get row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Tab
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($item)
    {
        return '#';
    }

    /**
     * get grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            '*/*/tabsGrid',
            array(
                'id' => $this->getTemple()->getId()
            )
        );
    }

    /**
     * get the current temple
     *
     * @access public
     * @return Lionleap_Temples_Model_Temple
     * @author Ultimate Module Creator
     */
    public function getTemple()
    {
        return Mage::registry('current_temple');
    }

    /**
     * Add filter
     *
     * @access protected
     * @param object $column
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Tab
     * @author Ultimate Module Creator
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_tabs') {
            $tabIds = $this->_getSelectedTabs();
            if (empty($tabIds)) {
                $tabIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('id', array('in'=>$tabIds));
            } else {
                if ($tabIds) {
                    $this->getCollection()->addFieldToFilter('id', array('nin'=>$tabIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
}