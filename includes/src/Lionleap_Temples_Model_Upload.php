<?php

/**
 * Upload model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Upload extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'lionleap_temples_upload';
    const CACHE_TAG = 'lionleap_temples_upload';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'lionleap_temples_upload';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'upload';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/upload');
    }

    /**
     * before save upload
     *
     * @access protected
     * @return Lionleap_Temples_Model_Upload
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save upload relation
     *
     * @access public
     * @return Lionleap_Temples_Model_Upload
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Temple
     * @author Ultimate Module Creator
     */
    public function getParentTemple()
    {
        if (!$this->hasData('_parent_temple')) {
            if (!$this->getTempleId()) {
                return null;
            } else {
                $temple = Mage::getModel('lionleap_temples/temple')
                    ->load($this->getTempleId());
                if ($temple->getId()) {
                    $this->setData('_parent_temple', $temple);
                } else {
                    $this->setData('_parent_temple', null);
                }
            }
        }
        return $this->getData('_parent_temple');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
