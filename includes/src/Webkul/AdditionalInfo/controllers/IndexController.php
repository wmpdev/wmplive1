<?php 

	require_once "Mage/Customer/controllers/AccountController.php";
	class Webkul_AdditionalInfo_IndexController extends Mage_Customer_AccountController	{

	    public function indexAction() {
	        $this->loadLayout(array("default","additionalinfo_index_index"));
			$this->getLayout()->getBlock("head")->setTitle($this->__("Customer Additional Info"));
	    	$this->renderLayout();
		}

		public function additionalinfoAction() {
	        $this->loadLayout(array("default","additionalinfo_index_additionalinfo"));
			$this->getLayout()->getBlock("head")->setTitle($this->__("Account Additional Info"));
	    	$this->renderLayout();
		}
		
		public function editadditionalinfoAction() {
			$data = $this->getRequest()->getPost();
			if($data){
				$customerid = $data["customerid"];
				$collection = Mage::getModel("additionalinfo/additionalinfofields")->getCollection()->addFieldToFilter("status",1);
				$store_id = Mage::app()->getStore()->getId(); 
				if(count($collection)){
					foreach ($collection as $record) {
						$store = $record->getStore();
						$store = explode(",",$store);
						if (in_array($store_id, $store)) {
							$additionaldatas = Mage::getModel("additionalinfo/additionalinfodata")->getCollection();
							$additionaldatas->addFieldToFilter("customer_id",$customerid);
							$additionaldatas->addFieldToFilter("field_id",$record->getId());
							$additionaldatas->addFieldToFilter("store",$store_id);
							$fieldid = 0;
							foreach ($additionaldatas as $additionaldata)
								$fieldid = $additionaldata->getId();
							if($record->getInputtype() == "file"){
								$upload_dir = Mage::getBaseDir()."/media/customeradditionalinfo/".$customerid."/";
								$file = new Varien_Io_File();
								$file->mkdir($upload_dir);								
								$new_file_name = $_FILES[$record->getInputname()]["name"];
								$imagename = time().$new_file_name;							
								if($new_file_name){
									move_uploaded_file($_FILES[$record->getInputname()]["tmp_name"],$upload_dir.$new_file_name);
									$newfile = $upload_dir.$imagename;
									$oldfile = $upload_dir.$new_file_name;
									copy($oldfile, $newfile);
									unlink($oldfile);									
									$addtionaldata = array("field_id"=>$record->getId(),"value"=>$imagename,"customer_id"=>$customerid,"store"=>$store_id);
								}
								else
									$addtionaldata = array("field_id"=>$record->getId(),"customer_id"=>$customerid,"store"=>$store_id);
							}
							elseif($record->getInputtype() == "multiselect")
								$addtionaldata = array("field_id"=>$record->getId(),"value"=>implode($data[$record->getInputname()],","),"customer_id"=>$customerid,"store"=>$store_id);
							else
								$addtionaldata = array("field_id"=>$record->getId(),"value"=>$data[$record->getInputname()],"customer_id"=>$customerid,"store"=>$store_id);
							if($fieldid){
								$model = Mage::getModel("additionalinfo/additionalinfodata")->load($fieldid)->addData($addtionaldata);
								$model->setId($fieldid)->save();
								$fieldid = 0;
							}
							else
								Mage::getModel("additionalinfo/additionalinfodata")->setData($addtionaldata)->save();
						}
					}
				}
				$this->_getSession()->addSuccess(Mage::helper("additionalinfo")->__("Info was successfully updated"));
			}
	        $this->_redirect("additionalinfo/index/additionalinfo/");
		}

	}