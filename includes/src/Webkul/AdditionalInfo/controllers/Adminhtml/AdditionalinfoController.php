<?php
    class Webkul_AdditionalInfo_Adminhtml_AdditionalinfoController extends Mage_Adminhtml_Controller_Action {

        protected function _initAction() {
            $this->loadLayout()->_setActiveMenu("additionalinfo")->_addBreadcrumb($this->__("Items Manager"),$this->__("Item Manager"));
            $this->getLayout()->getBlock("head")->setTitle($this->__("Customer Custom Registration fields"));
            return $this;
        }

        public function indexAction() {
            $this->_initAction()->renderLayout();
        }

        public function editAction() {
            $id = $this->getRequest()->getParam("id");
            $model = Mage::getModel("additionalinfo/additionalinfofields")->load($id);
            if ($model->getId() || $id == 0) {
                $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
                if (!empty($data))
                    $model->setData($data);
                Mage::register("additionalinfo_data", $model);
                $this->loadLayout();
                $this->_setActiveMenu("additionalinfo");
                $this->_addContent($this->getLayout()->createBlock("additionalinfo/adminhtml_additionalinfofields_edit"))
                        ->_addLeft($this->getLayout()->createBlock("additionalinfo/adminhtml_additionalinfofields_edit_tabs"));
                $this->renderLayout();
            }
            else {
                Mage::getSingleton("adminhtml/session")->addError(Mage::helper("additionalinfo")->__("Item does not exist"));
                $this->_redirect("*/*/");
            }
        }

        public function newAction() {
            $this->_forward("edit");
        }

        public function saveAction() {
            if ($data = $this->getRequest()->getPost()) { 
    			$store = $this->getRequest()->getParam("store");
    			$storeids = "";
    			for($j=0; $j<count($store); $j++){			
    			    $storeids = $storeids.$store[$j].",";
    			}
    			$storeids = rtrim($storeids,",");
                $model = Mage::getModel("additionalinfo/additionalinfofields");
                $model->setData($data)->setId($this->getRequest()->getParam("id"));
    			$model->setStore($storeids);
                try {
                    if($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL)
                        $model->setCreatedTime(now())->setUpdateTime(now());
                    else
                        $model->setUpdateTime(now());
                    $model->save();
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("additionalinfo")->__("Item was successfully saved"));
                    Mage::getSingleton("adminhtml/session")->setFormData(false);

                    if ($this->getRequest()->getParam("back")) {
                        $this->_redirect("*/*/edit", array("id" => $model->getId()));
                        return;
                    }
                    $this->_redirect("*/*/");
                    return;
                }
                catch (Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                    Mage::getSingleton("adminhtml/session")->setFormData($data);
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                    return;
                }
            }
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("additionalinfo")->__("Unable to find item to save"));
            $this->_redirect("*/*/");
        }
    	
    	public function getstoreoptionAction() {
    		$storeid = $this->getRequest()->getParam("storeid");
    		$html = "";
    		$data = array();
    		$customerid = $this->getRequest()->getParam("customerid");
    		$additionalinfo = Mage::getModel("additionalinfo/additionalinfofields")->getCollection();
    		$additionalinfo->setOrder("setorder","ASC");
    		foreach ($additionalinfo as $record) {
    			$store = $record->getStore();
    			$store = explode(",",$store);
    			if (in_array($storeid, $store)) {
    				$collection = Mage::getModel("additionalinfo/additionalinfodata")->getCollection();
    				$collection->addFieldToFilter("field_id",$record->getId());
    				$collection->addFieldToFilter("customer_id",$customerid);	
    				$collection->addFieldToFilter("store",$storeid);	
    				$fieldval = "";
    				foreach ($collection as $coll)
    					$fieldval = $coll->getValue();
    				$html .= "<tr><td class='label'><label for=_accountwebsite_id>".$record->getLabelname()."</label></td><td class='value'><div class='store-scope'>";
    				if($record->getInputtype() == "text"){
    					$html .= "<input type='".$record->getInputtype()."' class='input-text' id='".$record->getInputname()."' name='".$record->getInputname()."' title='".$record->getInputname()."' value='".$fieldval."'/>";
    				}
                    if($record->getInputtype() == "textarea"){
    					$html .= "<textarea class='input-text' id='".$record->getInputname()."' name='".$record->getInputname()."' title='".$record->getInputname()."'>".$fieldval."</textarea>";
    				}
                    if($record->getInputtype() == "select"){
    					$selectoption = explode(",",$record->getSelectoption());
    					$html .= "<select class='input-text' id='".$record->getInputname()."' name='".$record->getInputname()."' title='".$record->getInputname()."'><option value=''>".Mage::helper("additionalinfo")->__(" --Please Select-- ")."</option>";
    					foreach($selectoption as $opt){
    						if($fieldval == $opt)
    							$html .= "<option selected='selected' value='".$opt."'>".$opt."</option>";
    						else
    							$html .= "<option value='".$opt."'>".$opt."</option>";
    					}
    					$html .= "</select>";
    				}
                    if($record->getInputtype() == "multiselect"){
    					$selectoption = explode(",",$record->getSelectoption());
    					$html .= "<select class='input-text' id='".$record->getInputname()."' name='".$record->getInputname()."[]' title='".$record->getInputname()."' multiple><option value=''>".Mage::helper("additionalinfo")->__(" --Please Select-- ")."</option>";
						foreach($selectoption as $opt){
							$flag = 0;
							foreach(explode(",",$fieldval) as $multival){ 
								if($opt == $multival){
									$html .= "<option selected='selected' value='".$opt."'>".$opt."</option>";
									$flag = 1;
								}
							}
							if($flag == 0)
								$html .= "<option value='".$opt."'>".$opt."</option>";
						}
    					$html .= "</select>";
    				}
                    if($record->getInputtype() == "radio"){	
    					$selectoption = explode(",",$record->getSelectoption());
    					$html .= "<br/><div style=' width: 254px;'>";
    					foreach($selectoption as $opt){
    						if($opt == $fieldval)
    							$html .= "<input type='radio' id='".$record->getInputname()."' name='".$record->getInputname()."' value='".$opt."' title='".$opt."' checked />".$opt."&nbsp;&nbsp;";
                            else
    							$html .= "<input type='radio' id='".$record->getInputname()."' name='".$record->getInputname()."' value='".$opt."' title='".$opt."' />".$opt."&nbsp;&nbsp;";
    					}
    					$html .= "</div>";
    				}
                    if($record->getInputtype() == "file"){
    					if($fieldval)
    						$html .= "<br/><img src='".Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/customeradditionalinfo/'.$customerid.'/'.$fieldval."' width='100px' height='70px'/><br/>";
    					$html .= "<input type='file' id='".$record->getInputname()."' name='".$record->getInputname()."' title='".Mage::helper("additionalinfo")->__("Select Image")."'/>";
    				}
    				$html .= "</div></td></tr>";
    			}
    		}
    		echo $html;
    	}

        public function deleteAction() {
            if ($this->getRequest()->getParam("id") > 0) {
                try {
                    $model = Mage::getModel("additionalinfo/additionalinfo")->load($this->getRequest()->getParam("id"));
                    $model->delete();
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                    $this->_redirect("*/*/");
                }
                catch (Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                    $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                }
            }
            $this->_redirect("*/*/");
        }

        public function massDeleteAction() {
            $fields = $this->getRequest()->getParam("fields");
            if (!is_array($fields))
                Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Please select item(s)"));
            else {
                try {
                    foreach ($fields as $field) {
                        $model = Mage::getModel("additionalinfo/additionalinfofields")->load($field);
                        $model->delete();
                    }
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Total of %d record(s) were successfully deleted", count($fields)));
                }
                catch (Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                }
            }
            $this->_redirect("*/*/index");
        }

        public function massStatusAction() {
            $fields = $this->getRequest()->getParam("fields");
            if (!is_array($fields))
                Mage::getSingleton("adminhtml/session")->addError($this->__("Please select item(s)"));
            else {
                try {
                    foreach ($fields as $field) {
                        $wkadditionalinfo = Mage::getSingleton("additionalinfo/additionalinfofields")
                                        ->load($field)
                                        ->setStatus($this->getRequest()->getParam("status"))
                                        ->setIsMassupdate(true)
                                        ->save();
                    }
                    $this->_getSession()->addSuccess($this->__("Total of %d record(s) were successfully updated", count($fields)));
                }
                catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
            $this->_redirect("*/*/index");
        }

    }