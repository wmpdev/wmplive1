<?php
class Webkul_Eventmanager_ImageController extends Mage_Core_Controller_Front_Action {

    public function directiveAction() 	{ 
		$directive = $this->getRequest()->getParam("___directive"); 
		$directive = Mage::helper("core")->urlDecode($directive); 
		$url = Mage::getModel("core/email_template_filter")->filter($directive); 
		try { 
			$image = Varien_Image_Adapter::factory("GD2"); 
			$image->open($url); 
			$image->display(); 
		} 
		catch (Exception $e) { 
			$image = Varien_Image_Adapter::factory("GD2"); 
			$image->open(Mage::getSingleton("cms/wysiwyg_config")->getSkinImagePlaceholderUrl()); 
			$image->display(); 
		}
	}

}