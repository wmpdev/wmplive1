<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagergroup_Edit_Tab_Eventmanager extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("eventmanagerLeftGrid");
        $this->setDefaultSort("event_id");
        $this->setUseAjax(true);
    }

    public function getEventgroupData() {
        return Mage::registry("eventmanagergroup_data");
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel("eventmanager/eventmanager")->getCollection();
        $collection->getSelect()->order("event_id");
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _addColumnFilterToCollection($column) {
        if ($this->getCollection()) {
            if ($column->getId() == "eventmanager_triggers") {
                $eventIds = $this->_getSelectedEvent();
                if (empty($eventIds))
                    $eventIds = 0;
                if ($column->getFilter()->getValue()) 
                    $this->getCollection()->addFieldToFilter("event_id", array("in" => $eventIds));
                else {
                    if ($eventIds) 
                        $this->getCollection()->addFieldToFilter("event_id", array("in" => $eventIds));
                }
            } 
            else {
                parent::_addColumnFilterToCollection($column);
            }
        }
        return $this;;
    }

    protected function _prepareColumns() {
        $this->addColumn("eventmanager_triggers", array(
            "header_css_class" => "a-center",
            "type" => "checkbox",
            "values" => $this->_getSelectedEvents(),
            "align" => "center",
            "index" => "event_id"
        ));

        $this->addColumn("event_id", array(
            "header" => $this->__("ID"),
            "sortable" => true,
            "width" => "50px",
            "align" => "center",
            "index" => "event_id"
        ));

		$this->addColumn("title", array(
            "header" => $this->__("Event"),
            "index" => "title",
			"align" => "center",
        )); 

        $this->addColumn("body", array(
            "header" => $this->__("Content"),
            "index" => "body",
			"width" => "700px",
			"align" => "center",
        ));
		
        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl("*/*/eventgrid", array("_current" => true));
    }

    protected function _getSelectedEvents() {
        $events = $this->getRequest()->getPost("selected_eventmanagers");
        if (is_null($events)) {
            $events = explode(",", $this->getEventgroupData()->getEventIds());
            return (sizeof($events) > 0 ? $events : 0);
        }
        return $events;
    }

}