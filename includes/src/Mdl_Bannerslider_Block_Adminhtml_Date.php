<?php
class Mdl_Bannerslider_Block_Adminhtml_Date extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        $this->setChild('refresh_button', $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label' => Mage::helper('adminhtml')->__('Refresh'),
                            'onclick' => $this->getRefreshButtonCallback(),
                            'class' => 'task'
                        ))
        );
        parent::_prepareLayout();
        return $this;
    }

    public function getRefreshButtonHtml() {
        return $this->getChildHtml('refresh_button');
    }

    public function getDateFormat() {
        return $this->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    public function getLocale() {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }

    public function getFilter($nameFilter) {
		$date = array();			
        $filter = Mage::app()->getRequest()->getParam('filter');
		if($filter){
			$date = Mage::helper('adminhtml')->prepareFilterString($filter);
			if(isset($date[$nameFilter])) return $date[$nameFilter];			
		} 
		return null;
    }

}