<?php

    class Webkul_AdditionalInfo_Block_Adminhtml_Tabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs{

        private $parent;

        protected function _prepareLayout()   {
            $this->parent = parent::_prepareLayout();
            $this->addTab("tabid", array(
                          "label"     => Mage::helper("additionalinfo")->__("Custom Registration Fields"),
                          "content"   => $this->getLayout()->createBlock("additionalinfo/adminhtml_tabs_tabid")->toHtml()
            ));
            return $this->parent;
        }

    }