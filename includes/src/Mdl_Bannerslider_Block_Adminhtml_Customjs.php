<?php

class Mdl_Bannerslider_Block_Adminhtml_Customjs extends Mage_Core_Block_Template {

    public function _prepareLayout() {

        parent::_prepareLayout();
        return $this;
    }

    public function getDateFormat() {
        return $this->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }

    public function getLocale() {
        if (!$this->_locale) {
            $this->_locale = Mage::app()->getLocale();
        }
        return $this->_locale;
    }

}