<?php 

class Airpay_Block_Redirect extends Mage_Core_Block_Abstract
{
    /**
     * This will just spit out the html without loading any other magento stuff
     * and the form will be submitted right away.
     */
    protected function _toHtml() 
    {
        $airpay = Mage::getModel('airpay/transact');
        $fields = $airpay->getCheckoutFormFields();
        $form = '<form id="airpay_checkout" method="POST" action="' . $airpay->getAirpayTransactAction() . '">';
        foreach($fields as $key => $value) {
            $form .= '<input type="hidden" name="'.$key.'" value="'.Checksum::sanitizedParam($value).'" />'."\n";
            #$form .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />'."\n";
        }
        $form .= '</form>';
        $html = '<html><body>';
        $html .= $this->__('You will be redirected to the Airpay website in a few seconds.');
        $html .= $form;
        $html.= '<script type="text/javascript">document.getElementById("airpay_checkout").submit();</script>';
        $html.= '</body></html>';
        return $html;
    }
}
