<?php

/**
 * Search admin grid block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Search_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('searchGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Search_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('lionleap_temples/search')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Search_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Id'),
                'index'  => 'id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'deity_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Deity'),
                'index'     => 'deity_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/deity_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getDeityId'
                ),
                'base_link' => 'adminhtml/temples_deity/edit'
            )
        );
        $this->addColumn(
            'religion_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Religion'),
                'index'     => 'religion_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/religion_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getReligionId'
                ),
                'base_link' => 'adminhtml/temples_religion/edit'
            )
        );
        $this->addColumn(
            'short_temple_search',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Short Temple Search'),
                'align'     => 'left',
                'index'     => 'short_temple_search',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                    'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'long_temple_search',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Long Temple Search'),
                'index'  => 'long_temple_search',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'short_location_search',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Short Location Search'),
                'index'  => 'short_location_search',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'long_location_search',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Long Location Search'),
                'index'  => 'long_location_search',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'ip',
            array(
                'header' => Mage::helper('lionleap_temples')->__('IP Address'),
                'index'  => 'ip',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('lionleap_temples')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('lionleap_temples')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('lionleap_temples')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('lionleap_temples')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('lionleap_temples')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Search_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('search');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('lionleap_temples')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('lionleap_temples')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Status'),
                        'values' => array(
                            'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                            'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/deity_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'deity_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Deity'),
                'url'        => $this->getUrl('*/*/massDeityId', array('_current'=>true)),
                'additional' => array(
                    'flag_deity_id' => array(
                        'name'   => 'flag_deity_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Deity'),
                        'values' => $values
                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/religion_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'religion_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Religion'),
                'url'        => $this->getUrl('*/*/massReligionId', array('_current'=>true)),
                'additional' => array(
                    'flag_religion_id' => array(
                        'name'   => 'flag_religion_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Religion'),
                        'values' => $values
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Search
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Search_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
