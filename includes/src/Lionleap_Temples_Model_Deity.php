<?php

/**
 * Deity model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Deity extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'lionleap_temples_deity';
    const CACHE_TAG = 'lionleap_temples_deity';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'lionleap_temples_deity';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'deity';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/deity');
    }

    /**
     * before save deity
     *
     * @access protected
     * @return Lionleap_Temples_Model_Deity
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save deity relation
     *
     * @access public
     * @return Lionleap_Temples_Model_Deity
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return Lionleap_Temples_Model_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedTemplesCollection()
    {
        if (!$this->hasData('_temple_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('lionleap_temples/temple_collection')
                        ->addFieldToFilter('deity_id', $this->getId());
                $this->setData('_temple_collection', $collection);
            }
        }
        return $this->getData('_temple_collection');
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return Lionleap_Temples_Model_Search_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedSearchesCollection()
    {
        if (!$this->hasData('_search_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('lionleap_temples/search_collection')
                        ->addFieldToFilter('deity_id', $this->getId());
                $this->setData('_search_collection', $collection);
            }
        }
        return $this->getData('_search_collection');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Religion
     * @author Ultimate Module Creator
     */
    public function getParentReligion()
    {
        if (!$this->hasData('_parent_religion')) {
            if (!$this->getReligionId()) {
                return null;
            } else {
                $religion = Mage::getModel('lionleap_temples/religion')
                    ->load($this->getReligionId());
                if ($religion->getId()) {
                    $this->setData('_parent_religion', $religion);
                } else {
                    $this->setData('_parent_religion', null);
                }
            }
        }
        return $this->getData('_parent_religion');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
