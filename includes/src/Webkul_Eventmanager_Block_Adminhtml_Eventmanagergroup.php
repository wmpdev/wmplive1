<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagergroup extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = "adminhtml_eventmanagergroup";
        $this->_blockGroup = "eventmanager";
        $this->_headerText = $this->__("Add/Manage Event Group");
        $this->_addButtonLabel = $this->__("Add Event Group");
        parent::__construct();
    }

}