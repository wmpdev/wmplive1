<?php

	class Webkul_BookingSystem_Model_Mysql4_Bookings_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract	{

    	public function _construct()	{
		    parent::_construct();
		    $this->_init("bookingsystem/bookings");
    	}

	}