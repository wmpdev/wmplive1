<?php

    class Webkul_AdditionalInfo_Block_Adminhtml_Additionalinfofields_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

        protected function _prepareForm() {
            $form = new Varien_Data_Form();
            $this->setForm($form);
            $fieldset = $form->addFieldset("additionalinfo_form", array("legend" => Mage::helper("additionalinfo")->__("Item information")));

            $fieldset->addField("labelname", "text", array(
                "label"     => Mage::helper("additionalinfo")->__("Label"),
                "class"     => "required-entry",
                "required"  => true,
                "name"      => "labelname"
            ));

    		$fieldset->addField("inputname", "text", array(
                "label"     => Mage::helper("additionalinfo")->__("Name"),
                "class"     => "required-entry",
                "required"  => true,
                "name"      => "inputname"
            ));

            $fieldset->addField("inputtype", "select", array(
                "label"      => Mage::helper("additionalinfo")->__("Type"),
                "name"       => "inputtype",
    			"onkeydown"  => "selectoption()",
    			"onblur"     => "selectoption()",
    			"onchange"   => "selectoption()",
    			"values"     => array(
                    array(
                        "value" => "text",
                        "label" => Mage::helper("additionalinfo")->__("Text")
                    ),
    				array(
                        "value" => "textarea",
                        "label" => Mage::helper("additionalinfo")->__("Text Area")
                    ),
                    array(
                        "value" => "select",
                        "label" => Mage::helper("additionalinfo")->__("Dropdown")
                    ),
    				array(
                        "value" => "multiselect",
                        "label" => Mage::helper("additionalinfo")->__("Multiple Select")
                    ),
    				array(
                        "value" => "file",
                        "label" => Mage::helper("additionalinfo")->__("Media Image")
                    ),
    				array(
                        "value" => "radio",
                        "label" => Mage::helper("additionalinfo")->__("Radio Button")
                    )
                )
            ));

    		$fieldset->addField("selectoption", "textarea", array(
                "label"     => Mage::helper("additionalinfo")->__("Option"),
    			"style"     => "display:none",
                "name"      => "selectoption",
    			"after_element_html" => "<small style='color:red;'>eg. enter value seperated by ","</small>"
            ));

    		$fieldset->addField("setorder", "text", array(
                "label"     => Mage::helper("additionalinfo")->__("Set Order"),
    			"class"     => "required-entry",
                "required"  => true,
                "name"      => "setorder"
            ));

    		$fieldset->addField("reuiredfield", "select", array(
                "label"     => Mage::helper("additionalinfo")->__("Value Reuired"),
                "class"     => "required-entry",
                "name"      => "reuiredfield",
                "values"    => array(
                    array(
                        "value" => 1,
                        "label" => Mage::helper("additionalinfo")->__("Yes")
                    ),
                    array(
                        "value" => 0,
                        "label" => Mage::helper("additionalinfo")->__("No")
                    )
                )
            ));

    		$fieldset->addField("store", "multiselect", array(
                "label"		=> Mage::helper("additionalinfo")->__("Store To Show Fields"),
                "class" 	=> "required-entry",
                "name" 		=> "store",
                "values" 	=> $this->stores()
            ));

    		$fieldset->addField("status", "select", array(
                "label"     => Mage::helper("additionalinfo")->__("Status"),
                "class"     => "required-entry",
                "name"      => "status",
                "values"    => array(
                    array(
                        "value" => 1,
                        "label" => Mage::helper("additionalinfo")->__("Enabled")
                    ),
                    array(
                        "value" => 2,
                        "label" => Mage::helper("additionalinfo")->__("Disabled")
                    )
                )
            ));

            if (Mage::getSingleton("adminhtml/session")->getAdditionalinfoData()) {
                $form->setValues(Mage::getSingleton("adminhtml/session")->getAdditionalinfoData());
                Mage::getSingleton("adminhtml/session")->setAdditionalinfoData(null);
            }
            elseif (Mage::registry("additionalinfo_data")) {
                $form->setValues(Mage::registry("additionalinfo_data")->getData());
            }
            return parent::_prepareForm();
        }

    	public function stores(){
    		$store = array();
    		$everyStore = Mage::app()->getStores();
    		foreach ($everyStore as $eachStore => $val) {
    			array_push($store,array(
    					"label" => Mage::app()->getStore($eachStore)->getName(),
    					"value" => Mage::app()->getStore($eachStore)->getId()
    				));
    		}
    		return $store;
    	}

    }