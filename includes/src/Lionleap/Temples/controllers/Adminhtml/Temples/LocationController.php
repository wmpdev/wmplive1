<?php

/**
 * Location admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_LocationController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the location
     *
     * @access protected
     * @return Lionleap_Temples_Model_Location
     */
    protected function _initLocation()
    {
        $locationId  = (int) $this->getRequest()->getParam('id');
        $location    = Mage::getModel('lionleap_temples/location');
        if ($locationId) {
            $location->load($locationId);
        }
        Mage::register('current_location', $location);
        return $location;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Locations'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit location - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $locationId    = $this->getRequest()->getParam('id');
        $location      = $this->_initLocation();
        if ($locationId && !$location->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This location no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getLocationData(true);
        if (!empty($data)) {
            $location->setData($data);
        }
        Mage::register('location_data', $location);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Locations'));
        if ($location->getId()) {
            $this->_title($location->getCity());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add location'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new location action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save location - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('location')) {
            try {
                $location = $this->_initLocation();
                $location->addData($data);
                $location->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Location was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $location->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setLocationData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the location.')
                );
                Mage::getSingleton('adminhtml/session')->setLocationData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find location to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete location - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $location = Mage::getModel('lionleap_temples/location');
                $location->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Location was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting location.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find location to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete location - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $locationIds = $this->getRequest()->getParam('location');
        if (!is_array($locationIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select locations to delete.')
            );
        } else {
            try {
                foreach ($locationIds as $locationId) {
                    $location = Mage::getModel('lionleap_temples/location');
                    $location->setId($locationId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d locations were successfully deleted.', count($locationIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting locations.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $locationIds = $this->getRequest()->getParam('location');
        if (!is_array($locationIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select locations.')
            );
        } else {
            try {
                foreach ($locationIds as $locationId) {
                $location = Mage::getSingleton('lionleap_temples/location')->load($locationId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d locations were successfully updated.', count($locationIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating locations.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'location.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_location_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'location.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_location_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'location.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_location_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/location');
    }
}
