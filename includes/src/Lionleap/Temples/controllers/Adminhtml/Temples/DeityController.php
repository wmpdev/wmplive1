<?php

/**
 * Deity admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_DeityController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the deity
     *
     * @access protected
     * @return Lionleap_Temples_Model_Deity
     */
    protected function _initDeity()
    {
        $deityId  = (int) $this->getRequest()->getParam('id');
        $deity    = Mage::getModel('lionleap_temples/deity');
        if ($deityId) {
            $deity->load($deityId);
        }
        Mage::register('current_deity', $deity);
        return $deity;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Deities'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit deity - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $deityId    = $this->getRequest()->getParam('id');
        $deity      = $this->_initDeity();
        if ($deityId && !$deity->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This deity no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getDeityData(true);
        if (!empty($data)) {
            $deity->setData($data);
        }
        Mage::register('deity_data', $deity);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Deities'));
        if ($deity->getId()) {
            $this->_title($deity->getName());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add deity'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new deity action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save deity - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('deity')) {
            try {
                $deity = $this->_initDeity();
                $deity->addData($data);
                $deity->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Deity was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $deity->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setDeityData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the deity.')
                );
                Mage::getSingleton('adminhtml/session')->setDeityData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find deity to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete deity - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $deity = Mage::getModel('lionleap_temples/deity');
                $deity->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Deity was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting deity.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find deity to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete deity - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $deityIds = $this->getRequest()->getParam('deity');
        if (!is_array($deityIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select deities to delete.')
            );
        } else {
            try {
                foreach ($deityIds as $deityId) {
                    $deity = Mage::getModel('lionleap_temples/deity');
                    $deity->setId($deityId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d deities were successfully deleted.', count($deityIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting deities.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $deityIds = $this->getRequest()->getParam('deity');
        if (!is_array($deityIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select deities.')
            );
        } else {
            try {
                foreach ($deityIds as $deityId) {
                $deity = Mage::getSingleton('lionleap_temples/deity')->load($deityId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d deities were successfully updated.', count($deityIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating deities.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass religion change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massReligionIdAction()
    {
        $deityIds = $this->getRequest()->getParam('deity');
        if (!is_array($deityIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select deities.')
            );
        } else {
            try {
                foreach ($deityIds as $deityId) {
                $deity = Mage::getSingleton('lionleap_temples/deity')->load($deityId)
                    ->setReligionId($this->getRequest()->getParam('flag_religion_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d deities were successfully updated.', count($deityIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating deities.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'deity.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_deity_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'deity.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_deity_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'deity.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_deity_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/deity');
    }
}
