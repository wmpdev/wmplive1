<?php

/**
 * Tab admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_TabController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the tab
     *
     * @access protected
     * @return Lionleap_Temples_Model_Tab
     */
    protected function _initTab()
    {
        $tabId  = (int) $this->getRequest()->getParam('id');
        $tab    = Mage::getModel('lionleap_temples/tab');
        if ($tabId) {
            $tab->load($tabId);
        }
        Mage::register('current_tab', $tab);
        return $tab;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Tabs'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit tab - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $tabId    = $this->getRequest()->getParam('id');
        $tab      = $this->_initTab();
        if ($tabId && !$tab->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This tab no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getTabData(true);
        if (!empty($data)) {
            $tab->setData($data);
        }
        Mage::register('tab_data', $tab);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Tabs'));
        if ($tab->getId()) {
            $this->_title($tab->getTitle());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add tab'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new tab action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save tab - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('tab')) {
            try {
                $tab = $this->_initTab();
                $tab->addData($data);
                $pictureName = $this->_uploadAndGetName(
                    'picture',
                    Mage::helper('lionleap_temples/tab_image')->getImageBaseDir(),
                    $data
                );
                $tab->setData('picture', $pictureName);
				$temples = $this->getRequest()->getPost('temples', -1);
                if ($temples != -1) {
                    $tab->setTemplesData(
                        Mage::helper('adminhtml/js')->decodeGridSerializedInput($temples)
                    );
                }
                $tab->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Tab was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $tab->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['picture']['value'])) {
                    $data['picture'] = $data['picture']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setTabData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['picture']['value'])) {
                    $data['picture'] = $data['picture']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the tab.')
                );
                Mage::getSingleton('adminhtml/session')->setTabData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find tab to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete tab - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $tab = Mage::getModel('lionleap_temples/tab');
                $tab->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Tab was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting tab.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find tab to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete tab - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $tabIds = $this->getRequest()->getParam('tab');
        if (!is_array($tabIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select tabs to delete.')
            );
        } else {
            try {
                foreach ($tabIds as $tabId) {
                    $tab = Mage::getModel('lionleap_temples/tab');
                    $tab->setId($tabId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d tabs were successfully deleted.', count($tabIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting tabs.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $tabIds = $this->getRequest()->getParam('tab');
        if (!is_array($tabIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select tabs.')
            );
        } else {
            try {
                foreach ($tabIds as $tabId) {
                $tab = Mage::getSingleton('lionleap_temples/tab')->load($tabId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d tabs were successfully updated.', count($tabIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating tabs.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass temple change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massTempleIdAction()
    {
        $tabIds = $this->getRequest()->getParam('tab');
        if (!is_array($tabIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select tabs.')
            );
        } else {
            try {
                foreach ($tabIds as $tabId) {
                $tab = Mage::getSingleton('lionleap_temples/tab')->load($tabId)
                    ->setTempleId($this->getRequest()->getParam('flag_temple_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d tabs were successfully updated.', count($tabIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating tabs.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

	/**
     * get  action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function templesAction()
    {
        $this->_initTab();
        $this->loadLayout();
        $this->getLayout()->getBlock('lionleap_temples.tab.edit.tab.temple')
            ->setTabTemples($this->getRequest()->getPost('tab_temples', null));
        $this->renderLayout();
    }

    /**
     * get  grid  action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function templesgridAction()
    {
        $this->_initTab();
        $this->loadLayout();
        $this->getLayout()->getBlock('lionleap_temples.tab.edit.tab.temple')
            ->setTabTemples($this->getRequest()->getPost('tab_temples', null));
        $this->renderLayout();
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'tab.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_tab_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'tab.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_tab_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'tab.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_tab_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/tab');
    }
}
