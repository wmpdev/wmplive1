<?php

/**
 * Temple admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_TempleController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the temple
     *
     * @access protected
     * @return Lionleap_Temples_Model_Temple
     */
    protected function _initTemple()
    {
        $templeId  = (int) $this->getRequest()->getParam('id');
        $temple    = Mage::getModel('lionleap_temples/temple');
        if ($templeId) {
            $temple->load($templeId);
        }
        Mage::register('current_temple', $temple);
        return $temple;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Temples'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit temple - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $templeId    = $this->getRequest()->getParam('id');
        $temple      = $this->_initTemple();
        if ($templeId && !$temple->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This temple no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getTempleData(true);
        if (!empty($data)) {
            $temple->setData($data);
        }
        Mage::register('temple_data', $temple);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Temples'));
        if ($temple->getId()) {
            $this->_title($temple->getName());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add temple'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new temple action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save temple - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('temple')) {
            try {
                //$data = $this->_filterDates($data, array('best_time_start' ,'best_time_end'));
                $temple = $this->_initTemple();
                $temple->addData($data);
				$tabs = $this->getRequest()->getPost('tabs', -1);
                if ($tabs != -1) {
                    $temple->setTabsData(
                        Mage::helper('adminhtml/js')->decodeGridSerializedInput($tabs)
                    );
                }
                $temple->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Temple was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $temple->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setTempleData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the temple.')
                );
                Mage::getSingleton('adminhtml/session')->setTempleData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find temple to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete temple - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $temple = Mage::getModel('lionleap_temples/temple');
                $temple->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Temple was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting temple.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find temple to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete temple - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $templeIds = $this->getRequest()->getParam('temple');
        if (!is_array($templeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select temples to delete.')
            );
        } else {
            try {
                foreach ($templeIds as $templeId) {
                    $temple = Mage::getModel('lionleap_temples/temple');
                    $temple->setId($templeId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d temples were successfully deleted.', count($templeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting temples.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $templeIds = $this->getRequest()->getParam('temple');
        if (!is_array($templeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select temples.')
            );
        } else {
            try {
                foreach ($templeIds as $templeId) {
                $temple = Mage::getSingleton('lionleap_temples/temple')->load($templeId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d temples were successfully updated.', count($templeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating temples.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass location change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massLocationIdAction()
    {
        $templeIds = $this->getRequest()->getParam('temple');
        if (!is_array($templeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select temples.')
            );
        } else {
            try {
                foreach ($templeIds as $templeId) {
                $temple = Mage::getSingleton('lionleap_temples/temple')->load($templeId)
                    ->setLocationId($this->getRequest()->getParam('flag_location_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d temples were successfully updated.', count($templeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating temples.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass deity change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeityIdAction()
    {
        $templeIds = $this->getRequest()->getParam('temple');
        if (!is_array($templeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select temples.')
            );
        } else {
            try {
                foreach ($templeIds as $templeId) {
                $temple = Mage::getSingleton('lionleap_temples/temple')->load($templeId)
                    ->setDeityId($this->getRequest()->getParam('flag_deity_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d temples were successfully updated.', count($templeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating temples.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass religion change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massReligionIdAction()
    {
        $templeIds = $this->getRequest()->getParam('temple');
        if (!is_array($templeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select temples.')
            );
        } else {
            try {
                foreach ($templeIds as $templeId) {
                $temple = Mage::getSingleton('lionleap_temples/temple')->load($templeId)
                    ->setReligionId($this->getRequest()->getParam('flag_religion_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d temples were successfully updated.', count($templeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating temples.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'temple.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_temple_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'temple.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_temple_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'temple.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_temple_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/temple');
    }

	/**
     * get  action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function tabsAction()
    {
        $this->_initTemple();
        $this->loadLayout();
		$this->getLayout()->getBlock('lionleap_temples.temple.edit.tab.tab')
            ->setTempleTabs($this->getRequest()->getPost('temple_tabs', null));
        $this->renderLayout();
    }

    /**
     * get  grid  action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function tabsgridAction()
    {
        $this->_initTemple();
        $this->loadLayout();
        $this->getLayout()->getBlock('lionleap_temples.temple.edit.tab.tab')
            ->setTempleTabs($this->getRequest()->getPost('temple_tabs', null));
        $this->renderLayout();
    }
}
