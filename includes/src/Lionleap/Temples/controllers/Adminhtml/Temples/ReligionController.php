<?php

/**
 * Religion admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_ReligionController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the religion
     *
     * @access protected
     * @return Lionleap_Temples_Model_Religion
     */
    protected function _initReligion()
    {
        $religionId  = (int) $this->getRequest()->getParam('id');
        $religion    = Mage::getModel('lionleap_temples/religion');
        if ($religionId) {
            $religion->load($religionId);
        }
        Mage::register('current_religion', $religion);
        return $religion;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Religions'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit religion - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $religionId    = $this->getRequest()->getParam('id');
        $religion      = $this->_initReligion();
        if ($religionId && !$religion->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This religion no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getReligionData(true);
        if (!empty($data)) {
            $religion->setData($data);
        }
        Mage::register('religion_data', $religion);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Religions'));
        if ($religion->getId()) {
            $this->_title($religion->getName());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add religion'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new religion action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save religion - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('religion')) {
            try {
                $religion = $this->_initReligion();
                $religion->addData($data);
                $religion->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Religion was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $religion->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setReligionData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the religion.')
                );
                Mage::getSingleton('adminhtml/session')->setReligionData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find religion to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete religion - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $religion = Mage::getModel('lionleap_temples/religion');
                $religion->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Religion was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting religion.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find religion to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete religion - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $religionIds = $this->getRequest()->getParam('religion');
        if (!is_array($religionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select religions to delete.')
            );
        } else {
            try {
                foreach ($religionIds as $religionId) {
                    $religion = Mage::getModel('lionleap_temples/religion');
                    $religion->setId($religionId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d religions were successfully deleted.', count($religionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting religions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $religionIds = $this->getRequest()->getParam('religion');
        if (!is_array($religionIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select religions.')
            );
        } else {
            try {
                foreach ($religionIds as $religionId) {
                $religion = Mage::getSingleton('lionleap_temples/religion')->load($religionId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d religions were successfully updated.', count($religionIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating religions.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'religion.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_religion_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'religion.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_religion_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'religion.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_religion_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/religion');
    }
}
