<?php

/**
 * Search admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_SearchController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the search
     *
     * @access protected
     * @return Lionleap_Temples_Model_Search
     */
    protected function _initSearch()
    {
        $searchId  = (int) $this->getRequest()->getParam('id');
        $search    = Mage::getModel('lionleap_temples/search');
        if ($searchId) {
            $search->load($searchId);
        }
        Mage::register('current_search', $search);
        return $search;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Searches'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit search - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $searchId    = $this->getRequest()->getParam('id');
        $search      = $this->_initSearch();
        if ($searchId && !$search->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This search no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getSearchData(true);
        if (!empty($data)) {
            $search->setData($data);
        }
        Mage::register('search_data', $search);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Searches'));
        if ($search->getId()) {
            $this->_title($search->getShortTempleSearch());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add search'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new search action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save search - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('search')) {
            try {
                $search = $this->_initSearch();
                $search->addData($data);
                $search->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Search was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $search->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setSearchData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the search.')
                );
                Mage::getSingleton('adminhtml/session')->setSearchData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find search to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete search - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $search = Mage::getModel('lionleap_temples/search');
                $search->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Search was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting search.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find search to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete search - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $searchIds = $this->getRequest()->getParam('search');
        if (!is_array($searchIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select searches to delete.')
            );
        } else {
            try {
                foreach ($searchIds as $searchId) {
                    $search = Mage::getModel('lionleap_temples/search');
                    $search->setId($searchId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d searches were successfully deleted.', count($searchIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting searches.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $searchIds = $this->getRequest()->getParam('search');
        if (!is_array($searchIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select searches.')
            );
        } else {
            try {
                foreach ($searchIds as $searchId) {
                $search = Mage::getSingleton('lionleap_temples/search')->load($searchId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d searches were successfully updated.', count($searchIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating searches.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass deity change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeityIdAction()
    {
        $searchIds = $this->getRequest()->getParam('search');
        if (!is_array($searchIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select searches.')
            );
        } else {
            try {
                foreach ($searchIds as $searchId) {
                $search = Mage::getSingleton('lionleap_temples/search')->load($searchId)
                    ->setDeityId($this->getRequest()->getParam('flag_deity_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d searches were successfully updated.', count($searchIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating searches.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass religion change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massReligionIdAction()
    {
        $searchIds = $this->getRequest()->getParam('search');
        if (!is_array($searchIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select searches.')
            );
        } else {
            try {
                foreach ($searchIds as $searchId) {
                $search = Mage::getSingleton('lionleap_temples/search')->load($searchId)
                    ->setReligionId($this->getRequest()->getParam('flag_religion_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d searches were successfully updated.', count($searchIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating searches.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'search.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_search_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'search.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_search_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'search.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_search_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/search');
    }
}
