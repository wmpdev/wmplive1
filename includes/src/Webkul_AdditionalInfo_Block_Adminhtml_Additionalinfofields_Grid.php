<?php

    class Webkul_AdditionalInfo_Block_Adminhtml_Additionalinfofields_Grid extends Mage_Adminhtml_Block_Widget_Grid {

        public function __construct() {
            parent::__construct();
            $this->setDefaultSort("id");
            $this->setDefaultDir("ASC");
            $this->setSaveParametersInSession(true);
        }

        protected function _prepareCollection() {
            $collection = Mage::getModel("additionalinfo/additionalinfofields")->getCollection();
            $this->setCollection($collection);
            return parent::_prepareCollection();
        }

        protected function _prepareColumns() {
            $this->addColumn("id", array(
                "header"    => $this->__("ID"),
                "align"     => "center",
                "width"     => "30px",
                "index"     => "id"
            ));

            $this->addColumn("labelname", array(
                "header"    => $this->__("Label"),
                "index"     => "labelname"
            ));

    		$this->addColumn("inputname", array(
                "header"    => $this->__("Name"),
                "index"     => "inputname"
            ));

    		$this->addColumn("inputtype", array(
                "header"    => $this->__("Type"),
                "index"     => "inputtype"
            ));

    		$this->addColumn("setorder", array(
                "header"    => $this->__("Set Order"),
                "index"     => "setorder"
            ));

            $this->addColumn("status", array(
                "header"    => $this->__("Status"),
                "align"     => "left",
                "width"     => "80px",
                "index"     => "status",
                "type"      => "options",
                "options"   => array(1 => "Enabled",2 => "Disabled")
            ));

            $this->addColumn("action", array(
                "header"    => $this->__("Action"),
                "width"     => "80",
                "type"      => "action",
                "getter"    => "getId",
                "actions"   => array(array(
                "caption"   => $this->__("Edit"),
                "url"       => array("base" => "*/*/edit"),
                "field"     => "id")),
                "filter"    => false,
                "sortable"  => false,
                "index"     => "stores",
                "is_system" => true
            ));

            return parent::_prepareColumns();
        }

        protected function _prepareMassaction() {
            $this->setMassactionIdField("id");
            $this->getMassactionBlock()->setFormFieldName("fields");
            $this->getMassactionBlock()->addItem("delete", array(
                "label"     => $this->__("Delete"),
                "url"       => $this->getUrl("*/*/massDelete"),
                "confirm"   => $this->__("Are you sure?")
            ));
            $statuses = Mage::getSingleton("additionalinfo/status")->getOptionArray();
            array_unshift($statuses, array("label" => "", "value" => ""));
            $this->getMassactionBlock()->addItem("status", array(
                "label"     => $this->__("Change status"),
                "url"       => $this->getUrl("*/*/massStatus", array("_current" => true)),
                "additional"=> array("visibility" => array(
                                                        "name"  => "status",
                                                        "type"  => "select",
                                                        "class" => "required-entry",
                                                        "label" => $this->__("Status"),
                                                        "values"=> $statuses))
            ));
            return $this;
        }

        public function getRowUrl($row) {
            return $this->getUrl("*/*/edit", array("id" => $row->getId()));
        }

    }