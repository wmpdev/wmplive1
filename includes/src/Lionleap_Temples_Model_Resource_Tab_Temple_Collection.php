<?php

/**
 * Tab - Temple relation resource model collection
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Tab_Temple_Collection extends Lionleap_Temples_Model_Resource_Temple_Collection
{
    /**
     * remember if fields have been joined
     * @var bool
     */
    protected $_joinedFields = false;

    /**
     * join the link table
     *
     * @access public
     * @return Lionleap_Temples_Model_Resource_Tab_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('lionleap_temples/tab_temple')),
                'related.temple_id = main_table.id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    /**
     * add tab filter
     *
     * @access public
     * @param Lionleap_Temples_Model_Tab | int $tab
     * @return Lionleap_Temples_Model_Resource_Tab_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function addTabFilter($tab)
    {
        if ($tab instanceof Lionleap_Temples_Model_Tab) {
            $tab = $tab->getId();
        }
        if (!$this->_joinedFields) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.tab_id = ?', $tab);
        return $this;
    }
}
