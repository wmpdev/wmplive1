<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Location extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Location
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/location_collection')
            ->addFieldToFilter('city', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $location) {
            $arr[] = array(
                'id'          => 'location/1/'.$location->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Location'),
                'name'        => $location->getCity(),
                'description' => $location->getCity(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_location/edit',
                    array('id'=>$location->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
