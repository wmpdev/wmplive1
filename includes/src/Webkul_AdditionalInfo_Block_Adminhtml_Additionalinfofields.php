<?php
	class Webkul_AdditionalInfo_Block_Adminhtml_Additionalinfofields extends Mage_Adminhtml_Block_Widget_Grid_Container {

	    public function __construct() {
	        $this->_controller = "adminhtml_additionalinfofields";
	        $this->_blockGroup = "additionalinfo";
	        $this->_headerText = $this->__("Custom Registration Fields");
	        $this->_addButtonLabel = $this->__("Add Field");
	        parent::__construct();
	    }

	}