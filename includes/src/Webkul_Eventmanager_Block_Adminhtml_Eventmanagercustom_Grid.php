<?php

class Webkul_Eventmanager_Block_Adminhtml_Eventmanagercustom_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('eventmanagercustomGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('eventmanager/eventmanagercustom')->getCollection()
				->addAttributeToSort('id', 'DESC');
	  foreach ($collection as $index=>$coll) {
			if ($coll->getOccasion() > 0) {
				$categories = Mage::getModel('catalog/category')->getCollection()
				->addAttributeToFilter('entity_id', array('eq' => $coll->getOccasion()))
				->addAttributeToSelect('name');
				foreach ($categories as $category) {
					if ($category->getParentId() > 0) {
						$parent_categories = Mage::getModel('catalog/category')->getCollection()
						->addAttributeToFilter('entity_id', array('eq' => $category->getParentId()))
						->addAttributeToSelect('name');
					}

					$category_name = '';
					if ($parent_categories) {
						foreach ($parent_categories as $parent_category) {
							$category_name = $parent_category->getName() . " - " . $category->getName();
						}
					} else {
						$category_name = $category->getName();
					}
					$coll->setOccasion($category_name);
				}
			} else {
				$coll->setOccasion('Other - ' . $coll->getOtherOccasion());
			}

			$other_details = array();
			if ($coll->getBudget() > 0) {
				$other_details[] = 'Budget : Rs. ' . $coll->getBudget();
			}
			if ($coll->getPandit() == 'yes') {
				$other_details[] = 'Pandit : Yes';
			}
			if ($coll->getSamagri() == 'yes') {
				$other_details[] = 'Samagri : Yes';
			}
			if ($coll->getCatering() == 'yes') {
				$other_details[] = 'Catering : Yes';
			}
			if ($coll->getDecoration() == 'yes') {
				$other_details[] = 'Decoration : Yes';
			}
			if ($coll->getVenue() == 'yes') {
				$other_details[] = 'Venue : Yes';
			}
			if ($coll->getTechnical() == 'yes') {
				$other_details[] = 'Technical : Yes';
			}
			if ($coll->getEventManagement() == 'yes') {
				$other_details[] = 'Event Management : Yes';
			}
			if ($coll->getComments() != '') {
				$other_details[] = 'Comments : ' . nl2br($coll->getComments());
			}
			if (count($other_details) > 0) {
				$coll->setOther(implode(" | ", $other_details));
			}
	  }
	  $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('request', array(
          'header'    => Mage::helper('eventmanager')->__('Request Type'),
          'align'     =>'left',
          'index'     => 'request',
		  'width'	=>	'50'
      ));
		
	  $this->addColumn('created', array(
          'header'    => Mage::helper('eventmanager')->__('Request Made At'),
          'align'     =>'left',
          'index'     => 'created',
		  'width'	=>	'50'
      ));
	  
	  $this->addColumn('name', array(
          'header'    => Mage::helper('eventmanager')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
		  'width'	=>	'100'
      ));

	  $this->addColumn('email', array(
          'header'    => Mage::helper('eventmanager')->__('Email'),
          'align'     =>'left',
          'index'     => 'email',
		  'width'	=>	'100'
      ));

	  $this->addColumn('phone', array(
          'header'    => Mage::helper('eventmanager')->__('Phone Number'),
          'align'     =>'left',
          'index'     => 'phone',
		  'width'	=>	'50'
      ));

	  $this->addColumn('address', array(
          'header'    => Mage::helper('eventmanager')->__('Address'),
          'align'     =>'left',
          'index'     => 'address',
		  'width'	=>	'100'
      ));

	  $this->addColumn('looking_for', array(
          'header'    => Mage::helper('eventmanager')->__('Looking For'),
          'align'     =>'left',
          'index'     => 'looking_for',
		  'width'	=>	'50'
      ));

	  $this->addColumn('occasion', array(
          'header'    => Mage::helper('eventmanager')->__('Occasion'),
          'align'     =>'left',
          'index'     => 'occasion',
		  'width'	=>	'50'
      ));

	  $this->addColumn('occasion_on', array(
          'header'    => Mage::helper('eventmanager')->__('Scheduled On'),
          'align'     =>'left',
          'index'     => 'occasion_on',
		  'width'	=>	'50'
      ));

	  $this->addColumn('other', array(
          'header'    => Mage::helper('eventmanager')->__('Other Details'),
          'align'     =>'left',
          'index'     => 'other',
		  'width'	=>	'200'
      ));

	  $this->addColumn('status', array(
          'header'    => Mage::helper('eventmanager')->__('Status'),
          'align'     => 'left',
          'width'     => '50',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              'new' => 'New',
              'approved' => 'Approved',
			  'rejected' => 'Rejected',
          ),
      ));

		$this->addExportType('*/*/exportCsv', Mage::helper('eventmanager')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('eventmanager')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('eventmanagercustom');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('eventmanager')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('eventmanager')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('eventmanager/status')->getCustomOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('eventmanager')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('eventmanager')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}