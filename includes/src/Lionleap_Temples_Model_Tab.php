<?php

/**
 * Tab model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Tab extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'lionleap_temples_tab';
    const CACHE_TAG = 'lionleap_temples_tab';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'lionleap_temples_tab';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'tab';
    protected $_templeInstance = null;

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/tab');
    }

    /**
     * before save tab
     *
     * @access protected
     * @return Lionleap_Temples_Model_Tab
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the tab Description
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getDescription()
    {
        $description = $this->getData('description');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($description);
        return $html;
    }

    /**
     * save tab relation
     *
     * @access public
     * @return Lionleap_Temples_Model_Tab
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
		$this->getTempleInstance()->saveTabRelation($this);
        return parent::_afterSave();
    }

    /**
     * get temple relation model
     *
     * @access public
     * @return Lionleap_Temples_Model_Tab_Temple
     * @author Ultimate Module Creator
     */
    public function getTempleInstance()
    {
        if (!$this->_templeInstance) {
            $this->_templeInstance = Mage::getSingleton('lionleap_temples/tab_temple');
        }
        return $this->_templeInstance;
    }

    /**
     * get selected  array
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedTemples()
    {
        if (!$this->hasSelectedTemples()) {
            $temples = array();
            foreach ($this->getSelectedTemplesCollection() as $temple) {
                $temples[] = $temple;
            }
            $this->setSelectedTemples($temples);
        }
        return $this->getData('selected_temples');
    }

    /**
     * Retrieve collection selected 
     *
     * @access public
     * @return Lionleap_Temples_Model_Tab_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedTemplesCollection()
    {
        $collection = $this->getTempleInstance()->getTemplesCollection($this);
        return $collection;
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
