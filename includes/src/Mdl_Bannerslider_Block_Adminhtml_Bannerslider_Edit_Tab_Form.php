<?php
class Mdl_Bannerslider_Block_Adminhtml_Bannerslider_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        if (Mage::getSingleton('adminhtml/session')->getBannersliderData()) {
            $data = Mage::getSingleton('adminhtml/session')->getBannersliderData();
            Mage::getSingleton('adminhtml/session')->setBannersliderData(null);
        } elseif (Mage::registry('bannerslider_data'))
            $data = Mage::registry('bannerslider_data')->getData();

        //zend_debug::dump($data);die();
        $fieldset = $form->addFieldset('bannerslider_form', array('legend' => Mage::helper('bannerslider')->__('Slider information')));
        
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $wysiwygConfig->addData(array(
            'add_variables'				=> false,
            'plugins'					=> array(),
            'widget_window_url'			=> Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'directives_url'			=> Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted'		=> preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'files_browser_window_url'	=> Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
        ));        

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('bannerslider')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('show_title', 'select', array(
            'label' => Mage::helper('bannerslider')->__('Show Title'),
            'name' => 'show_title',
            'values' => array(
                array(
                    'value' => 0,
                    'label' => Mage::helper('bannerslider')->__('Enabled'),
                ),
                array(
                    'value' => 1,
                    'label' => Mage::helper('bannerslider')->__('Disabled'),
                ),
            ),
        ));


        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('bannerslider')->__('Status'),
            'name' => 'status',
            'values' => Mage::getSingleton('bannerslider/status')->getOptionHash(),
        ));
		
		$fieldset->addField('style_content', 'hidden', array(
            'label' => Mage::helper('bannerslider')->__('Select available Slider Styles'),
            'name' => 'style_content',           
            'values' => 0,
        ));

       $fieldset->addField('style_slide', 'hidden', array(
            'label' => Mage::helper('bannerslider')->__('Select Slider Mode'),
            'name' => 'style_slide',
        ));
		
		 $fieldset->addField('animation', 'select', array(
            'label' => Mage::helper('bannerslider')->__('Animation Effect'),
            'name' => 'animation',
            'values' => Mage::helper('bannerslider')->getAnimation(),
        ));
		
        $fieldset->addField('slider_speed', 'text', array(
            'label' => Mage::helper('bannerslider')->__('Speed'),
            'name' => 'slider_speed',
			'value' => '120',
            'note' => 'mini seconds. This is the display time of a banner',
        ));
        $fieldset->addField('position', 'select', array(
            'name' => 'position',
            'label' => $this->__('Position'),
            'title' => $this->__('Position'),            
            'values' => Mage::helper('bannerslider')->getBlockIdsToOptionsArray(),
            'onchange'	=> 'onchangeposition()',
        ));
            
        $categoryIds = implode(", ", Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('level', array('gt' => 0))->getAllIds());
      $fieldset->addField('category_ids', 'text', array(
            'label' => Mage::helper('bannerslider')->__('Categories'),
            'name' => 'category_ids',
            'after_element_html' => '<a id="category_link" href="javascript:void(0)" onclick="toggleMainCategories()"><img src="' . $this->getSkinUrl('images/rule_chooser_trigger.gif') . '" alt="" class="v-middle rule-chooser-trigger" title="Select Categories"></a>
                <div  id="categories_check" style="display:none">
                    <a href="javascript:toggleMainCategories(1)">Check All</a> / <a href="javascript:toggleMainCategories(2)">Uncheck All</a>
                </div>
                <div id="main_categories_select" style="display:none"></div>
                    <script type="text/javascript">
                    function toggleMainCategories(check){
                        var cate = $("main_categories_select");
                        if($("main_categories_select").style.display == "none" || (check ==1) || (check == 2)){
                            $("categories_check").style.display ="";
                            var url = "' . $this->getUrl('bannerslider/adminhtml_bannerslider/chooserMainCategories') . '";
                            if(check == 1){
                                $("category_ids").value = "'.$categoryIds.'";
                            }else if(check == 2){
                                $("category_ids").value = "";
                            }
                            var params = $("category_ids").value.split(", ");
                            var parameters = {"form_key": FORM_KEY,"selected[]":params };
                            var request = new Ajax.Request(url,
                                {
                                    evalScripts: true,
                                    parameters: parameters,
                                    onComplete:function(transport){
                                        $("main_categories_select").update(transport.responseText);
                                        $("main_categories_select").style.display = "block"; 
                                    }
                                });
                        if(cate.style.display == "none"){
                            cate.style.display = "";
                        }else{
                            cate.style.display = "none";
                        } 
                    }else{
                        cate.style.display = "none";
                        $("categories_check").style.display ="none";
                    }
                };
		</script>
            '
        )); 
        
        $fieldset->addField('caption', 'hidden', array(
            'label' => Mage::helper('bannerslider')->__('Caption'),          
            'name' => 'caption',
            'after_element_html' => '<script type="text/javascript"> 
                        function onchangeposition(){
                            
                             var checkposition = $(\'position\').value;
                             var tip = null;
                            if (checkposition == "customer-content-top"){
                                                                
                               $(\'position-tip-3\').hide();
                               $(\'position-tip-2\').hide();
                               $(\'position-tip-1\').show();
                                new Tooltip("position-tip-1", "'.Mage::getBaseUrl('media').'bannerslider/bannerslider-ex1.png");
                            }
                            else if (checkposition == "checkout-content-top"){                                                          
                                 $(\'position-tip-3\').hide();
                                 $(\'position-tip-1\').hide();
                                 $(\'position-tip-2\').show();
                                 new Tooltip("position-tip-2", "'.Mage::getBaseUrl('media').'bannerslider/bannerslider-ex2.png");
                            }else if(checkposition == "note-allsite"){                                                              
                                 $(\'position-tip-3\').hide();
                                 $(\'position-tip-1\').hide();
                                 $(\'position-tip-2\').hide();                                 
                                
                                 new Tooltip("position-tip-4", "'.Mage::getBaseUrl('media').'bannerslider/bannerslider-ex4.PNG");
                            }else if(checkposition == "pop-up"){                                                                     
                                 $(\'position-tip-3\').hide();
                                 $(\'position-tip-1\').hide();
                                 $(\'position-tip-2\').hide();                                 
                                
                                 new Tooltip("position-tip-5", "'.Mage::getBaseUrl('media').'bannerslider/bannerslider-ex5.PNG");
                            }
                            else{
                                                               
                                $(\'position-tip-2\').hide();
                                $(\'position-tip-1\').hide();
                                $(\'position-tip-3\').show();
                                new Tooltip("position-tip-3", "'.Mage::getBaseUrl('media').'bannerslider/bannerslider-ex3.png");
                            }
                            
                            var category = checkposition.split("-");
                            var selCata = category[0];
                          
                            $(\'category_ids\').parentNode.parentNode.hide();
                            if(selCata == "category"){
                                $(\'category_ids\').parentNode.parentNode.show();
                            }                                                        
                        }
                        Event.observe(window,\'load\',onchangeposition); 
                        </script>',     
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }

}