<?php
class Mdl_Bannerslider_Model_Status extends Varien_Object
{
	const STATUS_ENABLED	= 0;
	const STATUS_DISABLED	= 1;
	
	
	static public function getOptionArray(){
		return array(
			self::STATUS_ENABLED	=> Mage::helper('bannerslider')->__('Enabled'),
			self::STATUS_DISABLED   => Mage::helper('bannerslider')->__('Disabled')
		);
	}
	
	
	static public function getOptionHash(){
		$options = array();
		foreach (self::getOptionArray() as $value => $label)
			$options[] = array(
				'value'	=> $value,
				'label'	=> $label
			);
		return $options;
	}
}