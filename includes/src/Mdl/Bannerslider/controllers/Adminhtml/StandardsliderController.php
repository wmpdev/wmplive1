<?php
class Mdl_Bannerslider_Adminhtml_StandardsliderController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction(){
		$this->loadLayout()
			->_setActiveMenu('bannerslider/standslider')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		return $this;
	}
 
	
	public function indexAction(){
            
		$this->_initAction()
			->renderLayout();
	}
        
        public function previewAction(){
            $this->loadLayout(false)
                    ->renderLayout();
        }
}