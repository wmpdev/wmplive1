<?php
class Mdl_Bannerslider_Adminhtml_BannerController extends Mage_Adminhtml_Controller_Action {

   
    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('bannerslider/banner')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        return $this;
    }

   
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }
   
    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $store = $this->getRequest()->getParam('store');
        $model = Mage::getModel('bannerslider/banner')->setStoreId($store)->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $model->setData($data);

            Mage::register('banner_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('bannerslider/bannerslider');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('bannerslider/adminhtml_banner_edit'))
                    ->_addLeft($this->getLayout()->createBlock('bannerslider/adminhtml_banner_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bannerslider')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function addinAction() {
        $this->loadLayout();
        $this->_setActiveMenu('bannerslider/bannerslider');

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('bannerslider/adminhtml_addbutton')->setTemplate('bannerslider/addbanner.phtml'));

        $this->renderLayout();
    }

   
    public function saveAction() {    

        if ($data = $this->getRequest()->getPost()) {   

            $store = $this->getRequest()->getParam('store');
            $model = Mage::getModel('bannerslider/banner');
            if (isset($data['image']['delete'])) {
                Mage::helper('bannerslider')->deleteImageFile($data['image']['value']);
            }
            $image = Mage::helper('bannerslider')->uploadBannerImage();
            if ($image || (isset($data['image']['delete']) && $data['image']['delete'])) {
                $data['image'] = $image;
            } else {
                unset($data['image']);
            }
             $times = explode(" ", now());
                if ($data['end_time'] && $data['start_time']) {
                    $data['start_time'] = $data['start_time']. " " . $times[1];
                    $data['end_time'] = $data['end_time'] . " " . $times[1];
                }
			$model->setOrderBanner("7");
            $model->setData($data)
                    ->setStoreId($store)
                    ->setData('banner_id', $this->getRequest()->getParam('id'));            
            try {
               
                
                $model->save();				 
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bannerslider')->__('Slide was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                //Zend_debug::dump($this->getRequest()->getParam('slider'));die();
                if($this->getRequest()->getParam('slider') == 'check'){
                    $this->_redirect('*/*/addin', array('id' => $model->getId()));
                    return;
                }
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'store' => $this->getRequest()->getParam("store")));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bannerslider')->__('Unable to find slide to save'));
        $this->_redirect('*/*/');
    }

   
    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('bannerslider/banner');
                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Slide was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'), 'store' => $this->getRequest()->getParam("store")));
            }
        }
        $this->_redirect('*/*/');
    }

  
    public function massDeleteAction() {
        $bannersliderIds = $this->getRequest()->getParam('banner');
        if (!is_array($bannersliderIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($bannersliderIds as $bannersliderId) {
                    $bannerslider = Mage::getModel('bannerslider/banner')->load($bannersliderId);
                    $bannerslider->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($bannersliderIds)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index', array('store' => $this->getRequest()->getParam("store")));
    }

    public function massStatusAction() {
        $bannerIds = $this->getRequest()->getParam('banner');
        if (!is_array($bannerIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($bannerIds as $bannerId) {
                    $banner = Mage::getSingleton('bannerslider/banner')
                            ->load($bannerId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($bannerIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index', array('store' => $this->getRequest()->getParam("store")));
    }

    public function exportCsvAction() {
        $fileName = 'bannerslider.csv';
        $content = $this->getLayout()->createBlock('bannerslider/adminhtml_banner_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

  
    public function exportXmlAction() {
        $fileName = 'bannerslider.xml';
        $content = $this->getLayout()->createBlock('bannerslider/adminhtml_banner_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('banner');
    }
	
	 public function imageLayerAction() {
	
		if(isset($_FILES['layer_image']['name']) && $_FILES['layer_image']['name'] != '') 
		{
			try {
			/* Starting upload */
			$uploader = new Varien_File_Uploader('layer_image');
			
			$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
			$uploader->setAllowRenameFiles(false);

			$uploader->setFilesDispersion(false);

			
			$path = Mage::getBaseDir('media') . DS .'layer' . DS;
			$uploader->save($path, $_FILES['layer_image']['name'] );
			echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA). 'layer/' .$_FILES['layer_image']['name'];

			} catch (Exception $e) {

			}
			
		}
	 
    }
	
	 public function editCssAction() {
	
	$path = Mage::getBaseDir('skin') . DS .'adminhtml\default\default\css\mdl\bannerslider' ;
	$path_front = Mage::getBaseDir('skin') . DS .'frontend\instock\default\css\mdl\bannerslider' ;
	$id = $this->getRequest()->getParam('id');
	$content = $this->getRequest()->getPost('caption-text-area');
	$file = fopen($path.'\captions.css',"w+");
	fwrite($file,$content);
	$file1 = fopen($path_front.'\captions.css',"w+");
	fwrite($file1,$content);
	$this->_redirect('*/*/edit/id/'.$id);
	 
	 }

}