<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanager_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("eventmanager_form", array("legend" => Mage::helper("eventmanager")->__("Item information")));
        $wysiwygConfig = Mage::getSingleton("cms/wysiwyg_config")->getConfig(array("add_variables" => false, "add_widgets" => false,"files_browser_window_url"=>$this->getBaseUrl()."admin/cms_wysiwyg_images/index/"));
		
		$fieldset->addField("title", "text", array(
			"label"     => Mage::helper("eventmanager")->__("Event  (Max 26 words)"),
			"class"     => "required-entry",
			"required"  => true,
			"style"     => "width:582px;", 'maxlength' => 26,
			"name"      => "title",
			
		));

		$dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		
		$fieldset->addField("edate", "date", array(
			"label"     => $this->__("Event Date"),
			"title"     => $this->__("Event Date"),
			"required"  => true,
			"name"      => "edate",
			"style"     => "width:582px;",
			"image"     => $this->getSkinUrl("images/grid-cal.gif"),
			"input_format" => Varien_Date::DATE_INTERNAL_FORMAT,
			"format"   => Varien_Date::DATE_INTERNAL_FORMAT,
		));

		$fieldset->addField("body", "editor", array(
			"name"      => "body",
			"label"     => $this->__("Content"),
			"title"     => $this->__("Content"),
			"style"     => "width:500px; height:200px;",
			"state"     => "html",
			"config"    => $wysiwygConfig,
			"wysiwyg"   => true,
			"required"  => true,
		));

		$fieldset->addField("link", "text", array(
			"label"     => Mage::helper("eventmanager")->__("Link"),
			"class"     => "required-entry",
			"required"  => true,
			"style"     => "width:600px;",
			"name"      => "link",
		));

		$fieldset->addField("status", "select", array(
			"label" => $this->__("Status"),
			"class" => "required-entry",
			"style"     => "width:600px;",
			"name" => "status",
			"values" => array(
				array(
					"value" => 1,
					"label" => $this->__("Enabled"),
				),
				array(
					"value" => 2,
					"label" => $this->__("Disabled"),
				),
			),
		));

        if (Mage::getSingleton("adminhtml/session")->getEventData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getEventData());
            Mage::getSingleton("adminhtml/session")->setEventData(null);
        } 
        elseif (Mage::registry("eventmanager_data")) {
            $form->setValues(Mage::registry("eventmanager_data")->getData());
        }
        return parent::_prepareForm();
    }

}