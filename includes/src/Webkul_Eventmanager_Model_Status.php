<?php
class Webkul_Eventmanager_Model_Status extends Varien_Object {
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
	const STATUS_NEW    = 'new';
    const STATUS_APPROVED   = 'approved';
    const STATUS_REJECTED   = 'rejected';

    static public function getOptionArray() {
        return array(
            self::STATUS_ENABLED => Mage::helper("eventmanager")->__("Enabled"),
            self::STATUS_DISABLED => Mage::helper("eventmanager")->__("Disabled")
        );
    }

	static public function getCustomOptionArray() {
        return array(
            self::STATUS_NEW    => Mage::helper('eventmanager')->__('New'),
            self::STATUS_APPROVED   => Mage::helper('eventmanager')->__('Approved'),
            self::STATUS_REJECTED   => Mage::helper('eventmanager')->__('Rejected')
        );
    }

}