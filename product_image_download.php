<?php
ini_set('memory_limit','2048M');
set_time_limit(0);

error_reporting(E_ALL);
ini_set('display_errors',1);



//product_export.php

require_once('app/Mage.php'); //Path to Magento
umask(0);
Mage::app();


$path = Mage::getBaseDir('media').DS."csv".DS."productsku.csv";
$io = new Varien_Io_File();

if ($io->fileExists($path)){

	$io->streamOpen($path, 'r');	
	$header_read = $io->streamReadCsv(',');
	
	// open the csv file and write the header in the first line
	$fp = fopen('var/import/product_with_image_url.csv', 'w+') or die('not');
	$csvHeader = array(
	  'sku',
    'base_name',
    'image_url'
	);
	fputcsv( $fp, $csvHeader);


	while (($row = $io->streamReadCsv(',')) !== false)
	{
		
		$row = array_combine($header_read, $row);
		
		
		$sku = $row['sku'];
		
		$_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);



		$_images = Mage::getModel('catalog/product')->load($_product->getId())->getMediaGalleryImages(); 

		 if($_images){          
		    $i=0; 
		    foreach($_images as $_image){ 
				$i++;
				$my_variable = 'image_'.$i; 
				$image_name = $_image->getFile();
		    	$my_variable = Mage::helper('catalog/image')->init($_product, 'thumbnail', $_image->getFile())->resize(108,90);
		    	
		    	$sourcePath = 'https://www.wheresmypandit.com/media/catalog/product'.$image_name;
			    //$basename = basename($image_name);
                        //$k = $i+1;
                            $basename = $sku."($i).jpg";
                            
			    $product_row = array(
		            $sku, 
                $basename,
                $sourcePath
			    );
			    
			    fputcsv( $fp, $product_row);
				
				
				$imageData = file_get_contents($sourcePath) or die ('no content received');
				$img = 'var/import/sku_img_new/'.$basename;
				file_put_contents($img, file_get_contents($sourcePath));
		    } 
		} 
//echo "Ninad";die();
	}
	echo 'all records are done';
}
 ?>