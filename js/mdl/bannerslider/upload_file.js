 jQuery(document).ready(function() {
var bar = jQuery('.bar');
var percent = jQuery('.percent');
var status = jQuery('#status');
var upload_img = jQuery('.uploaded_layer_image');
   
jQuery('#image_layer_form').ajaxForm({
    beforeSend: function() {
        status.empty();
        var percentVal = '0%';
        bar.width(percentVal)
        percent.html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        bar.width(percentVal)
        percent.html(percentVal);
		//console.log(percentVal, position, total);
    },
    success: function(data) {
		
        var percentVal = '100%';
        bar.width(percentVal)
        percent.html(percentVal);
		upload_img.html('<img width="300px" src="'+data+'" alt="layer image"/><a href="javascript:void(0)" class="add_imglayer">Add to layer</a>');
		
    },
	complete: function(xhr) {
		status.html(xhr.responseText);
	}
}); 

});  