/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Varien
 * @package     js
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/**
 * @classDescription simple Navigation with replacing old handlers
 * @param {String} id id of ul element with navigation lists
 * @param {Object} settings object with settings
 */
var mainNav = function() {

    var main = {
        obj_nav :   $(arguments[0]) || $("nav"),

        settings :  {
            show_delay      :   0,
            hide_delay      :   0,
            _ie6            :   /MSIE 6.+Win/.test(navigator.userAgent),
            _ie7            :   /MSIE 7.+Win/.test(navigator.userAgent)
        },

        init :  function(obj, level) {
            obj.lists = obj.childElements();
            obj.lists.each(function(el,ind){
				main.handlNavElement(el);
				if (el.hasClassName("level1")) {
					if (ind > 0 && ind%4 == 0) {
						//el.setStyle({'clear': 'both'});
					}
					if (el.down("ul")) {
						el.down("ul").hide(el.down("ul"));
						el.down("a").addClassName('plus');
					}
				}

				/*if (el.down("div")) {
					el.down("div").childElements().each(function(el1, ind1) {
						if (el1.tagName.toLowerCase() == 'ul' && el1.hasClassName("level0")) {
							el1.setStyle({'left': (15 * (ind1)) + '%'});
						}
					});
				}*/

				/*if (el.hasClassName("shown-sub")) {
					el.setStyle({'position': 'absolute', 'left': '100px', 'top': '100px', 'width': '250px'});
				}*/

                if((main.settings._ie6 || main.settings._ie7) && level){
                    main.ieFixZIndex(el, ind, obj.lists.size());
                }
            });
            if(main.settings._ie6 && !level){
                document.execCommand("BackgroundImageCache", false, true);
            }
        },

        handlNavElement :   function(list) {
            if(list !== undefined){
                list.onmouseover = function(){
					if (!list.hasClassName("level1")) {
						main.fireNavEvent(this,true,false);
					}
                };
                list.onmouseout = function(){
                    if (!list.hasClassName("level1")) {
						main.fireNavEvent(this,false,false);
					}
                };
				if (list.down("a")) {
					list.down("a").onclick = function(event){
						var ele = Event.element(event);
						if (this.up(0).hasClassName('parent')) {
							Event.stop(event);
						}
						if (ele.up(0).hasClassName("plus") && !ele.up(0).hasClassName("level-top")) {
							$("nav").childElements().each(function(el,ind){
								el.childElements().each(function(el1, ind1) {
									el1.childElements().each(function(el2, ind2) {
										el2.childElements().each(function(el3, ind3) {
											if (el3.hasClassName("level1")) {
												main.fireNavEvent(el3,false,true);
											}
										});
									});
								});
							});
							this.up(0).down("a").removeClassName("plus").addClassName("minus");
							this.up(0).down("ul").show(this.up(0).down("ul"));
						} else if (ele.up(0).hasClassName("minus")) {
							this.up(0).down("a").removeClassName("minus").addClassName("plus");
							this.up(0).down("ul").hide(this.up(0).down("ul"));
						}
					};
				}
                if(list.down("ul")){
					main.init(list.down("ul"), true);
					list.down("ul").siblings().each(function(el1, ind1) {
						main.init(el1, true);
					});
                } else if (list.hasClassName("level1")) {
					if (list.childElements()[1]) {
						list.childElements()[1].hide(list.childElements()[1]);
					}
				}
            }
        },

        ieFixZIndex : function(el, i, l) {
            if(el.tagName.toString().toLowerCase().indexOf("iframe") == -1){
                el.style.zIndex = l - i;
            } else {
                el.onmouseover = "null";
                el.onmouseout = "null";
            }
        },

        fireNavEvent :  function(elm,ev,event) {
			if(ev){
                elm.addClassName("over");
                if (elm.down("a") && !event) {
					elm.down("a").addClassName("over");
				}
                if (elm.down("div") && !event) {
					main.show(elm.down("div"));
					elm.down("div").childElements().each(function(el,ind){
						//if (el.hasClassName("level0")) {
							main.show(el);
						//}
					});
					if (elm.up(0).hasClassName('level1')) {
						elm.down("a").removeClassName("plus").addClassName("minus");
					};
                } else if (event) {
					elm.childElements().each(function(el,ind){
						if (el.hasClassName("level0")) {
							main.show(el);
						}
					});
					elm.childElements().each(function(el,ind){
						el.removeClassName("plus").addClassName("minus");
					});
                }
            } else {
                elm.removeClassName("over");
                if (elm.down("a") && !event) {
					elm.down("a").removeClassName("over");
				}
				if (elm.down("div") && !event) {
					main.hide(elm.down("div"));
					elm.down("div").childElements().each(function(el,ind){
						if (el.tagName.toLowerCase() == 'ul' && (el.hasClassName("level0") || el.hasClassName("level1"))) {
							main.hide(el);
						}
					});
					elm.childElements().each(function(el,ind){
						el.removeClassName("minus").addClassName("plus");
					});
				} else if (event) {
					elm.childElements().each(function(el,ind){
						if (el.tagName.toLowerCase() == 'ul' && (el.hasClassName("level0") || el.hasClassName("level1"))) {
							el.hide(el);
						}
					});
					elm.childElements().each(function(el,ind){
						el.removeClassName("minus").addClassName("plus");
					});
				}
            }
        },

        show : function (sub_elm) {
            if (sub_elm.hide_time_id) {
                clearTimeout(sub_elm.hide_time_id);
            }
            sub_elm.show_time_id = setTimeout(function() {
                if (!sub_elm.hasClassName("shown-sub")) {
					sub_elm.addClassName("shown-sub");
					sub_elm.show(sub_elm);
                }
            }, main.settings.show_delay);
        },

        hide : function (sub_elm) {
            if (sub_elm.show_time_id) {
                clearTimeout(sub_elm.show_time_id);
            }
            sub_elm.hide_time_id = setTimeout(function(){
                if (sub_elm.hasClassName("shown-sub")) {
                    sub_elm.removeClassName("shown-sub");
					sub_elm.hide(sub_elm);
                }
            }, main.settings.hide_delay);
        }

    };
    if (arguments[1]) {
        main.settings = Object.extend(main.settings, arguments[1]);
    }
    if (main.obj_nav) {
        main.init(main.obj_nav, false);
    }
};

document.observe("dom:loaded", function() {
    //run navigation without delays and with default id="#nav"
    //mainNav();

    //run navigation with delays
    //mainNav("nav", {"show_delay":"700","hide_delay":"500"});
	mainNav("nav", {"fadeIn":"600"});
});
