/**
*	@name							accordionNew
*	@descripton						This Jquery plugin makes creating accordionNews pain free
*	@version						1.4
*	@requires						Jquery 1.2.6+
*
*	@author							Jan Jarfalk
*	@author-email					jan.jarfalk@unwrongest.com
*	@author-website					http://www.unwrongest.com
*
*	@licens							MIT License - http://www.opensource.org/licenses/mit-license.php
*/

(function(jQuery){
     jQuery.fn.extend({
         accordionNew: function() {       
            return this.each(function() {
            	
            	var jQueryul						= jQuery(this),
					elementDataKey			= 'accordiated',
					activeClassName			= 'active',
					activationEffect 		= 'slideToggle',
					panelSelector			= 'ul, div',
					activationEffectSpeed 	= 'fast',
					itemSelector			= 'li';
            	
				if(jQueryul.data(elementDataKey))
					return false;
													
				jQuery.each(jQueryul.find('ul, li>div'), function(){
					jQuery(this).data(elementDataKey, true);
					jQuery(this).hide();
				});
				
				jQuery.each(jQueryul.find('em.open-close'), function(){
					jQuery(this).click(function(e){
						activate(this, activationEffect);
						return void(0);
					});
					
					jQuery(this).bind('activate-node', function(){
						jQueryul.find( panelSelector ).not(jQuery(this).parents()).not(jQuery(this).siblings()).slideUp( activationEffectSpeed );
						activate(this,'slideDown');
					});
				});

				jQuery.each(jQueryul.find('li.level0 > a'), function(){
					if (jQuery(this).parent().has('em').length) {
						jQuery(this).click(function(e){
							e.preventDefault();
							activate(this, activationEffect);
							return void(0);
						});
					}
				});

				jQuery.each(jQueryul.find('ul.level0 > li.level1 > a'), function(){
					if (jQuery(this).parent().has('em').length) {
						jQuery(this).click(function(e){
							e.preventDefault();
							activate(this, activationEffect);
							return void(0);
						});
					}
				});
				
				var active = (location.hash)?jQueryul.find('a[href=' + location.hash + ']')[0]:jQueryul.find('li.current a')[0];

				if(active){
					activate(active, false);
				}
				
				function activate(el,effect){
					/*jQuery(el).parent( itemSelector ).each(function(){
						jQuery(this).children('div.menu-white').eq(0).css('width', '100% !important').show();
						jQuery(this).children('div.menu-white').eq(0).children().each(function() {
							jQuery(this).css('width', '100% !important').show();
						});
					});
					return;*/
					jQuery(el).parent( itemSelector ).siblings().removeClass(activeClassName).children('div.menu-white').eq(0).children( panelSelector ).slideUp( activationEffectSpeed );
					jQuery(el).siblings( panelSelector )[(effect || activationEffect)](((effect == "show")?activationEffectSpeed:false),function(){
						if(jQuery(el).siblings( panelSelector ).is(':visible')){
							jQuery(el).parents( itemSelector ).not(jQueryul.parents()).addClass(activeClassName);
							jQuery(el).siblings( 'div' ).show();
							jQuery(el).siblings( 'div' ).each(function() {
								//jQuery(this).show();
								jQuery(this).width('100%');
								jQuery(this).children().show();
								jQuery(this).children().width('100%');
							});
						} else {
							jQuery(el).parent( itemSelector ).removeClass(activeClassName);
							jQuery(el).siblings( 'div' ).hide();
							jQuery(el).siblings( 'div' ).each(function() {
								//jQuery(this).hide();
								jQuery(this).children().hide();
							});
						}
						
						if(effect == 'show'){
							jQuery(el).parents( itemSelector ).not(jQueryul.parents()).addClass(activeClassName);
						}
					
						jQuery(el).parents().show();
					
					});
					
				}
				
            });
        }
    }); 
})(jQuery);
jQuery(document).ready(function () {
	
	jQuery("ul.accordion li.parent").each(function(){
        jQuery(this).append('<em class="open-close">&nbsp;</em>');
      });
	
	jQuery('ul.accordion').accordionNew();
	
	jQuery("ul.accordion li.active").each(function(){
		jQuery(this).children().next("ul").css('display', 'block');
	});
});