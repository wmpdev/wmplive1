<?php

/**
 * Upload admin controller
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Adminhtml_Temples_UploadController extends Lionleap_Temples_Controller_Adminhtml_Temples
{
    /**
     * init the upload
     *
     * @access protected
     * @return Lionleap_Temples_Model_Upload
     */
    protected function _initUpload()
    {
        $uploadId  = (int) $this->getRequest()->getParam('id');
        $upload    = Mage::getModel('lionleap_temples/upload');
        if ($uploadId) {
            $upload->load($uploadId);
        }
        Mage::register('current_upload', $upload);
        return $upload;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Uploads'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit upload - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $uploadId    = $this->getRequest()->getParam('id');
        $upload      = $this->_initUpload();
        if ($uploadId && !$upload->getId()) {
            $this->_getSession()->addError(
                Mage::helper('lionleap_temples')->__('This upload no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getUploadData(true);
        if (!empty($data)) {
            $upload->setData($data);
        }
        Mage::register('upload_data', $upload);
        $this->loadLayout();
        $this->_title(Mage::helper('lionleap_temples')->__('Temples'))
             ->_title(Mage::helper('lionleap_temples')->__('Uploads'));
        if ($upload->getId()) {
            $this->_title($upload->getName());
        } else {
            $this->_title(Mage::helper('lionleap_temples')->__('Add upload'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new upload action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save upload - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('upload')) {
            try {
                $upload = $this->_initUpload();
                $upload->addData($data);
                $imageName = $this->_uploadAndGetName(
                    'name',
                    Mage::helper('lionleap_temples/upload_image')->getImageBaseDir(),
                    $data
                );
                $upload->setData('name', $imageName);
                $upload->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Upload was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $upload->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['name']['value'])) {
                    $data['name'] = $data['name']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setUploadData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['name']['value'])) {
                    $data['name'] = $data['name']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was a problem saving the upload.')
                );
                Mage::getSingleton('adminhtml/session')->setUploadData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Unable to find upload to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete upload - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $upload = Mage::getModel('lionleap_temples/upload');
                $upload->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Upload was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting upload.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('lionleap_temples')->__('Could not find upload to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete upload - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $uploadIds = $this->getRequest()->getParam('upload');
        if (!is_array($uploadIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select uploads to delete.')
            );
        } else {
            try {
                foreach ($uploadIds as $uploadId) {
                    $upload = Mage::getModel('lionleap_temples/upload');
                    $upload->setId($uploadId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('lionleap_temples')->__('Total of %d uploads were successfully deleted.', count($uploadIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error deleting uploads.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $uploadIds = $this->getRequest()->getParam('upload');
        if (!is_array($uploadIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select uploads.')
            );
        } else {
            try {
                foreach ($uploadIds as $uploadId) {
                $upload = Mage::getSingleton('lionleap_temples/upload')->load($uploadId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d uploads were successfully updated.', count($uploadIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating uploads.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Type Of Upload change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massTypeAction()
    {
        $uploadIds = $this->getRequest()->getParam('upload');
        if (!is_array($uploadIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select uploads.')
            );
        } else {
            try {
                foreach ($uploadIds as $uploadId) {
                $upload = Mage::getSingleton('lionleap_temples/upload')->load($uploadId)
                    ->setType($this->getRequest()->getParam('flag_type'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d uploads were successfully updated.', count($uploadIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating uploads.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass Is Primary Image change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIsPrimaryAction()
    {
        $uploadIds = $this->getRequest()->getParam('upload');
        if (!is_array($uploadIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select uploads.')
            );
        } else {
            try {
                foreach ($uploadIds as $uploadId) {
                $upload = Mage::getSingleton('lionleap_temples/upload')->load($uploadId)
                    ->setIsPrimary($this->getRequest()->getParam('flag_is_primary'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d uploads were successfully updated.', count($uploadIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating uploads.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass temple change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massTempleIdAction()
    {
        $uploadIds = $this->getRequest()->getParam('upload');
        if (!is_array($uploadIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('lionleap_temples')->__('Please select uploads.')
            );
        } else {
            try {
                foreach ($uploadIds as $uploadId) {
                $upload = Mage::getSingleton('lionleap_temples/upload')->load($uploadId)
                    ->setTempleId($this->getRequest()->getParam('flag_temple_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d uploads were successfully updated.', count($uploadIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('lionleap_temples')->__('There was an error updating uploads.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'upload.csv';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_upload_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'upload.xls';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_upload_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'upload.xml';
        $content    = $this->getLayout()->createBlock('lionleap_temples/adminhtml_upload_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('lionleap_temples/upload');
    }
}
