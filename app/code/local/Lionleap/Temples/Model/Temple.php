<?php

/**
 * Temple model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Temple extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'lionleap_temples_temple';
    const CACHE_TAG = 'lionleap_temples_temple';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'lionleap_temples_temple';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'temple';
    protected $_tabInstance = null;

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/temple');
    }

    /**
     * before save temple
     *
     * @access protected
     * @return Lionleap_Temples_Model_Temple
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the temple Description
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getDescription()
    {
        $description = $this->getData('description');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($description);
        return $html;
    }

	/**
     * get tab relation model
     *
     * @access public
     * @return Lionleap_Temples_Model_Temple_Tab
     * @author Ultimate Module Creator
     */
    public function getTabInstance()
    {
        if (!$this->_tabInstance) {
            $this->_tabInstance = Mage::getSingleton('lionleap_temples/temple_tab');
        }
        return $this->_tabInstance;
    }

    /**
     * save temple relation
     *
     * @access public
     * @return Lionleap_Temples_Model_Temple
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        $this->getTabInstance()->saveTempleRelation($this);
        return parent::_afterSave();
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return Lionleap_Temples_Model_Upload_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedUploadsCollection()
    {
        if (!$this->hasData('_upload_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('lionleap_temples/upload_collection')
                        ->addFieldToFilter('temple_id', $this->getId());
                $this->setData('_upload_collection', $collection);
            }
        }
        return $this->getData('_upload_collection');
    }

	/**
     * get selected  array
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedTabs()
    {
        if (!$this->hasSelectedTabs()) {
            $tabs = array();
            foreach ($this->getSelectedTabsCollection() as $tab) {
                $tabs[] = $tab;
            }
            $this->setSelectedTabs($tabs);
        }
		return $this->getData('selected_tabs');
	}

    /**
     * Retrieve  collection
     *
     * @access public
     * @return Lionleap_Temples_Model_Tab_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedTabsCollection()
    {
        if (!$this->hasData('_tab_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('lionleap_temples/tab_collection')
                        ->addFieldToFilter('temple_id', $this->getId());
                $this->setData('_tab_collection', $collection);
            }
        }
        return $this->getData('_tab_collection');
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return Lionleap_Temples_Model_Link_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedLinksCollection()
    {
        if (!$this->hasData('_link_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('lionleap_temples/link_collection')
                        ->addFieldToFilter('temple_id', $this->getId());
                $this->setData('_link_collection', $collection);
            }
        }
        return $this->getData('_link_collection');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Location
     * @author Ultimate Module Creator
     */
    public function getParentLocation()
    {
        if (!$this->hasData('_parent_location')) {
            if (!$this->getLocationId()) {
                return null;
            } else {
                $location = Mage::getModel('lionleap_temples/location')
                    ->load($this->getLocationId());
                if ($location->getId()) {
                    $this->setData('_parent_location', $location);
                } else {
                    $this->setData('_parent_location', null);
                }
            }
        }
        return $this->getData('_parent_location');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Deity
     * @author Ultimate Module Creator
     */
    public function getParentDeity()
    {
        if (!$this->hasData('_parent_deity')) {
            if (!$this->getDeityId()) {
                return null;
            } else {
                $deity = Mage::getModel('lionleap_temples/deity')
                    ->load($this->getDeityId());
                if ($deity->getId()) {
                    $this->setData('_parent_deity', $deity);
                } else {
                    $this->setData('_parent_deity', null);
                }
            }
        }
        return $this->getData('_parent_deity');
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Lionleap_Temples_Model_Religion
     * @author Ultimate Module Creator
     */
    public function getParentReligion()
    {
        if (!$this->hasData('_parent_religion')) {
            if (!$this->getReligionId()) {
                return null;
            } else {
                $religion = Mage::getModel('lionleap_temples/religion')
                    ->load($this->getReligionId());
                if ($religion->getId()) {
                    $this->setData('_parent_religion', $religion);
                } else {
                    $this->setData('_parent_religion', null);
                }
            }
        }
        return $this->getData('_parent_religion');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
		$values['months'] = '1';
        return $values;
    }
    
	/**
      * get Available Months
      *
      * @access public
      * @return array
      * @author Ultimate Module Creator
      */
    public function getMonths()
    {
        if (!$this->getData('months')) {
            return explode(',', $this->getData('months'));
        }
        return $this->getData('months');
    }
}
