<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Religion extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Religion
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/religion_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $religion) {
            $arr[] = array(
                'id'          => 'religion/1/'.$religion->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Religion'),
                'name'        => $religion->getName(),
                'description' => $religion->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_religion/edit',
                    array('id'=>$religion->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
