<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Upload extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Upload
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/upload_collection')
            ->addFieldToFilter('name', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $upload) {
            $arr[] = array(
                'id'          => 'upload/1/'.$upload->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Upload'),
                'name'        => $upload->getName(),
                'description' => $upload->getName(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_upload/edit',
                    array('id'=>$upload->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
