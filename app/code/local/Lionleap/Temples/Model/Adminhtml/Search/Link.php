<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Link extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Link
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/link_collection')
            ->addFieldToFilter('link', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $link) {
            $arr[] = array(
                'id'          => 'link/1/'.$link->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Link'),
                'name'        => $link->getLink(),
                'description' => $link->getLink(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_link/edit',
                    array('id'=>$link->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
