<?php

/**
 * Admin search model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Adminhtml_Search_Search extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Lionleap_Temples_Model_Adminhtml_Search_Search
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('lionleap_temples/search_collection')
            ->addFieldToFilter('short_temple_search', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $search) {
            $arr[] = array(
                'id'          => 'search/1/'.$search->getId(),
                'type'        => Mage::helper('lionleap_temples')->__('Search'),
                'name'        => $search->getShortTempleSearch(),
                'description' => $search->getShortTempleSearch(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/temples_search/edit',
                    array('id'=>$search->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
