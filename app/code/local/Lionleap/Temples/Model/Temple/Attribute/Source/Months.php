<?php

/**
 * Admin source model for Available Months
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Temple_Attribute_Source_Months extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    /**
     * get possible values
     *
     * @access public
     * @param bool $withEmpty
     * @param bool $defaultValues
     * @return array
     * @author Ultimate Module Creator
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        $options =  array(
            array(
                'label' => Mage::helper('lionleap_temples')->__('All'),
                'value' => 'All'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('January'),
                'value' => 'January'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('February'),
                'value' => 'February'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('March'),
                'value' => 'March'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('April'),
                'value' => 'April'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('May'),
                'value' => 'May'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('June'),
                'value' => 'June'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('July'),
                'value' => 'July'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('August'),
                'value' => 'August'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('September'),
                'value' => 'September'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('October'),
                'value' => 'October'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('November'),
                'value' => 'November'
            ),
            array(
                'label' => Mage::helper('lionleap_temples')->__('December'),
                'value' => 'December'
            ),
        );
        if ($withEmpty) {
            array_unshift($options, array('label'=>'', 'value'=>''));
        }
        return $options;

    }

    /**
     * get options as array
     *
     * @access public
     * @param bool $withEmpty
     * @return string
     * @author Ultimate Module Creator
     */
    public function getOptionsArray($withEmpty = true)
    {
        $options = array();
        foreach ($this->getAllOptions($withEmpty) as $option) {
            $options[$option['value']] = $option['label'];
        }
        return $options;
    }

    /**
     * get option text
     *
     * @access public
     * @param mixed $value
     * @return string
     * @author Ultimate Module Creator
     */
    public function getOptionText($value)
    {
        $options = $this->getOptionsArray();
        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        $texts = array();
        foreach ($value as $v) {
            if (isset($options[$v])) {
                $texts[] = $options[$v];
            }
        }
        return implode(', ', $texts);
    }
}
