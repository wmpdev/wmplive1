<?php

/**
 * Temple collection resource model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Temple_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_joinedFields = array();

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/temple');
    }

    /**
     * get temples as array
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionArray($valueField='id', $labelField='name', $additional=array())
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }

    /**
     * get options hash
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionHash($valueField='id', $labelField='name')
    {
        return parent::_toOptionHash($valueField, $labelField);
    }

    /**
     * add the tab filter to collection
     *
     * @access public
     * @param mixed (Lionleap_Temples_Model_Tab|int) $tab
     * @return Lionleap_Temples_Model_Resource_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function addTabFilter($tab)
    {
        if ($tab instanceof Lionleap_Temples_Model_Tab) {
            $tab = $tab->getId();
        }
        if (!isset($this->_joinedFields['tab'])) {
            $this->getSelect()->join(
                array('related_tab' => $this->getTable('lionleap_temples/temple_tab')),
                'related_tab.temple_id = main_table.id',
                array('position')
            );
            $this->getSelect()->where('related_tab.tab_id = ?', $tab);
            $this->_joinedFields['tab'] = true;
        }
        return $this;
    }
	
	/**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @access public
     * @return Varien_Db_Select
     * @author Ultimate Module Creator
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);
        return $countSelect;
    }
}
