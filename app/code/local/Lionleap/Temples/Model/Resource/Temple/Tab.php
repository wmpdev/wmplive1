<?php 

/**
 * Temple - Tab relation model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Temple_Tab extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * initialize resource model
     *
     * @access protected
     * @return void
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @author Ultimate Module Creator
     */
    protected function  _construct()
    {
        $this->_init('lionleap_temples/temple_tab', 'id');
    }

    /**
     * Save temple - tab relations
     *
     * @access public
     * @param Lionleap_Temples_Model_Temple $temple
     * @param array $data
     * @return Lionleap_Temples_Model_Resource_Temple_Tab
     * @author Ultimate Module Creator
     */
    public function saveTempleRelation($temple, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }

        $adapter = $this->_getWriteAdapter();
        $bind    = array(
            ':temple_id'    => (int)$temple->getId(),
        );
        $select = $adapter->select()
            ->from($this->getMainTable(), array('id', 'tab_id'))
            ->where('temple_id = :temple_id');

        $related   = $adapter->fetchPairs($select, $bind);
        $deleteIds = array();
        foreach ($related as $relId => $tabId) {
            if (!isset($data[$tabId])) {
                $deleteIds[] = (int)$relId;
            }
        }
        if (!empty($deleteIds)) {
            $adapter->delete(
                $this->getMainTable(),
                array('id IN (?)' => $deleteIds)
            );
        }

        foreach ($data as $tabId => $info) {
            $adapter->insertOnDuplicate(
                $this->getMainTable(),
                array(
                    'temple_id'      => $temple->getId(),
                    'tab_id'     => $tabId,
                    'position'      => @$info['position']
                ),
                array('position')
            );
        }
        return $this;
    }
}
