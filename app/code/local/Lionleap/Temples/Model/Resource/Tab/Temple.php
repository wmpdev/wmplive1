<?php 

/**
 * Tab - Temple relation model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Tab_Temple extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * initialize resource model
     *
     * @access protected
     * @return void
     * @see Mage_Core_Model_Resource_Abstract::_construct()
     * @author Ultimate Module Creator
     */
    protected function  _construct()
    {
        $this->_init('lionleap_temples/tab_temple', 'id');
    }

    /**
     * Save tab - temple relations
     *
     * @access public
     * @param Lionleap_Temples_Model_Tab $tab
     * @param array $data
     * @return Lionleap_Temples_Model_Resource_Tab_Temple
     * @author Ultimate Module Creator
     */
    public function saveTabRelation($tab, $data)
    {
        if (!is_array($data)) {
            $data = array();
        }

        $adapter = $this->_getWriteAdapter();
        $bind    = array(
            ':tab_id'    => (int)$tab->getId(),
        );
        $select = $adapter->select()
            ->from($this->getMainTable(), array('id', 'temple_id'))
            ->where('tab_id = :tab_id');

        $related   = $adapter->fetchPairs($select, $bind);
        $deleteIds = array();
        foreach ($related as $relId => $templeId) {
            if (!isset($data[$templeId])) {
                $deleteIds[] = (int)$relId;
            }
        }
        if (!empty($deleteIds)) {
            $adapter->delete(
                $this->getMainTable(),
                array('id IN (?)' => $deleteIds)
            );
        }

        foreach ($data as $templeId => $info) {
            $adapter->insertOnDuplicate(
                $this->getMainTable(),
                array(
                    'tab_id'      => $tab->getId(),
                    'temple_id'     => $templeId,
                    'position'      => @$info['position']
                ),
                array('position')
            );
        }
        return $this;
    }
}
