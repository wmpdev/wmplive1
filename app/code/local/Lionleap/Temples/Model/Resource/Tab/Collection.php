<?php

/**
 * Tab collection resource model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Tab_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_joinedFields = array();

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/tab');
    }

    /**
     * get tabs as array
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionArray($valueField='id', $labelField='title', $additional=array())
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }

    /**
     * get options hash
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _toOptionHash($valueField='id', $labelField='title')
    {
        return parent::_toOptionHash($valueField, $labelField);
    }

     /**
     * add the temple filter to collection
     *
     * @access public
     * @param mixed (Lionleap_Temples_Model_Temple|int) $temple
     * @return Lionleap_Temples_Model_Resource_Tab_Collection
     * @author Ultimate Module Creator
     */
    public function addTempleFilter($temple)
    {
        if ($temple instanceof Lionleap_Temples_Model_Temple) {
            $temple = $temple->getId();
        }
        if (!isset($this->_joinedFields['temple'])) {
            $this->getSelect()->join(
                array('related_temple' => $this->getTable('lionleap_temples/tab_temple')),
                'related_temple.tab_id = main_table.id',
                array('position')
            );
            $this->getSelect()->where('related_temple.temple_id = ?', $temple);
            $this->_joinedFields['temple'] = true;
        }
        return $this;
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @access public
     * @return Varien_Db_Select
     * @author Ultimate Module Creator
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);
        return $countSelect;
    }
}