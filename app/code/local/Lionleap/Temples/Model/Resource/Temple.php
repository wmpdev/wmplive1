<?php

/**
 * Temple resource model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Resource_Temple extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        $this->_init('lionleap_temples/temple', 'id');
    }

	/**
     * process multiple select fields
     *
     * @access protected
     * @param Mage_Core_Model_Abstract $object
     * @return Lionleap_Temples_Model_Resource_Temple
     * @author Ultimate Module Creator
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $months = $object->getMonths();
        if (is_array($months)) {
            $object->setMonths(implode(',', $months));
        }
        return parent::_beforeSave($object);
    }
}
