<?php 

/**
 * Tab temple model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Tab_Temple extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->_init('lionleap_temples/tab_temple');
    }

    /**
     * Save data for tab - temple relation
     * @access public
     * @param  Lionleap_Temples_Model_Tab $tab
     * @return Lionleap_Temples_Model_Tab_Temple
     * @author Ultimate Module Creator
     */
    public function saveTabRelation($tab)
    {
        $data = $tab->getTemplesData();
        if (!is_null($data)) {
            $this->_getResource()->saveTabRelation($tab, $data);
        }
        return $this;
    }

    /**
     * get  for tab
     *
     * @access public
     * @param Lionleap_Temples_Model_Tab $tab
     * @return Lionleap_Temples_Model_Resource_Tab_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function getTemplesCollection($tab)
    {
        $collection = Mage::getResourceModel('lionleap_temples/tab_temple_collection')
            ->addTabFilter($tab);
        return $collection;
    }
}
