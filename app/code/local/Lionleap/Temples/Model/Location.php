<?php

/**
 * Location model
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Model_Location extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'lionleap_temples_location';
    const CACHE_TAG = 'lionleap_temples_location';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'lionleap_temples_location';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'location';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('lionleap_temples/location');
    }

    /**
     * before save location
     *
     * @access protected
     * @return Lionleap_Temples_Model_Location
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save location relation
     *
     * @access public
     * @return Lionleap_Temples_Model_Location
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * Retrieve  collection
     *
     * @access public
     * @return Lionleap_Temples_Model_Temple_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedTemplesCollection()
    {
        if (!$this->hasData('_temple_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('lionleap_temples/temple_collection')
                        ->addFieldToFilter('location_id', $this->getId());
                $this->setData('_temple_collection', $collection);
            }
        }
        return $this->getData('_temple_collection');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }
    
}
