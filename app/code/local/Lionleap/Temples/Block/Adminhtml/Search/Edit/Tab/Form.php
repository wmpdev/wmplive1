<?php

/**
 * Search edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Search_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Search_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('search_');
        $form->setFieldNameSuffix('search');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'search_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Search'))
        );
        $values = Mage::getResourceModel('lionleap_temples/deity_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="search_deity_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeDeityIdLink() {
                if ($(\'search_deity_id\').value == \'\') {
                    $(\'search_deity_id_link\').hide();
                } else {
                    $(\'search_deity_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_deity/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'search_deity_id\').value);
                    $(\'search_deity_id_link\').href = realUrl;
                    $(\'search_deity_id_link\').innerHTML = text.replace(\'{#name}\', $(\'search_deity_id\').options[$(\'search_deity_id\').selectedIndex].innerHTML);
                }
            }
            $(\'search_deity_id\').observe(\'change\', changeDeityIdLink);
            changeDeityIdLink();
            </script>';

        $fieldset->addField(
            'deity_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Deity'),
                'name'      => 'deity_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/religion_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="search_religion_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeReligionIdLink() {
                if ($(\'search_religion_id\').value == \'\') {
                    $(\'search_religion_id_link\').hide();
                } else {
                    $(\'search_religion_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_religion/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'search_religion_id\').value);
                    $(\'search_religion_id_link\').href = realUrl;
                    $(\'search_religion_id_link\').innerHTML = text.replace(\'{#name}\', $(\'search_religion_id\').options[$(\'search_religion_id\').selectedIndex].innerHTML);
                }
            }
            $(\'search_religion_id\').observe(\'change\', changeReligionIdLink);
            changeReligionIdLink();
            </script>';

        $fieldset->addField(
            'religion_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Religion'),
                'name'      => 'religion_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'short_temple_search',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Short Temple Search'),
                'name'  => 'short_temple_search',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'long_temple_search',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Long Temple Search'),
                'name'  => 'long_temple_search',

           )
        );

        $fieldset->addField(
            'short_location_search',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Short Location Search'),
                'name'  => 'short_location_search',

           )
        );

        $fieldset->addField(
            'long_location_search',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Long Location Search'),
                'name'  => 'long_location_search',

           )
        );

        $fieldset->addField(
            'ip',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('IP Address'),
                'name'  => 'ip',

           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_search')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getSearchData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getSearchData());
            Mage::getSingleton('adminhtml/session')->setSearchData(null);
        } elseif (Mage::registry('current_search')) {
            $formValues = array_merge($formValues, Mage::registry('current_search')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
