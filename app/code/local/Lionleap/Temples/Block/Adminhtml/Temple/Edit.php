<?php

/**
 * Temple admin edit form
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Temple_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'lionleap_temples';
        $this->_controller = 'adminhtml_temple';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('lionleap_temples')->__('Save Temple')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('lionleap_temples')->__('Delete Temple')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_temple') && Mage::registry('current_temple')->getId()) {
            return Mage::helper('lionleap_temples')->__(
                "Edit Temple '%s'",
                $this->escapeHtml(Mage::registry('current_temple')->getName())
            );
        } else {
            return Mage::helper('lionleap_temples')->__('Add Temple');
        }
    }
}
