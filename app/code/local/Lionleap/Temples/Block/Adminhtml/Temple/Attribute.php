<?php
/**
 * Lionleap_Temples extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Lionleap
 * @package        Lionleap_Temples
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Temple admin attribute block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Temple_Attribute extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_temple_attribute';
        $this->_blockGroup = 'lionleap_temples';
        $this->_headerText = Mage::helper('lionleap_temples')->__('Manage Temple Attributes');
        parent::__construct();
        $this->_updateButton(
            'add',
            'label',
            Mage::helper('lionleap_temples')->__('Add New Temple Attribute')
        );
    }
}