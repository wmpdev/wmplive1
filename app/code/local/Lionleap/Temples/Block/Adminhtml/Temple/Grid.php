<?php

/**
 * Temple admin grid block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Temple_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('templeGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('lionleap_temples/temple')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Id'),
                'index'  => 'id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'location_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Location'),
                'index'     => 'location_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/location_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getLocationId'
                ),
                'base_link' => 'adminhtml/temples_location/edit'
            )
        );
        $this->addColumn(
            'deity_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Deity'),
                'index'     => 'deity_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/deity_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getDeityId'
                ),
                'base_link' => 'adminhtml/temples_deity/edit'
            )
        );
        $this->addColumn(
            'religion_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Religion'),
                'index'     => 'religion_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/religion_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getReligionId'
                ),
                'base_link' => 'adminhtml/temples_religion/edit'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Name'),
                'align'     => 'left',
                'index'     => 'name',
            )
        );
        $this->addColumn(
            'urlkey',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('SEO URL'),
                'align'     => 'left',
                'index'     => 'urlkey',
            )
        );
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                    'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                )
            )
        );
        /*$this->addColumn(
            'location',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Location'),
                'index'  => 'location',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'deity',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Deity'),
                'index'  => 'deity',
                'type'=> 'text',

            )
        );*/
        $this->addColumn(
            'nearest_location',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Nearest Location'),
                'index'  => 'nearest_location',
                'type'=> 'text',

            )
        );
        /*$this->addColumn(
            'best_time_start',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Best Time To Visit ( State Date )'),
                'index'  => 'best_time_start',
                'type'=> 'date',

            )
        );
        $this->addColumn(
            'best_time_end',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Best Time To Visit ( End Date )'),
                'index'  => 'best_time_end',
                'type'=> 'date',

            )
        );*/
        /*$this->addColumn(
            'months',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Months'),
                'align'     => 'left',
                'index'     => 'months',
            )
        );*/
        $this->addColumn(
            'languages',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Languages'),
                'index'  => 'languages',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'altitude',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Altitude'),
                'align'     => 'left',
                'index'     => 'altitude',
            )
        );
		$this->addColumn(
            'photography',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('Photography'),
                'index'   => 'photography',
                'type'    => 'options',
                'options' => array(
                    'allowed' => Mage::helper('lionleap_temples')->__('Allowed'),
                    'not allowed' => Mage::helper('lionleap_temples')->__('Not Allowed'),
					'not available' => Mage::helper('lionleap_temples')->__('Not Known'),
                )
            )
        );
        $this->addColumn(
            'unesco_listed',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('UNESCO Listed'),
                'index'   => 'unesco_listed',
                'type'    => 'options',
                'options' => array(
                    'yes' => Mage::helper('lionleap_temples')->__('Yes'),
                    'no' => Mage::helper('lionleap_temples')->__('No'),
                )
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('lionleap_temples')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('lionleap_temples')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('lionleap_temples')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('lionleap_temples')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('lionleap_temples')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('temple');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('lionleap_temples')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('lionleap_temples')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Status'),
                        'values' => array(
                            'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                            'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/location_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'location_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Location'),
                'url'        => $this->getUrl('*/*/massLocationId', array('_current'=>true)),
                'additional' => array(
                    'flag_location_id' => array(
                        'name'   => 'flag_location_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Location'),
                        'values' => $values
                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/deity_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'deity_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Deity'),
                'url'        => $this->getUrl('*/*/massDeityId', array('_current'=>true)),
                'additional' => array(
                    'flag_deity_id' => array(
                        'name'   => 'flag_deity_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Deity'),
                        'values' => $values
                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/religion_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'religion_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Religion'),
                'url'        => $this->getUrl('*/*/massReligionId', array('_current'=>true)),
                'additional' => array(
                    'flag_religion_id' => array(
                        'name'   => 'flag_religion_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Religion'),
                        'values' => $values
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Temple
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
