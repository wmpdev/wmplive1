<?php

/**
 * Temple edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Temple_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('temple_');
        $form->setFieldNameSuffix('temple');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'temple_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Temple'))
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $values = Mage::getResourceModel('lionleap_temples/location_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="temple_location_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeLocationIdLink() {
                if ($(\'temple_location_id\').value == \'\') {
                    $(\'temple_location_id_link\').hide();
                } else {
                    $(\'temple_location_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_location/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'temple_location_id\').value);
                    $(\'temple_location_id_link\').href = realUrl;
                    $(\'temple_location_id_link\').innerHTML = text.replace(\'{#name}\', $(\'temple_location_id\').options[$(\'temple_location_id\').selectedIndex].innerHTML);
                }
            }
            $(\'temple_location_id\').observe(\'change\', changeLocationIdLink);
            changeLocationIdLink();
            </script>';

        $fieldset->addField(
            'location_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Location'),
                'name'      => 'location_id',
                'required'  => true,
				'class' => 'required-entry',
                'values'    => $values,
                'after_element_html' => $html,
				'class' => 'required-entry',
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/deity_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="temple_deity_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeDeityIdLink() {
                if ($(\'temple_deity_id\').value == \'\') {
                    $(\'temple_deity_id_link\').hide();
                } else {
                    $(\'temple_deity_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_deity/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'temple_deity_id\').value);
                    $(\'temple_deity_id_link\').href = realUrl;
                    $(\'temple_deity_id_link\').innerHTML = text.replace(\'{#name}\', $(\'temple_deity_id\').options[$(\'temple_deity_id\').selectedIndex].innerHTML);
                }
            }
            $(\'temple_deity_id\').observe(\'change\', changeDeityIdLink);
            changeDeityIdLink();
            </script>';

        $fieldset->addField(
            'deity_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Deity'),
                'name'      => 'deity_id',
                'required'  => true,
				'class' => 'required-entry',
                'values'    => $values,
                'after_element_html' => $html
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/religion_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="temple_religion_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeReligionIdLink() {
                if ($(\'temple_religion_id\').value == \'\') {
                    $(\'temple_religion_id_link\').hide();
                } else {
                    $(\'temple_religion_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_religion/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'temple_religion_id\').value);
                    $(\'temple_religion_id_link\').href = realUrl;
                    $(\'temple_religion_id_link\').innerHTML = text.replace(\'{#name}\', $(\'temple_religion_id\').options[$(\'temple_religion_id\').selectedIndex].innerHTML);
                }
            }
            $(\'temple_religion_id\').observe(\'change\', changeReligionIdLink);
            changeReligionIdLink();
            </script>';

        $fieldset->addField(
            'religion_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Religion'),
                'name'      => 'religion_id',
                'required'  => true,
				'class' => 'required-entry',
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Name'),
                'name'  => 'name',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'urlkey',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Frontend Page URL'),
                'name'  => 'urlkey',
                'note' => 'Instead of empty space use -. Ex: chandra-temple-mumbai'
            )
        );
		
		$fieldset->addField(
            'page_title',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Page Title'),
                'name'  => 'page_title'
            )
        );
		
		$fieldset->addField(
            'canonical_tag',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Canonical meta tag'),
                'name'  => 'canonical_tag'
            )
        );
		
		$fieldset->addField(
            'alt_msg',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Alt text for image'),
                'name'  => 'alt_msg'
            )
        );

        $fieldset->addField(
            'meta_keywords',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Meta Keywords'),
                'name'  => 'meta_keywords',
                'note' => "Seperate keywords by commas(,)",
            )
        );

        $fieldset->addField(
            'meta_description',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Meta Description'),
                'name'  => 'meta_description',
            )
        );

        $fieldset->addField(
            'nearest_location',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Nearest Location'),
                'name'  => 'nearest_location',
				//'required'  => true,
				//'class' => 'required-entry',
           )
        );

        /*$fieldset->addField(
            'best_time_start',
            'date',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Best Time To Visit ( State Date )'),
                'name'  => 'best_time_start',
            'required'  => true,
            'class' => 'required-entry',

            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format'  => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
           )
        );

        $fieldset->addField(
            'best_time_end',
            'date',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Best Time To Visit ( End Date )'),
                'name'  => 'best_time_end',
            'required'  => true,
            'class' => 'required-entry',

            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format'  => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
           )
        );*/

        $fieldset->addField(
            'languages',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Languages'),
                'name'  => 'languages',
				//'required'  => true,
				//'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'description',
            'editor',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Description'),
                'name'  => 'description',
	            'config' => $wysiwygConfig,
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'google_map_link',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Google Map Link'),
                'name'  => 'google_map_link',
				//'required'  => true,
				//'class' => 'required-entry',
           )
        );

		$fieldset->addField(
            'months',
            'multiselect',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Available Months'),
                'name'  => 'months',
				'values'=> Mage::getModel('lionleap_temples/temple_attribute_source_months')->getAllOptions(false),
				'selected' => 'All',
				//'required'  => true,
				//'class' => 'required-entry',
			)
        );

        $fieldset->addField(
            'altitude',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Altitude'),
                'name'  => 'altitude',
			)
        );

		$fieldset->addField(
            'photography',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Photography'),
                'name'   => 'photography',
                'values' => array(
                    array(
                        'value' => 'allowed',
                        'label' => Mage::helper('lionleap_temples')->__('Allowed'),
                    ),
                    array(
                        'value' => 'not allowed',
                        'label' => Mage::helper('lionleap_temples')->__('Not Allowed'),
                    ),
                    array(
                        'value' => 'not available',
                        'label' => Mage::helper('lionleap_temples')->__('Not Known'),
                    ),
                ),
            )
        );
        
        $fieldset->addField(
            'unesco_listed',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('UNESCO Listed'),
                'name'   => 'unesco_listed',
                'values' => array(
                    array(
                        'value' => 'yes',
                        'label' => Mage::helper('lionleap_temples')->__('Yes'),
                    ),
                    array(
                        'value' => 'no',
                        'label' => Mage::helper('lionleap_temples')->__('No'),
                    ),
                ),
            )
        );
        
		$fieldset->addField(
            'show_on_homepage',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Show On Homepage'),
                'name'   => 'show_on_homepage',
                'values' => array(
                    array(
                        'value' => 'no',
                        'label' => Mage::helper('lionleap_temples')->__('No'),
                    ),
					array(
                        'value' => 'yes',
                        'label' => Mage::helper('lionleap_temples')->__('Yes'),
                    ),
                ),
            )
        );

		$fieldset->addField(
            'live_darshan_link',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Live Darshan Link'),
                'name'  => 'live_darshan_link',
           )
        );

		$fieldset->addField(
            'online_prasad_link',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Online Prasad Link'),
                'name'  => 'online_prasad_link',
           )
        );

		$fieldset->addField(
            'pooja_in_temple_link',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Pooja In Temple Link'),
                'name'  => 'pooja_in_temple_link',
           )
        );

		$fieldset->addField(
            'book_hotels_link',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Book Hotels Link'),
                'name'  => 'book_hotels_link',
           )
        );

        $fieldset->addField(
            'tags',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Tags'),
                'name'  => 'tags',
           )
        );
        
		$fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
				'required'  => true,
				'class' => 'required-entry',
            )
        );
        $formValues = Mage::registry('current_temple')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getTempleData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getTempleData());
            Mage::getSingleton('adminhtml/session')->setTempleData(null);
        } elseif (Mage::registry('current_temple')) {
            $formValues = array_merge($formValues, Mage::registry('current_temple')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
