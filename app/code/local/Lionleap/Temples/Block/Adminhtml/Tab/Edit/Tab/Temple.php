<?php
/**
 * Lionleap_Temples extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Lionleap
 * @package        Lionleap_Temples
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * tab - temple relation edit block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Temple extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     * @access protected
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('temple_grid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getTab()->getId()) {
            $this->setDefaultFilter(array('in_temples' => 1));
        }
    }

    /**
     * prepare the temple collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Temple
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('lionleap_temples/temple_collection');
        if ($this->getTab()->getId()) {
            $constraint = 'related.tab_id='.$this->getTab()->getId();
        } else {
            $constraint = 'related.tab_id=0';
        }
        $collection->getSelect()->joinLeft(
            array('related' => $collection->getTable('lionleap_temples/tab_temple')),
            'related.temple_id=main_table.id AND '.$constraint,
            array('position')
        );
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * prepare mass action grid
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Temple
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        return $this;
    }

    /**
     * prepare the grid columns
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Temple
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_temples',
            array(
                'header_css_class'  => 'a-center',
                'type'              => 'checkbox',
                'name'              => 'in_temples',
                'values'            => $this->_getSelectedTemples(),
                'align'             => 'center',
                'index'             => 'id'
            )
        );
        $this->addColumn(
            'name',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Name'),
                'align'     => 'left',
                'index'     => 'name',
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_relation',
                'params'    => array(
                    'id'    => 'getId'
                ),
                'base_link' => 'adminhtml/temples_temple/edit',
            )
        );
        $this->addColumn(
            'position',
            array(
                'header'         => Mage::helper('lionleap_temples')->__('Position'),
                'name'           => 'position',
                'width'          => 60,
                'type'           => 'number',
                'validate_class' => 'validate-number',
                'index'          => 'position',
                'editable'       => true,
            )
        );
    }

    /**
     * Retrieve selected 
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _getSelectedTemples()
    {
        $temples = $this->getTabTemples();
        if (!is_array($temples)) {
            $temples = array_keys($this->getSelectedTemples());
        }
        return $temples;
    }

    /**
     * Retrieve selected {{siblingsLabels}}
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedTemples()
    {
        $temples = array();
        $selected = Mage::registry('current_tab')->getSelectedTemples();
        if (!is_array($selected)) {
            $selected = array();
        }
        foreach ($selected as $temple) {
            $temples[$temple->getId()] = array('position' => $temple->getPosition());
        }
        return $temples;
    }

    /**
     * get row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Temple
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($item)
    {
        return '#';
    }

    /**
     * get grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            '*/*/templesGrid',
            array(
                'id' => $this->getTab()->getId()
            )
        );
    }

    /**
     * get the current tab
     *
     * @access public
     * @return Lionleap_Temples_Model_Tab
     * @author Ultimate Module Creator
     */
    public function getTab()
    {
        return Mage::registry('current_tab');
    }

    /**
     * Add filter
     *
     * @access protected
     * @param object $column
     * @return Lionleap_Temples_Block_Adminhtml_Tab_Edit_Tab_Temple
     * @author Ultimate Module Creator
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_temples') {
            $templeIds = $this->_getSelectedTemples();
            if (empty($templeIds)) {
                $templeIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('id', array('in'=>$templeIds));
            } else {
                if ($templeIds) {
                    $this->getCollection()->addFieldToFilter('id', array('nin'=>$templeIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
}