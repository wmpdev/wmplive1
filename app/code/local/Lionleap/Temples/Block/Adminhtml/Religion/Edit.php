<?php

/**
 * Religion admin edit form
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Religion_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'lionleap_temples';
        $this->_controller = 'adminhtml_religion';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('lionleap_temples')->__('Save Religion')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('lionleap_temples')->__('Delete Religion')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_religion') && Mage::registry('current_religion')->getId()) {
            return Mage::helper('lionleap_temples')->__(
                "Edit Religion '%s'",
                $this->escapeHtml(Mage::registry('current_religion')->getName())
            );
        } else {
            return Mage::helper('lionleap_temples')->__('Add Religion');
        }
    }
}
