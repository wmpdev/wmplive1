<?php

/**
 * Religion admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Religion_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('religion_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Religion'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Religion_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_religion',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Religion'),
                'title'   => Mage::helper('lionleap_temples')->__('Religion'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_religion_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve religion entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Religion
     * @author Ultimate Module Creator
     */
    public function getReligion()
    {
        return Mage::registry('current_religion');
    }
}
