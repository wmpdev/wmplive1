<?php

/**
 * Link edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Link_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Link_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('link_');
        $form->setFieldNameSuffix('link');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'link_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Link'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('lionleap_temples/adminhtml_link_helper_image')
        );
        $values = Mage::getResourceModel('lionleap_temples/temple_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="link_temple_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeTempleIdLink() {
                if ($(\'link_temple_id\').value == \'\') {
                    $(\'link_temple_id_link\').hide();
                } else {
                    $(\'link_temple_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_temple/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'link_temple_id\').value);
                    $(\'link_temple_id_link\').href = realUrl;
                    $(\'link_temple_id_link\').innerHTML = text.replace(\'{#name}\', $(\'link_temple_id\').options[$(\'link_temple_id\').selectedIndex].innerHTML);
                }
            }
            $(\'link_temple_id\').observe(\'change\', changeTempleIdLink);
            changeTempleIdLink();
            </script>';

        $fieldset->addField(
            'temple_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Temple'),
                'name'      => 'temple_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'link',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Link'),
                'name'  => 'link',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'text',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Text'),
                'name'  => 'text',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'picture',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Icon'),
                'name'  => 'picture',

           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
            )
        );
        $formValues = Mage::registry('current_link')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getLinkData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getLinkData());
            Mage::getSingleton('adminhtml/session')->setLinkData(null);
        } elseif (Mage::registry('current_link')) {
            $formValues = array_merge($formValues, Mage::registry('current_link')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
