<?php

/**
 * Link admin grid block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Link_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('linkGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Link_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('lionleap_temples/link')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Link_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Id'),
                'index'  => 'id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'temple_id',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Temple'),
                'index'     => 'temple_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('lionleap_temples/temple_collection')
                    ->toOptionHash(),
                'renderer'  => 'lionleap_temples/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getTempleId'
                ),
                'base_link' => 'adminhtml/temples_temple/edit'
            )
        );
        $this->addColumn(
            'link',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Link'),
                'align'     => 'left',
                'index'     => 'link',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                    'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'text',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Text'),
                'index'  => 'text',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('lionleap_temples')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('lionleap_temples')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('lionleap_temples')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('lionleap_temples')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('lionleap_temples')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Link_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('link');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('lionleap_temples')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('lionleap_temples')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Status'),
                        'values' => array(
                            'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                            'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $values = Mage::getResourceModel('lionleap_temples/temple_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'temple_id',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change Temple'),
                'url'        => $this->getUrl('*/*/massTempleId', array('_current'=>true)),
                'additional' => array(
                    'flag_temple_id' => array(
                        'name'   => 'flag_temple_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Temple'),
                        'values' => $values
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Link
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Link_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
