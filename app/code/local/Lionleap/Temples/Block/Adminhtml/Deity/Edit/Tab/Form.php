<?php

/**
 * Deity edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Deity_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Deity_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('deity_');
        $form->setFieldNameSuffix('deity');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'deity_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Deity'))
        );
        $values = Mage::getResourceModel('lionleap_temples/religion_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="deity_religion_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeReligionIdLink() {
                if ($(\'deity_religion_id\').value == \'\') {
                    $(\'deity_religion_id_link\').hide();
                } else {
                    $(\'deity_religion_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_religion/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'deity_religion_id\').value);
                    $(\'deity_religion_id_link\').href = realUrl;
                    $(\'deity_religion_id_link\').innerHTML = text.replace(\'{#name}\', $(\'deity_religion_id\').options[$(\'deity_religion_id\').selectedIndex].innerHTML);
                }
            }
            $(\'deity_religion_id\').observe(\'change\', changeReligionIdLink);
            changeReligionIdLink();
            </script>';

        $fieldset->addField(
            'religion_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Religion'),
                'name'      => 'religion_id',
                'required'  => true,
                'values'    => $values,
                'after_element_html' => $html,
				'class' => 'required-entry',
            )
        );

        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Name'),
                'name'  => 'name',
				'required'  => true,
				'class' => 'required-entry',
           )
        );
        
		/*$fieldset->addField(
            'tags',
            'textarea',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Tags'),
                'name'  => 'tags',
           )
        );*/
        
		$fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
				'required'  => true,
				'class' => 'required-entry',
            )
        );
        $formValues = Mage::registry('current_deity')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getDeityData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getDeityData());
            Mage::getSingleton('adminhtml/session')->setDeityData(null);
        } elseif (Mage::registry('current_deity')) {
            $formValues = array_merge($formValues, Mage::registry('current_deity')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
