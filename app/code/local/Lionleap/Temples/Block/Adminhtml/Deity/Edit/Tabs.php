<?php

/**
 * Deity admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Deity_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('deity_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Deity'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Deity_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_deity',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Deity'),
                'title'   => Mage::helper('lionleap_temples')->__('Deity'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_deity_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve deity entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Deity
     * @author Ultimate Module Creator
     */
    public function getDeity()
    {
        return Mage::registry('current_deity');
    }
}
