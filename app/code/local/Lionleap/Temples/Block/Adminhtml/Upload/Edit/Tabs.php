<?php

/**
 * Upload admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Upload_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('upload_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Upload'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Upload_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_upload',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Upload'),
                'title'   => Mage::helper('lionleap_temples')->__('Upload'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_upload_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve upload entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Upload
     * @author Ultimate Module Creator
     */
    public function getUpload()
    {
        return Mage::registry('current_upload');
    }
}
