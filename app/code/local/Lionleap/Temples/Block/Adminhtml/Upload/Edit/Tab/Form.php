<?php

/**
 * Upload edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Upload_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Upload_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('upload_');
        $form->setFieldNameSuffix('upload');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'upload_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Upload'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('lionleap_temples/adminhtml_upload_helper_image')
        );
        $values = Mage::getResourceModel('lionleap_temples/temple_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="upload_temple_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeTempleIdLink() {
                if ($(\'upload_temple_id\').value == \'\') {
                    $(\'upload_temple_id_link\').hide();
                } else {
                    $(\'upload_temple_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/temples_temple/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'upload_temple_id\').value);
                    $(\'upload_temple_id_link\').href = realUrl;
                    $(\'upload_temple_id_link\').innerHTML = text.replace(\'{#name}\', $(\'upload_temple_id\').options[$(\'upload_temple_id\').selectedIndex].innerHTML);
                }
            }
            $(\'upload_temple_id\').observe(\'change\', changeTempleIdLink);
            changeTempleIdLink();
            </script>';

        $fieldset->addField(
            'temple_id',
            'select',
            array(
                'label'     => Mage::helper('lionleap_temples')->__('Temple'),
                'name'      => 'temple_id',
                'required'  => true,
				'class' => 'required-entry',
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'type',
            'select',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Type Of Upload'),
                'name'  => 'type',
				'required'  => true,
				'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 'image',
                    'label' => Mage::helper('lionleap_temples')->__('Temple Image'),
                ),
                array(
                    'value' => 'festival image',
                    'label' => Mage::helper('lionleap_temples')->__('Festival Image'),
                ),
                array(
                    'value' => 'file',
                    'label' => Mage::helper('lionleap_temples')->__('File'),
                ),
            ), 
           )
        );

        $fieldset->addField(
            'name',
            'image',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Image'),
                'name'  => 'name',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        /*$fieldset->addField(
            'filesize',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('File Size'),
                'name'  => 'filesize',
            'required'  => true,
            'class' => 'required-entry',

           )
        );*/

        $fieldset->addField(
            'is_primary',
            'select',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Is Primary Image'),
                'name'  => 'is_primary',
            'required'  => true,
            'class' => 'required-entry',

            'values'=> array(
                array(
                    'value' => 'yes',
                    'label' => Mage::helper('lionleap_temples')->__('Yes'),
                ),
                array(
                    'value' => 'no',
                    'label' => Mage::helper('lionleap_temples')->__('No'),
                ),
            ),
           )
        );

        /*$fieldset->addField(
            'alias',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Alias File Name'),
                'name'  => 'alias',
            'required'  => true,
            'class' => 'required-entry',

           )
        );*/

        /*$fieldset->addField(
            'name',
            'image',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Image'),
                'name'  => 'image',

           )
        );*/
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'inactive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
				'required'  => true,
				'class' => 'required-entry',
            )
        );
        $formValues = Mage::registry('current_upload')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getUploadData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getUploadData());
            Mage::getSingleton('adminhtml/session')->setUploadData(null);
        } elseif (Mage::registry('current_upload')) {
            $formValues = array_merge($formValues, Mage::registry('current_upload')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}

