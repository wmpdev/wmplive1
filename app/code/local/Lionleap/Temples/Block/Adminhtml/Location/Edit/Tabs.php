<?php

/**
 * Location admin edit tabs
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Location_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('location_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('lionleap_temples')->__('Location'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Location_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_location',
            array(
                'label'   => Mage::helper('lionleap_temples')->__('Location'),
                'title'   => Mage::helper('lionleap_temples')->__('Location'),
                'content' => $this->getLayout()->createBlock(
                    'lionleap_temples/adminhtml_location_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve location entity
     *
     * @access public
     * @return Lionleap_Temples_Model_Location
     * @author Ultimate Module Creator
     */
    public function getLocation()
    {
        return Mage::registry('current_location');
    }
}
