<?php

/**
 * Location edit form tab
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Location_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Location_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('location_');
        $form->setFieldNameSuffix('location');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'location_form',
            array('legend' => Mage::helper('lionleap_temples')->__('Location'))
        );

        $fieldset->addField(
            'address1',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Address Line 1'),
                'name'  => 'address1',
           )
        );

        $fieldset->addField(
            'address2',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Address line 2'),
                'name'  => 'address2',
           )
        );

        $fieldset->addField(
            'city',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('City'),
                'name'  => 'city',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'state',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('State'),
                'name'  => 'state',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'country',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Country'),
                'name'  => 'country',
				'required'  => true,
				'class' => 'required-entry',
           )
        );

        $fieldset->addField(
            'zipcode',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Zipcode'),
                'name'  => 'zipcode',
           )
        );

        $fieldset->addField(
            'landmark',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Landmark'),
                'name'  => 'landmark',
           )
        );

        $fieldset->addField(
            'contact_number',
            'text',
            array(
                'label' => Mage::helper('lionleap_temples')->__('Contact Number'),
                'name'  => 'contact_number',
           )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('lionleap_temples')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 'active',
                        'label' => Mage::helper('lionleap_temples')->__('Enabled'),
                    ),
                    array(
                        'value' => 'ianctive',
                        'label' => Mage::helper('lionleap_temples')->__('Disabled'),
                    ),
                ),
				'required'  => true,
				'class' => 'required-entry',
            )
        );
        $formValues = Mage::registry('current_location')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getLocationData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getLocationData());
            Mage::getSingleton('adminhtml/session')->setLocationData(null);
        } elseif (Mage::registry('current_location')) {
            $formValues = array_merge($formValues, Mage::registry('current_location')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
