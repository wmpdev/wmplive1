<?php

/**
 * Location admin grid block
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
class Lionleap_Temples_Block_Adminhtml_Location_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('locationGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Location_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('lionleap_temples/location')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Location_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Id'),
                'index'  => 'id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'city',
            array(
                'header'    => Mage::helper('lionleap_temples')->__('City'),
                'align'     => 'left',
                'index'     => 'city',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('lionleap_temples')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                    'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'address1',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Address Line 1'),
                'index'  => 'address1',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'address2',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Address line 2'),
                'index'  => 'address2',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'state',
            array(
                'header' => Mage::helper('lionleap_temples')->__('State'),
                'index'  => 'state',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'country',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Country'),
                'index'  => 'country',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'zipcode',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Zipcode'),
                'index'  => 'zipcode',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'landmark',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Landmark'),
                'index'  => 'landmark',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'contact_number',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Contact Number'),
                'index'  => 'contact_number',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('lionleap_temples')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('lionleap_temples')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('lionleap_temples')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('lionleap_temples')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('lionleap_temples')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('lionleap_temples')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Location_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('location');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('lionleap_temples')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('lionleap_temples')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('lionleap_temples')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('lionleap_temples')->__('Status'),
                        'values' => array(
                            'active' => Mage::helper('lionleap_temples')->__('Enabled'),
                            'inactive' => Mage::helper('lionleap_temples')->__('Disabled'),
                        )
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Lionleap_Temples_Model_Location
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Lionleap_Temples_Block_Adminhtml_Location_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
