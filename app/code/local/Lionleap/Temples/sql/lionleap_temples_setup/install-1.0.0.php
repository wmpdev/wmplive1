<?php

/**
 * Temples module install script
 *
 * @category    Lionleap
 * @package     Lionleap_Temples
 * @author      Ultimate Module Creator
 */
$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/temple'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Temple ID'
    )
    ->addColumn(
        'location_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Location ID'
    )
    ->addColumn(
        'deity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Deity ID'
    )
    ->addColumn(
        'religion_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Religion ID'
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name'
    )
    ->addColumn(
        'location',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Location'
    )
    ->addColumn(
        'deity',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Deity'
    )
    ->addColumn(
        'nearest_location',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Nearest Location'
    )
    ->addColumn(
        'best_time_start',
        Varien_Db_Ddl_Table::TYPE_DATETIME, 255,
        array(
            'nullable'  => false,
        ),
        'Best Time To Visit ( State Date )'
    )
    ->addColumn(
        'best_time_end',
        Varien_Db_Ddl_Table::TYPE_DATETIME, 255,
        array(
            'nullable'  => false,
        ),
        'Best Time To Visit ( End Date )'
    )
    ->addColumn(
        'languages',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Languages'
    )
    ->addColumn(
        'description',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(
            'nullable'  => false,
        ),
        'Description'
    )
    ->addColumn(
        'google_map_link',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(
            'nullable'  => false,
        ),
        'Google Map Link'
    )
	->addColumn(
        'months',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(),
        'Available Months'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Temple Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Temple Creation Time'
    ) 
    ->addIndex($this->getIdxName('lionleap_temples/location', array('location_id')), array('location_id'))
    ->addIndex($this->getIdxName('lionleap_temples/deity', array('deity_id')), array('deity_id'))
    ->addIndex($this->getIdxName('lionleap_temples/religion', array('religion_id')), array('religion_id'))
    ->setComment('Temple Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/location'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Location ID'
    )
    ->addColumn(
        'address1',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Address Line 1'
    )
    ->addColumn(
        'address2',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Address line 2'
    )
    ->addColumn(
        'city',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'City'
    )
    ->addColumn(
        'state',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'State'
    )
    ->addColumn(
        'country',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Country'
    )
    ->addColumn(
        'zipcode',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Zipcode'
    )
    ->addColumn(
        'landmark',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Landmark'
    )
    ->addColumn(
        'contact_number',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Contact Number'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Location Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Location Creation Time'
    ) 
    ->setComment('Location Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/upload'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Upload ID'
    )
    ->addColumn(
        'temple_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Temple ID'
    )
    ->addColumn(
        'type',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable'  => false,
        ),
        'Type Of Upload'
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name of File'
    )
    ->addColumn(
        'filesize',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'nullable'  => false,
            'unsigned'  => true,
        ),
        'File Size'
    )
    ->addColumn(
        'is_primary',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(
            'nullable'  => false,
        ),
        'Is Primary Image'
    )
    ->addColumn(
        'alias',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Alias File Name'
    )
    ->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Image'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Upload Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Upload Creation Time'
    ) 
    ->addIndex($this->getIdxName('lionleap_temples/temple', array('temple_id')), array('temple_id'))
    ->setComment('Upload Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/tab'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Tab ID'
    )
    ->addColumn(
        'temple_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Temple ID'
    )
    ->addColumn(
        'title',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Title'
    )
    ->addColumn(
        'picture',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Image'
    )
    ->addColumn(
        'description',
        Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
        array(
            'nullable'  => false,
        ),
        'Description'
    )
    ->addColumn(
        'extra_classes',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Extra CSS Classes'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Tab Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Tab Creation Time'
    ) 
    ->addIndex($this->getIdxName('lionleap_temples/temple', array('temple_id')), array('temple_id'))
    ->setComment('Tab Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/search'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Search ID'
    )
    ->addColumn(
        'deity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Deity ID'
    )
    ->addColumn(
        'religion_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Religion ID'
    )
    ->addColumn(
        'short_temple_search',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Short Temple Search'
    )
    ->addColumn(
        'long_temple_search',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Long Temple Search'
    )
    ->addColumn(
        'short_location_search',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Short Location Search'
    )
    ->addColumn(
        'long_location_search',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Long Location Search'
    )
    ->addColumn(
        'ip',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'IP Address'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Search Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Search Creation Time'
    ) 
    ->addIndex($this->getIdxName('lionleap_temples/deity', array('deity_id')), array('deity_id'))
    ->addIndex($this->getIdxName('lionleap_temples/religion', array('religion_id')), array('religion_id'))
    ->setComment('Search Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/link'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Link ID'
    )
    ->addColumn(
        'temple_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Temple ID'
    )
    ->addColumn(
        'link',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Link'
    )
    ->addColumn(
        'text',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Text'
    )
    ->addColumn(
        'picture',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(),
        'Icon'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Link Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Link Creation Time'
    ) 
    ->addIndex($this->getIdxName('lionleap_temples/temple', array('temple_id')), array('temple_id'))
    ->setComment('Link Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/deity'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Deity ID'
    )
    ->addColumn(
        'religion_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'unsigned'  => true,
        ),
        'Religion ID'
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Deity Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Deity Creation Time'
    ) 
    ->addIndex($this->getIdxName('lionleap_temples/religion', array('religion_id')), array('religion_id'))
    ->setComment('Deity Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/religion'))
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Religion ID'
    )
    ->addColumn(
        'name',
        Varien_Db_Ddl_Table::TYPE_TEXT, 255,
        array(
            'nullable'  => false,
        ),
        'Name'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
        array(),
        'Enabled'
    )
    ->addColumn(
        'updated',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Religion Modification Time'
    )
    ->addColumn(
        'created',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(),
        'Religion Creation Time'
    ) 
    ->setComment('Religion Table');
/*$this->getConnection()->createTable($table);
$table = $this->getConnection()
    ->newTable($this->getTable('lionleap_temples/tab_store'))
    ->addColumn(
        'tab_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'nullable'  => false,
            'primary'   => true,
        ),
        'Tab ID'
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_SMALLINT,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Store ID'
    )
    ->addIndex(
        $this->getIdxName(
            'lionleap_temples/tab_store',
            array('store_id')
        ),
        array('store_id')
    )
    ->addForeignKey(
        $this->getFkName(
            'lionleap_temples/tab_store',
            'tab_id',
            'lionleap_temples/tab',
            'id'
        ),
        'tab_id',
        $this->getTable('lionleap_temples/tab'),
        'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $this->getFkName(
            'lionleap_temples/tab_store',
            'store_id',
            'core/store',
            'store_id'
        ),
        'store_id',
        $this->getTable('core/store'),
        'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Tabs To Store Linkage Table');
$this->getConnection()->createTable($table);
$this->endSetup();*/
