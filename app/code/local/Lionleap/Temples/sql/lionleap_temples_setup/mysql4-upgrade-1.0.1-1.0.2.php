<?php
/**
 * Created by PhpStorm.
 * User: Su
 * Date: 2/9/15
 * Time: 8:50 PM
 */


$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();
 
$installer->getConnection()
    ->addColumn($this->getTable('lionleap_temples/temple'),
	    'page_title',
	    array(
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'nullable' => true,
	        'default' => null,
	        'comment' => 'Page Title'
	    )
		
	);
	$installer->getConnection()
    ->addColumn($this->getTable('lionleap_temples/temple'),
	    'alt_msg',
	    array(
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'nullable' => true,
	        'default' => null,
	        'comment' => 'alt_msg'
	    )
		
	);
	$installer->getConnection()
    ->addColumn($this->getTable('lionleap_temples/temple'),
	    'canonical_tag',
	    array(
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'nullable' => true,
	        'default' => null,
	        'comment' => 'canonical_tag'
	    )
		
	);
 
$installer->endSetup();