<?php
/**
 * Created by PhpStorm.
 * User: Su
 * Date: 2/9/15
 * Time: 8:50 PM
 */


$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();
 
$installer->getConnection()
    ->addColumn($this->getTable('lionleap_temples/temple'),
	    'urlkey',
	    array(
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'nullable' => true,
	        'default' => null,
	        'comment' => 'URL'
	    )
		
	);
	$installer->getConnection()
    ->addColumn($this->getTable('lionleap_temples/temple'),
	    'meta_keywords',
	    array(
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'nullable' => true,
	        'default' => null,
	        'comment' => 'meta_keywords'
	    )
		
	);
	$installer->getConnection()
    ->addColumn($this->getTable('lionleap_temples/temple'),
	    'meta_description',
	    array(
	        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	        'nullable' => true,
	        'default' => null,
	        'comment' => 'meta_description'
	    )
		
	);
 
$installer->endSetup();