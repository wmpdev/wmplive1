<?php
$installer = $this;

$installer->startSetup();
try {
    $installer->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('mdlblog/mdlblog')} (
            `post_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
            `cat_id` smallint( 11 ) NOT NULL default '0',
            `title` varchar( 255 ) NOT NULL default '',
            `post_content` text NOT NULL ,
            `status` smallint( 6 ) NOT NULL default '0',
			`blogimage` varchar( 255 ) NOT NULL,
			`blogthumb` varchar( 255 ) NOT NULL,
            `created_time` datetime default NULL ,
            `update_time` datetime default NULL ,
            `identifier` varchar( 255 ) NOT NULL default '',
            `user` varchar( 255 ) NOT NULL default '',
            `update_user` varchar( 255 ) NOT NULL default '',
            `meta_keywords` text NOT NULL ,
            `meta_description` text NOT NULL ,
            `comments` TINYINT( 11 ) NOT NULL,
			`short_content` text NOT NULL,
            PRIMARY KEY ( `post_id` ) ,
            UNIQUE KEY `identifier` ( `identifier` )
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8;
		
INSERT INTO {$this->getTable('mdlblog/mdlblog')} (`post_id`, `cat_id`, `title`, `post_content`, `status`, `blogimage`, `blogthumb`, `created_time`, `update_time`, `identifier`, `user`, `update_user`, `meta_keywords`, `meta_description`, `comments`, `short_content`) VALUES
(1, 0, 'Lorem ipsum dolor', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse mattis dui odio, non suscipit sem egestas nec. Etiam sollicitudin iaculis arcu, at pharetra sapien porta at. Nunc euismod, nulla non vestibulum vehicula, justo sapien egestas enim, a aliquet nunc diam pretium velit. Phasellus dui lectus, blandit eget varius et, egestas non orci. Aliquam vestibulum venenatis congue. Nunc ante dui, imperdiet in velit a, tincidunt tincidunt arcu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent rutrum vestibulum lorem, vitae lacinia augue imperdiet sed. Mauris sollicitudin sem in congue ultrices. Donec luctus dictum massa non pretium. Ut viverra volutpat diam ac tempus. Cras suscipit dolor sit amet tellus dapibus semper. Quisque ullamcorper mi sit amet mauris varius cursus. Vestibulum nulla mauris, dignissim vel tincidunt vitae, dapibus eleifend nisl. Morbi congue, mauris id scelerisque elementum, elit libero sagittis lorem, sed consequat orci nisl id nunc.</p>', 1, 'blog/images/4360462761_dc2a2454a7_b.jpg', 'blog/thumb/4360462761_dc2a2454a7_b.jpg', '2014-04-10 08:58:42', '2014-04-10 09:16:46', 'Lorem-ipsum-dolor', 'John Deo', 'Sami Powell', '', '', 0, ''),
(2, 0, 'Nam sapien nunc', '<p>Nam sapien nunc, convallis in sollicitudin in, ullamcorper eu libero. Etiam cursus eu ipsum egestas commodo. Sed est neque, dictum eu mi non, varius commodo lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin nisi sed posuere volutpat. Nunc aliquet mi elit, vitae blandit purus sodales ac. Donec mattis sit amet purus at congue. Donec urna nibh, interdum non dolor et, commodo pharetra massa. Maecenas quis iaculis purus. Integer a diam vel urna placerat semper eu sed leo.</p>', 1, 'blog/images/6892395076_49c7352e1b_b.jpg', 'blog/thumb/6892395076_49c7352e1b_b.jpg', '2014-04-10 09:04:51', '2014-04-10 09:06:47', 'Nam-sapien-nunc', 'John Deo', 'Sami Powell', '', '', 0, ''),
(3, 0, 'Aliquam ultrices', '<p>Aliquam ultrices, nisl ut vehicula feugiat, lacus dolor dapibus nibh, id accumsan nunc lectus non nibh. Duis sed libero ligula. Mauris purus sapien, viverra quis dolor ut, consectetur pretium nisi. In sit amet mi vestibulum, varius tortor vitae, ultricies neque. Quisque sagittis vel nisi in iaculis. Curabitur massa orci, hendrerit nec facilisis in, fringilla et orci. Integer consectetur diam fringilla ipsum sagittis, eget feugiat nibh porta. Nulla lectus metus, faucibus at pretium at, porttitor vitae leo. Cras sollicitudin nisi vitae massa dignissim pellentesque. Pellentesque posuere neque ut egestas pulvinar. Aenean sed mi facilisis, congue leo vitae, ultricies dolor. Ut vel ligula purus. Pellentesque sed ultricies purus. Phasellus nec quam at erat faucibus fringilla quis ut leo. Nullam leo urna, aliquam at sagittis eu, tincidunt sed velit. Donec commodo rhoncus condimentum.</p>', 1, 'blog/images/news01.jpg', 'blog/thumb/news01.jpg', '2014-04-10 09:06:51', '2014-04-10 09:17:31', 'aliquam-ultrices', 'John Deo', 'Sami Powell', '', '', 0, ''),
(4, 0, 'Pellentesque habitant morbi ', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce sit amet pretium ligula, et ultrices sem. Etiam ac iaculis nisi. Maecenas porttitor eros non sem tristique bibendum. Morbi in ipsum nunc. Nulla facilisi. Vivamus eget pretium tellus, eu faucibus dolor. Sed eget condimentum purus. In ultricies viverra fermentum. Maecenas ut nulla et lectus viverra eleifend. Maecenas vitae fringilla nisi, eu ultricies ligula. Sed id ligula ultrices velit tincidunt suscipit. Phasellus ac diam dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras posuere vulputate ligula at mollis. Pellentesque euismod, enim eu vestibulum lacinia, ante diam tincidunt nisl, nec scelerisque nisi sapien nec nunc.</p>', 1, 'blog/images/news05.jpg', 'blog/thumb/news05.jpg', '2014-04-10 09:08:21', '2014-04-10 09:08:41', 'pellentesque-habitant', 'Sami Powell', 'Sami Powell', '', '', 0, ''),
(5, 0, 'Aliquam eget sapien placerat', '<p>Aliquam eget sapien placerat, ultricies quam vel, consectetur sapien. Vestibulum at sapien ornare, elementum quam sit amet, sagittis velit. Aliquam faucibus quam at quam congue porta at sed justo. In lectus nunc, hendrerit eget commodo id, dictum eget nibh. Nam rutrum quam vel vestibulum eleifend. Vestibulum adipiscing enim malesuada, hendrerit turpis non, pulvinar ligula. In ut imperdiet massa, id dignissim orci. Maecenas hendrerit egestas ipsum, in laoreet nunc vulputate quis. Proin a arcu faucibus, faucibus urna tempor, consequat neque. Nam mi orci, lobortis sit amet erat in, dictum cursus risus. Integer non libero sapien. Praesent sit amet mollis sapien. Vivamus vestibulum interdum nibh at tristique.</p>', 1, 'blog/images/news03.jpg', 'blog/thumb/news03.jpg', '2014-04-10 09:08:44', '2014-04-10 09:13:09', 'aliquam-eget', 'John Deo', 'Sami Powell', '', '', 0, ''),
(6, 0, 'Dolor lorem ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse mattis dui odio, non suscipit sem egestas nec. Etiam sollicitudin iaculis arcu, at pharetra sapien porta at. Nunc euismod, nulla non vestibulum vehicula, justo sapien egestas enim, a aliquet nunc diam pretium velit. Phasellus dui lectus, blandit eget varius et, egestas non orci. Aliquam vestibulum venenatis congue. Nunc ante dui, imperdiet in velit a, tincidunt tincidunt arcu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent rutrum vestibulum lorem, vitae lacinia augue imperdiet sed. Mauris sollicitudin sem in congue ultrices. Donec luctus dictum massa non pretium. Ut viverra volutpat diam ac tempus. Cras suscipit dolor sit amet tellus dapibus semper. Quisque ullamcorper mi sit amet mauris varius cursus. Vestibulum nulla mauris, dignissim vel tincidunt vitae, dapibus eleifend nisl. Morbi congue, mauris id scelerisque elementum, elit libero sagittis lorem, sed consequat orci nisl id nunc.</p>', 1, 'blog/images/news04.jpg', 'blog/thumb/news04.jpg', '2014-04-10 09:09:29', '2014-04-10 09:12:59', 'dolor-orem', 'John Deo', 'Sami Powell', '', '', 0, ''),
(7, 0, 'Pellentesque posuere', '<p>Nam sapien nunc, convallis in sollicitudin in, ullamcorper eu libero. Etiam cursus eu ipsum egestas commodo. Sed est neque, dictum eu mi non, varius commodo lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sollicitudin nisi sed posuere volutpat. Nunc aliquet mi elit, vitae blandit purus sodales ac. Donec mattis sit amet purus at congue. Donec urna nibh, interdum non dolor et, commodo pharetra massa. Maecenas quis iaculis purus. Integer a diam vel urna placerat semper eu sed leo.</p>', 1, 'blog/images/news02.jpg', 'blog/thumb/news02.jpg', '2014-04-10 09:10:39', '2014-04-10 09:12:49', 'pellentesque-posuere', 'John Deo', 'Sami Powell', '', '', 0, '');		
		
		
        CREATE TABLE IF NOT EXISTS {$this->getTable('mdlblog/comment')} (
            `comment_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
            `post_id` smallint( 11 ) NOT NULL default '0',
            `comment` text NOT NULL ,
            `status` smallint( 6 ) NOT NULL default '0',
            `created_time` datetime default NULL ,
            `user` varchar( 255 ) NOT NULL default '',
            `email` varchar( 255 ) NOT NULL default '',
            PRIMARY KEY ( `comment_id` )
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8;


INSERT INTO {$this->getTable('mdlblog/comment')} (`comment_id`, `post_id`, `comment`, `status`, `created_time`, `user`, `email`) VALUES
(1, 6, 'Quisque ullamcorper mi sit amet mauris varius cursus. Vestibulum nulla mauris, dignissim vel tincidunt vitae, dapibus eleifend nisl. Morbi congue, mauris id scelerisque elementum, elit libero sagittis lorem, sed consequat orci nisl id nunc.', 2, '2014-04-10 09:32:00', 'John Deo', 'john@mailinator.com'),
(2, 6, 'Suspendisse mattis dui odio, non suscipit sem egestas nec. Etiam sollicitudin iaculis arcu, at pharetra sapien porta at. Nunc euismod, nulla non vestibulum vehicula, justo sapien egestas enim, a aliquet nunc diam pretium velit. Phasellus dui lectus, blandit eget varius et, egestas non orci. Aliquam vestibulum venenatis congue. Nunc ante dui,', 2, '2014-04-10 09:32:23', 'John Deo', 'john@mailinator.com');
		

        CREATE TABLE IF NOT EXISTS {$this->getTable('mdlblog/cat')} (
            `cat_id` int( 11 ) unsigned NOT NULL AUTO_INCREMENT ,
            `title` varchar( 255 ) NOT NULL default '',
            `identifier` varchar( 255 ) NOT NULL default '',
            `sort_order` tinyint ( 6 ) NOT NULL ,
            `meta_keywords` text NOT NULL ,
            `meta_description` text NOT NULL ,
            PRIMARY KEY ( `cat_id` )
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8;
		
INSERT INTO {$this->getTable('mdlblog/cat')} (`cat_id`, `title`, `identifier`, `sort_order`, `meta_keywords`, `meta_description`) VALUES
(1, 'fashion', 'fashion', 0, '', '');		

        CREATE TABLE IF NOT EXISTS {$this->getTable('mdlblog/store')} (
            `post_id` smallint(6) unsigned,
            `store_id` smallint(6) unsigned
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO {$this->getTable('mdlblog/store')} (`post_id`, `store_id`) VALUES
(2, 1),
(2, 3),
(2, 2),
(4, 1),
(4, 3),
(4, 2),
(7, 1),
(7, 3),
(7, 2),
(6, 1),
(6, 3),
(6, 2),
(5, 1),
(5, 3),
(5, 2),
(1, 1),
(1, 3),
(1, 2),
(3, 1),
(3, 3),
(3, 2);

        CREATE TABLE IF NOT EXISTS {$this->getTable('mdlblog/cat_store')} (
            `cat_id` smallint(6) unsigned,
            `store_id` smallint(6) unsigned
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO {$this->getTable('mdlblog/cat_store')} (`cat_id`, `store_id`) VALUES
(1, 0);


        CREATE TABLE IF NOT EXISTS {$this->getTable('mdlblog/post_cat')} (
            `cat_id` smallint(6) unsigned,
            `post_id` smallint(6) unsigned
        ) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO {$this->getTable('mdlblog/post_cat')} (`cat_id`, `post_id`) VALUES
(1, 2),
(1, 4),
(1, 7),
(1, 6),
(1, 5),
(1, 1),
(1, 3);
		
        ALTER TABLE {$this->getTable('mdlblog/mdlblog')} ADD `short_content` TEXT NOT NULL;
		
");
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();