<?php
/*
* @author : preetesh
* @file   : overwrite the core file and increased the memory limit to solve the time execution limit issue
*           custom code is from line no 17 to 20 
*
*/
class Neo_Customoverwrite_Model_Dataflow_Profile extends Mage_Dataflow_Model_Profile
{
	/**
     * Run profile
     *
     * @return Mage_Dataflow_Model_Profile
     */
    public function run()
    {
    	/* Custom code by neo start here */
    		/* to solve export related time execution issue , increased the memory limit */
    		set_time_limit(0);
			ini_set('memory_limit','512M');
    	/* Custom code by neo ends  here*/
        
        /**
         * Save history
         */
        Mage::getModel('dataflow/profile_history')
            ->setProfileId($this->getId())
            ->setActionCode('run')
            ->save();

        /**
         * Prepare xml convert profile actions data
         */
        $xml = '<convert version="1.0"><profile name="default">' . $this->getActionsXml()
             . '</profile></convert>';
        $profile = Mage::getModel('core/convert')
            ->importXml($xml)
            ->getProfile('default');
        /* @var $profile Mage_Dataflow_Model_Convert_Profile */

        try {
            $batch = Mage::getSingleton('dataflow/batch')
                ->setProfileId($this->getId())
                ->setStoreId($this->getStoreId())
                ->save();
            $this->setBatchId($batch->getId());

            $profile->setDataflowProfile($this->getData());
            $profile->run();
        }
        catch (Exception $e) {
            echo $e;
        }

//        if ($batch) {
//            $batch->delete();
//        }

        $this->setExceptions($profile->getExceptions());
        return $this;
    }
}
		