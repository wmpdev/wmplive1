<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Neo_Ajaxcartupdate_CartController extends Mage_Checkout_CartController
{

    /*
    * @author : preetesh jain
    * @creation: 16th feb 16
    * @description: to process the custom ajax reuest to update the cart
    * @response : return json encoded response with all cart info like grand and sub total
    *             current product total and quantiy
    */
    
    public function ajaxUpdateCustomAction(){
        if (!$this->_validateFormKey()) {
            Mage::throwException('Invalid form key');
        }

        $product_id = explode('[', $this->getRequest()->getParam('id'));
        $product_id = explode(']', $product_id[1]);
        $id = (int) $product_id[0];

        $qty = $this->getRequest()->getParam('qty');

        $result = array();
        if ($id) {
            try {
                $cart = $this->_getCart();
                if (isset($qty)) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $qty = $filter->filter($qty);
                }

                $quoteItem = $cart->getQuote()->getItemById($id);
                if (!$quoteItem) {
                    Mage::throwException($this->__('Quote item is not found.'));
                }
                if ($qty == 0) {
                    $cart->removeItem($id);
                } else {
                    $quoteItem->setQty($qty)->save();
                }
                $this->_getCart()->save();

                $this->loadLayout();

                $quote      = Mage::getModel('checkout/session')->getQuote();
                $quoteData  = $quote->getData();
                
                $result['qty'] = $qty;
                $result['subtotal'] = Mage::helper('core')->formatPrice($quoteData['subtotal'], false);
                $result['grand_total'] = Mage::helper('core')->formatPrice($quoteData['grand_total'], false);
                $result['base_row_total'] = $quoteItem->getBaseRowTotalInclTax();
                $result['product_mini_id'] = $id;

                if (!$quoteItem->getHasError()) {
                    $result['message'] = $this->__('Item was updated successfully.');
                } else {
                    $result['notice'] = $quoteItem->getMessage();
                }
                $result['success'] = 1;
            } catch (Exception $e) {
                $result['success'] = 0;
                $result['error'] = $this->__('Can not save item.');
            }
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    
}