<?php
class Mdl_Bannerslider_Helper_Data extends Mage_Core_Helper_Abstract {

  const XML_CONFIG_PATH = 'bannerslider/settings/';
   
    const NAME_DIR_JS = 'mdl/bannerslider/';
  
    protected $_files = array(
        'jquery-1.8.3.js',
        'jquery.noconflict.js',
    );
   
    public function isEnabled()
    {
        return (bool) $this->_getConfigValue('enabled', $store = '');
    }
   
    public function getJQueryPath($file)
    {
        return self::NAME_DIR_JS . $file;
    }
	
    public function getFiles()
    {
        return $this->_files;
    }

    protected function _getConfigValue($key, $store)
    {
        return Mage::getStoreConfig(self::XML_CONFIG_PATH . $key, $store = '');
    }

     public function getBlockIdsToOptionsArray() {
        return array(
            array(
                'label' => $this->__('------- Please choose position -------'),
                'value' => ''),
			  array(
                'label' => $this->__('Category only'),
                'value' => array(
                    array('value' => 'category-sidebar-right-top', 'label' => $this->__('Category-Sidebar-Top-Right')),
                    array('value' => 'category-sidebar-right-bottom', 'label' => $this->__('Category-Sidebar-Bottom-Right')),
                    array('value' => 'category-sidebar-left-top', 'label' => $this->__('Category-Sidebar-Top-Left')),
                    array('value' => 'category-sidebar-left-bottom', 'label' => $this->__('Category-Sidebar-Bottom-Left')),
                    array('value' => 'category-content-top', 'label' => $this->__('Category-Content-Top')),
                    array('value' => 'category-menu-top', 'label' => $this->__('Category-Menu-Top')),
                    array('value' => 'category-menu-bottom', 'label' => $this->__('Category-Menu-Bottom')),
                    array('value' => 'category-page-bottom', 'label' => $this->__('Category-Page-Bottom')),
            )),
        );
    }

    public function getStyleSlider() {
        return array(
            array(
                'label' => $this->__('--------- Please choose style -------'),
                'value' => ''
            ),
            
            array(
                'value' => array(
                    array(
                        'label' => $this->__('Responsive Slider'),
                        'value' => '1'
                    ),
                ),
            ),
           
        );
    }
	
	 public function getLayerCaption() {
		return '<option value="big_white">big_white</option>
										<option value="big_orange">big_orange</option>
										<option value="big_black">big_black</option>
										<option value="medium_grey">medium_grey</option>
										<option value="small_text">small_text</option>
										<option value="medium_text">medium_text</option>
										<option value="large_text">large_text</option>
										<option value="very_large_text">very_large_text</option>
										<option value="very_big_white">very_big_white</option>
										<option value="very_big_black">very_big_black</option>
										<option value="boxshadow">boxshadow</option>
										<option value="black">black</option>
										<option value="noshadow">noshadow</option>';
	 
	 }
	 
	public function getLayerAnimation() {
		return '<option selected="selected" value="fade">Fade</option>
									<option value="sft">Short from Top</option>
									<option value="sfb">Short from Bottom</option>
									<option value="sfr">Short from Right</option>
									<option value="sfl">Short from Left</option>
									<option value="lft">Long from Top</option>
									<option value="lfb">Long from Bottom</option>
									<option value="lfr">Long from Right</option>
									<option value="lfl">Long from Left</option>
									<option value="randomrotate">Random Rotate</option>';
	  
	  }
	  
	public function getLayerEasing() {
		return '<option value="easeOutBack">easeOutBack</option>
									<option value="easeInQuad">easeInQuad</option>
									<option value="easeOutQuad">easeOutQuad</option>
									<option value="easeInOutQuad">easeInOutQuad</option>
									<option value="easeInCubic">easeInCubic</option>
									<option value="easeOutCubic">easeOutCubic</option>
									<option value="easeInOutCubic">easeInOutCubic</option>
									<option value="easeInQuart">easeInQuart</option>
									<option value="easeOutQuart">easeOutQuart</option>
									<option value="easeInOutQuart">easeInOutQuart</option>
									<option value="easeInQuint">easeInQuint</option>
									<option value="easeOutQuint">easeOutQuint</option>
									<option value="easeInOutQuint">easeInOutQuint</option>
									<option value="easeInSine">easeInSine</option>
									<option value="easeOutSine">easeOutSine</option>
									<option value="easeInOutSine">easeInOutSine</option>
									<option value="easeInExpo">easeInExpo</option>
									<option value="easeOutExpo">easeOutExpo</option>
									<option value="easeInOutExpo">easeInOutExpo</option>
									<option value="easeInCirc">easeInCirc</option>
									<option value="easeOutCirc">easeOutCirc</option>
									<option value="easeInOutCirc">easeInOutCirc</option>
									<option value="easeInElastic">easeInElastic</option>
									<option value="easeOutElastic">easeOutElastic</option>
									<option value="easeInOutElastic">easeInOutElastic</option>
									<option value="easeInBack">easeInBack</option>
									<option value="easeInOutBack">easeInOutBack</option>
									<option value="easeInBounce">easeInBounce</option>
									<option value="easeOutBounce">easeOutBounce</option>
									<option value="easeInOutBounce">easeInOutBounce</option>';
		
		}

    public function getOptionYesNo() {
        return array(
            array(
                'label' => $this->__('Yes'),
                'value' => '1'
            ),
            array(
                'label' => $this->__('No'),
                'value' => '0'
            ),
        );
    }

    public function getOptionSliderId() {
        $option = array();
        $bannerslider_id = Mage::app()->getRequest()->getParam('sliderid');
        if ($bannerslider_id) {
            $slider = Mage::getModel('bannerslider/bannerslider')->load($bannerslider_id);
            $option[] = array(
                'value' => $slider->getId(),
                'label' => $slider->getTitle(),
            );
            return $option;
        }
        $option[] = array(
            'value' => '',
            'label' => Mage::helper('bannerslider')->__('-------- Please select a slider --------'),
        );
        $slider = Mage::getModel('bannerslider/bannerslider')->getCollection();
        foreach ($slider as $value) {
            $option[] = array(
                'value' => $value->getId(),
                'label' => $value->getTitle(),
            );
        }

        return $option;
    }

    public function deleteImageFile($image) {
        if (!$image) {
            return;
        }
        $name = $this->reImageName($image);
        $banner_image_path = Mage::getBaseUrl('media') . DS . 'bannerslider' . DS . $name;
        if (!file_exists($banner_image_path)) {
            return;
        }

        try {
            unlink($banner_image_path);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public static function uploadBannerImage() {
        $banner_image_path = Mage::getBaseDir('media') . DS . 'bannerslider';
        $image = "";
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            try {
                /* Starting upload */
                $uploader = new Varien_File_Uploader('image');

                // Any extention would work
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(false);

                $uploader->setFilesDispersion(true);

                $uploader->save($banner_image_path, $_FILES['image']['name']);
            } catch (Exception $e) {
                
            }

            $image = $_FILES['image']['name'];
        }
        return $image;
    }

    public function getStandardSlider() {
        return array(
            array(
                'label' => $this->__('Slider Evolution Default'),
                'value' => '1'
            ),
            array(
                'label' => $this->__('Slider Evolution Caborno'),
                'value' => '2'
            ),
            array(
                'label' => $this->__('Slider Evolution Minimalist'),
                'value' => '3'
            ),
            array(
                'label' => $this->__('Slider Evolution Fresh'),
                'value' => '4'
            ),
            array(
                'label' => $this->__('FlexSlider 1'),
                'value' => '5'
            ),
            array(
                'label' => $this->__('FlexSlider 2'),
                'value' => '6'
            ),
            array(
                'label' => $this->__('FlexSlider 3'),
                'value' => '7'
            ),
            array(
                'label' => $this->__('FlexSlider 4'),
                'value' => '8'
            ),
             array(
                'label' => $this->__('Note display on all pages'),
                'value' => '9'
            ),
        );
    }

    public function getAnimation() {
        return array(
            array(
                'label' => $this->__('boxslide'),
                'value' => 'boxslide'
            ),
			 array(
                'label' => $this->__('boxfade'),
                'value' => 'boxfade'
            ),
			 array(
                'label' => $this->__('slotzoom-horizontal'),
                'value' => 'slotzoom-horizontal'
            ),
			 array(
                'label' => $this->__('slotslide-horizontal'),
                'value' => 'slotslide-horizontal'
            ),
			 array(
                'label' => $this->__('slotfade-horizontal'),
                'value' => 'slotfade-horizontal'
            ),
			 array(
                'label' => $this->__('slotzoom-vertical'),
                'value' => 'slotzoom-vertical'
            ),
			 array(
                'label' => $this->__('slotslide-vertical'),
                'value' => 'slotslide-vertical'
            ),
			 array(
                'label' => $this->__('slotfade-vertical'),
                'value' => 'slotfade-vertical'
            ),
			 array(
                'label' => $this->__('curtain-1'),
                'value' => 'curtain-1'
            ),
			 array(
                'label' => $this->__('curtain-2'),
                'value' => 'curtain-2'
            ),
			 array(
                'label' => $this->__('curtain-3'),
                'value' => 'curtain-3'
            ),
			 array(
                'label' => $this->__('slideleft'),
                'value' => 'slideleft'
            ),
			 array(
                'label' => $this->__('slideright'),
                'value' => 'slideright'
            ),
			 array(
                'label' => $this->__('slideup'),
                'value' => 'slideup'
            ),
			 array(
                'label' => $this->__('slidedown'),
                'value' => 'slidedown'
            ),
			 array(
                'label' => $this->__('fade'),
                'value' => 'fade'
            ),
        );
    }

    public function reImageName($imageName) {

        $subname = substr($imageName, 0, 2);
        $array = array();
        $subDir1 = substr($subname, 0, 1);
        $subDir2 = substr($subname, 1, 1);
        $array[0] = $subDir1;
        $array[1] = $subDir2;
        $name = $array[0] . '/' . $array[1] . '/' . $imageName;

        return strtolower($name);
    }

    public function getBannerImage($image) {
        $name = $this->reImageName($image);
        return Mage::getBaseUrl('media') . 'bannerslider' . '/' . $name;
    }

    public function getPreviewSlider() {
        return Mage::getSingleton('adminhtml/url')->getUrl('bannerslideradmin/adminhtml_standardslider/preview/');
    }

    public function getPathImageForBanner($image) {
        $name = $this->reImageName($image);
        return 'bannerslider' . '/' . $name;
    }

    public function getOptionGridSlider() {
        return array(
            'cms-page-content-top' => 'Homepage content top',
			'cms-page-menu-top' => 'Homepage Menu top',
            'category-sidebar-right-top' => 'Category-Sidebar-Top-Right ',
            'category-sidebar-right-bottom' => 'Category-Sidebar-Bottom-Right',
            'category-sidebar-left-top' => 'Category-Sidebar-Top-Left ',
            'category-sidebar-left-bottom' => 'Category-Sidebar-Bottom-Left',
            'category-content-top' => 'Category-Content-Top',
            'category-menu-top' => 'Category-Menu-Top',
            'category-menu-bottom' => 'Category-Menu-Bottom ',
            'category-page-bottom' => 'Category-Page-Bottom'
        );
    }

    public function getPositionNote() {
        return array(
            array('label' => $this->__('Top-Left'), 'value' => 1),
            array('label' => $this->__('Top-Middle'), 'value' => 2),
            array('label' => $this->__('Top-Right'), 'value' => 3),
            array('label' => $this->__('Left-Middle'), 'value' => 4),
            array('label' => $this->__('Right-Middle'), 'value' => 5),
            array('label' => $this->__('Bottom-Left'), 'value' => 6),
            array('label' => $this->__('Bottom-Middle'), 'value' => 7),
            array('label' => $this->__('Bottom-Right'), 'value' => 8),
        );
    }

    public function getOptionColor() {
        return array(
            array('label' => $this->__('Yellow'), 'value' => '#f7d700'),
            array('label' => $this->__('Red'), 'value' => '#dd0707'),
            array('label' => $this->__('Orange'), 'value' => '#ee5f00'),
            array('label' => $this->__('Green'), 'value' => '#83ba00'),
            array('label' => $this->__('Blue'), 'value' => '#23b8ff'),
            array('label' => $this->__('Gray'), 'value' => '#999'),
        );
    }

    public function getVaulePosition($value) {
        switch ($value) {
            case 1: return "top-left";
            case 2: return "middle-top";
            case 3: return "top-right";
            case 4: return "middle-left";
            case 5: return "middle-right";
            case 6: return "bottom-left";
            case 7: return "middle-bottom";
            case 8: return "bottom-right";
        }
    }

    public function getValueToCsv($itemCollection, $x=0) {
        
        $callback = null;
        $data = array();
        $data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getId()). '"';
        $data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getData('banner_name')) . '"';
		$data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getData('banner_url')) . '"';
        if($x ==0) $data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getData('slider_title')) . '"';
        $data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getData('clicks')) . '"';
        $data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getData('impmode')) . '"';
        $data [] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $itemCollection->getData('date_click')) . '"';
        $callback = implode(',', $data);        
        return $callback;
    }
    
    public function getValueToXml($keyIndex, $itemCollection){
		$callback = null;	
		switch ($keyIndex){		
			case "report_id":
				$callback = $itemCollection->getId();
				break;
			case "banner_name":
				$callback = $itemCollection->getBannerName();
				break;
			case "banner_url":
				$callback = $itemCollection->getBannerUrl();
				break;
			case "slider_title":
				$callback = $itemCollection->getSliderTitle();
				break;
			case "banner_click":
				
				$callback = $itemCollection->getBannerClick();				
				break;
                        case "banner_impress":
				
				$callback = $itemCollection->getBannerImpress();				
				break;
			default: return null;	
		}	
		return $callback;
	}

}