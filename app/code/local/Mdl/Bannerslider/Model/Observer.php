<?php
class Mdl_Bannerslider_Model_Observer
{
	public function controllerActionPredispatch($observer){
		$action = $observer->getEvent()->getControllerAction();
		return $this;
	}
	
	public function prepareLayoutBefore(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('bannerslider')->isEnabled()) {
            return $this;
        }
        $block = $observer->getEvent()->getBlock();

        if ("head" == $block->getNameInLayout()) {
            foreach (Mage::helper('bannerslider')->getFiles() as $file) {
                $block->addJs(Mage::helper('bannerslider')->getJQueryPath($file));
            }
        }
        return $this;
    }
}