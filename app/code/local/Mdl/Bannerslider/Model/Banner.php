<?php
class Mdl_Bannerslider_Model_Banner extends Mage_Core_Model_Abstract {

    protected $_store_id = null;

    public function _construct() {
        parent::_construct();
        if ($storeId = Mage::app()->getStore()->getId()) {
            $this->setStoreId($storeId);
        }
        $this->_init('bannerslider/banner');
    }

    protected function _beforeSave() {
        if ($storeId = $this->getStoreId()) {
            $defaultStore = Mage::getModel('bannerslider/banner')->load($this->getId());
            $storeAttributes = $this->getStoreAttributes();
            foreach ($storeAttributes as $attribute) {
                if ($this->getData($attribute . '_default')) {
                    $this->setData($attribute . '_in_store', false);
                } else {
                    $this->setData($attribute . '_in_store', true);
                    $this->setData($attribute . '_value', $this->getData($attribute));
                }
                $this->setData($attribute, $defaultStore->getData($attribute));
            }
        }
        return parent::_beforeSave();
    }

  

    public function getStoreId() {
        return $this->_store_id;
    }

    public function setStoreId($id) {
        $this->_store_id = $id;
        return $this;
    }

    public function getStoreAttributes() {
        return array(
            'name',
            'status',
            'click_url',
        );
    }

    public function load($id, $field = null) {
        parent::load($id, $field);
        if ($this->getStoreId()) {
            $this->getMultiStoreValue();
        }
        return $this;
    }

   

}