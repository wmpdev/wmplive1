<?php
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('bannerslider_banner')};
DROP TABLE IF EXISTS {$this->getTable('bannerslider_slider')};
DROP TABLE IF EXISTS {$this->getTable('bannerslider_report')};

CREATE TABLE  IF NOT EXISTS {$this->getTable('bannerslider_slider')} (
  `bannerslider_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',  
  `position` varchar(255) NULL ,
  `show_title` tinyint(1) NULL default '0',
  `status` smallint(6) NOT NULL default '0',
  `sort_type` int(11) NULL,
  `description` text NULL,
  `category_ids` varchar(255) NULL,
  `style_content` smallint(6) NOT NULL default '0',
  `custom_code` text NULL,  
  `style_slide` varchar(255) NULL,    
  `width` float(10) NULL,
  `height` float(10) NULL,
  `note_color` varchar(40) NULL,  
  `animation` varchar(255) NULL,  
  `caption` smallint(6) NULL,
  `position_note` int (11) NULL default '1',
  `slider_speed` float (10) NULL,
  `url_view` varchar(255) NULL,  
  `min_item` int(11) NULL,  
  `max_item` int(11) NULL,  
  PRIMARY KEY (`bannerslider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


INSERT INTO {$this->getTable('bannerslider_slider')} (`bannerslider_id`, `title`, `position`, `show_title`, `status`, `sort_type`, `description`, `category_ids`, `style_content`, `custom_code`, `style_slide`, `width`, `height`, `note_color`, `animation`, `caption`, `position_note`, `slider_speed`, `url_view`, `min_item`, `max_item`) VALUES
(1, 'Main slider 01', NULL, 1, 0, NULL, NULL, NULL, 0, NULL, '1', NULL, NULL, NULL, 'boxslide', 0, 1, 4500, NULL, NULL, NULL);


CREATE TABLE  IF NOT EXISTS {$this->getTable('bannerslider_banner')} (
  `banner_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `order_banner` int(11) NULL default '0',
  `bannerslider_id` int(11) NULL,
  `status` smallint(6) NOT NULL default '0',
  `click_url` varchar(255) NULL default '',
  `imptotal` int(11)  NULL default '0',
  `clicktotal` int(11)  NULL default '0',
  `tartget` int(11) NULL default '0', 
  `image`	varchar(255) NULL,
  `image_alt`	varchar(255) NULL,
  `width`	float(10) NULL,
  `height`	float(10) NULL,  
  `start_time` datetime NULL,
  `end_time` datetime NULL,
  `layer_data` text,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO {$this->getTable('bannerslider_banner')} (`banner_id`, `name`, `order_banner`, `bannerslider_id`, `status`, `click_url`, `imptotal`, `clicktotal`, `tartget`, `image`, `image_alt`, `width`, `height`, `start_time`, `end_time`, `layer_data`) VALUES
(1, 'banner-1', 0, 1, 1, 'google.com', 0, 0, 0, 'HornedGhostCrab_ROW11495522746_1366x768.jpg', 'banner-1', NULL, NULL, '2013-02-25 13:50:19', '2024-02-25 13:50:19', '{\"text\":\"Caption Text1\",\"style\":\"very_big_white\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfb\",\"time\":\"500\",\"serial\":\"1\",\"type\":\"text\",\"left\":289,\"top\":67};{\"text\":\"Caption Text4\",\"style\":\"large_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfl\",\"time\":\"800\",\"serial\":\"2\",\"type\":\"text\",\"left\":346,\"top\":142};{\"text\":\"Caption Text3\",\"style\":\"medium_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfr\",\"time\":\"1100\",\"serial\":\"3\",\"type\":\"text\",\"left\":422,\"top\":198};{\"text\":\"Caption Text2\",\"style\":\"small_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lft\",\"time\":\"1400\",\"serial\":\"4\",\"type\":\"text\",\"left\":447,\"top\":231};{\"type\":\"image\",\"text\":\"layer image\",\"speed\":\"300\",\"time\":\"2001\",\"serial\":\"5\",\"image_url\":\"http://localhost/instock2/media/layer/2about-us.fw.png\",\"left\":344,\"animation\":\"lfl\",\"top\":184};{\"type\":\"image\",\"text\":\"layer image\",\"speed\":\"300\",\"time\":\"2300\",\"serial\":\"6\",\"image_url\":\"http://localhost/instock2/media/layer/boomerang-logo-dark.png\",\"left\":520,\"animation\":\"lft\",\"top\":330}'),
(2, 'Banner02', 0, 1, 1, NULL, 0, 0, 0, 'banner-bg01.jpg', 'Banner02', NULL, NULL, '2013-02-25 13:50:34', '2024-02-25 13:50:34', '{\"text\":\"Winter Sale \",\"style\":\"very_big_black\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"fade\",\"time\":\"500\",\"serial\":\"1\",\"type\":\"text\",\"left\":75,\"top\":112};{\"text\":\"Duis autem vel eum iriure dolor in hendrerit in vulputate nulla facilisis at<br>vero eros et accumsan velit esse molestie consequat.\",\"style\":\"black\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"fade\",\"time\":\"800\",\"serial\":\"2\",\"type\":\"text\",\"left\":79,\"top\":226}'),
(3, 'Banner02', 0, 1, 1, NULL, 0, 0, 0, 'Pelican_ROW7896289593_1366x768.jpg', 'Banner02', NULL, NULL, '2013-02-25 13:50:44', '2024-02-25 13:50:44', NULL),
(5, 'Ends today!', 0, 1, 0, NULL, 0, 0, 0, 'banner-bg03.jpg', 'Ends today!', NULL, NULL, '2013-03-01 11:01:04', '2024-03-01 11:01:04', '{\"text\":\"Ends today!\",\"style\":\"big_white\",\"easing\":\"easeInOutSine\",\"speed\":\"300\",\"animation\":\"lfr\",\"time\":\"500\",\"serial\":\"1\",\"type\":\"text\",\"left\":505,\"top\":233};{\"text\":\"Sale\",\"style\":\"very_big_white\",\"easing\":\"easeInQuart\",\"speed\":\"300\",\"animation\":\"lft\",\"time\":\"800\",\"serial\":\"2\",\"type\":\"text\",\"left\":505,\"top\":160};{\"text\":\"use coupon code <span>sale60</span>\",\"style\":\"medium_text\",\"easing\":\"easeInOutCubic\",\"speed\":\"300\",\"animation\":\"lfl\",\"time\":\"1100\",\"serial\":\"3\",\"type\":\"text\",\"left\":505,\"top\":330};{\"text\":\"<a class=''large-green-btn'' href=''#'' title=''SHOP NOW''>SHOP NOW</a>\",\"style\":\"\",\"easing\":\"easeOutQuint\",\"speed\":\"300\",\"animation\":\"lfr\",\"time\":\"1400\",\"serial\":\"4\",\"type\":\"text\",\"left\":777,\"top\":318}'),
(7, 'fabulous fashion blow out', 0, 1, 0, NULL, 0, 0, 0, 'banner-bg02.jpg', 'fabulous fashion blow out', NULL, NULL, '2013-03-01 12:18:12', '2024-03-01 12:18:12', '{\"text\":\"fabulous fashion blow out\",\"style\":\"big_orange\",\"easing\":\"easeInBounce\",\"speed\":\"300\",\"animation\":\"lft\",\"time\":\"500\",\"serial\":\"1\",\"type\":\"text\",\"left\":156,\"top\":300};{\"text\":\"<a class=''green-btn'' href=''#'' title=''Shop Men''>Shop Men</a>\",\"style\":\"small_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfl\",\"time\":\"800\",\"serial\":\"2\",\"type\":\"text\",\"left\":296,\"top\":390};{\"text\":\"<a class=''red-btn'' href=''#'' title=''Shop Kids''>Shop  Kids</a>\",\"style\":\"small_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"sfr\",\"time\":\"1100\",\"serial\":\"4\",\"type\":\"text\",\"left\":568,\"top\":390};{\"text\":\"<a class=''white-btn'' href=''#'' title=''Shop Women''>Shop Women</a>\",\"style\":\"small_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfb\",\"time\":\"1400\",\"serial\":\"3\",\"type\":\"text\",\"left\":420,\"top\":390}'),
(8, 'Sale', 0, 1, 0, NULL, 0, 0, 0, 'banner-bg04.jpg', 'Sale', NULL, NULL, '2013-03-01 12:16:13', '2024-03-01 12:16:13', '{\"text\":\" Smile\",\"style\":\"very_large_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfl\",\"time\":\"500\",\"serial\":\"1\",\"type\":\"text\",\"left\":60,\"top\":190};{\"text\":\"Worthwhile \",\"style\":\"large_text\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfr\",\"time\":\"800\",\"serial\":\"2\",\"type\":\"text\",\"left\":196,\"top\":130};{\"text\":\"Sale\",\"style\":\"big_black\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfb\",\"time\":\"1100\",\"serial\":\"3\",\"type\":\"text\",\"left\":195,\"top\":180};{\"text\":\"<div class=''green-text''>GET</div>\",\"style\":\"\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfl\",\"time\":\"1400\",\"serial\":\"4\",\"type\":\"text\",\"left\":275,\"top\":95};{\"text\":\"<a href=''#'' title=''SHOP COLLECTION '' class=''white-big-btn''>SHOP COLLECTION </a>\",\"style\":\"\",\"easing\":\"easeOutBack\",\"speed\":\"300\",\"animation\":\"lfb\",\"time\":\"1700\",\"serial\":\"5\",\"type\":\"text\",\"left\":262,\"top\":347}');
  

CREATE TABLE  IF NOT EXISTS {$this->getTable('bannerslider_report')} (
  `report_id` int(11) unsigned NOT NULL auto_increment,
  `banner_id` int(11)  NULL,
  `bannerslider_id` int(11) NULL,
  `impmode` int(11)  NULL default '0',
  `clicks` int(11)  NULL default '0',
  `date_click` datetime NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;



");

$installer->endSetup();

