<?php

class Mdl_Bannerslider_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $dataObj = new Varien_Object(
                array(
            'store_id' => '',
            'name_in_store' => '',
            'status_in_store' => '',
            'click_url_in_store' => '',
                )
        );
        if (Mage::getSingleton('adminhtml/session')->getBannerData()) {
            $data = Mage::getSingleton('adminhtml/session')->getBannerData();
            Mage::getSingleton('adminhtml/session')->setBannerData(null);
        } elseif (Mage::registry('banner_data'))
            $data = Mage::registry('banner_data')->getData();
		$layer_content=$data['layer_data'];
		if($layer_content!='')
		{
		$layer_content=addslashes($layer_content);
		$layer_content='['.$layer_content.']';
		$layer_content=str_replace(';',',',$layer_content);
		}
        if (isset($data)) {
            $dataObj->addData($data);
        }
        $data = $dataObj->getData();
        $inStore = $this->getRequest()->getParam('store');
        $defaultLabel = Mage::helper('bannerslider')->__('Use Default');
        $defaultTitle = Mage::helper('bannerslider')->__('-- Please Select --');
        $scopeLabel = Mage::helper('bannerslider')->__('STORE VIEW');

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $wysiwygConfig->addData(array(
            'add_variables' => false,
            'plugins' => array(),
            'widget_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'directives_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted' => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
        ));

        $fieldset = $form->addFieldset('banner_form', array('legend' => Mage::helper('bannerslider')->__('Banner information')));

        $image_calendar = Mage::getBaseUrl('skin') . 'adminhtml/default/default/images/grid-cal.gif';

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('bannerslider')->__('Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
            'disabled' => ($inStore && !$data['name_in_store']),
            'after_element_html' => $inStore ? '</td><td class="use-default">
                                      <input id="name_default" name="name_default" type="checkbox" value="1" class="checkbox config-inherit" ' . ($data['name_in_store'] ? '' : 'checked="checked"') . ' onclick="toggleValueElements(this, Element.previous(this.parentNode))" />
                                      <label for="name_default" class="inherit" title="' . $defaultTitle . '">' . $defaultLabel . '</label>
                        </td><td class="scope-label">
                                      [' . $scopeLabel . ']
                        ' : '</td><td class="scope-label">
                                      [' . $scopeLabel . ']',
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('bannerslider')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 0,
                    'label' => Mage::helper('bannerslider')->__('Enabled'),
                ),
                array(
                    'value' => 1,
                    'label' => Mage::helper('bannerslider')->__('Disabled'),
                ),
            ),
            'disabled' => ($inStore && !$data['status_in_store']),
            'after_element_html' => $inStore ? '</td><td class="use-default">
                                      <input id="status_default" name="status_default" type="checkbox" value="1" class="checkbox config-inherit" ' . ($data['status_in_store'] ? '' : 'checked="checked"') . ' onclick="toggleValueElements(this, Element.previous(this.parentNode))" />
                                      <label for="status_default" class="inherit" title="' . $defaultTitle . '">' . $defaultLabel . '</label>
                        </td><td class="scope-label">
                                      [' . $scopeLabel . ']
                        ' : '</td><td class="scope-label">
                                      [' . $scopeLabel . ']',
        ));
		if ($this->getRequest()->getParam('id') || count(Mage::helper('bannerslider')->getOptionSliderId()) > 1){
			$fieldset->addField('bannerslider_id', 'select', array(
				'label' => Mage::helper('bannerslider')->__('Slider'),
				'name' => 'bannerslider_id',
				'values' => Mage::helper('bannerslider')->getOptionSliderId(),
				'note'   => '<script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'mdl/bannerslider/date_calculation.js"></script>',
			));
		}        

        $fieldset->addField('image_alt', 'text', array(
            'label' => Mage::helper('bannerslider')->__('Alt Text'),
            'name' => 'image_alt',
            'note' => 'Used for SEO',
        ));
		
		  $fieldset->addField('layer_data', 'hidden', array(
            'label' => Mage::helper('bannerslider')->__('test Text'),
            'name' => 'layer_data',
        ));

        $fieldset->addField('click_url', 'text', array(
            'label' => Mage::helper('bannerslider')->__('URL'),
            'required' => false,
            'name' => 'click_url',
            'disabled' => ($inStore && !$data['click_url_in_store']),
            'after_element_html' => $inStore ? '</td><td class="use-default">
                                      <input id="click_url_default" name="click_url_default" type="checkbox" value="1" class="checkbox config-inherit" ' . ($data['click_url_in_store'] ? '' : 'checked="checked"') . ' onclick="toggleValueElements(this, Element.previous(this.parentNode))" />
                                      <label for="click_url_default" class="inherit" title="' . $defaultTitle . '">' . $defaultLabel . '</label>
                        </td><td class="scope-label">
                                      [' . $scopeLabel . ']
                        ' : '</td><td class="scope-label">
                                      [' . $scopeLabel . ']',
        ));
		
        if (isset($data['image']) && $data['image']) {
            $imageName = Mage::helper('bannerslider')->reImageName($data['image']);
            $data['image'] = 'bannerslider' . '/' . $imageName;
        }
		
        $fieldset->addField('image', 'image', array(
            'label' => Mage::helper('bannerslider')->__('Banner Image'),
            'required' => false,
            'name' => 'image',
			'after_element_html' => '<button style="" id="extra-save-continue" onclick="saveAndContinueEdit()" class="scalable save" type="button" title="Save And Continue Edit"><span><span><span>Please Click to Continue</span></span></span></button>',
        ));
		
		
		
		  if (isset($data['image']) && $data['image']) {
		$fieldset->addField('label', 'label', array( 
          'label'     => Mage::helper('bannerslider')->__('Layer'),
		  'note'   => '<link rel="stylesheet" id="edit_layers-css" href="'. $this->getSkinUrl('css/mdl/bannerslider/edit_layers.css').'" type="text/css" media="all">
		  <link rel="stylesheet" id="rs-plugin-captions-css" href="'. $this->getSkinUrl('css/mdl/bannerslider/captions.css').'" type="text/css" media="all">
		   <link rel="stylesheet" id="rs-plugin-captions-css" href="'. $this->getSkinUrl('css/mdl/bannerslider/jquery-ui-1.css').'" type="text/css" media="all">
		  <script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'mdl/bannerslider/jquery-ui.js"></script>
		  <script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'mdl/bannerslider/admin.js"></script>
		  <script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'mdl/bannerslider/edit_layers.js"></script>
		  <script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'mdl/bannerslider/jquery.form.js"></script>
		  <script type="text/javascript" src="'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS).'mdl/bannerslider/upload_file.js"></script>
		  <div class="wrap settings_wrap">
		  <div class="edit_slide_wrapper">
		  <div class="editor_buttons_wrapper">
			<div class="editor_buttons_wrapper_top">
				<input name="radio_bgtype" data-bgtype="image" id="radio_back_image" checked="checked" type="radio" style="visibility:hidden;">
				<!--<label for="radio_back_image">Image BG</label>
				<a href="javascript:void(0)" id="button_change_image" class="button-primary margin_right10" style="margin-bottom:5px">Change Image</a>
				
				<span class="hor_sap"></span>
				
				<input name="radio_bgtype" data-bgtype="trans" id="radio_back_trans" type="radio">
				<label for="radio_back_trans">Trapsparent BG</label>
				
				<span class="hor_sap"></span>
				
				<input name="radio_bgtype" data-bgtype="solid" id="radio_back_solid" type="radio">
				<label for="radio_back_solid">Solid BG</label>
				<input disabled="disabled" name="bg_color" id="slide_bg_color" class="inputColorPicker slide_bg_color disabled" value="#E7E7E7" type="text">
				
				 <a href="javascript:void(0)" class="button-secondary" id="button_preview_slide" title="Preview Slide">Preview Slide</a> -->
				
			</div>
			<div class="clear"></div>
			
			<div class="editor_buttons_wrapper_bottom">
				<a href="javascript:void(0)" id="button_add_layer" class="mdl-btn add-btn"><span>Add Layer</span></a>
				<a href="javascript:void(0)" id="button_add_layer_image" class="mdl-btn images-btn"><span>Add Layer: Image</span></a>
				<a href="javascript:void(0)" id="button_add_layer_video" class="mdl-btn video-btn"><span>Add Layer: Video</span></a>
										
				<a href="javascript:void(0)" id="button_delete_layer" class="mdl-btn cancel-btn"><span>Delete Layer</span></a>
				<a href="javascript:void(0)" id="button_delete_all" class="mdl-btn cancel-btn"><span>Delete All Layers</span></a>
										
				<a href="javascript:void(0)" id="button_duplicate_layer" class="mdl-btn add-btn"><span>Duplicate Layer</span></a>
			</div>
			
			<div class="clear"></div>
			
		</div>
		<div class="clear"></div>
		
		<div style="width:960px;height:450px;background-image:url('.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$data['image'].');" class="slide_layers" id="divLayers"></div>
		<div class="clear"></div>
		<div class="vert_sap"></div>
		<div id="global_timeline" class="timeline">
			<div id="timeline_handle" class="timerdot"></div>
			<div style="display: block; left: 200px; width: 301px;" title="" id="layer_timeline" class="layertime"></div>
			<div class="mintime">0 ms</div>
			<div class="maxtime">2000 ms</div>
		</div>
		<div class="layer_props_wrapper">
		<div class="edit_layers_left">
		
				<form name="form_layers" id="form_layers">
					<div class="settings_wrapper">					
						<div class="postbox unite-postbox">
							<h3 class="no-accordion">
								<span>Layer Params</span>
							</h3>
							<div class="inside">
								<ul class="list_settings">
													<li id="layer_caption_row">
					
											<span original-title="" class="setting_text text-disabled">Style</span>
										
										
					<span class="setting_input">
										<!--<input disabled="disabled" autocomplete="off" class="textbox-caption ui-autocomplete-input setting-disabled" id="layer_caption" name="layer_caption" value="big_white" type="text">-->
										<select disabled="disabled" name="layer_caption" id="layer_caption" class="textbox-caption ui-autocomplete-input setting-disabled">
										<option value="">select</option>
										'.Mage::helper('bannerslider')->getLayerCaption().'
										</select><span class="ui-helper-hidden-accessible" aria-live="polite" role="status"></span>
								</span>
										<div class="clear"></div>
				</li>
				<li><span original-title="" class="setting_text"></span>
					<span class="setting_input settings_addhtml"><div style="display:none;" id="layer_captions_down" class="ui-corner-all ui-state-default"><span class="ui-icon ui-icon-arrowthick-1-s"></span></div><a href="javascript:void(0)" id="buttons_edit_css" class="button-secondary">Edit CSS File</a></span>
				</li>
							<li id="layer_text_row">
					
											<span original-title="" class="setting_text text-disabled">Text&nbsp;/&nbsp;Html</span>
										
											<!--<span class="settings_addhtmlbefore"><a href="javascript:void(0)" id="linkInsertButton" class="disabled">insert button</a></span> -->
										
					<span class="setting_input">
										<textarea disabled="disabled" style="height: 80px;" id="layer_text" class="area-layer-params setting-disabled" name="layer_text"></textarea>				
								</span>
																				<div class="clear"></div>
				</li>
							<!--<li id="layer_image_link_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled">Image&nbsp;Link</span> 
										
										
					<span class="setting_input">
										<input disabled="disabled" class="text-sidebar-link setting-disabled" id="layer_image_link" name="layer_image_link" type="text">
								</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_link_open_in_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled">Link&nbsp;Open&nbsp;In</span>
										
										
					<span class="setting_input">
									<select disabled="disabled" class="setting-disabled" id="layer_link_open_in" name="layer_link_open_in">
								<option value="same" selected="selected">Same Window</option>
									<option value="new">New Window</option>
							</select>
								</span>
																				<div class="clear"></div>
				</li>-->
							<li id="button_edit_video_row" style="display:none;">
					
										
										
					<span class="setting_input">
										<input disabled="disabled" id="button_edit_video" value="Edit Video" class="button-secondary setting-disabled" type="button">
								</span>
																				<div class="clear"></div>
				</li>
							<li id="button_change_image_source_row" style="display:none;">
					
										
										
					<span class="setting_input" style="display:none;">
										<input disabled="disabled" id="button_change_image_source" value="Change Image Source" class="button-secondary setting-disabled" type="button">
								</span>
																				<div class="clear"></div>
				</li>
												
									<!-- LAYER POSITION -->
									<!-- 
									<li class="attribute_title">
										<span class="setting_text_2 text-disabled" original-title="">Layer Position</span>
										<hr>										
									</li>
									 -->
													<li id="layer_left_row">
					
											<span original-title="" class="setting_text text-disabled">Position-X</span>
										
										
					<span class="setting_input">
										<input disabled="disabled" class="text-sidebar setting-disabled" id="layer_left" name="layer_left" type="text">
								</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_top_row">
					
											<span original-title="" class="setting_text text-disabled">Position-Y</span>
										
										
					<span class="setting_input">
										<input disabled="disabled" class="text-sidebar setting-disabled" id="layer_top" name="layer_top" type="text">
								</span>
																				<div class="clear"></div>
				</li>
												
									
									<!--LAYER START ANIMATION -->
									<!-- 
									<li class="attribute_title">
										<span class="setting_text_2 text-disabled" original-title="">Start Transition</span>
										<hr>										
									</li>
									 -->
													<li id="layer_video_autoplay_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled"><label class="text-disabled" for="layer_video_autoplay">Video&nbsp;Autoplay</label></span>
										
										
					<span class="setting_input">
										<input disabled="disabled" id="layer_video_autoplay" class="inputCheckbox setting-disabled" name="layer_video_autoplay" type="checkbox">
								</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_animation_row">
					
											<span original-title="" class="setting_text text-disabled">Animation</span>
										<span class="setting_input">
									<select name="layer_animation" id="layer_animation" class="">
								'.Mage::helper('bannerslider')->getLayerAnimation().'
							</select>
								</span>
										
					
																				<div class="clear"></div>
				</li>
							<li id="layer_easing_row">
					
											<span original-title="" class="setting_text text-disabled">Easing</span>
										
										
					<span class="setting_input">
									<select disabled="disabled" class="setting-disabled" id="layer_easing" name="layer_easing">
								'.Mage::helper('bannerslider')->getLayerEasing().'
							</select>
								</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_speed_row">
					
											<span original-title="" class="setting_text text-disabled">Speed</span>
										
										
					<span class="setting_input">
										<input disabled="disabled" class="text-sidebar setting-disabled" id="layer_speed" name="layer_speed" type="text">
								</span>
											<span class="setting_unit text-disabled">ms</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_hidden_row">
					
											<!--<span original-title="" class="setting_text text-disabled"><label class="text-disabled" for="layer_hidden">Hide&nbsp;Under&nbsp;Width</label></span> 
										
										
					<span class="setting_input">
										<input disabled="disabled" id="layer_hidden" class="inputCheckbox setting-disabled" name="layer_hidden" type="checkbox">
								</span>-->
																				<div class="clear"></div>
				</li>
									<!--LAYER END ANIMATION -->									
									<!-- <li class="attribute_title">
										<span class="setting_text_2 text-disabled" original-title="">End Transition (optional)</span>
										&nbsp;&nbsp;&nbsp;
										<a id="link_show_end_params" class="link_show_end_params" href="javascript:void(0)">Show End Params</a>
										
										<hr>										
									</li> -->
													<li id="layer_endtime_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled">End&nbsp;Time</span>
										
										
					<span class="setting_input">
										<input disabled="disabled" title="" class="text-sidebar setting-disabled" id="layer_endtime" name="layer_endtime" type="text">
								</span>
											<span class="setting_unit text-disabled">ms</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_endspeed_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled">End&nbsp;Speed</span>
										
										
					<span class="setting_input">
										<input disabled="disabled" class="text-sidebar setting-disabled" id="layer_endspeed" name="layer_endspeed" type="text">
								</span>
											<span class="setting_unit text-disabled">ms</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_endanimation_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled">End&nbsp;Animation</span>
										
										
					<span class="setting_input">
									<select disabled="disabled" class="setting-disabled" id="layer_endanimation" name="layer_endanimation">
								<option value="auto" selected="selected">Choose Automatic</option>
									<option value="fadeout">Fade Out</option>
									<option value="stt">Short to Top</option>
									<option value="stb">Short to Bottom</option>
									<option value="stl">Short to Left</option>
									<option value="str">Short to Right</option>
									<option value="ltt">Long to Top</option>
									<option value="ltb">Long to Bottom</option>
									<option value="ltl">Long to Left</option>
									<option value="ltr">Long to Right</option>
									<option value="randomrotateout">Random Rotate Out</option>
							</select>
								</span>
																				<div class="clear"></div>
				</li>
							<li id="layer_endeasing_row" style="display:none;">
					
											<span original-title="" class="setting_text text-disabled">Easing</span>
										
										
					<span class="setting_input">
									<select disabled="disabled" class="setting-disabled" id="layer_endeasing" name="layer_endeasing">
								<option value="nothing" selected="selected">No Change</option>
									'.Mage::helper('bannerslider')->getLayerEasing().'
							</select>
								</span>
																				<div class="clear"></div>
				</li>
											</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
					
				</form>	
				</div>
				<div class="edit_layers_right" style="display:block;">
				<div class="postbox unite-postbox layer_sortbox">
					<div class="no-accordion">
						<span>Layers Sorting</span>
						<ul>
						<li id="button_sort_visibility" title="Hide All Layers"><a href="javascript:void(0);"><span>Hide All Layers</span></a></li>
						<li id="button_sort_time" class="ui-state-active ui-corner-all button_sorttype"><a href="javascript:void(0)"><span>By Time</span></a></li>
						<li id="button_sort_depth" class="ui-state-hover ui-corner-all button_sorttype"><a href="javascript:void(0)"><span>By Depth<span></span></a></li>
						</ul>
					</div>			
					
					<div class="inside">
						<ul id="sortlist" class="sortlist ui-sortable"><li id="layer_sort_1" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><span class="sortbox_text">Caption Text2</span><div class="sortbox_eye"></div><input class="sortbox_time" title="Edit Timeline" value="500" type="text"><input class="sortbox_depth" readonly="readonly" title="Edit Depth" value="2" type="text"><div class="clear"></div></li></ul>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			
		</div>
		</div>
		<script type="text/javascript">
		
		jQuery(document).ready(function() {
							//set init layers object  
							//if('.!empty($layer_content).')
							//{
								UniteLayersRev.setInitLayersJson("'.$layer_content.'");
							//}
						//UniteLayersRev.setInitCaptionClasses("[\"big_white\",\"big_orange\",\"big_black\",\"medium_grey\",\"small_text\",\"medium_text\",\"large_text\",\"very_large_text\",\"very_big_white\",\"very_big_black\",\"boxshadow\",\"black\",\"noshadow\"]");
						
			UniteLayersRev.setCssCaptionsUrl("'.$this->getSkinUrl("css/captions.css").'"); 
			UniteLayersRev.init("2000");
			
		});
	
		</script>
		<div class="caption_css" style="display:none;" title="Edit captions.css"> 
			<div class="edit_css_content">
			<form id="edit_css_form" name="edit_css_form" method="post" action="'.$this->getUrl('bannerslider/adminhtml_banner/editCss').'?isAjax=true&id='.$this->getRequest()->getParam('id').'">
				<textarea rows="20" cols="50" name="caption-text-area" id="caption-text-area">'.file_get_contents($this->getSkinUrl('css/mdl/bannerslider/captions.css')).'</textarea>
				<input type="hidden" id="form_key_css" name="form_key" value="'.$this->getFormKey().'" />
				<input class="mdl-btn" type="submit" value="Update">
			</form>
			</div>
				</div>
		<div id="dialog_image" style="display:none;" title="Select Layer Image">
		   <div class="dialog_image_content">
		   <form id="image_layer_form" name="image_layer_form" method="post" action="'.$this->getUrl('bannerslider/adminhtml_banner/imageLayer').'?isAjax=true">
				<div class="block-header"><h3>Layer Image</h3></div>
				<div class="contents-uploader">
				<input type="hidden" name="form_key" value="'.$this->getFormKey().'" />
				<input type="file" id="layer_image" name="layer_image"/>
				<input class="mdl-btn" type="submit" value="Upload Image">
			<!--	<a href="javascript:void(0)" id="image_layer_form_submit" rel="'.$this->getUrl('bannerslider/adminhtml_banner/imageLayer').'">Upload Image</a> -->
			</div>
				</form>
				 <div class="progress">
        <div class="bar"></div >
        <div class="percent">0%</div >
    </div>
    
    <div id="status"></div>
	<div class="uploaded_layer_image">
	
	</div>
		   </div>
		</div>
		
		<div id="dialog_video" class="dialog-video" title="Add Youtube Layout" style="display:none">
	
	<div class="video_left">
		
		<!-- Type chooser -->
		
		<!-- <div class="video-type-chooser">
			<div class="choose-video-type">
				Choose video type:
			</div>
			
			<label for="video_radio_youtube">Youtube</label>
			<input checked="checked" id="video_radio_youtube" name="video_select" type="radio">
			
			<label for="video_radio_vimeo">Vimeo</label>
			<input id="video_radio_vimeo" name="video_select" type="radio">
			
		</div> -->
		
		<!-- Vimeo block -->
		
		<div id="video_block_vimeo" class="video-select-block" style="display:block;">
		
			<div class="video-title">
				Enter Vimeo ID:
			</div>
			
			<input id="vimeo_id" type="text">
			&nbsp;
			<input id="button_vimeo_search" class="button-regular" value="search" type="button">
			
			<img id="vimeo_loader" src="Revolution%20Slider%20%E2%80%B9%20My%20website%20%E2%80%94%20WordPress_files/loader.gif" style="display:none">
			
			<div class="video_example">
				example:  30300114
			</div>
		
		</div>
		
		<!-- Youtube block -->
		
		<div id="video_block_youtube" class="video-select-block">
			<div class="block-header"><h3>Enter Youtube ID:</h3></div>
			<div class="contents-uploader">
			<input id="youtube_id" type="text">
			&nbsp;
			<input id="button_youtube_search" class="button-regular mdl-btn" value="search" type="button">
			
			<img id="youtube_loader" src="Revolution%20Slider%20%E2%80%B9%20My%20website%20%E2%80%94%20WordPress_files/loader.gif" style="display:none">
			</div>
			<div class="video_example">
				example:  QohUdrgbD2k
			</div>
			
		</div>
		
		<!-- Video controls -->
		
		<div id="video_hidden_controls" style="display:none;">
		
			<div class="youtube-inputs-wrapper contents-uploader">
				Width:
				<input id="input_video_width" class="video-input-small" value="320" type="text">
				Height:
				<input id="input_video_height" class="video-input-small" value="240" type="text">
				<a href="javascript:void(0)" class="button-primary mdl-btn" id="button-video-add">Add This Video</a>
			</div>
		</div>
		
	</div>
	
	<div id="video_content" class="video_right"></div>		
	
</div>
	</div>
		  ',
        ));
		}
		
        $fieldset->addField('start_time', 'hidden', array(
            'label' => Mage::helper('bannerslider')->__('Start date'),
            'required' => false,
            'name' => 'start_time'
            
        ));

        $fieldset->addField('end_time', 'hidden', array(
            'label' => Mage::helper('bannerslider')->__('End date'),
            'required' => false,
            'name' => 'end_time'
           
        ));

        $fieldset->addField('tartget', 'select', array(
            'label' => Mage::helper('bannerslider')->__('Target'),
            'class' => 'required-entry',
            'name' => 'tartget',
            'values' => array(
				array(
                    'value' => 1,
                    'label' => Mage::helper('bannerslider')->__('New Window (On New Tab)'),
                ),
                array(
                    'value' => 0,
                    'label' => Mage::helper('bannerslider')->__('New Window (on Same Tab)'),
                ),            
            ),
        ));
        $form->setValues($data);

        return parent::_prepareForm();
    }

    protected function _prepareLayout() {
        $return = parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        return $return;
    }

}