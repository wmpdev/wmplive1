<?php
class Mdl_Bannerslider_Block_Adminhtml_Addbutton extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }
    
    public function getUrlAddBanner(){                        
        $url = Mage::getSingleton('adminhtml/url')->getUrl('bannerslideradmin/adminhtml_banner/addin');
        return $url.'sliderid/'.$this->getRequest()->getParam('id');
    }
    
    public function getBanner($id){
        return Mage::getModel('bannerslider/banner')->load($id);
    }
}