<?php
class Mdl_Bannerslider_Block_Bannerslider extends Mage_Core_Block_Template {

    protected function _toHtml() {         
                if(!Mage::getStoreConfig('bannerslider/general/enable')){	
                    return '';
                }
               
        $collection = null;
        $banners = array();        
        $collection = Mage::getModel('bannerslider/bannerslider')->getCollection()
                ->addFieldToFilter('status', 0)
                ->addFieldToFilter('position', array(
            'in' => array($this->getBlockPosition(), $this->getCateBlockPosition(), $this->getPopupPosition(), $this->getBlocknotePosition()),
                ));
        foreach ($collection as $item) {
            $block = $this->getLayout()->createBlock('bannerslider/default')
                            ->setTemplate('mdl/bannerslider/bannerslider.phtml')->setSliderData($item);
            $banners[] = $block->renderView();
        }
        return implode('', $banners);
    }

    public function getIsHomePage() {
        return $this->getUrl('') == $this->getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true));
    }
    

}