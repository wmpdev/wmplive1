<?php 
/*  Table::mdl_featured_category */
$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('featuredcategory/featured'))
    ->addColumn('featured_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
        ), 'Featured ID')
    ->addColumn('featured_category', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        ), 'Featured Category')
    ->setComment('Mdl featuredcategory/featured entity table');
$installer->getConnection()->createTable($table);

$installer->endSetup();