<?php

	class Webkul_AdditionalInfo_Model_Status extends Varien_Object {

	    const STATUS_ENABLED = 1;
	    const STATUS_DISABLED = 2;

	    static public function getOptionArray() {
	        return array(
	            self::STATUS_ENABLED => Mage::helper("additionalinfo")->__("Enabled"),
	            self::STATUS_DISABLED => Mage::helper("additionalinfo")->__("Disabled")
	        );
	    }

	}