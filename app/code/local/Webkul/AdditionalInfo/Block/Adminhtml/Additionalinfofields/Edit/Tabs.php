<?php

    class Webkul_AdditionalInfo_Block_Adminhtml_Additionalinfofields_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

        public function __construct() {
            parent::__construct();
            $this->setId("additionalinfo_tabs");
            $this->setDestElementId("edit_form");
            $this->setTitle(Mage::helper("additionalinfo")->__("Item Information"));
        }

        protected function _beforeToHtml() {
            $this->addTab("form_section", array(
                "label"     => Mage::helper("additionalinfo")->__("Custom Registration Fields"),
                "alt"       => Mage::helper("additionalinfo")->__("Custom Registration Fields"),
                "content"   => $this->getLayout()->createBlock("additionalinfo/adminhtml_additionalinfofields_edit_tab_form")->toHtml()
            ));
            return parent::_beforeToHtml();
        }

    }