<?php

	class Webkul_AdditionalInfo_Block_Adminhtml_Additionalinfofields_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	    public function __construct() {
	        parent::__construct();
	        $this->_objectId = "id";
	        $this->_blockGroup = "additionalinfo";
	        $this->_controller = "adminhtml_additionalinfofields";
			$fieldid = $this->getRequest()->getParam("id");
			$collection = Mage::getModel("additionalinfo/additionalinfofields")->getCollection()
						->addFieldToFilter("id",$fieldid)
						->addFieldToFilter("inputtype",array("in"=>array("select","multiselect","radio")));
			foreach ($collection as $record) {
				$selectedfieldtype = $record->getInputtype();
				$selectedfieldval = $record->getSelectoption();
				if($selectedfieldtype == "select"){
					$optselect1 = "multiselect";
					$optselect2 = "radio";
				}
				if($selectedfieldtype == "multiselect"){
					$optselect1 = "select";
					$optselect2 = "radio";
				}
				if($selectedfieldtype == "radio"){
					$optselect1 = "select";
					$optselect2 = "multiselect";
				}
			}
	        $this->_updateButton("save","label",Mage::helper("additionalinfo")->__("Save Field"),array(
	            "label" 	=> Mage::helper("adminhtml")->__("Save And Continue Edit"),
	            "onclick" 	=> "saveAndContinueEdit()",
	            "class" 	=> "save",
            ), -100);
	        $this->_updateButton("delete","label",Mage::helper("additionalinfo")->__("Delete Field"));
	        $this->_addButton("saveandcontinue", array(
	            "label" 	=> Mage::helper("adminhtml")->__("Save And Continue Edit"),
	            "onclick" 	=> "saveAndContinueEdit()",
	            "class" 	=> "save",
            ), -100);
	        $this->_formScripts[] = "
				var parent = document.getElementById('selectoption').parentNode;
				parent = parent.parentNode;
				parent.style.display='none'; 
				var getValue = document.getElementById('inputtype').value; 
				if (getValue == "."'".$selectedfieldtype."'".") {
					parent.removeAttribute('style');
					document.getElementById('selectoption').style.display='block'; 
					d = document.getElementById('selectoption');
					d.className = d.className + ' required-entry ';
				}
				else{
					parent.style.display='none';
					d = document.getElementById('selectoption');
					d.removeAttribute('class');
					d.className = d.className + ' input-text ';
				}
	            function toggleEditor() {
	                if (tinyMCE.getInstanceById('banner_content') == null) {
	                    tinyMCE.execCommand('mceAddControl', false, 'banner_content');
	                } else {
	                    tinyMCE.execCommand('mceRemoveControl', false, 'banner_content');
	                }
	            }
	            function saveAndContinueEdit(){
	                editForm.submit($('edit_form').action+'back/edit/');					
	            }  
				function selectoption(){
					var getValue = document.getElementById('inputtype').value;
					if("."'".$selectedfieldtype."'"."){
						if (getValue == "."'".$selectedfieldtype."'".") { 
							parent.removeAttribute('style');
							document.getElementById('selectoption').style.display='block'; 
							document.getElementById('selectoption').value="."'".$selectedfieldval."'"."; 
							d = document.getElementById('selectoption');
							d.className = d.className + ' required-entry ';
						}
						else{
							if (getValue == "."'".$optselect1."'"." || getValue == "."'".$optselect2."'".")
								document.getElementById('selectoption').value='';
							else{
								parent.style.display='none';
								d.removeAttribute('class');
								d.className = d.className + ' input-text ';
							}
						} 
					}
					else{
						if(getValue == 'select' || getValue == 'multiselect' || getValue == 'radio') { 
							parent.removeAttribute('style');
							document.getElementById('selectoption').style.display = 'block'; 
							d = document.getElementById('selectoption');
							d.className = d.className + ' required-entry ';
						}
						else{
							parent.style.display='none';
							d = document.getElementById('selectoption');
							d.removeAttribute('class');
							d.className = d.className + ' input-text ';
						}
					}
				}";
	    }

	    public function getHeaderText() {
	        if (Mage::registry("additionalinfo_data") && Mage::registry("additionalinfo_data")->getId())
	            return Mage::helper("additionalinfo")->__("Edit Field ", $this->htmlEscape(Mage::registry("additionalinfo_data")->getTitle()));
	        else
	            return Mage::helper("additionalinfo")->__("Add Field");
	    }

	}