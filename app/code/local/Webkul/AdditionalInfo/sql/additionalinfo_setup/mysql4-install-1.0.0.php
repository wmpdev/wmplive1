<?php
$installer = $this;
$installer->startSetup();
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('wk_additionalinfo_fields')};
CREATE TABLE {$this->getTable('wk_additionalinfo_fields')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `labelname` varchar(100) NOT NULL default '',
  `inputname` varchar(255) NOT NULL default '',
  `inputtype` varchar(255) NOT NULL default '',
  `selectoption` varchar(255) NOT NULL default '',
  `setorder` int(11) NOT NULL,
  `reuiredfield` smallint(6) NOT NULL default '0',
  `status` smallint(6) NOT NULL default '0',
  `store` varchar(255) NOT NULL default '',
  `created_time` DATETIME NULL,
  `update_time` DATETIME NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('wk_additionalinfo_data')};
CREATE TABLE {$this->getTable('wk_additionalinfo_data')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `field_id` int(11) NOT NULL default '0',
  `value` varchar(255) NOT NULL default '',
  `customer_id` int(11) NOT NULL default '0',
  `store` varchar(255) NOT NULL default '',  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();