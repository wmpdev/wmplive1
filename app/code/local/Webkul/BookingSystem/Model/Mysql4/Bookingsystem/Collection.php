<?php

	class Webkul_BookingSystem_Model_Mysql4_Bookingsystem_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract	{
		
    	public function _construct()	{
		    parent::_construct();
		    $this->_init("bookingsystem/bookingsystem");
    	}

	}