<?php

    class Webkul_BookingSystem_Block_Adminhtml_Bookingsystem_Grid extends Mage_Adminhtml_Block_Widget_Grid {

        public function __construct() {
            parent::__construct();
            $this->setId("null");
            $this->setDefaultSort("id");
            $this->setDefaultDir("Desc");
            $this->setUseAjax(true);
            $this->setSaveParametersInSession(true);
        }

        protected function _prepareCollection() {
            $collection = Mage::getModel("bookingsystem/bookings")->getCollection();
            $this->setCollection($collection);
            return parent::_prepareCollection();
        }

        protected function _prepareColumns() {
            $this->addColumn("id", array(
                "header"    => $this->__("ID"),
                "align"     => "center",
                "width"     => "50px",
                "index"     => "id"
            ));
      	  
      	    $this->addColumn("from_date", array(
                "header"    => $this->__("Deal Start Date"),
                "index"     => "from_date",
                "align"     => "right"
            ));

            $this->addColumn("from_time", array(
                "header"    => $this->__("Deal Start Time"),
                "index"     => "from_time",
                "align"     => "left"
            ));

            $this->addColumn("to_date", array(
          			"header"    => $this->__("Deal End Date"),
          			"align"     => "right",
          			"index"     => "to_date"
            ));

            $this->addColumn("to_time", array(
                "header"    => $this->__("Deal End Time"),
                "align"     => "left",
                "index"     => "to_time"
            ));

            $this->addColumn("action", array(
                "header"    => $this->__("Action"),
                "width"     => "150px",
                "type"      => "action",
                "getter"    => "getOrderId",
                "align"     => "center",
                "actions"   => array(array(
                                  "caption" => "View Order",
                                  "url"     => array("base"=>"adminhtml/sales_order/view/"),
                                  "field"   => "order_id"
                                )),
                "filter"    => false,
                "sortable"  => false,
                "index"     => "order_id",
                "is_system" => true
            ));

            $this->addExportType("*/*/exportCsv", $this->__("CSV"));
            $this->addExportType("*/*/exportXml", $this->__("XML"));
            return parent::_prepareColumns();
        }

        protected function _prepareMassaction() {
            $this->setMassactionIdField("banner_id");
            $this->getMassactionBlock()->setFormFieldName("bookingsystem");
            $this->getMassactionBlock()->addItem("assign", array(
                "label" => $this->__("Delete"),
                "url" => $this->getUrl("*/*/massDelete"),
                "confirm" => $this->__("Are you sure?")
            ));
            return $this;
        }

        public function getRowUrl($row) {
            return $this->getUrl("adminhtml/sales_order/view/", array("order_id" => $row->getOrderId()));
        }

        public function getGridUrl()    {
            return $this->getUrl("*/*/grid", array("_current" => true));
        }

    }