<?php

	class Webkul_BookingSystem_Block_Adminhtml_Bookingsystem extends Mage_Adminhtml_Block_Widget_Grid_Container {

	    public function __construct() {
	        $this->_controller = "adminhtml_bookingsystem";
	        $this->_blockGroup = "bookingsystem";
	        $this->_headerText = $this->__("Booking System Manager");
	        parent::__construct();
	        $this->removeButton("add");
	    }

	    public function isSingleStoreMode()    {
	        if (!Mage::app()->isSingleStoreMode())
				return false;
	        return true;
	    }

	}