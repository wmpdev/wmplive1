<?php
    $installer = $this;
    $installer->startSetup();
    $installer->run("

    CREATE TABLE IF NOT EXISTS {$this->getTable('wk_bs_bookings_rule')} (
        `rangeid` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `booking_type` int(11) NOT NULL,
        `datefrom` varchar(35) NOT NULL DEFAULT '',
        `dateto` varchar(35) NOT NULL DEFAULT '',
        `timeslot` int(10) NOT NULL DEFAULT '0',
        `cache_time` int(10) NOT NULL,
        `proid` int(11) NOT NULL DEFAULT '0',
        `suns` varchar(30) NOT NULL DEFAULT '',
        `sune` varchar(30) NOT NULL DEFAULT '',
        `mons` varchar(30) NOT NULL DEFAULT '',
        `mone` varchar(30) NOT NULL DEFAULT '',
        `tues` varchar(30) NOT NULL DEFAULT '',
        `tuee` varchar(30) NOT NULL DEFAULT '',
        `weds` varchar(30) NOT NULL DEFAULT '',
        `wede` varchar(30) NOT NULL DEFAULT '',
        `thus` varchar(30) NOT NULL DEFAULT '',
        `thue` varchar(30) NOT NULL DEFAULT '',
        `fris` varchar(30) NOT NULL DEFAULT '',
        `frie` varchar(30) NOT NULL DEFAULT '',
        `sats` varchar(30) NOT NULL DEFAULT '',
        `sate` varchar(30) NOT NULL DEFAULT '',
        `otm_from` text NOT NULL,
        `otm_to` text NOT NULL,
        PRIMARY KEY (`rangeid`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

    CREATE TABLE IF NOT EXISTS {$this->getTable('wk_bs_bookings_done')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `from_time` varchar(35) NOT NULL DEFAULT '',
        `from_date` varchar(35) NOT NULL DEFAULT '',
        `to_time` varchar(255) NOT NULL,
        `to_date` varchar(255) NOT NULL,
        `order_id` int(11) NOT NULL DEFAULT '0',
        `deal_id` int(11) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

    $installer->endSetup();