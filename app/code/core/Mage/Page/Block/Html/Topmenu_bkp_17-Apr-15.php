<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Top menu block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_Topmenu extends Mage_Core_Block_Template
{
    /**
     * Top menu data tree
     *
     * @var Varien_Data_Tree_Node
     */
    protected $_menu;

    /**
     * Current entity key
     *
     * @var string|int
     */
    protected $_currentEntityKey;

	protected $_ulWidth;
    /**
     * Init top menu tree structure
     */
    public function _construct()
    {
        $this->_menu = new Varien_Data_Tree_Node(array(), 'root', new Varien_Data_Tree());

        $this->addData(array(
            'cache_lifetime' => false,
        ));
    }

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '')
    {
        Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array(
            'menu' => $this->_menu,
            'block' => $this
        ));

        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);

        if ($renderer = $this->getChild('catalog.topnav.renderer')) {
            $renderer->setMenuTree($this->_menu)->setChildrenWrapClass($childrenWrapClass);
            $html = $renderer->toHtml();
        } else {
            $html = $this->_getHtml($this->_menu, $childrenWrapClass);
        }

        Mage::dispatchEvent('page_block_html_topmenu_gethtml_after', array(
            'menu' => $this->_menu,
            'html' => $html
        ));

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
		$html = '';

/*
        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
		$childrenCount = $children->count();
		$perColumnMenuItems = 7;
		$totalAllowedColumns = 7;
		$current0li = 0;
		$lefts = array('0', '155', '320', '511','865');

		foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>'
                . $this->escapeHtml($child->getName()) . '</span></a>';

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                if ($childLevel == 0) {
					$currentUl = 0;
					if ($childLevel == 0 && $child->getData('image') != '' && $child->getData('image')) {
						$columns = ceil($child->getChildren()->count() / $perColumnMenuItems) + 1;
					} else {
						$columns = ceil($child->getChildren()->count() / $perColumnMenuItems);
					}
					$divWidth = ( $columns * 100 ) / $totalAllowedColumns;
					$this->_ulWidth = (100 / $columns);
					//echo " ? " . $divWidth . " :: " . $this->_ulWidth . " :: " . ($currentUl*$this->_ulWidth);
					$parentPositionClass = $menuTree->getPositionClass();
					$itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

					$html .= '<div class="menu-white" style="display: none;left:' . $lefts[$current0li] . 'px;width: ' . $divWidth . '%;">';
					$current0li++;
                }
				if ($childLevel == 1) {
					$html .= '<ul class="level' . $childLevel . '">';
				} else {
					$html .= '<ul class="level' . $childLevel . '" style="width: ' . $this->_ulWidth . '%;left:' . ($currentUl*$this->_ulWidth). '%;">';
				}

				$html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';
				
				$parentLevels = $child->getLevel();
				if ($childLevel == 0 && $child->getData('image') != '' && $child->getData('image')) {
					$html .= '<ul class="level0" style="width: ' . $this->_ulWidth . '%;left:' . (($columns-1)*$this->_ulWidth). '%;"><li style="width:100%"><img src="' . $child->getData('image') . '" style="padding:10px;" alt=""></li></ul></div>';
				}
				
                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            }
            $html .= '</li>';
			if ($childLevel == 1 && $counter > 0 && $counter % $perColumnMenuItems == 0) {
				if ($counter != $childrenCount) {
					$html .= '</ul>';
					$currentUl++;
					//echo " :: (" . $this->_ulWidth . ") " . $currentUl*$this->_ulWidth;
					$html .= '<ul class="level' . ($childLevel-1) . '" style="width: ' . $this->_ulWidth . '%;left:' . ($currentUl*$this->_ulWidth). '%;">';
				}
			}

            $counter++;
        }

*/        

                
/*                
$html ='
<li class="level0 1 first level-top parent">
      <a class="level-top plus" href="https://www.wheresmypandit.com/home-pooja.html"><span>Home Pooja</span></a>
      <div style="left: 0px; width: 42.8571%; display: none;" class="menu-white plus">
         <ul style="width: 33.3333%; left: 0%; display: none;" class="level0">
            <li class="level1 1 first"><a href="https://www.wheresmypandit.com/laxmi-pooja.html" class="plus"><span>Laxmi Pooja</span></a></li>
            <li class="level1 2 "><a href="https://www.wheresmypandit.com/akhand-ramayan-paath.html" class="plus"><span>Akhand Ramayan</span></a></li>
            <li class="level1 3">
               <a href="https://www.wheresmypandit.com/griha-pravesh-pooja.html" class="plus"><span>Griha Pravesh Pooja</span></a>               
            </li>
            <li class="level1 4">
               <a href="https://www.wheresmypandit.com/hanuman-chalisa-paath.html" class="plus"><span>Hanuman Chalisa Paath</span></a>               
            </li>
            <li class="level1 5">
               <a href="https://www.wheresmypandit.com/havan.html" class="plus"><span>Havan</span></a>               
            </li>
            <li class="level1 6">
               <a href="https://www.wheresmypandit.com/kaal-bhairav-pooja.html" class="plus"><span>Kaal Bhairav Pooja</span></a>
            </li>
            <li class="level1 7">
               <a href="https://www.wheresmypandit.com/kaali-pooja.html" class="plus"><span>Kaali Pooja</span></a>
            </li>
         </ul>
         <ul style="width: 33.3333%; left: 33.3333%; display: none;" class="level0">
            <li class="level1 8">
               <a href="https://www.wheresmypandit.com/krishna-pooja.html" class="plus"><span>Krishna Pooja</span></a>               
            </li>
            <li class="level1 9">
               <a href="https://www.wheresmypandit.com/maha-mrityunjaya-jaap.html" class="plus"><span>Maha Mrityunjaya Jaap</span></a>               
            </li>
            <li class="level1 10">
               <a href="https://www.wheresmypandit.com/mata-ki-chowki.html" class="plus"><span>Mata ki Chowki</span></a>               
            </li>
            <li class="level1 11">
               <a href="https://www.wheresmypandit.com/navagraha-pooja.html" class="plus"><span>Navargrah Pooja</span></a>               
            </li>
            <li class="level1 12"><a href="https://www.wheresmypandit.com/satyanarayan-pooja.html" class="plus"><span>Satyanarayan Pooja</span></a></li>
            <li class="level1 13">
               <a href="https://www.wheresmypandit.com/shiv-pooja.html" class="plus"><span>Shiv Pooja</span></a>               
            </li>
            <li class="level1">
               <a href="https://www.wheresmypandit.com/sundarkand-paath.html" class="plus"><span>Sundarkand Paath</span></a>               
            </li>
         </ul>
         <ul style="width: 33.3333%; left: 66.6667%; display: none;" class="level0">
            <li class="level1 15">
               <a href="https://www.wheresmypandit.com/mool-shanti-pooja.html" class="plus"><span>Mool Shanti Pooja</span></a>               
            </li>
            <li class="level1 16">
               <a href="https://www.wheresmypandit.com/rudra-abhishek-pooja.html" class="plus"><span>Rudra Abhishek Pooja</span></a>               
            </li>
            <li class="level1 17">
               <a href="https://www.wheresmypandit.com/pitra-dosh-nivaran-pooja.html" class="plus"><span>Pitra Dosh Nivaran Pooja</span></a>               
            </li>
            <li class="level1 18">
               <a href="https://www.wheresmypandit.com/navgraha-shanti-jap.html" class="plus"><span>Navgrah Shanti Jap</span></a>               
            </li>
            <li class="level1 19">
               <a href="https://www.wheresmypandit.com/vivah-pooja.html" class="plus"><span>Vivah Pooja</span></a>               
            </li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-2 level-top parent">
      <a class="level-top plus" href="https://www.wheresmypandit.com/office-pooja.html"><span>Office Pooja</span></a>
      <div style="left: 155px; width: 14.2857%; display: none;" class="menu-white plus">
         <ul style="width: 100%; left: 0%; display: none;" class="level0">
            <li class="level1 1">
               <a href="https://www.wheresmypandit.com/bhoomi-pooja.html" class="plus"><span>Bhoomi Pooja</span></a>               
            </li>
            <li class="level1 2">
               <a href="https://www.wheresmypandit.com/laxmi-pooja.html" class="plus"><span>Laxmi Pooja</span></a>

            </li>
            <li class="level1 3">
               <a href="https://www.wheresmypandit.com/saraswati-pooja.html" class="plus"><span>Sarswati Pooja - Vasant Panchami</span></a>               
            </li>
            <li class="level1 4"><a href="https://www.wheresmypandit.com/satyanarayan-pooja.html" class="plus"><span>Satyanarayan Pooja</span></a></li>
            <li class="level1 5">
               <a href="https://www.wheresmypandit.com/sundarkand-paath.html" class="plus"><span>Sundarkand Paath</span></a>               
            </li>
            <li class="level1 6">
               <a href="https://www.wheresmypandit.com/vishwakarma-pooja.html" class="plus"><span>Vishwakarma Pooja</span></a>               
            </li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-3 level-top parent">
      <a class="level-top" href="https://www.wheresmypandit.com/festivals-pooja.html"><span>Festivals Pooja</span></a>
      <div style="display: none;left:320px;width: 14.2857142857%;" class="menu-white">
         <ul style="width: 100%;left:0%;" class="level0">
            <li class="level1 1">
               <a href="https://www.wheresmypandit.com/dhanteras-pooja.html" class="plus"><span>Dhanteras Pooja</span></a>               
            </li>
            <li class="level1 2">
               <a href="https://www.wheresmypandit.com/durga-pooja.html" class="plus"><span>Durga Pooja</span></a>               
            </li>
            <li class="level1 3">
               <a href="https://www.wheresmypandit.com/ganesh-chaturthi-pooja.html" class="plus"><span>Ganpati Pooja / Ganesh Chaturthi</span></a>               
            </li>
            <li class="level1 4">
               <a href="https://www.wheresmypandit.com/laxmi-pooja.html" class="plus"><span>Laxmi Pooja</span></a>
            </li>
            <li class="level1 5">
               <a href="https://www.wheresmypandit.com/navratri-pooja.html" class="plus"><span>Navratri Pooja</span></a>               
            </li>
            <li class="level1 6">
               <a href="https://www.wheresmypandit.com/gauri-pooja.html" class="plus"><span>Gauri Pooja</span></a>               
            </li>
            <li class="level1 7">
               <a href="https://www.wheresmypandit.com/mahashivratri-pooja.html" class="plus"><span>Mahashivratri Pooja</span></a>               
            </li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-4 level-top"><a class="level-top" href="https://www.wheresmypandit.com/organise-a-pooja.html"><span>Organise a Pooja</span></a></li>
   <li class="level0 nav-5 level-top parent">
      <a class="level-top" href="https://www.wheresmypandit.com/the-holy-store.html"><span>Holy Store</span></a>
      <div style="display: none;left:511px;width: 42.8571428571%;" class="menu-white">
         <ul style="width: 33.3333333333%;left:0%;" class="level0">
            <li class="level1 1 first"><a href="https://www.wheresmypandit.com/the-holy-store/necklace.html"><span>Necklace</span></a></li>
            <li class="level1 2"><a href="https://www.wheresmypandit.com/the-holy-store/chattar.html"><span>Chattar</span></a></li>
            <li class="level1 3"><a href="https://www.wheresmypandit.com/the-holy-store/jaldhari.html"><span>Jaldhari</span></a></li>
            <li class="level1 4"><a href="https://www.wheresmypandit.com/the-holy-store/coins.html"><span>Coins</span></a></li>
            <li class="level1 5"><a href="https://www.wheresmypandit.com/the-holy-store/trishul.html"><span>Trishul</span></a></li>
            <li class="level1 6"><a href="https://www.wheresmypandit.com/the-holy-store/chopda.html"><span>Chopda</span></a></li>
            <li class="level1 7"><a href="https://www.wheresmypandit.com/the-holy-store/agarbatti-stand.html"><span>Agarbatti Stand</span></a></li>
         </ul>
         <ul style="width: 33.3333333333%;left:33.3333333333%;" class="level0">
            <li class="level1 8"><a href="https://www.wheresmypandit.com/the-holy-store/diya.html"><span>Diya</span></a></li>
            <li class="level1 9"><a href="https://www.wheresmypandit.com/the-holy-store/nariyal-kalash.html"><span>Nariyal Kalash</span></a></li>
            <li class="level1 10"><a href="https://www.wheresmypandit.com/the-holy-store/katori.html"><span>Katori</span></a></li>
            <li class="level1 11"><a href="https://www.wheresmypandit.com/the-holy-store/loti.html"><span>Loti</span></a></li>
            <li class="level1 12"><a href="https://www.wheresmypandit.com/the-holy-store/fengshui-tortoise.html"><span>Fengshui Tortoise</span></a></li>
            <li class="level1 13"><a href="https://www.wheresmypandit.com/the-holy-store/aarti-ki-thali.html"><span>Aarti ki Thali</span></a></li>
            <li class="level1 14"><a href="https://www.wheresmypandit.com/the-holy-store/ghanti-bell.html"><span>Ghanti / Bell</span></a></li>
         </ul>
         <ul style="width: 33.3333333333%;left:66.6666666667%;" class="level0">
            <li class="level1 15"><a href="https://www.wheresmypandit.com/the-holy-store/rangoli.html"><span>Rangoli</span></a></li>
            <li class="level1 16"><a href="https://www.wheresmypandit.com/the-holy-store/fresh-on-water.html"><span>Fresh on water</span></a></li>
            <li class="level1 17"><a href="https://www.wheresmypandit.com/the-holy-store/wall-hanging.html"><span>Wall Hanging</span></a></li>
            <li class="level1 18"><a href="https://www.wheresmypandit.com/the-holy-store/diwali-special.html"><span>Diwali Special</span></a></li>
            <li class="level1 19 last"><a href="https://www.wheresmypandit.com/the-holy-store/poshak.html"><span>Poshak</span></a></li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-6 last level-top parent">
      <a class="level-top" href="https://www.wheresmypandit.com/holy-trails.html"><span>Tirtha</span></a>
      <div style="display: none;left:865px;width: 14.2857142857%;" class="menu-white">
         <ul style="width: 100%;left:0%;" class="level0">
            <li class="level1 1 first"><a href="/wmp/temples/" target="_blank"><span>Temples</span></a></li>
            <li class="level1 2"><a href="/wmp/yatras/" target="_blank"><span>Yatras</span></a></li>
            <!-- <li class="level1 3 last"><a href="https://www.wheresmypandit.com/holy-trails/epooja.html"><span>Ninad</span></a></li> -->
         </ul>
      </div>
   </li>'; 
 */  
           
/*
$html ='
    <li class="level0 1 first level-top parent">
      <a class="level-top plus" href="https://www.wheresmypandit.com/home-pooja.html"><span>Home Pooja</span></a>
      <div style="left: 0px; width: 42.8571%; display: none;" class="menu-white plus">
         <ul style="width: 33.3333%; left: 0%; display: none;" class="level0">
            <li class="level1 1 first"><a href="https://www.wheresmypandit.com/laxmi-pooja.html" class="plus"><span>Laxmi Pooja</span></a></li>
            <li class="level1 2 "><a href="https://www.wheresmypandit.com/akhand-ramayan-paath.html" class="plus"><span>Akhand Ramayan Paath</span></a></li>
            <li class="level1 3">
               <a href="https://www.wheresmypandit.com/griha-pravesh-pooja.html" class="plus"><span>Griha Pravesh Pooja</span></a>               
            </li>
            <li class="level1 4">
               <a href="https://www.wheresmypandit.com/hanuman-chalisa-paath.html" class="plus"><span>Hanuman Chalisa Paath</span></a>               
            </li>
            <li class="level1 5">
               <a href="https://www.wheresmypandit.com/havan.html" class="plus"><span>Havan</span></a>               
            </li>
            <li class="level1 6">
               <a href="https://www.wheresmypandit.com/kaal-bhairav-pooja.html" class="plus"><span>Kaal Bhairav Pooja</span></a>
            </li>
            <li class="level1 7">
               <a href="https://www.wheresmypandit.com/kali-pooja.html" class="plus"><span>Kali Pooja</span></a>
            </li>
         </ul>
         <ul style="width: 33.3333%; left: 33.3333%; display: none;" class="level0">
            <li class="level1 8">
               <a href="https://www.wheresmypandit.com/krishna-pooja.html" class="plus"><span>Krishna Pooja</span></a>               
            </li>
            <li class="level1 9">
               <a href="https://www.wheresmypandit.com/maha-mrityunjaya-jaap.html" class="plus"><span>Maha Mrityunjaya Jaap</span></a>               
            </li>
            <li class="level1 10">
               <a href="https://www.wheresmypandit.com/mata-ki-chowki.html" class="plus"><span>Mata ki Chowki</span></a>               
            </li>
            <li class="level1 11">
               <a href="https://www.wheresmypandit.com/navagraha-pooja.html" class="plus"><span>Navagraha Pooja</span></a>               
            </li>
            <li class="level1 12"><a href="https://www.wheresmypandit.com/satyanarayan-pooja.html" class="plus"><span>Satyanarayan Pooja</span></a></li>
            <li class="level1 13">
               <a href="https://www.wheresmypandit.com/shiv-pooja.html" class="plus"><span>Shiv Pooja</span></a>               
            </li>
            <li class="level1">
               <a href="https://www.wheresmypandit.com/sundarkand-paath.html" class="plus"><span>Sundarkand Paath</span></a>               
            </li>
         </ul>
         <ul style="width: 33.3333%; left: 66.6667%; display: none;" class="level0">
            <li class="level1 15">
               <a href="https://www.wheresmypandit.com/mool-shanti-pooja.html" class="plus"><span>Mool Shanti Pooja</span></a>               
            </li>
            <li class="level1 16">
               <a href="https://www.wheresmypandit.com/rudra-abhishek-pooja.html" class="plus"><span>Rudra Abhishek Pooja</span></a>               
            </li>
            <li class="level1 17">
               <a href="https://www.wheresmypandit.com/pitra-dosh-nivaran-pooja.html" class="plus"><span>Pitra Dosh Nivaran Pooja</span></a>               
            </li>
            <li class="level1 18">
               <a href="https://www.wheresmypandit.com/navgraha-shanti-jap.html" class="plus"><span>Navgrah Shanti Jap</span></a>               
            </li>
            <li class="level1 19">
               <a href="https://www.wheresmypandit.com/vivah-pooja.html" class="plus"><span>Vivah Pooja</span></a>               
            </li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-2 level-top parent">
      <a class="level-top plus" href="https://www.wheresmypandit.com/office-pooja.html"><span>Office Pooja</span></a>
      <div style="left: 155px; width: 14.2857%; display: none;" class="menu-white plus">
         <ul style="width: 100%; left: 0%; display: none;" class="level0">
            <li class="level1 1">
               <a href="https://www.wheresmypandit.com/bhoomi-pooja.html" class="plus"><span>Bhoomi Pooja</span></a>               
            </li>
            <li class="level1 2">
               <a href="https://www.wheresmypandit.com/laxmi-pooja.html" class="plus"><span>Laxmi Pooja</span></a>

            </li>
            <li class="level1 3">
               <a href="https://www.wheresmypandit.com/saraswati-pooja.html" class="plus"><span>Sarswati Pooja - Vasant Panchami</span></a>               
            </li>
            <li class="level1 4"><a href="https://www.wheresmypandit.com/satyanarayan-pooja.html" class="plus"><span>Satyanarayan Pooja</span></a></li>
            <li class="level1 5">
               <a href="https://www.wheresmypandit.com/sundarkand-paath.html" class="plus"><span>Sundarkand Paath</span></a>               
            </li>
            <li class="level1 6">
               <a href="https://www.wheresmypandit.com/vishwakarma-pooja.html" class="plus"><span>Vishwakarma Pooja</span></a>               
            </li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-3 level-top parent">
      <a class="level-top" href="https://www.wheresmypandit.com/festivals-pooja.html"><span>Festivals Pooja</span></a>
      <div style="display: none;left:320px;width: 14.2857142857%;" class="menu-white">
         <ul style="width: 100%;left:0%;" class="level0">
            <li class="level1 1">
               <a href="https://www.wheresmypandit.com/dhanteras-pooja.html" class="plus"><span>Dhanteras Pooja</span></a>               
            </li>
            <li class="level1 2">
               <a href="https://www.wheresmypandit.com/durga-pooja.html" class="plus"><span>Durga Pooja</span></a>               
            </li>
            <li class="level1 3">
               <a href="https://www.wheresmypandit.com/ganesh-chaturthi-pooja.html" class="plus"><span>Ganpati Pooja / Ganesh Chaturthi</span></a>               
            </li>
            <li class="level1 4">
               <a href="https://www.wheresmypandit.com/laxmi-pooja.html" class="plus"><span>Laxmi Pooja</span></a>
            </li>
            <li class="level1 5">
               <a href="https://www.wheresmypandit.com/navratri-pooja.html" class="plus"><span>Navratri Pooja</span></a>               
            </li>
            <li class="level1 6">
               <a href="https://www.wheresmypandit.com/gauri-pooja.html" class="plus"><span>Gauri Pooja</span></a>               
            </li>
            <li class="level1 7">
               <a href="https://www.wheresmypandit.com/maha-shivratri-pooja.html" class="plus"><span>Maha Shivratri Pooja</span></a>               
            </li>
         </ul>
      </div>
   </li>   
   <li class="level0 nav-4 level-top"><a class="level-top" href="https://www.wheresmypandit.com/book-a-pandit"><span>Book a Pandit</span></a></li>
   <li class="level0 nav-4 level-top"><a class="level-top" href="https://www.wheresmypandit.com/organise-a-pooja.html"><span>Organise a Pooja</span></a></li>
   <li class="level0 nav-5 level-top parent">
      <a class="level-top" href="https://www.wheresmypandit.com/the-holy-store.html"><span>Holy Store</span></a>
      <div style="display: none;left:511px;width: 42.8571428571%;" class="menu-white">
         <ul style="width: 33.3333333333%;left:0%;" class="level0">
            <li class="level1 1 first"><a href="https://www.wheresmypandit.com/the-holy-store/necklace.html" class="plus"><span>Necklace</span></a></li>
            <li class="level1 2"><a href="https://www.wheresmypandit.com/the-holy-store/chattar.html" class="plus"><span>Chattar</span></a></li>
            <li class="level1 3"><a href="https://www.wheresmypandit.com/the-holy-store/jaldhari.html" class="plus"><span>Jaldhari</span></a></li>
            <li class="level1 4"><a href="https://www.wheresmypandit.com/the-holy-store/coins.html" class="plus"><span>Coins</span></a></li>
            <li class="level1 5"><a href="https://www.wheresmypandit.com/the-holy-store/trishul.html" class="plus"><span>Trishul</span></a></li>
            <li class="level1 6"><a href="https://www.wheresmypandit.com/the-holy-store/chopda.html" class="plus"><span>Chopda</span></a></li>
            <li class="level1 7"><a href="https://www.wheresmypandit.com/the-holy-store/agarbatti-stand.html" class="plus"><span>Agarbatti Stand</span></a></li>
         </ul>
         <ul style="width: 33.3333333333%;left:33.3333333333%;" class="level0">
            <li class="level1 8"><a href="https://www.wheresmypandit.com/the-holy-store/diya.html" class="plus"><span>Diya</span></a></li>
            <li class="level1 9"><a href="https://www.wheresmypandit.com/the-holy-store/nariyal-kalash.html" class="plus"><span>Nariyal Kalash</span></a></li>
            <li class="level1 10"><a href="https://www.wheresmypandit.com/the-holy-store/katori.html" class="plus"><span>Katori</span></a></li>
            <li class="level1 11"><a href="https://www.wheresmypandit.com/the-holy-store/loti.html" class="plus"><span>Loti</span></a></li>
            <li class="level1 12"><a href="https://www.wheresmypandit.com/the-holy-store/fengshui-tortoise.html" class="plus"><span>Fengshui Tortoise</span></a></li>
            <li class="level1 13"><a href="https://www.wheresmypandit.com/the-holy-store/aarti-ki-thali.html" class="plus"><span>Aarti ki Thali</span></a></li>
            <li class="level1 14"><a href="https://www.wheresmypandit.com/the-holy-store/ghanti-bell.html" class="plus"><span>Ghanti / Bell</span></a></li>
         </ul>
         <ul style="width: 33.3333333333%;left:66.6666666667%;" class="level0">
            <li class="level1 15"><a href="https://www.wheresmypandit.com/the-holy-store/rangoli.html" class="plus"><span>Rangoli</span></a></li>
            <li class="level1 16"><a href="https://www.wheresmypandit.com/the-holy-store/fresh-on-water.html" class="plus"><span>Fresh on water</span></a></li>
            <li class="level1 17"><a href="https://www.wheresmypandit.com/the-holy-store/wall-hanging.html" class="plus"><span>Wall Hanging</span></a></li>
            <li class="level1 18"><a href="https://www.wheresmypandit.com/the-holy-store/diwali-special.html" class="plus"><span>Diwali Special</span></a></li>
            <li class="level1 19 last"><a href="https://www.wheresmypandit.com/the-holy-store/poshak.html" class="plus"><span>Poshak</span></a></li>
         </ul>
      </div>
   </li>
   <li class="level0 nav-6 last level-top parent">
      <a class="level-top" href="https://www.wheresmypandit.com/holy-trails.html"><span>Tirtha</span></a>
      <div style="display: none;left:1040px;width: 14.2857142857%;" class="menu-white">
         <ul style="width: 100%;left:0%;" class="level0">
            <li class="level1 1 first"><a href="/temples/" target="_blank" class="plus"><span>Temples</span></a></li>
            <li class="level1 2"><a href="/yatras/" target="_blank" class="plus"><span>Yatras</span></a></li>             
         </ul>
      </div>
   </li>
';
        */ 



$html ='
<li class="level0 1 first level-top parent">
   <a href="https://www.wheresmypandit.com/home-pooja.html" class="level-top"><span>Home Pooja</span></a>
   <div class="menu-white" style="display: none;left:0px;width: 42.8571428571%;">
      <ul class="level0" style="width: 33.3333333333%;left:0%;">
         <li class="level1 1 first ">
            <a class="plus" href="https://www.wheresmypandit.com/laxmi-pooja.html"><span>Laxmi Pooja</span></a>
         </li>
         <li class="level1 2 ">
            <a class="plus" href="https://www.wheresmypandit.com/akhand-ramayan-paath.html"><span>Akhand Ramayan Paath</span></a>
         </li>
         <li class="level1 3 ">
            <a class="plus" href="https://www.wheresmypandit.com/griha-pravesh-pooja.html"><span>Griha Pravesh Pooja</span></a>
         </li>
         <li class="level1 4 ">
            <a class="plus" href="https://www.wheresmypandit.com/hanuman-chalisa-paath.html"><span>Hanuman Chalisa Paath</span></a>
         </li>
         <li class="level1 5 ">
            <a class="plus" href="https://www.wheresmypandit.com/havan.html"><span>Havan</span></a>
         </li>
         <li class="level1 6 ">
            <a class="plus" href="https://www.wheresmypandit.com/kaal-bhairav-pooja.html"><span>Kaal Bhairav Pooja</span></a>
         </li>
         <li class="level1 7 ">
            <a class="plus" href="https://www.wheresmypandit.com/kali-pooja.html"><span>Kali Pooja</span></a>
         </li>
      </ul>
      <ul class="level0" style="width: 33.3333333333%;left:33.3333333333%;">
         <li class="level1 8 ">
            <a class="plus" href="https://www.wheresmypandit.com/krishna-pooja.html"><span>Krishna Pooja</span></a>
         </li>
         <li class="level1 9 ">
            <a class="plus" href="https://www.wheresmypandit.com/maha-mrityunjaya-jaap.html"><span>Maha Mrityunjaya Jaap</span></a>
         </li>
         <li class="level1 10 ">
            <a class="plus" href="https://www.wheresmypandit.com/mata-ki-chowki.html"><span>Mata ki Chowki</span></a>
         </li>
         <li class="level1 11 ">
            <a class="plus" href="https://www.wheresmypandit.com/navagraha-pooja.html"><span>Navagraha Pooja</span></a>
         </li>
         <li class="level1 12 ">
            <a class="plus" href="https://www.wheresmypandit.com/satyanarayan-pooja.html"><span>Satyanarayan Pooja</span></a>
         </li>
         <li class="level1 13 ">
            <a class="plus" href="https://www.wheresmypandit.com/shiv-pooja.html"><span>Shiv Pooja</span></a>
         </li>
         <li class="level1 14 ">
            <a class="plus" href="https://www.wheresmypandit.com/sundarkand-paath.html"><span>Sundarkand Paath</span></a>
         </li>
      </ul>
      <ul class="level0" style="width: 33.3333333333%;left:66.6666666667%;">
         <li class="level1 15 ">
            <a class="plus" href="https://www.wheresmypandit.com/mool-shanti-pooja.html"><span>Mool Shanti Pooja</span></a>
         </li>
         <li class="level1 16 ">
            <a class="plus" href="https://www.wheresmypandit.com/rudra-abhishek-pooja.html"><span>Rudra Abhishek Pooja</span></a>
         </li>
         <li class="level1 17 ">
            <a class="plus" href="https://www.wheresmypandit.com/pitra-dosh-nivaran-pooja.html"><span>Pitra Dosh Nivaran Pooja</span></a>
         </li>
         <li class="level1 18 ">
            <a class="plus" href="https://www.wheresmypandit.com/navgraha-shanti-jap.html"><span>Navgrah Shanti Jap</span></a>
         </li>
         <li class="level1 19 last ">
            <a class="plus" href="https://www.wheresmypandit.com/vivah-pooja.html"><span>Vivah Pooja</span></a>
         </li>
      </ul>
   </div>
</li>
<li class="level0 nav-2 level-top ">
   <a href="https://www.wheresmypandit.com/office-pooja.html" class="level-top"><span>Office Pooja</span></a>
   <div class="menu-white" style="display: none;left:155px;width: 14.2857142857%;">
      <ul class="level0" style="width: 100%;left:0%;">
         <li class="level1 1 first ">
            <a class="plus" href="https://www.wheresmypandit.com/bhoomi-pooja.html"><span>Bhoomi Pooja</span></a>
         </li>
         <li class="level1 2 ">
            <a class="plus" href="https://www.wheresmypandit.com/laxmi-pooja.html"><span>Laxmi Pooja</span></a>
         </li>
         <li class="level1 3 ">
            <a class="plus" href="https://www.wheresmypandit.com/saraswati-pooja.html"><span>Sarswati Pooja - Vasant Panchami</span></a>
         </li>
         <li class="level1 4 ">
            <a class="plus" href="https://www.wheresmypandit.com/satyanarayan-pooja.html"><span>Satyanarayan Pooja</span></a>
         </li>
         <li class="level1 5 ">
            <a class="plus" href="https://www.wheresmypandit.com/sundarkand-paath.html"><span>Sundarkand Paath</span></a>
         </li>
         <li class="level1 6 last ">
            <a class="plus" href="https://www.wheresmypandit.com/vishwakarma-pooja.html"><span>Vishwakarma Pooja</span></a>
         </li>
      </ul>
   </div>
</li>
<li class="level0 nav-3 level-top parent">
   <a href="https://www.wheresmypandit.com/festivals-pooja.html" class="level-top plus"><span>Festivals Pooja</span></a>
   <div class="menu-white plus" style="left: 320px; width: 14.2857%; display: none;">
      <ul class="level0" style="width: 100%; left: 0%; display: none;">
         <li class="level1 1 first ">
            <a class="plus" href="https://www.wheresmypandit.com/dhanteras-pooja.html"><span>Dhanteras Pooja</span></a>
         </li>
         <li class="level1 2 ">
            <a class="plus" href="https://www.wheresmypandit.com/durga-pooja.html"><span>Durga Pooja</span></a>
         </li>
         <li class="level1 3 ">
            <a class="plus" href="https://www.wheresmypandit.com/ganesh-chaturthi-pooja.html"><span>Ganpati Pooja / Ganesh Chaturthi</span></a>
         </li>
         <li class="level1 4 ">
            <a class="plus" href="https://www.wheresmypandit.com/laxmi-pooja.html"><span>Laxmi Pooja</span></a>
         </li>
         <li class="level1 5 ">
            <a class="plus" href="https://www.wheresmypandit.com/navratri-pooja.html"><span>Navratri Pooja</span></a>
         </li>
         <li class="level1 6 ">
            <a class="plus" href="https://www.wheresmypandit.com/gauri-pooja.html"><span>Gauri Pooja</span></a>
         </li>
         <li class="level1 7 last ">
            <a class="plus" href="https://www.wheresmypandit.com/maha-shivratri-pooja.html"><span>Maha Shivratri Pooja</span></a>
         </li>
      </ul>
   </div>
</li>
<li class="level0 nav-4 level-top"><a class="level-top" href="https://www.wheresmypandit.com/book-a-pandit"><span>Book a Pandit</span></a></li>
<li class="level0 nav-4 level-top"><a href="https://www.wheresmypandit.com/organise-a-pooja.html" class="level-top"><span>Organise a Pooja</span></a></li>
<li class="level0 nav-5 level-top parent">
   <a href="https://www.wheresmypandit.com/the-holy-store.html" class="level-top plus"><span>Holy Store</span></a>
   <div class="menu-white plus" style="left: 511px; width: 42.8571%; display: none;">
      <ul class="level0" style="width: 33.3333%; left: 0%; display: none;">
         <li class="level1 1 first"><a href="https://www.wheresmypandit.com/the-holy-store/necklace.html" class="plus"><span>Necklace</span></a></li>
         <li class="level1 2"><a href="https://www.wheresmypandit.com/the-holy-store/chattar.html" class="plus"><span>Chattar</span></a></li>
         <li class="level1 3"><a href="https://www.wheresmypandit.com/the-holy-store/jaldhari.html"class="plus"><span>Jaldhari</span></a></li>
         <li class="level1 4"><a href="https://www.wheresmypandit.com/the-holy-store/coins.html" class="plus"><span>Coins</span></a></li>
         <li class="level1 5"><a href="https://www.wheresmypandit.com/the-holy-store/trishul.html" class="plus"><span>Trishul</span></a></li>
         <li class="level1 6"><a href="https://www.wheresmypandit.com/the-holy-store/chopda.html" class="plus"><span>Chopda</span></a></li>
         <li class="level1 7"><a href="https://www.wheresmypandit.com/the-holy-store/agarbatti-stand.html" class="plus"><span>Agarbatti Stand</span></a></li>
      </ul>
      <ul class="level0" style="width: 33.3333%; left: 33.3333%; display: none;">
         <li class="level1 8"><a href="https://www.wheresmypandit.com/the-holy-store/diya.html" class="plus"><span>Diya</span></a></li>
         <li class="level1 9"><a href="https://www.wheresmypandit.com/the-holy-store/nariyal-kalash.html" class="plus"><span>Nariyal Kalash</span></a></li>
         <li class="level1 10"><a href="https://www.wheresmypandit.com/the-holy-store/katori.html" class="plus"><span>Katori</span></a></li>
         <li class="level1 11"><a href="https://www.wheresmypandit.com/the-holy-store/loti.html" class="plus"><span>Loti</span></a></li>
         <li class="level1 12"><a href="https://www.wheresmypandit.com/the-holy-store/fengshui-tortoise.html" class="plus"><span>Fengshui Tortoise</span></a></li>
         <li class="level1 13"><a href="https://www.wheresmypandit.com/the-holy-store/aarti-ki-thali.html" class="plus"><span>Aarti ki Thali</span></a></li>
         <li class="level1 14"><a href="https://www.wheresmypandit.com/the-holy-store/ghanti-bell.html" class="plus"><span>Ghanti / Bell</span></a></li>
      </ul>
      <ul class="level0" style="width: 33.3333%; left: 66.6667%; display: none;">
         <li class="level1 15"><a href="https://www.wheresmypandit.com/the-holy-store/rangoli.html" class="plus"><span>Rangoli</span></a></li>
         <li class="level1 16"><a href="https://www.wheresmypandit.com/the-holy-store/fresh-on-water.html" class="plus"><span>Fresh on water</span></a></li>
         <li class="level1 17"><a href="https://www.wheresmypandit.com/the-holy-store/wall-hanging.html" class="plus"><span>Wall Hanging</span></a></li>
         <li class="level1 18"><a href="https://www.wheresmypandit.com/the-holy-store/diwali-special.html" class="plus"><span>Diwali Special</span></a></li>
         <li class="level1 19"><a href="https://www.wheresmypandit.com/the-holy-store/poshak.html" class="plus"><span>Poshak</span></a></li>
         <li class="level1 20"><a href="https://www.wheresmypandit.com/the-holy-store/toran.html" class="plus"><span>Toran</span></a></li>
         <li class="level1 21 last"><a href="https://www.wheresmypandit.com/the-holy-store/view-more.html" class="plus"><span>View More</span></a></li>
      </ul>
   </div>
</li>
<li class="level0 nav-6 last level-top parent">
   <a href="https://www.wheresmypandit.com/holy-trails.html" class="level-top"><span>Tirtha</span></a>
   <div class="menu-white" style="display: none;left:1040px;width: 14.2857142857%;">
      <ul class="level0" style="width: 100%;left:0%;">
         <li class="level1 1 first"><a class="plus" target="_blank" href="/temples/"><span>Temples</span></a></li>
         <li class="level1 2 last"><a class="plus" target="_blank" href="/yatras/"><span>Yatras</span></a></li>
      </ul>
   </div>
</li>
';
                

        
        return $html;
    }

    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }

        return $html;
    }

    /**
     * Returns array of menu item's attributes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        $attributes = array(
            'class' => implode(' ', $menuItemClasses)
        );

        return $attributes;
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = array();

        $classes[] = 'level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren()) {
            $classes[] = 'parent';
        }

        return $classes;
    }

    /**
     * Retrieve cache key data
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $shortCacheId = array(
            'TOPMENU',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrentEntityKey()
        );
        $cacheId = $shortCacheId;

        $shortCacheId = array_values($shortCacheId);
        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['entity_key'] = $this->getCurrentEntityKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    /**
     * Retrieve current entity key
     *
     * @return int|string
     */
    public function getCurrentEntityKey()
    {
        if (null === $this->_currentEntityKey) {
            $this->_currentEntityKey = Mage::registry('current_entity_key')
                ? Mage::registry('current_entity_key') : Mage::app()->getStore()->getRootCategoryId();
        }
        return $this->_currentEntityKey;
    }
}
