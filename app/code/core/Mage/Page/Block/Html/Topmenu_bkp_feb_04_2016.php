<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Top menu block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_Topmenu extends Mage_Core_Block_Template {

    /**
     * Top menu data tree
     *
     * @var Varien_Data_Tree_Node
     */
    protected $_menu;

    /**
     * Current entity key
     *
     * @var string|int
     */
    protected $_currentEntityKey;
    protected $_ulWidth;

    /**
     * Init top menu tree structure
     */
    public function _construct() {
        $this->_menu = new Varien_Data_Tree_Node(array(), 'root', new Varien_Data_Tree());

        $this->addData(array(
            'cache_lifetime' => false,
        ));
    }

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '') {
        Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array(
            'menu' => $this->_menu,
            'block' => $this
        ));

        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);

        if ($renderer = $this->getChild('catalog.topnav.renderer')) {
            $renderer->setMenuTree($this->_menu)->setChildrenWrapClass($childrenWrapClass);
            $html = $renderer->toHtml();
        } else {
            $html = $this->_getHtml($this->_menu, $childrenWrapClass);
        }

        Mage::dispatchEvent('page_block_html_topmenu_gethtml_after', array(
            'menu' => $this->_menu,
            'html' => $html
        ));

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass) {
        $html = '';

        /*
          $children = $menuTree->getChildren();
          $parentLevel = $menuTree->getLevel();
          $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

          $counter = 1;
          $childrenCount = $children->count();
          $perColumnMenuItems = 7;
          $totalAllowedColumns = 7;
          $current0li = 0;
          $lefts = array('0', '155', '320', '511','865');

          foreach ($children as $child) {

          $child->setLevel($childLevel);
          $child->setIsFirst($counter == 1);
          $child->setIsLast($counter == $childrenCount);
          $child->setPositionClass($itemPositionClassPrefix . $counter);

          $outermostClassCode = '';
          $outermostClass = $menuTree->getOutermostClass();

          if ($childLevel == 0 && $outermostClass) {
          $outermostClassCode = ' class="' . $outermostClass . '" ';
          $child->setClass($outermostClass);
          }

          $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
          $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>'
          . $this->escapeHtml($child->getName()) . '</span></a>';

          if ($child->hasChildren()) {
          if (!empty($childrenWrapClass)) {
          $html .= '<div class="' . $childrenWrapClass . '">';
          }
          if ($childLevel == 0) {
          $currentUl = 0;
          if ($childLevel == 0 && $child->getData('image') != '' && $child->getData('image')) {
          $columns = ceil($child->getChildren()->count() / $perColumnMenuItems) + 1;
          } else {
          $columns = ceil($child->getChildren()->count() / $perColumnMenuItems);
          }
          $divWidth = ( $columns * 100 ) / $totalAllowedColumns;
          $this->_ulWidth = (100 / $columns);
          //echo " ? " . $divWidth . " :: " . $this->_ulWidth . " :: " . ($currentUl*$this->_ulWidth);
          $parentPositionClass = $menuTree->getPositionClass();
          $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

          $html .= '<div class="menu-white" style="display: none;left:' . $lefts[$current0li] . 'px;width: ' . $divWidth . '%;">';
          $current0li++;
          }
          if ($childLevel == 1) {
          $html .= '<ul class="level' . $childLevel . '">';
          } else {
          $html .= '<ul class="level' . $childLevel . '" style="width: ' . $this->_ulWidth . '%;left:' . ($currentUl*$this->_ulWidth). '%;">';
          }

          $html .= $this->_getHtml($child, $childrenWrapClass);
          $html .= '</ul>';

          $parentLevels = $child->getLevel();
          if ($childLevel == 0 && $child->getData('image') != '' && $child->getData('image')) {
          $html .= '<ul class="level0" style="width: ' . $this->_ulWidth . '%;left:' . (($columns-1)*$this->_ulWidth). '%;"><li style="width:100%"><img src="' . $child->getData('image') . '" style="padding:10px;" alt=""></li></ul></div>';
          }

          if (!empty($childrenWrapClass)) {
          $html .= '</div>';
          }
          }
          $html .= '</li>';
          if ($childLevel == 1 && $counter > 0 && $counter % $perColumnMenuItems == 0) {
          if ($counter != $childrenCount) {
          $html .= '</ul>';
          $currentUl++;
          //echo " :: (" . $this->_ulWidth . ") " . $currentUl*$this->_ulWidth;
          $html .= '<ul class="level' . ($childLevel-1) . '" style="width: ' . $this->_ulWidth . '%;left:' . ($currentUl*$this->_ulWidth). '%;">';
          }
          }

          $counter++;
          }

         */

        /*
          $html ='
          <li class="level0 1 first level-top parent">
          <a href="https://www.wheresmypandit.com/home-puja.html" class="level-top"><span>Home Puja</span></a>
          <div class="menu-white" style="display: none;left:0px;width: 42.8571428571%;">
          <ul class="level0" style="width: 33.3333333333%;left:0%;">
          <li class="level1 1 first ">
          <a class="plus" href="https://www.wheresmypandit.com/laxmi-puja.html"><span>Laxmi Puja</span></a>
          </li>
          <li class="level1 2 ">
          <a class="plus" href="https://www.wheresmypandit.com/akhand-ramayan-paath.html"><span>Akhand Ramayan Paath</span></a>
          </li>
          <li class="level1 3 ">
          <a class="plus" href="https://www.wheresmypandit.com/griha-pravesh-puja.html"><span>Griha Pravesh Puja</span></a>
          </li>
          <li class="level1 4 ">
          <a class="plus" href="https://www.wheresmypandit.com/hanuman-chalisa-paath.html"><span>Hanuman Chalisa Paath</span></a>
          </li>
          <li class="level1 5 ">
          <a class="plus" href="https://www.wheresmypandit.com/havan.html"><span>Havan</span></a>
          </li>
          <li class="level1 6 ">
          <a class="plus" href="https://www.wheresmypandit.com/kaal-bhairav-puja.html"><span>Kaal Bhairav Puja</span></a>
          </li>

          </ul>
          <ul class="level0" style="width: 33.3333333333%;left:33.3333333333%;">
          <li class="level1 8 ">
          <a class="plus" href="https://www.wheresmypandit.com/krishna-puja.html"><span>Krishna Puja</span></a>
          </li>
          <li class="level1 9 ">
          <a class="plus" href="https://www.wheresmypandit.com/maha-mrityunjaya-jaap.html"><span>Maha Mrityunjaya Jaap</span></a>
          </li>
          <li class="level1 10 ">
          <a class="plus" href="https://www.wheresmypandit.com/rudrabhishek-puja.html"><span>Rudrabhishek Puja</span></a>
          </li>


          <li class="level1 12 ">
          <a class="plus" href="https://www.wheresmypandit.com/satyanarayan-puja.html"><span>Satyanarayan Puja</span></a>
          </li>
          <li class="level1 13 ">
          <a class="plus" href="https://www.wheresmypandit.com/shiv-puja.html"><span>Shiv Puja</span></a>
          </li>
          <li class="level1 14 ">
          <a class="plus" href="https://www.wheresmypandit.com/sundarkand-paath.html"><span>Sundarkand Paath</span></a>
          </li>
          </ul>
          <ul class="level0" style="width: 33.3333333333%;left:66.6666666667%;">

          <li class="level1 19 last ">
          <a class="plus" href="https://www.wheresmypandit.com/vivah-puja.html"><span>Vivah Puja</span></a>
          </li>
          <li class="level1 16 ">
          <a class="plus" href="https://www.wheresmypandit.com/mata-ki-chowki.html"><span>Mata ki Chowki</span></a>
          </li>
          <li class="level1 7 ">
          <a class="plus" href="https://www.wheresmypandit.com/kali-puja.html"><span>Kali Puja</span></a>
          </li>

          </ul>
          </div>
          </li>
          <li class="level0 nav-2 level-top ">
          <a href="https://www.wheresmypandit.com/office-puja.html" class="level-top"><span>Office Puja</span></a>
          <div class="menu-white" style="display: none;left:11%;width: 14.2857142857%;">
          <ul class="level0" style="width: 100%;left:0%;">
          <li class="level1 1 first ">
          <a class="plus" href="https://www.wheresmypandit.com/bhoomi-puja.html"><span>Bhoomi Puja</span></a>
          </li>
          <li class="level1 2 ">
          <a class="plus" href="https://www.wheresmypandit.com/laxmi-puja.html"><span>Laxmi Puja</span></a>
          </li>
          <li class="level1 3 ">
          <a class="plus" href="https://www.wheresmypandit.com/saraswati-puja.html"><span>Sarswati Puja - Vasant Panchami</span></a>
          </li>
          <li class="level1 4 ">
          <a class="plus" href="https://www.wheresmypandit.com/satyanarayan-puja.html"><span>Satyanarayan Puja</span></a>
          </li>
          <li class="level1 5 ">
          <a class="plus" href="https://www.wheresmypandit.com/sundarkand-paath.html"><span>Sundarkand Paath</span></a>
          </li>
          <li class="level1 6 last ">
          <a class="plus" href="https://www.wheresmypandit.com/vishwakarma-puja.html"><span>Vishwakarma Puja</span></a>
          </li>
          </ul>
          </div>
          </li>
          <li class="level0 nav-3 level-top parent">
          <a href="https://www.wheresmypandit.com/festivals-puja.html" class="level-top plus"><span>Festivals Puja</span></a>
          <div class="menu-white plus" style="left: 22%; width: 14.2857%; display: none;">
          <ul class="level0" style="width: 100%; left: 0%; display: none;">
          <li class="level1 1 first ">
          <a class="plus" href="https://www.wheresmypandit.com/dhanteras-puja.html"><span>Dhanteras Puja</span></a>
          </li>
          <li class="level1 2 ">
          <a class="plus" href="https://www.wheresmypandit.com/durga-puja.html"><span>Durga Puja</span></a>
          </li>
          <li class="level1 3 ">
          <a class="plus" href="https://www.wheresmypandit.com/ganesh-chaturthi-puja.html"><span>Ganpati Puja / Ganesh Chaturthi</span></a>
          </li>
          <li class="level1 4 ">
          <a class="plus" href="https://www.wheresmypandit.com/laxmi-puja.html"><span>Laxmi Puja</span></a>
          </li>
          <li class="level1 5 ">
          <a class="plus" href="https://www.wheresmypandit.com/navratri-puja.html"><span>Navratri Puja</span></a>
          </li>
          <li class="level1 6 ">
          <a class="plus" href="https://www.wheresmypandit.com/gauri-puja.html"><span>Gauri Puja</span></a>
          </li>
          <li class="level1 7 last ">
          <a class="plus" href="https://www.wheresmypandit.com/maha-shivratri-puja.html"><span>Maha Shivratri Puja</span></a>
          </li>
          </ul>
          </div>
          </li>
          <li class="level0 nav-4 level-top"><a class="level-top" href="https://www.wheresmypandit.com/book-a-pandit"><span>Book a Pandit</span></a></li>
          <li class="level0 nav-4 level-top"><a href="https://www.wheresmypandit.com/organise-a-puja.html" class="level-top"><span>Organise a Puja</span></a></li>
          <li class="level0 nav-4 level-top"><a href="https://www.wheresmypandit.com/e-puja" class="level-top"><span>E-Puja</span></a></li>
          <li class="level0 nav-5 level-top parent">
          <a href="https://www.wheresmypandit.com/the-holy-store.html" class="level-top plus"><span>Religious Products</span></a>
          <div class="menu-white plus" style="left: 55%; width: 42.8571%; display: none;">
          <ul class="level0" style="width: 33.3333%; left: 0%; display: none;">
          <li class="level1 1 first"><a href="https://www.wheresmypandit.com/the-holy-store/necklace.html" class="plus"><span>Necklace</span></a></li>
          <li class="level1 2"><a href="https://www.wheresmypandit.com/the-holy-store/chattar.html" class="plus"><span>Chattar</span></a></li>
          <li class="level1 3"><a href="https://www.wheresmypandit.com/the-holy-store/jaldhari.html"class="plus"><span>Jaldhari</span></a></li>
          <li class="level1 4"><a href="https://www.wheresmypandit.com/the-holy-store/coins.html" class="plus"><span>Coins</span></a></li>
          <li class="level1 5"><a href="https://www.wheresmypandit.com/the-holy-store/trishul.html" class="plus"><span>Trishul</span></a></li>
          <li class="level1 6"><a href="https://www.wheresmypandit.com/the-holy-store/chopda.html" class="plus"><span>Chopda</span></a></li>
          <li class="level1 7"><a href="https://www.wheresmypandit.com/the-holy-store/agarbatti-stand.html" class="plus"><span>Agarbatti Stand</span></a></li>
          </ul>
          <ul class="level0" style="width: 33.3333%; left: 33.3333%; display: none;">
          <li class="level1 8"><a href="https://www.wheresmypandit.com/the-holy-store/diya.html" class="plus"><span>Diya</span></a></li>
          <li class="level1 9"><a href="https://www.wheresmypandit.com/the-holy-store/nariyal-kalash.html" class="plus"><span>Nariyal Kalash</span></a></li>
          <li class="level1 10"><a href="https://www.wheresmypandit.com/the-holy-store/katori.html" class="plus"><span>Katori</span></a></li>
          <li class="level1 11"><a href="https://www.wheresmypandit.com/the-holy-store/loti.html" class="plus"><span>Loti</span></a></li>
          <li class="level1 12"><a href="https://www.wheresmypandit.com/the-holy-store/fengshui-tortoise.html" class="plus"><span>Fengshui Tortoise</span></a></li>
          <li class="level1 13"><a href="https://www.wheresmypandit.com/the-holy-store/aarti-ki-thali.html" class="plus"><span>Aarti ki Thali</span></a></li>
          <li class="level1 14"><a href="https://www.wheresmypandit.com/the-holy-store/ghanti-bell.html" class="plus"><span>Ghanti / Bell</span></a></li>
          </ul>
          <ul class="level0" style="width: 33.3333%; left: 66.6667%; display: none;">
          <li class="level1 15"><a href="https://www.wheresmypandit.com/the-holy-store/rangoli.html" class="plus"><span>Rangoli</span></a></li>
          <li class="level1 16"><a href="https://www.wheresmypandit.com/the-holy-store/fresh-on-water.html" class="plus"><span>Fresh on water</span></a></li>
          <li class="level1 17"><a href="https://www.wheresmypandit.com/the-holy-store/wall-hanging.html" class="plus"><span>Wall Hanging</span></a></li>
          <li class="level1 18"><a href="https://www.wheresmypandit.com/the-holy-store/diwali-special.html" class="plus"><span>Diwali Special</span></a></li>
          <li class="level1 19"><a href="https://www.wheresmypandit.com/the-holy-store/poshak.html" class="plus"><span>Poshak</span></a></li>
          <li class="level1 20"><a href="https://www.wheresmypandit.com/the-holy-store/toran.html" class="plus"><span>Toran</span></a></li>
          <li class="level1 21 last"><a href="https://www.wheresmypandit.com/the-holy-store/view-more.html" class="plus"><span>View More</span></a></li>
          </ul>
          </div>
          </li>
          <li class="level0 nav-6 last level-top parent">
          <a href="/temples" class="level-top"><span>Tirtha</span></a>
          <div class="menu-white" style="display: none;left:83%;width: 14.2857142857%;">
          <ul class="level0" style="width: 100%;left:0%;">
          <li class="level1 1 first"><a class="plus" target="_blank" href="/temples"><span>Temples</span></a></li>
          <li class="level1 2 last"><a class="plus" target="_blank" href="/yatras"><span>Yatras</span></a></li>
          </ul>
          </div>
          </li>
          ';

         */


        $base_url = $this->getBaseUrl();
        $holly_stor_url = "https://www.wheresmypandit.com";

        $html = '

<li class="level0 1 first level-top parent">
   <a href="' . $base_url . 'puja-samagri" class="level-top"><span>Puja Samagri</span></a>
   <div class="menu-white" style="display: none;left:0px;width: 42.8571428571%;height:380px !important;">
      
                <ul class="level0" style="width: 33.3333333333%;left:0%;">

                    <li class="level1 1 first">
                        <a class="plus" href="' . $base_url . 'akhand-ramayan-paath"><span>Akhand Ramayan Paath</span></a>
                    </li>
                    <li class="level1 2 ">
                        <a class="plus" href="' . $base_url . 'bhoomi-puja"><span>Bhoomi Puja</span></a>
                    </li>
                    <li class="level1 3 ">
                        <a class="plus" href="' . $base_url . 'dhanteras-puja"><span>Dhanteras Puja</span></a>
                    </li>
                    <li class="level1 4 ">
                        <a class="plus" href="' . $base_url . 'durga-puja"><span>Durga Puja</span></a>
                    </li>
                    <li class="level1 5 ">
                        <a class="plus" href="' . $base_url . 'ganesh-chaturthi-puja"><span>Ganpati Puja / Ganesh Chaturthi</span></a>
                    </li>
                    <li class="level1 6 ">
                        <a class="plus" href="' . $base_url . 'gauri-puja"><span>Gauri Puja</span></a>
                    </li>
                    <li class="level1 7 ">
                        <a class="plus" href="' . $base_url . 'griha-pravesh-puja"><span>Griha Pravesh Puja</span></a>
                    </li>
                    <li class="level1 8 last">
                        <a class="plus" href="' . $base_url . 'hanuman-chalisa-paath"><span>Hanuman Chalisa Paath</span></a>
                    </li>

                </ul>
                <ul class="level0" style="width: 33.3333333333%;left:33.3333333333%;">

                    <li class="level1 9 first">
                        <a class="plus" href="' . $base_url . 'havan"><span>Havan</span></a>
                    </li>
                    <li class="level1 10 ">
                        <a class="plus" href="' . $base_url . 'kaal-bhairav-puja"><span>Kaal Bhairav Puja</span></a>
                    </li>
                    <li class="level1 11 ">
                        <a class="plus" href="' . $base_url . 'kali-puja"><span>Kali Puja</span></a>
                    </li> 
                    <li class="level1 12 ">
                        <a class="plus" href="' . $base_url . 'krishna-puja"><span>Krishna Puja</span></a>
                    </li>
                    <li class="level1 13">
                        <a class="plus" href="' . $base_url . 'laxmi-puja"><span>Laxmi Puja</span></a>
                    </li>
                    <li class="level1 14">
                        <a class="plus" href="' . $base_url . 'maha-mrityunjaya-jaap"><span>Maha Mrityunjaya Jaap</span></a>
                    </li>
                    <li class="level1 15">
                        <a class="plus" href="' . $base_url . 'maha-shivratri-puja"><span>Maha Shivratri Puja</span></a>
                    </li>
                    <li class="level1 16 last">
                        <a class="plus" href="' . $base_url . 'mata-ki-chowki"><span>Mata ki Chowki</span></a>
                    </li>
                    <li class="level1 17 first">
                        <a class="plus" href="' . $base_url . 'navratri-puja"><span>Navratri Puja</span></a>
                    </li>
                </ul>
                <ul class="level0" style="width: 33.3333333333%;left:66.6666666667%;">         


                    <li class="level1 18">
                        <a class="plus" href="' . $base_url . 'rudrabhishek-puja"><span>Rudrabhishek Puja</span></a>
                    </li> 
                    <li class="level1 19">
                        <a class="plus" href="' . $base_url . 'saraswati-puja"><span>Sarswati Puja - Vasant Panchami</span></a>
                    </li>
                    <li class="level1 20">
                        <a class="plus" href="' . $base_url . 'satyanarayan-puja"><span>Satyanarayan Puja</span></a>
                    </li>
                    <li class="level1 21">
                        <a class="plus" href="' . $base_url . 'shiv-puja"><span>Shiv Puja</span></a>
                    </li>
                    <li class="level1 22">
                        <a class="plus" href="' . $base_url . 'sundarkand-paath"><span>Sundarkand Paath</span></a>
                    </li>
                    <li class="level1 23">
                        <a class="plus" href="' . $base_url . 'vishwakarma-puja"><span>Vishwakarma Puja</span></a>
                    </li>
                    <li class="level1 24 last ">
                        <a class="plus" href="' . $base_url . 'vivah-puja"><span>Vivah Puja</span></a>
                    </li>

                </ul>       
     
       
       
   </div>
</li>

<li class="level0 nav-4 level-top"><a class="level-top" href="' . $base_url . 'book-a-pandit"><span>Book a Pandit</span></a></li>
<li class="level0 nav-4 level-top"><a href="' . $base_url . 'organise-a-puja" class="level-top"><span>Organise a Puja</span></a></li>
<li class="level0 nav-4 level-top"><a href="' . $base_url . 'e-puja" class="level-top"><span>E-Puja</span></a></li>
<li class="level0 nav-5 level-top parent">
   <a href="' . $base_url . 'religious-products" class="level-top plus"><span>Religious Products</span></a>
   <div class="menu-white plus" style="left: 48%; width: 42.8571%; height:80px; display: none;">
      <ul class="level0" style="width: 33.3333%; left: 0%; display: none;">
      
    <li class="level1 1 first"><a href="' . $base_url . 'aarti-ki-thali" class="plus"><span>Aarti ki Thali</span></a></li>
    <li class="level1 2"><a href="' . $base_url . 'aasan" class="plus"><span>Aasan</span></a></li>
    
    <li class="level1 4"><a href="' . $base_url . 'candles-and-diyas" class="plus"><span>Candles & Diyas</span></a></li>
    <li class="level1 5"><a href="' . $base_url . 'chattar" class="plus"><span>Chattar</span></a></li>   
    
    <li class="level1 8"><a href="' . $base_url . 'decorative-items" class="plus"><span>Decorative Items</span></a></li>
    
    <li class="level1 9"><a href="' . $base_url . 'fengshui" class="plus"><span>Fengshui</span></a></li>
        
    <li class="level1 9"><a href="' . $base_url . 'idol" class="plus"><span>Idols</span></a></li>
    
    
    
</ul> 
<ul class="level0" style="width: 33.3333%; left: 33.3333%; display: none;">
    
    
    <li class="level1 15"><a href="' . $base_url . 'maala-and-beads" class="plus"><span>Mala and Beads</span></a></li>
    <li class="level1 10"><a href="' . $base_url . 'mandir" class="plus"><span>Mandir</span></a></li>
    <li class="level1 16"><a href="' . $base_url . 'ornaments" class="plus"><span>Ornaments</span></a></li>
    <li class="level1 17"><a href="' . $base_url . 'poshak" class="plus"><span>Poshak</span></a></li>
    <li class="level1 14"><a href="' . $base_url . 'puja-essentials" class="plus"><span>Puja Essentials</span></a></li>
    <li class="level1 18"><a href="' . $base_url . 'rangoli" class="plus"><span>Rangoli</span></a></li>
    <li class="level1 9"><a href="' . $base_url . 'religious-books" class="plus"><span>Religious Books</span></a></li>
    
    
</ul>
<ul class="level0" style="width: 33.3333%; left: 66.6667%; display: none;">


    
    <li class="level1 19"><a href="' . $base_url . 'shubh-labh" class="plus"><span>Shubh Labh</span></a></li>
    <li class="level1 20"><a href="' . $base_url . 'sinhasan-palna" class="plus"><span>Sinhasan & Palna</span></a></li>    
    <li class="level1 21"><a href="' . $base_url . 'toran" class="plus"><span>Toran</span></a></li>
    <li class="level1 22"><a href="' . $base_url . 'trishul" class="plus"><span>Trishul</span></a></li>
    <li class="level1 23"><a href="' . $base_url . 'wall-hanging" class="plus"><span>Wall Hanging</span></a></li>    
    <li class="level1 24 last"><a href="' . $base_url . 'view-more" class="plus"><span>View More</span></a></li>

</ul>
   </div>
</li>
<li class="level0 nav-6 last level-top parent">
   <a href="' . $base_url . 'temples" class="level-top"><span>Tirtha</span></a>
   <div class="menu-white" style="display: none;left:64%;width: 14.2857142857%;height:150px !important;min-height:50px !important">
      <ul class="level0" style="width: 100%;left:0%;">
         <li class="level1 1 first"><a class="plus" target="_blank" href="' . $base_url . 'temples"><span>Temples</span></a></li>
         <li class="level1 2 last"><a class="plus" target="_blank" href="' . $base_url . 'yatras"><span>Yatras</span></a></li>
        <li class="level1 2 last"><a class="plus" target="_blank" href="' . $base_url . 'yatra-registration-form"><span>Book Your Package</span></a></li>
             
      </ul>
   </div>
</li>


<li class="level0 nav-6 last level-top parent">
   <a href="' . $base_url . 'indian-wedding-season" class="level-top"><span>Indian Wedding Season</span></a>
   <div class="menu-white" style="display: none;left:76%;width: 23%;height:175px !important;min-height:50px !important">
      <ul class="level0" style="width: 100%;left:0%;">
        <li class="level1 1 first"><a class="plus" href="' . $base_url . 'vivah-marriage-puja"><span>Book Vivah Puja</span></a></li>       
        <li class="level1 2 last"><a class="plus" href="' . $base_url . 'decorative-items-for-wedding"><span>Vivah Products Store</span></a></li>
        <li class="level1 2 last"><a class="plus" href="' . $base_url . 'indian-hindu-wedding"><span>Indian Wedding Information</span></a></li>
      </ul>
   </div>
</li>    





';

        /*
          01 - Dec - 2015
         <li class="level0 nav-4 level-top"><a class="level-top" href="' . $base_url . 'indian-wedding-season"><span>Indian Wedding Season</span></a></li> 
          
         */
        
        /*         
                  
          10 - Nov - 2015
          <li class="level0 nav-6 last level-top parent">
          <a href="' . $base_url . 'divine-diwali" class="level-top"><span>Divine Diwali</span></a>
          <div class="menu-white" style="display: none;left:76%;width: 18%;height:120px !important;min-height:50px !important">
          <ul class="level0" style="width: 100%;left:0%;">
          <li class="level1 1 first"><a class="plus" target="_blank" href="' . $base_url . 'diwali-laxmi-puja-vidhi"><span>Diwali Laxmi Puja Vidhi</span></a></li>
          <li class="level1 2 last"><a class="plus" target="_blank" href="' . $base_url . 'dhanteras-puja-vidhi"><span>Dhanteras Puja Vidhi</span></a></li>
          <li class="level1 2 last"><a class="plus" target="_blank" href="' . $base_url . 'divine-diwali-product-offers"><span>Puja Products</span></a></li>
          </ul>
          </div>
          </li>

         */

        /*
          03 - Nov - 2015
          <li class="level0 nav-4 level-top">
          <a class="level-top" href="' . $base_url . 'divine-diwali"><span>Divine Diwali</span></a>
          </li>

         */

        /*
          <li class="level1 9"><a href="' . $base_url . 'fengshui-tortoise" class="plus"><span>Fengshui Tortoise</span></a></li>
          <li class="level1 6"><a href="' . $base_url . 'chopda" class="plus"><span>Chopda</span></a></li>
          <li class="level1 7"><a href="' . $base_url . 'coins" class="plus"><span>Coins</span></a></li>
          <li class="level1 3"><a href="' . $base_url . 'agarbatti-stand" class="plus"><span>Agarbatti Stand</span></a></li>
          <li class="level1 10"><a href="' . $base_url . 'ghanti-bell" class="plus"><span>Ghanti / Bell</span></a></li>
          <li class="level1 12"><a href="' . $base_url . 'jaldhari"class="plus"><span>Jaldhari</span></a></li>
          <li class="level1 13"><a href="' . $base_url . 'katori" Class="plus"><span>Katori</span></a></li>

         */


//<span style="background-color: rgb(204, 51, 51); color: rgb(255, 255, 255); font-size: 14px; font-weight: normal; line-height: 18px; margin-left: 10px; padding: 2px 4px; position: absolute;  text-shadow: none; text-transform: none; top: -15px; transition: color 450ms ease-in-out 0s, background-color 450ms ease-in-out 0s; width: 50px;" class="">New</span>
//Buy Eco Ganpati   Url : eco-friendly-ganesh-murti
        return $html;
    }

    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item) {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }

        return $html;
    }

    /**
     * Returns array of menu item's attributes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemAttributes(Varien_Data_Tree_Node $item) {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        $attributes = array(
            'class' => implode(' ', $menuItemClasses)
        );

        return $attributes;
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item) {
        $classes = array();

        $classes[] = 'level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren()) {
            $classes[] = 'parent';
        }

        return $classes;
    }

    /**
     * Retrieve cache key data
     *
     * @return array
     */
    public function getCacheKeyInfo() {
        $shortCacheId = array(
            'TOPMENU',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrentEntityKey()
        );
        $cacheId = $shortCacheId;

        $shortCacheId = array_values($shortCacheId);
        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['entity_key'] = $this->getCurrentEntityKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    /**
     * Retrieve current entity key
     *
     * @return int|string
     */
    public function getCurrentEntityKey() {
        if (null === $this->_currentEntityKey) {
            $this->_currentEntityKey = Mage::registry('current_entity_key') ? Mage::registry('current_entity_key') : Mage::app()->getStore()->getRootCategoryId();
        }
        return $this->_currentEntityKey;
    }

}

/*
 
 <ul class="level0" style="width: 33.3333333333%;left:0%;">
         <li class="level1 1 first ">
            <a class="plus" href="' . $base_url . 'laxmi-puja.html"><span>Laxmi Puja</span></a>
         </li>
         <li class="level1 2 ">
            <a class="plus" href="' . $base_url . 'akhand-ramayan-paath.html"><span>Akhand Ramayan Paath</span></a>
         </li>
         <li class="level1 3 ">
            <a class="plus" href="' . $base_url . 'griha-pravesh-puja.html"><span>Griha Pravesh Puja</span></a>
         </li>
         <li class="level1 4 ">
            <a class="plus" href="' . $base_url . 'hanuman-chalisa-paath.html"><span>Hanuman Chalisa Paath</span></a>
         </li>
         <li class="level1 5 ">
            <a class="plus" href="' . $base_url . 'havan.html"><span>Havan</span></a>
         </li>
         <li class="level1 6 ">
            <a class="plus" href="' . $base_url . 'kaal-bhairav-puja.html"><span>Kaal Bhairav Puja</span></a>
         </li>
         
         
         
         <li class="level1 16 first ">
            <a class="plus" href="' . $base_url . 'bhoomi-puja.html"><span>Bhoomi Puja</span></a>
         </li>
         
         <li class="level1 17 ">
            <a class="plus" href="' . $base_url . 'saraswati-puja.html"><span>Sarswati Puja - Vasant Panchami</span></a>
         </li>
         

      </ul>
      <ul class="level0" style="width: 33.3333333333%;left:33.3333333333%;">
         <li class="level1 7 ">
            <a class="plus" href="' . $base_url . 'krishna-puja.html"><span>Krishna Puja</span></a>
         </li>
         <li class="level1 8 ">
            <a class="plus" href="' . $base_url . 'maha-mrityunjaya-jaap.html"><span>Maha Mrityunjaya Jaap</span></a>
         </li>
         <li class="level1 9 ">
            <a class="plus" href="' . $base_url . 'rudrabhishek-puja.html"><span>Rudrabhishek Puja</span></a>
         </li> 
         
                  
         <li class="level1 10 ">
            <a class="plus" href="' . $base_url . 'satyanarayan-puja.html"><span>Satyanarayan Puja</span></a>
         </li>
         <li class="level1 11 ">
            <a class="plus" href="' . $base_url . 'shiv-puja.html"><span>Shiv Puja</span></a>
         </li>
         <li class="level1 12 ">
            <a class="plus" href="' . $base_url . 'sundarkand-paath.html"><span>Sundarkand Paath</span></a>
         </li>
         
         <li class="level1 25 ">
            <a class="plus" href="' . $base_url . 'gauri-puja.html"><span>Gauri Puja</span></a>
         </li>
         <li class="level1 26 last ">
            <a class="plus" href="' . $base_url . 'maha-shivratri-puja.html"><span>Maha Shivratri Puja</span></a>
         </li>         

         <li class="level1 20 last ">
            <a class="plus" href="' . $base_url . 'vishwakarma-puja.html"><span>Vishwakarma Puja</span></a>
         </li>
         
      </ul>
      <ul class="level0" style="width: 33.3333333333%;left:66.6666666667%;">         
         
        <li class="level1 13 last ">
            <a class="plus" href="' . $base_url . 'vivah-puja.html"><span>Vivah Puja</span></a>
         </li>
         <li class="level1 14 ">
            <a class="plus" href="' . $base_url . 'mata-ki-chowki.html"><span>Mata ki Chowki</span></a>
         </li>
         <li class="level1 15 ">
            <a class="plus" href="' . $base_url . 'kali-puja.html"><span>Kali Puja</span></a>
         </li> 
         
                  <li class="level1 21 first ">
            <a class="plus" href="' . $base_url . 'dhanteras-puja.html"><span>Dhanteras Puja</span></a>
         </li>
         <li class="level1 22 ">
            <a class="plus" href="' . $base_url . 'durga-puja.html"><span>Durga Puja</span></a>
         </li>
         <li class="level1 23 ">
            <a class="plus" href="' . $base_url . 'ganesh-chaturthi-puja.html"><span>Ganpati Puja / Ganesh Chaturthi</span></a>
         </li>
         
         <li class="level1 24 ">
            <a class="plus" href="' . $base_url . 'navratri-puja.html"><span>Navratri Puja</span></a>
         </li>

         <li class="level1 6 last ">
            <a class="plus" href="' . $base_url . 'vishwakarma-puja.html"><span>Vishwakarma Puja</span></a>
         </li>
         
      </ul>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * Holly store
 * <ul class="level0" style="width: 33.3333%; left: 0%; display: none;">
         <li class="level1 1 first"><a href="' . $base_url . 'the-holy-store/necklace.html" class="plus"><span>Mala and Beads</span></a></li>
         <li class="level1 2"><a href="' . $base_url . 'the-holy-store/chattar.html" class="plus"><span>Chattar</span></a></li>
         <li class="level1 3"><a href="' . $base_url . 'the-holy-store/jaldhari.html"class="plus"><span>Jaldhari</span></a></li>
         <li class="level1 4"><a href="' . $base_url . 'the-holy-store/coins.html" class="plus"><span>Coins</span></a></li>
         <li class="level1 5"><a href="' . $base_url . 'the-holy-store/trishul.html" class="plus"><span>Trishul</span></a></li>
         <li class="level1 6"><a href="' . $base_url . 'the-holy-store/chopda.html" class="plus"><span>Chopda</span></a></li>
         <li class="level1 7"><a href="' . $base_url . 'the-holy-store/agarbatti-stand.html" class="plus"><span>Agarbatti Stand</span></a></li>
      </ul>
      <ul class="level0" style="width: 33.3333%; left: 33.3333%; display: none;">
         <li class="level1 8"><a href="' . $base_url . 'the-holy-store/diya.html" class="plus"><span>Diya</span></a></li>
         <li class="level1 9"><a href="' . $base_url . 'the-holy-store/nariyal-kalash.html" class="plus"><span>Nariyal Kalash</span></a></li>
         <li class="level1 10"><a href="' . $base_url . 'the-holy-store/katori.html" class="plus"><span>Katori</span></a></li>
         <li class="level1 11"><a href="' . $base_url . 'the-holy-store/loti.html" class="plus"><span>Loti</span></a></li>
         <li class="level1 12"><a href="' . $base_url . 'the-holy-store/fengshui-tortoise.html" class="plus"><span>Fengshui Tortoise</span></a></li>
         <li class="level1 13"><a href="' . $base_url . 'the-holy-store/aarti-ki-thali.html" class="plus"><span>Aarti ki Thali</span></a></li>
         <li class="level1 14"><a href="' . $base_url . 'the-holy-store/ghanti-bell.html" class="plus"><span>Ghanti / Bell</span></a></li>
      </ul>
      <ul class="level0" style="width: 33.3333%; left: 66.6667%; display: none;">
         <li class="level1 15"><a href="' . $base_url . 'the-holy-store/rangoli.html" class="plus"><span>Rangoli</span></a></li>
         <li class="level1 16"><a href="' . $base_url . 'the-holy-store/fresh-on-water.html" class="plus"><span>Fresh on water</span></a></li>
         <li class="level1 17"><a href="' . $base_url . 'the-holy-store/wall-hanging.html" class="plus"><span>Wall Hanging</span></a></li>
         <li class="level1 18"><a href="' . $base_url . 'the-holy-store/diwali-special.html" class="plus"><span>Diwali Special</span></a></li>
         <li class="level1 19"><a href="' . $base_url . 'the-holy-store/poshak.html" class="plus"><span>Poshak</span></a></li>
         <li class="level1 20"><a href="' . $base_url . 'the-holy-store/toran.html" class="plus"><span>Toran</span></a></li>
         <li class="level1 21 last"><a href="' . $base_url . 'the-holy-store/view-more.html" class="plus"><span>View More</span></a></li>
      </ul>
 
 */