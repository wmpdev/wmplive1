<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Page
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Top menu block
 *
 * @category    Mage
 * @package     Mage_Page
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Page_Block_Html_Topmenu extends Mage_Core_Block_Template {

    /**
     * Top menu data tree
     *
     * @var Varien_Data_Tree_Node
     */
    protected $_menu;

    /**
     * Current entity key
     *
     * @var string|int
     */
    protected $_currentEntityKey;
    protected $_ulWidth;

    /**
     * Init top menu tree structure
     */
    public function _construct() {
        $this->_menu = new Varien_Data_Tree_Node(array(), 'root', new Varien_Data_Tree());

        $this->addData(array(
            'cache_lifetime' => false,
        ));
    }

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '') {
        Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array(
            'menu' => $this->_menu,
            'block' => $this
        ));

        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);

        if ($renderer = $this->getChild('catalog.topnav.renderer')) {
            $renderer->setMenuTree($this->_menu)->setChildrenWrapClass($childrenWrapClass);
            $html = $renderer->toHtml();
        } else {
            $html = $this->_getHtml($this->_menu, $childrenWrapClass);
        }

        Mage::dispatchEvent('page_block_html_topmenu_gethtml_after', array(
            'menu' => $this->_menu,
            'html' => $html
        ));

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass) {
        $html = '';

    /*    
          $children = $menuTree->getChildren();
          $parentLevel = $menuTree->getLevel();
          $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

          $counter = 1;
          $childrenCount = $children->count();
          $perColumnMenuItems = 7;
          $totalAllowedColumns = 7;
          $current0li = 0;
          $lefts = array('0', '155', '320', '511','865');

          foreach ($children as $child) {

          $child->setLevel($childLevel);
          $child->setIsFirst($counter == 1);
          $child->setIsLast($counter == $childrenCount);
          $child->setPositionClass($itemPositionClassPrefix . $counter);

          $outermostClassCode = '';
          $outermostClass = $menuTree->getOutermostClass();

          if ($childLevel == 0 && $outermostClass) {
          $outermostClassCode = ' class="' . $outermostClass . '" ';
          $child->setClass($outermostClass);
          }

          $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
          $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>'
          . $this->escapeHtml($child->getName()) . '</span></a>';

          if ($child->hasChildren()) {
          if (!empty($childrenWrapClass)) {
          $html .= '<div class="' . $childrenWrapClass . '">';
          }
          if ($childLevel == 0) {
          $currentUl = 0;
          if ($childLevel == 0 && $child->getData('image') != '' && $child->getData('image')) {
          $columns = ceil($child->getChildren()->count() / $perColumnMenuItems) + 1;
          } else {
          $columns = ceil($child->getChildren()->count() / $perColumnMenuItems);
          }
          $divWidth = ( $columns * 100 ) / $totalAllowedColumns;
          $this->_ulWidth = (100 / $columns);
          //echo " ? " . $divWidth . " :: " . $this->_ulWidth . " :: " . ($currentUl*$this->_ulWidth);
          $parentPositionClass = $menuTree->getPositionClass();
          $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

          $html .= '<div class="menu-white" style="display: none;left:' . $lefts[$current0li] . 'px;width: ' . $divWidth . '%;">';
          $current0li++;
          }
          if ($childLevel == 1) {
          $html .= '<ul class="level' . $childLevel . '">';
          } else {
          $html .= '<ul class="level' . $childLevel . '" style="width: ' . $this->_ulWidth . '%;left:' . ($currentUl*$this->_ulWidth). '%;">';
          }

          $html .= $this->_getHtml($child, $childrenWrapClass);
          $html .= '</ul>';

          $parentLevels = $child->getLevel();
          if ($childLevel == 0 && $child->getData('image') != '' && $child->getData('image')) {
          $html .= '<ul class="level0" style="width: ' . $this->_ulWidth . '%;left:' . (($columns-1)*$this->_ulWidth). '%;"><li style="width:100%"><img src="' . $child->getData('image') . '" style="padding:10px;" alt=""></li></ul></div>';
          }

          if (!empty($childrenWrapClass)) {
          $html .= '</div>';
          }
          }
          $html .= '</li>';
          if ($childLevel == 1 && $counter > 0 && $counter % $perColumnMenuItems == 0) {
          if ($counter != $childrenCount) {
          $html .= '</ul>';
          $currentUl++;
          //echo " :: (" . $this->_ulWidth . ") " . $currentUl*$this->_ulWidth;
          $html .= '<ul class="level' . ($childLevel-1) . '" style="width: ' . $this->_ulWidth . '%;left:' . ($currentUl*$this->_ulWidth). '%;">';
          }
          }

          $counter++;
          }

       */  
        $base_url = $this->getBaseUrl('');
        $holly_stor_url = "https://www.wheresmypandit.com";
        $templeBaseUrl = $base_url.'temples';
// 		$temples = Mage::getSingleton('lionleap_temples/temple')
// 				   ->getCollection()
// 				   ->addFilter('status','active');
// 		$templeMenu = "";
// 		$liCount = 0;
// 		$liClose = false;
// 		$rows = 35;
// 		foreach ($temples as $temple){
// 			if(($liCount%$rows)==0){
// 				$templeMenu = $templeMenu.'<ul class="dropMenu">';
// 				$liClose = true;
// 			}
// 			$templeMenu = $templeMenu.'<li><a href="'.$templeBaseUrl.$temple->getUrlkey().'">'.$temple->getName().'</a></li>';
			
			
// 			if(($liCount%$rows)==($rows-1)){
// 				$templeMenu = $templeMenu.'</ul>';
// 			}
// 			$liCount++;
// 		}
        $templeMenu = '';
        $templeMenu = $templeMenu.'<ul class="dropMenu"><li><a target="_blank" href="'.$templeBaseUrl.'">Temples</a></li>';
        $templeMenu = $templeMenu.'<li><a target="_blank" href="'.$base_url.'yatras">Yatras</a></li>';
        $templeMenu = $templeMenu.'<li><a target="_blank" href="'.$base_url.'yatra-registration-form">Book Your Package</a></li></ul>';
        
        $religiousCat = $this->getCategoryMenuItems(Mage::getModel('catalog/category')->loadByAttribute('name','Religious Products')->getId());
        $pujas = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('puja-main-menu-items')->toHtml();//$this->getPujaCategoryMenuItems(Mage::getModel('catalog/category')->loadByAttribute('name','All Pujas')->getId());
        
        
        $html = ' 


            <li class="dropLink"><a>Puja</a>
            
            <!-- Dropdown Starts -->
            <div class="dropContainer puja-dropdown" style="width:auto;"> 
        <div class="tab-content-temp">
                  <div role="tabpanel" class="tab-pane fade in active" id="menuTabPuja1">
                  '.$pujas.'
          </div> 
                </div>
            </div>
            <!-- Dropdown Ends -->

          </li> 
          <li><a href="'.$base_url.'book-a-pandit">Book a Pandit</a></li> 
          <li><a href="'.$base_url.'organise-a-puja">Organise a Puja</a></li> 
          
          <li class="dropLink"><a>Religious Products</a>
          
            <!-- Dropdown Starts -->
            <div class="dropContainer"> 

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">'.$religiousCat[0].'
                   
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  
					'.$religiousCat[1].'
                  
                </div>
 
            </div>
            <!-- Dropdown Ends -->

          </li> 
          <li><a href="'.$base_url.'e-puja">E-Puja</a></li> 
          <li class="dropLink"><a>Yatra & Tirth</a>
            
            <!-- Dropdown Starts -->
            <div class="dropContainer yatra-dropdown" style="width:auto;left: 700px;"> 

                <!-- Tab panes -->
                <div class="tab-content-temp">
                  <div role="tabpanel" class="tab-pane fade in active" id="menuTabTemple1">
						'.$templeMenu.'
                  </div> 
                </div>
 
            </div>
            <!-- Dropdown Ends -->

          </li> 
          <!-- <li><a href="#">Yatra & Tirth</a></li> -->

         
          <!-- <li><a href="#">Puja</a></li>   -->
          <li><a href="'.$base_url.'hssf">HSSF</a></li>
';

        return $html;
    }

    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item) {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue) . '"';
        }

        return $html;
    }

    /**
     * Returns array of menu item's attributes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemAttributes(Varien_Data_Tree_Node $item) {
        $menuItemClasses = $this->_getMenuItemClasses($item);
        $attributes = array(
            'class' => implode(' ', $menuItemClasses)
        );

        return $attributes;
    }

    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item) {
        $classes = array();

        $classes[] = 'level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren()) {
            $classes[] = 'parent';
        }

        return $classes;
    }

    /**
     * Retrieve cache key data
     *
     * @return array
     */
    public function getCacheKeyInfo() {
        $shortCacheId = array(
            'TOPMENU',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrentEntityKey()
        );
        $cacheId = $shortCacheId;

        $shortCacheId = array_values($shortCacheId);
        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['entity_key'] = $this->getCurrentEntityKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    /**
     * Retrieve current entity key
     *
     * @return int|string
     */
    public function getCurrentEntityKey() {
        if (null === $this->_currentEntityKey) {
            $this->_currentEntityKey = Mage::registry('current_entity_key') ? Mage::registry('current_entity_key') : Mage::app()->getStore()->getRootCategoryId();
        }
        return $this->_currentEntityKey;
    }
    public function getPujaCategoryMenuItems($catId){
    	$id=$catId;
    	$collection = Mage::getResourceModel('catalog/category_collection')
    	->addAttributeToSelect('*')
    	->addAttributeToFilter('is_active', array('in' => array(1)))
    	->addAttributeToFilter('parent_id', $id);
    	$religiousMainCategory = "";
    	$categoryLink = "";
    	$subCategoryLink = "";
    	$religiousSubCategory = "";
    	$i = "active";
    	foreach ($collection as $category):
    	$religiousSubCategory = $religiousSubCategory.'<ul class="dropMenu">';
    	if($category->getChildrenCount()){
    		$collectionChildren = Mage::getResourceModel('catalog/category_collection')
    		->addAttributeToSelect('*')
    		->addAttributeToFilter('is_active', array('in' => array(1)))
    		->addAttributeToFilter('parent_id', $category->getId());
    		 
    		foreach ($collectionChildren as $childCategory){
    			$religiousSubCategory = $religiousSubCategory.'<li><a href="'.rtrim(Mage::getUrl($childCategory->getUrlKey()),"/").'">'.$childCategory->getName().'</a></li>';
    		}
    		 
    	} else {
    		$religiousSubCategory = $religiousSubCategory.'<li><a href="'.rtrim(Mage::getUrl($category->getUrlKey()),"/").'">'.$category->getName().'</a></li>';
    	}
    	$religiousSubCategory = $religiousSubCategory."</ul>";
    	$i = "";
    	endforeach;
    	//echo $religiousSubCategory;
    	return $religiousSubCategory;
    }
    public function getCategoryMenuItems($catId){
    	$id=$catId;
    	$collection = Mage::getResourceModel('catalog/category_collection')
    	->addAttributeToSelect('*')
    	->addAttributeToFilter('is_active', array('in' => array(1)))
    	->addAttributeToFilter('parent_id', $id);
    	$religiousMainCategory = "";
    	$categoryLink = "";
    	$subCategoryLink = "";
    	$religiousSubCategory = "";
    	$i = "";
    	$defaultActive = "";
    	$activeCategory = "";
    	$activeFirst = "";
    	$activeSubCategory = "";
    	if(Mage::registry('current_category')){
    		$activeSubCategory =  Mage::registry('current_category')->getId();
    	}
    	$columnCount = 0;
    	foreach ($collection as $category):
    	if(!$activeFirst && !$activeSubCategory){
    		$activeFirst = $category->getId();
    	}
    	$religiousSubCategory = $religiousSubCategory.'<div role="tabpanel" class="tab-pane fade in " id="menuTab'.$category->getId().'">';
    	if($category->getChildrenCount()){
    			
    		$collectionChildren = Mage::getResourceModel('catalog/category_collection')
    		->addAttributeToSelect('*')
    		->addAttributeToFilter('is_active', array('in' => array(1)))
    		->addAttributeToFilter('parent_id', $category->getId());
    		$ulCount = 0;
    		
    		foreach ($collectionChildren as $childCategory){
    			$religiousSubCategory = $religiousSubCategory.'<ul class="dropMenu">';
    			$religiousSubCategory = $religiousSubCategory.'<li class="dropHeadingMain"><a id="menu-link-'.$childCategory->getId().'" href="'.rtrim(Mage::getUrl($childCategory->getUrlKey()),"/").'">'.$childCategory->getName().'</a></li>';
    			if($childCategory->getChildrenCount()){
    				$collectionChildrenSub = Mage::getResourceModel('catalog/category_collection')
    				->addAttributeToSelect('*')
    				->addAttributeToFilter('is_active', array('in' => array(1)))
    				->addAttributeToFilter('parent_id', $childCategory->getId());

            /*
              @author : preetesh jain
              @Description : added last child menu check condition to add custom class reuqied by designer
            */
            $sub_menu_child_count = 1;


            foreach ($collectionChildrenSub as $subSubCategory){
              if($sub_menu_child_count != count($collectionChildrenSub))
              { 
    					 $religiousSubCategory = $religiousSubCategory.'<li><a id="menu-link-'.$subSubCategory->getId().'" href="'.rtrim(Mage::getUrl($subSubCategory->getUrlKey()),"/").'">'.$subSubCategory->getName().'</a></li>';
    					}else{
                 $religiousSubCategory = $religiousSubCategory.'<li><a id="menu-link-'.$subSubCategory->getId().'" href="'.rtrim(Mage::getUrl($subSubCategory->getUrlKey()),"/").'">'.$subSubCategory->getName().'</a></li>';
              }
              $sub_menu_child_count++;
              if($activeSubCategory == $subSubCategory->getId() || $activeSubCategory == $childCategory->getId() || $activeSubCategory == $category->getId()){
    						$activeCategory = $category->getId();
    					}
    				}
    				 
    			}
    			if($activeSubCategory == $childCategory->getId() || $activeSubCategory == $category->getId()){
    				$activeCategory = $category->getId();
    			}
    			$religiousSubCategory = $religiousSubCategory.'</ul>';
    		}
    	
    	} else {
    		$religiousSubCategory = $religiousSubCategory.'<ul class="dropMenu">';
    		$religiousSubCategory = $religiousSubCategory.'<li class="dropHeadingMain"><a id="menu-link-'.$category->getId().'" href="'.rtrim(Mage::getUrl($category->getUrlKey()),"/").'">'.$category->getName().'</a></li>';
    		$religiousSubCategory = $religiousSubCategory.'</ul>';
    	}
    	if($activeSubCategory == $category->getId()){
    		$activeCategory = $category->getId();
    	}
    	$religiousSubCategory = $religiousSubCategory.'</div>';
    	$religiousMainCategory = $religiousMainCategory.'<li id="mainmenu'.$category->getId().'" role="presentation"><a id="menu-link-'.$category->getId().'" href="#menuTab'.$category->getId().'"  aria-controls="menuTab'.$category->getId().'" role="tab" data-toggle="tab">'.$category->getName().'</a></li>';
    	endforeach;
    	if(!$activeCategory){
    		$activeCategory = $activeFirst;
    	}
    	$religiousMainCategory = $religiousMainCategory.'<input type="hidden" id="activeCathetogy" value="'.$activeCategory.'"/>';
    	$religiousMainCategory = $religiousMainCategory.'<input type="hidden" id="activeCathetogyLint" value="'.$activeSubCategory.'"/>';
    	return array($religiousMainCategory,$religiousSubCategory);
    }
    
    public function getCategoryMenuItemsMobile($catId){
    	$id=$catId;
    	$collection = Mage::getResourceModel('catalog/category_collection')
    	->addAttributeToSelect('*')
    	->addAttributeToFilter('is_active', array('in' => array(1)))
    	->addAttributeToFilter('parent_id', $id);
    	$religiousMainCategory = "";
    	$categoryLink = "";
    	$subCategoryLink = "";
    	$religiousSubCategory = "";
    	$i = "active";
    	foreach ($collection as $category):
    	$religiousSubCategory = "";
    	if($category->getChildrenCount()){
    		$religiousSubCategory = '<ul class="dl-submenu">';
    		$collectionChildren = Mage::getResourceModel('catalog/category_collection')
    		->addAttributeToSelect('*')
    		->addAttributeToFilter('is_active', array('in' => array(1)))
    		->addAttributeToFilter('parent_id', $category->getId());
    		 
    		foreach ($collectionChildren as $childCategory){
    			$religiousSubCategory = $religiousSubCategory.'<li><a href="'.rtrim(Mage::getUrl($childCategory->getUrlKey()),"/").'">'.$childCategory->getName().'</a>';
    			if($childCategory->getChildrenCount()){
    				//$religiousSubCategory = $religiousSubCategory.'<ul class="dl-submenu"><li><a href="'.Mage::getUrl($category->getUrlKey()).'">'.$category->getName().'</a>';
    				$collectionChildrenSub = Mage::getResourceModel('catalog/category_collection')
    				->addAttributeToSelect('*')
    				->addAttributeToFilter('is_active', array('in' => array(1)))
    				->addAttributeToFilter('parent_id', $childCategory->getId());
    				$religiousSubCategory = $religiousSubCategory.'<ul class="dl-submenu"><li><a href="'.rtrim(Mage::getUrl($childCategory->getUrlKey()),"/").'">'.$childCategory->getName().'</a>';
    				foreach ($collectionChildrenSub as $subSubCategory){
    					$religiousSubCategory = $religiousSubCategory.'<li><a href="'.rtrim(Mage::getUrl($subSubCategory->getUrlKey()),"/").'">'.$subSubCategory->getName().'</a></li>';
    				}
    				$religiousSubCategory = $religiousSubCategory.'</li></ul>';
    			} else {
    				$religiousSubCategory = $religiousSubCategory.'<ul class="dl-submenu"><li><a href="'.rtrim(Mage::getUrl($childCategory->getUrlKey()),"/").'">'.$childCategory->getName().'</a></li></ul>';
    			}
    			$religiousSubCategory = $religiousSubCategory."</li>";
    		}
    		$religiousSubCategory = $religiousSubCategory.'</ul>';
    	} else {
    		$religiousSubCategory = $religiousSubCategory.'<ul class="dl-submenu"><li><a href="'.rtrim(Mage::getUrl($category->getUrlKey()),"/").'">'.$category->getName().'</a></li></ul>';
    	}
    	
    	$religiousMainCategory = $religiousMainCategory.'<li><a href="#menuTab'.$category->getId().'">'.$category->getName().'</a> '.$religiousSubCategory.'</li>';
    	$i = "";
    	endforeach;
    	return $religiousMainCategory;
    }
	
    public function getMoboleHtml(){
    	$base_url = $this->getBaseUrl('');
    	$holly_stor_url = "https://www.wheresmypandit.com";
    	$templeBaseUrl = $base_url.'temples';
//     	$temples = Mage::getSingleton('lionleap_temples/temple')
//     	->getCollection()
//     	->addFilter('status','active');
//     	$templeMenu = "";
//     	$liCount = 0;
//     	$liClose = false;
//     	$rows = 35;
//     	foreach ($temples as $temple){
    		
//     		$templeMenu = $templeMenu.'<li><a href="'.$templeBaseUrl.$temple->getUrlkey().'">'.$temple->getName().'</a></li>';
    			
    		
//     		$liCount++;
//     	}
    	$templeMenu = '';
    	$templeMenu = $templeMenu.'<li><a target="_blank" href="'.$templeBaseUrl.'">Temples</a></li>';
    	$templeMenu = $templeMenu.'<li><a target="_blank" href="'.$base_url.'yatras">Yatras</a></li>';
    	$templeMenu = $templeMenu.'<li><a target="_blank" href="'.$base_url.'yatra-registration-form">Book Your Package</a></li>';
    	
    	$religiousCat = $this->getCategoryMenuItemsMobile(Mage::getModel('catalog/category')->loadByAttribute('name','Religious Products')->getId());
    	$pujas = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('puja-main-menu-items-mobile')->toHtml();//$this->getCategoryMenuItems(Mage::getModel('catalog/category')->loadByAttribute('name','All Pujas')->getId());
    	
    	
    	$html = '
    	
    	
          <li><a href="'.$base_url.'book-a-pandit">Book a Pandit</a></li>
          <li><a href="'.$base_url.'organise-a-puja">Organise a Puja</a></li>
          <li><a href="'.$base_url.'e-puja">E-Puja</a></li>
          <li><a>Religious Products</a><ul class="dl-submenu">'.$religiousCat.'</ul></li>
          <li><a>Yatra & Tirth</a><ul class="dl-submenu">'.$templeMenu.'</ul></li>
          <li><a href="#">Puja</a>'.$pujas.'</li>
    	    <li><a href="'.$base_url.'hssf">HSSF</a></li>
';
    	
    	return $html;
    }
}


/*
 
 <ul class="level0" style="width: 33.3333333333%;left:0%;">
         <li class="level1 1 first ">
            <a class="plus" href="' . $base_url . 'laxmi-puja.html"><span>Laxmi Puja</span></a>
         </li>
         <li class="level1 2 ">
            <a class="plus" href="' . $base_url . 'akhand-ramayan-paath.html"><span>Akhand Ramayan Paath</span></a>
         </li>
         <li class="level1 3 ">
            <a class="plus" href="' . $base_url . 'griha-pravesh-puja.html"><span>Griha Pravesh Puja</span></a>
         </li>
         <li class="level1 4 ">
            <a class="plus" href="' . $base_url . 'hanuman-chalisa-paath.html"><span>Hanuman Chalisa Paath</span></a>
         </li>
         <li class="level1 5 ">
            <a class="plus" href="' . $base_url . 'havan.html"><span>Havan</span></a>
         </li>
         <li class="level1 6 ">
            <a class="plus" href="' . $base_url . 'kaal-bhairav-puja.html"><span>Kaal Bhairav Puja</span></a>
         </li>
         
         
         
         <li class="level1 16 first ">
            <a class="plus" href="' . $base_url . 'bhoomi-puja.html"><span>Bhoomi Puja</span></a>
         </li>
         
         <li class="level1 17 ">
            <a class="plus" href="' . $base_url . 'saraswati-puja.html"><span>Sarswati Puja - Vasant Panchami</span></a>
         </li>
         

      </ul>
      <ul class="level0" style="width: 33.3333333333%;left:33.3333333333%;">
         <li class="level1 7 ">
            <a class="plus" href="' . $base_url . 'krishna-puja.html"><span>Krishna Puja</span></a>
         </li>
         <li class="level1 8 ">
            <a class="plus" href="' . $base_url . 'maha-mrityunjaya-jaap.html"><span>Maha Mrityunjaya Jaap</span></a>
         </li>
         <li class="level1 9 ">
            <a class="plus" href="' . $base_url . 'rudrabhishek-puja.html"><span>Rudrabhishek Puja</span></a>
         </li> 
         
                  
         <li class="level1 10 ">
            <a class="plus" href="' . $base_url . 'satyanarayan-puja.html"><span>Satyanarayan Puja</span></a>
         </li>
         <li class="level1 11 ">
            <a class="plus" href="' . $base_url . 'shiv-puja.html"><span>Shiv Puja</span></a>
         </li>
         <li class="level1 12 ">
            <a class="plus" href="' . $base_url . 'sundarkand-paath.html"><span>Sundarkand Paath</span></a>
         </li>
         
         <li class="level1 25 ">
            <a class="plus" href="' . $base_url . 'gauri-puja.html"><span>Gauri Puja</span></a>
         </li>
         <li class="level1 26 last ">
            <a class="plus" href="' . $base_url . 'maha-shivratri-puja.html"><span>Maha Shivratri Puja</span></a>
         </li>         

         <li class="level1 20 last ">
            <a class="plus" href="' . $base_url . 'vishwakarma-puja.html"><span>Vishwakarma Puja</span></a>
         </li>
         
      </ul>
      <ul class="level0" style="width: 33.3333333333%;left:66.6666666667%;">         
         
        <li class="level1 13 last ">
            <a class="plus" href="' . $base_url . 'vivah-puja.html"><span>Vivah Puja</span></a>
         </li>
         <li class="level1 14 ">
            <a class="plus" href="' . $base_url . 'mata-ki-chowki.html"><span>Mata ki Chowki</span></a>
         </li>
         <li class="level1 15 ">
            <a class="plus" href="' . $base_url . 'kali-puja.html"><span>Kali Puja</span></a>
         </li> 
         
                  <li class="level1 21 first ">
            <a class="plus" href="' . $base_url . 'dhanteras-puja.html"><span>Dhanteras Puja</span></a>
         </li>
         <li class="level1 22 ">
            <a class="plus" href="' . $base_url . 'durga-puja.html"><span>Durga Puja</span></a>
         </li>
         <li class="level1 23 ">
            <a class="plus" href="' . $base_url . 'ganesh-chaturthi-puja.html"><span>Ganpati Puja / Ganesh Chaturthi</span></a>
         </li>
         
         <li class="level1 24 ">
            <a class="plus" href="' . $base_url . 'navratri-puja.html"><span>Navratri Puja</span></a>
         </li>

         <li class="level1 6 last ">
            <a class="plus" href="' . $base_url . 'vishwakarma-puja.html"><span>Vishwakarma Puja</span></a>
         </li>
         
      </ul>
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * Holly store
 * <ul class="level0" style="width: 33.3333%; left: 0%; display: none;">
         <li class="level1 1 first"><a href="' . $base_url . 'the-holy-store/necklace.html" class="plus"><span>Mala and Beads</span></a></li>
         <li class="level1 2"><a href="' . $base_url . 'the-holy-store/chattar.html" class="plus"><span>Chattar</span></a></li>
         <li class="level1 3"><a href="' . $base_url . 'the-holy-store/jaldhari.html"class="plus"><span>Jaldhari</span></a></li>
         <li class="level1 4"><a href="' . $base_url . 'the-holy-store/coins.html" class="plus"><span>Coins</span></a></li>
         <li class="level1 5"><a href="' . $base_url . 'the-holy-store/trishul.html" class="plus"><span>Trishul</span></a></li>
         <li class="level1 6"><a href="' . $base_url . 'the-holy-store/chopda.html" class="plus"><span>Chopda</span></a></li>
         <li class="level1 7"><a href="' . $base_url . 'the-holy-store/agarbatti-stand.html" class="plus"><span>Agarbatti Stand</span></a></li>
      </ul>
      <ul class="level0" style="width: 33.3333%; left: 33.3333%; display: none;">
         <li class="level1 8"><a href="' . $base_url . 'the-holy-store/diya.html" class="plus"><span>Diya</span></a></li>
         <li class="level1 9"><a href="' . $base_url . 'the-holy-store/nariyal-kalash.html" class="plus"><span>Nariyal Kalash</span></a></li>
         <li class="level1 10"><a href="' . $base_url . 'the-holy-store/katori.html" class="plus"><span>Katori</span></a></li>
         <li class="level1 11"><a href="' . $base_url . 'the-holy-store/loti.html" class="plus"><span>Loti</span></a></li>
         <li class="level1 12"><a href="' . $base_url . 'the-holy-store/fengshui-tortoise.html" class="plus"><span>Fengshui Tortoise</span></a></li>
         <li class="level1 13"><a href="' . $base_url . 'the-holy-store/aarti-ki-thali.html" class="plus"><span>Aarti ki Thali</span></a></li>
         <li class="level1 14"><a href="' . $base_url . 'the-holy-store/ghanti-bell.html" class="plus"><span>Ghanti / Bell</span></a></li>
      </ul>
      <ul class="level0" style="width: 33.3333%; left: 66.6667%; display: none;">
         <li class="level1 15"><a href="' . $base_url . 'the-holy-store/rangoli.html" class="plus"><span>Rangoli</span></a></li>
         <li class="level1 16"><a href="' . $base_url . 'the-holy-store/fresh-on-water.html" class="plus"><span>Fresh on water</span></a></li>
         <li class="level1 17"><a href="' . $base_url . 'the-holy-store/wall-hanging.html" class="plus"><span>Wall Hanging</span></a></li>
         <li class="level1 18"><a href="' . $base_url . 'the-holy-store/diwali-special.html" class="plus"><span>Diwali Special</span></a></li>
         <li class="level1 19"><a href="' . $base_url . 'the-holy-store/poshak.html" class="plus"><span>Poshak</span></a></li>
         <li class="level1 20"><a href="' . $base_url . 'the-holy-store/toran.html" class="plus"><span>Toran</span></a></li>
         <li class="level1 21 last"><a href="' . $base_url . 'the-holy-store/view-more.html" class="plus"><span>View More</span></a></li>
      </ul>
 
 */
