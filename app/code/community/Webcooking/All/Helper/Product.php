<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Product extends Mage_Core_Helper_Abstract {

    public function _getProduct($product) {
        if (is_object($product)) {
            return $product;
        }
        return Mage::getModel('catalog/product')->load($product);
    }

    public function getMinSaleQty($productId) {
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
        if ($stockItem->getMinQty() > 1) {
            return $stockItem;
        }
        if ($stockItem->getQtyIncrements() > 1) {
            return $stockItem->getQtyIncrements();
        }
        return 1;
    }

    public function isBundleProductWithoutConfiguration($product) {
        $product = $this->_getProduct($product);
        if ($product->getTypeId() != 'bundle') {
            return false;
        }
        
        $bundleProduct = clone $product;
        $additional = array();
        $isBundleDefault = true;
       
        $typeInstance = $bundleProduct->getTypeInstance(true);
        $typeInstance->setStoreFilter($bundleProduct->getStoreId(), $bundleProduct);
        $optionCollection = $typeInstance->getOptionsCollection($bundleProduct);

        $selectionCollection = $typeInstance->getSelectionsCollection(
                $typeInstance->getOptionsIds($bundleProduct), $bundleProduct
        );
        $options = $optionCollection->appendSelections($selectionCollection, false);
        $bundleOption = array();
        foreach ($options as $option) {
            foreach ($option->getSelections() as $selection) {
                if ($option->getRequired()) {
                    if ($selection->getIsDefault()) {
                        $bundleOption[$option->getOptionId()] = $selection->getSelectionId();
                    } else {
                        $isBundleDefault = false;
                        continue;
                    }
                }
            }
        }

        if ($isBundleDefault) {
            $additional['bundle_option'] = $bundleOption;
        }

        return $additional;
    }

}
