<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Dashboard extends Webcooking_All_Helper_Data {
    
    public function getApiHelper() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
    public function getSelectedProfile() {
        $profileId = Mage::getStoreConfig('googleuniversalanalytics/dashboard/profile');
        if(!$profileId) {
            $profile = Mage::helper('googleuniversalanalytics/api')->getProfileForStore();
                if($profile) {
                    $config = new Mage_Core_Model_Config();
                    $config->saveConfig('googleuniversalanalytics/dashboard/profile', $profile->getId());
                    return $profile->getWebPropertyId();
                }
        }
        if(!$profileId) {
            foreach ($this->getApiHelper()->getProfiles() as $profile) {
                return $profile->getWebPropertyId();
            }
        }
        return $profileId;
    }

    public function getChartData($from, $to = false, $query = 'visitors', $projectId=false) {
        $resultData = array();
        if(!$projectId) {
            $projectId = $this->getSelectedProfile();
        }
        if(!$projectId) {
            return false;
        }
        
        $metrics = 'ga:' . $query;

        if(!$from) {
            $from = date('Y-m-d');
        }
        if (!$to) {
            $to = $from;
        }

        if ($from == $to) {
            $dimensions = 'ga:hour';
        } else {
            $dimensions = 'ga:year,ga:month,ga:day';
        }

        $cacheId = 'gua_dash_' . $from . $to . $query . $projectId . '_cache';
        $tryCache = true;
        if ($to == date('Y-m-d') || $from == date('Y-m-d')) {
            $tryCache = false;
        }

        if ($tryCache) {
            $resultData = Mage::app()->loadCache($cacheId);
            if($resultData) {
                return $resultData;
            }
        }
        try {
            $data = Mage::helper('googleuniversalanalytics/api')->getService()->data_ga->get('ga:' . $projectId, $from, $to, $metrics, array(
                'dimensions' => $dimensions
            ));
        } catch (Exception $e) {
            Mage::helper('googleapi')->logException($e, 'gua');
        }

        $jsonData = "";

        //var_dump($data);
        if ($dimensions == 'ga:hour') {
            for ($i = 0; $i < $data['totalResults']; $i ++) {
                $jsonData .= "['" . $data['rows'][$i][0] . ":00'," . round($data['rows'][$i][1], 2) . "],";
            }
        } else {
            for ($i = 0; $i < $data['totalResults']; $i ++) {
                $jsonData .= "['" . $data['rows'][$i][0] . "-" . $data['rows'][$i][1] . "-" . $data['rows'][$i][2] . "'," . round($data['rows'][$i][3], 2) . "],";
            }
        }
        
        //var_dump($jsonData);
        $resultData['json'] = $jsonData;
        $resultData['totalResults'] = $data['totalResults'];
        $resultData['totalsForAllResults'] = $data['totalsForAllResults'];
        
        if ($tryCache) {
            Mage::app()->saveCache($resultData, $cacheId, array(
                'reports',
                'store'
            ));
        }
        return $resultData;
    }

}
