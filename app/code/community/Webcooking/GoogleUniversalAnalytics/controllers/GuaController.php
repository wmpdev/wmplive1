<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

class Webcooking_GoogleUniversalAnalytics_GuaController extends Mage_Core_Controller_Front_Action
{
   public function updatesessionAction() {
       $guaClientId = $this->getRequest()->getParam('clientid');
       $session = Mage::getSingleton('core/session');
       $session->setGuaClientId($guaClientId);
       $session->setGuaUserAgent(Mage::helper('core/http')->getHttpUserAgent());
   }
   
   public function testAction() {
       $test = array(
       'v' => 1,
        'ds' => 'web',
        'uip' => '90.16.254.110',
        'tid' => '',
        'ua' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36',
        'dh' => 'dev.twonav.com/fr/',
        'cid' => '.',
        't' => 'event',
        'ec' => 'transaction',
        'ea' => 'invoice',
        'el' => 'OR-2015-6-017020',
        'ev' => 299,
        'ni' => 1,
        'uid' => 3,
        'pr1id' => 'twonav_anima_generic_fr-twonav_anima_escandallo_dispositivo-1205',
        'pr1nm' => 'Anima',
        'pr1ca' => 'GPS',
        'pr1br' => '',
        'pr1va' => '',
        'pr1pr' => 299.0000,
        'pr1qt' => 1,
        'ti' => 'OR-2015-6-017020',
        'ta' => 'FranÃ§ais',
        'tr' => 299,
        'ts' => 0,
        'tt' => 51.8926,
        'pa' => 'purchase',
        'cu' => 'EUR'
           );
       echo "<pre>";
       var_dump($test);
       echo "</pre>";
       $result = Mage::getModel('googleuniversalanalytics/measurement_protocol')->execRequest($test);
       var_dump($result);
   }
}