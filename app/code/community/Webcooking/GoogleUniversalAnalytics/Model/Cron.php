<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Model_Cron {

    protected function _getApi() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
    
    protected $_productFeed = null;
    protected function _getProductFeed($storeId = false) {
        if(is_null($this->_productFeed)) {
            $productCollection = Mage::getModel('catalog/product')->getCollection();
            $productCollection->addAttributeToSelect('name', 'left');
            $productCollection->addAttributeToSelect('price', 'left');
            $productCollection->addCategoryIds();
            //$productCollection->addAttributeToSelect('category_ids', 'left');
            $brandAttributeCode = Mage::helper('googleuniversalanalytics/ecommerce')->getBrandAttributeCode($storeId);
            if($brandAttributeCode) {
                $productCollection->addAttributeToSelect($brandAttributeCode, 'left');
            }
            $header = array('ga:productSku','ga:productBrand','ga:productCategoryHierarchy','ga:productName','ga:productPrice');
            $this->_productFeed = implode(',', $header) . "\n";
            Mage::getSingleton('core/resource_iterator')
                        ->walk($productCollection->getSelect(), array(array($this, '_addProductInFeed')), array('brandAttributeCode'=>$brandAttributeCode));
            $this->_productFeed = trim($this->_productFeed);
        }
        return $this->_productFeed;
        
    }
    
    public function _addProductInFeed($args) {
        $productData = $args['row'];
        $product = Mage::getModel('catalog/product');
        $product->setData($productData);
        $brandAttributeCode = $args['brandAttributeCode'];
        $row = array();
        $row[] = $productData['sku'];
        if($brandAttributeCode) {
            $row[] = '"' . Mage::helper('wcooall/attribute')->getAttributeOptionNameById($brandAttributeCode, $productData[$brandAttributeCode]) . '"';
        } else {
            $row[] = '';
        }
        $row[] = '"' . Mage::helper('googleuniversalanalytics/ecommerce')->getProductCategoryValue($product, false) . '"';
        $row[] = '"' . $productData['name'] . '"';
        $row[] = '"' . $productData['price'] . '"';
        $this->_productFeed .= implode(',' , str_replace($row, ",", "\,")) . "\n";
    }

    
    public function updateProducts() {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/export_products_via_api')) {
            return;
        }
        $customDataSourceId = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/export_products_data_source_id');
        if(!$customDataSourceId) {
            return;
        }
        if(!$this->_getApi() || !$this->_getApi()->getClient()) {
            return;
        }
        try {
            $profile = $this->_getApi()->getProfileForStore();
            if($profile) {
                $productFeed = $this->_getProductFeed();
              
                $analytics = new Google_Service_Analytics($this->_getApi()->getClient());
                $result = $analytics->management_uploads->uploadData(
                        $profile->getAccountId(), 
                        Mage::helper('googleuniversalanalytics')->getAccountId(), 
                        $customDataSourceId, 
                        array('data' => $productFeed,
                              'mimeType' => 'application/octet-stream',
                              'uploadType' => 'media'));
            }
        } catch (apiServiceException $e) {
            Mage::helper('googleuniversalanalytics')->log('There was an Analytics API service error '
                    . $e->getCode() . ':' . $e->getMessage());
        } catch (apiException $e) {
            Mage::helper('googleuniversalanalytics')->log('There was a general API error '
                    . $e->getCode() . ':' . $e->getMessage());
        } catch(Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('There was an API error ' . $e->getMessage());
        }
        
    }

}
