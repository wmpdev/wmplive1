<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleTagManager
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 * @deprecated since version 1.3.2
 */
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Active_Comment extends Mage_Core_Model_Config_Data {

    public function getCommentText(Mage_Core_Model_Config_Element $element, $currentValue) {
        $guaUrl = 'http://restaurant.web-cooking.net/seo/magento-google-universal-analytics.html';
        $comment = '';
        if(Mage::helper('wcooall')->isModuleInstalled('Webcooking_GoogleTagManager')) {
           if(Mage::helper('googletagmanager')->isGoogleAnalyticsEnabled()) {
                $comment = '<span style="color:orange;">Webcooking_GoogleTagManager is installed : You should disable classical Analytics, except if you want a double tracking.</span>';
           }
        }
        return $comment;
    }

}
