<?php

class Airpay_Model_Transact extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'airpay';

    /* protected $_canAuthorize            = true; */
    protected $_canCapture              = true;
    protected $_isInitializeNeeded      = true;
    protected $_canUseInternal          = false;
    protected $_canUseForMultishipping  = false;
    protected $_canVoid                 = true;
    protected $_canRefund               = true;

    /**
     * Return Order place redirect url
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('airpay/transact/redirect', array('_secure' => true));
    }

    /**
     * Url to which the post form will be submitted
     * and the user will be redirected
     * @param String Url
     */
    public function getAirpayTransactAction()
    {
        return "https://payments.airpay.co.in/pay/index.php";
    }
    /**
     * A wrapper method to access the checkout and order details
     * stored in the session
     * @param ? Session
     */
    public function getCheckout() 
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Instantiate state and set it to state object
     * @param string $paymentAction
     * @param Varien_Object
     */
    public function initialize($paymentAction, $stateObject)
    {
        $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
        $stateObject->setState($state);
        $stateObject->setStatus('pending_payment');
        $stateObject->setIsNotified(false);
    }

    /**
     * Method to get the form fields with the relevant fields filled in
     * @return Array of form fields in the name=>value form 
     */
    public function getCheckoutFormFields() 
    {
        $orderIncrementId = $this->getCheckout()->getLastRealOrderId();

        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        $this->getCheckout()->setAirpayOrderId($orderIncrementId);
        $api = Mage::getModel('airpay/api_transact')->setConfigObject($this->getConfig());
        $api->setOrderId($orderIncrementId)
            ->setCurrencyCode($order->getBaseCurrencyCode())
            ->setOrder($order)
            ->setAirpayConfig(Mage::getStoreConfig('payment/airpay'))
            ->setReturnUrl(Mage::getUrl('airpay/transact/response'));
        // export address
        $isOrderVirtual = $order->getIsVirtual();
        $api->setBillingAddress($order->getBillingAddress());
        if ($isOrderVirtual) {
            $api->setNoShipping(true);
        } elseif ($order->getShippingAddress()->validate()) {
            $api->setShippingAddress($order->getShippingAddress());
        }
        // add cart totals and line items
        $result = $api->getRequestFields();
        $this->getCheckout()->setAirpayChecksum($api->getAirpayChecksum());
        return $result;
    }  

    public function airpaySuccessOrderState()
    {
        $config = Mage::getStoreConfig('payment/airpay');
        $order_status = $config['order_status'];
        switch ($order_status) {
        case "processing":
            $state = Mage_Sales_Model_Order::STATE_PROCESSING;
            break;
        case "complete":
            $state = Mage_Sales_Model_Order::STATE_COMPLETE;
            break;
        case "closed":
            $state = Mage_Sales_Model_Order::STATE_CLOSED;
            break;
        case "canceled":
            $state = Mage_Sales_Model_Order::STATE_CANCELED;
            break;
        case "holded":
            $state = Mage_Sales_Model_Order::STATE_HOLDED;
            break;
        case "pending":
        default:
            $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
        }
        return $state;
    }
}
