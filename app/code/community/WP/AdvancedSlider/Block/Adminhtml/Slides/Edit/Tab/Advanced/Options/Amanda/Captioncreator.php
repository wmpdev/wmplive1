<?php

class WP_AdvancedSlider_Block_Adminhtml_Slides_Edit_Tab_Advanced_Options_Amanda_Captioncreator
    extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('advancedslider/slide_amanda_captioncreator.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild('options', $this->getLayout()->createBlock('advancedslider/adminhtml_slides_edit_tab_advanced_options_amanda_captioncreator_options'));
        return parent::_prepareLayout();
    }
}
