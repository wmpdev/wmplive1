<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$installer->getTable('advancedslider_insertion')};
CREATE TABLE IF NOT EXISTS {$installer->getTable('advancedslider_insertion')} (
  `obj_type` tinyint(3) unsigned NOT NULL,
  `obj_id` int(10) unsigned NOT NULL,
  `slider_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `insertion` (`obj_type`,`obj_id`,`slider_id`),
  KEY `slider_id` (`slider_id`),
  KEY `obj` (`obj_type`,`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE {$installer->getTable('advancedslider_insertion')}
  ADD CONSTRAINT {$installer->getTable('advancedslider_insertion_ibfk_1')} FOREIGN KEY (`slider_id`) REFERENCES {$installer->getTable('advancedslider_sliders')} (`slider_id`) ON DELETE CASCADE ON UPDATE CASCADE;

    ");

$installer->endSetup();
