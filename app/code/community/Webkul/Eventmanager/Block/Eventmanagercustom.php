<?php
class Webkul_Eventmanager_Block_Eventmanagercustom extends Mage_Core_Block_Template
{
        public function _prepareLayout()
    {
                return parent::_prepareLayout();
    }

     public function getEventmanagercustom()
     {
        if (!$this->hasData('eventmanagercustom')) {
            $this->setData('eventmanagercustom', Mage::registry('eventmanagercustom'));
        }
                return $this->getData('eventmanagercustom');

    }
}
