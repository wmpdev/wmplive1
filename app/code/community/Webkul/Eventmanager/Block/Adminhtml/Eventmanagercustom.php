<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagercustom extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_eventmanagercustom';
    $this->_blockGroup = 'eventmanager';
    $this->_headerText = Mage::helper('eventmanager')->__('Custom Event Requests');
    parent::__construct();
        $this->_removeButton('add');
  }
}
