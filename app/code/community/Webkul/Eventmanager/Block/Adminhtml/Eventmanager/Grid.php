<?php 
class Webkul_Eventmanager_Block_Adminhtml_Eventmanager_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("eventGrid");
        $this->setDefaultSort("event_id");
        $this->setDefaultDir("ASC");
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel("eventmanager/eventmanager")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
      $this->addColumn("event_id", array(
          "header"    => $this->__("ID"),
          "align"     =>"center",
          "index"     => "event_id",
      ));

      $this->addColumn("title", array(
          "header"    => $this->__("Event"),
          "align"     =>"center",
          "index"     => "title",
      ));
	  
      $this->addColumn("edate", array(
          "header"    => $this->__("Date"),
          "align"     =>"center",
          "index"     => "edate",
      ));
	  
      $this->addColumn("body", array(
    			"header"   => $this->__("Content"),
    			"align"     =>"center",
    			"index"     => "body",
      ));

      $this->addColumn("status", array(
          "header"    => $this->__("Status"),
          "align"     => "center",
          "width"     => "80px",
          "index"     => "status",
          "type"      => "options",
          "options"   => array(
              1 => $this->__("Enabled"),
              2 => $this->__("Disabled"),
          ),
      ));

      $this->addColumn("action",array(
          "header" => $this->__("Action"),
          "width" => "80",
          "type" => "action",
          "getter" => "getId",
          "actions" => array(
              array(
                  "caption" => $this->__("Edit"),
                  "url" => array("base" => "*/*/edit"),
                  "field" => "id"
              )
          ),
          "filter" => false,
          "sortable" => false,
          "index" => "stores",
          "is_system" => true,
      ));

      $this->addExportType("*/*/exportCsv", $this->__("CSV"));
      $this->addExportType("*/*/exportXml", $this->__("XML"));
      return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField("event_id");
        $this->getMassactionBlock()->setFormFieldName("eventmanager");
        $this->getMassactionBlock()->addItem("delete", array(
            "label" => $this->__("Delete"),
            "url" => $this->getUrl("*/*/massDelete"),
            "confirm" => $this->__("Are you sure?")
        ));
        $statuses = Mage::getSingleton("eventmanager/status")->getOptionArray();
        array_unshift($statuses, array("label" => "", "value" => ""));
        $this->getMassactionBlock()->addItem("status", array(
            "label" => $this->__("Change status"),
            "url" => $this->getUrl("*/*/massStatus", array("_current" => true)),
            "additional" => array(
                "visibility" => array(
                    "name" => "status",
                    "type" => "select",
                    "class" => "required-entry",
                    "label" => $this->__("Status"),
                    "values" => $statuses
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    public function getGridUrl()    {
        return $this->getUrl("*/*/grid",array("_current"=>true));
    }

}