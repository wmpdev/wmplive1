<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagergroup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "eventmanager";
        $this->_controller = "adminhtml_eventmanagergroup";
        $this->_updateButton("save", "label", $this->__("Save Item"));
        $this->_updateButton("delete", "label", $this->__("Delete Item"));
        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("adminhtml")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText() {
        if (Mage::registry("eventmanagergroup_data") && Mage::registry("eventmanagergroup_data")->getId())
            return $this->__("Edit Item ").$this->__("'%s'", $this->htmlEscape(Mage::registry("eventmanagergroup_data")->getGroupName()));
        else 
            return $this->__("Add Item");
    }

}