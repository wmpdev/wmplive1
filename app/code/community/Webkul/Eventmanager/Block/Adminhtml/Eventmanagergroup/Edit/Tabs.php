<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanagergroup_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("eventmanagergroup_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle($this->__("Event Group Information"));
    }

    protected function _beforeToHtml() {
        $this->addTab("form_section", array(
            "label" => $this->__("Event Group"),
            "alt" => $this->__("event Group"),
            "content" => $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_edit_tab_form")->toHtml(),
        ));
        $this->addTab("grid_section", array(
            "label" => $this->__("Events"),
            "alt" => $this->__("Events"),
            "content" => $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_edit_tab_grideventmanager")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}