<?php
class Webkul_Eventmanager_Block_Adminhtml_Eventmanager extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = "adminhtml_eventmanager";
        $this->_blockGroup = "eventmanager";
        $this->_headerText = $this->__("Add/Manage Event");
        $this->_addButtonLabel = $this->__("Add Event");
        parent::__construct();
    }

}