<?php

class Webkul_Eventmanager_Block_Adminhtml_Eventmanagercustom_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('eventmanagercustom_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('eventmanager')->__('Custom Event Requests'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('eventmanager')->__("Custom Events"),
          'title'     => Mage::helper('eventmanager')->__('Custom Event Requests'),
          'content'   => $this->getLayout()->createBlock('eventmanager/adminhtml_eventmanagercustom_edit_tab_form')->toHtml(),
      ));

      return parent::_beforeToHtml();
  }
}
