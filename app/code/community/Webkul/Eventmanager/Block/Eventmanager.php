<?php
class Webkul_Eventmanager_Block_Eventmanager extends Mage_Core_Block_Template {

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getEventgroupcodebyid($eventid){
		$collection = Mage::getModel("eventmanager/eventmanagergroup")->getCollection()
			->addFieldToFilter("status","1")
			->addFieldToFilter("event_ids",array("like" => "%".$eventid."%"));
		foreach($collection as $coll)
			$code = $coll->getGroupCode();
		return $code;
	}
	
	public function getEventgrouptitle($groupCode){
		$collection = Mage::getModel("eventmanager/eventmanagergroup")->getCollection()
			->addFieldToFilter("group_code",$groupCode);
		foreach($collection as $coll)
			$title = $coll->getGroupTitle();
		return $title;
	}

	public function getDataByGroupCode($groupCode){
        return Mage::getModel("eventmanager/eventmanagergroup")->getDataByGroupCode($groupCode);
    }

    public function getAllEvents()	{
		$collection = Mage::getModel("eventmanager/eventmanager")->getCollection();
		return $collection;
	}

	public function getAllEventsbyGroup($eventdata)	{
		foreach($eventdata as $events){
			if($events["event_ids"])
				$eventids = explode(",",$events["event_ids"]);
		}
		$collection = Mage::getModel("eventmanager/eventmanager")->getCollection()
			->addFieldToFilter("status","1")
			->addFieldToFilter("event_id",array("in" => $eventids));
		return $collection;
	}

}