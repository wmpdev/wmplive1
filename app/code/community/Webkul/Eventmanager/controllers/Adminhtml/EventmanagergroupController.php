<?php
class Webkul_Eventmanager_Adminhtml_EventmanagergroupController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
			->_setActiveMenu("eventmanager/items")
			->_addBreadcrumb(Mage::helper("adminhtml")->__("Items Manager"), Mage::helper("adminhtml")->__("Item Manager"));
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("eventmanager/eventmanagergroup")->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
            if (!empty($data)) 
                $model->setData($data);
        }
        Mage::register("eventmanagergroup_data", $model);
        return $this;
    }

    public function indexAction() {
        $this->_initAction()->renderLayout();
    }

    public function eventgridAction() {
        $this->_initAction();
        $this->getResponse()->setBody(
			$this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_edit_tab_eventmanager")->toHtml()
        );
    }

    public function editAction() {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("eventmanager/eventmanagergroup")->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
            if (!empty($data)) 
                $model->setData($data);
            Mage::register("eventmanagergroup_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("eventmanager");
            $this->_addContent($this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_edit"))
				->_addLeft($this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError($this->__("Item does not exist"));
            $this->_redirect("*/*/");
        }
    }

    public function newAction() {
        $this->_forward("edit");
    }

    public function saveAction() {
       	$group_id = $this->getRequest()->getParam("id");
        if ($data = $this->getRequest()->getPost()) {
            $events = array();
            $availEventIds = Mage::getModel("eventmanager/eventmanager")->getAllAvailEventIds();
            parse_str($data["eventmanagergroup_eventmanagers"], $events);
            foreach ($events as $k => $v) {
                if (preg_match("/[^0-9]+/", $k) || preg_match("/[^0-9]+/", $v)) {
                    unset($events[$k]);
                }
            }
            $eventIds = array_intersect($availEventIds, $events);
            $data["event_ids"] = implode(",", $eventIds);            
            $model = Mage::getModel("eventmanager/eventmanagergroup");
            $group_name=$data["group_name"];
			$number=count($data["cms_pages"]);
			$aaa="";
			for($j=0;$j<count($data["cms_pages"]);$j++)		
			    $aaa = $aaa.$data["cms_pages"][$j].",";
			$data["cms_pages"]=$aaa;
			if($group_id!="" || $group_id!=NULL){
				$collection=Mage::getModel("eventmanager/eventmanagergroup")->load($group_id);	
				$newcms = explode(",",$data["cms_pages"]);
				$oldcms = explode(",",$collection["cms_pages"]);
				$removecmspageids = array_diff($oldcms,$newcms);
				if(count($removecmspageids)){
					foreach($removecmspageids as $removecmspageid)
					$removecmspage = Mage::getModel("cms/page")->load($removecmspageid);
					$removepagecon = $removecmspage->getContent();
					$removeblockcon = "{{block type='eventmanager/eventmanager' name='".$collection['group_name']."' eventmanager_group_code='".$collection['group_code']."' template='eventmanager/events.phtml'}}";
					$removepos = strpos($removepagecon, "eventmanager_group_code='".$collection['group_code']."' template='eventmanager/events.phtml'");
					if ($removepos != "" || $removepos != NULL) {
						str_replace($removeblockcon,"",$removepagecon);
						$savedata = str_replace($removeblockcon,"",$removepagecon);
						$removecmspage->setContent($savedata);
						$removecmspage->save();
					}
				}
				$group_code=$collection->getGroupCode();                
			}else{
                $group_code=$data["group_code"];
			}
            $model->setData($data)
                    ->setId($this->getRequest()->getParam("id"));		
			$str="{{block type='eventmanager/eventmanager' name='".$group_name."' eventmanager_group_code='".$group_code."' template='eventmanager/events.phtml'}}";	
			$cms_pages=explode(",",$aaa);
			for($k=0;$k<$number;$k++){
				$pagecms=$cms_pages[$k];
				$page= Mage::getModel("cms/page")->load($pagecms);
				$pagecon=$page->getContent();
				$pos = strpos($pagecon, "template='eventmanager/events.phtml'");
				if ($pos == "" || $pos==NULL) {
					$savedata=$str."</br>".$pagecon;
					$page->setStores(array(0)); 
					$page->setContent($savedata);
					$page->save();
				}
                else{
					$strsearch="block type='eventmanager/eventmanager' name='".$group_name."'";
					$pos1 = strpos($pagecon, $strsearch);
					if ($pos1 == "" || $pos1==NULL) {
						$p=explode("block type='eventmanager/eventmanager' name=",$pagecon);
						$p1=explode("eventmanager_group_code=",$p[1]);
						$groupname=$p1[0];
						$p2=explode("template='eventmanager/events.phtml'",$p1[1]);
						$groupcode=$p2[0];
						$groupname1="'".$group_name."' ";
						$groupcode1="'".$group_code."' ";
						$newphrase=$p[0]."block type='eventmanager/eventmanager' name=".$groupname1.""."eventmanager_group_code=".$groupcode1."template='eventmanager/events.phtml'".$p2[1] ;
						$page->setStores(array(0)); 
						$page->setContent($newphrase);
						$page->save();
					}
				}
			}
			try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) 
                    $model->setCreatedTime(now())->setUpdateTime(now());
                else
                    $model->setUpdateTime(now());
                $model->save();
                Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Item was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setFormData(false);
                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } 
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setFormData($data);
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        Mage::getSingleton("adminhtml/session")->addError($this->__("Unable to find item to save"));
        $this->_redirect("*/*/");
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("eventmanager/eventmanagergroup")->load($this->getRequest()->getParam("id"));
                $model->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } 
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massDeleteAction() {
        $eventIds = $this->getRequest()->getParam("eventmanager");
        if (!is_array($eventIds)) {
            Mage::getSingleton("adminhtml/session")->addError($this->__("Please select item(s)"));
        } 
        else {
            try {
                foreach ($eventIds as $eventId) {
                    $event = Mage::getModel("eventmanager/eventmanagergroup")->load($eventId);
                    $event->delete();
                }
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Total of ").$this->__("%d", count($eventIds)).$this->__(" record(s) were successfully deleted"));
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            }
        }
        $this->_redirect("*/*/index");
    }

    public function massStatusAction() {
        $eventIds = $this->getRequest()->getParam("eventmanager");
        if (!is_array($eventIds))
            Mage::getSingleton("adminhtml/session")->addError($this->__("Please select item(s)"));
        else {
            try {
                foreach ($eventIds as $eventId) {
                    $event = Mage::getSingleton("eventmanager/eventmanagergroup")
								->load($eventId)
								->setStatus($this->getRequest()->getParam("status"))
								->setIsMassupdate(true)
								->save();
                }
                $this->_getSession()->addSuccess($this->__("Total of ").$this->__("%d", count($eventIds)).$this->__(" record(s) were successfully updated"));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect("*/*/index");
    }

    public function exportCsvAction() {
        $fileName = "eventmanagergroup.csv";
        $content = $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_grid")->getCsv();
        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName = "eventmanagergroup.xml";
        $content = $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_grid")->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType="application/octet-stream") {
        $response = $this->getResponse();
        $response = $this->getResponse();
        $response->setHeader("HTTP/1.1 200 OK", "");
        $response->setHeader("Pragma", "public", true);
        $response->setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0", true);
        $response->setHeader("Content-Disposition", "attachment; filename=" . $fileName);
        $response->setHeader("Last-Modified", date("r"));
        $response->setHeader("Accept-Ranges", "bytes");
        $response->setHeader("Content-Length", strlen($content));
        $response->setHeader("Content-type", $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    public function gridAction()    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock("eventmanager/adminhtml_eventmanagergroup_grid")->toHtml()); 
    }

}