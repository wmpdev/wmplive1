<?php
class Webkul_Eventmanager_Adminhtml_EventmanagerController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
			->_setActiveMenu("eventmanager/items")
			->_addBreadcrumb(Mage::helper("adminhtml")->__("Items Manager"), Mage::helper("adminhtml")->__("Item Manager"));
        return $this;
    }

    public function indexAction() {
        $this->_initAction()->renderLayout();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("eventmanager/eventmanager")->load($id);
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
            if (!empty($data))
                $model->setData($data);
            Mage::register("eventmanager_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("eventmanager/items");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Item Manager"), Mage::helper("adminhtml")->__("Item Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Item News"), Mage::helper("adminhtml")->__("Item News"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
			if (Mage::getSingleton("cms/wysiwyg_config")->isEnabled())
				$this->getLayout()->getBlock("head")->setCanLoadTinyMce(true);
            $this->_addContent($this->getLayout()->createBlock("eventmanager/adminhtml_eventmanager_edit"))
                    ->_addLeft($this->getLayout()->createBlock("eventmanager/adminhtml_eventmanager_edit_tabs"));
            $version = substr(Mage::getVersion(), 0, 3);
            if (($version=="1.4" || $version=="1.5") && Mage::getSingleton("cms/wysiwyg_config")->isEnabled()) 
                $this->getLayout()->getBlock("head")->setCanLoadTinyMce(true);
            $this->renderLayout();
        } 
        else {
            Mage::getSingleton("adminhtml/session")->addError($this->__("Item does not exist"));
            $this->_redirect("*/*/");
        }
    }

    public function newAction() {
        $this->_forward("edit");
    }

    public function saveAction() {        
        if ($data = $this->getRequest()->getPost()) {
			$data["body"] = trim(preg_replace("/\s\s+/","",$data["body"]));			
            $model = Mage::getModel("eventmanager/eventmanager");
            $model->setData($data)->setId($this->getRequest()->getParam("id"));
            try {
                if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
                    $model->setCreatedTime(now())
                            ->setUpdateTime(now());
                } 
                else {
                    $model->setUpdateTime(now());
                }
                $model->save();
                Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Item was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setFormData(false);
                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } 
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setFormData($data);
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        Mage::getSingleton("adminhtml/session")->addError($this->__("Unable to find item to save"));
        $this->_redirect("*/*/");
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("eventmanager/eventmanager")->load($this->getRequest()->getParam("id"));
                $_helper = Mage::helper("eventmanager");
                $model->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } 
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massDeleteAction() {
        $eventIds = $this->getRequest()->getParam("eventmanager");
        if (!is_array($eventIds))
            Mage::getSingleton("adminhtml/session")->addError($this->__("Please select item(s)"));
        else {
            try {
                foreach ($eventIds as $eventId) {
                    $model = Mage::getModel("eventmanager/eventmanager")->load($eventId);
                    $_helper = Mage::helper("eventmanager");
                    $model->delete();
                }
                Mage::getSingleton("adminhtml/session")->addSuccess($this->__("Total of ").$this->__("%d", count($eventIds)).$this->__(" record(s) were successfully deleted"));
            } 
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            }
        }
        $this->_redirect("*/*/index");
    }

    public function massStatusAction() {
        $eventIds = $this->getRequest()->getParam("eventmanager");
        if (!is_array($eventIds)) 
            Mage::getSingleton("adminhtml/session")->addError($this->__("Please select item(s)"));
        else {
            try {
                foreach ($eventIds as $eventId) {
                    $event = Mage::getSingleton("eventmanager/eventmanager")
								->load($eventId)
								->setStatus($this->getRequest()->getParam("status"))
								->setIsMassupdate(true)
								->save();
                }
                $this->_getSession()->addSuccess($this->__("Total of ").$this->__("%d", count($eventIds)).$this->__(" record(s) were successfully updated"));
            } 
            catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect("*/*/index");
    }

    public function exportCsvAction() {
        $fileName = "eventmanager.csv";
        $content = $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanager_grid")->getCsv();
        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName = "eventmanager.xml";
        $content = $this->getLayout()->createBlock("eventmanager/adminhtml_eventmanager_grid")->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType="application/octet-stream") {
        $response = $this->getResponse();
        $response->setHeader("HTTP/1.1 200 OK", "");
        $response->setHeader("Pragma", "public", true);
        $response->setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0", true);
        $response->setHeader("Content-Disposition", "attachment; filename=" . $fileName);
        $response->setHeader("Last-Modified", date("r"));
        $response->setHeader("Accept-Ranges", "bytes");
        $response->setHeader("Content-Length", strlen($content));
        $response->setHeader("Content-type", $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    public function gridAction()    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock("eventmanager/adminhtml_eventmanager_grid")->toHtml()); 
    }

}