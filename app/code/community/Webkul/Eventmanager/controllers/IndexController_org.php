<?php
class Webkul_Eventmanager_IndexController extends Mage_Core_Controller_Front_Action {

	public $escapeCategories = array('Organise a Puja', 'Organise a Pooja', 'Recommended for you', 'Hotel Bookings','The Holy Store');

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

	
	public function getCategoriesAction($category_id = 2) {
		if(!isset($_POST['id'])) {
			$category_id = 2;
		} else {
			$category_id = $_POST['id'];
		}

		$already_added_categories = array();

		$categories = Mage::getModel('catalog/category')->getCollection()
		->addAttributeToFilter('parent_id', array('eq' => $category_id))
		->addAttributeToSelect('id')
		->addAttributeToSelect('name')
		->addAttributeToSelect('url_key')
		->addAttributeToSelect('url')
		->addAttributeToSelect('is_active');
		foreach ($categories as $category) {
			if ($category->getIsActive()) { // Only pull Active categories
				if(isset($_POST['level']) && $_POST['level'] == 2) {
					$shouldAdd = true;
					//echo "\nHere : " . $shouldAdd . " :: ";
					foreach ($this->escapeCategories as $escapeCategory) {
						if (strtolower($escapeCategory) == strtolower($category->getName())) {
							$shouldAdd = false;
							break;
						}
					}
					foreach ($already_added_categories as $alreadyCategory) {
						//echo $alreadyCategory . " :: " . $category->getName() . " :: " . preg_match('/.*' . $alreadyCategory . '.*/i', $category->getName()) . " :: " . preg_match('/.*' . $category->getName() . '.*/i', $alreadyCategory) . "\n\n";
						if (preg_match('/.*' . $category->getName() . '.*/i', $alreadyCategory)) {
							$shouldAdd = false;
							break;
						}
					}
					//echo $category->getId() . " :: " . $category->getName() . " :: " . $category->getIsActive() . " :: " . $shouldAdd . "\n\n";
					if($shouldAdd) {
						$categories1 = Mage::getModel('catalog/category')->getCollection()
						->addAttributeToFilter('parent_id', array('eq' => $category->getId()))
						->addAttributeToSelect('id')
						->addAttributeToSelect('name')
						->addAttributeToSelect('url_key')
						->addAttributeToSelect('url')
						->addAttributeToSelect('is_active');
						if(count($categories1) == 0) {
							$shouldAdd = true;
							foreach ($this->escapeCategories as $escapeCategory) {
								if (strtolower($escapeCategory) == strtolower($category->getName())) {
									$shouldAdd = false;
									break;
								}
							}
							foreach ($already_added_categories as $alreadyCategory) {
								if (preg_match('/.*' . $category->getName() . '.*/i', $alreadyCategory)) {
									$shouldAdd = false;
									break;
								}
							}
							if($shouldAdd) {
								$main_categories[] = array(
									'value' => $category->getId(),
									'label' => $category->getName(),
								);
								$already_added_categories[] = $category->getName();
							}
						} else {
							foreach ($categories1 as $category1) {
								if ($category1->getIsActive()) { // Only pull Active categories
									$shouldAdd = true;
									foreach ($this->escapeCategories as $escapeCategory) {
										if (strtolower($escapeCategory) == strtolower($category1->getName())) {
											$shouldAdd = false;
											break;
										}
									}
									foreach ($already_added_categories as $alreadyCategory) {
										if (preg_match('/.*' . $category1->getName() . '.*/i', $alreadyCategory)) {
											$shouldAdd = false;
											break;
										}
									}
									//echo $category1->getId() . " :: " . $category1->getName() . " :: " . $category1->getIsActive() . " :: " . $shouldAdd . "\n\n";
									if($shouldAdd) {
										$main_categories[] = array(
											'value' => $category1->getId(),
											'label' => $category1->getName(),
										);
										$already_added_categories[] = $category1->getName();
									}
								}
							}
						}
					}
				} else {
					$shouldAdd = true;
					foreach ($this->escapeCategories as $escapeCategory) {
						if (strtolower($escapeCategory) == strtolower($category->getName())) {
							$shouldAdd = false;
							break;
						}
					}
					foreach ($already_added_categories as $alreadyCategory) {
						if (preg_match('/.*' . $alreadyCategory . '.*/i', $category->getName()) || preg_match('/.*' . $category->getName() . '.*/i', $alreadyCategory)) {
							$shouldAdd = false;
							break;
						}
					}
					if($shouldAdd) {
						$main_categories[] = array(
							'value' => $category->getId(),
							'label' => $category->getName(),
						);
						$already_added_categories[] = $category->getName();
					}
				}
			}
		}
		echo json_encode($main_categories); exit();
	}

	/*public function getTitlesAction($category_id) {
		if(!isset($_POST['id'])) {
			$category_id = 3;
		} else {
			$category_id = $_POST['id'];
		}

		$products = array();
		$category = Mage::getModel('catalog/category')->load($category_id);
		$collection = $category->getProductCollection()->addAttributeToSort('name');
		Mage::getModel('catalog/layer')->prepareProductCollection($collection);

		foreach ($collection as $product) {
			//if ($product->getIsActive()) { // Only pull Active products
				$products[] = array(
					'value' => $product->getId(),
					'label' => $product->getName(),
					'image' => $product->getSmallImageUrl(200,200),
					'url' => $product->getProductUrl(),
				);
			//}
		}

		echo json_encode($products); exit();
	}*/

	public function sendCustomRequestEmail($to, $name, $parameters){
		$emailTemplate  = Mage::getModel('core/email_template')
                        ->loadDefault($parameters['template']);
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', Mage::app()->getStore()->getStoreId()));
		$emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name', Mage::app()->getStore()->getStoreId()));

		$emailTemplate->send($to, $name, $parameters);
    }

	public function eventRequestAction() {
		if ($data = $this->getRequest()->getPost()) {
			if(isset($data['occasion_on_time']) && $data['occasion_on_time'] != '') {
				$data['occasion_on_time'] = date('H:i', strtotime($data['occasion_on_time'])) . ":00";
			}

			if(isset($data['occasion_on']) && $data['occasion_on'] != '') {
				$data['occasion_on'] = date('Y-m-d', strtotime($data['occasion_on']));
				$data['occasion_on'] = $data['occasion_on'] . " " . $data['occasion_on_time'];
			}

			$model = Mage::getModel('eventmanager/eventmanagercustom');		
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				if ($data['request'] == 'Custom Event') {
					$data['other_occasion'] = $data['comments'] = $data['budget'] = '';
					$data['occasion_on'] = (isset($data['occasion_on']) && $data['occasion_on'] != '')?$data['occasion_on']:'0000-00-00 00:00:00';
					$data['pandit'] = $data['samagri'] = $data['catering'] = $data['decoration'] = $data['venue'] = $data['technical'] = $data['event_management'] = 'no';
				}
				$data['status'] = 'new';
				$data['created'] = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
				$data['modified'] = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
				
				$model->setData($data);
				$model->save();
				
				if(isset($data['type']) && $data['type'] > 0) {
					$categories = Mage::getModel('catalog/category')->getCollection()
						->addAttributeToFilter('entity_id', array('eq' => $data['type']))
						->addAttributeToSelect('name');
					foreach ($categories as $category) {
						$data['category'] = $category->getName();
					}
				}

				if(isset($data['occasion']) && $data['occasion'] > 0) {
					$categories = Mage::getModel('catalog/category')->getCollection()
						->addAttributeToFilter('entity_id', array('eq' => $data['occasion']))
						->addAttributeToSelect('name', 'url');
					foreach ($categories as $category) {
						$data['subcategory'] = $category->getName();
						$data['url'] = $category->getUrl();
					}
				}

				//echo "<h1>Thank you for your interest. We will contact you soon.</h1><script language='javascript'>setTimeout(function() { window.location.href = '" . str_replace('.html', '/pooja-ingredients.html', $data['url']) . "';}, 3000);</script>";
				echo "<script language='javascript'>window.location.href = '" . str_replace('.html', '/pooja-ingredients.html', $data['url']) . "';</script>";
				
				$data['template'] = 'new_custom_event';
				$this->sendCustomRequestEmail($data['email'], $data['name'], $data);

				$data['template'] = 'new_custom_event_to_admin';
				$data['looking_for'] = ucfirst($data['looking_for']);
                                $data['cname'] = ucwords($data['name']);
                                $data['cemail'] = $data['email'];
				$categories = Mage::getModel('catalog/category')->getCollection()
				->addAttributeToFilter('entity_id', array('eq' => $data['type']))
				->addAttributeToSelect('name');
				foreach ($categories as $category) {
					$data['category'] = $category->getName();
				}

				$subcategories = Mage::getModel('catalog/category')->getCollection()
				->addAttributeToFilter('entity_id', array('eq' => $data['occasion']))
				->addAttributeToSelect('name');
				foreach ($subcategories as $subcategory) {
					$data['subcategory'] = $subcategory->getName();
				}

				$this->sendCustomRequestEmail(Mage::getStoreConfig('trans_email/ident_general/email', Mage::app()->getStore()->getStoreId()), Mage::getStoreConfig('trans_email/ident_general/name', Mage::app()->getStore()->getStoreId()), $data);
				exit();
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
	}
}