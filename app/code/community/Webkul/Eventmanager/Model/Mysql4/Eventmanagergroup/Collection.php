<?php
class Webkul_Eventmanager_Model_Mysql4_Eventmanagergroup_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init("eventmanager/eventmanagergroup");
    }

    public function getDuplicateGroupCode($groupCode) {
        $this->setConnection($this->getResource()->getReadConnection());
        $table = $this->getTable("eventmanager/eventmanagergroup");
        $select = $this->getSelect();
        $select->from(array("main_table" => $table), "group_id")
                ->where("group_code = ?", $groupCode);
        return $this;
    }

}