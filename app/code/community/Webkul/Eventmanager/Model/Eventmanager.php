<?php
class Webkul_Eventmanager_Model_Eventmanager extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init("eventmanager/eventmanager");
    }

    public function getAllAvailEventIds(){
        $collection = Mage::getResourceModel("eventmanager/eventmanager_collection")->getAllIds();
        return $collection;
    }

}