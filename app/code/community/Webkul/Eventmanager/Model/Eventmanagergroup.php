<?php
class Webkul_Eventmanager_Model_Eventmanagergroup extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init("eventmanager/eventmanagergroup");
    }

    public function getDataByGroupCode($groupCode) {
        $groupData = array();
        $eventData = array();
        $result = array("group_data"=>$groupData,"eventmanager_data"=>$eventData);
        $collection = Mage::getResourceModel("eventmanager/eventmanagergroup_collection");
        $collection->getSelect()->where("group_code = ?", $groupCode)->where("status = 1");
        foreach ($collection as $record) {
            $eventIds = $record->getEventIds();
            $eventModel = Mage::getModel("eventmanager/eventmanager");
            $eventData = $eventModel->getDataByEventIds($eventIds);
            $result = array("group_data" => $record, "eventmanager_data" => $eventData);
        }
        return $result;
    }

}