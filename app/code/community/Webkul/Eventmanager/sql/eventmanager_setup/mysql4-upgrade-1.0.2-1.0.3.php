<?php

$installer = $this;
$connection = $installer->getConnection();
 
$installer->startSetup();
 
$installer->getConnection()
    ->addColumn($this->getTable('webkul_eventmgrcustom'),
    'mobile',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'default' => null,
        'comment' => 'Mobile Number'
    )
);
 
$installer->endSetup();