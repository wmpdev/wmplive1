<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('webkul_eventmgr')};
CREATE TABLE {$this->getTable('webkul_eventmgr')} (
  `event_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `edate` date NULL,
  `body` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('webkul_eventmgrgroup')};
CREATE TABLE {$this->getTable('webkul_eventmgrgroup')} (
 `group_id` int(11) unsigned NOT NULL auto_increment,
 `event_ids` varchar(255) NOT NULL default '', 
 `group_title` varchar(255) NOT NULL default '', 
 `group_code` varchar(25) NOT NULL default '',
 `group_name` varchar(25) NOT NULL default '',
 `status` smallint(6) NOT NULL default '0',
 `cms_pages` varchar(255) NOT NULL,
 `created_time` DATETIME NULL,
 `update_time` DATETIME NULL,
 PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");
$installer->endSetup();