<?php
 //include 'mailchimp_post.php';
/**
 * Template Name: Sign Up Page
 *
 * @package WordPress

 */
session_start();
  get_header(); ?>
  
    <style>
  a#login51_pop:hover, a#join51_pop:hover {
    border-color: #eee;
}
.overlay51 {
    background-color: rgba(0, 0, 0, 0.6);
    bottom: 0;
    cursor: default;
    left: 0;
    opacity: 0;
    position: fixed;
    right: 0;
    top: 0;
    visibility: hidden;
    z-index: 1;

    -webkit-transition: opacity .5s;
    -moz-transition: opacity .5s;
    -ms-transition: opacity .5s;
    -o-transition: opacity .5s;
    transition: opacity .5s;
}
.overlay51:target {
    visibility: visible;
    opacity: 1;
	z-index:99999;
}
.popup51 {
  background-color: #fff;
    border: 3px solid #fff;
    display: inline-block;
    left: 50%;
    opacity: 0;
    padding: 15px;
    position: fixed;
    text-align: justify;
    top: 40%;
    visibility: hidden;
    z-index: 99999;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
	border-radius: 10px;
	border-radius: 10px;
	width: 35%;
	min-width:250px;
	-webkit-box-shadow: 0 0px 0 0 rgba(0, 0, 0, 0.4) inset;
    -moz-box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
    -ms-box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
    -o-box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
    box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
}
.overlay51:target+.popup51 {
    top: 50%;
    opacity: 1;
    visibility: visible;
}
.close51 {
    background-color: rgba(0, 0, 0, 0.8);
    height: 30px;
    line-height: 30px;
    position: absolute;
    right: 0;
    text-align: center;
    text-decoration: none;
    top: -15px;
    width: 30px;

    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    -ms-border-radius: 15px;
    -o-border-radius: 15px;
    border-radius: 15px;
}
.close51:before {
    color: rgba(255, 255, 255, 0.9);
    content: "X";
    font-size: 24px;
    text-shadow: 0 -1px rgba(0, 0, 0, 0.9);
}
.close51:hover {
    background-color: rgba(64, 128, 128, 0.8);
}
.popup51 p, .popup51 div {
    margin-bottom: 0;
}
.popup51 label {
    display: inline-block;
    text-align: left;
    width: 120px;
}
.popup51 input[type="text"], .popup input[type="password"] {
    border: 1px solid;
    border-color: #999 #ccc #ccc;
    margin: 0;
    padding: 2px;

    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -ms-border-radius: 2px;
    -o-border-radius: 2px;
    border-radius: 2px;
}
.popup51 input[type="text"]:hover, .popup51 input[type="password"]:hover {
    border-color: #555 #888 #888;
}


  
  
  </style> 
  
 <?php
  if(isset($_POST['signup']))
  {
 unset($_SESSION['gift_amount_xtra']);
 unset($_SESSION['title']);
 unset($_SESSION['fname']);
  unset($_SESSION['lname']);
  unset($_SESSION['address']);
 unset($_SESSION['country']);
   unset($_SESSION['city']);
  unset($_SESSION['citys']);
 unset($_SESSION['email']);
 unset($_SESSION['confirm_email']);
  unset($_SESSION['phone']);
  unset($_SESSION['birth_date']);
  
  unset($_SESSION['po_box']);

  unset($_SESSION['lexile']);
  unset($_SESSION['send_email']);
  unset($_SESSION['send_text_due']);
  unset($_SESSION['send_text_reserve']);
  unset($_SESSION['send_text_watch']);

  
  $fname = $_POST['first_name'];
  $lname = $_POST['last_name'];
  $title = $_POST['title'];
  $address = $_POST['address'];
  $country = $_POST['country'];
  if($_POST['city'] != "Other")
  {
  $city = $_POST['city'];
  }else{
   $city = $_POST['citys'];
  }
 
  $email = $_POST['email'];
  $confirm_email = $_POST['confirm_email'];
  $phone = $_POST['phone'];
  $birth = $_POST['birth_date'];
  $birth = implode("-", array_reverse(explode("/", $birth)));
  $po_box = $_POST['pobox'];

  $lexile = $_POST['lexile'];
  $contact_preference = $_POST['contact_preference'];
  $send_email = $_POST['send_email'];
  $item_due = $_POST['item_due'];
  $item_advance = $_POST['item_advance'];
  $send_text_due = $_POST['send_text_due'];
  $send_text_reserve = $_POST['send_text_reserve'];
  $send_text_watch = $_POST['send_text_watch'];
  $plan = $_POST['plan'];
  $date = date("Y/m/d");
  $captcha = $_POST['captcha'];
  $total_amount = $_POST['total_amount'];
  
  $student = $_POST['student'];
  $payment = $_POST['payment'];
  if($payment == '1')
  $payment_option = "Anually";
  else
  $payment_option = "EMI";
  
  
  $today = date("Y-m-d");
  if($today > $birth)
  $dobcheck = "failed";
  if($dobcheck == 'failed')                                               {
  if($email == $confirm_email)
  {
  
  if($captcha == $_SESSION['captcha']['code'])
  {
  $result11 = mysql_query("select * from signup WHERE email = '$email'");
  $num = mysql_num_rows($result11);
  $row11 = mysql_fetch_array($result11);
  //echo "shubham".$num;
   if($num == '0')
  {
$data = mysql_query("INSERT INTO signup (first_name, last_name, title, address, po_box, country, city, email, phone, birth_date, lexile, contact_preference, send_email, item_due, item_advance, send_text_due, send_text_reserve, send_text_watch, plan, active_date, total_amount,payment_option, student) VALUES ('$fname', '$lname', '$title', '$address', '$po_box', '$country', '$city', '$email', '$phone', '$birth', '$lexile', '$contact_preference', '$send_email', '$item_due', '$item_advance', '$send_text_due', '$send_text_reserve', '$send_text_watch', '$plan', '$date', '$total_amount','$payment_option','$student')"); 
 
$fetch_id = mysql_query("select * from signup ORDER BY id DESC LIMIT 1");
 $result_id = mysql_fetch_array($fetch_id);
 $id = $result_id['id']; 
  
if($data){

$status  = "User added successfully!";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";  
$headers .= 'From: Reademption - Web Alert <webalert@reademption.com>' . "\r\n";
$headers .= "Reply-To: hello@reademption.com <hello@reademption.com>\r\n"; 
$headers .= "Return-Path: hello@reademption.com <hello@reademption.com>\r\n";
$headers .= "X-Priority: 3\r\n"; 
$headers .= "X-MSMail-Priority: Normal\r\n"; 

$subjectm = "New Signup on Reademption";  
$toemail = "hello@reademption.com,chhabria.nitin@gmail.com,shubham.xtreamerider@gmail.com";  
$message = "<b>First Name : </b> $fname<br> <b>Last Name : </b> $lname<br> <b>Email : </b> $email<br> <b>Phone : </b> $phone <br/>";  
$data = mail($toemail,$subjectm,$message,$headers);


if(isset($_POST['newssignup']))   {
$list_id = "30f6ccac95";

$merge_vars = array("FNAME" => $fname,"LNAME" => $lname,"MMERGE7" => $phone,"MMERGE4"=>$birth,"MMERGE3" => $address);
$result = subscribe($apikey,$list_id,$email,$merge_vars);

                                  }
								  
$list_id = "35a74b0ebb";
$double_optin = "false";
$merge_vars = array("FNAME" => $fname,"LNAME" => $lname,"MMERGE3" => $plan);
$result = subscribe($apikey,$list_id,$email,$merge_vars,$double_optin);								  
//$subject = "Welcome";
//$email_to = $email;
//send_email($subject,$email_to);	
//header('Location: http://demo.tecmerch.com/reademption/payment/?payment='.$id.'');

if(isset($_SESSION['gift_status']))
{
echo "<script>window.open('http://www.reademption.com/payment/?payment=".$id."&gift=".$_SESSION['gift_status']."&g=".$_SESSION['gift_id']."','_self')</script>";
}
else{
echo "<script>window.open('http://www.reademption.com/payment/?payment=".$id."','_self')</script>";
}
  //echo'<meta http-equiv="refresh" content="0; URL=http://demo.tecmerch.com/reademption/payment/?payment='.$id.'">';
  
  //echo "<script>window.open('http://demo.tecmerch.com/reademption/payment/?payment=".$id.",'_self')</script>";
  }
  
  
  echo "entered1";

  
  }
  else{
  
  if($row11['deleted'] == '1')
  {
  echo "entered";
mysql_query("UPDATE signup SET first_name = '$fname', last_name = '$lname', title = '$title', address = '$address', po_box = '$po_box', country = '$country', city = '$city', email = '$email', phone = '$phone', birth_date = '$birth', lexile = '$lexile', contact_preference = '$contact_preference', send_email = '$send_email', item_due = '$item_due', item_advance = '$item_advance', send_text_due = '$send_text_due', send_text_reserve = '$send_text_reserve', send_text_watch = '$send_text_watch', plan = '$plan', active_date= '$date', total_amount = '$total_amount',payment_option = '$payment_option', student = '$student' , deleted = '0' WHERE email = '$email'"); 
 
if(isset($_SESSION['gift_status']))
{
echo "<script>window.open('http://www.reademption.com/payment/?payment=".$row11['id']."&gift=".$_SESSION['gift_status']."&g=".$_SESSION['gift_id']."','_self')</script>";
}
 else{
 echo "<script>window.open('http://www.reademption.com/payment/?payment=".$row11['id']."','_self')</script>";
 } 
  
$mdb = date("d/m", strtotime($birth));								  
if(isset($_POST['newssignup']))   {
$list_id = "30f6ccac95";
$merge_vars = array("FNAME" => $fname,"LNAME" => $lname,"PHONE" => $phone,"BIRTHDAY"=>$mdb,"ADDRESS" => $address);
$result = subscribe($apikey,$list_id,$email,$merge_vars);
                                  }
$list_id = "35a74b0ebb";
$double_optin = "false";
$merge_vars = array("FNAME" => $fname,"LNAME" => $lname,"MMERGE3" => $plan,"BIRTHDAY"=>$mdb,"PHONE" => $phone);
$result = subscribe($apikey,$list_id,$email,$merge_vars,$double_optin);	
$subject = "Welcome to Reademption";
$email_to = $email;
send_email($subject,$email_to);	
  echo "entered2";

  }
  elseif($row11['activate'] == '0')
  {
  
  mysql_query("UPDATE signup SET first_name = '$fname', last_name = '$lname', title = '$title', address = '$address', po_box = '$po_box', country = '$country', city = '$city', email = '$email', phone = '$phone', birth_date = '$birth', lexile = '$lexile', contact_preference = '$contact_preference', send_email = '$send_email', item_due = '$item_due', item_advance = '$item_advance', send_text_due = '$send_text_due', send_text_reserve = '$send_text_reserve', send_text_watch = '$send_text_watch', plan = '$plan', active_date= '$date', total_amount = '$total_amount',payment_option = '$payment_option',student = '$student' WHERE email = '$email'"); 
  $mdb = date("d/m", strtotime($birth));								  
if(isset($_POST['newssignup']))   {
$list_id = "30f6ccac95";
$merge_vars = array("FNAME" => $fname,"LNAME" => $lname,"PHONE" => $phone,"BIRTHDAY"=>$mdb,"ADDRESS" => $address);
$result = subscribe($apikey,$list_id,$email,$merge_vars);
                                  }
$list_id = "35a74b0ebb";
$double_optin = "false";
$merge_vars = array("FNAME" => $fname,"LNAME" => $lname,"MMERGE3" => $plan,"BIRTHDAY"=>$mdb,"PHONE" => $phone);
$result = subscribe($apikey,$list_id,$email,$merge_vars,$double_optin);	
$subject = "Welcome to Reademption";
$email_to = $email;
send_email($subject,$email_to);	
  //header('Location: http://demo.tecmerch.com/reademption/payment/?payment='.$row11['id'].'');
 //echo'<meta http-equiv="refresh" content="0; URL=http://demo.tecmerch.com/reademption/payment/?payment='.$row11['id'].'">';
if(isset($_SESSION['gift_status']))
{
echo "<script>window.open('http://www.reademption.com/payment/?payment=".$row11['id']."&gift=".$_SESSION['gift_status']."&g=".$_SESSION['gift_id']."','_self')</script>";
}
 else{
 echo "<script>window.open('http://www.reademption.com/payment/?payment=".$row11['id']."','_self')</script>";
 }
echo "entered3";
 }else{
 
 
 $status = "Email Already Registered";
 }
  
      }
  
  }
  else{
  
  $status ="Incorrect Code. Pls re-enter.";
  
  }
  
  
  
  }
  
  else{
  
  $status = "Emails doesn't matches";
  
      }
                                                                                               }
																							   
																							   else{
  
  $status = "Invalid Date";
  
      }
  
  }
  
  
  
  ?> 
  
  <?php
  
  if($_GET['gift_status'] == 'gift_voucher')
  {
  
  if(isset($_SESSION['gift_amount_xtra']))
  
  {
  $gift_amount = $_SESSION['tam'];
  }
  else{
  $gift_amount = $_SESSION['amount'];
  }
  
  }
  
   if(isset($_POST['voucher']))
   {
   
     $_SESSION['title'] = $_POST['title'];
    $_SESSION['fname']= $_POST['first_name'];
  $_SESSION['lname'] = $_POST['last_name'];
  $_SESSION['address'] = $_POST['address'];
   
  $_SESSION['country'] = $_POST['country'];
  
   $_SESSION['city'] = $_POST['city'];
   $_SESSION['citys'] = $_POST['citys'];
  $_SESSION['email'] = $_POST['email'];
  $_SESSION['confirm_email'] = $_POST['confirm_email'];
  $_SESSION['phone'] = $_POST['phone'];
  $_SESSION['birth_date'] = $_POST['birth_date'];
  
  $_SESSION['po_box'] = $_POST['pobox'];

  $_SESSION['lexile'] = $_POST['lexile'];
  
  $_SESSION['contact_preference'] = $_POST['contact_preference'];
  $_SESSION['send_email'] = $_POST['send_email'];
  $_SESSION['item_due'] = $_POST['item_due'];
  $_SESSION['item_advance'] = $_POST['item_advance'];
  $_SESSION['send_text_due'] = $_POST['send_text_due'];
  $_SESSION['send_text_reserve'] = $_POST['send_text_reserve'];
  $_SESSION['send_text_watch'] = $_POST['send_text_watch'];
  
  
  
  
   
  $voucher_no = trim($_POST['voucher_no']);
$_SESSION['voucher'] = trim($voucher_no);

$voucher_detailed = mysql_query("SELECT * FROM gift_card WHERE voucher = '$voucher_no'");
$voucher_check = mysql_num_rows($voucher_detailed);
if($voucher_check != '0')
{
$voucher_row = mysql_fetch_array($voucher_detailed);
extract($voucher_row);
if($activate == '1') 
{
$_SESSION['gift_status'] = $gift_status;

$_SESSION['gift_id'] = $id;
if($gift_status == "gift_plan")
{
echo'<script>window.open("http://reademption.com/signup?plan='.$plan.'&gift_status='.$gift_status.'","_self")</script>';
}
if($gift_status == "gift_voucher")
{
$exact_gift_amount = $amount;

$data = mysql_query("select * from plan WHERE id = '1'");
	$show = mysql_fetch_array($data);
	extract($show);
	
	switch($_GET['plan'])
		{
		
		case "sheldon":
		
		$price = $sheldon;
		$period = $period_sheldon;
		break;
		
		case "lindgren":
		
		$price = $lindgren;
		$period = $period_lindgren;
		break;
		
		case "tolstoy":
		
		$price = $tolstoy;
		$period = $period_tolstoy;
		break;
		
		case "rowling":
		
		$price = $rowling;
		$period = $period_rowling;
		break;
		
		}
		
	$tam = $price * $period;
    $_SESSION['tam'] = $tam;
	$_SESSION['amount'] = $amount;
   if($tam < $exact_gift_amount)
   {
   $_SESSION['gift_amount_xtra'] = 'yes';
   
  }
  
  $_SESSION['gift_amount'] = $amount;
echo'<script>window.open("http://reademption.com/signup?plan=sheldon&gift='.$id.'&gift_status='.$gift_status.'","_self")</script>'; 



}
$voucher_status = "Voucher no. is applied";
  
} 
else{

if($activate == '2')

{
$voucher_status ="Voucher Code Expire";
}


elseif($activate == '3')

{
$voucher_status ="Voucher Code Used";
}
elseif($activate == '0')

{
$voucher_status ="Voucher Inactive";
}

elseif($activate == '4')

{
$voucher_status ="Looks like there is an issue with the eGift Card. Pls call Reademption on 043212521 or emailhello@reademption.com";
}


}

}

else{

$voucher_status = "Voucher no. is Invalid";
}
  
}  
  ?>
  
  
  <?php
  if(isset($_GET['sign']))
  {
  unset($_SESSION['gift_amount_xtra']);
  unset($_SESSION['voucher']);
  unset($_SESSION['gift_status']);
  unset($_SESSION['gift_amount']);
  unset($_SESSION['gift_id']);
  unset($_SESSION['tam']);
  unset($_SESSION['amount']);
  echo'<script>window.open("http://www.reademption.com/signup?plan='.$_GET['plan'].'","_self")</script>'; 

  }
  
  if(isset($_GET['clearform']))
  {
  unset($_SESSION['gift_amount_xtra']);
  unset($_SESSION['voucher']);
  unset($_SESSION['gift_status']);
  unset($_SESSION['gift_amount']);
  unset($_SESSION['gift_id']);
  unset($_SESSION['title']);
 unset($_SESSION['fname']);
  unset($_SESSION['lname']);
  unset($_SESSION['address']);
   
 unset($_SESSION['country']);
   unset($_SESSION['city']);
  unset($_SESSION['citys']);
 unset($_SESSION['email']);
 unset($_SESSION['confirm_email']);
  unset($_SESSION['phone']);
  unset($_SESSION['birth_date']);
  
  unset($_SESSION['po_box']);

  unset($_SESSION['lexile']);
  unset($_SESSION['send_email']);
  unset($_SESSION['send_text_due']);
  unset($_SESSION['send_text_reserve']);
  unset($_SESSION['send_text_watch']);
  echo'<script>window.open("http://www.reademption.com/signup?plan='.$_GET['plan'].'","_self")</script>'; 

  }
  ?>
  
  <style>
  
  .contacts-form input[type="text"],.contacts-form input[type="email"] , .contacts-form textarea , .comment-form input[type="text"],.comment-form input[type="email"] , .comment-form textarea{
	width:100%; 
	max-width:100%
}

  </style>

 
  
  
     <style>
  a#login12_pop:hover, a#join12_pop:hover {
    border-color: #eee;
}
.overlay12 {
    background-color: rgba(0, 0, 0, 0.6);
    bottom: 0;
    cursor: default;
    left: 0;
    opacity: 0;
    position: fixed;
    right: 0;
    top: 0;
    visibility: hidden;
    z-index: 1;

    -webkit-transition: opacity .5s;
    -moz-transition: opacity .5s;
    -ms-transition: opacity .5s;
    -o-transition: opacity .5s;
    transition: opacity .5s;
}
.overlay12:target {
    visibility: visible;
    opacity: 1;
	z-index:99999;
}
.popup12 {
    background-color: #fff;
    border: 3px solid #fff;
    display: inline-block;
    left: 50%;
    opacity: 0;
    padding: 15px;
    position: fixed;
    text-align: justify;
    top: 40%;
    visibility: hidden;
    z-index: 99999;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    border-radius: 10px;
  border-radius: 10px;
width: 45%;

-webkit-box-shadow: 0 0px 0 0 rgba(0, 0, 0, 0.4) inset;
    -moz-box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
    -ms-box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
    -o-box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
    box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4) inset;
}
.overlay12:target+.popup12 {
    top: 50%;
    opacity: 1;
    visibility: visible;
}
.close12 {
    background-color: #8CC341;
    height: 25px;
    line-height: 26px;
    position: absolute;
    right: -13px;
    text-align: center;
    text-decoration: none;
    top: -12px;
    width: 25px;

    -webkit-border-radius: 15px;
    -moz-border-radius: 15px;
    -ms-border-radius: 15px;
    -o-border-radius: 15px;
    border-radius: 15px;
}
.close12:before {
    color: rgba(255, 255, 255, 0.9);
    content: "X";
    font-size: 13px;
    vertical-align: text-bottom;
}

.popup12 p, .popup12 div {
    margin-bottom: 10px;
}
.popup12 label {
    display: inline-block;
    text-align: left;
    width: 120px;
}
.popup12 input[type="text"], .popup input[type="password"] {
    border: 1px solid;
    border-color: #999 #ccc #ccc;
    margin: 0;
    padding: 2px;

    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    -ms-border-radius: 2px;
    -o-border-radius: 2px;
    border-radius: 2px;
}
.popup12 input[type="text"]:hover, .popup11 input[type="password"]:hover {
    border-color: #555 #888 #888;
}


  
  
  </style>
  
  
  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/bootstrap-inputmask.min.js"></script>

   
   
  <script language="javascript" type="text/javascript">
	
    function gift_card()
	{
	document.form.first_name.removeAttribute("required", "required");
	document.form.last_name.removeAttribute("required", "required");
	document.form.address.setAttribute("disabled", "disabled");
	document.form.email.removeAttribute("required", "required");
	document.form.confirm_email.removeAttribute("required", "required"); 
	document.form.phone.removeAttribute("required", "required"); 
	document.form.birth_date.removeAttribute("required", "required"); 
	document.form.captcha.removeAttribute("required", "required"); 
	document.form.country.setAttribute("disabled", "disabled");
	document.form.city.setAttribute("disabled", "disabled");
	document.getElementById('typeaddress').style.display = 'none'; 
	}
	
     </script>
   
  
  <script>
  $(function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "fade",
        duration: 1000
      },
      hide: {
        effect: "fade",
        duration: 1000
      }
    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });
  });
  </script>
    
  <script>
function validatemyForm()  {
var a = document.form.address.value;
var add = 0;;
var country = 0;
var city = 0;
if(a=="")
{
//alert("Please Enter Your Details Here");
document.getElementById('typeaddress').style.display = 'block';

document.form.address.focus();
add = 1;
}
	
	
	
	if(document.getElementById('city').value == "Select City") {
        document.getElementById('cityselect').style.display = 'block';
		city = 1;
    } 
	
	if(document.getElementById('country').value == "Select Country") {
        document.getElementById('countryselect').style.display = 'block';
        country = 1;
    } 
	
	if(add == 1 || city == 1 || country == 1) {
		//alert('wrong');

	return false; 
	}
	else
	return true;
	
	
                          }
						  
	function othercity() {					  
	if(document.getElementById('city').value == "Other") {
       document.getElementById('test').style.display = 'block';
	   document.form.citys.setAttribute("required", "required");

	
    } else {
        document.getElementById('test').style.display = 'none';
		document.form.citys.removeAttribute("required", "required");

    }					  }
	
	
	function submitv() {		
		document.form.captcha.removeAttribute("required", "required");
				document.form.first_name.removeAttribute("required", "required");
				document.form.last_name.removeAttribute("required", "required");
				document.form.address.removeAttribute("required", "required");
				document.form.pobox.removeAttribute("required", "required");
				document.form.email.removeAttribute("required", "required");
				document.form.confirm_email.removeAttribute("required", "required");
				document.form.phone.removeAttribute("required", "required");
				document.form.birth_date.removeAttribute("required", "required");
				document.getElementById('country').id = 'dummy1';
								document.getElementById('city').id = 'dummy2';
								document.getElementById('typeaddress').id = 'dummy3';




		//document.forms["form"].submit();
	
					   }
	
	
	
	
</script>  
 

   
  <script type="text/javascript">
     var RecaptchaOptions = {
        theme : 'white',
        custom_theme_widget: 'recaptcha_widget'
     };
</script>  
<style>
.input-recaptcha{
 width:172px;   
}
</style>
<?php if(isset($_POST['voucher_gift']))
{


}
?>


<section id="section_0" class="section full-width section-border-no section-height-content section-parallax-no section-bgtype-image section-fixed-background-no  section-bgstyle-norepeat section-triangle-no triangle-location-top section-overlay-no section-overlay-dot-no  bg-behing-search" data-parallax_speed="0.8" style="padding-top:70px;padding-bottom:70px;background-color:#ffffff;background-image:url(http://www.reademption.com/wp-content/uploads/2013/12/cropped-books-wide-variation-53.jpg);" data-video-ratio=""><div class="section-overlay"></div><div class="container section-content"><div class="row-fluid"></div></div></section>
<section id="section_0" class="section post-content" style="padding-top:0px;padding-bottom:0px;">
    <div class="container">
     <div class="row-fluid" style="margin-top:40px;"> 
        <div class="row-fluid element-padding-default ">
	<div class="span12">
		<div class="inner-content">
			
		</div> 
	</div> 
</div><div class="row-fluid element-padding-default signup-page">
<h4 class="title textcenter default divider-dark divider-default color-default" style="margin-bottom:20px"><span>Sign-up form</span>
	</h4>
 
 <!--<a href="#x" class="overlay51" id="join51_form" ></a>

 <div class="popup51 common-popup" >
 <p>If you have a voucher code then enter voucher code in text box</p>
  <a class="close12" href="#close" ></a>
 </div> -->
	<div class="column-text clearfix">
		

	</div> 
	
	<div class="gap  hidden-mobile" style="height:20px;line-height:20px;"></div>
	<div class="span3 plan-pricing-mobile hidden-desktop hidden-tablet">
		<div class="inner-content">
		
<form action="" method="Get">
		
		<?php if($_SESSION['gift_status'] == "gift_plan") { ?>
		<h5 style="margin-bottom:0;font-size: 18px;margin-bottom: 0;">Selected Plan
<input type="text" name="plan" class="plan-select-box" value=": <?php echo $_GET[plan]; ?>" 
disabled style="text-transform: uppercase;border:0px;display: inline-block;font-size: 17px;width:51%;font-weight: 700;"></h5>
<?php } elseif($_SESSION['gift_status'] == "gift_voucher") {?>
<h5 style="margin-bottom:0;">Selected Plan</h5>
           <select name="plan" class="plan-select-box" onchange="this.form.submit()" style="width:100%">
		  <option <?php if($_GET[plan] == 'sheldon') echo"selected"; ?> value="sheldon">SHELDON</option>
        <option <?php if($_GET[plan] == 'lindgren') echo"selected"; ?>  value="lindgren">LINDGREN</option>
        <option <?php if($_GET[plan] == 'tolstoy') echo"selected"; ?>  value="tolstoy">TOLSTOY</option>
        <option <?php if($_GET[plan] == 'rowling') echo"selected"; ?>  value="rowling">ROWLING</option>
    </select>
	<input type="hidden" name="gift" value="<?php echo $_GET['gift']; ?>">
	<input type="hidden" name="gift_status" value="<?php echo $_GET['gift_status']; ?>">
		<?php } else { ?> 
		 
    <select name="plan" class="plan-select-box" onchange="this.form.submit()" style="width:100%">
		  <option <?php if($_GET[plan] == 'sheldon') echo"selected"; ?> value="sheldon">SHELDON</option>
        <option <?php if($_GET[plan] == 'lindgren') echo"selected"; ?>  value="lindgren">LINDGREN</option>
        <option <?php if($_GET[plan] == 'tolstoy') echo"selected"; ?>  value="tolstoy">TOLSTOY</option>
        <option <?php if($_GET[plan] == 'rowling') echo"selected"; ?>  value="rowling">ROWLING</option>
    </select>
	<?php } ?>
</form>
		<br> 
		
		 <?php 
		 switch($_GET['plan'])
		{
		
		case "sheldon":
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4 ">
		

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>SHELDON</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>120'.$starter.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$starter.'in monthly instalments</div></div>
		</div>
			<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>
<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		<span style="text-decoration: line-through; color: #ff0606;">Delivered to doorstep</span>
	</li>
</ul>
						

	</div></div>

	

	

	</div>
	</div>';
	
	break;
	
	case "lindgren" :
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		 

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>LINDGREN</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>170'.$basic.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$basic.'in monthly instalments</div></div>
		</div>
				<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>

<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		Delivered to doorstep
	</li>
</ul>
						</div></div></div></div>';

	break;
	
	case "tolstoy" :
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>TOLSTOY</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>100'.$premium.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$premium.'in monthly instalments</div></div>
		</div>
				<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>

	<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		<span style="text-decoration: line-through; color: #ff0606;">Delivered to doorstep</span>
	</li>
</ul>
						</div></div></div></div>';
	
	break;
	
	case "rowling" :
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>ROWLING</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>150'.$extreme.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$extreme.'in monthly instalments</div></div>
		</div>
				<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>
<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		Delivered to doorstep
	</li>
</ul>
						</div></div>
						</div></div>
						';
	
	

	}?>

	

	</div>
	
	<div class="span12 hidden-mobile" style="">
	<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		

	<div class="field-wrap"><div class="signup-page-total">
	<ul class="feature-list">
	<li class="included-text"><strong>Membership Fees</strong>    <span><?php echo $price * $period; ?></span ></li>
	
	<li class="included-text"><strong>Deposit</strong> <span ><?php echo $deposit; ?> </span ></li>
	<li class="included-text"><strong >Total </strong>  <span id="total"><?php $tot = $price * $period + $deposit; echo $tot; ?></span></li>
	</ul>
	</div></div> </div></div>
	
	</div>
	
	
	</div>
	<script>
	
	function check_city() {
    
	
}
 
 
 function check_country(){
 
 
 
 }
	
	</script>
	
	<script type="text/javascript">
    function confirmEmail() {
        var email = document.getElementById("email").value;
        var confemail = document.getElementById("confemail").value;
        if(email != confemail) {
            //alert('Email Not Matching!');
					document.getElementById('confirmemail').style.display = 'block' ;

			document.getElementById("confirmemail").innerHTML = "Email ids do not match. Pls re-enter.";

        }
		else
		document.getElementById('confirmemail').style.display = 'none' ;
		//document.getElementById("confirmemail").remove();

    }
</script>
	
	<div class="span7"style="float:left;">
		<!-- <h5>Please Enter Following Details</h5>-->
		<div class="inner-content">		 		
<form action="" method="POST" class="contacts-form field-icons-no validate-form" name = "form"  onsubmit="return validatemyForm()" >
<div class="sign-up-page-inputs ">
       <div class="span2 title-of-name">
         <div class="field-wrap">
		 <div class="control-wrap">
	 <h5 style = "display:inline;">Title</h5>
     <font color="red" style="display:inline;">*</font>	 
		 <select name="title">
		 
		 
         <option value="Mrs" <?php if($_SESSION['title'] == 'Mrs' or $_POST['title'] == 'Mrs'){ echo "selected";}?>> Mrs</option>
		 <option value="Mr" <?php if($_SESSION['title'] == 'Mr' or $_POST['title'] == 'Mr' ){  echo "selected";} ?>>Mr</option>
		 <option value="Ms" <?php if($_SESSION['title'] == 'Ms' or $_POST['title'] == 'Ms'){ echo "selected";} ?>>Ms</option>
		 
		 <option value="Dr" <?php if($_SESSION['title'] == 'Dr' or $_POST['title'] == 'Mrs'){ echo "selected";} ?>>Dr</option>
		 </select>

		 </div>
		 </div>
  </div>
  
<div class="span5 input-of-name">
<div class="field-wrap">
<div class="control-wrap">	
<h5 style = "display:inline;">First name</h5><font color="red" style="display:inline;">*</font>		 
<input type="text" maxlength="100" value = "<?php 
if(isset($fname))
$sfname =  $fname;
if(isset($_SESSION['fname'])) 
$sfname =  $_SESSION['fname'];
echo $sfname; ?>" required="required" name="first_name"  placeholder="" style="" onkeypress="return onlyAlphabets(event,this);">
</div>
		 </div>
		 </div>
		 <div class="span5 input-of-name">
		 <div class="field-wrap">
		 <div class="control-wrap">	
 <h5 style = "display:inline;">Last  name</h5><font color="red" style="display:inline;">*</font>			 
		 <input type="text" maxlength="100" name="last_name" value = "<?php if(isset($lname))
$slname = $lname;
if(isset($_SESSION['lname'])) 
$slname =  $_SESSION['lname'];
		 echo $slname; ?>" required="required"  placeholder="" style="" onkeypress="return onlyAlphabets(event,this);">
		 </div>
		 </div>
		 </div>
		 
		
		    <div class="span12 sgnup" style="">
		    <div class="span12 sgnup" style="">
		    
			<div class="field-wrap">
			<div class="control-wrap" style="margin-bottom: 2px;">
			<h5 style = "display:inline;">Address</h5><font color="red" style="display:inline;">*</font>	
			<?php if(isset($address))$saddress = $address;if(isset($_SESSION['address']))$saddress = $_SESSION['address'];?>
			<textarea type="textarea" maxlength="5000" name="address" required  placeholder="" style="min-height: 50px;max-height: 50px;"><?php if(isset($saddress)) echo $saddress; ?></textarea>           		
			</div>
			
			
			</div>
			<div class="span12" style="display:none;" id = "typeaddress" >
					 <font color = "red">Please enter your address</font>
                     </div>	
			</div>
			
			</div>
			
			 <div class="span12 sgnup" style="">
			

 <div class = "span4">
	
		 			<div class="field-wrap">

		 <div class="control-wrap">
			 <h5 style = "display:inline;background: none;">P.O.Box</h5>	
			<input type="text" maxlength="7" name="pobox" value = "<?php 
			if(isset($po_box)) 
			{$spo_box = $po_box; }
			if($_SESSION['po_box'])
			$spo_box = $_SESSION['po_box'];
			echo $spo_box; ?>" placeholder="" style="" onkeypress="return isNumber(event)">
			</div>
			</div>
		 
		 	
		</div>
	
		<div class="span4">
<div class="field-wrap">
<div class="control-wrap">
<h5 style = "display:inline;background: none;">Country</h5><font color="red" style="display:inline;">*</font>
<select name="country" id="country" style="width:100%" > 



<?php if(isset($_POST['country'])) { ?>
<option value="<?php echo $_POST['country'];  ?>" selected="selected"><?php echo $_POST['country']; ?></option> 
<?php } else { ?> 
<?php if(isset($_SESSION['country'])) { ?>
<option value="<?php echo $_SESSION['country']; ?>" selected="selected"><?php echo $_SESSION['country']; ?></option> 
<?php } else { ?>
<option value="Select Country" selected="selected">Select Country</option>
<?php } ?>

<?php } ?>
<option value="United Arab Emirates">United Arab Emirates</option> 
<option value="United States">United States</option> 
<option value="United Kingdom">United Kingdom</option> 
<option value="Afghanistan">Afghanistan</option> 
<option value="Albania">Albania</option> 
<option value="Algeria">Algeria</option> 
<option value="American Samoa">American Samoa</option> 
<option value="Andorra">Andorra</option> 
<option value="Angola">Angola</option> 
<option value="Anguilla">Anguilla</option> 
<option value="Antarctica">Antarctica</option> 
<option value="Antigua and Barbuda">Antigua and Barbuda</option> 
<option value="Argentina">Argentina</option> 
<option value="Armenia">Armenia</option> 
<option value="Aruba">Aruba</option> 
<option value="Australia">Australia</option> 
<option value="Austria">Austria</option> 
<option value="Azerbaijan">Azerbaijan</option> 
<option value="Bahamas">Bahamas</option> 
<option value="Bahrain">Bahrain</option> 
<option value="Bangladesh">Bangladesh</option> 
<option value="Barbados">Barbados</option> 
<option value="Belarus">Belarus</option> 
<option value="Belgium">Belgium</option> 
<option value="Belize">Belize</option> 
<option value="Benin">Benin</option> 
<option value="Bermuda">Bermuda</option> 
<option value="Bhutan">Bhutan</option> 
<option value="Bolivia">Bolivia</option> 
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
<option value="Botswana">Botswana</option> 
<option value="Bouvet Island">Bouvet Island</option> 
<option value="Brazil">Brazil</option> 
<option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
<option value="Brunei Darussalam">Brunei Darussalam</option> 
<option value="Bulgaria">Bulgaria</option> 
<option value="Burkina Faso">Burkina Faso</option> 
<option value="Burundi">Burundi</option> 
<option value="Cambodia">Cambodia</option> 
<option value="Cameroon">Cameroon</option> 
<option value="Canada">Canada</option> 
<option value="Cape Verde">Cape Verde</option> 
<option value="Cayman Islands">Cayman Islands</option> 
<option value="Central African Republic">Central African Republic</option> 
<option value="Chad">Chad</option> 
<option value="Chile">Chile</option> 
<option value="China">China</option> 
<option value="Christmas Island">Christmas Island</option> 
<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
<option value="Colombia">Colombia</option> 
<option value="Comoros">Comoros</option> 
<option value="Congo">Congo</option> 
<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
<option value="Cook Islands">Cook Islands</option> 
<option value="Costa Rica">Costa Rica</option> 
<option value="Cote D'ivoire">Cote D'ivoire</option> 
<option value="Croatia">Croatia</option> 
<option value="Cuba">Cuba</option> 
<option value="Cyprus">Cyprus</option> 
<option value="Czech Republic">Czech Republic</option> 
<option value="Denmark">Denmark</option> 
<option value="Djibouti">Djibouti</option> 
<option value="Dominica">Dominica</option> 
<option value="Dominican Republic">Dominican Republic</option> 
<option value="Ecuador">Ecuador</option> 
<option value="Egypt">Egypt</option> 
<option value="El Salvador">El Salvador</option> 
<option value="Equatorial Guinea">Equatorial Guinea</option> 
<option value="Eritrea">Eritrea</option> 
<option value="Estonia">Estonia</option> 
<option value="Ethiopia">Ethiopia</option> 
<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
<option value="Faroe Islands">Faroe Islands</option> 
<option value="Fiji">Fiji</option> 
<option value="Finland">Finland</option> 
<option value="France">France</option> 
<option value="French Guiana">French Guiana</option> 
<option value="French Polynesia">French Polynesia</option> 
<option value="French Southern Territories">French Southern Territories</option> 
<option value="Gabon">Gabon</option> 
<option value="Gambia">Gambia</option> 
<option value="Georgia">Georgia</option> 
<option value="Germany">Germany</option> 
<option value="Ghana">Ghana</option> 
<option value="Gibraltar">Gibraltar</option> 
<option value="Greece">Greece</option> 
<option value="Greenland">Greenland</option> 
<option value="Grenada">Grenada</option> 
<option value="Guadeloupe">Guadeloupe</option> 
<option value="Guam">Guam</option> 
<option value="Guatemala">Guatemala</option> 
<option value="Guinea">Guinea</option> 
<option value="Guinea-bissau">Guinea-bissau</option> 
<option value="Guyana">Guyana</option> 
<option value="Haiti">Haiti</option> 
<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
<option value="Honduras">Honduras</option> 
<option value="Hong Kong">Hong Kong</option> 
<option value="Hungary">Hungary</option> 
<option value="Iceland">Iceland</option> 
<option value="India">India</option> 
<option value="Indonesia">Indonesia</option> 
<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
<option value="Iraq">Iraq</option> 
<option value="Ireland">Ireland</option> 
<option value="Israel">Israel</option> 
<option value="Italy">Italy</option> 
<option value="Jamaica">Jamaica</option> 
<option value="Japan">Japan</option> 
<option value="Jordan">Jordan</option> 
<option value="Kazakhstan">Kazakhstan</option> 
<option value="Kenya">Kenya</option> 
<option value="Kiribati">Kiribati</option> 
<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
<option value="Korea, Republic of">Korea, Republic of</option> 
<option value="Kuwait">Kuwait</option> 
<option value="Kyrgyzstan">Kyrgyzstan</option> 
<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
<option value="Latvia">Latvia</option> 
<option value="Lebanon">Lebanon</option> 
<option value="Lesotho">Lesotho</option> 
<option value="Liberia">Liberia</option> 
<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
<option value="Liechtenstein">Liechtenstein</option> 
<option value="Lithuania">Lithuania</option> 
<option value="Luxembourg">Luxembourg</option> 
<option value="Macao">Macao</option> 
<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
<option value="Madagascar">Madagascar</option> 
<option value="Malawi">Malawi</option> 
<option value="Malaysia">Malaysia</option> 
<option value="Maldives">Maldives</option> 
<option value="Mali">Mali</option> 
<option value="Malta">Malta</option> 
<option value="Marshall Islands">Marshall Islands</option> 
<option value="Martinique">Martinique</option> 
<option value="Mauritania">Mauritania</option> 
<option value="Mauritius">Mauritius</option> 
<option value="Mayotte">Mayotte</option> 
<option value="Mexico">Mexico</option> 
<option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
<option value="Moldova, Republic of">Moldova, Republic of</option> 
<option value="Monaco">Monaco</option> 
<option value="Mongolia">Mongolia</option> 
<option value="Montserrat">Montserrat</option> 
<option value="Morocco">Morocco</option> 
<option value="Mozambique">Mozambique</option> 
<option value="Myanmar">Myanmar</option> 
<option value="Namibia">Namibia</option> 
<option value="Nauru">Nauru</option> 
<option value="Nepal">Nepal</option> 
<option value="Netherlands">Netherlands</option> 
<option value="Netherlands Antilles">Netherlands Antilles</option> 
<option value="New Caledonia">New Caledonia</option> 
<option value="New Zealand">New Zealand</option> 
<option value="Nicaragua">Nicaragua</option> 
<option value="Niger">Niger</option> 
<option value="Nigeria">Nigeria</option> 
<option value="Niue">Niue</option> 
<option value="Norfolk Island">Norfolk Island</option> 
<option value="Northern Mariana Islands">Northern Mariana Islands</option> 
<option value="Norway">Norway</option> 
<option value="Oman">Oman</option> 
<option value="Pakistan">Pakistan</option> 
<option value="Palau">Palau</option> 
<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
<option value="Panama">Panama</option> 
<option value="Papua New Guinea">Papua New Guinea</option> 
<option value="Paraguay">Paraguay</option> 
<option value="Peru">Peru</option> 
<option value="Philippines">Philippines</option> 
<option value="Pitcairn">Pitcairn</option> 
<option value="Poland">Poland</option> 
<option value="Portugal">Portugal</option> 
<option value="Puerto Rico">Puerto Rico</option> 
<option value="Qatar">Qatar</option> 
<option value="Reunion">Reunion</option> 
<option value="Romania">Romania</option> 
<option value="Russian Federation">Russian Federation</option> 
<option value="Rwanda">Rwanda</option> 
<option value="Saint Helena">Saint Helena</option> 
<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
<option value="Saint Lucia">Saint Lucia</option> 
<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
<option value="Samoa">Samoa</option> 
<option value="San Marino">San Marino</option> 
<option value="Sao Tome and Principe">Sao Tome and Principe</option> 
<option value="Saudi Arabia">Saudi Arabia</option> 
<option value="Senegal">Senegal</option> 
<option value="Serbia and Montenegro">Serbia and Montenegro</option> 
<option value="Seychelles">Seychelles</option> 
<option value="Sierra Leone">Sierra Leone</option> 
<option value="Singapore">Singapore</option> 
<option value="Slovakia">Slovakia</option> 
<option value="Slovenia">Slovenia</option> 
<option value="Solomon Islands">Solomon Islands</option> 
<option value="Somalia">Somalia</option> 
<option value="South Africa">South Africa</option> 
<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
<option value="Spain">Spain</option> 
<option value="Sri Lanka">Sri Lanka</option> 
<option value="Sudan">Sudan</option> 
<option value="Suriname">Suriname</option> 
<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
<option value="Swaziland">Swaziland</option> 
<option value="Sweden">Sweden</option> 
<option value="Switzerland">Switzerland</option> 
<option value="Syrian Arab Republic">Syrian Arab Republic</option> 
<option value="Taiwan, Province of China">Taiwan, Province of China</option> 
<option value="Tajikistan">Tajikistan</option> 
<option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
<option value="Thailand">Thailand</option> 
<option value="Timor-leste">Timor-leste</option> 
<option value="Togo">Togo</option> 
<option value="Tokelau">Tokelau</option> 
<option value="Tonga">Tonga</option> 
<option value="Trinidad and Tobago">Trinidad and Tobago</option> 
<option value="Tunisia">Tunisia</option> 
<option value="Turkey">Turkey</option> 
<option value="Turkmenistan">Turkmenistan</option> 
<option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
<option value="Tuvalu">Tuvalu</option> 
<option value="Uganda">Uganda</option> 
<option value="Ukraine">Ukraine</option> 
<option value="United Kingdom">United Kingdom</option> 
<option value="United States">United States</option> 
<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
<option value="Uruguay">Uruguay</option> 
<option value="Uzbekistan">Uzbekistan</option> 
<option value="Vanuatu">Vanuatu</option> 
<option value="Venezuela">Venezuela</option> 
<option value="Viet Nam">Viet Nam</option> 
<option value="Virgin Islands, British">Virgin Islands, British</option> 
<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
<option value="Wallis and Futuna">Wallis and Futuna</option> 
<option value="Western Sahara">Western Sahara</option> 
<option value="Yemen">Yemen</option> 
<option value="Zambia">Zambia</option> 
<option value="Zimbabwe">Zimbabwe</option>
</select>
			
			</div>
			</div>
		<div class="span12" style="display:none;" id = "countryselect" >
		 <div style = "color:red;">Please select your country</div>
		 </div>
		 </div>
		 	
					 
			<div class="span4">
			<div class="field-wrap">
			<div class="control-wrap">
			 <h5 style = "display:inline;background: none;">City</h5><font color="red" style="display:inline;">*</font>	
			<select name="city" id="city" required style="width:100%" onchange = "othercity()">
			
			
			
			
			<?php if(isset($_SESSION['city'])) { ?>
<option value="<?php   echo $_SESSION['city']; ?>" selected="selected"><?php  echo $_SESSION['city'];  ?></option> 
<?php } else { ?>
<?php if(isset($_POST['city'])) { ?>
<option value="<?php   echo $_POST['city']; ?>" selected="selected"><?php  echo $_POST['city'];  ?></option> 
<?php } else { ?>
<option value="Select City" selected="selected">Select City</option>
<?php } ?>
<?php } ?>


			
			
			<option value="dubai">Dubai</option>
			<option value="sharjah">Sharjah</option>
			<option value="abu dhabi">Abu Dhabi</option>
			<option value="al ain">Al Ain</option>
			<option value="ras alkhaimah">Ras Al Khaimah</option>
			<option value="ajman">Ajman</option>
			<option value="Other">Other</option>
			</select>
			</div>
			</div>
			
		<div class="span12" style="display:none;" id = "cityselect" >
	    <div  style = "color:red;">Please select your city</div>
		</div>
		 </div>

		</div>
               
	 
		
			   
		
		 
		
		
		 
		 <div id="test" style="<?php if($_SESSION['city'] == 'Other' or $_POST['city'] == 'Other' ){ 
 echo'display:block;';}
 else
 echo'display:None;';
		      ?>">
		 <div class="span12" style="padding: 0 20px!important;">
		 
			 <div class="field-wrap">
			 <div class="control-wrap">
			  <h5 style = "display:inline;">Your city name</h5><font color="red" style="display:inline;">*</font>	

			 <input type="text" maxlength="30" name="citys" value="<?php if(isset($_SESSION['citys']))
			 $scitys = $_SESSION['citys'];
			 if(isset($_POST['citys']))
			 $scitys = $_POST['citys'];
			 echo $scitys;?>"  placeholder="" style="">
			 
			 </div>
			 </div>
			 </div>
		 </div>
		 
		 <div>
		 <div class="span6">
			 <div class="field-wrap">
			 <div class="control-wrap">
			  <h5 style = "display:inline;">Email</h5><font color="red" style="display:inline;">*</font>	
			 <input type="email" maxlength="100" name="email" id="email" value = "<?php if(isset($email)) 
			 $semail = $email;
			 if($_SESSION['email'])
			 $semail = $_SESSION['email'];
			 echo $semail; ?>" required placeholder="" style="">
			 </div>
			 </div>
			 </div>
		 
		 <div class="span6">
			 <div class="field-wrap">
			 <div class="control-wrap">
			 
			 <h5 style="display:inline;">Confirm Email</h5><font color="red" style="display:inline;">*</font>	
			 <input type="email" maxlength="100" name="confirm_email" value = "<?php if(isset($email)) 
			 $semail = $email;
			 if($_SESSION['email'])
			 $semail = $_SESSION['email'];
			 echo $semail; ?>" id="confemail" required  placeholder="" style="" onblur="confirmEmail()">
			 </div>
			 </div>
			 </div>
			 <div class="span12" style = "color:red;padding-left: 20px !important;">
			<div id = "confirmemail">
			</div>
			 </div>
			 </div>
		 
			
		 
		 <div class="span12">
			 <div class="field-wrap">
			 <div class="control-wrap">	
             <div class="span4">			 
			 <h5 style="display:inline;">Phone Number</h5><font color="red" style="display:inline;">*</font>	
			 <input type="text" required = "required" name="phone" value = "<?php 
			 if(isset($phone)) 
			 $sphone = $phone;
			 if(isset($_SESSION['phone']))
			 $sphone = $_SESSION['phone'];
			 echo $sphone; ?>" pattern=".{9,14}"   title="9 to 14 characters" placeholder="" maxlength="14" style="" onkeypress="return isNumber(event)">
			 </div>
			  
			 
			 <div class="span3">
		 <div class="control-wrap" id="lowercase">
		 <h5 style="display:inline;">Date of Birth</h5><font color="red" style="display:inline;">*</font>
		 <input type="text" maxlength="100"  name="birth_date" data-mask="99/99/9999" required ="required" value = "<?php if(isset($_POST['birth_date'])) 
		 $sdob = $_POST['birth_date'];
		 if(isset($_SESSION['birth_date']))
		 $sdob = $_SESSION['birth_date'];
		 echo $sdob;
		   ?>"  placeholder="dd/mm/yyyy *" style="height: 35px;">
		 </div>
			 </div>
			 
			  
		<div id="dialog" title="WHAT LEXILE MEASURE MEANS ?" style="padding:5px;">
			<p>This is an animated dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
			</div>
		 
		 <div class="span5">
		 <div class="control-wrap">
		 <h5>Lexile Measure   <a href="#join12_form"  id="join12_pop" class="tooltip1"style="font-size:9px;border-bottom:1px dotted #8cc341;"> <span >What’s this?</span></a></h5> 
		 <input type="text" maxlength="4" name="lexile" onkeypress="return isNumber(event)" value = "<?php if(isset($lexile))
		 $slexile = $lexile;
		 if($_SESSION['lexile'])
		 $slexile = $_SESSION['lexile'] ;
		 echo $slexile; ?>"  placeholder="" style=" margin-left: -3%;"  >		
		 </div>
		 </div>
		 <a href="#x" class="overlay12" id="join12_form"></a>
		 <div class="popup12 common-popup">
		<div id="lexile-center" style="height:300px; text-align:center; overflow:auto;overflow-x:hidden;">
		<div ><b>What is a Lexile Measure?</b></div>
<div  >It is a piece of information about either an individual's reading ability or the difficulty of a text, like a book or magazine article. The Lexile measure is shown as a number with an "L" after it — 880L is 880 Lexile.</div>
<div  ><span style="color: #333333; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">e.g.:, The first "Harry Potter" book measures 880L, so it's called an 880 Lexile book.</span></div>
<div  ></div>
<div  ><b>Lexile to Grade Correspondence</b></div>
<div  ></div>
<div  >
<table border="0" cellspacing="0" cellpadding="5">
<tbody>
<tr valign="top">
<td align="center" valign="middle">Grade</td>
<td align="center" valign="middle">Reader Measures, Mid-Year
25th percentile to 75th percentile (IQR)</td>
</tr>
<tr>
<td align="center" valign="middle">1</td>
<td align="center" valign="middle">Up to 300L</td>
</tr>
<tr class="ecxdgitem">
<td align="center" valign="middle">2</td>
<td align="center" valign="middle">140L to 500L</td>
</tr>
<tr>
<td align="center" valign="middle">3</td>
<td align="center" valign="middle">330L to 700L</td>
</tr>
<tr>
<td align="center" valign="middle">4</td>
<td align="center" valign="middle">445L to 810L</td>
</tr>
<tr>
<td align="center" valign="middle">5</td>
<td align="center" valign="middle">565L to 910L</td>
</tr>
<tr>
<td align="center" valign="middle">6</td>
<td align="center" valign="middle">665L to 1000L</td>
</tr>
<tr>
<td align="center" valign="middle">7</td>
<td align="center" valign="middle">735L to 1065L</td>
</tr>
<tr>
<td align="center" valign="middle">8</td>
<td align="center" valign="middle">805L to 1100L</td>
</tr>
<tr>
<td align="center" valign="middle">9</td>
<td align="center" valign="middle">855L to 1165L</td>
</tr>
<tr>
<td align="center" valign="middle">10</td>
<td align="center" valign="middle">905L to 1195L</td>
</tr>
<tr>
<td align="center" valign="middle">11 and 12</td>
<td align="center" valign="middle">940L to 1210L</td>
</tr>
</tbody>
</table> 
</div>
		</div>
		<a class="close12" href="#close12"></a>
        </div>
		
			 
		 </div>
		 </div>
		 </div>
		 <div class="span12" >
		 <div class="row-fluid element-padding-default ">
		 
		 </div>
		
			 
		 </div>
		
		 <div class="span12">
		 
		 <div class="span12">
		 <div class="span4">
		 <div class="element-padding-default">
		 <div class="field-wrap">
		 <div class="control-wrap" style="margin-top: 5%;">
		 <h5 >Contact Preference</h5>
<ul style="padding:0;margin-left: 0;">
<li style="display: block;width: 100%;overflow: hidden;"><input type="radio" name="contact_preference" value="phone" style="" <?php if($_SESSION['contact_preference'] == "phone" or $_POST['contact_preference'] == "phone"){echo'checked';} else{ echo'checked';} ?>>Email</li>
<li style="display: block;width: 100%;overflow: hidden;"><input type="radio" name="contact_preference" value="email" style="" <?php if($_SESSION['contact_preference'] == "email" or $_POST['contact_preference'] == "email"){echo'checked';} ?>>Phone</li>		 		 </ul>
		 </div>
		 </div>
		 </div>
		 </div>
		 
		  <div class="span4" >
		 <div class="element-padding-default ">
		 <div class="field-wrap">
		 <div class="control-wrap" style="margin-top: 7%;"> 
		 <h5>Send E-Mail When</h5>
			<ul style="padding:0;margin-left: 0;">
			<li style="display: block;width: 100%;overflow: hidden;">
			<input type="checkbox" name="item_due" value="1" <?php if($_SESSION['item_due'] == "1" or $_POST['item_due'] == "1"){echo'checked';} ?>> Items Are Due</li>
			</ul>
		   
		 </div>
		 </div>
		 </div>
		 </div>
		 
		 
		 <div class="span4" >
		 <div class="element-padding-default ">
		 <div class="field-wrap">
		 <div class="control-wrap" style="margin-top: 7%;">
		 <h5>Send Text When</h5>
		
		 <ul style="padding:0;margin-left: 0;">
			<li style="display: block;width: 100%;overflow: hidden;"><input type="checkbox" name="send_text_due" value="1" <?php if($_SESSION['send_text_due'] == "1" or $_POST['send_text_due'] == "1" ){echo'checked';} ?>>Items Are Due</li>
		 
		  
		  <li style="display: block;width: 100%;overflow: hidden;"><input type="checkbox" name="send_text_reserve" value="1"  <?php if($_SESSION['send_text_reserve'] == "1" or $_POST['send_text_reserve'] == "1"){echo'checked';} ?>> Reserve Pickup </li>
		
		  <li style="display: block;width: 100%;overflow: hidden;"><input type="checkbox" name="send_text_watch" value="1"  <?php if($_SESSION['send_text_watch'] == "1" or $_POST['send_text_watch'] == "1" ){echo'checked';} ?>> Watch List Triggered </li>
		   </ul>
		   
		 </div>
		 </div>
		 </div>
		 </div>
		 </div>
		 
		 
	<!--	<div class="row-fluid element-padding-default ">
		  <div class="span6">
		 <div class="control-wrap">
		 <h4>Send Text When</h4>
		
		<input type="checkbox" name="send_text_due" value="1"> &nbsp; Items Are Due <br>
		<input type="checkbox" name="send_text_reserve" value="1"> &nbsp; Reserve Pickup <br>
		<input type="checkbox" name="send_text_watch" value="1"> &nbsp; Watch List Triggered
		 </div>
		 </div>
		 <div class="span6">
		 <div class="control-wrap">
		 <h4>Send Email When</h4>
		
		<input type="checkbox" name="send_email" value="1"> &nbsp; Items Are Due 
		 </div></div>
		 
		
		 </div>-->
		 






			 <?php 
	$data = mysql_query("select * from plan WHERE id = '1'");
	$show = mysql_fetch_array($data);
	extract($show);
	
	switch($_GET['plan'])
		{
		
		case "sheldon":
		
		$price = $sheldon;
		$period = $period_sheldon;
		break;
		
		case "lindgren":
		
		$price = $lindgren;
		$period = $period_lindgren;
		break;
		
		case "tolstoy":
		
		$price = $tolstoy;
		$period = $period_tolstoy;
		break;
		
		case "rowling":
		
		$price = $rowling;
		$period = $period_rowling;
		break;
		
		}
$today_date = date('d-m-Y');
$full_period =  $period;
$emi_period = $full_period - 1;
$total_days = $full_period * 30;
$total_emi_days = $emi_period * 30;
$ending_date = date('d-m-Y',strtotime("+".$total_days."days", strtotime($today_date)));
$after_month = date('d-m-Y',strtotime("+30 days", strtotime($today_date)));
$emi_ending_date = date('d-m-Y',strtotime("+".$total_emi_days."days", strtotime($after_month)));
	?>

	<div class="span6 span8">
	<div class="span12">
	<!--<div class="field-wrap">
    <div class="control-wrap signup-page-amount">
    <h5>Total Amount</h5>
	<div class="span3">

		
		<input id="total" name="total_amount" value = "<?php $tot = $price + $deposit; echo $tot; ?>" type="text" readonly  disable style="margin-left:9%"/>
	</div>	</div>
		<div class="span3">
		<select name = "
		" id = "payment_term" onchange="sumtotal(this.value)">
		<option>1 Month</option>
		<option>6 Months</option>
		<option>12 Months</option>
		</select>
		</div>
		</div>	-->
  
		 <div class="element-padding-default ">
		 <div class="field-wrap">
		 <div class="control-wrap" >
		 <h5>Payment Options</h5>
			<ul style="padding:0;margin-left: 0;">
			<li style="display: block;width: 50%; float:left;">
			<input type="radio" id = "p1" name="payment"  value="1" onclick="check(this);" checked> Full Term</li>
			<?php if(!isset($_SESSION['gift_status'])) { ?>
			<li> 
			<input type="radio" id = "p2" name="payment" value="0" style="" onclick="check(this);">Convert to EMI </li>
			<?php } ?>
			<li id="charge" style = "width:425px;"></li>
			<?php if($_SESSION['gift_status'] != 'gift_plan') { ?>
			<li>I am a Student<input type="checkbox" id = "stucheckbox" name="student" value="1" style="" <?php if(!isset($_SESSION['gift_status'])){?> onclick="checkstu();" <?php } else{ ?> onclick="cstudent(this.checked);" <?php } ?>></li>
			<?php } ?>
			<li id = "student" style = "display:none;font-colour:red;width:500px;">This entitles you to access only Children and Young Adult classified books. Your membership will be activated only upon receipt of a copy of your School or College id at <a href = "mailto:hello@reademption.com">hello@reademption.com</a>.</li>
			</ul>
		   
		 </div>
		 </div>
		 </div>	
	 
		</div>
	<div class="span12 hidden-desktop hidden-tablet" style="">
	<center><h5>Payment Summary (AED)</h5></center>
	<div class="column-text clearfix">
	<div class="pricing-table row-fluid columns-4" style="margin-bottom:10px !important;margin-top:10px !important;">
	<div class="field-wrap">
	<div class="signup-page-total">
	<ul class="feature-list ">
	<li class="included-text"><strong>Membership Fees</strong> <span id = "planprice"><?php $planprice = $price * $period; echo  number_format($planprice); ?>.00</span ></li>
	<li class="included-text"><strong>Refundable Deposit</strong> <span > <?php echo $deposit; ?>.00 </span ></li>
	<li class="included-text"><strong >Current charge to card  </strong> <span id="totalprice"> <?php $tot = $price * $period + $deposit; echo number_format($tot); ?>.00</span></li>
	<li class="included-text" id = "scheduledcharges">
	</li>
	</ul>
	</div>
	</div> 
	</div>
	</div>	
	</div>			

	<br>
	
	       
	</div>
			<div class="span12" style="">
			
				
				<div class="span5" style="padding-right:0;">	
			   
				<label for="email" class="spn3">Redeem Gift Card 
				<!-- <span ><a href="#join51_form"   class="tooltip1" style="font-size:9px;border-bottom:1px dotted #8cc341;z-index: 2;"> <span >whats this?</span></a></span>-->
				</label>
				
				<input type="text" name="voucher_no" onkeypress="return isNumber(event)" value="<?php echo trim($_SESSION['voucher']); ?>"    title="10 characters Maximum" class="spn3"/>
				<?php if(isset($_SESSION['gift_status'])){echo'<span style="float: right;margin-top: -27px;font-size: 40px;"><a href="http://www.reademption.com/signup?plan='.$_GET['plan'].'&sign">&#215</a></span>';} ?>
				</div>
				<div class="span2" style="padding:0">
				<div class="gap  hidden-mobile" style="height:20px;line-height:20px;"></div>
				<input type="submit" onclick = "submitv()" name="voucher" value="Apply" >

				</div>
						<div class="span6" style="margin-top: 10px;width: 672px;">
		<?php if(isset($voucher_status)){ echo '<img src="'.get_template_directory_uri().'/images/reademp.png" style="height:26px;margin-right:3px;" class="ing"><span style="color:red;"> '.$voucher_status.'</span>'; }  ?>
		
		<?php 
		
if(isset($_SESSION['gift_status']))
{
echo '<img src="'.get_template_directory_uri().'/images/reademp.png" style="height:26px;margin-right:3px;" class="ing">
<span style="color:#8cc341;"> Voucher No. is Applied.' ;	}

if(isset($_SESSION['gift_amount_xtra'])) {
		echo 'You have chosen to redeem your Gift card for a plan of a lesser value. You will not be entitled to a credit of the remaining amount. 
		Please either choose another plan or accept to proceed ahead with the forfeiture of the balance amount';
		echo '</span>';  }		

		?>
		
		</div> 
			
			
			</div>
			
			
			
			<div class="span12" style="">
			
			<div class="span4" style="">
			<div class="field-wrap" style="margin-top:10px;">
			<?php  
			$_SESSION['captcha'] = simple_php_captcha();
			echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" />
			<br><div class="span12 full-width" style="padding: 0; width:226px;">
			<input type="text" maxlength="100" name="captcha" required placeholder="Please Enter The Above Code" style=""></div>';
			?><br>
			</div>
			</div>
			
			
		
			
			</div>
		 
		 

		
		
			</div>
	   		
			<div class="span12 sign-up-page-padd" style="padding:0 20px">
			<div class="control-wrap" >
			<?php
			if(isset($_POST['signup'])) { ?>
			<input type="checkbox" name="newssignup" value="1" <?php if(isset($_POST['newssignup'])) echo "checked"?>> I’d like to have the best of Reademption delivered to my Inbox. Sign me up for the Newsletter
			<?php } else { ?>
			<input type="checkbox" name="newssignup" value="1" checked> I’d like to have the best of Reademption delivered to my Inbox. Sign me up for the Newsletter
            <?php } ?>
			
			</div>
			
			
	 <?php if(isset($status)) { ?>
<center><div style = "display:inline;"><img src="<?php echo get_template_directory_uri();?>/images/reademp.png" style="height:60px;" ></div>
<?php } ?>
<div  style="color:red;display:inline"><?php echo $status; ?></div></center>

		<?php /* if(isset($status))
		{
		echo'<div class="field-wrap " style="color:red;margin:0;">'.$status.'</div>';
		
		} */?>
	   
<input type="hidden" name="plan" value="<?php echo $_GET['plan']; ?>">	
<?php if($_SESSION['gift_status'] == "gift_plan") { ?>
<input type="hidden" name="total_amount" id = "tot_mem" value="<?php  echo $deposit; ?>">
<?php } elseif ($_SESSION['gift_status'] == 'gift_voucher') { 
$giftv = mysql_query("SELECT * FROM gift_card WHERE id = '".$_GET['gift']."'");
  $gift = mysql_fetch_array($giftv);
    
  ?>

<input type="hidden" name="total_amount" id = "tot_mem" value="<?php  echo $tot-$gift_amount; ?>">
<?php } else{ ?> 
<input type="hidden" name="total_amount" id = "tot" value="<?php  echo $tot; ?>">	
<?php } ?>
<div class="field-wrap sign-up-submit" style="margin-top:10px;">
<input type="submit" class="button button_default button_default" name="signup" value="Proceed to Payment" >
<a href = "http://www.reademption.com/signup?plan=<?php echo $_GET['plan'];?>&clearform"><button type=button class="button button_default button_default" name="reset" >Reset
</button>
</a>
</div>
<div class="field-wrap sign-up-submit" style="margin-top:10px;">

</div>

</div>
</div>

</form>
	
	
	
	
	<script>
  function cstudent(sc){
 if(sc == true)
 {
 <?php if($_SESSION['gift_status'] == "gift_plan") { ?>
 var sprice = "<?php  $deposit; ?>";
 var cprice = (sprice) - (sprice * (10/100));
 <?php } ?>
 <?php if($_SESSION['gift_status'] == "gift_voucher") { ?>
 
 var sprice2 = "<?php echo $price * $period - $gift_amount; ?>";
 var sprice1 = sprice2 - (sprice2 * (10/100));
 var deposit = "<?php  echo $deposit; ?>";
 var sprice = parseInt(sprice1) + parseInt(deposit);
 var cprice = sprice;
 <?php } ?>
 
 document.getElementById("plan_member").value = sprice;
 document.getElementById("tot_mem").value = cprice;
 document.getElementById("total_member").innerHTML = cprice+".00";
  document.getElementById("plan_member").innerHTML = sprice1+".00";
 }
 else{
 <?php if($_SESSION['gift_status'] == "gift_plan") { ?>
 var sprice = "<?php  echo $deposit; ?>";
 <?php } ?>
 <?php if($_SESSION['gift_status'] == "gift_voucher") { ?>
 var sprice = "<?php  echo $tot-$gift_amount;?>";
 var sprice1 = "<?php  echo $price * $period - $gift_amount;?>";
 <?php } ?>
 var cprice = sprice;
  document.getElementById("tot_mem").value = cprice;
 document.getElementById("total_member").innerHTML = cprice+".00";
 document.getElementById("plan_member").innerHTML = sprice1+".00";
 }
  
  }
  
  
  </script>
		 
		 
		 
		</div> 
		</div> 
	
	
	
	
	
	<div class="span3 plan-pricing-mobile hidden-mobile">
		<div class="inner-content">
		
		<form action="" method="Get">
		
		<?php if($_SESSION['gift_status'] == "gift_plan") { ?>
		<h5 style="margin-bottom:0;font-size: 18px;margin-bottom: 0;">Selected Plan
<input type="text" name="plan" class="plan-select-box" value=": <?php echo $_GET[plan]; ?>" 
disabled style="text-transform: uppercase;border:0px;display: inline-block;font-size: 17px;width:51%;font-weight: 700;"></h5>
<?php } elseif($_SESSION['gift_status'] == "gift_voucher") {?>
<h5 style="margin-bottom:0;">Selected Plan</h5>
           <select name="plan" class="plan-select-box" onchange="this.form.submit()" style="width:100%">
		  <option <?php if($_GET[plan] == 'sheldon') echo"selected"; ?> value="sheldon">SHELDON</option>
        <option <?php if($_GET[plan] == 'lindgren') echo"selected"; ?>  value="lindgren">LINDGREN</option>
        <option <?php if($_GET[plan] == 'tolstoy') echo"selected"; ?>  value="tolstoy">TOLSTOY</option>
        <option <?php if($_GET[plan] == 'rowling') echo"selected"; ?>  value="rowling">ROWLING</option>
    </select>
	<input type="hidden" name="gift" value="<?php echo $_GET['gift']; ?>">
	<input type="hidden" name="gift_status" value="<?php echo $_GET['gift_status']; ?>">
		<?php } else { ?> 
		 
    <select name="plan" class="plan-select-box" onchange="this.form.submit()" style="width:100%">
		  <option <?php if($_GET[plan] == 'sheldon') echo"selected"; ?> value="sheldon">SHELDON</option>
        <option <?php if($_GET[plan] == 'lindgren') echo"selected"; ?>  value="lindgren">LINDGREN</option>
        <option <?php if($_GET[plan] == 'tolstoy') echo"selected"; ?>  value="tolstoy">TOLSTOY</option>
        <option <?php if($_GET[plan] == 'rowling') echo"selected"; ?>  value="rowling">ROWLING</option>
    </select>
	<?php } ?>
</form>
		<br> 
		
		 <?php 
		 switch($_GET['plan'])
		{
		
		case "sheldon":
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4 ">
		

	<div class="field-wrap">
	<div class="pricing-column">
		<div class="title-box"><h2>SHELDON</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>120'.$starter.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$starter.'in monthly instalments</div></div>
		</div>
			<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>
<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		<span style="text-decoration: line-through; color: #ff0606;">Delivered to doorstep</span>
	</li>
</ul>
						

	</div></div>

	

	


	</div>
	</div>';
	
	break;
	
	case "lindgren" :
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		 

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>LINDGREN</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>170'.$basic.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$basic.'in monthly instalments</div></div>
		</div>
				<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>

<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		Delivered to doorstep
	</li>
</ul>
						</div></div>
						
						</div></div>';

	break;
	
	case "tolstoy" :
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>TOLSTOY</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>100'.$premium.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$premium.'in monthly instalments</div></div>
		</div>
				<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>

	<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		<span style="text-decoration: line-through; color: #ff0606;">Delivered to doorstep</span>
	</li>
</ul>
						</div></div>
						
						</div></div>';
	
	break;
	
	case "rowling" :
		
		echo'<div class="column-text clearfix">
		
	<div class="pricing-table row-fluid columns-4">
		

	<div class="field-wrap"><div class="pricing-column">
		<div class="title-box"><h2>ROWLING</h2>
			<div class="pricing-box"><div><span class="price"><span class="dollor" style="margin-left: -15%;">AED</span>150'.$extreme.'</span><span class="month">/ Month</span></div><div class="price-info">billed annually or '.$extreme.'in monthly instalments</div></div>
		</div>
				<ul class="feature-list hidden-mobile">

	<li class="included-text">
		<strong>Physical Resources</strong>
	</li>

	<li class="included-text">
		Unlimited Books / month
	</li>

	<li class="included-text">
		7 books at a time
	</li>

	<li class="included-text">
		<strong>Online Resources</strong>
	</li>

	<li class="included-text">
		eBooks
	</li>

	<li class="included-text">
		Audiobooks
	</li>

	<li class="included-text">
		Britannica
	</li>

	<li class="included-text">
		Tumblebooks
	</li>

	<li class="included-text">
		Busythings
	</li>

	<li class="included-text">
		Mango Languages
	</li>

	<li class="included-text">
		<strong>Privileges</strong>
	</li>
<li class="included-text">
		Access to Library - Store
	</li>

	<li class="included-text">
		Access to Library - Online
	</li>
	
	<li class="included-text">
		Access to Library – Mobile
	</li>

	<li class="included-text">
		Reserve titles
	</li>

	<li class="included-text">
		Delivered to doorstep
	</li>
</ul>
						</div></div>
						</div></div>
						';
	
	

	}?>

	

	</div>
	
<script type="text/javascript">
function number_format(number, decimals, dec_point, thousands_sep) {
  //  discuss at: http://phpjs.org/functions/number_format/
  // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: davook
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Theriault
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Michael White (http://getsprink.com)
  // bugfixed by: Benjamin Lupton
  // bugfixed by: Allan Jensen (http://www.winternet.no)
  // bugfixed by: Howard Yeend
  // bugfixed by: Diogo Resende
  // bugfixed by: Rival
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  //  revised by: Luke Smith (http://lucassmith.name)
  //    input by: Kheang Hok Chin (http://www.distantia.ca/)
  //    input by: Jay Klehr
  //    input by: Amir Habibi (http://www.residence-mixte.com/)
  //    input by: Amirouche
  //   example 1: number_format(1234.56);
  //   returns 1: '1,235'
  //   example 2: number_format(1234.56, 2, ',', ' ');
  //   returns 2: '1 234,56'
  //   example 3: number_format(1234.5678, 2, '.', '');
  //   returns 3: '1234.57'
  //   example 4: number_format(67, 2, ',', '.');
  //   returns 4: '67,00'
  //   example 5: number_format(1000);
  //   returns 5: '1,000'
  //   example 6: number_format(67.311, 2);
  //   returns 6: '67.31'
  //   example 7: number_format(1000.55, 1);
  //   returns 7: '1,000.6'
  //   example 8: number_format(67000, 5, ',', '.');
  //   returns 8: '67.000,00000'
  //   example 9: number_format(0.9, 0);
  //   returns 9: '1'
  //  example 10: number_format('1.20', 2);
  //  returns 10: '1.20'
  //  example 11: number_format('1.20', 4);
  //  returns 11: '1.2000'
  //  example 12: number_format('1.2000', 3);
  //  returns 12: '1.200'
  //  example 13: number_format('1 000,50', 2, '.', ' ');
  //  returns 13: '100 050.00'
  //  example 14: number_format(1e-8, 8, '.', '');
  //  returns 14: '0.00000001'

  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}
<?php 
$emi_result = mysql_query("SELECT * FROM emi WHERE id = '1'");
$emi_row = mysql_fetch_array($emi_result);
$emi_percentage = $emi_row['emi'];
$interest  = $emi_percentage;
$interest = $interest/100;
 ?>
function check(el) {
document.getElementById("stucheckbox").checked = false;
document.getElementById('student').style.display = "none";
var deposit = <?php echo $deposit; ?>;
var old_price = "0";
if (el.value == "1") {
var old_price = <?php echo $price; ?>;

var planprice = old_price * <?php echo $period ?>;
document.getElementById("planprice").innerHTML = number_format(planprice)+".00";

var planpricem = old_price * <?php echo $period ?>;
document.getElementById("planpricem").innerHTML = number_format(planpricem)+".00";

var old_total = old_price * <?php echo $period ?> + deposit;
document.getElementById("totalsm").innerHTML = number_format(old_total)+".00";
document.getElementById("totalprice").innerHTML = number_format(old_total)+".00";
var elem = document.getElementById("tot");
elem.value = old_total;
document.getElementById('charge').style.display = "none";
document.getElementById("scheduledcharges").innerHTML = '';
document.getElementById("scheduledchargesm").innerHTML = '';

}
else {
var old_price = <?php echo $price; ?>;
var interest =  old_price * <?php echo $interest; ?>;
var new_price = old_price + interest;
var total = new_price + deposit;

//var tplanprice = <?php echo $price; ?> * <?php echo $period; ?>;  // Total plan price without including EMi
//document.getElementById("planprice").innerHTML = number_format(tplanprice)+".00";
//document.getElementById("planpricem").innerHTML = number_format(tplanprice)+".00";

var tiplanprice = new_price * <?php echo $period; ?>;                  //Total Plan price including Emi tax
document.getElementById("planprice").innerHTML = number_format(tiplanprice)+".00";
document.getElementById("planpricem").innerHTML = number_format(tiplanprice)+".00";


document.getElementById("totalprice").innerHTML = number_format(total)+".00";
document.getElementById("totalsm").innerHTML = number_format(total)+".00";
document.getElementById('charge').style.display = "block";
document.getElementById("charge").innerHTML = "This will include AED "+interest+" as additional fee into your charge each month";
var elem = document.getElementById("tot");
elem.value = total;
document.getElementById("scheduledcharges").innerHTML = '<strong >Scheduled charge to card</strong><span >AED '+new_price+'.00</span> <strong><i>This amount will be debited to your card for <?php  $diff = $period - 1; echo $diff;  ?> subsequent months beginning <?php echo $after_month; ?></i></strong>';
document.getElementById("scheduledchargesm").innerHTML = '<strong >Scheduled charge to card</strong><span >AED '+new_price+'.00</span> <strong><i>This amount will be debited to your card for <?php  $diff = $period - 1; echo $diff;  ?> subsequent months beginning <?php echo $after_month; ?></i></strong>';
}
}

function checkstu() {
var deposit = <?php echo $deposit; ?>;
if(document.getElementById("p1").checked == true) {
var old_price = <?php $old = $price * $period; echo $old;  ?>;
                                                  }
												  else {
var original_price = <?php echo $price; ?>;
var interest =  original_price * 10/100;
var old_price = original_price + interest;	
var emi = "yes";											  
											  
												       }
if(document.getElementById("stucheckbox").checked == true) {

if(emi == 'yes')   {
var total_plan_price = <?php echo $price; ?> * <?php echo $period; ?>;
var current_payment_price = <?php echo $price; ?>;
var total = current_payment_price + deposit;


                   }	else {
				   
var interest =  old_price * 10/100;
var total_plan_price = old_price - interest;
var total = total_plan_price + deposit;

                             }

document.getElementById("planprice").innerHTML = number_format(total_plan_price)+".00";
document.getElementById("planpricem").innerHTML = number_format(total_plan_price)+".00";
document.getElementById("totalprice").innerHTML = number_format(total)+".00";
document.getElementById("totalsm").innerHTML = number_format(total)+".00";
var elem = document.getElementById("tot");
elem.value = total;
document.getElementById('student').style.display = "block";
if(emi == 'yes'){
document.getElementById("scheduledcharges").innerHTML = '<strong >Scheduled charge to card</strong><span >AED <?php echo $price; ?>.00</span> <strong><i>This amount will be debited to your card for the period <?php echo $after_month.' to '.$ending_date ; ?></i></strong>';
document.getElementById("scheduledchargesm").innerHTML = '<strong >Scheduled charge to card</strong><span >AED <?php echo $price; ?>.00</span> <strong><i>This amount will be debited to your card for the period <?php echo $after_month.' to '.$ending_date ; ?></i></strong>';
                }
                                                           }
		else
                        {
if(emi == 'yes')   {
var current_payment_price = <?php echo $price; ?> + .1 * <?php echo $price; ?> ;
var total_plan_price = current_payment_price * <?php echo $period; ?>;
var total = current_payment_price + deposit;

                   }	else {
				   
var single_payment_price = <?php echo $price; ?> - .1 * <?php echo $price; ?> ;
var total_plan_price = single_payment_price * <?php echo $period; ?> ;
var total = total_plan_price + deposit;

                             }

var interest =  old_price * 10/100;
var new_price = old_price;
var total = new_price + deposit;
document.getElementById("planprice").innerHTML = number_format(total_plan_price)+".00";
document.getElementById("planpricem").innerHTML = number_format(total_plan_price)+".00";
document.getElementById("totalprice").innerHTML = number_format(total)+".00";
document.getElementById("totalsm").innerHTML = number_format(total)+".00";
var elem = document.getElementById("tot");
elem.value = total;document.getElementById('student').style.display = "none";
if(emi == 'yes'){
document.getElementById("scheduledcharges").innerHTML = '<strong >Scheduled charge to card</strong><span >AED '+current_payment_price+'.00</span> <strong><i>This amount will be debited to your card for the period <?php echo $after_month.' to '.$ending_date ; ?></i></strong>';
document.getElementById("scheduledchargesm").innerHTML = '<strong >Scheduled charge to card</strong><span >AED '+current_payment_price+'.00</span> <strong><i>This amount will be debited to your card for the period <?php echo $after_month.' to '.$ending_date ; ?></i></strong>';
                }

                        }						
}

</script>						
	<?php if($_SESSION['gift_status'] == "gift_plan")
	{
	$voucher = mysql_query("SELECT * FROM gift_card WHERE id = '1'");
	$voucher_detailed = mysql_fetch_array($voucher);
	$discount = $voucher_detailed['amount'];
	$total = ($price * $period) - $discount;
	
	?>
	<div class="span12 hidden-mobile">
	<center>
	<h5>Payment Summary (AED)</h5>
	</center>
	<div class="column-text clearfix">
	<div class="pricing-table row-fluid columns-4">
	<div class="field-wrap">
	<div class="signup-page-total">
	<ul class="feature-list hidden-mobile">
	<li class="included-text"><strong>Membership Fees </strong><span id = "plan_member" style="color: #ff0606;text-decoration: line-through;"><?php if($_GET['gift_status'] == 'gift_plan') { echo ''.$price*$period.'.00'; } ?></span ><span id = "planpricem"><?php if($_GET['gift_status'] == 'gift_plan') { echo '0.00'; } ?></span ></li>
	
	<li class="included-text"><strong >Current charge to card</strong><span id="total_member"> <?php if($_GET['gift_status'] == 'gift_plan') {$tot = $deposit; echo ''.$tot.'.00';} ?></span></li>
	<li class="included-text" id = "scheduledchargesm">
	</li>
	</ul>
	</div>
	</div> 
	</div>
	</div>	
	</div>
	
   <?php } elseif ($_SESSION['gift_status'] == "gift_voucher") { ?> 
   <div class="span12 hidden-mobile">
	<center>
	<h5>Payment Summary (AED)</h5>
	</center>
	<div class="column-text clearfix">
	<div class="pricing-table row-fluid columns-4">
	<div class="field-wrap">
	<div class="signup-page-total">
	<ul class="feature-list hidden-mobile">
	<li class="included-text"><strong>Membership Fees AED </strong><span id = "planbn" style="color: #ff0606;text-decoration: line-through;"><?php echo ''.$price*$period.'.00'; ?></span ><span id = "plan_member"><?php echo $price * $period - $gift_amount; ?>.00</span ></li>
	<li class="included-text"><strong>Refundable Deposit AED </strong><span > <?php echo $deposit; ?>.00 </span ></li>
	<li class="included-text"><strong >Current charge to card AED </strong><span id="total_member" style=""> <?php $tot1 = $price * $period + $deposit - $gift_amount; echo $tot1; ?>.00</span></li>
	<li class="included-text" id = "scheduledchargesm">
	</li>
	</ul>
	</div>
	</div> 
	</div>
	</div>	
	</div>
   
   
   
   
  

	<?php } else { ?>
	<div class="span12 hidden-mobile">
	<center>
	<h5>Payment Summary (AED)</h5>
	</center>
	<div class="column-text clearfix">
	<div class="pricing-table row-fluid columns-4">
	<div class="field-wrap">
	<div class="signup-page-total">
	<ul class="feature-list hidden-mobile">
	<li class="included-text"><strong>Membership Fees  </strong><span id = "planpricem"><?php $pp = $price * $period; echo number_format($pp); ?>.00</span ></li>
	<li class="included-text"><strong>Refundable Deposit  </strong><span > <?php echo $deposit; ?>.00 </span ></li>
	<li class="included-text"><strong >Current charge to card </strong><span id="totalsm"> <?php $tot = $price * $period + $deposit; echo number_format($tot); ?>.00</span></li>
	<li class="included-text" id = "scheduledchargesm">
	</li>
	</ul>
	</div>
	</div> 
	</div> 
	</div>	
	</div>
	<?php } ?>
	</div>

	
	

 
	 
		
	</div> 
</div>
  
           </div>
     </div>
  </section>
  
  
  
  
  
  <script>
  function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }
 
  
  
  </script>

  <script>

    
	function isNumber(evt) 
	{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
	}
	
	function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }
 
	
</script>
  
  
  <?php get_footer(); ?>