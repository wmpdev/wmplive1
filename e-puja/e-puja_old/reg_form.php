<?php
include 'config.php';

$sql = "SELECT * FROM tbl_pooja";
$result = $dbh->query($sql);
$result_arr = array();
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        //echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
        //print_r($row);
        array_push($result_arr, $row);
        //list($result_arr)=$row;
    }
} else {
    echo "0 results";
}



if ($_POST["Submit"] == "Submit") {
    $name = $_POST["Name"];
    $Email = $_POST["Email"];
    $phone = $_POST["phone"];
    $address = $_POST["address"];
    $country = $_POST["country"];
    $sel_pooja = $_POST["sel_pooja"];
    $pooja_date = $_POST["pooja_date"];
    $vc_type = $_POST["vc_type"];
    $comments = $_POST["comments"];

    if (!isset($pooja_name))
        $pooja_name = "Home";
    $client_ip = $_SERVER['REMOTE_ADDR'];

    $message = <<<EOD

<!DOCTYPE html>
<html>
    <body>
        <div align="center" style="width:100%">
            <div align="center" style="background-color:#FFF;max-width:770px;min-width:200px">

                <table class="tbl_comparison" style="margin-top: 25px;width: 100%;">
                    <tbody>
                    <tr>
                        <th class="col_label" colspan="2" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;text-align: left;"> 
                        <h3>Registration Details For E-Puja </h3>
                        </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$name</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Email</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$Email</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Phone</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$phone</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Address</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$address</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Country</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$country</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Selected Pooja</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$sel_pooja</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Pooja Date</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$pooja_date</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">E-Puja Type</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$vc_type</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Comments</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$comments</td>                
                    </tr>            

            <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Comments</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$comments</td>                
                    </tr>

            <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Generated From</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$pooja_name Page</td>                
                    </tr>

            <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Client Ip</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$client_ip</td>                
                    </tr>
   
   
                    </tbody></table>



            </div>
        </div>
    </body>
</html>
            
            
EOD;

//echo $message;
// Always set content-type when sending HTML email
    //$to = 'ninad.tilkar@choiceindia.com';
    $to = 'panditji@wheresmypandit.com, ninad.tilkar@choiceindia.com';
    $from = "wheresmypandit.com";
    //$from = "choiceuae.ae";

    $subject = "Registration Details For E-Puja :- Name : $name , Puja : $sel_pooja";

    $headers = "From: $from \r\n";
    $headers .= "Reply-To: $from \r\n";
    $headers .= "CC: ninad.tilkar@choiceindia.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";



    if (mail($to, $subject, $message, $headers))
        $mail_msg = "Your details are send Successfully";
    else
        $mail_msg = "There is some problem to send mail";
}
?>

<style>
    .error_strings{ color:red;}    
</style>

<script language="JavaScript" src="js/gen_validatorv4.js" type="text/javascript" xml:space="preserve"></script>
<form action="" name="myform" id="myform" method="post">

    <!--
    
      <div class="row">
        <div class="col-lg-4 wow fadeInLeft">	
                
  
           <div class="form">
            <input class="input-text" type="text" name="" value="Your Name *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
            <input class="input-text" type="text" name="" value="Your E-mail *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
            <input class="input-text" type="text" name="" value="Enter Pooja Date and Time " onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
            <input class="input-text" type="text" name="" value="Your Country Code *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
            <textarea class="input-text text-area" cols="0" rows="0" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">Your Address *</textarea>
          
          </div>            
            
            
        </div>
          
        <div class="col-lg-4 wow fadeInLeft delay-06s">
            
           <div class="form">

            
            <select class="input-text" name="country" id="country">
                <option value="000" selected="selected">Select Country *</option>
<?php GetCountry(); ?>
            </select>
            
            <select class="input-text" name="sel_pooja" id="sel_pooja">
                  <option value="000" selected="selected">Select Pooja</option>
<?php for ($i = 0; $i < count($result_arr); $i++) { ?>
        <option value="<?php echo $result_arr[$i]["name"]; ?>" <?php if ($pooja_name == $result_arr[$i]["name"]) echo "selected='selected'"; ?>  ><?php echo $result_arr[$i]["name"]; ?></option>
    <?php } ?>                
              </select>
              <div id='myform_sel_pooja_errorloc' class="error_strings"></div>



<select class="input-text" name="vc_type" id="vc_type">
                <option value="000" selected="selected">Select Pooja Type</option>
                <option value="BILATERAL POOJA">BILATERAL POOJA</option>
                <option value="YAJMAAN EDGE">YAJMAAN EDGE</option>
                <option value="WMP EDGE">WMP EDGE</option>
              </select>
              <div id='myform_vc_type_errorloc' class="error_strings"></div>
              
            
               
            <input class="input-text" type="text" name="" value="Your Phone Number *" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
            
            <textarea class="input-text text-area" cols="0" rows="0" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">Your Message *</textarea>
            <input class="input-btn" type="submit" value="send message">
          
          </div>
            
        </div>
      </div>
        
    -->

    <div class="col-lg-4 wow fadeInLeft">	
        <div class="contact_info">
            <div class="detail">
                <h4><strong><font color="white">Address</font></strong></h4>
                <p>Choice House,<br/>
                    Plot No. 156-158,<br/>
                    Shree Shakambhari Corporate Park,<br/>
                    J B Nagar, Andheri (E), Mumbai-400 099</p>
            </div>
            <div class="detail">
                <h4><strong><font color="white">Contact us</font></strong></h4>
                <p>022 67079797 / +91 9820573385</p>
            </div>
            <div class="detail">
                <h4><strong><font color="white">Email us</font></strong></h4>
                <p>panditji@wheresmypandit.com</p>
            </div> 
        </div>


        <!--
        <ul class="social_links">
          <li class="twitter animated bounceIn wow delay-02s"><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
          <li class="facebook animated bounceIn wow delay-03s"><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
          <li class="pinterest animated bounceIn wow delay-04s"><a href="javascript:void(0)"><i class="fa fa-pinterest"></i></a></li>
          <li class="gplus animated bounceIn wow delay-05s"><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li> 
        </ul>
        -->
    </div>
    <div class="col-lg-8 wow fadeInLeft delay-06s">

        <div class="form">
            <div id='myform_Name_errorloc' class="error_strings"></div>
            <input class="input-text" type="text"  id="Name" name="Name" value="" placeholder="Your Name *" onFocus="if (this.value == this.defaultValue)
                        this.value = '';" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;">

            <div id='myform_Email_errorloc' class="error_strings"></div>            
            <input class="input-text" type="text" id="Email" name="Email" value=""  placeholder="Your E-mail *"  onFocus="if (this.value == this.defaultValue)
                        this.value = '';" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;">

<!-- <input class="input-text" type="text" id="code" name="code" value="+91" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" style="width: 10%"> + -->
            <div id='myform_phone_errorloc' class="error_strings"></div>
            <input class="input-text" type="text" id="phone" name="phone" value="" placeholder="Your Phone Number *" onFocus="if (this.value == this.defaultValue)
                        this.value = '';" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;" >

            <div id='myform_Name_errorloc' class="error_strings"></div>  
            <textarea class="input-text text-area"  id="address" name="address"  cols="0" rows="0" placeholder="Your Address" onFocus="if (this.value == this.defaultValue)
                        this.value = '';" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;"></textarea>

            <div id='myform_country_errorloc' class="error_strings"></div>
            <select class="input-text" name="country" id="country">
                <option value="000" selected="selected">Select Your Country *</option>
<?php GetCountry(); ?>
            </select>

            <div id='myform_sel_pooja_errorloc' class="error_strings"></div>                        
            <select class="input-text" name="sel_pooja" id="sel_pooja">
                <option value="000" selected="selected">Select Your Puja *</option>
<?php for ($i = 0; $i < count($result_arr); $i++) { ?>
                    <option value="<?php echo $result_arr[$i]["name"]; ?>" <?php if ($pooja_name == $result_arr[$i]["name"]) echo "selected='selected'"; ?>  ><?php echo $result_arr[$i]["name"]; ?></option>
                <?php } ?>                
            </select>


            <div id='myform_pooja_date_errorloc' class="error_strings"></div>
            <!--
            <input class="input-text" type="text" id="pooja_date1" name="pooja_date1" value="" placeholder="Your Puja Date and Time *" onFocus="if (this.value == this.defaultValue)
                        this.value = '';" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;">
            -->






            <link href="css/date_picker/bootstrap-combined.min.css" rel="stylesheet">  
            <link  href="css/date_picker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" media="screen">



            <div id="datetimepicker_date" class="input-append date">
                <input type="text" style="display:inline-block" id="pooja_date" name="pooja_date" class="input-text" readonly  placeholder="Puja Date"></input>
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>      
            </div>
            <div id="datetimepicker_time" class="input-append date">
                <input type="text" style="display:inline-block" class="input-text" readonly  placeholder="Puja Time"></input>
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>
            <!--
            <script type="text/javascript"
             src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
            </script> 
            <script type="text/javascript"
             src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
            </script>
            <script type="text/javascript"
             src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
            </script>
            -->

            <script type="text/javascript"
                    src="js/date_picker/jquery.min.js">
            </script> 
            <script type="text/javascript"
                    src="js/date_picker/bootstrap.min.js">
            </script>
            <script type="text/javascript"
                    src="js/date_picker/bootstrap-datetimepicker.min.js">
            </script>


<!-- <script type="text/javascript"
 src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
</script>
            -->
            <script type="text/javascript">
                $('#datetimepicker_date').datetimepicker({
                    format: 'dd/MM/yyyy',
                    weekStart: 1,
                    pickTime: false,
                    //startDate: "22-02-2015",

                });
                $('#datetimepicker_time').datetimepicker({
                    format: 'hh:mm:ss',
                    weekStart: 1,
                    pickDate: false,
                    //startDate: "22-02-2015",

                });
            </script>






            <div id='myform_vc_type_errorloc' class="error_strings"></div>            
            <select class="input-text" name="vc_type" id="vc_type">
                <option value="000" selected="selected">Select Puja Type *</option>
                <option value="BILATERAL PUJA">BILATERAL PUJA</option>
                <!-- <option value="YAJMAAN EDGE">YAJMAAN EDGE</option> -->
                <option value="WMP EDGE">WMP EDGE</option>
            </select>


            <textarea class="input-text text-area"  id="comments" name="comments"  placeholder="Your Comments"  cols="0" rows="0" onFocus="if (this.value == this.defaultValue)
                        this.value = '';" onBlur="if (this.value == '')
                                    this.value = this.defaultValue;"></textarea>            



            <input class="input-btn" type="submit" value="Submit" name="Submit" >

        </div>
    </div>


<?php /* ?>


  <table cellspacing="2" cellpadding="2" border="0"  style="width: 90% !important;">
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Name <font color="red">*</font></label></td>
  <td class="td_element">
  <input type="text" class="form-control" id="Name" name="Name" placeholder="Enter Name">
  <div id='myform_Name_errorloc' class="error_strings"></div>
  <!-- <input type="text" name="Name" /> inputError1 -->
  </td>
  </tr>
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Email Id <font color="red">*</font></label></td>
  <td class="td_element">
  <!-- <input type="text" name="Email" /> -->
  <input type="email" class="form-control" id="Email" name="Email" placeholder="Enter Email">
  <div id='myform_Email_errorloc' class="error_strings"></div>
  </td>
  </tr>
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Phone Number <font color="red">*</font></label></td>
  <td class="td_element">
  <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone Number">
  <div id='myform_phone_errorloc' class="error_strings"></div>
  </td>
  </tr>
  <tr>
  <td class="td_label" ><label for="exampleInputEmail1">Address</label></td>
  <td class="td_element">
  <textarea class="form-control" rows="3" type="text" id="address" name="address" placeholder="Enter Address"></textarea>
  <div id='myform_Name_errorloc' class="error_strings"></div>
  </td>
  </tr>
  <tr>
  <td class="td_label" style="padding-top: 10px"><label for="exampleInputEmail1">Country <font color="red">*</font></label></td>
  <td class="td_element">
  <!-- <input type="text" class="form-control" id="country" name="country" placeholder="Enter Country">
  <div id='myform_country_errorloc' class="error_strings"></div> -->

  <select class="form-control" name="country" id="country">
  <option value="000" selected="selected">Select Country</option>
  <?php GetCountry(); ?>
  </select>
  <div id='myform_country_errorloc' class="error_strings"></div>

  </td>


  </tr>
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Select Your Pooja <font color="red">*</font></label></td>
  <td class="td_element">
  <!-- <input type="email" class="form-control" id="dl-horizontal" name="Name" placeholder="Enter Address"> -->
  <select class="form-control" name="sel_pooja" id="sel_pooja">
  <option value="000" selected="selected">Select Pooja</option>
  <?php for($i=0;$i<count($result_arr);$i++){?>
  <option value="<?php echo $result_arr[$i]["name"];?>" <?php if($pooja_name==$result_arr[$i]["name"]) echo "selected='selected'"; ?>  ><?php echo $result_arr[$i]["name"];?></option>
  <?php } ?>
  </select>
  <div id='myform_sel_pooja_errorloc' class="error_strings"></div>
  </td>
  </tr>
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Pooja Date and Time <font color="red">*</font></label></td>
  <td class="td_element">
  <input type="date" class="form-control" id="pooja_date" name="pooja_date" placeholder="Enter Pooja Date and Time">
  <div id='myform_pooja_date_errorloc' class="error_strings"></div>
  </td>
  </tr>
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Pooja Type</label></td>
  <td class="td_element">
  <select class="form-control" name="vc_type" id="vc_type">
  <option value="000" selected="selected">Select Pooja Type</option>
  <option value="BILATERAL POOJA">BILATERAL POOJA</option>
  <option value="YAJMAAN EDGE">YAJMAAN EDGE</option>
  <option value="WMP EDGE">WMP EDGE</option>
  </select>
  <div id='myform_vc_type_errorloc' class="error_strings"></div>
  <!-- <input type="text" name="Name" /> inputError1 -->
  </td>
  </tr>
  <tr>
  <td class="td_label"><label for="exampleInputEmail1">Comments</label></td>
  <td class="td_element">
  <!-- <input type="text" class="form-control" id="dl-horizontal" name="comments" placeholder="Enter Your Comments Here"> -->
  <textarea class="form-control" rows="3" type="text" id="comments" name="comments" placeholder="Enter Your Comments Here"></textarea>
  <div id='myform_Name_errorloc' class="error_strings"></div>
  <!-- <input type="text" name="Name" /> inputError1 -->
  </td>
  </tr>


  <tr>
  <td align="right"></td>
  <td>
  <!-- <input type="submit" value="Submit" /> -->
  <input class="btn btn-default" type="submit" value="Submit" name="Submit" style="width:100px" >
  </td>
  </tr>

  </table>
  <?php */ ?>  
</form>
<script language="JavaScript" type="text/javascript"   xml:space="preserve">//<![CDATA[
//You should create the validator only after the definition of the HTML form
    var frmvalidator = new Validator("myform");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("Name", "req", "Please enter your First Name");
    frmvalidator.addValidation("Name", "maxlen=20", "Max length for FirstName is 20");

    frmvalidator.addValidation("Email", "maxlen=50");
    frmvalidator.addValidation("Email", "req", "Please enter Email");
    frmvalidator.addValidation("Email", "email", "Please enter valid email");

    frmvalidator.addValidation("phone", "req", "Please enter your Phone Number");
    frmvalidator.addValidation("phone", "maxlen=10", "Max length for Phone Number is 10");
    frmvalidator.addValidation("phone", "numeric", "Please enter numbers only");

    frmvalidator.addValidation("country", "req", "Please select your Country");
    frmvalidator.addValidation("sel_pooja", "req", "Please select your Puja");
    frmvalidator.addValidation("pooja_date", "req", "Please enter Pooja Date and Time ");

    frmvalidator.addValidation("country", "dontselect=000", "Please select Country");
    frmvalidator.addValidation("vc_type", "dontselect=000", "Please select Puja type");
    frmvalidator.addValidation("sel_pooja", "dontselect=000", "Please select Puja");
//]]></script>

<?php

function GetCountry() {




    $country = ' 
<option value="Afghanistan">Afghanistan</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="American Samoa">American Samoa</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antarctica">Antarctica</option>
<option value="Antigua and Barbuda">Antigua and Barbuda</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bhutan">Bhutan</option>
<option value="Bolivia">Bolivia</option>
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Bouvet Island">Bouvet Island</option>
<option value="Brazil">Brazil</option>
<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
<option value="Brunei Darussalam">Brunei Darussalam</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burundi">Burundi</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Cape Verde">Cape Verde</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Central African Republic">Central African Republic</option>
<option value="Chad">Chad</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Christmas Island">Christmas Island</option>
<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option value="Colombia">Colombia</option>
<option value="Comoros">Comoros</option>
<option value="Congo">Congo</option>
<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
<option value="Cook Islands">Cook Islands</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Cote Divoire">Cote Divoire</option>
<option value="Croatia">Croatia</option>
<option value="Cuba">Cuba</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="French Guiana">French Guiana</option>
<option value="French Polynesia">French Polynesia</option>
<option value="French Southern Territories">French Southern Territories</option>
<option value="Gabon">Gabon</option>
<option value="Gambia">Gambia</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Greece">Greece</option>
<option value="Greenland">Greenland</option>
<option value="Grenada">Grenada</option>
<option value="Guadeloupe">Guadeloupe</option>
<option value="Guam">Guam</option>
<option value="Guatemala">Guatemala</option>
<option value="Guernsey">Guernsey</option>
<option value="Guinea">Guinea</option>
<option value="Guinea-bissau">Guinea-bissau</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Isle of Man">Isle of Man</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jersey">Jersey</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea, Democratic Peoples Republic of">Korea, Democratic Peoples Republic of</option>
<option value="Korea, Republic of">Korea, Republic of</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Lao Peoples Democratic Republic">Lao Peoples Democratic Republic</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macao">Macao</option>
<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
<option value="Madagascar">Madagascar</option>
<option value="Malawi">Malawi</option>
<option value="Malaysia">Malaysia</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall Islands">Marshall Islands</option>
<option value="Martinique">Martinique</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mayotte">Mayotte</option>
<option value="Mexico">Mexico</option>
<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
<option value="Moldova, Republic of">Moldova, Republic of</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montenegro">Montenegro</option>
<option value="Montserrat">Montserrat</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Myanmar">Myanmar</option>
<option value="Namibia">Namibia</option>
<option value="Nauru">Nauru</option>
<option value="Nepal">Nepal</option>
<option value="Netherlands">Netherlands</option>
<option value="Netherlands Antilles">Netherlands Antilles</option>
<option value="New Caledonia">New Caledonia</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>
<option value="Niue">Niue</option>
<option value="Norfolk Island">Norfolk Island</option>
<option value="Northern Mariana Islands">Northern Mariana Islands</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palau">Palau</option>
<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Pitcairn">Pitcairn</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Reunion">Reunion</option>
<option value="Romania">Romania</option>
<option value="Russian Federation">Russian Federation</option>
<option value="Rwanda">Rwanda</option>
<option value="Saint Helena">Saint Helena</option>
<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option value="Saint Lucia">Saint Lucia</option>
<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
<option value="Samoa">Samoa</option>
<option value="San Marino">San Marino</option>
<option value="Sao Tome and Principe">Sao Tome and Principe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Somalia">Somalia</option>
<option value="South Africa">South Africa</option>
<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Syrian Arab Republic">Syrian Arab Republic</option>
<option value="Taiwan, Province of China">Taiwan, Province of China</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
<option value="Thailand">Thailand</option>
<option value="Timor-leste">Timor-leste</option>
<option value="Togo">Togo</option>
<option value="Tokelau">Tokelau</option>
<option value="Tonga">Tonga</option>
<option value="Trinidad and Tobago">Trinidad and Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
<option value="Tuvalu">Tuvalu</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Emirates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="United States">United States</option>
<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
<option value="Uruguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Venezuela">Venezuela</option>
<option value="Viet Nam">Viet Nam</option>
<option value="Virgin Islands, British">Virgin Islands, British</option>
<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
<option value="Wallis and Futuna">Wallis and Futuna</option>
<option value="Western Sahara">Western Sahara</option>
<option value="Yemen">Yemen</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
';

    echo "$country";
}
?>