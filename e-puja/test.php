<h1> Hellow, Welcome Ninad </h1>




<body style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top" style="padding:20px 0 20px 0">
                <!-- [ header starts here] -->
                <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #E0E0E0;">
                    <tr>
                        <td valign="top">
                            <a href="https://www.wheresmypandit.com/"><img src="https://www.wheresmypandit.com/media/logo/default/logo.png" alt="Where's My Pandit" style="margin-bottom:10px;width: 100px;" border="0"/></a>
                        </td>
                    </tr>
                    <!-- [ middle starts here] -->
                    <tr>
                        <td valign="top">
                            <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Dear $enq_name,</h1>
                            <p style="font-size:12px; line-height:16px; margin:0 0 16px 0;">Panditji kindheartedly thanks you for contacting the team of Where's My Pandit for your Religious Needs. Someone from the team will get in touch with you shortly.</p>
                            <p style="font-size:12px;line-height:16px;margin:0">For any further queries and clarifications feel free to get in touch with our team by writing to us at <a href="mailto:panditji@wheresmypandit.com" style="color:#1e7ec8" target="_blank">panditji@wheresmypandit.com</a> or by giving a call on 022-67079797.</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#EAEAEA" align="center" style="background:#EAEAEA; text-align:center;">
                    <center><p style="font-size:12px; margin:0;">Thank you again, </p></center></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>




<html>

<head>
<meta content="en-us" http-equiv="Content-Language">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>Fresh Newsletter</title>
<style type="text/css">
body {
	margin:0;
	padding:0;
	background-color:#cccccc;
	background:#cccccc;
}
</style>
</head>

<body bgcolor="#cccccc" link="#f26422" vlink="#f26422">
<!-- Start of main container -->
<table align="center" bgcolor="#cccccc" cellpadding="0" cellspacing="0" style="width: 100%; background:#cccccc; background-color:#cccccc; margin:0; padding:0 20px;">
	<tr>
		<td>
		<table align="center" cellpadding="0" cellspacing="0" style="width: 620px; border-collapse:collapse; text-align:left; font-family:Tahoma; font-weight:normal; font-size:12px; line-height:15pt; color:#444444; margin:0 auto;">
			<!-- Start of logo and top links -->
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:20px 0 0 0;;line-height:0;">
				<img alt="" height="5" src="images/BottomBackground_Blue_1.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<tr>
				<td style=" width:620px;" valign="top">
					<table cellpadding="0" cellspacing="0" style="width:100%; border-collapse:collapse;font-family:Tahoma; font-weight:normal; font-size:12px; line-height:15pt; color:#444444;" >
						<tr>
							<td bgcolor="#f26422" style="width: 320px; padding:10px 0 10px 20px; background:#f26422; background-color:#f26422; color:#ffffff;" valign="top">
								<a style="color:#6f2300; text-decoration:underline;" href="#">Forward to a friend</a><span style="color:#fb8047;"> | </span><a style="color:#6f2300; text-decoration:underline;" href="#">Visit our website </a>
							</td>
							<td bgcolor="#f26422" style="width: 300px; padding:10px 20px 10px 20px; background:#f26422; background-color:#f26422; text-align:right; color:#ffffff;" valign="top">
								Sales and Support: +1 (555) 555-5555
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF" style="width: 320px; padding:20px 0 15px 20px; background:#ffffff; background-color:#ffffff;" valign="middle">
								<p style="padding:0; margin:0; line-height:160%; font-size:18px;">
									<img alt="Fresh Newsletter" height="80" src="images/Logo.png" style="padding:0;border:0;" width="300">
								</p>
							</td>
							<td bgcolor="#FFFFFF" style="width: 300px; padding:20px 20px 15px 20px; background:#ffffff; background-color:#ffffff; text-align:center;" valign="middle">
								<a style="text-decoration:none; color:#f26422;" href="#">Having trouble viewing this email?<br>Click here to view the hosted version.</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_Bottom.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<!-- End of logo and top links -->
			<!-- Start of banner -->
			<tr>
				<td style="padding:20px 0 0 0; color:#000000;" valign="top">
				<img hspace="0" vspace="0" alt="image" height="250" src="images/MainPicture.jpg" style="border:0;" width="620"></td>
			</tr>
			<!-- End of banner -->
			<!-- Start of First Content -->
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:20px 0 0 0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_top.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF" style="padding:10px 20px; background:#ffffff;background-color:#ffffff;" valign="top">
					<span style="color:#999999; font-size:8pt;">Sunday, 2 May 2010, Issue # 301</span><br>
					<p style="padding:0; margin:0 0 11pt 0;line-height:160%; font-size:18px;">
					Fresh, and professional Newsletter solution for your business</p>
					<img hspace="20" align="left" alt="image" src="images/Pencil.png" height="120" width="90">Fresh Newsletter is professional and very 
					customizable. It comes in 10 layouts, 12 color themes, 
					Layered and sliced PSD file which contains all graphic Elements of 
					each theme for any color changes you want, commented HTML 
					pages, and user friendly help manual which 
					covers customization steps and instructions how to mix and 
					create new layouts.<br><a style="text-decoration:none; color:#f26422;" href="#"><img alt="�" height="7" src="images/arrow.png" vspace="0" style="border:0;" width="12">&nbsp;Read More</a>
				</td>
			</tr>
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_Bottom.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<!-- End of First Content -->
			<!-- Start of Second Content -->
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:20px 0 0 0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_top.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<tr>
				<td width="620" bgcolor="#FFFFFF" style="padding:10px 20px; background:#ffffff;background-color:#ffffff;" valign="top">
					<span style="font-size: 8pt;color: #999999;">
					Compatibility <img alt="?" height="6" src="images/arrow2.png" width="10"></span><br>
					<p style="padding:0; margin:0 0 11pt 0;line-height:160%; font-size:18px;">
					Tested in major email clients</p>
					<img hspace="20" align="left" alt="image" src="images/area_chart1.png" height="120" width="90">Compatible with all major email clients, Tested in Microsoft Outlook (2000, 2002, 2003, 2007,2010 
					beta), Apple Mail (3, 4), Lotus Notes8, Thunderbird (2, 3), Windows Mail, Windows Live Mail, Postbox, Yahoo Mail, Hotmail, Gmail, and 
					AOL Mail. Compatible with all browsers, HTML pages rendered 
					perfectly In Internet Explorer (5.5, 6, 7, 8), Firefox, 
					Google Chrome, Opera and Safari.<br><a style="text-decoration:none; color:#f26422;" href="#"><img alt="�" height="7" src="images/arrow.png" vspace="0" style="border:0;" width="12">&nbsp;Read More</a>
				</td>
			</tr>
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_Bottom.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<!-- End of Second Content -->
			<!-- AD Start -->
			<tr>
			<td style="padding:20px 0 0 0;">
				<a href="http://videohive.net?ref=Gifky">
				<img alt="AD" height="130" src="images/620ad.png" width="620" style="border:0;"></a></td>
			</tr>
			<!-- AD End -->
			<!-- Start of Third Content -->
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:20px 0 0 0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_top.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<tr>
				<td bgcolor="#FFFFFF" style="padding:10px 20px; background:#ffffff;background-color:#ffffff;" valign="top">
					<span style="font-size: 8pt;color: #999999;">Professionally built <img alt="?" height="6" src="images/arrow2.png" width="10"></span><br>
					<p style="padding:0; margin:0 0 11pt 0;line-height:160%; font-size:18px;">
					Usability and elegance</p>
					<img hspace="20" align="left" alt="image" src="images/star.png" height="120" width="90">It&#39;s 
					designed with usability and elegance in mind. Take for 
					example layout 1, height of two columns at the bottom always 
					matching no matter if your contents height (in the left and 
					right columns) are equal or not, the same with sidebar height 
					(layout 6 &amp; 7) always matching the other side column 
					height, you never get taller column or messy look of your newsletter. Check out screenshots.<br><a style="text-decoration:none; color:#f26422;" href="#"><img alt="�" height="7" src="images/arrow.png" vspace="0" style="border:0;" width="12">&nbsp;Read More</a>
				</td>
			</tr>
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:0;line-height:0;">
				<img alt="" height="5" src="images/Newsletter_border_Bottom.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<!-- End of Third Content -->
			<!-- Start of two content in row Container -->
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" style="width: 100%;border-collapse:collapse;">
					<tr>
						<!-- Start of left Column -->
						<td style="width:300px;" valign="top">
						<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse:collapse; font-family:Tahoma; font-weight:normal; font-size:12px; line-height:15pt; color:#444444;">
							<!-- Start of First content in left Column -->
							<tr>
								<td valign="bottom" style="height:5px;margin:0;padding:20px 0 0 0;line-height:0;">
								<img alt="" height="5" src="images/300pxWindow_Top.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="300"></td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFFFFF" style="padding:10px 20px; background:#ffffff;background-color:#ffffff; width:300px; margin:0;">
									<span style="font-size: 8pt;color: #999999;">Fact 3 <img alt="?" height="6" src="images/arrow2.png" width="10"></span><br>
									<p style="padding:0; margin:0 0 11pt 0;line-height:160%; font-size:18px;">Visual appeal!</p>
									<img hspace="20" vspace="5" align="left" alt="image" src="images/pencil_icon.png" height="24" width="24">HTML formatting was favored by 95% in 2009. - E-Tail Group(2010)<br><a style="text-decoration:none; color:#f26422;" href="#"><img alt="�" height="7" src="images/arrow.png" vspace="0" style="border:0;" width="12">&nbsp;Read More</a>
								</td>
							</tr>
							<tr>
								<td valign="top" style="height:5px;margin:0;padding:0 0 20px 0;line-height:0;"><img alt="" height="5" src="images/300pxWindow_Bottom.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="300"></td>
							</tr>
							<!-- End of First content in left Column -->
							<!-- Start of Second content in left Column -->
							<tr>
								<td valign="top" style="padding:0; width:300px; margin:0;">
									<a style="text-decoration:none; color:#f26422;" href="http://graphicriver.net?ref=Gifky">
									<img hspace="0" vspace="0" alt="image" src="images/ad_300px1.png" height="100" width="300" style="border:0;"></a>
								</td>
							</tr>
							<!-- End of Second content in left Column -->
						</table>
						</td>
						<!-- End of left Column -->
						<!-- Start of space bewteen left and right Columns - DO NOT REMOVE IT - -->
						<td style="width:20px;" valign="top">&nbsp;</td>
						<!-- End of space bewteen left and right Columns -->
						<!-- Start of right Column -->
						<td style="width:300px;" valign="top">
						<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse:collapse; font-family:Tahoma; font-weight:normal; font-size:12px; line-height:15pt; color:#444444;">
							<tr>
								<td valign="bottom" style="height:5px;margin:0;padding:20px 0 0 0;line-height:0;">
								<img alt="" height="5" src="images/300pxWindow_Top.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="300"></td>
							</tr>
							<tr>
								<td valign="top" bgcolor="#FFFFFF" style="padding:10px 20px; background:#ffffff;background-color:#ffffff; width:300px; margin:0;">
									<span style="font-size: 8pt;color: #999999;">Do you know about <img alt="?" height="6" src="images/arrow2.png" width="10"></span><br>
									<p style="padding:0; margin:0 0 11pt 0;line-height:160%; font-size:18px;">
									The power  of social media</p>
									<img hspace="20" align="left" alt="image" src="images/twitter_icon.png" height="64" width="64">Do you know that (based on actual tweet ID numbers) 
									over 12 Billion Tweets have been made! That&#39;s why you should 
									put Twitter and other social media links in your newsletter, if people like 
									your content, they will follow you! and tweet about your products.<br><a style="text-decoration:none; color:#f26422;" href="#"><img alt="�" height="7" src="images/arrow.png" vspace="0" style="border:0;" width="12">&nbsp;Read More</a>
								</td>
							</tr>
							<tr>
								<td valign="top" style="height:5px;margin:0;padding:0;line-height:0;"><img alt="" height="5" src="images/300pxWindow_Bottom.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="300"></td>
							</tr>
						</table>
						</td>
						<!-- End of right Column -->
					</tr>
					</table>
				</td>
			</tr>
			<!-- End of two content in row Container -->
			<!-- Start of Footer -->
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:20px 0 0 0;line-height:0;">
				<img alt="" height="5" src="images/BottomBackground_Blue_1.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<tr>
				<td bgcolor="#f26422" style="padding:0 20px 15px 20px; background-color:#f26422; background:#f26422;">
					<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse:collapse; font-family:Tahoma; font-weight:normal; font-size:12px; line-height:15pt; color:#FFFFFF;">
						<tr>
							<td style="padding:20px 0;" colspan="2">
								<img align="left" alt="Fresh Newsletter" height="80" src="images/Logo-orange.png" style="border:0;" width="300">
							</td>
						</tr>
						<tr>
							<td style="width:340px; padding:0 20px 0 0;">
								301 some street name, city name, state, country<br>
								<a style="color:#6f2300; text-decoration:underline;" href="#">www.yourwebsite.com</a> 
								| <a style="color:#6f2300; text-decoration:underline;" href="#">email@website.com</a><br>
								Sales and Support: +1 (555) 555-5555
							</td>
							<td valign="middle" align="center" style="text-align:center; width:240px;">
								<table cellpadding="0" cellspacing="5" style="width: 100%; border-collapse:collapse;">
									<tr>
										<td style="width:20%;"><a style="text-decoration:none; color:#6f2300;" href="#"><img alt="RSS" height="32" src="images/rss.png" style="border:0;" width="32"></a></td>
										<td style="width:20%;"><a style="text-decoration:none; color:#6f2300;" href="#"><img alt="Twitter" height="32" src="images/twitter.png" style="border:0;" width="32"></a></td>
										<td style="width:20%;"><a style="text-decoration:none; color:#6f2300;" href="#"><img alt="Facebook" height="32" src="images/facebook.png" style="border:0;" width="32"></a></td>
										<td style="width:20%;"><a style="text-decoration:none; color:#6f2300;" href="#"><img alt="Youtube" height="32" src="images/youtube.png" style="border:0;" width="32"></a></td>
										<td style="width:20%;"><a style="text-decoration:none; color:#6f2300;" href="#"><img alt="Linkedin" height="32" src="images/linkedin.png" style="border:0;" width="32"></a></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:20px 0 0 0;" colspan="2">
								You are currently signed up to Company�s newsletters as: <a style="color:#6f2300; text-decoration:underline;" href="#">email@email.com</a>. <a style="color:#6f2300; text-decoration:underline;" href="#">To unsubscribe click here</a><br>Copyright � 2010 
								Your Company Name.
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="height:5px;margin:0;padding:0 0 20px 0;line-height:0;">
				<img alt="" height="5" src="images/BottomBackground_Blue_2.png" vspace="0" style="border:0; padding:0; margin:0; line-height:0;" width="620"></td>
			</tr>
			<!-- End of Footer -->
		</table>
		</td>
	</tr>
</table>
<!-- End of main container -->
</body>
</html>





<!DOCTYPE html>
<html>
    <body>
        <div align="center" style="width:100%">
            <div align="center" style="background-color:#FFF;max-width:770px;min-width:200px">

                <table class="tbl_comparison" style="margin-top: 25px;width: 100%;">
                    <tbody>
                        <tr>
                            <th class="col_label" colspan="2" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;text-align: left;"> 
                    <h3>Registration Details For E-Puja </h3>

                    </th>
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Name</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$name</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Email</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$Email</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Phone</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$phone</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Address</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$address</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Country</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$country</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Selected Pooja</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$sel_pooja</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Pooja Date</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$pooja_date</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">E-Puja Type</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$vc_type</td>                
                    </tr>
                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Comments</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$comments</td>                
                    </tr>            

                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Generated From</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$pooja_name Page</td>                
                    </tr>

                    <tr>
                        <td class="col_label" style="color: #333;font-weight: bold;width: 5%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">Client Ip</td>
                        <td class="col_B" style="padding-left: 38px;border-left: 1px solid #dce3eb;border-right: 1px solid #dce3eb;width: 35%;border-bottom: 1px solid #dce3eb;color: #7b7b79;padding: 15px;vertical-align: top;">$client_ip</td>                
                    </tr>


                    </tbody>
                </table>



            </div>
        </div>
    </body>
</html>






<?php

{
  "data": [
    {
      "address_obj": {
        "street1": "1 Seaport Lane",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02210",
        "address_string": "1 Seaport Lane, Boston, MA 02210"
      },
      "percent_recommended": null,
      "latitude": "42.349224",
      "rating": "4.5",
      "location_id": "94330",
      "ranking_data": {
        "ranking_string": "#1 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "1",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/94330?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d94330-Reviews-m28724-Seaport_Boston_Hotel-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Seaport Boston Hotel",
      "num_reviews": "2771",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d94330-m28724-Seaport_Boston_Hotel-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d94330-m28724-Reviews-Seaport_Boston_Hotel-Boston_Massachusetts.html#photos",
      "longitude": "-71.041275"
    },
    {
      "address_obj": {
        "street1": "70 Rowes Wharf",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02110",
        "address_string": "70 Rowes Wharf, Boston, MA 02110"
      },
      "percent_recommended": null,
      "latitude": "42.356506",
      "rating": "4.5",
      "location_id": "89575",
      "ranking_data": {
        "ranking_string": "#2 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "2",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/89575?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d89575-Reviews-m28724-Boston_Harbor_Hotel-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Boston Harbor Hotel",
      "num_reviews": "1306",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d89575-m28724-Boston_Harbor_Hotel-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d89575-m28724-Reviews-Boston_Harbor_Hotel-Boston_Massachusetts.html#photos",
      "longitude": "-71.05025"
    },
    {
      "address_obj": {
        "street1": "500 Commonwealth Avenue",
        "street2": null,
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02215",
        "address_string": "500 Commonwealth Avenue, Boston, MA 02215"
      },
      "percent_recommended": null,
      "latitude": "42.34873",
      "rating": "4.5",
      "location_id": "258705",
      "ranking_data": {
        "ranking_string": "#3 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "3",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/258705?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d258705-Reviews-m28724-Hotel_Commonwealth-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Hotel Commonwealth",
      "num_reviews": "3075",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d258705-m28724-Hotel_Commonwealth-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d258705-m28724-Reviews-Hotel_Commonwealth-Boston_Massachusetts.html#photos",
      "longitude": "-71.09526"
    },
    {
      "address_obj": {
        "street1": "61 Exeter Street at Boylston",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02116-2699",
        "address_string": "61 Exeter Street at Boylston, Boston, MA 02116-2699"
      },
      "percent_recommended": null,
      "latitude": "42.349316",
      "rating": "4.5",
      "location_id": "114134",
      "ranking_data": {
        "ranking_string": "#4 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "4",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/114134?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d114134-Reviews-m28724-Lenox_Hotel-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "GreenLeader",
          "year": "",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleader_small-28724-5.gif",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleaders/GreenLeaders_API_en_large_platinum-28724-5.jpg"
          },
          "categories": [
            "Platinum level"
          ],
          "display_name": "GreenLeader Platinum"
        },
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Lenox Hotel",
      "num_reviews": "2627",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d114134-m28724-Lenox_Hotel-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d114134-m28724-Reviews-Lenox_Hotel-Boston_Massachusetts.html#photos",
      "longitude": "-71.07981"
    },
    {
      "address_obj": {
        "street1": "15 Beacon St",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02108",
        "address_string": "15 Beacon St, Boston, MA 02108"
      },
      "percent_recommended": null,
      "latitude": "42.358",
      "rating": "4.5",
      "location_id": "94337",
      "ranking_data": {
        "ranking_string": "#5 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "5",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/94337?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d94337-Reviews-m28724-XV_Beacon-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "XV Beacon",
      "num_reviews": "823",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d94337-m28724-XV_Beacon-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d94337-m28724-Reviews-XV_Beacon-Boston_Massachusetts.html#photos",
      "longitude": "-71.062164"
    },
    {
      "address_obj": {
        "street1": "776 Boylston St",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02199",
        "address_string": "776 Boylston St, Boston, MA 02199"
      },
      "percent_recommended": null,
      "latitude": "42.34869",
      "rating": "4.5",
      "location_id": "1136705",
      "ranking_data": {
        "ranking_string": "#6 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "6",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/1136705?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d1136705-Reviews-m28724-Mandarin_Oriental_Boston-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Mandarin Oriental, Boston",
      "num_reviews": "442",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d1136705-m28724-Mandarin_Oriental_Boston-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d1136705-m28724-Reviews-Mandarin_Oriental_Boston-Boston_Massachusetts.html#photos",
      "longitude": "-71.08165"
    },
    {
      "address_obj": {
        "street1": "88 Exeter St",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02116",
        "address_string": "88 Exeter St, Boston, MA 02116"
      },
      "percent_recommended": null,
      "latitude": "42.348515",
      "rating": "4.5",
      "location_id": "313573",
      "ranking_data": {
        "ranking_string": "#7 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "7",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/313573?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d313573-Reviews-m28724-Courtyard_by_Marriott_Boston_Copley_Square-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "GreenLeader",
          "year": "",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleader_small-28724-5.gif",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleaders/GreenLeaders_API_en_large_gold-28724-5.jpg"
          },
          "categories": [
            "Gold level"
          ],
          "display_name": "GreenLeader Gold"
        },
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Courtyard by Marriott Boston Copley Square",
      "num_reviews": "1071",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d313573-m28724-Courtyard_by_Marriott_Boston_Copley_Square-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d313573-m28724-Reviews-Courtyard_by_Marriott_Boston_Copley_Square-Boston_Massachusetts.html#photos",
      "longitude": "-71.079"
    },
    {
      "address_obj": {
        "street1": "200 Boylston St",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02116",
        "address_string": "200 Boylston St, Boston, MA 02116"
      },
      "percent_recommended": null,
      "latitude": "42.352467",
      "rating": "4.5",
      "location_id": "89585",
      "ranking_data": {
        "ranking_string": "#8 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "8",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/89585?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d89585-Reviews-m28724-Four_Seasons_Hotel_Boston-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Four Seasons Hotel Boston",
      "num_reviews": "1105",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d89585-m28724-Four_Seasons_Hotel_Boston-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d89585-m28724-Reviews-Four_Seasons_Hotel_Boston-Boston_Massachusetts.html#photos",
      "longitude": "-71.06852"
    },
    {
      "address_obj": {
        "street1": "1271 Boylston Street",
        "street2": null,
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02215",
        "address_string": "1271 Boylston Street, Boston, MA 02215"
      },
      "percent_recommended": null,
      "latitude": "42.34505",
      "rating": "4.5",
      "location_id": "6485213",
      "ranking_data": {
        "ranking_string": "#9 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "9",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/6485213?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d6485213-Reviews-m28724-The_Verb_Hotel-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "The Verb Hotel",
      "num_reviews": "348",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d6485213-m28724-The_Verb_Hotel-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d6485213-m28724-Reviews-The_Verb_Hotel-Boston_Massachusetts.html#photos",
      "longitude": "-71.096924"
    },
    {
      "address_obj": {
        "street1": "3 McKinley Square",
        "street2": "",
        "city": "Boston",
        "state": "Massachusetts",
        "country": "United States",
        "postalcode": "02109",
        "address_string": "3 McKinley Square, Boston, MA 02109"
      },
      "percent_recommended": null,
      "latitude": "42.359097",
      "rating": "4.5",
      "location_id": "94344",
      "ranking_data": {
        "ranking_string": "#10 of 82 hotels in Boston",
        "ranking_out_of": "82",
        "geo_location_id": "60745",
        "ranking": "10",
        "geo_location_name": "Boston"
      },
      "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/94344?key=8b9908b52e2e4e2c8e780541450f628e",
      "location_string": "Boston, Massachusetts",
      "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d94344-Reviews-m28724-Marriott_s_Custom_House-Boston_Massachusetts.html",
      "price_level": "$$$$",
      "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
      "awards": [
        {
          "award_type": "Certificate of Excellence",
          "year": "2015",
          "images": {
            "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
            "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
          },
          "categories": [],
          "display_name": "Certificate of Excellence 2015"
        }
      ],
      "name": "Marriott's Custom House",
      "num_reviews": "593",
      "write_review": "http://www.tripadvisor.com/UserReview-g60745-d94344-m28724-Marriott_s_Custom_House-Boston_Massachusetts.html",
      "category": {
        "name": "hotel",
        "localized_name": "Hotel"
      },
      "subcategory": [
        {
          "name": "hotel",
          "localized_name": "Hotel"
        }
      ],
      "ancestors": [
        {
          "abbrv": null,
          "level": "City",
          "name": "Boston",
          "location_id": "60745"
        },
        {
          "abbrv": "MA",
          "level": "State",
          "name": "Massachusetts",
          "location_id": "28942"
        },
        {
          "abbrv": null,
          "level": "Country",
          "name": "United States",
          "location_id": "191"
        }
      ],
      "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d94344-m28724-Reviews-Marriott_s_Custom_House-Boston_Massachusetts.html#photos",
      "longitude": "-71.05338"
    }
  ],
  "paging": {
    "next": null,
    "previous": null,
    "results": "10",
    "total_results": "10",
    "skipped": "0"
  }
}














{
    "data": [
        {
            "address_obj": {
                "street1": "81 South Huntington Ave",
                "street2": null,
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02130",
                "address_string": "81 South Huntington Ave, Boston, MA 02130"
            },
            "distance": ".62",
            "percent_recommended": null,
            "bearing": "west",
            "latitude": "42.33",
            "rating": "4.5",
            "location_id": "3225572",
            "ranking_data": {
                "ranking_string": "#18 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "18",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/3225572?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d3225572-Reviews-m28724-EnVision_Hotel_Boston-Boston_Massachusetts.html",
            "price_level": "$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
            "awards": [
                {
                    "award_type": "GreenLeader",
                    "year": "",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleader_small-28724-5.gif",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleaders/GreenLeaders_API_en_large_partner-28724-5.jpg"
                    },
                    "categories": [
                        "GreenPartner"
                    ],
                    "display_name": "GreenLeader Partner"
                },
                {
                    "award_type": "Certificate of Excellence",
                    "year": "2015",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
                    },
                    "categories": [],
                    "display_name": "Certificate of Excellence 2015"
                }
            ],
            "name": "enVision Hotel Boston",
            "num_reviews": "391",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d3225572-m28724-EnVision_Hotel_Boston-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d3225572-m28724-Reviews-EnVision_Hotel_Boston-Boston_Massachusetts.html#photos",
            "longitude": "-71.111336"
        },
        {
            "address_obj": {
                "street1": "342 Longwood Avenue",
                "street2": "",
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02115",
                "address_string": "342 Longwood Avenue, Boston, MA 02115"
            },
            "distance": ".63",
            "percent_recommended": null,
            "bearing": "northwest",
            "latitude": "42.33871",
            "rating": "4.0",
            "location_id": "77638",
            "ranking_data": {
                "ranking_string": "#46 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "46",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/77638?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d77638-Reviews-m28724-The_Inn_at_Longwood_Medical-Boston_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-28724-5.png",
            "awards": [],
            "name": "The Inn at Longwood Medical",
            "num_reviews": "743",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d77638-m28724-The_Inn_at_Longwood_Medical-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d77638-m28724-Reviews-The_Inn_at_Longwood_Medical-Boston_Massachusetts.html#photos",
            "longitude": "-71.10686"
        },
        {
            "address_obj": {
                "street1": "1271 Boylston Street",
                "street2": null,
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02215",
                "address_string": "1271 Boylston Street, Boston, MA 02215"
            },
            "distance": ".95",
            "percent_recommended": null,
            "bearing": "north",
            "latitude": "42.34505",
            "rating": "4.5",
            "location_id": "6485213",
            "ranking_data": {
                "ranking_string": "#9 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "9",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/6485213?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d6485213-Reviews-m28724-The_Verb_Hotel-Boston_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
            "awards": [
                {
                    "award_type": "Certificate of Excellence",
                    "year": "2015",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
                    },
                    "categories": [],
                    "display_name": "Certificate of Excellence 2015"
                }
            ],
            "name": "The Verb Hotel",
            "num_reviews": "349",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d6485213-m28724-The_Verb_Hotel-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d6485213-m28724-Reviews-The_Verb_Hotel-Boston_Massachusetts.html#photos",
            "longitude": "-71.096924"
        },
        {
            "address_obj": {
                "street1": "125 Brookline Avenue",
                "street2": null,
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02215",
                "address_string": "125 Brookline Avenue, Boston, MA 02215"
            },
            "distance": ".98",
            "percent_recommended": null,
            "bearing": "north",
            "latitude": "42.34563",
            "rating": "4.5",
            "location_id": "4325990",
            "ranking_data": {
                "ranking_string": "#14 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "14",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/4325990?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d4325990-Reviews-m28724-Residence_Inn_Boston_Back_Bay_Fenway-Boston_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
            "awards": [
                {
                    "award_type": "GreenLeader",
                    "year": "",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleader_small-28724-5.gif",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleaders/GreenLeaders_API_en_large_silver-28724-5.jpg"
                    },
                    "categories": [
                        "Silver level"
                    ],
                    "display_name": "GreenLeader Silver"
                },
                {
                    "award_type": "Certificate of Excellence",
                    "year": "2015",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
                    },
                    "categories": [],
                    "display_name": "Certificate of Excellence 2015"
                }
            ],
            "name": "Residence Inn Boston Back Bay / Fenway",
            "num_reviews": "621",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d4325990-m28724-Residence_Inn_Boston_Back_Bay_Fenway-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d4325990-m28724-Reviews-Residence_Inn_Boston_Back_Bay_Fenway-Boston_Massachusetts.html#photos",
            "longitude": "-71.10024"
        },
        {
            "address_obj": {
                "street1": "1023 Beacon Street",
                "street2": "",
                "city": "Brookline",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02446",
                "address_string": "1023 Beacon Street, Brookline, MA 02446"
            },
            "distance": "1.08",
            "percent_recommended": null,
            "bearing": "northwest",
            "latitude": "42.34572",
            "rating": "3.0",
            "location_id": "258730",
            "ranking_data": {
                "ranking_string": "#3 of 3 hotels in Brookline",
                "ranking_out_of": "3",
                "geo_location_id": "60823",
                "ranking": "3",
                "geo_location_name": "Brookline"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/258730?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Brookline, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60823-d258730-Reviews-m28724-Beacon_Townhouse_Inn_1023-Brookline_Massachusetts.html",
            "price_level": null,
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/3.0-28724-5.png",
            "awards": [],
            "name": "Beacon Townhouse Inn 1023",
            "num_reviews": "3",
            "write_review": "http://www.tripadvisor.com/UserReview-g60823-d258730-m28724-Beacon_Townhouse_Inn_1023-Brookline_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Brookline",
                    "location_id": "60823"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60823-d258730-m28724-Reviews-Beacon_Townhouse_Inn_1023-Brookline_Massachusetts.html#photos",
            "longitude": "-71.10777"
        },
        {
            "address_obj": {
                "street1": "220 Huntington Avenue",
                "street2": "",
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02115",
                "address_string": "220 Huntington Avenue, Boston, MA 02115"
            },
            "distance": "1.16",
            "percent_recommended": null,
            "bearing": "northeast",
            "latitude": "42.343666",
            "rating": "3.5",
            "location_id": "94350",
            "ranking_data": {
                "ranking_string": "#63 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "63",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/94350?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d94350-Reviews-m28724-The_Midtown_Hotel-Boston_Massachusetts.html",
            "price_level": "$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/3.5-28724-5.png",
            "awards": [],
            "name": "The Midtown Hotel",
            "num_reviews": "1531",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d94350-m28724-The_Midtown_Hotel-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d94350-m28724-Reviews-The_Midtown_Hotel-Boston_Massachusetts.html#photos",
            "longitude": "-71.08383"
        },
        {
            "address_obj": {
                "street1": "645 Beacon St",
                "street2": "",
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02215",
                "address_string": "645 Beacon St, Boston, MA 02215"
            },
            "distance": "1.17",
            "percent_recommended": null,
            "bearing": "north",
            "latitude": "42.34833",
            "rating": "3.5",
            "location_id": "94372",
            "ranking_data": {
                "ranking_string": "#66 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "66",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/94372?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d94372-Reviews-m28724-Boston_Hotel_Buckminster-Boston_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/3.5-28724-5.png",
            "awards": [],
            "name": "Boston Hotel Buckminster",
            "num_reviews": "878",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d94372-m28724-Boston_Hotel_Buckminster-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d94372-m28724-Reviews-Boston_Hotel_Buckminster-Boston_Massachusetts.html#photos",
            "longitude": "-71.09783"
        },
        {
            "address_obj": {
                "street1": "500 Commonwealth Avenue",
                "street2": null,
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02215",
                "address_string": "500 Commonwealth Avenue, Boston, MA 02215"
            },
            "distance": "1.22",
            "percent_recommended": null,
            "bearing": "north",
            "latitude": "42.34873",
            "rating": "4.5",
            "location_id": "258705",
            "ranking_data": {
                "ranking_string": "#3 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "3",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/258705?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d258705-Reviews-m28724-Hotel_Commonwealth-Boston_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-28724-5.png",
            "awards": [
                {
                    "award_type": "Certificate of Excellence",
                    "year": "2015",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_small-28724-5.jpg",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/CERTIFICATE_OF_EXCELLENCE_2015_en_US_large-28724-5.jpg"
                    },
                    "categories": [],
                    "display_name": "Certificate of Excellence 2015"
                }
            ],
            "name": "Hotel Commonwealth",
            "num_reviews": "3075",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d258705-m28724-Hotel_Commonwealth-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d258705-m28724-Reviews-Hotel_Commonwealth-Boston_Massachusetts.html#photos",
            "longitude": "-71.09526"
        },
        {
            "address_obj": {
                "street1": "1200 Beacon Street",
                "street2": "",
                "city": "Brookline",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02446",
                "address_string": "1200 Beacon Street, Brookline, MA 02446"
            },
            "distance": "1.22",
            "percent_recommended": null,
            "bearing": "northwest",
            "latitude": "42.343834",
            "rating": "3.5",
            "location_id": "94238",
            "ranking_data": {
                "ranking_string": "#2 of 3 hotels in Brookline",
                "ranking_out_of": "3",
                "geo_location_id": "60823",
                "ranking": "2",
                "geo_location_name": "Brookline"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/94238?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Brookline, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60823-d94238-Reviews-m28724-Holiday_Inn_Boston_Brookline-Brookline_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/3.5-28724-5.png",
            "awards": [
                {
                    "award_type": "GreenLeader",
                    "year": "",
                    "images": {
                        "small": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleader_small-28724-5.gif",
                        "large": "http://www.tripadvisor.com/img/cdsi/img2/awards/greenleaders/GreenLeaders_API_en_large_partner-28724-5.jpg"
                    },
                    "categories": [
                        "GreenPartner"
                    ],
                    "display_name": "GreenLeader Partner"
                }
            ],
            "name": "Holiday Inn Boston Brookline",
            "num_reviews": "461",
            "write_review": "http://www.tripadvisor.com/UserReview-g60823-d94238-m28724-Holiday_Inn_Boston_Brookline-Brookline_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Brookline",
                    "location_id": "60823"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60823-d94238-m28724-Reviews-Holiday_Inn_Boston_Brookline-Brookline_Massachusetts.html#photos",
            "longitude": "-71.1164"
        },
        {
            "address_obj": {
                "street1": "40 Dalton St",
                "street2": "",
                "city": "Boston",
                "state": "Massachusetts",
                "country": "United States",
                "postalcode": "02115-3123",
                "address_string": "40 Dalton St, Boston, MA 02115-3123"
            },
            "distance": "1.27",
            "percent_recommended": null,
            "bearing": "northeast",
            "latitude": "42.346348",
            "rating": "4.0",
            "location_id": "111428",
            "ranking_data": {
                "ranking_string": "#50 of 82 hotels in Boston",
                "ranking_out_of": "82",
                "geo_location_id": "60745",
                "ranking": "50",
                "geo_location_name": "Boston"
            },
            "api_detail_url": "http://api.tripadvisor.com/api/partner/2.0/location/111428?key=8b9908b52e2e4e2c8e780541450f628e",
            "location_string": "Boston, Massachusetts",
            "web_url": "http://www.tripadvisor.com/Hotel_Review-g60745-d111428-Reviews-m28724-Hilton_Boston_Back_Bay-Boston_Massachusetts.html",
            "price_level": "$$$$",
            "rating_image_url": "http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-28724-5.png",
            "awards": [],
            "name": "Hilton Boston Back Bay",
            "num_reviews": "1810",
            "write_review": "http://www.tripadvisor.com/UserReview-g60745-d111428-m28724-Hilton_Boston_Back_Bay-Boston_Massachusetts.html",
            "category": {
                "name": "hotel",
                "localized_name": "Hotel"
            },
            "subcategory": [
                {
                    "name": "hotel",
                    "localized_name": "Hotel"
                }
            ],
            "ancestors": [
                {
                    "abbrv": null,
                    "level": "City",
                    "name": "Boston",
                    "location_id": "60745"
                },
                {
                    "abbrv": "MA",
                    "level": "State",
                    "name": "Massachusetts",
                    "location_id": "28942"
                },
                {
                    "abbrv": null,
                    "level": "Country",
                    "name": "United States",
                    "location_id": "191"
                }
            ],
            "see_all_photos": "http://www.tripadvisor.com/Hotel_Review-g60745-d111428-m28724-Reviews-Hilton_Boston_Back_Bay-Boston_Massachusetts.html#photos",
            "longitude": "-71.08496"
        }
    ],
    "paging": {
        "next": null,
        "previous": null,
        "results": "10",
        "total_results": "10",
        "skipped": "0"
    }
}


?>